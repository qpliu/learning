# 概述

目前主要有三类 model free 的深度强化学习方法：

1. TRPO， PPO
2. DDPG 及其扩展（D4PG，TD3等）
3. Soft Q-Learning, Soft Actor-Critic



PPO 是目前比较主流的，可以应对离散和连续控制，在 OpenAI Five 上很成功。但是 PPO 是一种 on-policy 的算法。所有 on-policy 的算法都面对一个问题： sample inefficiency. 要成功训练，需要大量的采样，对于实体机器人来说，这是很难实现的。



DDPG 是 DeepMind 开发的面向连续控制的 off policy 算法，因此比 PPO 更 sample efficient. DDPG 训练的是一种确定性策略，即每一个 state 下都只考虑最优的一个动作。DDPG 的扩展版 D4PG 效果据说很好，但没有开源，也没有人复现 DeepMind 的效果。



SAC 是基于最大熵的一种 off policy 算法。与 DDPG 不同的是，SAC 采用随机策略，这相比于确定性策略有一定的优势。SAC 在公开的 benchmark 上取得了非常好的效果，并且很适用于真是机器人。SAC 代码是完全公开的。





# stable_baselines

## 内置 policy 网络

|     Policy      |                         Description                          |
| :-------------: | :----------------------------------------------------------: |
|    MlpPolicy    | Policy object that implements actor critic, using a MLP (2 layers of 64) |
|  MlpLstmPolicy  | Policy object that implements actor critic, using LSTMs with a MLP feature extraction |
| MlpLnLstmPolicy | Policy object that implements actor critic, using a layer normalized LSTMs with a MLP feature extraction |
|    CnnPolicy    | Policy object that implements actor critic, using a CNN (the nature CNN) |
|  CnnLstmPolicy  | Policy object that implements actor critic, using LSTMs with a CNN feature extraction |
| CnnLnLstmPolicy | Policy object that implements actor critic, using a layer normalized LSTMs with a CNN feature extraction |

其中 CnnPolicies 只能用于图片，MlpPolicies 用于其他的场景。

如果动作空间是连续的，则会有边界限制 （DDPG 和 SAC 除外），以避免出错。



如果要修改内置的 policy 网络

- 通过设定相应 policy 中 `policy_kwargs` 参数，这是小修改
- 也可以创建全新的自定义 policy

  

   

 

   



