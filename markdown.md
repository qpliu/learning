## Basic concept
The Markdown language was created in 2004 by John Gruber with the goal of allowing people "to write using an easy-to-read, easy-to-write plain text format, and optionally convert it to structurally valid XHTML or HTML".

## 段落引用
> This text is cited from other place.
> This is the second line.
>
> >Citations can be nested
> >this is the second line. 

## 超链接
有Inline和Reference两种方式。
 - Inline，即link地址直接给出: [I'm a link to Google](www.google.com "Google is here")
 - Reference, 先标记一下，link地址在后边给出： [I'm a link to Google][ref]

[ref]: www.google.com "Google is here"

## 插入图片
也有Inline和Reference两种方式
  - Inline: 
      ![show me if image cannot be loaded](https://images-na.ssl-images-amazon.com/images/I/71IA-vgtyZL._AC_UL115_.jpg "some image")
  - Reference:
      ![show me if image cannot be loaded][id]
[id]: https://images-na.ssl-images-amazon.com/images/I/71IA-vgtyZL._AC_UL115_.jpg "some image"
   Markdown中出入图片无法设定图片大小，一个解决方案是用HTML语言。
   ```html
   <img src="file_path" alt="show me if img is not loaded" width=300 height=300 align="center">
   ```

## 原样显示编程代码
 - inline
   This is my code `def funtion():`
 - block
  ```
   def function(): 
  
  ```
 - syntax highlight
  ```python
   def function(): 
  
  ```

## 字体加强

   *斜体*,  **粗体**

## 任务列表

- [ ] 

- [x] 

实现方式：

`- [] `

即 横线+空格+中括号（内有空格）+ 空格 + 回车



## 表格

| Tables          |      Are      | Cool |
| --------------- | :-----------: | ---: |
| *col 3 is*      | right-aligned | 1600 |
| **col 2 is**    |   centered    |   12 |
| `zebra stripes` |   are neat    |    1 |

## 公式

- 分段函数，即左侧有大括号

$$
\begin{cases}
x = 1 & \text{if  $~~x=1$} \\
x = 2 & \text{if $~x=2$}
\end{cases}
$$

- 矩阵
  $$
  A = \begin{vmatrix}
  1 & 2 & 3 \\
  4 & 5 & 6 \\
  7 & 8 & 9
  \end{vmatrix}
  $$

  $$
  B = \begin{bmatrix}
  1 & 2 & 3 \\
  4 & 5 & 6 \\
  7 & 8 & 9
  \end{bmatrix}
  $$

- 多行函数
  $$
  \begin{align}
  y &= Ax + Bu \\
  z &= cx
  \end{align}
  $$


