## 区块链 blockchain









## 定义算力

- **FLOPS**： **fl**oat-point **o**perations **p**er **s**econd. 每秒浮点运算次数。浮点运算就是
  - MFLOPS = megaFLOPS  每秒多少 $10^6$ 次浮点运算
  - GFLOPS = gigaFLOPS  $10^9$次
  - TFLOPS = teraFLOPS  $10^{12}$次  （1 太拉）
  - PFLOPS = petaFLOPS $10^{15}$次
- **TOPS**: **t**era **o**perations **p**er **s**econd 每秒 多少 $10^{12}$次运算。注意，这里不局限于浮点运算了。

- **DL TOPS**: deep learning TOPS 执行神经网络深度学习相关操作的次数



## 网络检测

- 查询网地址： route
- 用 Ping 检测网络故障
  - ping 127.0.0.1 通则说明网卡工作正常
  - ping 网关 ip 通则说明网关工作正常
  - ping 网关内其他主机  通则说明主机到网关这一段工作正常



## Duplex 双工与 simplex 单工

双工是指双向点对点通讯，包括全双工和半双工：

- full duplex (FDX) 全双工：可以同时双向通讯，例如电话
- half duplex (HDX) 半双工：不能同时双向通讯，同一时刻只能一方向另一方通讯，例如对讲机 walkie-talkie，一方想讲话时，按下对讲按钮，此时只能发送，不能接收；如果要听对方讲话，则放开按钮，此时只能收不能发。

在不需要双向通讯的场合，例如广播、电视、监视器等，可以使用单工通讯。



## WebSocket

这是一种计算机通讯协议，基于 TCP 链接之上的全双工 ( full duplex ) 通讯通道。

与 HTTP 的异同：

- 都是基于 TCP 协议。
- HTTP 是半双工 half duplex，只能一方请求，然后一方发送。
- WebSocket 兼容 HTTP 协议，使用 TCP 80 端口或者在 TLS 加密情况下使用 443 端口。对于那些有防火墙屏蔽 HTTP 协议以外的应用时，WebSocket 依然可以使用。
- 大部分浏览器都支持 WebSocket 协议通许。
- 在建立 WebSocket 通讯时，与 HTTP 类似，也是一方作为 client 先请求，另一方作为 server 接收。之后的通讯就是双向的了。