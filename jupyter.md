### 添加和删除 kernel

- 查看现有 kernel：`jupyter kernelspec list`

- 添加 python kernel

  ```
  python2 -m pip install ipykernel
  python2 -m ipykernel install --user
  
  python3 -m pip install ipykernel
  python3 -m ipykernel install --user
  ```

- 添加 julia kernel，就是安装 "IJulia" package

- 删除 kernel:  `jupyter kernelspec uninstall Kernel_name`



### 加 TOC 扩展

jupyter notebook 默认不支持Markdown中的TOC，可以按照如下方法添加 Jupyter notebook extensions：

[github地址](https://github.com/ipython-contrib/jupyter_contrib_nbextensions)

1. Install python package: 
     `pip install https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tarball/master --user`

2. Install javascript and css files:
    `jupyter contrib nbextension install --user`

3. Enable extension
    `jupyter nbextension enable toc2`

  如果报错，也可以直接从notebook页面的extension中设置

### 修改默认 cell type

当jupyter notebook主要用来写markdown时，为了方便，可以将cell的默认type从code改成markdown，方法如下：

1. 找到 main.min.js 文件，可以用命令 `locate main.min.js`。不同机子上位置可能不同，我见过的 
  - `/usr/local/lib/python2.7/dist-packages/notebook/static/notebook/js/main.min.js`
  - `/.local/lib/python2.7/site-packages/notebook/static/notebook/js`  

2. 打开文件，找到 
```
Notebook.options_default = {
        // can be any cell type, or the special values of
        // 'above', 'below', or 'selected' to get the value from another cell.
        default_cell_type: 'code',
        Header: true,
        Toolbar: true
    };
```
  将其中 `default_cell_type` 设定为 `markdown`即可。

### 在virtualenv 中使用 notebook

既然 jupyter 中可以有 python2 , python3 作为 kernel，也可以把用户自定义的 Python 环境拿来作为 kernel.

1. 在virtual环境中安装 `pip install ipykernel`

2. 将本环境作为kernel添加到notebook中： `ipython kernel install --user --name=projectname`

3. 不论是在什么环境下，打开notebook，都有本环境kernel

4. 在 virtualenv 中安装 package 可以直接在 Jupyter 中使用，不需要重启 jupyter notebook，但可能需要 reload 已经打开的 .ipynb 文件才能看到新的 kernel


