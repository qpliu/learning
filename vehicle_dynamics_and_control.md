# OEM, tier 1 - 3

- OEM： original equipment manufacturer，是汽车制造商，其产品就是最终面向市场的汽车，例如大众，宝马，丰田，福特等。

- tier 1: 直接为并且只为 OEM 提供汽车零部件的公司，也是汽车领域比较知名的，例如 Bosch, Denso, continental 等

- tier 2: 其零部件并不局限于汽车领域

- tier 3: 基本上是提供原材料级别的产品

  

![tiers](pic/tiers.jpg)





# ECU 与 DCU

电动汽车 electric car

## ECU

electronic control unit  电子控制单元

随着车辆的电子化程度提高，一辆车里一般包含 50-100 个 ECU，渗透到了 anti-lock braking system, automatic transmission, airbag system, entertainment system 等。

一个 ECU 包含若干组成部分：micro control unit (MCU), microprocessor unit (MPU), digital signal processor (DSP), logic integrated circuits (IC).

目前全球知名的 ECU 供应商包括 Bosch, Denso, Continental, Aptiv, Visteon 等。

ECU 架构是分散式，one-to-one，一个 ECU 负责一个特定功能，导致线路结构比较复杂。目前出现了一些集中式的架构，如 domain control unit (DCU) 和 multi-domain controller (MDC).

## DCU

domain control unit 域控制单元。

在高级别的自动驾驶汽车中，DCU 承担了传感器融合、定位、规划、无线通讯等功能。由于需要大量运算，DCU 一般都应有运算能力很强的处理器芯片。

目前业内的 nvidia、TI、Mobileye、华为等都提出了具有超强算力的芯片。比较知名的包括：

- Nvidia  drive PX2 计算平台
- 华为在昇腾系统 AI 芯片基础上打造了 MDC (MobileData Center)，试图为车企提供自动驾驶全栈服务
- Intel 收购 Mobileye 之后，推出自动驾驶计算平台 Eye Q5
- NXP 恩智浦，汽车半导体传统供应商，为自动驾驶打造了 BlueBox 计算平台
- Qualcomm 高通，自动驾驶平台 DriveAutomotive
- 特斯拉 FSD 自动驾驶芯片
- 另外，地平线、寒武纪等都有AI 芯片发布，可用于深度学习和自动驾驶。







# 四冲程汽车引擎工作原理

这里主要介绍汽油引擎 (gasoline engine 或 petrol engine).

本质上是使用汽油作为燃料的**内燃机**。

一般引擎包括多个气缸，例如 4 缸， 6 缸等。

以 4 冲程气缸为例，工作过程如下图所示：

1. 空气流过化油器，携带汽油进入气缸
2. 压缩气缸中的气体，与空气混合成均匀的可然混合气
3. 点火系统提供瞬时高电压，通过火花塞打出火花，引燃缸内气体，刚内气压迅速升高，推动曲轴做功。
4. 曲轴旋转回来，将燃烧后的废气排出。



![engine1](pic/engine1.gif)



# Actuation lag

在考虑车辆控制时，油门的控制量并不能瞬间传达到车轮转速，这中间会有延迟。

大概传动过程如下：

期望加速度 --- 油门 --- 进气阀 --- 引擎转速 --- 变速箱 --- 传动轴 --- 轮子转速

这中间肯定会有延迟，而且由于是机械设备 (不是电传动)，延迟时间比较可观。

在建模中，常常用一阶低通滤波器表示 actuation lag。

## 一阶低通滤波器

### RC filter

最常见的就是 RC 滤波器

![rc_lowpass1](pic/rc_lowpass1.png)

直观的理解是 

- 电容对高频信号表现为短路，因此 vout 中不会有高频的输出
- 电容对低频信号表现为断路，因此 vout 中包含了低频信号

### RC filter 离散形式为滑动平均

上图中满足公式
$$
v_{in} - v_{out} = R i \\
Q_c = Cv_{out} \\
i = \frac{dQ_c}{dt}
$$
后两个式子也可以合并写成
$$
i = C\frac{dv_{out}}{dt}
$$
都整合在一起，得到
$$
v_{in} - v_{out} = RC \frac{dv_{out}}{dt}
$$
上式离散化，采样间隔为 $\Delta_T$，并以 x, y 表示输入和输出，得到
$$
x_i - y_i = RC\frac{y_i - y_{i-1}}{\Delta_T}
$$
整理一下
$$
\begin{align}
y_i &= \frac{RC}{RC+\Delta_T}y_{i-1} + \frac{\Delta_T}{RC+\Delta_T}x_i \\
    &= (1-\alpha) y_{i-1} + \alpha x_i 
\end{align}
$$
其中 
$$
\alpha = \frac{\Delta_T}{RC+\Delta_T}
$$
几点观察：

- $RC = \Delta_T$  时，即时间常数等于采样间隔，那么输出等于输入和前一时刻输出的平均值
- $RC$ 越大，采样间隔越短，则输出变化的越平缓。

cutoff 频率 $f_c$ 可以时间按常数表示：
$$
f_c = \frac{1}{2\pi RC} \\
w_c = 2\pi f_c = \frac{1}{RC}
$$
最后，再提一下连续时间表达式对应的 Laplace 形式
$$
x - y =RCsy = \tau s y \\
y = \frac{1}{\tau s + 1} x
$$
这就是最基础、最经典的一阶系统表达式。

## 一阶高通滤波器 （附赠）

### RC filter

![highpass_filter1](pic/highpass_filter1.png)

实际上就是将 R 与 C 调换了位置。

- 高频时 C 相当于短路，高频电流影响输出
- 低频时 C 相当于断路，低频电流被阻碍。

### 离散形式

上图满足关系式：
$$
v_{out} = R i \\
Q_c = C(v_{in} - v_{out}) \\
i = \frac{dQ}{dt}
$$
上式整合
$$
v_{out} = RC (\frac{dv_{in}}{dt} - \frac{dv_{out}}{dt} )
$$
离散化得
$$
y_i = RC (\frac{x_i - x_{i-1}}{\Delta_T} - \frac{y_i - y_{i-1}}{\Delta_T})
$$
整理一下
$$
\begin{align}
y_i &= \frac{RC}{RC+\Delta_T}y_{i-1} + \frac{RC}{RC+\Delta_T}(x_i - x_{i-1})\\
&= \alpha y_{i-1} +  \alpha (x_i - x_{i-1})
\end{align}
$$
几点观察：

- 与 lowpass 相同，当时间常数 == 采样间隔时，输出等于前一时刻输出与两次输入之差的平均值

高通似乎没有太直观的解释，低通的话就是滑动平均形式。



## 用一阶低通滤波器表示 actuation lag 

在文献中，经常将时间常数设置为 $\tau = 0.5s$，这里包含了从油门到车轮的所有延迟，可以看作是最坏情况。

用如下方程表示 actuation lag
$$
\ddot{x}_k = 
\begin{cases}
u_0 & \textrm{if } k=0 \\
(1-\alpha) \ddot{x}_{k-1} + \alpha u_k & \textrm{otherwise}  
\end{cases}
$$
这里
$$
\alpha = \frac{\Delta_T}{\tau + \Delta_T}
$$
这样建模之后，控制量作用的曲线如下：

![actuation_lag1](pic/actuation_lag1.png)

这种建模是把油门 (正加速) 和刹车 (负加速) 认为具有相同的 lag，实际上刹车的 lag 要更小一些。可以考虑建立更贴近实际的 lag 模型。





























