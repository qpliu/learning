# 5机器学习研究概览

  *The Discipline of Machine Learning (From Tom Mitchell, 2006)*

- 机器学习要解决的两个问题：（1）如何构建一个可以自我提升的计算系统；（2）探索所有学习过程（人、动物、机器）的共同规律。
- 机器学习自然发源于计算机科学和统计学。另外，还与生理学和神经科学有关，但由于我们当前对后两种学科了解的还很少，因此并没有给机器学习带来重大启示。
- 机器学习的成功应用包括
  - **语音识别**，尤其是有些软件在交付使用之后，能够根据客户的口音，逐渐学习提高识别水平
  - **机器视觉**，目前美国超过85%的手写邮件通过手写识别软件自动分类，在经过大规模数据训练之后，精度很高
  - **机器人控制**，Darpa挑战赛，采用机器学习的自动驾驶汽车在沙漠中行驶超过100英里，它可以通过传感器数据不断提升自己的水平
  - **数据密集型的实证性科学**，如气象学、生物医学、脑科学等，很多都在相关领域会议中举办了机器学习的workshop，表明机器学习在这些学科中的巨大应用潜力

- 机器学习擅长的领域
   - 复杂的应用环境，**无法手动编写全部程序**，如基于外部信息输入的任务，语言识别、机器视觉等。我们很难写出程序，告诉计算机什么样的图像中包含有猫。
   - 需要软件**个性化的应用环境**，如语音识别系统，可以根据使用人的口音，调整识别系统中的参数，还有个性化的推荐系统，垃圾邮件识别系统等。

- 未来研究方向
  - **未标记数据能否帮助提升监督学习效果？**人工对数据进行标记比较费时，如果那些未标记的数据也能提高监督学习的效果，则整个学校效率将会极大的提升。
  - **在哪种情况下采用哪种算法？**需要有一个比较明确的理论分析和解释。这些算法的性能，优缺点都需要比较系统的理论分析。
  - **如何有效的收集训练数据？**训练数据对于机器学习是至关重要的，但在收集数据的过程中，可能存在冗余，垃圾信息，如何更有效的收集数据，直接关系训练效果。
  - **如何在保护数据隐私性同时从数据挖掘中获益？**在医疗诊断系统的开发中，一个主要的障碍是病人数据的保密性。一种解决方法是，把系统给医院，有他们输入数据进行训练，而不是把数据给外人。
  - **如何构建永久学习的系统？**人和动物的一生都是在学习的，现在机器学习则面对某一任务，训练完了就结束了。我们是不是也能够构建长期的学习系统，例如编写一个程序，训练它浏览网页，不断学习如何提取信息，从最基本的识别人的名字和邮箱，到从大段文章中提取复杂的信息。
  - **机器学习和动物的学习能否互相借鉴？**
  - **能否设计新的编程语言，更好的适应机器学习？**编程人员秩序编写部分程序，剩余的部分可以命令程序“去学习”。

# machine learning and data science

machine learning can be viewed as a tool in many applications, and data science is one of them.

**The lifecycle of data science**

1. Hypothesis
2. Data
3. Model
4. Product

Machine learning is used to find an optimal model in Step 3.

So machine learning is a useful tool to achieve some specific goals.

# machine learning 基本元素

machine learning 本质上是构造数学模型通过数据学习其中的参数，从而逼近某种映射关系，进而预测未知的输出。

machine learning 大体由三部分构成： 
- **representation:** 即数学模型的形式，是线性多项式模型，是神经网络，还是决策树。
- **evaluation:** 即评价模型的优劣，在给定参数下，学习、预测的错误率。比较常见的是 MSE(Mean Squared Error).
- **optimization:** 基于模型评价，改进模型，最优的逼近理想模型。这个寻优的过程就是 optimization.这也是为什么优化在机器学习中这么重要、这么基础。机器学习的过程本质上就是优化数学模型中参数的过程。







# 数据集预处理

## 数据集划分

不同的研究者会有不同的划分方式，主要是以下两类：

- 划分 1

| training | cross validation | test |
| :------: | :--------------: | :--: |
|   80%    |       10%        | 10%  |

- 划分 2

| training | test |
| :------: | :--: |
|   80%    | 20%  |

## feature scaling

![feature_scaling1](/home/qpliu/learning/pic/feature_scaling1.png)



- feature 很大时，loss 对其权重改变就会很敏感，例如上图 x2 和 w2，等高线较密集。
- 反之，则等高线较稀疏

这种长宽相差较大的椭圆形不利于搜索，而进行了 feature scaling 之后搜索会更快。

一般希望 feature 数据大小近似，比如预测房价时，离中心城区的位置 x1 和 房间面积 x2 显然不是一个数量级的，应该转化一下。

一般是转化到以 0 为均值，方差为 1。主要方法是 Z score
$$
z = \frac{x-\mu}{\sigma}
$$
其中，$x$ 为一个 feature，$\mu$ 为数据集中该 feature 的均值，$\sigma$  为相应的标准差。 转化之后，$z$ 的均值为 0 ， 标准差是 1.

Take image data as an example.

![data_preprocess](/home/qpliu/learning/pic/data_preprocess1.png)





# supervised learning

以下介绍典型的 supervised learning 方法，包括

- 线性回归
- Bayes 分类器
- logistic 回归
- 
- 

# 线性回归 (Linear regression)

## 为什么叫回归？

这是由达尔文的表兄弟 Francis Galton 发明的。他当时做了很多统计预测工作，如根据上一代豌豆的尺寸预测下一代豌豆的尺寸，人的身高等。他发现，上一代的尺寸如果超过平均值，则下一代的尺寸也倾向于超过平均值，但不及父母。也就是说孩子的身高向着平均身高回归 (regression)。Galton在很多研究对象上看到这种现象，就将这种预测问题称为回归问题。待求的预测方程称为回归方程，待求的系数称为回归系数。

## 为什么叫线性回归？

因为回归方程里面的输入项都是与常量相乘，然后结果相加，都是线性项，不存在输入与输入相乘的高次项，例如 $y = a_0 + a_1x_1 + \cdots + a_nx_n $。非线性回归则可能出现形式 $y = a x_1/x_2$ 等.

## Loss function

一个选择是 RMSE，Root Mean Square Error 均方根误差。
$$
RMSE = \sqrt{\frac{1}{n}\sum_{i=1}^n(\hat{y}-y)^2}
$$
在 Scikit-learn中没有 RMSE 函数，只有 MSE 函数，对其取平方根即可。

## 如何求线性回归系数？

基于给定的一堆数据，找到一条与他们拟合最好的直线，最基本的方法是**最小二乘法**。

### 二维形式

假设待求函数为 $y = w_1 x + w_0$，其优化目标函数为
$$
\min\sum_i(y_i - w_1x_i - w_0)^2
$$
经过基本的求导等零运算，得到
$$
w_0 = \frac{1}{m}\sum y_i - \frac{w_1}{m}\sum x_i 
$$

$$
w_1 = \frac{m\sum x_i y_i - \sum x_i \sum y_i}{m\sum x_i^2 - (\sum x_i)^2}
$$


### 一般形式

优化目标函数可以写成 
$$
\sum_{i=1}^m (y_i - x_i^Tw)^2
$$
其中 $x_i, y_i (i = 1, \cdots ,m)$ 为已知的输入、输出数据， $w$ 为待求的系数向量。
写成对应的矩阵形式
$$
(Y-Xw)^T(Y-Xw)
$$
为了寻找 $w$ 使目标函数最小，可以用求偏导数的方法，解得最优的回归系数为
$$
w = (X^TX)^{-1}X^TY
$$

### 非线性函数

对于非线性函数，一般找不到上面这么简单的解析解。所以一般用梯度下降之类的算法，逐步迭代求解。形式如下：

- 给定初始参数取值 $w^0$
- 依照梯度下降法修改参数 $w^m = w^{(m-1)} - \alpha \frac{\partial L}{\partial w} $

## 梯度下降法

### 方向导数

我们可以很容易地求出单变量函数 $f(x)$ 的导数。对于双变量函数 $f(x,y)$，我们可以很容易地求出沿 $x$ 轴正方向和 $y$ 轴正方向的偏导数。那么**如何求出双变量函数沿任意方向的导数呢？**

在二维平面上，我们用单位向量 $ u=\cos\theta~ \textrm{i} + \sin\theta ~\textrm{j}$ 表示任意方向，其中 $\theta$ 是该向量与 $x$ 轴的夹角。如下图所示：
![u direction](pic/u_direction.png)

我们希望求出函数 $f(x,y)​$ 在 $P(x_0, y_0)​$ 点处沿 $u​$ 方向的导数。实际上，如下图所示，曲面函数 $f(x,y)​$ 在 $u​$ 方向的导数/斜率，就是曲线 C 在点 $(x_0, y_0, f(x_0, y_0))​$ 处的斜率。
![curveC](pic/curveC.png)

这实际上转化成了普通导数的问题。只需要计算当 $ P(x_0, y_0)$ 和 $Q(x_0 + t\cos\theta, y_0 + t\sin\theta)$ 的距离 $t$ 趋于 0 时， 对应函数的差距变化趋势即可。

- **定义双变量函数 $ f(x,y)$ 在点 $P(x_0, y_0)$ 处沿 $u=\cos\theta~ \textrm{i} + \sin\theta ~\textrm{j}$ 方向的导数如下：**

$$
D_u f(x_0, y_0) = \lim_{t\rightarrow 0} \frac{f(x_0 + t\cos \theta , y_0 + t\sin\theta) - f(x_0, y_0)}{t}
$$

- **方向导数与偏导数的关系**(证明略)

$$
D_u f(x, y) = f_x(x,y)\cos\theta + f_y(x,y)\sin\theta
$$

### 梯度

- **定义 $ f(x,y)$ 的梯度为**

$$
\bigtriangledown f(x,y) = f_x(x,y) ~\textrm{i} + f_y(x,y) ~\textrm{j}
$$

- **方向导数与梯度的关系**

$$
D_u f(x, y) 
= f_x(x,y)\cos\theta + f_y(x,y)\sin\theta\\
 = [f_x(x,y) ~\textrm{i} + + f_y(x,y) ~\textrm{j} ] \cdot [\cos\theta ~\textrm{i} + \sin\theta ~\textrm{j}]\\
 = \bigtriangledown f(x,y) \cdot u
$$

即方向导数就是梯度点乘该方向的单位向量。

由于$
D_u f(x, y) = \bigtriangledown f(x,y) \cdot u = \|\bigtriangledown f(x,y)\| \|u\|\cos \phi ​$ , 其中 $\phi​$ 为梯度方向与导数方向$u​$ 的夹角。 当 $\phi = 0​$ 时取最大值，即**某一点处的方向导数在梯度方向上取最大值**，也就是梯度方向上斜率最大，此处上升最快，梯度反方向下降最快。

### 偏导数、方向导数、梯度之间的关系

- 有了沿 $ x$ 轴正方向和沿 $y$ 轴正方向的偏导数 $f_x(x,y)$ 和 $f_y(x,y)$， 则可以很容易的得到沿 $u = \cos\theta i + \sin \theta j$ 的**方向导数** $f_x(x,y)\cos\theta + f_y(x,y)\sin \theta$。方向导数是一个标量。
- 基于偏导数，也可以很容易得到**梯度** $ \bigtriangledown f(x,y) = f_x(x,y) i + f_y(x,y) j$。梯度是一个向量，与 $x,y$ 属于同一维度。所以才有自变量沿梯度方向改变的可能。

## constrained optimization and Lagrange multiplier

### Contour map

Contour map is used to depict the output inside an input plane.

Let's see the following function graph.

![contour](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/contour1.png)

This graph is shown in 3D space. We hope to show its basic information in 2D space, more precisely, in its input variables space: $xy$-plane.

We can first slice the $z$-axis by evenly-spaced planes as follows:

![contour](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/contour2.png)

The intersections of the function graph and the planes are some curves as follows:

![contour](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/contour3.png)

At last, project these curves into the $xy$-plane, and label the heights/$z$ value correspond to.

![contour](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/contour4.png)

Properties of contour graph:

- In general, the slicing planes are evenly-spaced, such that the labels of contour lines increase or decrease with a constant value.
- At any point in the $xy$-plane, gradient at that point is perpendicular to the contour line.
- An area with dense contour lines cooresponds to steep grpah. 

### Lagrange multiplier

Suppose we are facing the following constrained optimization:
$$
\max ~f(x,y)
$$

$$
\textrm{s.t.}~~ g(x,y)=c
$$

We can depict the line of $g(x,y)=c$ and contour lines of $f(x,y$ on $xy$-plane at the same time.

![lagrange](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/lagrange1.png)

In general, the maximum point is where the constrained line $g(x,y)=c$  is tangent with the contour line of $f(x,y)$. And once tangent, their gradients are at the same direction with different magnitude, which means, at the point $(x_0, y_0)$ we have
$$
\bigtriangledown f(x_0,y_0) = \lambda_0 g(x_0,y_0)
$$
The above vector-equation can be broken into its components as follows:
$$
 f_x(x_0,y_0) = \lambda_0 g_x(x_0,y_0)
$$

$$
 f_y(x_0,y_0) = \lambda_0 g_y(x_0,y_0)
$$

Together with the constraint:
$$
g(x_0, y_0) = c
$$
We only need to solve the above equations to get the solutions. If there exists multiple solutions satisfy the equations, compare them and find the maximum and minimum.

However, Lagrange provided a single equation:
$$
L(x,y,\lambda) = f(x,y) - \lambda (g(x,y)-c)
$$
Note that 
$$
\bigtriangledown L = 0
$$
can be broken into
$$
L_x = f_x - \lambda g_x =0
$$

$$
L_y = f_y - \lambda g_y =0
$$

$$
L_{\lambda} = g(x,y) - c =0
$$

which are equivalent to the former set of equations.

## LASSO 

LASSO stands for *** least absolute shrinkage and selection operator***.

From its name we know that LASSO can do two things: make the absolute value of coefficients as small as possible and make some of them to be zero. We will see them in detail later.

### Basic form 

Essentially, LASSO is a kind of regression analysis method. Its optimization function has the following from:

$$ \min_{\beta}\|y-X\beta\|_2^2$$
$$s.t. \|\beta\|_1\le t$$

That is a standard least square optimization function with a $L_1$ constraint.

It has the following Lagrangian form:
$$ \min_{\beta}(\|y-X\beta\|_2^2 + \lambda \|\beta\|_1)$$

### Compared with other constrain optimization form

- *** regression with best subset selection: *** $ \min_{\beta}(\|y-X\beta\|_2^2 + \lambda \|\beta\|_0)$
  where $\|Z\|=m$ if exactly $m$ components of $Z$ are nonzero.
  This kind of regression method can selection part of the coefficients, and leave others being zero, i.e., selecting significant feature
- *** ridge regression: *** $ \min_{\beta}(\|y-X\beta\|_2^2 + \lambda \|\beta\|_2^2)$
  This kind of regression method can confine the value of the coefficient, i.e., shrink their value.

LASSO has both functions, and that is why it is called least absolute shrinkage and selection operator. 

### Geometry interpretation

The following figure shows why LASSO can select coefficient while ridge cannot.

![lasso_compare](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/lasso_compare.png)



## 正则化 Regularization 

本质上是一种 complexity control。

一般优化目标函数写成 Loss = Loss (data) + Loss (parameters)，例如
$$
\sum_i(y_i - w_1x_i - w_0)^2 + \sum_j||w_j||^p
$$
后边那一项 p 范数就是对 parameter 的限制，使 parameter 拉到靠近 0 的位置。另外，不同的 p 范数可以得到不同的 parameter 分布形式。p = 2 对应围绕 0 点的圆， p = 1 对应顶点在轴上的正方形。



# 贝叶斯分类器

## 基本思想

- 例1： 从 box 中取球

![naive_bayes1](pic/naive_bayes1.png)

- 例2： 将例 1 推广

  ![naive_bayes2](pic/naive_bayes2.png)

- 总之，我们最终是要计算 $p(class|new\_data)$  的值，即得到一个新的 data ，计算概率，如果概率 > 0.5 就认为属于某个 class。
- 为了得到这个概率值，按照 Bayes 法则， 我们需要计算出来 prior:  $p(class)$ 和 likelyhood:  $p(new\_data|class)$ ，然后根据 bayes公式计算
- 这里的关键是如何计算 likelyhood，首先是需要根据 现有的 sample  拟合出来 sample 是从哪个概率分布中提取出来的。这一步就是简单的 maximum likelyhood 方法。有了这个分布，就可以求 $p(new\_data|class)$  了.

## 计算 prior

P(c1) 和 P(c2) 计算都很简单，就是每个类型中数据占全体的比例

## 计算 likelyhood

![naive_bayes3](pic/naive_bayes3.png)

每个 data 都有它的 feature。先通过现有的采样数据把分布拟合出来，比如用 maximum likelyhood 找到合适的正态分布，然后再计算在这个正态分布下 p(x|C1) 是多少。一般是用 multivariable Gaussian distribution:
$$
f_{\mu, \Sigma}(x) = \frac{1}{(2\pi)^{D/2}}\frac{1}{|\Sigma|^{1/2}}\exp\{-\frac{1}{2}(x-\mu)^T\Sigma^{-1}(x-\mu)\}
$$
其中 D 为 x 维度。

## 分类结果

![naive_bayes4](pic/naive_bayes4.png)

## 修改模型：共享 variance

原本每个 class 我们都要计算一个 p(data|class) 的分布，他们 mean 和 variance matrix 是不同的。现在强制要求他们的 variance matrix 相同，即。这样就减少了很多需要拟合的参数。

![naive_bayes5](pic/naive_bayes5.png)

![naive_bayes6](pic/naive_bayes6.png)

此时分割线就是一条直线。

## 朴素贝叶斯分类器 （Naive Bayes classifier）

上边考虑的分布都是 multi-variable 正态分布，各个 feature 之间是有关联的。如果我们假设各个 feature 之间是独立的，就转化成了多个 一维 正态分布，有
$$
p(x|c1) = p(x_1|c1)p(x_2|c1)\cdots p(x_n|c1)
$$
这个时候，variance matrix 就是 diagonal 的，因此，在 maximum likelyhood 中需要考虑的参数就更少。

但是不考虑各维之间的关联，可能会丢失重要信息。

![weak_naive_bayes1](pic/weak_naive_bayes1.png)

在上述例子中， testing data 直觉来看应该是属于 class 1的，但是用 naive bayes classifier 得到的结果是属于 class 2.

# Logistic regression

## 与 Bayesian classifier 的联系

实际上，Logistic regression 的**形式**可以从 Bayesian classifier 推导出来。

在 Bayesian classifier 中，我们最终要求的公式为
$$
p(c1|x) = \frac{p(x|c1)p(c1)}{p(x|c1)p(c1)+p(x|c2)p(c2)} = \frac{1}{1+\frac{p(x|c2)p(c2)}{p(x|c1)p(c1)}}
$$
令
$$
z = ln\frac{p(x|c1)p(c1)}{p(x|c2)p(c2)}
$$
则，
$$
p(c1|x) = \frac{1}{1+e^{-z}} = \sigma(z)
$$
这就是 logistic function.

下面是对 $z$ 做变换

![naive_bayes7](pic/naive_bayes7.png)





![naive_bayes8](pic/naive_bayes8.png)





![naive_bayes9](pic/naive_bayes9.png)



还是需要假设 $\Sigma^1 = \Sigma^2 = \Sigma$. 在这一假设的前提下， Bayes classifier 就得到了 Logistic regression 的形式。

最终就转化成了 $z=w^Tx +b$ 的形式，要求的概率就是 $p(c1|x) = \sigma(w^Tx + b)$. 

**总结**： Bayes classifier 和由其推导出的 Logistic regression 对模型的拟合是殊途同归。前者用概率分布的方法 maximum likelyhood 求出分布参数；后者直接计算 $w^T$ 和 $b$。

**注意：** 并不能说两者在假设 $\Sigma^1 = \Sigma^2 = \Sigma$.的前提下就完全等价。两者的设定还是有些不同

- 在假设 $\Sigma^1 = \Sigma^2 = \Sigma$.的前提下，Bayes 在形式上可以转化成 logistic regression，但只是特殊设定下的 logistic regression
- 一般的 logisitc regression 没有什么假设条件，但是 bayes 是有分布概率的假设的，例如服从 Gaussian distribution. 
- 因此，logistic regression 找到的模型可能比 bayes 更好，因为它可以在更广阔的环境中搜索参数。

## 对比 Discriminative 与 Generative 

像 bayes 这样先计算分布参数或者联合概率分布，再计算概率的方式称为 Generative 模型；logistic regression 中的模型直接计算最终的条件概率分布，称为 discriminative 模型

![compare_bayes_logistic](pic/compare_bayes_logistic1.png)

这两种模型各有优缺点，一般来说 discriminative 更好些，但并不是一定的。

generative 的优势在于：

![discriminative_generative1](pic/generative_discriminative1.png)

即 generative 模型是要做一些分布假设的，这个假设在数据数量较少时或者噪声较大时是有帮助的，而且在求概率分布的过程中 prior 和 条件概率可以分开写，这就可以通过不同的数据渠道得到两方面的值。

## 基本形式

令  $z=w^Tx +b$ ，

![logistic_regression1](pic/logistic_regression1.png)

## 图形表示

![logistic_regression2](pic/logistic_regression2.png)

## 激活函数

### Sigmoid function

sigmoid 函数并不是一个函数，而是一类函数，共同特点是曲线是 “S” 型的。

常见的 sigmoid function 包括：

#### Logistic function

![logistic1](/home/qpliu/learning/pic/logistic1.png)

一般形式
$$
f(x) = \frac{L}{1+e^{-k(x-x_0)}}
$$
标准形式 (standard logistic function)
$$
f(x) = \frac{1}{1+e^{-x}} = \frac{e^x}{e^x + 1}
$$
即 $L=1$, $k=1$, $x_0 =0$ 

- 导数
  $$
  f'(x) = \frac{e^x(e^x+1) - e^{2x}}{(e^x+1)^2} = \frac{e^x}{(e^x + 1)^2} = \frac{e^x}{e^x +1} \frac{1}{e^x+1} = f(x) f(-x) = f(x)(1-f(x))
  $$







![logistic_function1](/home/qpliu/learning/pic/logistic_function1.png)

上图中 $\sigma(z) = f(x)$，可以看出来在曲线的首、尾部分，斜率比较小，上升慢，中间部分，斜率比较大，上升快。

#### hyperbolic tangent 双曲正切

<img src="/home/qpliu/learning/pic/tanh1.svg" width=10cm>
$$
\tanh(x) = \frac{e^x - e^{-x}}{e^x + e^{-x}} = \frac{e^{2x} - 1}{e^{2x} + 1}
$$

#### logistic function 与 tanh 的关系

$$
f(x) = \frac{1}{2}\left(\tanh(\frac{x}{2}) + 1\right)
$$

或
$$
\tanh(x) = 2f(2x) - 1
$$
所以说，tanh 函数是 shifted and scaled version of the logistic function



### Softplus

<img src="/home/qpliu/learning/pic/softplus1.png" width=13cm>

对 ReLU 的光滑近似，公式为
$$
f(x) = \log(1+e^x)
$$

- 导数
  $$
  f'(x) = \frac{e^x}{e^x+1}
  $$
  即 softplus 函数的导数是 logistic function
  **激励函数一般是需要可导的，可是 relu 函数明显不可导啊，它在 back propagation 中怎么办的？**

## Loss function 

衡量一个函数与 data 的拟合程度，可以计算这些 sample data 在函数条件下产生的概率，概率越大，说明函数越接近实际。也就是将 data 带入到函数中，得到的概率越大越好。

-  先对 class 进行数值化

  ![logistic_loss1](pic/logistic_loss1.png)

- loss 函数可以转化成 cross-entropy

![logistic_loss3](pic/logistic_loss3.png)



- 如何得到最小的  cross-entropy

  ![logistic_regression4](pic/logistic_regression4.png)

   最后就得到了权重更新公式。


###  cross-entropy 与 Kullback–Leibler divergence 的区别

- cross-entropy: $H(p,q) = - \sum_i p_i \log(q_i)$ 

  一个cross-entropy 对应一个数据得到的概率分布，这里的 sum 是元素的 sum，而不是数据集的 sum。

- KL divergence: $D(p||q) = \sum_i p_i \log\frac{p_i}{q_i} = \sum_i p_i \log p_i - \sum_i p_i \log q_i = -H(p) + H(p,q)$

  其中 $H(p)$ 为 $p$ 的 entropy

- 两者关系： $H(p,q) = H(p) + D(p||q)$

- cross-entropy 最小值为 $H(p)$ ，KL divergence 最小值为 0

在写 cross-entropy 的时候，注意 p 和 q 都对应什么。其实也很好记。label 元素有 0 ，不可能对 0 取 log。而 Softmax函数的输出都是大于0的，都是可以取 log 的，所以放在后边 log 的位置上。

### gradient descent

有了 Loss function 之后，下一步就是如何调整参数，使 loss function 最小。gradient descent is very commonly used. 以 2 参数优化为例，假设中心区域对应较小 loss：

![gradient_descent1](/home/qpliu/learning/pic/gradient_descent1.png)

### 如何选择初始参数

![initial_parameter1](/home/qpliu/learning/pic/initial_parameter1.png)

可以考虑以 0 为均值选择。方差小，表示参数都集中在类似区域，计算出来的 softmax 差别不大，不确定性比较高。一般初始时都用较小的方差。

## logistic regression 与 linear regression 对比

![compare_linear_logistic1](pic/compare_linear_logistic1.png)

- 采用的函数方面，一个是 logistic function， 一个是 linear function
- 采用的 loss function 方面，一个是 cross-entropy，一个是 square error
- 采用的权重更新方面，两者是一样的。

上边的 logistic regression 中 loss function 是从概率 maximum likelyhood 推导出来的，看起来比较复杂，能不能直接用 square error 呢？ 跟 linear 一样？

**不可以！**

![logistic_square1](pic/logistic_square1.png)

参数较优的时候，预测与实际很接近，导数小，没有问题。

但是参数不好时，导数也很小，这样就更新很慢。

以下是 logistic regression 中用 cross entropy 和用 square error 作为 loss function 的对比

![logistic_square2](pic/logistic_square2.png)



## Multi-class classification

### softmax function

在 2 类分类中，只要计算 
$$
\frac{1}{1+e^{-\sigma}} = \frac{1}{1+e^{-(wx+b)}} 
$$
就可以得到概率判定分类。

对于 multi-class 情况，只计算一个概率不行。要计算所有类型的概率，这里就用到了 softmax function，将线性的 $wx+b$ 转化成概率形式.

### 与 logistic 函数的关系

- logistic 函数：

  本来在 logistic 函数中，WX + b 得到的 score z 就可以看作是某个分类对应的 score，本质上就是这个 score 越大，越属于哪个分类。

  在对应的神经网络中，二分类问题输出端只需要有一个输出，表明属于某一类的概率。

  多分类问题则需要有与种类相等的输出个数，对应每个种类的概率。

  两种情况下，输入都是只有一个。

  比如输入一个猫的图片，二分类问题可以是一个输出：是猫的概率有多大

  多分类问题就要有很多个输出：是猫的概率多大，是狗的概率多大，是鸭的概率多大 ...

- 对于多个分类的情形，需要计算每个分类的 score，这样每个类型都有一套权重 W 和 b。比如输入 $x$ 经过计算对应类型 $C_1$ 的 score 是 $z_1$; 类似的，计算得到其他类型的 score 是 $z_2, z_3$。 然后按照下图所示方法进行概率计算。

![softmax3](pic/softmax3.png)



- 为什么叫 softmax

  假设有 3 classes: class-1, class-2, class-3

  The scores are: [1, 7, 2]

  - hardmax 函数会基于上述 score 为各个 class 赋值概率 [0,  1,  0], Predict hardly !
  - softmax 函数会兼顾大值和小值，不是简单的取 max，最终可能赋值 [0.1,  0.7,  0.2], predict softly !

- 为什么是 softmax 这种情形？ 它的合理性在哪里？

  这个是可以推导出来的， Bishop, p209-210

### softmax 重要性质

- 若上图中 $y_i$ 都扩大十倍，即 [20, 10, 1]，则对应的 $e^{y_i}$ 不成比例的扩大，大者愈大。

- 类似的，如果 $y_i$ 都缩小十倍，即 [0.2, 0.1, 0.01]，则最后的概率趋于平均

  ```python
  import numpy as np
  scores = np.array([2.0, 1.0, 0.1])
  def softmax(x):
      """Compute softmax values for each sets of scores in x."""
      pro_temp = np.exp(x)  # change to np.exp(x*10) or np.exp(x*0.1)
      return 1.0 * pro_temp / pro_temp.sum(axis=0)
  print(softmax(scores))
  ## output: [0.65900114 0.24243297 0.09856589]
  ## output of *10: [9.99954597e-01 4.53978684e-05 5.60254205e-09]
  ## output of *0.1: [0.36605947 0.33122431 0.30271622]
  ```

### Loss function 与 One-hot encoding

相比于 2 类分类情况， multi-class 不能仅用 0, 1, 2, 3,  ... 代表类别，因为这样会默认 0 类和 1 类比较接近， 1 类和 2 类比较接近。一种方式叫做 one-hot encoding

对应 label 的位置为 1，其余位置为 0。这个 label 到 code 的转化过程称为 one-hot encoding.

![one_hot](/home/qpliu/learning/pic/one_hot1.png)

- panda 实现 one-hot coding

  ```python
  # Make dummy variables for rank
  one_hot_data = pd.concat([data, pd.get_dummies(data['rank'], prefix='rank')], axis=1)
  
  # Drop the previous rank column
  one_hot_data = one_hot_data.drop('rank', axis=1)
  
  # Print the first 10 rows of our data
  one_hot_data[:10]
  ```

  原本 data 为

  ![one_hot_coding0](pic/one_hot_coding0.png)

  one-hot coding 之后变为

  ![one_hot_coding1](pic/one_hot_coding1.png)


- loss function

![loss_function1](/home/qpliu/learning/pic/loss_function1.png)

最终的目的是 S 输出尽可能接近对应的 label。

下一步就是minimize loss function，从而找到最优的 $w, b$。到这里，机器学习问题就转化到了核心的优化问题。

### 流程图

![logistic_classifier](/home/qpliu/learning/pic/logistic_classifier1.png)



1. 先线性组合，得到 logit scores
2. logit scores 经过 **softmax 函数**转换成 pro
3. 用 **cross-entropy** 计算 pro 与 label 的差别

上述多类型分类方法也称为 **multinomial logistic classification**。 



# K-nearest neighbors (KNN)

## 基本思想

找到最相近的 K 个邻居，以它们大多数的标签 label 标记自己。 

增加 K 可以起到平滑分类区域的作用。

![knn1](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/knn1.png)

# 决策树

## 原理

本质上可以转化成 *if-then* 规则集合，特点是samples的分类互斥且完备，即每个给定的sample都被且仅被一条feature路径覆盖。

下图中绿色代表features，橙色对应标记类别
   <img src="/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/decision_tree.png" alt="decisiion tree" width="300" height="300" align="center">

## 构造

构造决策树时一个关键问题是feature节点如何排列？

简单的说，优先选择最有利于降低分类混乱（不纯）程度的特征。不同的刻画分类的混乱程度的方法，衍生出不同的决策树算法。

## ID3

 ID3 (Iterative Dichotomiser 3) 由Ross Quinlan 于1986年提出。其特点是以信息增益的标准选择树的节点，即特征/属性（feature）。

  选择特征时，原则上选择最有利于减少分类不纯度（impurity）的特征，即划分之后，不纯度减小最多。ID3中用信息熵刻画不纯度，熵越大，越不纯，并用信息增益的大小区分特征的优先级。

  假设集合$S$中包含若干标签分类：$S=s_1 \cup s_2 \cup \cdots$. 

  令 $p_i=\frac{|s_i|}{|S|}$，即 $p_i$ 为属于分类集合$s_i$的元素占整个集合 $S$ 的比例。

  **集合$S$不纯度的定义**： 
  $$H(S)=-\sum_i p_i \log p_i$$

  上述定义实际上就是信息熵的定义。所有标签属于统一类时，不纯度最低，$H(S)=0$；所有标签均分到各类，不纯度最高。

  在选择特征时，首先测试每一种特征划分后不纯度的大小，将不纯度最小的划分对应的特征作为当前节点的特征，进行划分。然后对划分后的每一个分支，如果标签类型不唯一，则继续按上述方法选特征，划分。

  **信息增益的定义**： *Information Gain = entropy (parent) - average entropy (children)*

  有人将后一部分称为*条件信息熵*，记为$H(Y|X)$.

  **计算信息增益的实例**：

<img src="/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/information_gain.png" alt="information gain" width=400 hight=400 align = "center">

**ID3的缺陷：**ID3无法处理数值型数据，尽管可以通过量化的方法将数值型转化为标称型，但是如果存在太多特征划分，ID3会出现overfitting等问题。

**与K-近邻算法相比的优点**: 基于训练数据集构造的tree可以存储起来，构造tree的过程可能比较耗时，但是一旦构造完成，使用的时候速度很快。

## C4.5

  选择特征时，以信息增益比为判断指标。

## CART

 以Gini index： $H=\sum p_i(1-p_i)$ 作为计算熵的公式。







# Support Vector Machine (SVM)

The idea is to find the optimal hyperplane which has the largest distance from the support vectors. This distance is usually called margin.
<img src="pic/svm1.png" width=300 align="center">

In general, in a $n$-dimensional space, the hyperplane is $n-1$-dimensional, like a line in 2D plane and a plane in a 3D space.

SVM 本质上是寻求 margin 最大。

## relation with Logistic classifier

The sigmoid function used in Logistic classifier is 
$$
h_\theta(x) = g(\theta^T x) = \frac{1}{1+e^{-\theta^T x}}
$$

<img src="pic/svm2.png" width=300 align="center">


Usually, we say a sample belongs to 
- Class 1 if $h_\theta (x) > 0.5$
- Class 0 if $h_\theta (x) < 0.5$

The value of $h_\theta (x)$ is in turn decided by $\theta^T x$: 
- $h_\theta (x) > 0.5$ if $\theta^T x > 0$, and in particular,  $h_\theta (x) \rightarrow 1 $ if $\theta^T x >> 0$
- $h_\theta (x) < 0.5$ if $\theta^T x < 0$, and in particular, $h_\theta (x) \rightarrow 0 $ if $\theta^T x << 0$

So, the training process of a logistic classifier can be viewed as finding $\theta$ such that for class 1 $\theta^T x >> 0$ (to make the output as close as possible to the label 1) and for class 0 $\theta^T x << 0$ (to make the output as close as possible to the label 0).

**This training considers all samples in the training set.**

Similary to logistic classifier, SVM also wants to find a $\theta$ such that the line $\theta^T x = 0$ can separate the different classies. 

But SVM has a different optimial object. As mentioned in the first figure, SVM is trying to find the hyperplane which is as far away as possible from the support vectors. 

**It does not care about other samples, only consider the support vectors.**
**Another difference is that the class labels are -1 and 1 instead of 0 and 1. **
**The former $\theta ^T x​$ is replaced by $w^T x + b​$. In fact, these are just different notations.  **

## 待优化目标函数

首先是给出一个sample与分割面的距离，即空间中点到面的距离
- 点到平面的距离公式推导方式 1： 借助投影
<img src="pic/svm_distance1.png" width=250>

求 $P = (x_0, y_0, z_0)$ 到平面 $E: ax + by + cz +d =0$ 的距离 $r$。实际上就是向量 $QP = (x_0-x, y_0-y, z_0-z)$ 在法线方向 $n = (a,b,c)$ 上的投影，取绝对值
$$
r = |QP \frac{n}{\|n\|} | = \frac{|a(x-x_0) + b(y-y_0) + c(z-z_0)|}{\|n\|} = \frac{|ax_0 + by_o + cz_0 +d|}{\sqrt{a^2+b^2+c^2}}
$$
实际上，就是把平面外的那一点带入平面公式，然后对法向量归一化。

- 点到平面的距离公式推导方式 2： 不用投影
假设平面方程 $w^Tx + b =0$， 并且假设点 $P=(x_0, y_0)$ 到平面的距离为 $r$, 则 $P$ 减去 $r$ 乘上平面的单位法向量（假设法向量在$P$这一边，如果在另一边，则要加负号），得到的点满足平面方程，即
$$
w^T (P-\frac{w}{\|w\|}r) + b = 0
$$
解得
$$
r = \frac{w^TP+b}{\|w\|}
$$
考虑到法向量与点是否在平面的同一边，比较精确的距离应该表示为
$$
r = \frac{|w^TP+b|}{\|w\|}
$$
结果与前一种方法相同。

在SVM中，上述绝对值符号可以去掉，而借用class标签。假设一个sample $i$ 包含特征向量$x^i$ 和class标签标量 $y^i$，可以将标签的取值设定为：法向量正方向一侧的class标签为 1, 负方向一侧的class 标签为 -1. 因此，一个sample $i$ 距离平面的距离为
$$
r^i = y^i \frac{w^Tx^i+b}{\|w\|}
$$
如果法向量一开始就取为归一化的值，则
$$
r^i = y^i (w^Tx^i+b)
$$

由于SVM是使距离平面最近的点距离最远，所以优化问题可以表示为
$$ \begin{align}
 &\max_{w,b} ~~r \\
\textrm{s.t.} &~~ y^i (w^Tx^i+b) \ge r, i=1,2,\cdots,m  \\
&~~\|w\| = 1
\end{align}
$$
SVM也被称为最优间隔分类器。只要我们训练出了最优的 $w, b$ ，就可以进行分类了。

接下来的问题是如何求解最优的 $w,b$.

## 用线性方法拟合非线性函数

将问题扩维，非线性转化为在更高维度上的线性函数。

任何非线性函数都可以通过这种扩维的方式，转化成线性函数。

![svm3](pic/svm3.png)

# unsupervised learning

有两大类常见应用情景：

- clustering：为众多 data 划分不同的组，然后研究他们的共性。也可以在划分之后，发现一些 outlier，比如对客户交易的划分，可能发现那些异常帐号。
- recommender

## K-mean clustering

![k_means1](pic/k_means1.png)

最初的 cluster center 从 sample 中随机选取，而不是完全随机撒点，是为了避免某些 cluster center 周围没有一个跟它最接近的 sample，导致它成了一个孤立的，额外的一类，也就是凭空冒出来一个空个 cluster，不含有任何的 sample.

## Hierarchical Agglomerative Clustering (HAC)

![HAC1](pic/HAC1.png)

先找最相似的 sample，平均一下得到新的 sample, 然后不断融合相近的 sample，直到最终一个 sample

# Dimension reduction

## Feature selection

![dimension_reduction1](pic/dimension_reduction1.png)

这种 dimension reduction 的方法使用范围太小。很难找到 samples 仅靠部分 feature 就可以判别类型的。

## Principle comment analysis (PCA)

### 基本思想

找到变换矩阵，使得降维之后的 sample 有最大的 variance，即分的很开，而不是堆在一起。

![pca1](pic/pca1.png)

可以一维一维的寻找变换矩阵

![pca2](pic/pca2.png)

变换矩阵各维之间是正交的，另外目标都是让 variance 尽量大。

### Lagrangian multiplier 方法求解

1. 先求解第一个变换矩阵，转化成了一个受限的优化问题  

   ![pca3](pic/pca3.png)

   2. 可以用 Lagrangian multiplier 方法求解上述优化问题

   ![pca4](pic/pca4.png)



   3. 类似的，求解第二个 变换向量

   ![pca5](pic/pca5.png)

   4. 经过 PCA 之后，得到新的 sample 之间是没有耦合的，协方差矩阵为对角矩阵。 

   ![pca6](pic/pca6.png)

### 

# Transfer learning

现有的数据集与希望学习的领域不完全相同，比如有很多猫狗的数据，但是要分类大象和羊。

![transfer1](pic/transfer1.png)

有没有可能基于某种数据学习，帮助其他类数据的分析？



# 集成学习算法(ensemble method)

 能否使用多个弱分类器构造一个强分类器？ 这里的“弱”是指分类器的分类效果仅仅比随机猜测略好。也就是说，用一堆低质量的元件能否构建出一个高质量的整体。
由上述问题出发，出现了一类集成学习算法，即将多种基本算法组合起来，得到一种集成的学习算法。集成的方法有很多

- 不同算法的集成
- 同一种算法在不同参数设置下的集成
- 从样本中提取出不同的新数据集，送进同种或不同种算法，训练得到不同的分类器

## bagging (bootstrap aggregating)

适用范围：当 model 很复杂，担心它 overfitting 的时候，才应该做 bagging. 比如 decision tree, 只要层数足够多，可以在 training data 上达到 100 % 的正确率，但是在 testing data 上可能就会很差。

bagging 并不一定会让 model 在 training data 上效果更好，毕竟它要解决的是 overfitting 的问题。

- 从原数据集中有放回的随机抽取若干次，得到一个新的数据集。
- 重复这一过程，直到得到希望数目 $S$ 的新数据集为止。
- 将这 $S$ 个数据集分别训练 $S$ 个**类型相同的分类器**
- 预测时采用多数原则确定分类结果

### 例如 random forest: bagging of decision tree

对于 decision tree 来说，单纯的 resampling data 效果并不好。需要随机的限制 feature/ question，才能得到不同的结果，然后再集合起来。

### testing 

在 bagging 中不需要特意从全部数据中切割一部分当作 validating data 或者 testing data. 可以简单的把 训练中没用到的 data 当作 testing  data，测试每个或者若干个组合，然后取平均。因为这些 data 是那些模型没有看过的，用来做测试是合适的。这就称为 out-of-bag (OOB) validation.

## boosting

bagging 是面对一个很复杂的、很强的 model，而与 bagging 相反， boosting 是用在很弱的 model 上的。

- 与bagging相同，也采用多个同类型的基本分类器。
- bagging中不同数据集并行的送给分类器。不同的是，boosting采用串联的形式，不对数据集再抽样，直接送给第一个弱分类器，后边分类器是在前边分类器的基础上进行训练，并**重点关注被已有分类器错分的数据**。
- bagging中各分类器权重是相同的，boosting中分类器的权重与其正确率相关。

### AdaBoost (Adaptive Boosting)： 最流行的boosting方法

它实际上是用了一个 re-weight 的方法，让前边 classifier 分错的数据 weight 变大，正确的数据 weight 变小。

![boosting1](pic/boosting1.png)



![boosting2](pic/boosting2.png)

如何确定 weight 改变的比例呢？

![boosting3](pic/boosting3.png)



最终的算法如下：

![boostin4](pic/boosting4.png)

训练完之后，得到一堆 f(x)，再按权重整合起来

![boosting5](pic/boosting5.png)

A toy example can be found in Hungyi Lee 's course.

# 安装 keras

## ubuntu 系统的准备工作

- 更新系统软件，安装一些开发必备软件，如pip, g++, vim等。

```shell
>>> sudo apt update
>>> sudo apt upgrade
>>> sudo apt install -y python-dev python-pip python-nose gcc g++ git gfortran vim
```
    这里的 `-y`是指程序安装过程中如果有问是否同意安装，一律选yes.

- 运算加速库 

```shell
>>> sudo apt install -y libopenblas-dev liblapack-dev libatlas-base-dev
```

## 安装CUDA，以便GPU加速

如果不需要GPU加速，仅用CPU，则不需要安装CUDA。

由于GPU的加速计算是通过CUDA平台实现的，因此需要查看一下，我们的GPU是否支持CUDA平台。

### 如何查看本机是否有CUDA-capable GPU

首先确定本机NVIDIA显卡类型，然后去如下网址查找对应类型显卡是否支持CUDA

https://developer.nvidia.com/cuda-gpus

一般来说，只要不是太旧的独立显卡，基本上都支持CUDA。

CUDA® is a parallel computing platform and programming model invented by NVIDIA. It enables dramatic increases in computing performance by harnessing the power of the graphics processing unit (GPU).

CUDA-capable GPUs have hundreds of cores that can collectively run thousands of computing threads. These cores have shared resources including a register file and a shared memory. The on-chip shared memory allows parallel tasks running on these cores to share data without sending it over the system memory bus.

### 安装注意版本兼容性

这里要注意版本信息，目前 tensorflow还不支持 CUDA 9.1，所以要指定安装9.0,而不是安装最新的9.1。
1. 去[官网](https://developer.nvidia.com/cuda-toolkit-archive)下载 CUDA 9.0 安装包

2. 然后按照下述步骤安装：
```shell
>>> sudo dpkg -i cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb`
>>> sudo apt-key add /var/cuda-repo-<version>/7fa2af80.pub`
>>> sudo apt-get update`
>>> sudo apt-get install cuda-9-0`
```
    注意：最后安装时一定要注意指定 `cuda-9-0`！否则会安装最新的cuda。
3. 设置环境变量：
    在`～/.bashrc`文件末尾添加以下命令
  ```shell
export CUDA_HOME=/usr/local/cuda-9.0
export PATH=/usr/local/cuda-9.0/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
  ```
4. 测试： `nvcc -V`，若出现CUDA 编译器信息，就说明安装成功。

### 安装加速库 cuDNN 

安装时只需要将下载的 cuDNN 包里面的文件拷贝到CUDA文件夹下即可，具体步骤如下：
1. 去[官网](https://developer.nvidia.com/rdp/cudnn-download)下载与CUDA 9.0匹配的安装包

2. 解压之后拷贝到CUDA路径下：
```shell
>>> sudo cp include/cudnn.h /usr/local/cuda/include/
>>> sudo cp lib64/* /usr/local/cuda/lib64/
>>> cd /usr/local/cuda/lib64
>>> sudo ln -sf libcudnn.so.7.1.13 libcudnn.so.7
>>> sudo ln -sf libcudnn.so.7 libcudnn.so
>>> sudo ldconfig -v
```

## 在python虚拟环境中安装tensorflow和keras

1. 首先安装创建虚拟环境的工具virtualenv： `pip install virtualenv`
2. 建立一个文件夹专门存放各种虚拟环境，起名 pyenvs：`mkdir pyenvs`
3. 在pyenvs文件夹中创建虚拟环境py-keras: `virtual py-keras`, 如果要用python3，则`virtualenv -p python3 envname`
4. 进入py-keras虚拟环境，并安装tensorflow和keras: 
   ```shell
   >>> source py-keras/bin/active
   >>> pip install numpy scipy matplotlib scikit-learn scikit-image
   >>> pip install tensorflow-gpu
   >>> pip install keras
   ```
5. 至此安装完成，可以import tensorflow 和 keras，进行工作了。
   ```python
     >>> import tensorflow
     >>> import keras
   ```
```

```