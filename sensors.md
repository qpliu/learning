
## 轮速计  optical encoder

是一种内部传感器，对自身轮速检测十分精确，几乎可以认为100%精确。但是由于轮子打滑，地面不平导致轮子离地等因素，这个对自身轮速的记录并不能精确对应车在环境中实际运动了多少距离。

### 基本形式

![optical_encoder1](pic/optical_encoder1.png)

转轴 shaft 旋转一周对应多少个明暗相间（或者高低脉冲信号）.

encoder 的精度表示为转轴转一周会有多少个明暗相间，cycles per revolution (CPR).

### 双通道形式

![optical_encoder2](pic/optical_encoder2.png)

额外增加一个光源和光感知器，构成 A，B两个信号通道，而且使信号之间构成四种组合形式。另外还有一个slot槽，标记一个 revolution 的开始。

- 优点1： 可以获得旋转方向。以 slot 槽为 revolution 的起点，看 A 和 B 谁先得到脉冲。
- 优点2： 精确度翻倍。 对比 A，B 两组脉冲，可以更加精确的知道处在脉冲的哪个部位。

## IMU （ inertial measurement unit ）

IMU 内部一般带有加速计（accelerometer）和陀螺仪（gyroscope），可以分别测量**直线加速度**和**角速度**。

### 陀螺仪 gyroscope

#### 早期机械陀螺仪

机械 gyroscope 是基于角动量守恒的原理设计出来的。核心装置是一个位于轴心且可旋转的转子。

电子马达的发展可以保证陀螺仪可以持续高速旋转，产生了可实用的陀螺仪。

陀螺仪一旦开始旋转，由于转子的角动量，陀螺仪有抗拒方向改变的趋向。

无论外壳如何旋转，内转子转轴始终不变，通过相对角度变化就可以测量（与飞机、汽车相对固定的）外壳的转角了。进而可以计算角速度。

![gyroscope](pic/gyroscope.gif)

#### MEMS 陀螺仪 

机械陀螺仪对加工精度要求很高，在实际使用中误差较大。

后来发展出了激光陀螺仪，光纤陀螺仪，以及微机电 (Micro-electro-mechanical Systems，缩写为 MEMS) 陀螺仪，其中 MEMS 应用最为广泛，它是微电子系统和机械系统的融合。
- 智能手机里面用到的陀螺仪就是 MEMS。
- 高档汽车里面可能包含几十个 MEMS，测量不同部位的姿态。
- 电子游戏中有关姿态的操作也是用 MEMS 测量.

这些装置虽然还叫“陀螺仪”，但是其工作原理与传统的机械陀螺仪完全不同。

MEMS 是利用物理学的科里奥利力，在内部产生微小的电容变化，然后测量电容，计算出角速度

### 加速计 accelerometer

用来测量外界对其施加的力的总和，然后得到相应的加速度

每个加速计只能测量一个直线方向的加速度，需要用3个加速计正交放置，才能得到全方向的加速度。

由于加速计测量所有受到的力，其中包括重力。一个平稳放置在地面的加速计，它也是受到重力，其读数为1g，自由落体时，读数为0.

### gyroscope 错称为 IMU

理论上来说，一个完整的 IMU 应该可以估计三个位置和三个角度，但是有些 gyroscope 也被错称为 IMU，很显然它只能提供角度方面的信息。就像我们仿真平台上的IMU一样。

另外，功能比较强大的IMU除了提供6个pose信息外，还有速度和加速度信息。

### IMU block diagram
![IMU_diagram](pic/IMU_diagram.png)

## GPS

从卫星信号中可以获得如下信息：
1. 卫星在空中的位置：$(x_i,y_i,z_i) $
2. 卫星发送信号的时间： $ T_i​$

假设车辆位置表示为 $ (x,y,z)$，车辆接收到信号的时间为 $T$，光速为 $c$，则满足方程：
$$
 (x-x_i)^2 + (y-y_i)^2 + (z-z_i)^2 = [c(T-T_i)]^2
$$
三个未知数，只需要三个卫星信号即可求解。

但是，这里有个问题。如果地面车辆的时间与卫星时间是精确同步的，上式是合理的，但是很显然，车辆时间并没有用铯原子中，与卫星时间之间可能有偏差，假设偏差为 $ \Delta$，则新的方程为：
$$
(x-x_i)^2 + (y-y_i)^2 + (z-z_i)^2 = [c(T+\Delta-T_i)]^2
$$

此时有四个未知数，需要四个卫星信号才行。这就是为什么很多书上说由4颗卫星定位。

另外，如果接收到多于4个卫星的信号，则未知数个数少于方程个数，此时考虑到微小的误差，可能无法找到满足所有方程的解，这个超定方程的问题可以用最小二乘方法解决。

## 电磁波分类  electromagnetic waves

电磁波是一类具有电场和磁场的能量波

与机械波不同，电磁波可以在真空中传播

声波速度大概为 0.3m/ms, 电磁波速度为 0.3m/ns，相差 1百万倍

根据频率的不同，电磁波可以分成很多类，具有不同的用途

波长与频率成反比，频率越低，波长越长，传播范围越广，穿透力越强

![electromagnetic_spectrum](pic/electromagnetic_spectrum.jpg)

### Radio Waves 无线电波

Radio waves have the longest wavelengths of all the electromagnetic waves. They range from around a foot long to several miles long. Radio waves are often used to transmit data and have been used for all sorts of applications including radio, satellites, radar, and computer networks. 

### Microwaves 微波

Microwaves are shorter than radio waves with wavelengths measured in centimeters. We use microwaves to cook food, transmit information, and in radar that helps to predict the weather. Microwaves are useful in communication because they can penetrate clouds, smoke, and light rain. The universe is filled with cosmic microwave background radiation that scientists believe are clues to the origin of the universe they call the Big Bang. 

### Infrared  红外线

Between microwaves and visible light are infrared waves. Infrared waves are sometimes classified as "near" infrared and "far" infrared. Near infrared waves are the waves that are closer to visible light in wavelength. These are the infrared waves that are used in your TV remote to change channels. Far infrared waves are further away from visible light in wavelength. Far infrared waves are thermal and give off heat. Anything that gives off heat radiates infrared waves. This includes the human body! 

### Visible light 

The visible light spectrum covers the wavelengths that can be seen by the human eye. This is the range of wavelengths from 390 to 700 nm which corresponds to the frequencies 430-790 THz.

### Ultraviolet 

Ultraviolet waves have the next shortest wavelength after visible light. It is ultraviolet rays from the Sun that cause sunburns. We are protected from the Sun's ultraviolet rays by the ozone layer. Some insects, such as bumblebees, can see ultraviolet light. Ultraviolet light is used by powerful telescopes like the Hubble Space Telescope to see far away stars. 

### X-rays 

X-rays have even shorter wavelengths than ultraviolet rays. At this point in the electromagnetic spectrum, scientists begin to think of these rays more as particles than waves. X-rays were discovered by German scientist Wilhelm Roentgen. They can penetrate soft tissue like skin and muscle and are used to take X-ray pictures of bones in medicine. 

### Gamma rays 

As the wavelengths of electromagnetic waves get shorter, their energy increases. Gamma rays are the shortest waves in the spectrum and, as a result, have the most energy. Gamma rays are sometimes used in treating cancer and in taking detailed images for diagnostic medicine. Gamma rays are produced in high energy nuclear explosions and supernovas. 

## ultrasonic sensor

###  ultrasonic sensor 与 sonar sensor 的关系

- ultrasonic sensor 是利用超声波进行检测
- sonar = sound navigation and ranging，并不局限于用超声波，可以是人类感知范围内的声波段，也可以是次声波
- 一般来说，如果没有特殊说明，sonar sensor 与 ultrasonic sensor 是一回事，默认用超声波。

![sound_spectrum](pic/sound_spectrum.jpeg)

### ultrasonic sensor 特点

利用机械波（声波）传播与反射原理了。

频率： 40 -- 180  kHz

一般检测范围：12 cm -- 5 m

精度： 2 cm

缺陷：声波的传播很难限制在特定方向，一般会形成一个扇形区域，角度在 20～40 度，所以我们只能知道在这个较为宽泛的区域内的某个距离上有没有物体，而无法精确知道其方位。sensor 的读数也是画在一个圆盘上，一段圆弧一段圆弧的探测

![ultrasonic](pic/ultrasonic.png)

### 经典的 pioneer 3-DX 机器人 sonar sensor 
![p3-dx](pic/p3-dx.png)

![p3-dx_sonar](pic/p3-dx_sonar.png)

The angle between two consecutive sensor directions is 20 degrees except for the four side sensors (so0, so7, so8 and so15) for which the angle is 40 degrees.

##  激光测距仪 laser rangefinder  
也称为 LIDAR， light detection and ranging

利用激光反射进行检测

![lidar2](pic/lidar2.png)


典型的产品
- 180 degree laser range sensor from Sick Inc., Germany

![sick](pic/sick.png)

2005年Stanley 就是用的这一款

![stanley](pic/stanley.png)

- Velodyne HDL-64E 

![velodyne](pic/velodyne.png)

这一款 3D LIDAR sensor 是2007年几乎所有DARPA 比赛车地图构建和障碍物将测的标配

指标：
- 1.3 million data points per second
- 水平：360度，0.09精度
- 垂直：26.8度，0.4精度
- 距离：反射率较好时 120m， 距离精度好于 2cm

### 分类

- 机械激光雷达：带有控制激光发射角度的旋转部件，体积较大

  ![velodyne_series](pic/velodyne_series.png)

  其中 VLP-16 没有可视的旋转机构，在内部还是旋转的。

- 固态激光雷达：依靠电子部件控制激光发射角度，体积较小

## Doppler motion detector

利用 Doppler effect 检测移动物体的速度，最常见的就是 Radar

波频率与相对速度有关，以此估计速度

可以用声波，也可以用电磁波。 声波主要用在工业过程控制，安防，鱼群检测;电磁波用在振动监测，雷达系统，目标跟踪
。

对于自动驾驶汽车环境，微波和激光都可以用，各有优缺点：
- 两者具有相同的检测距离，但是激光对可见环境要求较高，在雨天、大雾中效果较差
- 微波的缺点是频率低，约 2Hz


## camera, Lidar, and Radar

![sensor_compare](pic/sensor_compare.png)

- Radar --- Radio Detection and Ranging, 采用的是无线电波，波长最长，穿透力强，受恶劣天气影响小。
- Lidar --- Light Detection and Ranging, 目前多采用红外激光，也有采用更长波长的激光，穿透力更好些。

天气恶劣的条件下，Radar 的物体检测效果比较好，受下雨等天气影响较小。但是 radar 在区分物体方面比较差，需要摄像头配合。



## 毫米波雷达

- **频率**：以 24GHz (用于短距) 和 77GHz (用于中长距) 为主，79GHz 尚处于研发阶段。

- **检测距离**： 短距毫米波雷达为 50~70 m，中长距毫米波雷达为 150~250 m
- **成本**：77 GHz 比 24 GHz 更贵一些，但是随着 77GHz 成本降低，量产方案中 77GHz 成为前向雷达的主流，24 GHz 主要用于侧向检测，如 BSD (blind spot detection) 盲区检测， LCA (lane change assistant) 换道辅助。
- 