交通仿真分类

以精细度，或者说最小不可分单元来分类：

- macroscopic：交通流不再可分，不涉及单个车辆
- microscopic：单个车辆不再可分，不涉及车的零部件或者驾驶员活动
- sub-microscopic：深入到车的零部件层面

另外，还可以将上述多种类型混合起来，即又粗粒度，又有细粒度。例如

- mesoscopic：介于 macro 与 micro 之间的精细度。



从时间、空间的连续性方面分类：

- 连续时间，连续空间
- 连续时间，离散空间
- 离散时间，连续空间：SUMO
- 离散时间，离散空间：cellular automata





# SUMO

## 引言

Simulation of Urban MObility

正式推出 SUMO 的文章是这一篇

https://www.researchgate.net/publication/224793504_SUMO_Simulation_of_Urban_MObility_An_open-source_traffic_simulation， 4th Middle East Symposium on Simulation and Modelling， 2002

主要由 German Aerospace Center  下属的 Institute of Transportation Systems 的研究人员开发，最早发布于 2001年。

目前官方希望的引用文献为

> Pablo Alvarez Lopez, Michael Behrisch, Laura Bieker-Walz, Jakob Erdmann, Yun-Pang Flötteröd, Robert Hilbrich, Leonhard Lücken, Johannes Rummel, Peter Wagner, and Evamarie Wießner. Microscopic Traffic Simulation using SUMO. IEEE Intelligent Transportation Systems Conference (ITSC), 2018.

（以下内容主要来自 SUMO 官网 tutorial 以及 [youtube 视频教程](https://www.youtube.com/watch?v=9MyIABer_NY), published by Rodrigue Tchamna)



## 安装 SUMO

SUMO 已经在 Ubuntu 的官方 repo 中了，可以直接安装

```bash
sudo apt-get install sumo sumo-tools sumo-doc
```

如果要安装最新版本，可以额外添加 SUMO 的 repo

```bash
sudo add-apt-repository ppa:sumo/stable
sudo apt-get update
```

然后再按照

```bash
sudo apt-get install sumo sumo-tools sumo-doc
```



## 运行 SUMO

SUMO 并不是单一的可执行文件，它里面包含了负责各种功能的 package/application，如下

![SUMO_packages](pic/SUMO_packages.png)

一般是从命令行运行 SUMO 的各个 application。

### 运行参数设置

每个 application 都带有很多参数设置，为了方便起见，可以将参数统一放到 xml 配置文件中，例如

```xml
<configuration>
    <net-file value="test.net.xml"/>    # net-file 可简写为 n
    <route-files value="test.rou.xml"/>   # route-files 可简写为 r
    <additional-files value="test.add.xml"/>  # additional-files 可简写为 a
</configuration>
```

这样在运行时，可以调用这个 config 文件，例如

```bash
sumo test.sumocfg
```

如果一个参数既出现在了 config 文件中，又在 command line 中，则采用 command line 的设置。

也可以通过一下方式获得参数列表：

-  `--save-tamplate <file> ` ：得到参数列表及默认值
- `--save-configuration <file>`：得到参数列表及当前值

### 创建路网

三种方式

1. 手动创建
2. `netgenerate` 命令
3. 从外部导入，例如从 OSM (open street map)， VISSIM， VISUM 等导入。

##### 1 手动创建路网

分为 5 步：

1. node file (.nod.xml)
2. edge file (.edg.xml)
3. edge type file (.type.xml)
4. 基于上述三个文件创建网络 (.net.xml)
5. route file (.rou.xml)

###### node file

例如创建如下 node file，名为 my_nod.nod.xml：

```xml
<nodes>
	<node id="n1" x="-500" y="0" type="priority"/>
    <node id="n2" x="-250" y="0" type="traffic_light"/>
    <node id="n3" x="-150" y="200" type="traffic_light"/>
	<node id="n4" x="0" y="0"/>
    <node id="n5" x="150" y="200"/>
</nodes>
```

其中 id 可以是任意标记。

对应的 node 位置如下图所示（忽略图中连边，因为此时还没有定义连边）：

![SUMO_manual_network1](pic/SUMO_manual_network1.png)

###### edge file

例如，创建对应上述路网的 edge file，名为 my_edge.edg.xml，内容如下：

```xml
<edges>
	<edge from="n1" to="n2" id="1to2" type="3L45"/>  
    <edge from="n2" to="n3" id="2to3" type="2L15"/>
    <edge from="n3" to="n4" id="3to4" type="3L30"/>
    <edge from="n4" to="n5" id="out" type="3L30"/>
</edges>
```

其中 id 和 type 可以随便命名，这里为了方便记忆，用 3L45，表示有 3 条 lane，速度上限是 45 m/s，随后会在 type file 中对相应的 type 进行定义。

###### type file

对前述  edge 文件中的 type 进行定义，文件名为 my_type.type.xml:

```xml
<types>
	<type id="3L45" priority="3" numLanes="3" speed="45"/>
    <type id="2L15" priority="3" numLanes="2" speed="15"/>
    <type id="3L30" priority="2" numLanes="3" speed="30"/>
</types>
```

###### convert to network file

基于以上创建的三个文件，可以通过命令/程序 netconvert 创建 net 文件，命令如下：

```bash
netconvert --node-files my_nodes.nod.xml --edge-files my_edge.edg.xml -t my_type.type.xml -o my_net.net.xml
```

以上命令各个参数的含义非常直观的。

上述命令生成 my_net.net.xml 文件。以后如果改动了 node, edge, type files，都要重新生成一次 net file.

###### route file

沿着之前的例子，定义 route file，名为 my_route_rou.xml:

```xml
<routes>
    <route id="route0" edges="1to2 2to3"/>  # edges 中的基本格式为"edge1 edge2 edge3 ..."
    <route id="route1" edges="2to3 3to4"/>
    <route id="route2" edges="3to4 out"/>
    
    <vType accel="1.0" decel="5.0" id="Car" length="2.0" maxSpeed="100.0" sigma="0.0"/>
    <vType accel="1.0" decel="5.0" id="Bus" length="12.0" maxSpeed="1.0" sigma="0.0"/>
    
    <vehicle id="veh0" depart="10" route="route0" type="Bus"/>
    <vehicle id="veh1" depart="10" route="route1" type="Car"/>
    <vehicle id="veh2" depart="30" route="route2" type="Car"/>    
</routes>
```

###### 测试

运行程序时需要送入一些参数，这里用 config 文件的方式送入参数，定义 my_config_file.sumocfg:

```xml
<configuration>
	<input>
    	<net-file value="my_net.net.xml"/>
        <route-files value="my_route.rou.xml"/>
    </input>
    <time>
    	<begin value="0"/>
        <end value="2000"/>
    </time>
</configuration>
```

然后运行

```bash
sumo-gui my_config_file.sumocfg
```

出现如下界面：

![SUMO_manual2](pic/SUMO_manual2.png)

可以检查一下生成的路网是否与期望的相同，以及每条路上的 lane 数目。

然后将工具栏中的 Delay 设置为 100 ms，否则仿真瞬间结束。点击绿色三角符号，运行仿真。在时间走到 10 时，bus 和 car 出现在相应的到路上。时间到达 1999 时仿真结束。

在工具栏中还可以设置 standard, fast standard, real world. 可以切换一下，看看显示效果。



上述手动设置路网的方式只适用于比较简单的情况，对于大型路网，我们需要其他的方法。

##### 2 导入 OSM 网络

###### 下载 open street map

进入 https://www.openstreetmap.org， 通过搜索城市、街道找到目标道路网，然后 export 即可。

这里我选用了上海市东川路附近的路网，如下图

![SUMO_sjtu](pic/SUMO_sjtu.png)

###### 转化成 SUMO 路网文件 

```bash
netconvert --osm-files map.osm -o sjtu.net.xml
```

到这一步为止，我们就创建了 net.xml 文件，这里不是由 node, edge, type 整合而成的，而是直接从 OSM 转化来的。只要再有一个 rou.xml 文件就可以进行仿真了。

###### route file

对于这种大型的路网，手动创建 route 文件也很麻烦，这里我们用 sumo 自带的 randomTrips.py 程序创建随机的 route 文件

```bash
python <path_to_randomTrips.py> -n sjtu.net.xml -r sjtu.rou.xml -e 50 -l  # -e 表示 end time
```

其中 randomTrips.py 文件放在了 SUMO_HOME 目录下，即 /usr/share/sumo 目录下。

######  config file

config 文件名为 sjtu.sumocfg，内容如下：

```xml
<configuration>
	<input>
    	<net-file value="sjtu.net.xml"/>
        <route-files value="sjtu.rou.xml"/>
    </input>
    <time>
    	<begin value="0"/>
        <end value="2000"/>
    </time>
</configuration>
```

###### 运行仿真

通过命令

```bash
sumo-gui sjtu.sumocfg
```

启动仿真，截图如下：

![SUMO_sjtu1](pic/SUMO_sjtu1.png)



局部放大：

![SUMO_sjtu2](pic/SUMO_sjtu2.png)



###### 用 osmWebWizard.py 实现类似的效果

osmWebWizard.py 程序整合了上述较为独立的步骤（选取 osm 地图，转化成.net.xml，设置 randomTrip route，运行仿真），操作起来更加方便一些。

用 osmWebWizard.py 运行仿真也是 SUMO tutorial 中第一个项目。

注意：必须以 python 2 运行，python 3 会在 generate scenario 的时候报错。

```bash
python osmWebWizard.py
```

如果提示找不到该文件，就直接去 SUMO_HOME 的目录下面 `/usr/share/sumo/tools` 运行该文件。

没有问题的话，应该会在浏览器中打开如下页面。这里初始地图位置是 Berlin。

![sumo_osm1](pic/sumo_osm1.png)

右侧有四个 tab，由上到下以此为:

- 位置：这里可以选择其他城市或者其他经纬度坐标，数字中，纬度在前，经度在后。

- 车辆类型：里面几乎涵盖了常见的交通工具，以及行人。可以设置交通流的密集程度。
- 其他选项：例如行驶在路的左侧还是右侧
- copyright 信息

首先是选定要仿真的地图环境。可以缩放、移动视图，通过右侧的 Select Area 可以选定一个区域。最好不要选择太大范围，否则仿真很占资源，甚至导致死机。

然后选择交通流中的车辆类型和密集程度，through traffic factor 对应了车辆穿过率，count 的单位是 per hour-lane-kilometer。randomTrip 程序会产生相应的 random route。

以上就设定好了地图和 route，点击右上方的 Generate Scenario， 就可以进入仿真界面了。

### Traffic light 设置

#### 基本设置

traffic light 可以通过 additional file 的形式加入到仿真中。考虑如下的交通路口场景

![SUMO_tl1](pic/SUMO_tl1.png)

可以用如下的设置：

```xml
<additional>
    <tlLogic id="0" type="static" programID="0" offset="0">  
        <phase duration="31" state="GrGr"/>
        <phase duration="6" state="yryr"/>
        <phase duration="31" state="rGrG"/>
        <phase duration="6" state="ryry"/>
    </tlLogic>
</additional>
```

其中，

- id 与 type 为 traffic_light 的 node id 对应
- type 分为三种
  - static: fixed phase durations,
  - actuated: phase prolongation based on time gaps between vehicles
  - delay_based: on accumulated time loss of queued vehicles 
- programID 为该红绿灯转换机制的ID，一个 traffic light 可以有多个 program，以最后的 program 为准。若要修改某个 net.xml 中的转换机制，可以通过添加 add.xml 文件的方式用新的 program 覆盖之前的 program。



phase 中各条 lane 的 index 是从12 点钟方向 edge 的右转 lane 开始，顺时针方向，依次经过中间的 lane，左转 lane，然后到下一个 edge, 依次编号。因此，图中场景就对应了 state: GrGr. 

关于 颜色字母含义的说明：

![SUMO_tl2](pic/SUMO_tl2.png)

r, y, G 这三个符号就对应了我们最常用的 红、黄、绿信号。对于绿灯时左转让直行的情形，应该将左转 lane 设置为 g，即让对面直行车辆先通过，再左转。



#### actuated traffic light

这种交通灯可以检测通过的车流，如果某条道路有持续的车流，则延长相应道路上的绿灯时间，否则当车辆之间的 time gap 超过一定时间，则转到下一个 phase。这是一种动态调整红路灯时长的机制。

为了使用 actuated，可以把 tlLogic 的 type 设置为 actuated，如下：

```xml
<tlLogic id="0" programID="my_program" offset="0" type="actuated">
    <param key="max-gap" value="3.0"/>
    <param key="detector-gap" value="2.0"/>
    <param key="show-detectors" value="false"/>
    <param key="file" value="NULL"/>
    <param key="freq" value="300"/>

    <phase duration="31" minDur="5" maxDur="45" state="GGggrrrrGGggrrrr"/>
    ...
</tlLogic>
```

在这种设置下，还需要设置更多的参数，其中

- phase tag 要设置 minDur 和 maxDur。如果只有 duration 或者 minDur==maxDur，则还是固定时长的 phase.
- max-gap: 两车最大时间间隔，单位 s
- detector-gap: 从 detector 到 stop line 的时间间隔，以该 lane 的最大速度计算。



#### delay_based traffic light

这种机制是为了均衡各条道路的延迟，基本设置如下：

```xml
<tlLogic id="0" programID="my_program" offset="0" type="delay_based">
    <param key="detectorRange" value="100" />
    <param key="minTimeLoss" value="1" />
    <param key="file" value="NULL"/>
    <param key="freq" value="300"/>
    <param key="show-detectors" value="false"/>

    <phase duration="31" minDur="5" maxDur="45" state="GGggrrrrGGggrrrr"/>
    ...
</tlLogic>
```

其中 type 设置为 delay_based。

- detectorRange 为检测范围。如果车辆进入该范围，就计算其时间延迟 1-v/v_max。
- minTimeLoss: 如果道路的时间延迟积累超过该数值，则增加下次绿灯的时长。



### TraCI

TraCI: Traffic Control Interface. 交通控制接口。

作用：获取交通模拟环境中的数据，并实时修改、控制。

已经有很多主流编程语言实现了该接口，包括 python, c++, .NET, MATLAB, Java，其中 python 版本的 TraCI 功能最全面。下面我们只考虑 python 版本的 TraCI。关于 TraCI 中类、函数的详细说明，可以参考[官方文档](https://sumo.dlr.de/daily/pydoc/traci.html)。



在 SUMO 与 TraCI 交互中，SUMO 作为 server 提供运行数据, TraCI 作为 Client 控制、修改运行状态. SUMO 中原本设置的 `--end <time>` 不再其作用，由 TraCI 控制仿真的结束。



在上述 server-client 框架中，可以存在 multi-client。只有当所有 client 都与 server 连接上之后才会开启仿真，而且只有所有 client 都发布了 `simulationStep` 命令之后才会进行下一步的仿真。所有的 client 都要有一个附带的整数，以决定各自的动作顺序，数越小，动作越早。



#### Traci 基本命令

[参考网址](https://sumo.dlr.de/wiki/TraCI/Vehicle_Value_Retrieval)

- 获取汽车从起点以来的行驶距离：`traci.vehicle.getDistance(vehID) -> double ` 
- 获取速度：`traci.vehicle.getSpeed(vehID) -> double`
- 获取位置：`traci.vehicle.getPosition(vehID) -> (double, double)`
- 在给定时段内平缓的改变至给定速度：`traci.vehicle.slowDown(vehID, speed, duration) -> None`
- 设置速度，瞬间生效，没有减速、加速的过程：`traci.vehicle.setSpeed(vehID, speed) -> None`

#### 设置系统搜索路径

为了方便 import 调用，需要设置系统的搜索路径，方式如下：

```python
import os, sys
if `SUMO_HOME` in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")
```

以上将 $SUMO_HOME/tools  加入了系统搜索路径，这样 python 就可以直接 import  里面的 traci module 了。

注意在运行上述程序之前，要先设定好 SUMO_HOME 环境变量。

其实也可以直接把 SUMO_HOME/tools 的绝对路径加入到 python 搜索路径，能成功 import traci 即可。这样做程序会简洁一些，但是可移植性变差。

**设置 traci 的路径也是为了指定系统使用的 sumo / traci 版本，避免出现兼容问题。**



#### 通过 TraCI 启动 SUMO 

只需要将运行 sudo 的命令作为参数送给 traci.start 即可，命令如下：

```python
sumoBinary = "path_to_sumo-gui"  # 给出 sumo-gui 的路径
sumoCmd = [sumoBinary, "-c", "yourConfig.sumocfg"]

import traci

traci.start(sumoCmd)
### you code here
traci.close()  # 
```

上述命令中是手动设置 sumo-gui 的路径，我们也可以通过 checkBinary 自动查找、返回路径给 sumoBinary，如下：

```python
from sumolib import checkBinary  # 一定要先设置好搜索路径，否则会提示找不到 sumolib 
sumoBinary = checkBinary('sumo-gui')
```



#### 在 script 中运行多个仿真

如果要在同一个 python script 中运行多个仿真，可以通过 traci.switch 的方式，或者通过构造 connection 对象的方式，只要给不同的仿真贴上标签就行。

- traci.switch

  ```python
  traci.start(["sumo", "-c", "sim1.sumocfg"], label="sim1")
  traci.start(["sumo", "-c", "sim2.sumocfg"], label="sim2")
  traci.switch("sim1")
  traci.simulationStep()  # run 1 step for sim1
  traci.switch("sim2")
  traci.simulationStep()  # run 1 step for sim2
  ```

- connection 对象

  ```python
  traci.start(["sumo", "-c", "sim1.sumocfg"], label="sim1")
  traci.start(["sumo", "-c", "sim2.sumocfg"], label="sim2")
  conn1 = traci.getConnection("sim1")
  conn2 = traci.getConnection("sim2")
  conn1.simulationStep()  # run 1 step for sim1
  conn2.simulationStep()  # run 1 step for sim2
  ```

  

#### 多个 client 连接同一个仿真

只需要告诉仿真有几个 client 要连接它就可以了，所有的 client 通过同一个 port 与仿真通信。

例如：

- 先找到一个可用的 port

  ```python
  from sumolib.miscutils import getFreeSocketPort
  port = getFreeSocketPort()
  ```

- 启动多个 client，只需要有一个 client start SUMO 即可，其他的都是与 port 连接。

  ```python
  # client 1
  traci.start(["sumo-gui", "-c", "sim.sumocfg", "--num-clients", "2"], port=PORT)
  traci.setOrder(1)
  # do something here
  traci.close()
  ```

- 其他的 client

  ```python
  traci.init(PORT)
  traci.setOrder(2)
  # do something here
  traci.close()
  ```

  

#### Demo： 通过 TraCI 控制 SUMO 中的交通灯状态 

来自 [SUMO 官网教程](https://sumo.dlr.de/wiki/Tutorials/TraCI4Traffic_Lights)。所有程序可以在 https://github.com/eclipse/sumo/tree/master/tests/complex/tutorial/traci_tls 中找到。

考虑如下所示路口：



![SUMO_TraCI1](pic/SUMO_TraCI1.png)

基础信号灯变换顺序如下：

```xml
<tlLogic id="0" type="static" programID="0" offset="0">
    <phase duration="31" state="GrGr"/>
    <phase duration="6" state="yryr"/>
    <phase duration="31" state="rGrG"/>
    <phase duration="6" state="ryry"/>
</tlLogic>
```

然后，希望通过 TraCI 修改信号灯转换机制：

- 当南北方向没有车辆过来时，东西方向一直保持绿灯
- 当南北方向有车过来时，切换信号灯，东西方向黄灯 6 s，然后变红，同时南北方向变绿，持续 31 s，然后变黄，再变红，东西方向变绿。
- 重复上述两个过程

现在假设 net.xml 文件已经得到。实际上，通过 [netedit](https://sumo.dlr.de/wiki/NETEDIT) 可以很容易的构造上述交通路网。

SUMO 与 TraCI 的交互是在文件 runner.py 中实现的，主要包括如下内容：

1. 首先检查系统路径，以便后续 python 的 module 调用，主要是调用 traci 

   ```python
   if 'SUMO_HOME' in os.environ:
       tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
       sys.path.append(tools)
   else:
       sys.exit("please declare environment variable 'SUMO_HOME'")
    
   import traci  
   ```

2. 定义函数 generate_routefile()，用来创建 .rou.xml 文件

   ```python
   def generate_routefile():
       random.seed(42)  # make tests reproducible
       N = 3600  # number of time steps
       # demand per second from different directions
       pWE = 1. / 10
       pEW = 1. / 11
       pNS = 1. / 30
       with open("data/cross.rou.xml", "w") as routes:
           print("""<routes>
           <vType id="typeWE" accel="0.8" decel="4.5" sigma="0.5" length="5" minGap="2.5" maxSpeed="16.67" \
   guiShape="passenger"/>
           <vType id="typeNS" accel="0.8" decel="4.5" sigma="0.5" length="7" minGap="3" maxSpeed="25" guiShape="bus"/>
           
           <route id="right" edges="51o 1i 2o 52i" />
           <route id="left" edges="52o 2i 1o 51i" />
           <route id="down" edges="54o 4i 3o 53i" />""", file=routes)
           vehNr = 0
           for i in range(N):
               if random.uniform(0, 1) < pWE:
                   print('    <vehicle id="right_%i" type="typeWE" route="right" depart="%i" />' % (
                       vehNr, i), file=routes)
                   vehNr += 1
               if random.uniform(0, 1) < pEW:
                   print('    <vehicle id="left_%i" type="typeWE" route="left" depart="%i" />' % (
                       vehNr, i), file=routes)
                   vehNr += 1
               if random.uniform(0, 1) < pNS:
                   print('    <vehicle id="down_%i" type="typeNS" route="down" depart="%i" color="1,0,0"/>' % (
                       vehNr, i), file=routes)
                   vehNr += 1
           print("</routes>", file=routes)
   ```

   运行上述函数之后，会在 data/ 目录下生成 cross.rou.xml 文件，里面包含了由东向西、由西向东、由北向南的交通流信息。

3. 从 SUMO 中获取交通信息，然后对交通灯状态施加控制

   ```python
   def run():
       """execute the TraCI control loop"""
       step = 0
       traci.trafficlight.setPhase("0", 2)  # 这里 0 是 traffic light 的 ID，四个 phase 依次编号为 0, 1, 2, 3，初始时设置 phase 为 2，即 东西道路为 G，南北道路为 r。
       while traci.simulation.getMinExpectedNumber() > 0:  # 该函数得到当前 net 中的车辆数目加上还没有进入 net 的车辆数目。只要该数值 > 0，就表明还有车辆需要处理。
           traci.simulationStep()  # 运行一步仿真
           if traci.trafficlight.getPhase("0") == 2:
               if traci.inductionloop.getLastStepVehicleNumber("0") > 0:  # 在上一步仿真中，经过 induction loop 的汽车数量
                   traci.trafficlight.setPhase("0", 3)  # 如果有车进来，则切换 phase
               else:
                   traci.trafficlight.setPhase("0", 2)  # 否则依然保持 phase。注意，这里是重置了 phase, 所以会重新计时。
           step += 1
       traci.close() 
   ```

4. 运行主程序

   ```python
   if __name__ == "__main__":
       generate_routefile()
       traci.start(["sumo-gui", "-c", "data/cross.sumocfg",
                                "--tripinfo-output", "tripinfo.xml"])  # tripinfo 中记录了每一辆车在网络中的行驶信息，包括出发时间、出发车道、到达时间、等待时间、车辆类型等等。
       run()
   ```

   首先是生成 .rou.xml 文件，然后运行已经设置好的 sumocfg 文件，里面实际上是调用了 .net.xml 文件、.rou.xml 文件以及感应线圈的设置文件，通过 traci.start 启动 SUMO，建立 traci 与 SUMO 的通信连接。最后运行 run 函数，实现两者的交互。

   在官方给出的程序中，并没有直接调用 sumo-gui ，而是通过 sumolib 中的 checkBinary 函数先查找 sumo-gui 程序的位置，然后再运行它。这两者效果是一样的。



## Vehicle dynamics model

SUMO 中车辆动力学模型包括两方面 

- longitudinal model ： 纵向动力学模型，描述车辆加速和减速
- lateral model：横向动力学模型，描述车辆换道



在 longitudinal model 方面，由于 SUMO 主要用于研究车辆的外部行为、多车交互和交通流，对于单个车辆建模精度需要不高，可以近似看作质点，所以采用比较简单的 car following model (跟车模型) 来描述车辆速度和位置变化规律。car following model 中包含两种情况：无前车和有前车。

- 对于无前车的情形，车辆保持为最大速度，这里最大速度要考虑 3 方面的因素：

  1. 该类型车辆本身能够达到的最大物理速度
  2. 前一时刻速度经过最大加速之后在当前时刻所能达到的最大速度
  3. 当前行驶道路规定的最大速度

  最终的行驶速度为上述三个最大速度中的最小值。

- 对于有前车的情形，则要计算安全的行驶速度，保证任何情况下（尤其是当前车急刹车时）车辆不会相撞。不同的 car following model 的主要区别就在于如何计算安全行驶速度。目前 SUMO 中采用的 car following model 为改进的 Krauss model. 后边会详细介绍基本的 Krauss model 和 SUMO 采用的改进 Krauss model.



lateral model 采用 lane changing model ([参考文献](https://pdfs.semanticscholar.org/fd1f/394b16ddffa167cf00b69181d08c4512af78.pdf)）。简单地说就是以决策树的方式设定诸多换道条件，只要满足一定的换道条件，就进行相应的操作。 

默认的 lane changing model 是瞬间换道，即在一个 simulation step 中完成换道。

更加精细的模型包括：

- SublaneModel

- Simple Continous lane-change model

具体设置可以参考 https://sumo.dlr.de/wiki/Simulation/SublaneModel#Sublane-Model



### original Krauss model

要了解 SUMO 中的改进 Krauss model，需要先了解一下原始的 Krauss model 的建模思想。

来自文献

Stefan Krauß. Microscopic Modeling of Traffic Flow: Investigation of Collision Free Vehicle Dynamics. PhD thesis. 1998

假设 $g=x_l - x_f -l$ 为车间距，其中 $l$ 为车身长度。

如果要求车辆不相撞，需要满足
$$
L(v_f) + v_f\tau \le L(v_l) + g
$$
其中

- $v_f$ 为 follower 的速度，也就是我们需要计算的量
- $v_l$ 为 leader 的速度
- $L(v_f)$ 为 follower 的刹车距离
- $L(v_l)$ 为 leader 的刹车距离
- $\tau$ 为驾驶员从观察到动作的反应时间
- $g$ 为前边定义的车间距。

为了计算 $v_f$， 需要给出速度与刹车距离的函数的表达式  $L(v_f)$ 和 $L(v_l)$ 。下边用 $L(\cdot)$ 函数在 $\bar{v} = (v_f + v_l)/2$ 处的 Taylor 展开近似替代  $L(\cdot)$ 函数，忽略高阶项得到
$$
L'(\bar{v})v_f + v_f\tau \le L'(\bar{v})v_l + g
$$
下边的问题就是如何计算导数  $L'(\bar{v})$.

假设刹车时加速度为 $\dot{v} = -b(v)$，则有
$$
L'(v) =\frac{d}{dv}\int_v^0 \frac{s}{-b(s)}ds = \frac{v}{b(v)}
$$
其中的积分项对应了刹车加速度为 $-b(v)$ 情况下的刹车距离。

一般我们在计算距离时习惯将积分区间设定为时间，而被积函数为速度。这里是将积分区间设定为速度的变化区间，对时间进行积分。这样积分之后得到关于速度的函数，以便后续的操作。

将 (3) 式带入 (2) 式中得
$$
v_f \le v_l + \frac{g - v_l\tau}{\frac{\bar{v}}{b(\bar{v})}+\tau}
$$
上述表达式右边 $\bar{v} = (v_f + v_l)/2$  含有$v_f$ ，所以需要再整理一下，得到 $v_f$ 的显式表达为
$$
v_f \le -b\tau + \sqrt{(b\tau)^2 + v_l^2 + 2bg}
$$
其中原本的 $-b(v)$ 也替换为了最大刹车加速度 $-b$.

上述式子就是 SUMO 中 original Krauss model 中的安全跟车速度表达式，部分[程序源码](https://sumo.dlr.de/doxygen/d7/d4b/_m_s_c_f_model___krauss_orig1_8cpp_source.html)如下：

```c++
double MSCFModel_KraussOrig1::vsafe(double gap, double predSpeed, double /* predMaxDecel */) const {
	 .
     .
     .
     double vsafe = (double)(-1. * myTauDecel
                             + sqrt(
                                 myTauDecel * myTauDecel
                                 + (predSpeed * predSpeed)
                                 + (2. * myDecel * gap)
                             ));
     assert(vsafe >= 0);
     return vsafe;
 }
```



这里 (5)式右边就是安全的跟车速度，记做 $v_{safe}$. 但是，这一速度还不是最终车辆采用的速度。与无前车情况类似，我们也要保证跟车速度不能超过允许的最大速度，因此要取安全速度和允许最大速度中的较小值，即
$$
v_f = \min\{v_{max}, v(t)+a(t)\Delta t, v_{safe}(t)\}
$$
其中 $a$ 为最大加速度，$\Delta t$ 为仿真更新步长。

另外，可以引入随机因子，表示车辆并不一定按照上述安全跟车速度行驶，可以取更小的值，即
$$
v = \max\{0, v_f-rand(0, \epsilon a) \}
$$
其中 $\epsilon\in [0,1]$ 为外部设定的 imperfection parameter，表征了偏离 $v_f$ 的程度。

总结算法步骤：

1. 由 (5) 式计算 $v_{safe}$
2. 由 (6)(7) 式计算最终的跟车速度 $v$
3. 由速度更新车辆位置：$x(t+\Delta t) = x(t) + v\Delta t$



### 改进的 Krauss model

尽管 SUMO 中包含了上述原始 Krauss model，但是没有作为默认 car following model，而是做了较大改动。改进模型与原始的 Krauss 模型的出发点是相同的：在保证不碰撞的前提下，车速尽量的快。但在计算安全速度方面，与原始 Krauss 完全不同。

改进的 Krauss model 依然基于上述公式(1)，但并没有采用泰勒展开方式近似表达刹车距离函数 $L( \cdot)$，而是直接计算。步骤与源码实现如下：

1. 计算前车的刹车距离 
   $$
   t = \frac{v_l}{b} \\
   L(v_l) = v_lt - \frac{1}{2}bt^2
   $$
   上式中 $t$ 为前车速度减到 0 需要的时间。

   在 SUMO 源码中通过 [brakeGapEuler 函数](https://sumo.dlr.de/doxygen/d1/d9d/class_m_s_c_f_model.html#a2bca3656d19dc62d945297d39b3e07b1) 实现上述计算过程。

   这里需要注意的是，SUMO 默认采用 [Euler 数值积分方式](https://sumo.dlr.de/wiki/Simulation/Basic_Definition#Defining_the_Integration_Method)，在计算位置时公式如下：
   $$
   x(t+1) = x(t) + \Delta t* v(t+1)
   $$
   即 $t+1$ 时刻的位置等于 $t$ 时刻的位置加上 $t+1$ 时刻的速度造成的位置变化。

   明白了这种积分方式才能更好的理解源码中的计算公式。

2. 基于公式 (1) 计算 $v_f$. 基本思想是找到一个速度使得后车在此速度下刹车距离 (包括反应距离) 正好等于前车的刹车距离加上原本两车间距。具体在 [maximumSafeStopSpeedEuler 函数](https://sumo.dlr.de/doxygen/d1/d9d/class_m_s_c_f_model.html#a7f343f2b33d5e851ee6ef79e181cff29)中实现。

得到安全跟车速度之后，其余部分与原始的 Krauss model 类似的，要与允许的最大速度比较，并且考虑随机因素。具体在 [followSpeed 函数](https://sumo.dlr.de/doxygen/d3/d44/class_m_s_c_f_model___krauss.html#a1e704feadb3c0a4f5c8bb1e10eb5899c) 和 [dawdle2 函数](https://sumo.dlr.de/doxygen/d3/d44/class_m_s_c_f_model___krauss.html#a2365823eb88bb2bb11c16058ece1e1d7) 中实现。



## SUMO 仿真结果统计分析

SUMO 作为一款常用的交通系统仿真软件，其仿真结果常被用于统计分析，例如计算某条道路上单位时间内的车流量，某种交通灯控制策略对行车延时的影响等。

SUMO 提供了多种获取统计结果的方式。本文主要介绍两种：
- 设置参数 `--duration-log.statistics ` ，自动获取实时统计结果。[参考网址](https://sumo.dlr.de/wiki/Simulation/Output#Aggregated_Traffic_Measures)
- 设置参数 `--tripinfo-output`，得到仿真数据文件，再进行后续分析。[参考网址](https://sumo.dlr.de/wiki/Simulation/Output/TripInfo)

### 获取实时统计结果
在启动 SUMO 时，添加参数  `--duration-log.statistics`，即
```bash
sumo-gui  <config_file>  --duration-log.statistics 
```
启动 sumo 之后，鼠标右键点击绿色背景区域，选择 " Show Parameters" 就可以得到如下图所示的实时统计结果：

![sumo_statistic1.gif](pic/sumo_statistic1.gif)



其中：
|     parameter      |                         description                          |
| :----------------: | :----------------------------------------------------------: |
|  loaded vehicles   |                      已经生成的车辆数目                      |
|  running vehicles  |                    正在路上运行的车辆数目                    |
|  arrived vehicles  |                    已经到达终点的车辆数目                    |
|  avg.trip.length   |                       车辆平均行驶距离                       |
| avg.trip.duration  |                       车辆平均行驶时间                       |
| avg.trip.time loss | 平均延迟时间 =  平均行驶时间 - 按照期望速度行驶所需时间 (上述程序中为 16m/s) |
|   avg.trip.speed   |            平均车速 = 平均行驶距离 / 平均行驶时间            |

实际上，不添加上述参数也可以获得一些统计结果，只不过添加参数之后统计结果更多一些。

### 获取仿真数据文件

如果自己想要的统计结果不在上述统计参数中，则需要导出仿真数据，手动分析。
例如，我们不仅想知道平均延迟时间，还想知道延迟时间的标准差，后者反映了交通控制策略的公平性。

为了得到输出数据，在启动 SUMO 时，添加参数  ` --tripinfo-output`，即
```bash
sumo-gui  <config_file>    --tripinfo-output    my_output_file.xml
```
仿真结束之后，数据都存放在了 `my_output_file.xml` 文件中，内容如下：
```xml
<tripinfos xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.dlr.de/xsd/tripinfo_file.xsd">
    ...
    <tripinfo id="v.146.3" depart="1460.01" departLane="end2_junction_0" departPos="65.63" departSpeed="16.00" departDelay="0.00" arrival="1517.92" arrivalLane="junction_end1_0" arrivalPos="486.40" arrivalSpeed="16.00" duration="57.91" routeLength="916.20" waitingTime="0.00" waitingCount="0" stopTime="0.00" timeLoss="-0.00" rerouteNo="0" devices="tripinfo_v.146.3" vType="vtypeauto" speedFactor="1.15" vaporized=""/>
    ...
</tripinfos>
```
这里包含了每一辆车的详细行驶信息。由于输出的是 xml 文件，我们可以借助 python 的 [xml.etree.ElementTree](https://docs.python.org/3/library/xml.etree.elementtree.html#module-xml.etree.ElementTree) module  提取其中的数据并分析。



# OMNeT++

## 概述与安装

Objective Modular Network Testbed in C++

是一个面向对象、模块化的基于 C++ 的函数库，主要用来实现网络的仿真。

可以参考[本文档](https://doc.omnetpp.org/omnetpp/InstallGuide.pdf)进行安装。

这里以 5.1.1 版本为例，基本安装步骤如下：

1. 准备工作

   ```bash
   sudo apt update
   
   sudo apt-get install build-essential gcc g++ bison flex perl \
    python python3 qt5-default libqt5opengl5-dev tcl-dev tk-dev \
    libxml2-dev zlib1g-dev default-jre doxygen graphviz libwebkitgtk-1.0
   ```

   OMNeT++ 当前默认的图形界面是 Qtenv，如果要使用 Qtenv 3D 可视化功能，还需要安装 OpenSceneGraph (>=3.2) 和 osgEarth (>=2.7)。对于 Ubuntu 16.04，自带的 osgEarth 版本太旧 (2.5)，所以还需要添加 repo。

   ```bash
   sudo add-apt-repository ppa:ubuntugis/ppa
   sudo apt update
   
   sudo apt-get install openscenegraph-plugin-osgearth libosgearth-dev
   ```

   为了支持平行仿真，还需要安装 MPI 包。

   ```bash
   sudo apt-get install openmpi-bin libopenmpi-dev
   ```

   如果某些网络模型需要绕过系统自带的通讯协议，则还需要安装 Pcap 库

   ```bash
   sudo apt-get install libpcap-dev
   ```

    

2. 安装 OMNeT++

- 下载 OMNeT++ 压缩包，并解压。

- 设置路径：将 OMNeT++ 文件夹下的 bin 目录添加到环境变量 PATH 中，为了永久有效，可以修改 .bashrc 文件如下：

  ```
  echo 'export PATH=<path_to_bin>:$PATH'  >> ~/.bashrc
  ```

- 运行 configure 文件，生成 makefile.inc 文件，以备随后的编译使用

  ```bash
  ./configure
  ```

- 编译

  ```bash
  make  -j 8  # -j 后边跟同时进行的 job 数量，视 CPU 的核数而定
  ```

- 测试：运行 sample/dyna 文件夹中的 dyna 文件

  ```bash
  cd samples/dyna
  
  ./dyna
  ```

  如果可以打开 GUI，并可以正常运行，就说明安装成功了。



一个完整的 OMNeT++ 仿真程序由以下部分组成：

- NED 文件：定义网络架构。一个 OMNeT++ 模型最基础的是 module，他们之间可以通信交换数据。Module 可以嵌套。
- C++ 文件：具体的网络信息流动在 C++ 文件中定义。
- omnetpp.ini 文件：用来配置参数。可以包含多个仿真实例。

最后编译程序并运行。text-based 仿真结果会保存到输出文件中。可以在 IDE 中可视化它们，也可以通过 R， MATLAB 等工具进行后续的分析。



## Demo1: 基本的 tictoc

要实现的仿真场景：一个网络包含 2 个节点，分别叫做 tic， toc。tic 节点产生一个 packet。随后这个 packet 就在 2 个节点之间来回传递。步骤：

#### 启动 IDE

OMNeT++ 的 IDE 是在 Eclipse 基础上扩展的。Eclipse 提供了 C++ 编辑功能，然后外加了 OMNeT++ 相关的功能，如编辑模型 NED 文件、ini 文件，分析、可视化仿真结果等。

也就是说可以用该 IDE 编辑所有相关的文件，而且对不同类型的文件，会有一些特征支持。

```bash
nmetpp
```

首先会要求用户设置工作空间，我这里创建了文件夹 Liu_ws 专门存放自己的文件。然后出现如下界面：

![omnetpp1](pic/omnetpp1.png)

点击最右侧的图标，进入 IDE 编辑界面，如下

![omnetpp2](pic/omnetpp2.png)

创建新 Project。 一个 Project 文件夹中包含了该 project 所有相关文件，这样管理、迁移比较方便。IDE 只是集成了众多界面操作功能。实际上，完全可以手动创建一个文件夹，用来存在 Project 的文件，不用 IDE 而只用 text 编辑器和 OMNeT++ 提供的其他命令行程序完成 project 开发。

选择 New --- OMNeT++ Project，命名为  tictoc，如下新项目界面：

![omnetpp3](pic/omnetpp3.png)

#### 添加 NED 文件

Network Description File，用来创建网络的各个 module，并把他们整合成一个网络。

一个 Project 中可以包含多个网络，所以 NED 文件也可以有多个。

右键点左侧 Project 栏，New --- Network Description File (NED)，命名为 tictoc1.ned

![omnetpp4](pic/omnetpp4.png)

一旦创建 ned 文件，右边编辑窗口就打开该文件，等待编辑。下方可以切换编辑模式，可以以 design (graphical mode) 或者 source (text-based mode) 编辑 NED 文件。两种模式是完全同步的。

在 source mode 下写入如下程序：

```c++
simple Txc1    // 先创建一个 module 类，后边的具体 module 可以由此类生成
{
    gates:
        input in;
        output out;
}

//
// Two instances (tic and toc) of Txc1 connected both ways.
// Tic and toc will pass messages to one another.
//
network Tictoc1   // 构造网络
{
    submodules:    // 包含的子模块/组件，这里都是从前边的 Txc1 类生成的对象
        tic: Txc1; 
        toc: Txc1;
    connections:    // 子模块之间的连接方式
        tic.out --> {  delay = 100ms; } --> toc.in;
        tic.in <-- {  delay = 100ms; } <-- toc.out;
}
```

对应的 design mode 如下：

![omnetpp5](pic/omnetpp5.png)

上图中 Txc1 是 module type, 具体的行为要在 C++ 程序中定义。

#### 定义 module 行为

右键点击左侧 project 栏，New --- Source File。命名为 txc1.cc，内容如下：

```c++
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

/**
 * Derive the Txc1 class from cSimpleModule. In the Tictoc1 network,
 * both the `tic' and `toc' modules are Txc1 objects, created by OMNeT++
 * at the beginning of the simulation.
 */
class Txc1 : public cSimpleModule   // 这里的 Class 必须与 ned 文件中名字相同
    								// 而且必须是从 cSimpleModule 继承下来的
{
  protected:
    // The following redefined virtual function holds the algorithm.
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(Txc1);                // 而且必须在 OMNeT++ 中注册一下

void Txc1::initialize()
{
    // Initialize is called at the beginning of the simulation.
    // To bootstrap the tic-toc-tic-toc process, one of the modules needs
    // to send the first message. Let this be `tic'.

    // Am I Tic or Toc?
    if (strcmp("tic", getName()) == 0) {
        // create and send first message on gate "out". "tictocMsg" is an
        // arbitrary string which will be the name of the message object.
        cMessage *msg = new cMessage("tictocMsg");  // cMessage 是消息类, tictocMsg 为 														// msg object 的名字
        send(msg, "out");
    }
}

void Txc1::handleMessage(cMessage *msg)  // 这个函数名字应该是固定的
    									 // 默认就是从 gate in 进来
{
    // The handleMessage() method is called whenever a message arrives
    // at the module. Here, we just send it to the other module, through
    // gate `out'. Because both `tic' and `toc' does the same, the message
    // will bounce between the two.
    send(msg, "out"); // send out the message
}
```

message 和 events 都是事件，被 simulation kernel 按照设定的序列安排好，一旦到了哪个事件动作的时刻，就送入某个 module 进行  handleMessage( ) 操作。

也就是说，传输延迟 100ms 是由 simulation kernel 控制的，等到了 100ms 才发送，而不是在各个 module 内部实现延迟。

#### 添加 omnetpp.ini 文件

该文件可以告诉仿真程序要运行哪个 network，以及相应的参数设置。

右键点击 project 栏，选择 New --- Initialization File (ini)，编辑器中出现了 ini 文件的编辑界面。有 Source 和 Form 两种 mode。

- Source mode 比较适合设置指定仿真网络、参数

- Form mode 比较适合配置仿真平台环境。

在 Source mode 中编辑如下：

```ini
[General]
network = Tictoc1
```

即指明这次仿真 Tictoc1 这个网络。也可以有额外的 network，可以同时启动多个。

#### 编译文件

将编辑器调整至 ini 文件窗口，然后点工具栏中的 run 即可完成编译。这是在 IDE 中进行的编译操作。

也可以用命令行编译：

- 首先通过 opp_makemake 命令创建一个 Makefile 
- 通过 make 命令编译程序，会得到可直接运行的 tictoc 文件

#### 运行仿真

编译完成之后会出现一个新的 GUI，这是基于 Qtenv 的，是 OMNeT++ 默认的运行 GUI。

![omnetpp6](pic/omnetpp6.png)

- Run: 点击 run 可以看到 tic 和 toc 两个节点来回传递 msg。

- Fast: 不显示动画，快速的运行，只在下部显示文本信息 

- Express: 甚至连文本信息也不显示，以最大速度运行。

当运行时，本次仿真的配置都存放在了一个 launch configuration 中，可以通过 run 或者 debug 旁边的向下箭头查看。以后运行时可以在这里改变配置，比如是否需要 GUI。

#### Debug

OMNeT++ 核心是 c++ 程序对 module 行为的管理。所以难免需要 debug。

IDE 集成了 debug 环境，默认的 debugger 为 gbd。

点击 run 旁边的 debug ，即可进入 debug 模式，此时 IDE 布局发生变化，更适合 debug，右上角可以切换视图。而且还多了一个红色实心方框，可以点击结束 debug 模式。

![omnetpp7](pic/omnetpp7.png)

#### 数据记录

每次仿真中信息交换都可以被记录到 log file 中，以备后续分析。

有 3 种途径开启 log：

1. 在 launch configuration dialog 中，check eventlog
2. 在  omnetpp.ini 中添加 `record-eventlog = true`
3. 在 Qtenv 窗口中选择 Record

运行仿真之后，会自动创建一个 result 文件夹，其中就包含了 .elog 文件

IDE 提供了可视化工具，可以将 elog 文件内容通过 Sequence Chart 表示出来，非常直观，便于理解和 debug。

![omnetpp8](pic/omnetpp8.png)



## Demo2: tictoc 扩展

#### 添加 icon

目标：为两个 node 添加图案，图案相同，但颜色不同

在 OMNeT++ 文件夹中已经包含了很多 image 可供选择。

修改 ned 文件如下:

```c++
simple Txc2
{
    parameters:
        @display("i=block/routing"); // add a default icon
    gates:
        input in;
        output out;
}

//
// Make the two module look a bit different with colorization effect.
// Use cyan for `tic', and yellow for `toc'.
//
network Tictoc2
{
    submodules:
        tic: Txc2 {
            parameters:
                @display("i=,cyan"); // do not change the icon (first arg of i=) just colorize it
        }
        toc: Txc2 {
            parameters:
                @display("i=,gold"); // here too
            @display("p=112,108");
        }
    connections:
        tic.out --> {  delay = 100ms; } --> toc.in;
        tic.in <-- {  delay = 100ms; } <-- toc.out;
}
```

修改之后，外形如下
![omnetpp9](pic/omnetpp9.png)

#### 添加屏幕 log 信息

额外的 log 信息有时可以帮助用户更好的监控程序。

原本 tictoc 只显示如下基本的事件信息

```
** Event #694  t=69.4  Tictoc1.tic (Txc1, id=2)  on tictocMsg (omnetpp::cMessage, id=0)
** Event #695  t=69.5  Tictoc1.toc (Txc1, id=3)  on tictocMsg (omnetpp::cMessage, id=0)
** Event #696  t=69.6  Tictoc1.tic (Txc1, id=2)  on tictocMsg (omnetpp::cMessage, id=0)
** Event #697  t=69.7  Tictoc1.toc (Txc1, id=3)  on tictocMsg (omnetpp::cMessage, id=0)
** Event #698  t=69.8  Tictoc1.tic (Txc1, id=2)  on tictocMsg (omnetpp::cMessage, id=0)
** Event #699  t=69.9  Tictoc1.toc (Txc1, id=3)  on tictocMsg (omnetpp::cMessage, id=0)
```

如果还希望了解各个节点在事件中都做了什么事情，可以在 C++ 文件的相应位置添加 log 显示信息

```c++
# 在 initialize() 中添加
EV << "Sending initial message\n";

# 在 andleMessage(cMessage *msg) 中添加
EV << "Received message `" << msg->getName() << "', sending it out again\n";
```

仿真运行时得到的 log 如下：

```c++
Tictoc1.tic: Initializing module Tictoc1.tic, stage 0
INFO (Txc1)Tictoc1.tic: Tictoc1.tic: Sending initial message
Tictoc1.toc: Initializing module Tictoc1.toc, stage 0
** Event #1  t=0.1  Tictoc1.toc (Txc1, id=3)  on tictocMsg (omnetpp::cMessage, id=0)
INFO (Txc1)Tictoc1.toc: Received message 'tictocMsg', sending it out again
** Event #2  t=0.2  Tictoc1.tic (Txc1, id=2)  on tictocMsg (omnetpp::cMessage, id=0)
INFO (Txc1)Tictoc1.tic: Received message 'tictocMsg', sending it out again
** Event #3  t=0.3  Tictoc1.toc (Txc1, id=3)  on tictocMsg (omnetpp::cMessage, id=0)
INFO (Txc1)Tictoc1.toc: Received message 'tictocMsg', sending it out again
```

其中 INFO 开头的 log 就是新加入的信息。

如果只想看某个 component (例如 tic 节点) 的 log 信息，可以右键点击，选择 "Open component log for 'tic'"。

#### 添加变量用来计数

希望设定一个计数变量，记录 msg 交换的次数，交互 10 次之后就删除。

在 class 中定义新的变量 counter: 

```c++
class Txc3 : public cSimpleModule
{
    int counter;  // Note the counter here

  protected:
```

在 initialize( ) 中设置其值为 10

```c++
counter = 10;
WATCH(counter);  // 可以在仿真时点击 node，在 children mode tab 下可以看到 counter 的值变化
if (strcmp("tic", getName()) == 0) { 
    ...
```

handleMessage( ) 中没处理一次 counter - 1。

需要注意的是，tic 和 toc 各有一个 counter，是单独计数的，谁接收到 msg，谁就减 1。当某个节点 counter 减到 0 就 delete msg。

```c++
    counter -= 1;
    if (counter == 0){
        EV << getName() << "'s counter reached zero, deleting message\n";
        delete msg;
    }
    else {
        send(msg, "out"); // send out the message
        //EV << "Received message '" << msg->getName() << "', sending it out again\n";
        EV << getName() << "'s counter is" << counter << ", seding back message\n";
    }
```

这里 getName( ) 的作用是获取 object name，这里是获取 node name，即 tic 或者 toc。

前边 msg->getName( ) 则是获取了对应 msg 的名字。

由于 delete msg，已经没有事件可被触发了，所以出现如下提示：

![omnetpp10](pic/omnetpp10.png)

#### ned, c++, ini 变量交互

上述例子中 ned 文件中 moduel class txc1 与 c++ 中 class txc1 实际上是同一个对象在不同层面的定义。

前边决定谁初始化 msg 时是在 c++ 通过 getName( ) 读取 object 名字，如果是 tic 就发送初始的 msg。为了实现这个判断，我们也可以在 ned 中设定 boolean 变量，在 object 中指定不同的 boolean 变量值，在 c++ 中直接读取这个变量值，来决定是否发送初始 msg。

另外，原本计数也是仅在 c++ 中设定的。也可以在 ned 中 (或者 ini 文件中) 设置变量和赋值，然后在 c++ 中读取。如果同时在 ini 和 ned 中赋值， ned 优先，除非 ned 中只是 default 赋值。

这样修改之后，感觉是控制变量都在 ned 或者  ini 中设置了，c++ 只是调用，来实现具体功能。

在 ned 中添加变量 

```c++
simple Txc4
{
    parameters:
        bool sendMsgOnInit = default(false); // whether the module should send out a message on initialization
        int limit = default(2);   // another parameter with a default value
        @display("i=block/routing");
    ...

network Tictoc4
{
    submodules:
        tic: Txc4 {
            parameters:
                sendMsgOnInit = true;
                @display("i=,cyan");
        }
        toc: Txc4 {
            parameters:
                sendMsgOnInit = false;
                @display("i=,gold");
        }
    ...
```

在 ini 中设置变量值

```ini
[General]
network = Tictoc1
Tictoc1.toc.limit = 5
```

这里 toc.limit 将取代原本 ned 中的 default (2)。

在 c++ 中调用 ned 和 ini 设置好的变量：

```c++
counter = par("limit");   // 不再内部赋值 10 了

if (par("sendMsgOnInit").boolValue() == true) {  // 通过 sendMsgOnInit 变量来判定发送 msg
```

最后结果是，当 tic limit 从 2 降到 0 时，仿真结束。

![omnetpp11](pic/omnetpp11.png)

#### ned 中的类继承

与 c++ 类似， ned 也可以实现类继承。下边就改写之前的 ned 程序。

目标：定义 base class，然后 tic 类 和 toc 类都从 base class 继承而来。

首先定义 base class 如下：

```c++
simple Txc1
{
    parameters:
        bool sendMsgOnInit = default(false);
        int limit = default(2);
        @display("i=block/routing");
    gates:
        input in;
        output out;
}
```

tic 类：

```
simple Tic1 extends Txc1
{
    parameters:
        @display("i=,cyan");
        sendMsgOnInit = true;   // Tic modules should send a message on init
}
```

toc 类：

```c++
simple Toc1 extends Txc1
{
    parameters:
        @display("i=,gold");
        sendMsgOnInit = false;  // Toc modules should NOT send a message on init
}
```

最后分别从 tic 和 toc 类生成两个对象

```c++
network Tictoc1
{
    submodules:
        tic: Tic1;  // the limit parameter is still unbound here. We will get it from the ini file
        toc: Toc1;
    connections:
```

这样 ned 中就是由一个 base 衍生出两个 类，然后各产生一个 对象作为 node。

c++ 中依然是对 base class 定义即可，最终都会反映在继承类里面。

最后运行仿真应该获得与之前同样的效果。

#### 模拟内部处理延迟

前边的程序都是接受之后马上就发送，延迟都是体现在通信网络中。

下面看看如何模拟处理过程的延迟。在 OMNeT++ 中，这种延迟一般是通过向自己发送一个 msg (self-msg) 来实现的。这个 self-msg 本质上就是 msg，只不过用在这种环境中，才称为  self-msg.

目标：tic 和 toc hold the msg for 1 sec, then send out.

这里不再设置 time limit 了，跟最初的来回传递一样。

只需要修改 c++ 如下：

```c++
class Txc1 : public cSimpleModule
{
  private:
    cMessage *event;  // pointer to the event object which we'll use for timing
    cMessage *tictocMsg;  // variable to remember the message until we send it back

  public:
    Txc1();
    virtual ~Txc1();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Txc1);

Txc1::Txc1()
{
    // Set the pointer to nullptr, so that the destructor won't crash
    // even if initialize() doesn't get called because of a runtime
    // error or user cancellation during the startup process.
    event = tictocMsg = nullptr;   // 让指针变量指向一个地方，这样不至于出问题。
}

Txc1::~Txc1()
{
    // Dispose of dynamically allocated the objects
    cancelAndDelete(event);
    delete tictocMsg;
}

void Txc1::initialize()
{
    // Create the event object we'll use for timing -- just any ordinary message.
    event = new cMessage("event");

    // No tictoc message yet.
    tictocMsg = nullptr;

    if (strcmp("tic", getName()) == 0) {  // 如果是 tic，才开始准备发送初始 msg
        // We don't start right away, but instead send an message to ourselves
        // (a "self-message") -- we'll do the first sending when it arrives
        // back to us, at t=5.0s simulated time.
        EV << "Scheduling first send to t=5.0s\n";
        tictocMsg = new cMessage("tictocMsg");
        scheduleAt(5.0, event);  // 在 5s 之后发送 event msg 给自己！
    }
}

void Txc1::handleMessage(cMessage *msg)
{
    // There are several ways of distinguishing messages, for example by message
    // kind (an int attribute of cMessage) or by class using dynamic_cast
    // (provided you subclass from cMessage). In this code we just check if we
    // recognize the pointer, which (if feasible) is the easiest and fastest
    // method.
    if (msg == event) {  // 收到 event msg，意味着计时时间到啦
        // The self-message arrived, so we can send out tictocMsg and nullptr out
        // its pointer so that it doesn't confuse us later.
        EV << "Wait period is over, sending back message\n";
        send(tictocMsg, "out");
        tictocMsg = nullptr;
    }
    else {
        // If the message we received is not our self-message, then it must
        // be the tic-toc message arriving from our partner. We remember its
        // pointer in the tictocMsg variable, then schedule our self-message
        // to come back to us in 1s simulated time.
        EV << "Message arrived, starting to wait 1 sec...\n";
        tictocMsg = msg;
        scheduleAt(simTime()+1.0, event);
    }
}
```

#### 引入随机数

目标：将前边的内部延迟时间从 1s 变为 随机数

这个可以在 ned 中设置参数，然后在 c++ 中直接调用即可。

ned 文件中的修改：

```c++
simple Txc7
{
    parameters:
        volatile double delayTime @unit(s);   // delay before sending back message
        @display("i=block/routing");
    gates:
```

volatile 是 ned 文件中很常用的设置，它使得 c++ 每次需要使用 delayTime 的时候都要重新计算一下 delayTime 的值，这常用在 delayTime 为随机数时，每次调用都是不同的随机数，如果不用 volatile，则第一次随机，然后固定下来，后边每次都一样了。

可以在 ini 中设置 delayTime 的随机分布，例如令 tic delayTime 为指数分布， toc delayTime 为正态分布，只取正数。

```ini
[General]
network = Tictoc1
seed-0-mt=532569  # 可以不设置，那么产生的随机序列是一样的，seed 为任意 32-bit value
Tictoc1.tic.delayTime = exponential(3s)
Tictoc1.toc.delayTime = truncnormal(3s,1s)
```

在 c++ 里面只需要用 par(delayTime) 获取延迟时间即可：

```c++ 
simtime_t delay = par("delayTime");
```

注意这里时间的类型是 simtime_t，这是 omnetpp.h 中定义的类型。

#### 模拟丢包

目标： toc 收到 msg 之后以一定概率丢掉 packet，如果 tic 长时间没有收到 toc 的回复，则 tic 需要再次发送（新生成 msg 发送，不是重新发送）

在 c++ 中每次发送都要设置 timeoutEvent msg，如果收到对方的 acknowledge，则取消计时。每次发送都重新计时。

这里为了条理清晰，用了完全独立的两个类： tic, toc。

在 ned 中:

```c++
simple Tic8
{
    parameters:
        @display("i=block/routing");
    gates:
        input in;
        output out;
}

simple Toc8
{
    parameters:
        @display("i=block/process");
    gates:
        input in;
        output out;
}

network Tictoc8
{
    submodules:
        tic: Tic8 {
            parameters:
                @display("i=,cyan");
        }
        toc: Toc8 {
            parameters:
                @display("i=,gold");
        }
    connections:
        tic.out --> {  delay = 100ms; } --> toc.in;
        tic.in <-- {  delay = 100ms; } <-- toc.out;
}
```

在 c++ 中

```c++
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

/**
 * we'll do something very common in telecommunication networks:
 * if the packet doesn't arrive within a certain period, we'll assume it
 * was lost and create another one. The timeout will be handled using
 * a self-message.
 */
class Tic8 : public cSimpleModule
{
  private:
    simtime_t timeout;  // timeout
    cMessage *timeoutEvent;  // holds pointer to the timeout self-message

  public:
    Tic8();
    virtual ~Tic8();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Tic8);

Tic8::Tic8()
{
    timeoutEvent = nullptr;
}

Tic8::~Tic8()
{
    cancelAndDelete(timeoutEvent);
}

void Tic8::initialize()
{
    // Initialize variables.
    timeout = 1.0;
    timeoutEvent = new cMessage("timeoutEvent");

    // Generate and send initial message.
    EV << "Sending initial message\n";
    cMessage *msg = new cMessage("tictocMsg");
    send(msg, "out");
    scheduleAt(simTime()+timeout, timeoutEvent);
}

void Tic8::handleMessage(cMessage *msg)
{
    if (msg == timeoutEvent) {
        // If we receive the timeout event, that means the packet hasn't
        // arrived in time and we have to re-send it.
        EV << "Timeout expired, resending message and restarting timer\n";
        cMessage *newMsg = new cMessage("tictocMsg");
        send(newMsg, "out");
        scheduleAt(simTime()+timeout, timeoutEvent);
    }
    else {  // message arrived
            // Acknowledgement received -- delete the received message and cancel
            // the timeout event.
        EV << "Timer cancelled.\n";
        cancelEvent(timeoutEvent);   // 取消发送，这是timeoutEvent 还在排队，等时间到了才发送
        delete msg;   // 把接受到的 msg 也删掉，每次都是发送新的，不管有没有 timeout

        // Ready to send another one.
        cMessage *newMsg = new cMessage("tictocMsg");
        send(newMsg, "out");
        scheduleAt(simTime()+timeout, timeoutEvent);
    }
}

/**
 * Sends back an acknowledgement -- or not.
 */
class Toc8 : public cSimpleModule
{
  protected:
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Toc8);

void Toc8::handleMessage(cMessage *msg)
{
    if (uniform(0, 1) < 0.1) {
        EV << "\"Losing\" message.\n";
        bubble("message lost");  // making animation more informative...
        delete msg;
    }
    else {
        EV << "Sending back same message as acknowledgement.\n";
        send(msg, "out");
    }
}
```

omnetpp.ini 中内容

```ini
[General]
network = Tictoc8
```

#### 丢包重新发送

前边的例子是丢包则产生一个新的包发送，现实中更多的时候是重新发送原来的包。

这里我们只需要修改 c++ 文件。

由于 toc 会 delete packet，tic 指向该 packet 的指针也没有用了，所以在发送的时候可以发送一个 copy，等收到回复之后，tic 这边再 delete 原始 msg。

```c++
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

/**
 * In the previous model we just created another packet if we needed to
 * retransmit. This is OK because the packet didn't contain much, but
 * in real life it's usually more practical to keep a copy of the original
 * packet so that we can re-send it without the need to build it again.
 */
class Tic9 : public cSimpleModule
{
  private:
    simtime_t timeout;  // timeout
    cMessage *timeoutEvent;  // holds pointer to the timeout self-message
    int seq;  // message sequence number
    cMessage *message;  // message that has to be re-sent on timeout

  public:
    Tic9();
    virtual ~Tic9();

  protected:
    virtual cMessage *generateNewMessage();
    virtual void sendCopyOf(cMessage *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Tic9);

Tic9::Tic9()
{
    timeoutEvent = message = nullptr;
}

Tic9::~Tic9()
{
    cancelAndDelete(timeoutEvent);
    delete message;
}

void Tic9::initialize()
{
    // Initialize variables.
    seq = 0;
    timeout = 1.0;
    timeoutEvent = new cMessage("timeoutEvent");

    // Generate and send initial message.
    EV << "Sending initial message\n";
    message = generateNewMessage();
    sendCopyOf(message);
    scheduleAt(simTime()+timeout, timeoutEvent);
}

void Tic9::handleMessage(cMessage *msg)
{
    if (msg == timeoutEvent) {
        // If we receive the timeout event, that means the packet hasn't
        // arrived in time and we have to re-send it.
        EV << "Timeout expired, resending message and restarting timer\n";
        sendCopyOf(message);
        scheduleAt(simTime()+timeout, timeoutEvent);
    }
    else {  // message arrived
            // Acknowledgement received!
        EV << "Received: " << msg->getName() << "\n";
        delete msg;

        // Also delete the stored message and cancel the timeout event.
        EV << "Timer cancelled.\n";
        cancelEvent(timeoutEvent);
        delete message;

        // Ready to send another one.
        message = generateNewMessage();
        sendCopyOf(message);
        scheduleAt(simTime()+timeout, timeoutEvent);
    }
}

cMessage *Tic9::generateNewMessage()
{
    // Generate a message with a different name every time.
    char msgname[20];
    sprintf(msgname, "tic-%d", ++seq);
    cMessage *msg = new cMessage(msgname);
    return msg;
}

void Tic9::sendCopyOf(cMessage *msg)
{
    // Duplicate message and send the copy.
    cMessage *copy = (cMessage *)msg->dup();
    send(copy, "out");
}

/**
 * Sends back an acknowledgement -- or not.
 */
class Toc9 : public cSimpleModule
{
  protected:
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Toc9);

void Toc9::handleMessage(cMessage *msg)
{
    if (uniform(0, 1) < 0.1) {
        EV << "\"Losing\" message " << msg << endl;
        bubble("message lost");
        delete msg;
    }
    else {
        EV << msg << " received, sending back an acknowledgement.\n";
        delete msg;
        send(new cMessage("ack"), "out");
    }
}
```

## Demo3: 多个 node 

#### 基本设置

目标： 一个 node 产生新的 msg，随机在 nodes 中传递，知道传到某个预先指定的 node 结束。

对 ned 的修改：

```c++
simple Txc10
{
    parameters:
        @display("i=block/routing");
    gates:
        input in[];  // declare in[] and out[] to be vector gates
        output out[];
}
```

每个 node 由于需要与多个 node 通信，input 和 output 应为多个。

构造网络 

```c++
network Tictoc10
{
    submodules:
        tic[6]: Txc10;  // 
    connections:
        tic[0].out++ --> {  delay = 100ms; } --> tic[1].in++;  // 这里的 ++ 是什么意思？
        tic[0].in++ <-- {  delay = 100ms; } <-- tic[1].out++;

        tic[1].out++ --> {  delay = 100ms; } --> tic[2].in++;
        tic[1].in++ <-- {  delay = 100ms; } <-- tic[2].out++;

        tic[1].out++ --> {  delay = 100ms; } --> tic[4].in++;
        tic[1].in++ <-- {  delay = 100ms; } <-- tic[4].out++;

        tic[3].out++ --> {  delay = 100ms; } --> tic[4].in++;
        tic[3].in++ <-- {  delay = 100ms; } <-- tic[4].out++;

        tic[4].out++ --> {  delay = 100ms; } --> tic[5].in++;
        tic[4].in++ <-- {  delay = 100ms; } <-- tic[5].out++;
}
```

网络结构如下（仿真启动之后才会呈现出来）：

![tictoc1](pic/tictoc1.png)

从这里可以看出来，ned 主要负责定义网络拓扑，通信过程中的动态细节主要在 c++ 中定义。

```c++

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

/**
 * Let's make it more interesting by using several (n) `tic' modules,
 * and connecting every module to every other. For now, let's keep it
 * simple what they do: module 0 generates a message, and the others
 * keep tossing it around in random directions until it arrives at
 * module 3.
 */
class Txc10 : public cSimpleModule
{
  protected:
    virtual void forwardMessage(cMessage *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Txc10);

void Txc10::initialize()
{
    if (getIndex() == 0) {
        // Boot the process scheduling the initial message as a self-message.
        char msgname[20];
        sprintf(msgname, "tic-%d", getIndex());  // sprintf 跟 prinft 类似，但不是打印到屏
       											 //幕，而是存到 msgname 这个 string 中。
        cMessage *msg = new cMessage(msgname);
        scheduleAt(0.0, msg);
    }
}

void Txc10::handleMessage(cMessage *msg)
{
    if (getIndex() == 3) {
        // Message arrived.
        EV << "Message " << msg << " arrived.\n";
        delete msg;
    }
    else {
        // We need to forward the message.
        forwardMessage(msg);
    }
}

void Txc10::forwardMessage(cMessage *msg)
{
    // In this example, we just pick a random gate to send it on.
    // We draw a random number between 0 and the size of gate `out[]'.
    int n = gateSize("out");   // 这些都是 omnetpp 里面的函数
    int k = intuniform(0, n-1);

    EV << "Forwarding message " << msg << " on port out[" << k << "]\n";
    send(msg, "out", k);   // 这样是从编号为 k 的 out 口发送出去
}
```

#### channel delay

原本 ned 文件中 delay 是这样设置的：

```c++
connections:
tic[0].out++ --> {  delay = 100ms; } --> tic[1].in++;
tic[0].in++ <-- {  delay = 100ms; } <-- tic[1].out++;

tic[1].out++ --> {  delay = 100ms; } --> tic[2].in++;
tic[1].in++ <-- {  delay = 100ms; } <-- tic[2].out++;

tic[1].out++ --> {  delay = 100ms; } --> tic[4].in++;
tic[1].in++ <-- {  delay = 100ms; } <-- tic[4].out++;

tic[3].out++ --> {  delay = 100ms; } --> tic[4].in++;
tic[3].in++ <-- {  delay = 100ms; } <-- tic[4].out++;

tic[4].out++ --> {  delay = 100ms; } --> tic[5].in++;
tic[4].in++ <-- {  delay = 100ms; } <-- tic[5].out++;
```

既然大家都是一样的 delay，能不能以类似变量的形式定义一下，这样方便统一修改。

网络中在  type 属性下，新定义一个 Channel 变量，只设置他的 delay 即可：

```c++
network Tictoc11
{
    types:
        channel Channel extends ned.DelayChannel {
            delay = 100ms;
        }
    submodules:
```

然后应用在 connection 中

```c++
connections:
tic[0].out++ --> Channel --> tic[1].in++;
tic[0].in++ <-- Channel <-- tic[1].out++;

tic[1].out++ --> Channel --> tic[2].in++;
tic[1].in++ <-- Channel <-- tic[2].out++;

tic[1].out++ --> Channel --> tic[4].in++;
tic[1].in++ <-- Channel <-- tic[4].out++;

tic[3].out++ --> Channel --> tic[4].in++;
tic[3].in++ <-- Channel <-- tic[4].out++;

tic[4].out++ --> Channel --> tic[5].in++;
tic[4].in++ <-- Channel <-- tic[5].out++;
```

#### 采用双向通讯 gate

前边所有通讯设置都是有来有回的，即双向通讯，实际上系统就有专门的双向通讯 gate，不需要 input, output 这样略显冗余的定义。

ned 中 module 修改如下：

```c++
simple Txc12
{
    parameters:
        @display("i=block/routing");
    gates:
        inout gate[];  // declare two way connections
}
```

这样 connection 就可以更简单的定义：

```c++
connections:
tic[0].gate++ <--> Channel <--> tic[1].gate++;
tic[1].gate++ <--> Channel <--> tic[2].gate++;
tic[1].gate++ <--> Channel <--> tic[4].gate++;
tic[3].gate++ <--> Channel <--> tic[4].gate++;
tic[4].gate++ <--> Channel <--> tic[5].gate++;
```

在 c++ 中的修改也仅仅是将 input, output 相关的地方用双向 gate 代替

```c++
void Txc10::forwardMessage(cMessage *msg)
{
    // In this example, we just pick a random gate to send it on.
    // We draw a random number between 0 and the size of gate `out[]'.
    int n = gateSize("gate");   // 这些都是 omnetpp 里面的函数
    int k = intuniform(0, n-1);

    EV << "Forwarding message " << msg << " on gate[" << k << "]\n";
    send(msg, "gate$o", k);   // 这样是从编号为 k 的 gate 出口发送出去
    						  // $o and $i 分别指代 gate 的出口和入口
}
```

#### 自定义 msg 类型

由于 msg 里面有很多设置，所以不推荐直接手写 msg 类型，而是从 cMessage 衍生出来，这样很多函数，比如 getter, setter 函数都直接继承了。

右键点击 project 栏，新建 msg 文件 tictoc13.msg 如下：

```c++
message TicTocMsg13
{
    int source;
    int destination;
    int hopCount = 0;
}
```

由于是从 cMessage 继承的，原本普通 msg 的内容还是有的，比如生成 msg 时，其内部保存的信息还是有的

```c++
TicTocMsg13 *msg = new TicTocMsg13(msgname); // 还是通过这种方式存放 msg 内容。
```



在 IDE 中编译，得到 tictoc13_m.cc 和 tictoc13_m.h 文件。其中 tictoc13_m.h 中包含了 TicTocMsg13  class

在 c++ 中调用 tictoc13_m.h 文件，就像普通的头文件一样

```c++
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

// Include a generated file: the header file created from tictoc13.msg.
// It contains the definition of the TictocMsg10 class, derived from
// cMessage.
#include "tictoc13_m.h"

class Txc13 : public cSimpleModule
{
  protected:
    virtual TicTocMsg13 *generateMessage();
    virtual void forwardMessage(TicTocMsg13 *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Txc13);

void Txc13::initialize()
{
    // Module 0 sends the first message
    // 这个 initialize() 只在初始节点中起作用
    if (getIndex() == 0) {
        // Boot the process scheduling the initial message as a self-message.
        TicTocMsg13 *msg = generateMessage();
        scheduleAt(0.0, msg);
    }
}

void Txc13::handleMessage(cMessage *msg)
{
    TicTocMsg13 *ttmsg = check_and_cast<TicTocMsg13 *>(msg);

    if (ttmsg->getDestination() == getIndex()) {
        // Message arrived.
        EV << "Message " << ttmsg << " arrived after " << ttmsg->getHopCount() << " hops.\n";
        bubble("ARRIVED, starting new one!");
        delete ttmsg;

        // Generate another one.
        EV << "Generating another message: ";
        TicTocMsg13 *newmsg = generateMessage();
        EV << newmsg << endl;
        forwardMessage(newmsg);
    }
    else {
        // We need to forward the message.
        forwardMessage(ttmsg);
    }
}

TicTocMsg13 *Txc13::generateMessage()
{
    // Produce source and destination addresses.
    int src = getIndex();  // our module index
    int n = getVectorSize();  // module vector size
    int dest = intuniform(0, n-2);
    if (dest >= src)
        dest++;

    char msgname[20];
    sprintf(msgname, "tic-%d-to-%d", src, dest);

    // Create message object and set source and destination field.
    TicTocMsg13 *msg = new TicTocMsg13(msgname);
    msg->setSource(src);
    msg->setDestination(dest);
    return msg;  // 生成一个 TicTocMsg13 类型的 msg
}

void Txc13::forwardMessage(TicTocMsg13 *msg)
{
    // Increment hop count.
    msg->setHopCount(msg->getHopCount()+1);

    // Same routing as before: random gate.
    int n = gateSize("gate");  // 返回这个节点维护的 gate 数目，而不是整个网络中的 gate 数目
    int k = intuniform(0, n-1);

    EV << "Forwarding message " << msg << " on gate[" << k << "]\n";
    send(msg, "gate$o", k);
}
```

## Demo4: 加入统计分析

#### 增加变量计数

目标：记录每个节点 发送 和 接收 了多少 packet。这里只有 src 和 destination 节点才算是 初始发送和接收到，中转的不算。

c++ 文件中额外定义两个变量

```c++
class Txc14 : public cSimpleModule
{
  private:
    long numSent;
    long numReceived;

  protected:
```

在 initialize( ) 函数中赋初值，并 WATCH

```c++
void Txc14::initialize()
{
    // Module 0 sends the first message
    numSent = 0;
    numReceived = 0;
    WATCH(numSent);
    WATCH(numReceived);
```

然后在 0 号 节点中由于始发节点，发送数值 + 1。

```c++
if (getIndex() == 0) {
    // Boot the process scheduling the initial message as a self-message.
    TicTocMsg13 *msg = generateMessage();
    numSent++;
    scheduleAt(0.0, msg);
}  // 初始 0 号节点发送数据
```

随后的传递中 handleMessage，到达 destination 之后，receive +1, 然后 sent +1 ，发送新的 packet

```c++
if (ttmsg->getDestination() == getIndex()) {
    // Message arrived.
    EV << "Message " << ttmsg << " arrived after " << ttmsg->getHopCount() << " hops.\n";
    numReceived++;
    bubble("ARRIVED, starting new one!");
    delete ttmsg;

    // Generate another one.
    EV << "Generating another message: ";
    TicTocMsg13 *newmsg = generateMessage();
    EV << newmsg << endl;
    forwardMessage(newmsg);
    numSent++;
}
```

启动仿真之后，在 inspector 窗口中搜索统计变量，可以得到如下直观的统计信息

![tictoc_num](pic/tictoc_num.png)

#### 计数信息显示在节点顶部

在  c++ 文件中添加相应的函数

```c++
protected:
    virtual TicTocMsg13 *generateMessage();
    virtual void forwardMessage(TicTocMsg13 *msg);
    virtual void refreshDisplay() const override;  // 负责显示
```

其中 refreshDisplay( ) 函数具体定义如下：

```c++
void Txc14::refreshDisplay() const
{
    char buf[40];
    sprintf(buf, "rcvd: %ld sent: %ld", numReceived, numSent);
    getDisplayString().setTagArg("t", 0, buf);
}
```

最后显示效果如下：

![tictoc_num1](pic/tictoc_num1.png)

#### 统计 packet hop 数是

可以设置程序发送某些感兴趣的信号，接收之后统计即可。

在 ned 中添加 signal 相关的变量

```c++
simple Txc16
{
    parameters:
        @signal[arrival](type="long");   // 信号定义
        @statistic[hopCount](title="hop count"; source="arrival"; record=vector,stats; interpolationmode=none);  // source 表示我们的统计需要对应的 signal
        				  // record 表示 what should be done with the data
        				  // vector 表示每个值都要保存，stats 表示计算统计量

        @display("i=block/routing");
    gates:
```

- vector 以时间为横坐标，记录数据
  - scalar 在仿真结束后记录整合的结果

c++ 中也对应定义信号变量

```c++
class Txc16 : public cSimpleModule
{
  private:
    simsignal_t arrivalSignal;

  protected:
```

在 initialize( ) 函数中注册该 信号

```c++
arrivalSignal = registerSignal("arrival");
```

在 handelMessage 中定义发送信号的内容

```c++
int hopcount = ttmsg->getHopCount();
// send a signal
emit(arrivalSignal, hopcount);  // 把 变量 hopcount 经过 arrialSignal 发送出去
```

最后，在 ini 中可以进一步设定感兴趣的 node

```ini
[Config Tictoc16]
network = Tictoc16
**.tic[1].hopCount.result-recording-modes = +histogram  // 只对 1 号 node 感兴趣
**.tic[0..2].hopCount.result-recording-modes = -vector // 不考虑 0, 1, 2
```

所有的数据都保存在了 result 文件夹中，可以在 IDE 中双击打开 vec, sca 文件，在 browse data 界面画图进行可视化分析。

## Demo5: 中心节点数目对 hop 的影响

目标：中间节点数量为可变的，研究数量与 平均 hop 的关系

![omnetpp_central1](pic/omnetpp_central1.png)

ned 文件中首先设定中心节点数目变量：

```c++
network TicToc18
{
    parameters:
        int numCentralNodes = default(2);  // 设定为变量
    types:
        channel Channel extends ned.DelayChannel {
            delay = 100ms;
        }
    submodules:
        tic[numCentralNodes+4]: Txc18;  // 节点数目由变量而定
```

然后设定节点连接方式：

```c++
connections:
// connect the 2 nodes in one side to the central nodes
tic[0].gate++ <--> Channel <--> tic[2].gate++;
tic[1].gate++ <--> Channel <--> tic[2].gate++;
// connect the central nodes together
for i=2..numCentralNodes+1 {    // x..y 这就表示了从 x, x+1, x+2, ... 到 y
    tic[i].gate++ <--> Channel <--> tic[i+1].gate++;
}
// connect the 2 nodes on the other side to the central nodes
tic[numCentralNodes+2].gate++ <--> Channel <--> tic[numCentralNodes+1].gate++;
tic[numCentralNodes+3].gate++ <--> Channel <--> tic[numCentralNodes+1].gate++;
```

在 ini 文件中设定 central node 的变量值

```ini
*.numCentralNodes = ${N=2..100 step 2}
```

一般像这种要多次仿真统计结果的情况，就不需要 GUI 仿真界面了，可以将 user interface 选为 Cmdenv 。

为了结果的可靠，应该在不同的随机数下多次仿真

```ini
repeat = 4
```



后记：当在 IDE 中执行 run 命令时，对应的 cmd 命令为

``` bash
cd /home/qpliu/plexe/omnetpp/Liu_ws/tictoc
./tictoc -m -u Qtenv omnetpp.ini
```

启动之后会有后续处理流程的记录：

```bash
Setting up Qtenv...

Loading NED files from .:  1

Loading images from './bitmaps': *: 0 
Loading images from './images': *: 0 
Loading images from '/home/qpliu/plexe/omnetpp/images': *: 0  abstract/*: 90  background/*: 4  block/*: 320  device/*: 195  logo/*: 1  maps/*: 9  misc/*: 70  msg/*: 55  old/*: 111  status/*: 28 
```

从这里可以看出来，NED 文件并没有放入编译好的文件，而是在程序运行时调用的。



# Veins

## 引言

Veins: Vehicles in Network Simulation

A suite of simulation models for vehicular networking.

包括两个核心部件：

1. 事件触发的网络模拟器 OMNeT++
2. 道路交通流模拟器 SUMO 

在仿真中，OMNeT++ 和 SUMO 平行地运行，两者之间通过 TCP 接口通信，使用的协议为标准的 Traffic Control Interface (TraCI). 

尽管 Veins 只需要设定少量参数就可以执行仿真任务，但是它的主要功能还是作为实验平台测试用户自己的算法程序。一般用法是用户编写上层的算法程序，Veins 负责底层实现以及仿真结果数据收集等。

当前稳定版本的 Veins 要求 SUMO 0.32 版本 + OMNet++ 5.3 版本。



## 安装 SUMO 0.32

从 [SUMO 官网](https://sourceforge.net/projects/sumo/files/sumo/)下载 older release 0.32 版本，解压缩，如果只有源文件，就到文件夹下编译一下：

```bash
./configure && make
```

注意：要将 sumo 所在的 bin 文件夹放入系统搜索路径 PATH 变量中



## 安装 OMNet++ 5.3

具体可参考前边 OMNET++ 小节的安装教程。



## 运行 Veins

先通过 sumo-launchd.py 开启 port 监听 request

```bash
./sumo-launchd.py -vv -c sumo-gui
```

-vv:  increase verbosity。默认只 log warning 和 error，不 log infos 和 debug. 这种情况下 log 所有

-q == quiet:  decrease verbosity。连 warning 和 error 都不 log 了。

-c  后跟启动 sumo 的命令



只要有连接进来，马上按照连接送过来的 launch config 文件，在可用的 port 开启 SUMO .

一般 launch config 文件内容如下：

```xml
<?xml version="1.0"?>
<launch>
	<copy file="demo.net.xml" />
	<copy file="demo.rou.xml" />
	<copy file="demo.sumo.cfg" type="config" />
</launch>
```



# Plexe

## 引言

包含两大块：

1. Plexe-Veins
2. Plexe-SUMO

当前版本兼容性

> - Plexe-Veins 2.1 (based on Veins 4.7)
> - Plexe-SUMO 2.1 (based on SUMO 0.32.0)
> - OMNeT++ 5.1.1



## 安装 plexe

### 安装依赖

```bash
sudo apt-get install build-essential gcc g++ bison flex perl python python3 qt5-default libqt5opengl5-dev tcl-dev tk-dev libxml2-dev zlib1g-dev default-jre doxygen graphviz libwebkitgtk-1.0
```



### 安装 OMNet++ 5.1.1

- 创建一个安装目录，例如 plexe

- 下载 [OMNeT++ 5.1.1](https://omnetpp.org/download/old) ，解压到 plexe 目录中。

- 将 OMNeT++ 加入系统路径

  ```bash
  echo 'export PATH=$PATH:~/plexe/omnetpp-5.1.1/bin' >> ~/.bashrc
  source ~/.bashrc
  ```

- 安装依赖

  ```
  sudo add-apt-repository ppa:ubuntugis/ppa
  sudo apt-get update
  sudo apt-get install openscenegraph-plugin-osgearth libosgearth-dev
  sudo apt-get install openmpi-bin libopenmpi-dev
  sudo apt-get install libpcap-dev
  ```

- Build OMNeT++

  ```bash
  cd ~/plexe/omnetpp-5.1.1
  ./configure
  make -j 8
  ```

### 安装 plexe-vein 和 plexe-sumo

- git clone 两个文件夹

  ```bash
  cd ~/plexe
  git clone https://github.com/michele-segata/plexe-veins.git
  git clone https://github.com/michele-segata/plexe-sumo.git
  ```

- build plexe-vein

  ```bash
  cd plexe-veins
  git checkout plexe-2.1
  ./configure
  make -j 8 MODE=release
  ```

- build plexe-sumo

  ```bash
  cd ~/plexe/plexe-sumo
  git checkout plexe-2.1
  mkdir build-release
  cd build-release
  cmake -DCMAKE_BUILD_TYPE=Release ..
  make -j 8
  echo 'export PATH=~/plexe/plexe-sumo/bin:$PATH' >> ~/.bashrc
  ```

### 安装 R

如果要用 plexe 提供的工具画图，则还需要安装 R 

- 首先安装基本的 R ： [参考网址](https://www.digitalocean.com/community/tutorials/how-to-install-r-on-ubuntu-16-04-2) 

- 下载处理 OMNeT++ 运行结果的 Package

  ```bash
  wget http://plexe.car2x.org/download/omnetpp_0.7-1.tar.gz
  ```

  启动 R，安装下列 package：

  ```bash
  install.packages(c('ggplot2', 'reshape2', 'data.table'))
  install.packages("omnetpp_0.7-1.tar.gz", repos=NULL)
  ```



## plexe 的 python API

API for the platooning functions provided by Plexe in SUMO.

### 安装与卸载

下载  repo : https://github.com/michele-segata/plexe-pyapi

然后用如下 pip 命令安装

```bash
pip install --user .
```

卸载命令为

```bash
pip uninstall PlexeAPI
```



### Demos

在 examples 中有多个 demo，可以直接运行，包括

- brakedemo.py: platoon 协同刹车
- joindemo.py: 从中间加入 platoon
- enginedemo.py: 三车( alfa-147,  audi-r8,  bugatti-veyron ) 启动性能对比
- platoon-lane-change-test.py: platoon 协同换道超车
- overtake-and-keep-right-test.py: 单辆车超车

另外，还可以显示汽车仪表盘信息 dashboard，包括引擎转速、档位、车速、加速度。

不过需要安装 PyQt5

```bash
pip install --user PyQt5
```



### demo 程序解读

在官方给的上述几个 demo 文件基础上，我们可以构造感兴趣的交通场景，测试自己的车联网和车路协同策略的效果。

下边先详细分析一下 brakedemo.py 文件和 joindemo.py 文件，里面包含了很多典型场景（如何构造 platoon, 如何实现刹车、platoon 分裂、获取其他车辆信息等），然后自己编写一个简单的文件，实现在交通路口车辆与交通信号灯的通信。

#### brakedemo.py

在 CACC 环境下，platoon 实现协同刹车，即使跟车距离比较小，也不会发生碰撞。

![brakedemo](pic/brakedemo.gif)

```python
import os
import sys
import random

# 调用 utils module，里面包含了 platooning 的中层实现函数
from utils import add_platooning_vehicle, start_sumo, running, communicate

# 确保路径设置正确，python 能够搜索到 traci module
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")
import traci

# 调用 plexe module, 包含了 platooning 的底层实现函数
from plexe import Plexe, ACC, CACC, RPM, GEAR, RADAR_DISTANCE

# vehicle length
LENGTH = 4
# inter-vehicle distance
DISTANCE = 5
# cruising speed
# 120 km/h = 33.3 m/s
SPEED = 120/3.6  
# distance between multiple platoons
# 不同 Platoon 之间的距离，在该 demo 中只有一个 platoon
PLATOON_DISTANCE = SPEED * 1.5 + 2
# vehicle who starts to brake
# 最初开始刹车的是头车，编号为 "v.0.0"，即 0 号 platoon 中的 0 号车辆
# 由于只有一个 platoon，所以所有车辆编号都是 "v.0.* "的形式
BRAKING_VEHICLE = "v.0.0"

# 构造 platoon，返回 platoon 中车辆之间的通信连接关系
def add_vehicles(plexe, n, n_platoons, real_engine=False):
    """
    Adds a set of platoons of n vehicles each to the simulation
    :param plexe: API instance
    :param n: number of vehicles of the platoon
    :param n_platoons: number of platoons
    :param real_engine: set to true to use the realistic engine model,
    false to use a first order lag model
    :return: returns the topology of the platoon, i.e., a dictionary which
    indicates, for each vehicle, who is its leader and who is its front
    vehicle. The topology can the be used by the data exchange logic to
    automatically fetch data from leading and front vehicle to feed the CACC
    """
    # add a platoon of n vehicles
    # topology 中以 dictionary 的形式存储车辆之间的通信拓扑结构，包括每辆车的前车和头车编号
    topology = {}
    p_length = n * LENGTH + (n - 1) * DISTANCE
    for p in range(n_platoons):
        for i in range(n):
            vid = "v.%d.%d" % (p, i)
            # 调用 utils module 中的函数，将车辆编排成 platoon 的形式
            add_platooning_vehicle(plexe, vid, (n-p+1) *
                                   (p_length + PLATOON_DISTANCE) + (n-i+1) *
                                   (DISTANCE+LENGTH), 0, SPEED, DISTANCE,
                                   real_engine)
            
            # 将车辆保持在 lane 0 ，并忽略安全距离限制
            plexe.set_fixed_lane(vid, 0, False)
            
            # 设置车辆速度模式。车辆的速度有 5 个影响因素
            # https://sumo.dlr.de/wiki/TraCI/Change_Vehicle_State#speed_mode_.280xb3.29
            # 1. safe speed
            # 2. max accel
            # 3. max speed
            # 4. right of way at intersections
            # 5. brake hard to avoid passing red light
            # 如果全都不考虑，则设置为 [0, 0, 0, 0, 0] = 0, 此时，车速完全由 traci 控制
            # 如果全都考虑，则设置为 [1, 1, 1, 1, 1] = 31
            traci.vehicle.setSpeedMode(vid, 0)
            
            # 在 platoon 中头车采用 adaptive cruise control 的控制方式
            # 后边的跟车采用 cooperative adative cruise control 的控制方式
            plexe.use_controller_acceleration(vid, False)
            if i == 0:
                plexe.set_active_controller(vid, ACC)
            else:
                plexe.set_active_controller(vid, CACC)
            if i > 0:
                topology[vid] = {"front": "v.%d.%d" % (p, i - 1),
                                 "leader": "v.%d.0" % p}
            else:
                topology[vid] = {}
    return topology


def main(demo_mode, real_engine, setter=None):    
    # used to randomly color the vehicles
    # 具体着色是在 utils module 的 add_platooning_vehicle 函数中实现的
    random.seed(1)
    
    # 运行 SUMO 的配置文件，后边的参数 False / True 表示 SUMO server 是否已经在运行了。
    # 若为 False，则打开 SUMO 并加载配置文件
    # 若为 True，则重新加载配置文件
    # freeway.sumo.cfg 中仿真步长为 0.01s
    start_sumo("cfg/freeway.sumo.cfg", False)
    
    # 以下设置可以使得每次 traci.simulationStep() 之后都调用一次 plexe 
    plexe = Plexe()
    traci.addStepListener(plexe)
    
    step = 0
    topology = dict()
    min_dist = 1e6

    # 主循环
    while running(demo_mode, step, 1500):

        # when reaching 15 seconds, reset the simulation when in demo_mode
        if demo_mode and step == 1500:
            print("Min dist: %f" % min_dist)
            start_sumo("cfg/freeway.sumo.cfg", True)
            step = 0
            random.seed(1)

        traci.simulationStep()

        # 仿真初始化阶段，构造含有 8 辆车的 platoon
        # 设置 GUI 中画面在整个仿真过程中始终聚焦在 v.0.0， 即头车
        # 镜头缩放参数 20000, 这个可以根据具体场景设置，使得镜头既不会拉的太近，也不会拉的太远。
        if step == 0:
            # create vehicles and track the braking vehicle
            topology = add_vehicles(plexe, 8, 1, real_engine)
            traci.gui.trackVehicle("View #0", BRAKING_VEHICLE)
            traci.gui.setZoom("View #0", 20000)
            
        # 每隔 10 步车辆之间通信一次，获得其他车辆的位置、速度、加速度等信息
        if step % 10 == 1:
            # simulate vehicle communication every 100 ms
            communicate(plexe, topology)
            
        # 是否使用 plexe 中改进的更加逼真的引擎模型
        if real_engine and setter is not None:
            # if we are running with the dashboard, update its values
            tracked_id = traci.gui.getTrackedVehicle("View #0")
            if tracked_id != "":
                ed = plexe.get_engine_data(tracked_id)
                vd = plexe.get_vehicle_data(tracked_id)
                setter(ed[RPM], ed[GEAR], vd.speed, vd.acceleration)
        # 在第 500 步时头车刹车，由于是 车联网 状态，所以其他车辆也会及时得到头车信息，几乎同步刹车，即使跟车距离很小，也不会追尾。这就是 车辆网 的优势。
        if step == 500:
            plexe.set_fixed_acceleration(BRAKING_VEHICLE, True, -6)
            
        # 记录在整个仿真过程中车辆间隔的最小距离，有需要的话可以随后进行分析
        if step > 1:
            radar = plexe.get_radar_data("v.0.1")
            if radar[RADAR_DISTANCE] < min_dist:
                min_dist = radar[RADAR_DISTANCE]

        step += 1

    traci.close()


if __name__ == "__main__":
    main(True, True)
```

其中，plexe.set_fixed_acceleration(BRAKING_VEHICLE, True, -6) 应该是对应了 C++ 中的void setFixedAcceleration(int activate, double acceleration) 函数，如果设置 activate=1，则按照设定的加速度运行，如果 activate=0，则该函数不起作用，后边给的加速度也被忽略，完全采用 controller —— ACC 或者 CACC 计算出来的加速度。[参考](<http://plexe.car2x.org/api/>)

#### joindemo.py

单个车辆从中部加入 platoon 的过程

![joindemo](pic/joindemo.gif)

```python
import os
import sys
import random
from utils import add_platooning_vehicle, communicate, get_distance, \
    start_sumo, running

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

import traci
from plexe import Plexe, ACC, CACC, FAKED_CACC, RPM, GEAR, ACCELERATION, SPEED

# vehicle length
LENGTH = 4
# inter-vehicle distance
DISTANCE = 5
# inter-vehicle distance when leaving space for joining
JOIN_DISTANCE = DISTANCE * 2
# cruising speed
SPEED = 120 / 3.6

# maneuver states:
GOING_TO_POSITION = 0
OPENING_GAP = 1
COMPLETED = 2

# maneuver actors
# 这里只考虑 1 个 platoon，其中车辆编号以此为 v.0, v.1, v.2, ...
LEADER = "v.0"
JOIN_POSITION = 3
FRONT_JOIN = "v.%d" % (JOIN_POSITION - 1)
BEHIND_JOIN = "v.%d" % JOIN_POSITION
N_VEHICLES = 8
JOINER = "v.%d" % N_VEHICLES


# 该程序与 brakedemo.py 中类似，向场景中加入车辆，这里包含 1 个 platoon 和 1 个单独的车辆
# 返回 platoon 的 topology，这个 topology 中不包含后加入的单个车辆
def add_vehicles(plexe, n, real_engine=False):
    """
    Adds a platoon of n vehicles to the simulation, plus an additional one
    farther away that wants to join the platoon
    :param plexe: API instance
    :param n: number of vehicles of the platoon
    :param real_engine: set to true to use the realistic engine model,
    false to use a first order lag model
    :return: returns the topology of the platoon, i.e., a dictionary which
    indicates, for each vehicle, who is its leader and who is its front
    vehicle. The topology can the be used by the data exchange logic to
    automatically fetch data from leading and front vehicle to feed the CACC
    """
    # add a platoon of n vehicles
    topology = {}
    for i in range(n):
        vid = "v.%d" % i
        add_platooning_vehicle(plexe, vid, (n - i + 1) * (DISTANCE + LENGTH) +
                               50, 0, SPEED, DISTANCE, real_engine)
        plexe.set_fixed_lane(vid, 0, safe=False)
        traci.vehicle.setSpeedMode(vid, 0)
        if i == 0:
            plexe.set_active_controller(vid, ACC)
        else:
            plexe.set_active_controller(vid, CACC)
        if i > 0:
            topology[vid] = {"front": "v.%d" % (i - 1), "leader": LEADER}
    # add a vehicle that wants to join the platoon
    vid = "v.%d" % n
    add_platooning_vehicle(plexe, vid, 10, 1, SPEED, DISTANCE, real_engine)
    plexe.set_fixed_lane(vid, 1, safe=False)
    traci.vehicle.setSpeedMode(vid, 0)
    plexe.set_active_controller(vid, ACC)
    plexe.set_path_cacc_parameters(vid, distance=JOIN_DISTANCE)
    return topology

# 该函数的作用是通过改变待加入车辆的通讯拓扑，令其到达指定位置，准备变道加入 platoon
def get_in_position(plexe, jid, fid, topology):
    """
    Makes the joining vehicle get close to the join position. This is done by
    changing the topology and setting the leader and the front vehicle for
    the joiner. In addition, we increase the cruising speed and we switch to
    the "fake" CACC, which uses a given GPS distance instead of the radar
    distance to compute the control action
    :param plexe: API instance
    :param jid: id of the joiner
    :param fid: id of the vehicle that will become the predecessor of the joiner
    :param topology: the current platoon topology
    :return: the modified topology
    """
    topology[jid] = {"leader": LEADER, "front": fid}
    plexe.set_cc_desired_speed(jid, SPEED + 15)
    plexe.set_active_controller(jid, FAKED_CACC)
    return topology

# 该函数的作用是当待加入车辆到达指定位置时，后车为其留出足够的距离
# 在此过程中，platoon 分裂，后车成为新的 leader，带领它的 follower 减速，为待加入车辆留出足够的空间
def open_gap(plexe, vid, jid, topology, n):
    """
    Makes the vehicle that will be behind the joiner open a gap to let the
    joiner in. This is done by creating a temporary platoon, i.e., setting
    the leader of all vehicles behind to the one that opens the gap and then
    setting the front vehicle of the latter to be the joiner. To properly
    open the gap, the vehicle leaving space switches to the "fake" CACC,
    to consider the GPS distance to the joiner
    :param plexe: API instance
    :param vid: vehicle that should open the gap
    :param jid: id of the joiner
    :param topology: the current platoon topology
    :param n: total number of vehicles currently in the platoon
    :return: the modified topology
    """
    index = int(vid.split(".")[1])
    for i in range(index + 1, n):
        # temporarily change the leader
        topology["v.%d" % i]["leader"] = vid
    # the front vehicle if the vehicle opening the gap is the joiner
    topology[vid]["front"] = jid
    plexe.set_active_controller(vid, FAKED_CACC)
    plexe.set_path_cacc_parameters(vid, distance=JOIN_DISTANCE)
    return topology

# 该函数的作用是，完成 join 操作后，修改通讯网络拓扑，回归最初的 CACC 情形，都以头车作为 leader
def reset_leader(vid, topology, n):
    """
    After the maneuver is completed, the vehicles behind the one that opened
    the gap, reset the leader to the initial one
    :param vid: id of the vehicle that let the joiner in
    :param topology: the current platoon topology
    :param n: total number of vehicles in the platoon (before the joiner)
    :return: the modified topology
    """
    index = int(vid.split(".")[1])
    for i in range(index + 1, n):
        # restore the real leader
        topology["v.%d" % i]["leader"] = LEADER
    return topology


def main(demo_mode, real_engine, setter=None):
    # used to randomly color the vehicles
    random.seed(1)
    start_sumo("cfg/freeway.sumo.cfg", False)
    plexe = Plexe()
    traci.addStepListener(plexe)
    step = 0
    state = GOING_TO_POSITION
    while running(demo_mode, step, 6000):

        # when reaching 60 seconds, reset the simulation when in demo_mode
        if demo_mode and step == 6000:
            start_sumo("cfg/freeway.sumo.cfg", True)
            step = 0
            state = GOING_TO_POSITION
            random.seed(1)

        traci.simulationStep()

        if step == 0:
            # create vehicles and track the joiner
            topology = add_vehicles(plexe, N_VEHICLES, real_engine)
            traci.gui.trackVehicle("View #0", JOINER)
            traci.gui.setZoom("View #0", 20000)
        if step % 10 == 1:
            # simulate vehicle communication every 100 ms
            communicate(plexe, topology)
         
        # 程序执行 100 步 (0.01s * 100 = 1s) 后，令单个车辆驶近目标位置
        if step == 100:
            # at 1 second, let the joiner get closer to the platoon
            topology = get_in_position(plexe, JOINER, FRONT_JOIN, topology)
            
        # 当车辆达到指定位置，令后车留出足够的空间，以便汇入。
        if state == GOING_TO_POSITION and step > 0:
            # when the distance of the joiner is small enough, let the others
            # open a gap to let the joiner enter the platoon
            if get_distance(plexe, JOINER, FRONT_JOIN) < JOIN_DISTANCE + 1:
                state = OPENING_GAP
                topology = open_gap(plexe, BEHIND_JOIN, JOINER, topology,
                                    N_VEHICLES)
                
        # 当空间足够大时，完成汇入
        if state == OPENING_GAP:
            # when the gap is large enough, complete the maneuver
            if get_distance(plexe, BEHIND_JOIN, FRONT_JOIN) > \
                    2 * JOIN_DISTANCE + 2:
                state = COMPLETED
                plexe.set_fixed_lane(JOINER, 0, safe=False)
                plexe.set_active_controller(JOINER, CACC)
                plexe.set_path_cacc_parameters(JOINER, distance=DISTANCE)
                plexe.set_active_controller(BEHIND_JOIN, CACC)
                plexe.set_path_cacc_parameters(BEHIND_JOIN, distance=DISTANCE)
                topology = reset_leader(BEHIND_JOIN, topology, N_VEHICLES)
        if real_engine and setter is not None:
            # if we are running with the dashboard, update its values
            tracked_id = traci.gui.getTrackedVehicle("View #0")
            if tracked_id != "":
                ed = plexe.get_engine_data(tracked_id)
                vd = plexe.get_vehicle_data(tracked_id)
                setter(ed[RPM], ed[GEAR], vd.speed, vd.acceleration)

        step += 1

    traci.close()


if __name__ == "__main__":
    main(True, False)

```

### Intersection

以上 demo 涵盖了很多基本场景模块，如构造编队、队列分割、添加新 leader，换道、刹车等。基于这些模块，我们可以构造不同的仿真场景，测试自己的车联网和车路协同策略。

假设一个 platoon 通过 intersection 的场景。交通灯可以向一定范围内的车辆发送信号，其中包含当前交通灯状态以及剩余时间等信息，platoon leader 基于这些信息作出决策，platoon 中哪些车辆可以通过，哪些车辆需要提前刹车避免闯红灯。

![intersection](pic/intersection.gif)

创建 intersection.py 文件，写入以下程序。该程序是在 brakedemo.py 基础上做的简单修改，基本实现了上述 platoon 过交通路口的场景。程序中添加了注释，应该不难理解其中的功能。

需要注意的是，这里的 sumo 网络和配置文件不再是之前的 freeway，而是需要自己设计，加入交通灯等元素。

在车辆颜色方面，为了区分 leader 和 follower，我对 utils 中的车辆颜色做了一些修改，不再使用 random，而是将 leader 设定为红色， follower 设定为黄色。

platoon leader 在进入交通灯 100m 范围内就可以获得交通灯信号信息，知道当前状态和剩余时间，然后进行后续的 platoon split 操作。前部的 platoon 会正常行驶通过路口，后部的 platoon 在新的 leader 带领下刹车，停在路口处，等待下个绿灯再通行。

```python
import os
import sys
from math import floor

import random
from utils import add_platooning_vehicle, start_sumo, running, communicate

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")
import traci
from plexe import Plexe, ACC, CACC, RPM, GEAR, RADAR_DISTANCE

# vehicle length
LENGTH = 4
# inter-vehicle distance
DISTANCE = 5
# platoon size
PLATOON_SIZE = 8
# number of platoon
PLATOON_NUM = 1
# cruising speed
SPEED = 33 # m/s
# distance between multiple platoons
PLATOON_DISTANCE = SPEED * 1.5 + 2
# vehicle who starts to brake
BRAKING_VEHICLE = "v.0.0"
# the original leader ID
ORI_LEADER_ID ="v.0.0"
# traffic light ID
TL_ID = "tl_0"
# range of traffic light broadcast
RANGE = 100 
# traffic light position
TL_POS = 558



def add_vehicles(plexe, n, n_platoons, real_engine=False):
    """
    Adds a set of platoons of n vehicles each to the simulation
    :param plexe: API instance
    :param n: number of vehicles of the platoon
    :param n_platoons: number of platoons
    :param real_engine: set to true to use the realistic engine model,
    false to use a first order lag model
    :return: returns the topology of the platoon, i.e., a dictionary which
    indicates, for each vehicle, who is its leader and who is its front
    vehicle. The topology can the be used by the data exchange logic to
    automatically fetch data from leading and front vehicle to feed the CACC
    """
    # add a platoon of n vehicles
    topology = {}
    p_length = n * LENGTH + (n - 1) * DISTANCE
    for p in range(n_platoons):
        for i in range(n):
            vid = "v.%d.%d" % (p, i)
            add_platooning_vehicle(plexe, vid, 200 + (n-i+1) * (DISTANCE+LENGTH), 0, SPEED, DISTANCE, real_engine)
            plexe.set_fixed_lane(vid, 0, False)
            traci.vehicle.setSpeedMode(vid, 0)
            plexe.use_controller_acceleration(vid, False)
            if i == 0:
                plexe.set_active_controller(vid, ACC)
            else:
                plexe.set_active_controller(vid, CACC)
            if i > 0:
                topology[vid] = {"front": "v.%d.%d" % (p, i - 1),
                                 "leader": "v.%d.0" % p}
            else:
                topology[vid] = {}
    return topology


def main(demo_mode, real_engine, setter=None):
    # used to randomly color the vehicles
    random.seed(1)
    start_sumo("cfg/intersection/intersection.sumo.cfg", False)
    plexe = Plexe()
    traci.addStepListener(plexe)
    step = 0
    topology = dict()
    min_dist = 1e6
    split = False

    while running(demo_mode, step, 3000):

        traci.simulationStep()

        if step == 0:
            # create vehicles and track the braking vehicle
            topology = add_vehicles(plexe, PLATOON_SIZE, PLATOON_NUM, real_engine)
            tracked_veh = "v.0.%d" %(PLATOON_SIZE-1)
            traci.gui.trackVehicle("View #0", tracked_veh)
            traci.gui.setZoom("View #0", 2000)

        # when the leader is 100m away from the traffic light, it will receive the current phase of the traffic light
        # Accordingly, it computes which vehicles could pass safely.
        leader_data = plexe.get_vehicle_data(ORI_LEADER_ID)
        # the structure of vehicle data is defined in vehicle_data.py file in plexe folder
        # self.acceleration,  self.speed, self.pos_x, self.pos_y 
        if leader_data.pos_x >= TL_POS - RANGE and not split:
            current_phase = traci.trafficlight.getPhase(TL_ID)
            if current_phase == 0:
                absolute_time = traci.trafficlight.getNextSwitch(TL_ID)
                time_left = absolute_time - traci.simulation.getTime()
                new_leader = floor((leader_data.speed * time_left - RANGE)/(LENGTH + DISTANCE))
                new_leader_id = "v.0.%d" % new_leader
                # change topology: add new leader and decelerate.
                for i in range(new_leader+1,PLATOON_SIZE):
                    topology["v.0.%d" %i]["leader"] = new_leader_id
                topology[new_leader_id] = {}
                new_leader_data = plexe.get_vehicle_data(new_leader_id)
                decel = new_leader_data.speed**2 / (2* (RANGE + new_leader * (LENGTH + DISTANCE)))
                plexe.set_fixed_acceleration(new_leader_id, True, -1 * decel)
            split = True

        # set color for leader
        for i in range(PLATOON_SIZE):
            vid = "v.0.%d" % i
            if topology[vid] == {}:
                traci.vehicle.setColor(vid, (250,0,0, 255))

        if step % 10 == 1:
            # simulate vehicle communication every 100 ms
            communicate(plexe, topology)
        if real_engine and setter is not None:
            # if we are running with the dashboard, update its values
            tracked_id = traci.gui.getTrackedVehicle("View #0")
            if tracked_id != "":
                ed = plexe.get_engine_data(tracked_id)
                vd = plexe.get_vehicle_data(tracked_id)
                setter(ed[RPM], ed[GEAR], vd.speed, vd.acceleration)

        if split == True:
            new_leader_data = plexe.get_vehicle_data(new_leader_id)
            current_phase = traci.trafficlight.getPhase(TL_ID)
            if TL_POS - new_leader_data.pos_x < 10 and current_phase == 0: 
                plexe.set_fixed_acceleration(new_leader_id, True, 3)

        if step > 1:
            radar = plexe.get_radar_data("v.0.1")
            if radar[RADAR_DISTANCE] < min_dist:
                min_dist = radar[RADAR_DISTANCE]

        step += 1
        if step > 3000:
            break  

    traci.close()


if __name__ == "__main__":
    main(True, True)

```



### plexe python API 拆解分析

#### .rou.xml 中的设置

```xml
<vType id="vtypeauto" accel="5" decel="10" sigma="0.0" length="4"
		   minGap="0" maxSpeed="36" color="1,0,0" probability="1"
		   carFollowModel="CC" tauEngine="0.5" omegaN="0.2" xi="1" c1="0.5"
		   lanesCount="4" ccAccel="4" ploegKp="0.2" ploegKd="0.7"
		   ploegH="0.5" />
```

其中

-  `accel` 和 `ccAccel` 中较小的值是车辆的最大加速度。注意：加速度不是一步到位的，而是逐渐增加或减小到极值。
- `carFollowModel` 必须为 CC，否则会报错。

#### 主程序内容

1. 首先是要启动 sumo。

   - 基本启动命令

     ```python
     sumo_cmd = ['sumo-gui', '-c', 'cfg_file']
     traci.start(sumo_cmd)
     ```

   - 非跳变换道

     ```python
     sumo_cmd = ['sumo-gui', '--lanechange.duration', '3', '-c', 'cfg_file']
     ```

   - 显示实时统计信息

     ```python
     sumo_cmd = ['sumo-gui', '--duration-log.statistics', '-c', 'cfg_file']
     ```

   - 输出仿真数据文件

     ```python
     sumo_cmd = ['sumo-gui', '--tripinfo-output", "my_output_file.xml', '-c', 'cfg_file']
     ```

2. 只有 sumo 启动之后，才能调用 plexe

   ```python
   traci.start(sumo_cmd)
   
   plexe = Plexe()
   traci.addStepListener(plexe)
   
   step = 0
   topology = {}
   ```

   将 plexe 对象加入到 StepListener 中，这样，每次 traci.simulationStep( ) 之后自动执行 plexe。

   另外，还要对一些变量初始化，例如 step 和  topology

3. 然后就是开始仿真循环

   ```python
   while True:
       traci.simulationStep()
       ...
       do something
       ...
       step += 1
       
   traci.close()    
   ```

   

####  基本的 traci 添加车辆 

这是最基本的添加车辆操作，完全通过 traci 实现，不需要调用额外的 module 函数

```python
traci.vehicle.add(vid, routeID, departPos=str(position), departSpeed=str(speed), departLane=str(lane), typeID=vtype)
```

需要给定的参数：

- vid
- routeID
- position
- speed
- lane
- vtype



#### 添加 platoon 中的车辆

如果添加的车辆属于某个 platoon，则需要借助 plexe 中的一些函数，在 traci 添加车辆之后进行额外的设置

```python
traci.vehicle.add(vid, routeID, departPos=str(position), departSpeed=str(speed), departLane=str(lane), typeID=vtype)

plexe.set_path_cacc_parameters(vid, cacc_spacing, 2, 1, 0.5)
plexe.set_cc_desired_speed(vid, speed)
plexe.set_acc_headway_time(vid, 1.5)

plexe.use_controller_acceleration(vid, False)
plexe.set_fixed_lane(vid, lane_num, False)

traci.vehicle.setSpeedMode(vid, 0)
traci.vehicle.setColor(vid, (int_R,int_G,int_B), 255)
```

其中，

- set_path_cacc_parameters: 设置 PATH CACC 控制模式中的参数，依次为
  - 期望的固定车间距，这是 PATH CACC 采用的车辆间隔方式
  - damping ratio   $\xi$
  - bandwidth   $\omega_n$
  - leader data weighting parameter

  PATH cacc 用到了前车和头车的信息。

- set_cc_desired_speed: 设置 cruise control 中的期望速度。

  > 亲测：
  >
  > 1. 在车辆起步时，加速度并不是马上到了 rou.xml 文件中设置的值，而是逐渐增加。
  >
  > 2. 在到达指定速度之前就开始减小加速度，最后会有一点超速，然后再稍微减点速。
  >
  > 3. 在程序运行中，如果改变这里 speed 值，车速会改变，依然是通过逐渐加速或减速到达新值。
  > 4. 无论是一次性设置，还是每次循环中都设置，效果都一样
  > 5. 如果 speed 设置为负值，则相当于 0.
  > 6. 一旦用 traci.vehicle.setSpeed(veh_id, speed) 设置了速度，无论比 desired speed 高还是低，都无法再用plexe.set_cc_desired_speed(veh_id, speed) 恢复！！！随后可以依靠 traci.vehicle.setSpeed(veh_id, speed) 和 traci.vehicle.slowDown(veh_id, speed, duration) 设置速度。总之，traci 的这两个命令之间是可以交互作用的。
  > 7. 用 traci.vehicle.setSpeed() 设置了速度，用  traci.vehicle.slowDown() 重设之后会一直保持下去
  > 8. 用 plexe.set_fixed_acceleration() 设置了加速度，虽然用  traci.vehicle.slowDown()  可以在 duration 时间内减速，但是 duration 结束之后继续加速！
  > 9. 如果用了  traci.vehicle.slowDown(veh_id, speed, duration) 调节了速度，随后依然可以用 plexe.set_cc_desired_speed(veh_id, speed)  继续调节。所以 slowDown 对 plexe 更友好一些！

- plexe.set_fixed_acceleration()

  plexe.set_fixed_acceleration(BRAKING_VEHICLE, True, -6) 应该是对应了 C++ 中的void setFixedAcceleration(int activate, double acceleration) 函数，如果设置 activate=1，则按照设定的加速度运行，如果 activate=0，则该函数不起作用，后边给的加速度也被忽略，完全采用 controller —— ACC 或者 CACC 计算出来的加速度。[参考](<http://plexe.car2x.org/api/>)

  > 亲测：
  >
  > 1. plexe.set_fixed_acceleration() 之后没办法用 plexe.set_cc_desired_speed 
  > 2. 可以通过 traci.vehicle.setSpeed(veh_id, speed) 
  > 3. 如果设置了 plexe.set_fixed_acceleration() ，那么无论是先 setSpeed 再 slowDown 还是直接 traci.vehicle.slowDown() ，等 duration 结束之后总会恢复原本的加速度！

- set_acc_headway_time:  设置 ACC 方式中的 headway time

  > 亲测：
  >
  > 1. headway_time 就是对应了后边队列头车与前边队列尾车之间的距离，由于控制作用的波动，并不是严格相等，但是大概差不多。
  > 2. 如果在程序运行中改变了这里 headway_time 的值，则会产生相应的影响。
  > 3. 无论一次性设置还是每个 step 都设置，效果相同

这里只是添加了车辆，还没有确定其是 leader 还是 follower，也就无法确定其采用哪种控制方式，所以把所有可能用到的参数都设置好。

- use_controller_acceleration: 在计算控制策略时是使用车辆实际的加速度还是控制器给定的加速度。False = 车辆实际加速度

- set_fixed_lane: 将车辆固定在某个 lane 上，如果这个 lane 不是当前行驶的 lane，就涉及到换道，True = 换道时考虑安全距离，False = 尽快换道
- traci.vehicle.setSpeedMode: 设置 speed mode，即车辆要考虑的额外限速因素
  - safe speed (低位)
  - max accel
  - max speed
  - right of way at intersection
  - brake hard to avoid passing red light (高位)



#### 构造 platoon topology

在 platoon 中，如果是 leader，则采用 ACC 的控制方式；如果是 follower，就采用 CACC 的控制方式

```python
if leader:
    plexe.set_active_controller(vid, ACC)
else:
    plexe.set_active_controller(vid, CACC)
```

然后设定 topology

```python
topology = dict()

if leader:
    topology[vid] = {}
else:
    topology[vid] = {"front": "front_vid", "leader": "leader_vid"}
```

通过上述程序，添加 vehicle，并建立了 platoon topology。



#### 车间通讯

获取 leader 和 front 车辆的信息，送给 CACC 算法计算

```python
def communicate(plexe, topology):
    for vid, l in topology.items():
        if "leader" in l.keys():
            ld = plexe.get_vehicle_data(l["leader"])
            plexe.set_leader_vehicle_data(vid, ld)
        if "front" in l.keys():
            fd = plexe.get_vehicle_data(l["front"])
            plexe.set_front_vehicle_data(vid, fd)
```

其中获取的前车、头车 data 主要包括  acceleration,  speed, pos_x, pos_y  (vehicle_data.py file in plexe folder)



#### plexe 程序中获取车辆信息的流程

在车辆通信时，可以获得 leader 或者 front 车辆的信息

- 第一层命令

  ```python
  ld = plexe.get_vehicle_data(vid_of_leader)
  ```

  这是调用了 plexe 中的 get_vehicle_data 函数，参数只有一个，就是车辆的 vid。

  返回的 VehicleData 类型的数据。

- 第二层命令

  在 plexe.py 中只是中转了一下，真正的程序在 plexe >> plexe_imp >> plexe_sumo_eclipse.py 中，函数内容如下：

  ```python
  def get_vehicle_data(self, vid):
      ret = self._get_par(vid, cc.PAR_SPEED_AND_ACCELERATION)
      return VehicleData(None, ret[2], ret[1], ret[0], ret[3], ret[4], ret[5])
  ```

  这里是进一步调用了 _get_par 函数，并添加了一个参数: `cc. PAR_SPEED_AND_ACCELERATION = "ccsa"`

  返回值用 VehicleData 类型包装一下，格式如下：

  ```python
  class VehicleData:
  	def __init__(self, index=None, u=None, acceleration=None, speed=None,
               	pos_x=None, pos_y=None, time=None, length=None): 
  ```

  其中 u 为控制输入即期望加速度，acceleration 为当前实际加速度

  

- 第三层命令

  ```python
  def _get_par(vid, par, *args):
      """
      Shorthand for the getParameter method
          :param vid: vehicle id
          :param par: parameter name
          :param args: optional arguments
          :return: the required parameter value
      """
      arguments = cc.pack(par, *args)
      ret = traci.vehicle.getParameter(vid, "carFollowModel." + arguments)
      return cc.unpack(ret)
  ```

  调用了基本的 traci 命令，获得车辆参数，该 traci 函数在 domain.py 文件中。

  ret 是后边函数的返回内容，通过 unpack 可以得到 list 形式，类似：

  `[32.7108, -0.052643, 0.445989, 1147.48, 242.45, 1.52, 32.7108, 0, 0]`

  其中的内容依次为： speed, accel, u, pos_x, pos_y, time, . . .

- 第四层命令

  ```python
  def getParameter(self, objID, param):
      """getParameter(string, string) -> string
  	Returns the value of the given parameter for the given objID
      """
      self._connection._beginMessage(
          self._cmdGetID, tc.VAR_PARAMETER, objID, 1 + 4 + len(param))
      self._connection._packString(param)
      result = self._connection._checkResult(
          self._cmdGetID, tc.VAR_PARAMETER, objID)
      return result.readString()
  ```

  这里 result 是 traci.storage.Storage object，里面的内容可以通过 readString( ) 函数以此读出来，但是只能读一遍，读完之后，再读就会报错了。

  result.readString( ) 返回的结果类似： `32.7108:-0.052643:0.445989:1147.48:242.45:1.52:32.7108:0:0`

### 遇到的问题

- 报错内容

  ```
  File "/home/qpliu/plexe/plexe-sumo/tools/traci/_vehicle.py", line 998, in changeLane
      "!BiBBBd", tc.TYPE_COMPOUND, 2, tc.TYPE_BYTE, laneIndex, tc.TYPE_DOUBLE, duration)
  struct.error: ubyte format requires 0 <= number <= 255
  ```

  根本原因在于 plexe_sumo_eclipse.py 中  traci.vehicle.getLaneIndex(vid) 返回错误值 -2^30。这个问题可能是某些车辆不在当前行驶中，所以返回错误值。需要进一步排查。

  如果只是避免这个报错，可以将 direction 始终设置为 0 ，这样所有的车都不换道。

- 报错内容

  ```
   File "/home/qpliu/plexe/plexe-sumo/tools/traci/connection.py", line 107, in _sendExact
      raise TraCIException(err, prefix[1], _RESULTS[prefix[2]])
  traci.exceptions.TraCIException: Vehicle 'v.15.4.0' is not known
  ```

  Vehicle ***  is not known。 这个问题可能是某些车辆不在当前环境中，无法获取其信息。

  

