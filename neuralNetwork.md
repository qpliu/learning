

# 发展历史

![AI_person1](./pic/AI_person1.png)



## 基础奠定的热烈时期

（20世纪50年代初到60年代中叶）

- 1949年：Hebb 学习规则。 生物神经网络中如果两个神经细胞同时激活或者同时未激活，它们之间的连接权重可能会增加。例如巴甫洛夫的神经反射学说，喂狗的同时响铃，时间长了，只要一响铃，狗就会分泌唾液。基于这个理论，设计了权重更新规则，即 Hebb 学习规则
  $$
  \Delta W = k \times X \times f(W^TX)
  $$
  即权重的变化与 输入和输出的乘积有关，如果乘积为正，则权重增加。很显然，Hebb 学习是一种无监督的学习，没有涉及 output label。

- 1950年：“人工智能之父”图灵设计了图灵测试来判断机器是否智能：如果一台机器能够与人类展开对话（通过电传设备）而不能被辨别出其机器身份，那么称这台机器具有智能。

- 1952年：IBM科学家亚瑟·塞缪尔开发了一个跳棋程序。该程序能够通过观察当前位置，并学习一个隐含的模型，从而为后续动作提供更好的指导。他创造了“机器学习”的概念，并将它定义为“可以提供计算机能力而无需显式编程的研究领域”。

- 1957年：罗森·布拉特 提出了第一个计算机神经网络——感知机 perceptron，模拟大脑的运作方式。

- 1960年：维德罗提出了感知机的 Delta 训练算法，训练得到了线性分类器。

- 1967年：最近邻（the nearest neighbor， kNN）算法出现。核心思想是：如果一个样本在特征空间中的k个最相邻的样本中的大多数属于某一个类别，则该样本也属于这个类别。

- 1969年：马文·明斯基推进了感知器的研究，提出了著名的XOR问题，研究了感知器数据线性不可分的情形

- 



# 人工智能、机器学习、深度学习的关系

![relation1](pic/relation1.png)

- **早期的人工智能**：

  hand-crafted rule， 比如 If then 这种规则，可以实现一定程度的人工智能，但是它的缺点是

  - 需要花费大量资源设计相对全面的规则
  - 无法应对未设定规则的情形

- **机器学习**：面对那些无法用明确的规则应对的环境，要实现所谓的人工智能，一个可行的方法是用机器学习，即给机器大量的数据，让机器自己学习正确的应对规则。人要做的事是教给机器的如何学习到规则，而不是直接给机器规则。规则是什么，规则本质上就是函数或者说映射，给定输入，得到输出。机器学习本质上就是去找到最佳的函数映射，使得给定了输入，能够得到希望的输出。

- **deep learning**: 在所有目前的机器学习方法中，deep learning neural network 的表现是比较优秀的，在有些应用中优势非常突出



# 人工神经网络

## comparison with biological neural networks

Human brain:  100 billion neuron, 1,000 trillion synapses

ResNet-152 neural network: 60 million synapses

![compare_bio_ann1](pic/compare_bio_ann1.jpg)



## interpret ANN from logistic regression

普通的 logistic regression 不能应对线性不可分的状况。一种解决办法是进行 feature transformation，

![feature_transformation](pic/feature_transformation.png)

变换之后，在新的平面上，新的 feature 对应的 data 就是可分的了。

但问题是，feature transformation 很难全都用人力解决。

一个办法就是用 cascading logistic regression

![feature_transformation2](pic/feature_transformation2.png)

这就是 neural network.

## 历史

- 1958 perceptron: 

- 1969 perceptron has limitation

- 1980s multi-layer perceptron

- 1986 backpropagation: could not deal with more than 3 hidden layers

- 1989 1 hidden layer is good enough.

- 2006 RBM initialization

- 2009 GPU

- 2011 popular in speech recognition

- 2012 win ILSVRC image competition  

## Back-propagation

- 要计算任意的权重 $w$ 对应的梯度 $\frac{\partial C}{\partial w}$, 就要计算两项 $\frac{\partial z}{\partial w}$ 和 $\frac{\partial C}{\partial z}$. 

![backprop1](pic/backprop1.png)



- $\frac{\partial z}{\partial w}$ 实际上就是与 $w$ 权重相乘的那一项 input。对于输入层来说，它对应了网络的 input x, 对于后边的层来说，它是每个 neuron 的输出，所以计算这一项，可以从前往后依次计算每个 neuron 的输出。如下图所示：

  ![backprop2](pic/backprop2.png)

- 计算 $\frac{\partial C}{\partial z}$ 则需要从后往前逐步推导 ![backprop3](pic/backprop3.png)

要计算前边的 $\frac{\partial C}{\partial z}$ ，则需要知道后边的 $\frac{\partial C}{\partial z}$ ，以此类推，直到最后一层，该层是很容易计算出来的。

反向计算  $\frac{\partial C}{\partial z}$  的过程可以看成如下的过程：

![backprop4](pic/backprop4.png)



## Why deep rather than fat ?

实验证明，如果参数数目相同，deep 比 fat 更好

![deep1](pic/deep1.png)

从 modularization 角度考虑，例如如下的分类器：

![deep2](pic/deep2.png)

由于 boys with long hair 的数据太少，很难训练出来较好的分类器。

下面用 modularization 的方式

![deep3](pic/deep3.png)

由于基础类别里面，各类类别数据都很多，可以很好的提取 feature，然后再在后边的层中组合。这样就可以用较少的 data 得到较好的训练效果。

# what is ImageNet?

## ImageNet 简介
![imagenet1](pic/imagenet1.jpg)

## Competition: ILSVRC

There is a competition on ImageNet: 

**ILSVRC**: ImageNet Large Scale Visual Recognition Challenge

## 经典网络结构

- 使用 CNN 获得突破是从 2012年的 AlexNet 开始的，作者为

  > Alex Krizhevsky, Ilya Sutskever, and Geoffrey Hinton

![cnn1](pic/cnn1.png)





![imagenet2](pic/imagenet2.JPG)



# recipe of deep learning

![recipe1](pic/recipe1.png)

 

## training 部分的技巧

**注意**：下边这些技巧 例如 dropout 并不针对 CNN，普通 dense network 的训练也可以使用。

### 分批训练数据集

#### Mini-batch

原本每一次更新权重都是要计算所有 data 的误差和，然后 backprop 更新。这叫做 Batch gradient descent.

我们从 data 中比较均匀的随机选择 data，其实也可以比较好的反映 data 的分布，而且可以减小一次更新的计算两。这就是 mini-batch 的思想。在有些教材中，也把 mini-batch 叫做 stochastic gradient descent，而不仅限于 batch size = 1 的特殊情况。

- 将全部 data 随机分成 n 组，每一组就是一个 batch
- 训练的时候，只用一个 batch 中的数据进行权重更新，n 个 batch 就更新 n 次
- 在所有 batch 中训练一遍，称为一个 epoch
- mini-batch size 需要权衡 speed 和 performance。把 batch size 设为 1 和 10 大概需要相同的训练时间，因为 GPU 可以同时训练多个数据。 

#### Stochastic Gradient Descent

如果令 mini-batch 中的 batch size = 1，则就是 Stochastic Gradient Descent，即每个 data 都更新一次权重

- 优点：权重更新频率快，有可能学习的更快
- 缺点：权重改变方向不稳定。但也有好处，有随机性就可能避免局部最小值。

### overfitting 与 underfitting

- overfitting 是指在 training data 上过度训练，导致模型过分拟合给定的 training data，连其中的噪声、误差也被拟合上。这样反而在新的 data 上表现会不好
- underfitting 是指模型选择过于简单，参数再怎么训练都无法拟合 data， 这是李宏毅给的定义。也有人将没有充分训练的模型称为 underfitting。

### vanishing gradient

#### 问题描述

后部的 gradient 比较大，前边几乎为零，即前边权重的变化对 output 影响很小。

![vanishing_gradient1](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/vanishing_gradient1.png)

![vanishing_gradient2](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/vanishing_gradient2.png)



前边的输入到后边会有一种被压缩的趋势，导致前边权重的变化对最后的 output 影响很小，所以求出来的偏导数几乎为 0, 即权重改变很慢。

#### 解决方案

##### ReLU

![relu1](./pic/relu1.png)

Rectified Linear Units (ReLU): 就是实现 rectifier 的单元

rectifier 公式为 
$$
f(x) = \max(0, x)
$$
ReLU 是目前在深度学习中最流行的激活函数。相比于 sigmoid function 有更好的学习效果。

优点：

1. 计算快：先行函数计算要比 sigmoid 中指数计算快

2. 有生物学中的解释

3. 解决 vanishing gradient 问题。

**注意**：当用 ReLU 时，如果希望输出还是概率，则在最后一层输出时依然采用 Sigmoid 函数或者其他能够得到概率分布的函数。

   **重要性质**：

   - 用了 relu 之后，一个 neuron 的输出值要么为 0 ，要么直接为输入值。导数上来说，要么为 0 ，要么为 1. 
   - 不去考虑 z = 0 那个特殊点，这种特殊情况出现的很少，随便选个导数，影响不大。
   - 当把那些输出为 0 的 neuron 去掉之后，整个网络就是一个较为稀疏的线性网络
   - 当参数在 >0 区域变化时，整个网络是线性函数；当某些参数在 <0 区域变化时，整个网络是非线性函数

![relu2](./pic/relu2)

##### ReLU variant

在 ReLU 中， <0 时导数是 0 ，就不能学习了。可以用一个小的导数，这就是 Leaky ReLU，或者加个参数可调的，就是 parametric ReLU.

![relu3](./pic/relu3.png)

##### Maxout

ReLU 可以看成是 maxout 的一种特例

在 maxout 中，线性组合部分分成若干组，每组中取出最大值作为 neuron 的输出。

![maxout1](./pic/maxout1.png)

可以证明，这种 maxout 方式可以包含 ReLU

![maxout2](./pic/maxout2.png)

maxout 还可以包含更多的形式

![maxout3](./pic/maxout3.png)

由于在 maxout 中包含 max 函数，这个要如何求导呢？实际上可以把 max 的输出用那个较大的输入替换掉，成为线性函数

![maxout4](./pic/maxout4.png)

不同的 data 下，保留下来的网络结构不同，可以训练不同连边上的权重。



### 调整步长

#### 步长随时间衰减

这是最容易想到的一种调节步长的方法，初始位置可能离目标点比较远，步长可以设置大一些；随着靠近目标，步长应该逐渐减小。常见的是如下的形式
$$
\alpha(t) = \frac{\alpha}{\sqrt{t+1}}
$$
因此，有
$$
\alpha(0) = \alpha
$$

$$
\alpha(1) = \frac{\alpha}{\sqrt{2}}
$$

$$
\alpha(2) = \frac{\alpha}{\sqrt{3}}
$$

$$
\alpha(3) = \frac{\alpha}{2} \\
\cdots
$$

#### AdaGrad

- 设定每个待学习参数的 learning rate 都不同。
- 假设 $g^t$ 为 $t$ 时刻的梯度
- 权重 $w$ 的更新公式为

$$
w^{t+1} = w^t -\frac{\alpha^t}{\sigma^t}* g^t
$$

其中 $\alpha^t$ 就是前边的随时间衰减的步长。$\sigma$ 是到目前为止所有梯度的 root mean square，即
$$
\sigma^0 = \sqrt{(g^0)^2}\\
\sigma^1 = \sqrt{\frac{1}{2}\Big[(g^0)^2 + (g^1)^2\Big]}\\
\sigma^2 = \sqrt{\frac{1}{3}\Big[(g^0)^2 + (g^1)^2 + (g^2)^2\Big]}\\
\cdots \\
\sigma^t = \sqrt{\frac{1}{t+1}\sum_{i=0}^t(g^i)^2}
$$
另外，由于 
$$
\alpha^t = \frac{\alpha}{\sqrt{t+1}}
$$
所以，最终 AdaGrad 权重更新方式为
$$
w^{t+1} = w^t - \frac{\alpha}{\sqrt{\sum_{i=0}^t(g^i)^2}} * g^t
$$

#### RMSProp

Root Mean Square of the gradients with previous gradients being decayed

![rmsprop](/mnt/0002D34F000E55F4/F%E7%9B%98/%E7%A7%AF%E7%B4%AF/learning/pic/rmsprop1.png)

包含了前一时刻的 $\sigma$ 以及当前的 $g$ 。 其中两部分的权重是人工设定的，相比与 AdaGrad 更灵活一些。 

- AdaGrad 中:

$$
w^{t+1} = w^t - \frac{\eta}{\sqrt{\beta^t}} * g^t
$$

其中 $\beta^t = \beta^{t-1} + (g^t)^2$， $\beta^0 = (g^0)^2$



- RMSProp 中：

$$
w^{t+1} = w^t - \frac{\eta}{\sqrt{\beta^t}} * g^t
$$

其中 $\beta^t = \alpha\beta^{t-1} +(1-\alpha) (g^t)^2$， $\beta^0 = (g^0)^2$

### 调整梯度

#### Momentum (动量)

原本对权重的修改公式为： $w = w - \eta * dx$

考虑到动量，即运动有一些惯性，不会马上改变很大，新的权重修改公式为： 
$$
v = \alpha * v - \eta	* dx
$$

$$
w = w + v
$$

即当前的运动不仅与当前的 gradient 有关，还与之前的运动有关。

![momentum1](pic/momentum1.png)



**图示：**

![momentum2](pic/momentum2.png)



### Adam

本质上是 Momentum + RMSProp，按照 RMSProp 调整步长，同时按照 Momentum 调整梯度改变

![adam1](pic/adam1.png)


$$
m = b1 * m + (1-b1) * dx
$$

$$
v = b2 * v + (1-b2) * (dx)^2
$$

$$
w = w -\frac{\eta}{\sqrt{v}} * m
$$
## 解决 overfitting

如果 training data 上的表现很好，但是 testing data 不好，就是 overfitting 了，也有一些应对措施。比较常见的是如下三种：

- Early stopping
- Regularization
- Dropout

### Early stopping (or early stoppage)

![early_stopping1](pic/early_stopping1.png)

用 validation set 做检测，看什么时候比较合适停止训练，减少 overfitting。

这也是为什么要在 training data 和 testing data 之外再分出来一个 validation data。

![early_stopping2](pic/early_stopping2.png)



### regularization

Also known as **weight penalty**

加上一个惩罚项，限制权重，不要离 0 太远，也可以看成是权重有衰减的趋势。

- 例子

  对比 models  $x_1 + x_2$ 和 $10x_1 + 10x_2$

   ![regularization3](pic/regularization3.png)

  同一个 x 输入到两个 model 中，得到的 Prediction 是不同的，尽管他们的分类一样，但是 likelihood 差别很大。

  input 与 prediction 之间的关系曲线如下图所示，很显然，后者更 certain，对应了 overfitting，它的可扩展性会受到影响。

  ![regularization4](pic/regularization4.png)

#### L2 regularization （L2 penalty）

![regularization1](pic/regularization1.png)

#### L1 regularization (L1 penalty)

![regularization2](pic/regularization2.png)

#### 对比 L1 和 L2

- L1 中限制项是每次向 0 靠近固定的长度；L2 中靠近 0 的长度会随权重的大小而变化
- L1 训练出的权重有大也有小，比较 sparse；L2 找的到权重都比较小，因为它改变长度可以很小

### dropout

可以理解为在训练中，部分网络被训练的比较好，而另外一部分网络没有得到充分的训练。可以考虑依次隐藏部分网络，使大家都有机会得到训练。

- 只有当在 training data 上效果好，而 testing data 效果不好时，才应该用 dropout
- 如果 training data 上效果不好，则 dropout 只会让结果更差

**步骤：**

1. training : 对于每一个 mini-batch，都重新采样一次 neuron，得到新的网络结构，进行训练

![dropout1](pic/dropout1.png)

2. testing: 用完整的网络 test，其中的权重要按比例衰减

   ![dropout2](pic/dropout2.png)



# MNIST 手写数字识别

对于计算机来说，它看到的并不是图片，而是数字矩阵，或者对于 dense network 更确切地说，是将数字 flattening 之后的向量

![mnist1](pic/mnist1.png)

![mnist2](pic/mnist2.png)

在确定网络结构时，udacity-pytorch course 的意见是去 google 搜网络结构，看到差不多的就尝试一下。



# 卷积神经网络 CNN

## 基本框架

![cnn2](pic/cnn2.png)

## 内涵思想

![cnn3](pic/cnn3.png)

CNN 实际上是基于图片这种 input data 的特征，对 DNN 做了特殊处理，使其参数更少，训练更快。如果算力足够强大，直接用 CNN 训练也可以。能用 CNN 识别的图片，理论上用 DNN 可能会有更好的效果，但是参数太多，很难训练。

上边的图片也说明了什么时候比较适用 CNN

- some patterns are much smaller than the whole image

- the same patterns appear in different regions

- sub-sampling the pixels will not change the object (若不满足，可以不用 pooling 这一层。例如 Alpha Go)


## 实现步骤

### 用 filter 依次对原图片做卷积

![cnn_step1](pic/cnn_step1.png)

filter 中的参数就是需要训练的参数。

如果是彩色图片，则用立体的 filter 对原来的立体图片做卷积

![cnn_step2](pic/cnn_step2.png)

### Max/Average Pooling

选取一组中最大值或者平均值，进一步压缩网络结构。

![cnn_step4](pic/cnn_step4.png)

经过卷积和 Pooling 之后，网络结构为

![cnn_step5](pic/cnn_step5.png)

**The number of channel is the number of filter.**

### Flatten

最后还是回归到了 DNN 的形式，因为参数已经足够少了，可以进行 DNN 的训练了。

将所有的 neuron 都排在一列，相当于拉直排成一层，然后连接上 fully connected network.

![cnn_step6](pic/cnn_step6.png)

## 各层的参数数量

![cnn_step7](pic/cnn_step7.png)

filter 不仅仅是平面，还可以是多层平面。

上图中第二个 convolution 层，每个 filter 有3 × 3 × 25 个参数，其中的 25 就是前一层数据的 channel 数量







## CNN 与 传统 DNN 的区别

- 前后两层之间不是 fully connected，减少了很多参数
- 同一个 filter 中参数共享，进一步减少了参数
- 一般 Convolutional layer 与 pooling layer 连用，更减少了参数

一般来说，较少的参数可以防止 overfitting.

![cnn_step3](pic/cnn_step3.png)





## CNN 可视化

参考文献：

[Understanding your Convolution network with Visualizations](https://towardsdatascience.com/understanding-your-convolution-network-with-visualizations-a4883441533b)

该文献以 InceptionV3 为例，介绍了 3 种对 CNN 内部内容可视化的方法

1. 将中间层的输出可视化。

   最初的输入是人类可识别的照片，那么随后每层经过作用之后的输出是什么样的呢？

   ![cnn_visualization1](pic/cnn_visualization1.png)



   其中某些层的输出

   ![cnn_visualization2](pic/cnn_visualization2.png)



   再来看看 ReLU 的输出

   ![cnn_visualization3](pic/cnn_visualization3.png)

2. 将 filter 可视化。即找到那个使 filter 激活程度最大的图片，这也是寻找每个 filter 对什么样的输入图片反应比较强烈，每个 filter 在搜索图片上的什么信息。这种方法就是李宏毅课程中提到的，如下：

   ![cnn_step8](pic/cnn_step8.png)

   1) train 完之后，选定某个 filter 的输出

   2) 定义 filter 上各元素的和，目的是找到一个 Input 使得该 filter 上各元素和最大

   3) 搜索 input 的过程是用 gradient ascent 方法

   4) 对于前端的 filter，找到的 图片可能是这样的

   ![cnn_step9](pic/cnn_step9.png)

   5) 后端的 filter 可能是这样的

   ![cnn_step10](pic/cnn_step10.png)



   所以在做 transfer learning 的时候，一般是保留前部的 filter 参数不变，只重新训练后部的 filter。因为前部的 filter 都是在搜索图片中的基本图案，如横条竖条之类的，后部的是对基本图案进行集成的，不同的图片需要进行不同的权重训练。

3. Class Activation Map，即确认图片中哪些部位导致了最后分类的结果。

   最常见的方式是产生一个 heatmap，计算每个部位对最后输出结果的影响大小。

   ![cnn_visualization4](pic/cnn_visualization4.png)

   - 左边是输入的图片
   - 中间是 InceptionV3 最后一层对应的 activation heatmap
   - 右边是将 heatmap 叠加到原图上

   表明了图片中哪一部分区域决定了最终的分类结果，上述分类都是 Daisy，但是很显然有对有错。这可以方便我们 debug，查找到底是什么导致了错误分类。



# Style Transfer

经典文章：

Leon A. Gatys, Alexander S. Ecker, and Matthias Bethge. A neural algorithm of artistic style. arXiv 2015.

文章首先介绍了如何基于 VGG-19 重构一幅图片的 content 和 style.

![style-transfer1](pic/style_transfer1.png)

## content

对于 VGG 网络来说，前部各层的输出保留了 pixel 等细节，到了后部就主要是 content 信息了，具体的 pixel 已经丢失了，毕竟 VGG 是用来进行物体分类的，越到后部，越抽象，具体细节不重要。

一般选取后部某一层的输出，即 feature map，表征 input image 的内容。

**重构 content 的过程：**

1. 选定某一层的输出作为 content，例如第 k 层
2. 待生成的图片初始为随机图片，其第 k 层的输出表示为 F_k. 同时，内容图片的第 k 层输出表示为 P_k
3. 将 F_k 和 P_k 之间的差距设定为 loss function。很显然，loss function 是待生成图片像素值的函数
4. 通过梯度下降法和 back-prop 调整待生成图片的像素值，直到 Loss function 最小。
5. 此时得到的图片就可以看成是对图片 content 的重构。

## Style

本文将 style 定义为同层输出的 feature maps 之间的关联性。假设某层的输出有 N 个 feature map，定义一个矩阵，称为 Gram matrix，维度为 N × N， 其中在 i 行 j 列上的元素是第 i 个 feature map 与 第 j 个 feature map 向量化之后的内积，这种内积形式就表征了两个 feature map 之间的关联性。

每一层都有一个对应的 Gram matrix，一般 style 重构时同时考虑多个层上的 gram，用权重进行权衡。

**重构 style 的过程：**

1. 选定若干层输出的 feature map 用于重构，例如 k1, k2, k3
2. 带生成的图片初始为随机，同样也有第 k1 层的输出为 F_k1，然后定义 gram 关联矩阵，同样的，style图片也可以定义 gram。这两个 Gram 的差距表征了该层上的待求图片与 style 图片的 loss
3. 将这些层的 loss 加权起来，得到最后的 loss function，依然是关于待求图片的函数
4. 还是用 梯度下降和 back-prop 优化待求图片，使 Loss function 最小。
5. 此时得到的图片就认为是表征了 style

## 最终的 content 与 style 的融合

实际上就是将上述两个 loss function 加权组合起来，这样的图片即保证了 content 又有 style.

## pytorch 实现

详见 pytorch 部分文件。

# capsule network

## CNN 中存在的问题

![capsule1](pic/capsule1.png)

CNN 只是寻找图中是否有某个 feature，但是丢弃了 feature 之间空间关联。


# 递归(循环)神经网络 RNN
## 基本形式

RNN:  Recurrent Neural Network

RNN 适合输入数据之间有时间关联、有序的情况。例如语音处理，语言处理。

对于图像的识别也可以用 RNN，可以以每一行的 pixel 作为一个输入，依次输入的每行 pixel 之间肯定是有关联的，就构成了关联数据。 

![rnn](pic/rnn.png)

上图中不同时刻输入用的是同一个网络结构，而不是一串网络。要训练的也是同一组参数。

RNN 的数学描述：
$$
\begin{align}
h_t &= \sigma (W_{hh}h_{t-1} + W_{Xh}X_t) \\
Y_t &= \sigma (W_{hY}h_t)
\end{align}
$$
即影响最终输出的是隐含层的输出，而隐含层的输出取决于当前输入和前一时刻隐含层的输出。

对于多个隐含层的RNN，例如第 i 层，它的输入一个是前一时刻第 i 层的输出，另一个是当前时刻第 i-1 层的输出。

![rnn4](pic/rnn4.png)

## Elman network 与 Jordan network

上图中, 层与层之间对应传递的形式称为 Elman network。

另外也有一种变种，是将前一时刻的最终输出送给下一时刻的隐含层输入，称为 Jordan network.

![rnn5](pic/rnn5.png)

pytorch 中搭建 RNN 层比较简单，以 Elman Network 为例，格式如下：

```python
# 指明基本网络结构
>> rnn = nn.RNN(10, 20, 2)   # input_size=10, hidden_state=20, layers=2. 这里还可以设定 batch_size=True，那么后边的 input 和 output 都是第一维是 batch，顺序更直观一些，但是 hidden 的改不了

# 指明 input, hidden 两方面的输入格式
>> input = torch.randn(5, 3, 10)  # 每个 sample 是长为5的seq，batch_size=3, inputfeature_size=10
>> h0 = torch.randn(2, 3, 20)  # 2 个 RNN 串起来，batch_size=3, hidden_size=20

# 最后一层 RNN 的输出以及 hidden_state 更新
>> output, hn = rnn(input, h0) 
# out 是固定为最后层的output, 遍历 seq
# hn 是固定为最后 seq，遍历所有层的 output，最后 hn 的 size 与 h0 完全相同
```

在最开始构造网络结构时，其中的参数可以按顺序指定，也可以通过关键字指定，其中关键字依次为：

- **input_size** – The number of expected features in the input x
- **hidden_size** – The number of features in the hidden state h
- **num_layers** – Number of recurrent layers. E.g., setting `num_layers=2` would mean stacking two RNNs together to form a stacked RNN, with the second RNN taking in outputs of the first RNN and computing the final results. Default: 1
- **nonlinearity** – The non-linearity to use. Can be either ‘tanh’ or ‘relu’. Default: ‘tanh’
- **bias** – If `False`, then the layer does not use bias weights b_ih and b_hh. Default: `True`
- **batch_first** – If `True`, then the input and output tensors are provided as (batch, seq, feature). Default: `False`
- **dropout** – If non-zero, introduces a Dropout layer on the outputs of each RNN layer except the last layer, with dropout probability equal to `dropout`. Default: 0
- **bidirectional** – If `True`, becomes a bidirectional RNN. Default: `False`

上边只是系统自带的一层 RNN 构造，如果要构造完成的网络，从输入到最后的输出，还是需要自己搭建，如下：

```python
class RNN(nn.Module):
    def __init__(self, input_size, output_size, hidden_dim, n_layers):   # 这里只比单纯的 RNN 多了对 output_size 的设定
        super(RNN, self).__init__()
        
        self.hidden_dim=hidden_dim

        # define an RNN with specified parameters
        # batch_first means that the first dim of the input and output will be the batch_size
        self.rnn = nn.RNN(input_size, hidden_dim, n_layers, batch_first=True)
        
        # last, fully-connected layer
        self.fc = nn.Linear(hidden_dim, output_size)

    def forward(self, x, hidden):
        # x (batch_size, seq_length, input_size)
        # hidden (n_layers, batch_size, hidden_dim)
        # r_out (batch_size, time_step, hidden_size)
        batch_size = x.size(0)
        
        # get RNN outputs
        r_out, hidden = self.rnn(x, hidden)
        # shape output to be (batch_size*seq_length, hidden_dim)
        r_out = r_out.view(-1, self.hidden_dim)  
        
        # get final output 
        output = self.fc(r_out)
        
        return output, hidden
```

## Bidirectional RNN

上述网络中，早期的输出只见过更早时刻的输入，而不知道后边的输入，而双向 RNN 则可以兼顾整个输入序列。

![rnn6](pic/rnn6.png)

在双向 RNN 中，每个输出都经历过了所有的整个输入序列。

## 训练

RNN 的训练算法成为 BPTT： BP through time，实际上就是考虑了当前权重影响 loss 的两个途径：时间和空间

![BPTT](pic/BPTT.png)

推导过程与 BP 一模一样。

## 基本 RNN 的问题

单纯的 RNN 是很难训练的

![rnn1](pic/rnn1.png)

loss function 有时变动会很大，这本质上是因为 loss vs weight 函数就比较崎岖

![rnn2](pic/rnn2.png)

所以，如果权重改变正好在悬崖和平地之间， Loss 的变化会很大。

一个解决方案是把 learning rate 设置很小，避免权重改变飞出去；但是在平坦的地方，小的 learning rate 就会学习很慢，会出现 gradient vanishing 问题。

RNN 梯度消失的问题也可以从输入到输出传递来解释，理论上来说，初始的输入要一直影响最终的输出，可以说时间序列有多长，这个传递就有多少，这样在权重更新时就有可能出现梯度下降的问题。

为了解决 gradient vanishing 的问题，引入了 LSTM。在 LSTM 中，由于记忆作用，平坦地方几乎消失，大多数 gradient 都是很大的，所以可以统一设置较小的 learning rate。

目前常见的 RNN 主要包括：

- simpleRNN (就是上边的基本形式)
- LSTM
- GRU： Gated Recurrent Unit (类似与 LSTM， 但只有两个 gate，参数少了一些)，旧的不去，新的不来，input gate 和 forget gate 连接起来，要把旧值清除掉，新值才会被放进来。

## LSTM 

Long short-term memory 长短期记忆， 是当前最流行的 RNN 形式之一。当一个人说用 RNN 的时候，很可能他就是在用 LSTM。

基本的 RNN 的记忆作用比较短，LSTM 可以有选择的长期记忆。

![lstm1](pic/lstm1.png)

之所以称为 Long short-term memory，是因为在基本的 RNN 中，memory 是短期的，每次都要被新的 Input 更新。在 LSTM 中，memory 可以选择是否更新，可以保存更长的时间。

**LSTM 相比于普通的 RNN 多了三个控制器，也是三个 gate： 输入控制，输出控制，遗忘控制**

在最初的 LSTM 中，不存在 forget gate，因为就是为了保存记忆才设计的 LSTM。

更加详细的操作过程：

![lstm2](pic/lstm2.png)

### 简单的例子

![lstm3](pic/lstm4.png)

具体实现：

![lstm5](pic/lstm5.png)

### 一般网络结构

![lstm6](pic/lstm6.png)

LSTM 中，input 不仅要连接到 neuron 的输入端，还要连到另外三个 control gate 上，参数数量是普通 DNN 的 4 倍。

从上图可以看出来，隐藏层中的神经元只跟下一时刻自己相连，不会连到同一层的其他神经元。也就是说不会用自己的记忆去修改别人的记忆。

对于多层 LSTM 一般形式为

![lstm7](pic/lstm7.png)

- 输入层不仅仅是 x，还包括前一时刻的输出，前一时刻的 memory。
- **Peephole**: 就是将前一时刻的 memory c 送入输入层
- 在有些教材中，神经元中维持的 memory 对应 long term memory，神经元的 output 对应 short term memory。这样，每个神经元的输入包括原本的输入 + 前一时刻该神经元的输出，即 short term memory + 前一时刻该神经元的 memory，即 long term memory.

在 pytorch 中构造 LSTM 的基于语法如下：

```python
>>> rnn = nn.LSTM(10, 20, 2) # 从外部看， LSTM 与 RNN 需要的设置的超参数相同，也是 input_feature_size1=10, hidden_size=20, num_layers=2

>>> input = torch.randn(5, 3, 10) # seq_len=5, batch=3, input_size=10

# 初始 hidden_state 和 初始 cell_state
>>> h0 = torch.randn(2, 3, 20) # num_lay=2, batch=3, hidden_size=20
>>> c0 = torch.randn(2, 3, 20) # num_lay=2, batch=3, hidden_size=20

>>> output, (hn, cn) = rnn(input, (h0, c0)) # output=(seq_len, batch, hidden_size)
```



## GRU

GRU: Gated Recurrent Unit。

RNN， GRU， LSTM 对比：

![RNN_compare](pic/RNN_compare.png)

- RNN 必须要更新 memory， 所以 memory 中很难长久保留之前的东西;
- GRU 加上了决定 memory 是否更新的 gate，有可能长久保留 memory;
- LSTM 则增加了更多的控制 gate，功能更强大，也更复杂。

很多情况下 GRU 与 LSTM 效果差不多，但是 GRU 参数更少，所有也是比较流行的 RNN 网络结构。

Pytorch 中实现 GRU 的语句：

```python
>>> rnn = nn.GRU(10, 20, 2) # 与 RNN， LSTM  完全相同

>>> input = torch.randn(5, 3, 10) # input(seq_len, batch, input_size)

>>> h0 = torch.randn(2, 3, 20) # (num_lay, batch, hidden_size)

>>> output, hn = rnn(input, h0) # output=(seq, batch, hidden_size)
```

从 pytorch 语句上来看，三种 RNN 的结构从外部看都是相同的，区别只是内部运算是信息流动的复杂程度。

## 应用

比较常见的就是给 RNN 很多资料，让她学习其中的承接关系，然后让她自己以此产生新的内容。

比如学习很多散文，然后自己写散文；学习科研论文，然后自己写文章。。。



# Semi-supervised learning

## Generative model

在 supervised learning 中，我们先研究了 generative model, 然后由它引出 logistic regression 等。这里我们也可以先看看如何计算 semi-supervised generative model。

**步骤如下：**

1. 初始的 generative model 可以从 labeled data 估计出来，也可以随机产生。

2. 然后计算每个 unlabeled data 对应的分类概率。如果有 label 的话，我们是应该可以直接知道它是属于哪一类的，现在我们只能估计一个概率。
3. 有了新数据的分类概率，更新现有的 generative model。其中的更新公式都还比较直观。

 

![semi_supervised1](pic/semi_supervised1.png)







# Reinforcement learning

## 与 supervised learning 区别

- supervised learning 中，人类会直接给出期望的输出，让机器由期望的输出去学习
- reinforcement learning 中，人类只会告诉机器做的好或不好，而不是应该怎么做，可能人类也不知道应该怎么做，具体应该怎么做则由机器自己摸索。

以alpha go 下围棋为例。先从棋谱学习，这是 supervised learning，然后再进行 reinforcement learning ，两个机器下棋，由输赢判断下的好不好，然后不断修正自己的策略。

一般来说，如果有条件做 supervised learning，还是首选它，因为我们有绝对正确的答案供机器参考。机器只需要严格按照我们给定的数据去学习就好了。只有当我们没有标准答案时，才不得不借助 reinforcement learning。



## 基本概念

三个关键变量：

- State (or observation, environment)： 就是当前所处的环境信息

- Action：机器发出的动作，这也是要不断学习的东西，希望最终给出越来越好的 action

- Reward：由 state 和 action 共同决定的动作的奖励或惩罚

  ![reinforcement1](pic/reinforcement1.png)



## 例子： space invader

![reinforcement2](pic/reinforcement2.png)

 

从开始训练到 game over 称为一个 episode, 机器的目标就是在 episode 中获得最大的 reward.



## Difficulties of reinforcement learning

- **reward delay:** in space invader, only "fire" obtains reward, but the move before "fire" is important. Some times may be better to sacrifice immediate reward  to gain more long-term reward
- **balance between exploration and exploitation**



## RL 分类

![a3c12](pic/a3c12.png)



- Model-based approach: 适用于规则明确的情景，例如棋类游戏，环境的变化比较容易穷举

- Model-free approach：不太容易对外界变化进行建模，比如星际争霸
  
  - Value-based： 例如 **Q-learning** 和  **Deep Q-learning**，通过寻找最大收益值来确定最佳 action。这是比较容易理解的一种，Q-learning 好理解，基于输出值及其误差的神经网络的训练也好理解。
  
  - Policy-based：例如 **policy gradient method**，直接预测 action，而且 action 可以是连续的。
    - on-policy：正在学习的这个 model 、与环境互动的 model 是同一个
    - off-policy：…… 不是同一个
  - Policy-Value-based：例如 **A3C**



很多人介绍 reinforcement learning 是从 Markov Decision Process 引入的，也有从 Deep Q-learning 介绍的。不过，这些都已经过时了，现在最厉害的 Model 都是用 A3C 训练出来的。



## Deep Q - learning

Deep Q-learning 属于 value-based.

### 传统的 Q - learning

在传统的 Q-learning 中，假设存在一个表，其中的值表明了在某一环境 $s_i$ 下采用动作 $a_i$ 得到的收益 $Q(s_i, a_i)$，即 Q-table 和 Q-value.

|       | $a_1$         | $a_2$         | $a_3$         | ...  |
| ----- | ------------- | ------------- | ------------- | :--: |
| $s_1$ | $Q(s_1, a_1)$ | $Q(s_1, a_2)$ | $Q(s_1, a_3)$ |      |
| $s_2$ | $Q(s_2, a_1)$ | $Q(s_2, a_2)$ | $Q(s_2, a_3)$ |      |
| $s_3$ | $Q(s_3, a_1)$ | $Q(s_3, a_2)$ | $Q(s_3, a_3)$ |      |
| ...   |               |               |               |      |

当有了 Q-table 之后，所有的动作判断就很简单了。关键是如何得到 Q-table 中的值？这是一个不断学习，不断提高的过程。

![q_learning6](./pic/q_learning6.png)

其中 Q update 也可以写成：
$$
Q(s,a) = (1-\alpha)Q(s,a) + \alpha[r+\gamma \max_{a'}Q(s',a')]
$$
上图中 $\epsilon$ -greedy 是指以 $\epsilon$ 的概率随机选动作，以 ($1-\epsilon$) 的概率选当前 Table 中指明的最优动作。

- 选动作是根据 Table 进行的
- 每次选完动作，得到相应收益之后，就更新一次 Table 中对应的 value
- 新的 value  =  $(1-\alpha)$ * 旧表中的 value + $\alpha$ * 当前动作对应的近期实际收益和折扣之后的远期预计收益
- 动作完之后就到了新的环境中，继续基于更新之后的 Table 重复上述步骤，直到一局游戏完整结束。



### Deep Q - learning

在传统的 Q-learning 中，如果状态组合、动作比较多，比如下围棋，得到的 Table 会很大，而且搜索很费时。

实际上，Table 本身反映的是一个输入输出的映射关系，而拟合映射关系正是神经网络擅长的，这就产生了 Deep Q - Learning。

#### 构造网络

Deep Q - learning 中用一个 deep network （称为 DQN, Deep Q network）去拟合 Q-Table，实际上是个 Q-value function，即给定了 $s$ 和 $a$ ，对应怎么样的 Q - value，即 2 输入 1 输出，也可以只输入当前状态，神经网络给出所有可能动作 $a$ 及其对应的 Q-value，即 1 输入 N 输出，然后采用那个对应最大 Q-value 的动作。

![q_learning7](./pic/q_learning7.png)

或者表示为

![q_learning0](./pic/q_learning0.png)



![q_learning10](./pic/q_learning10.png)

上图中，输入是 4 帧图片，capture the motion，输出是预测在这种情况下所有 act 对应的 value.  

用 NN 就必须训练它，采用 supervised learning 方式，其中 target 就是每步动作之后的实际和折扣预期收益，这是由状态+动作获得的收益，是更接近真实的收益，而网络的当前输出就是待改进的 Q-value，有了这两个值，就知道了误差，就可以训练网络了。

![q_learning8](./pic/q_learning8.png)



#### DQN 特有的机制

与传统 supervised learning 相比，上述 Deep Q-learning 有两个问题：

- 用 NN 进行函数拟合的时候，学习样本的相关性是比较麻烦的问题。如果没有充分随机化选样本，比如最后送进去的样本都是偏离真实函数很大的，那么学出来的效果就很差。

- DQN 中 target 是不断变化的，这有可能导致学习不收敛，因为目标老是变来变去。

为了解决上边两个问题，DQN 增加了两个机制，这也正式 DQN 强大的所在：

1. Experience replay : 随机抽取之前的经历进行学习，打乱了经历之间的相关性。这个机制是其决定性作用的。当前被学习的经历不是当前步的经历，而是从一个从过往经历中随机提取的一个。

2. Fixed Q-targets : 用两个结构相同但是参数不同的神经网络，分别用于 target 和当前输出。计算当前输出时用的是最新参数并时刻调节，而 target 部分（即计算 $R+r\max Q $ 中的 Q）用的是旧参数。然后每隔若干步同步一次两个网络的参数。目的是保证一个相对稳定的 target，而不是去拟合一个老是在改变的 Q function. 而且由于 target 中参数变化的滞后，在一定程度上打破了经历关联性。这个机制作用也很明显，但不如 Experience replay 那么决定性。可参考下图，target 更新慢。

   ![q_learning4](./pic/q_learning4.png)



#### 具体算法流程

![q_learning9](pic/q_learning9.png)

一些注意事项：

- To capture the motion, last 4 image frames are processed in  $\phi$ .

- 上述算法中更新网络权重时采用 Huber loss function，如下图中绿色函数曲线。

  ![huber_loss1](./pic/huber_loss1.png)
  
  

![huber_loss2](pic/huber_loss2.png)

即在自变量很小的时候，以 quadratic 的形式变化，当自变量很大时，以线性方式变化。

Huber loss 有利于减小学习中的剧烈震荡或者 outlier 和 noise。

最终 Loss 的形式为：
$$
L = \frac{1}{|B|} \sum_{(s, a, s', r)\in B}L(\delta)
$$
where
$$
L(\delta) = \begin{cases} \frac{1}{2}\delta^2  & \text{for} ~|\delta|\le 1 \\ |\delta| - \frac{1}{2} & \text{otherwise}\end{cases}
$$
 and
$$
\delta = Q(s,a) - (r + \gamma \max_a Q(s', a))
$$
$(s, a, s', r)$ 是 batch 中的一个 transition，对应了 $ s $ 环境时 $ a $ 行为导致的进入新的 $s' $ 环境，收益为 $r$.

- $\epsilon$ 一般初始值为 1，即初始选的 action 是完全随机的，然后逐步减少到0.05，既保证了初始阶段的 exploration，又保证了后期的 exploitation.



## 基础的 policy gradient 方法

在这种 RL 方法中，network 的输入依然是 state，例如游戏画面。与 DQN 不同的是，这里的输出不是多个 Q value (每个对应一个 action)，而是多个 probabilities (每个对应一个 action)。

其实这里 Q learning 和 Policy based learning 很像，都是通过一个值（Q value 或者 probability）去选择 action，只不过 Q learning 中选择 action 的过程是确定性的，就是选择最大 Q value 对应的 action，而 policy 中是按照 probability 选择的。**一个是确定性的，一个是有随机性的**。

![rl_0](./pic/rl_0.png)



### 推导过程

Actor (或者说 Agent）的目的是最大化一个 episode 的 reward $R=\sum_{t=1}^Tr_t$.

![rl_1](./pic/rl_1.png)



可以把上述过程抽象一下：

![rl_2](./pic/rl_2.png)



一个 episode 实际上就是一个 trajectory，里面是一系列的 state 和 action。

某个 trajectory  $\tau$ 出现的概率表示成 $p_{\theta}(\tau)$，它是与网络参数 $\theta$ 相关的，因为网络决定了某个状态有怎样的输出。

![rl_3](./pic/rl_3.png)



网络参数的更新是基于 reward 的。但是由于 action （以及可能的环境状态）的随机性，应该用 reward 的期望值，即遍历所有的 trajectory，得到其概率以及对应的 reward，然后取期望。

在更新网络参数时，需要用到 reward 关于 $\theta$ 的梯度，即 

![rl_4](./pic/rl_4.png)



上述式子中 $p_{\theta}(\tau^n)$ 实际上是一堆东西的乘积(可以参考前边第二幅图)，写成 log 之后，就成了它们相加的形势，而且由于环境相关的那一部分与网络参数 $\theta$ 无关，求偏导为零。最后的梯度表达式如上所示。

虽然说是 policy gradient，但并不是 policy 的 gradient，而是归结到 policy 对应的概率的 gradient.

如果说 policy 就是 action 的概率的话，也行！

在实际执行时，要用同一个网络，跟环境互动，得到多个 trajectory，然后那这些 trajectory 去计算梯度，更新网络参数。

![rl_5](./pic/rl_5.png)



### 两个改进

- 添加一个 baseline。在某些游戏中，reward 总是正的，因此，在更新网络参数时，那个 action 被更新到了，它的概率就会相对增大，考虑到采样的不完备，有些 action 仅仅可能是没有被采样到，它的概率就会降低。为了避免这种情况，可以从 reward 中减去一个 baseline，让某些 reward 成为负值。这样如果它其中的 action 被更新，概率就会降低。一般取 baseline 约为所有 trajectory reward 的平均值

  ![rl_6](./pic/rl_6.png)

  

- 同一个 trajectory 中的 action 可以有不同系数，这样坏 trajectory 中的好 action 以及好 trajectory 中的坏 action 都可以被区别对待。

  ![rl_7](./pic/rl_7.png)

  一种修改方式时只考虑该动作之后得到的 reward，而不是整个 trajectory 的 reward.

  另一种修改方式是进一步考虑 discount factor.

  题外话：$(R-b)$ 这一项也被称为 advantage function，它表示的是这个 reward 相对与其他 action 得到的 reward 的好坏。而这个 baseline 也可以通过其他方式（例如 critic ）计算。



 



## A3C

A3C: Asynchronous Advantage Actor-Critic 

属于 Policy-value-based 方法，既训练一个 actor，又训练一个 critic

文章如下：

![a3c](pic/a3c1.png)



### First part: Learning an Actor

#### Neural network as Actor



![a3c2](pic/a3c2.png)

最终的动作是依概率选择的，不是确定性的选择概率最大的那个动作。

#### 如何评价 Actor

- 让 Actor 玩游戏，直到游戏结束，获得 reward，但是由于动作存在随机性，每次游戏的 reward 不同

- 玩多次，求平均值，最终就反映了这个 Actor 的好坏 

  ![a3c3](pic/a3c3.png)

这个均值用数学严格的描述就是

![a3c4](pic/a3c4.png)

#### Policy gradient 

用 Policy Gradient 方法，或者称 Gradient Ascent ，因为这里是 reward，而不是 loss ，所以应该求最大化。

![a3c6](pic/a3c6.png)

下面的关键就是求平均 reward 的梯度

![a3c7](pic/a3c7.png)

![a3c9](pic/a3c9.png)



注意一个问题，如果用多次采样求平均的方法，那些没有被采样到的 action 就会被动减小概率，因为只要被采样到了，肯定会增加或者不变，因为 reward 总是正的或者 0 。

为了避免这种情况，就要避免所有被采样的 action 都增加，要制造出来 reward 为负的情况，比如将 reward 与一个 baseline 比较，只有超过了 baseline，reward 那一项才为正。

![a3c10](pic/a3c10.png)

### Second part: Learning a critic

在传统的 Q-learning 中，可以从 critic 中得到一个 actor.

但是 A3C 是把两部分分开的。critic 就是单纯的评价，不会直接调整 action.

#### state value function

 ![a3c11](pic/a3c11.png)

对于现在训练的 actor，以及 observation, critic 会给出一个期望的 reward.

例如：对于当前的 actor, 输入一个游戏画面，即当前的游戏状态，估计在游戏结束前还可以得到多少 reward

#### estimate state value function

- Monte-Carlo based approach: 就是从 model 过去的 s 和 reward 记录中学习

![a3c13](pic/a3c13.png)

- Temporal-Difference approach

![a3c14](pic/a3c14.png)



**Toy example**

In general, MC is unbiased, but may have large variance; in contrast, TD is biased with smaller variance.

And the two approaches may have different results.

![a3c15](pic/a3c15.png)



### Finally: Combine Actor-Critic

整体框架：

![a3c16](pic/a3c16.png)

具体实现：

![a3c17](pic/a3c17.png)

在这个训练中，critic 可以告诉 actor 在每一个 s 状态下，action 的 reward 与 期望的 reward 的差距，指出 action 的好坏。

再加一个训练技巧：

![a3c18](pic/a3c18.png)







# Big companies and big shot

## Company: DeepMind

London startup, was bought by Google for 4 亿 pounds (6.5 US dollars) in 2014.

## people: David Silver

## people: Richard S. Sutton

Canadian computer scientist, University of Alberta.

One of the founding fathers of reinforcement learning.

Reinforcement learning Bible: 

>  Richard S. Sutton and and Andrew G. Barto. Reinforcement Learning: An Introduction. Second Edition, in progress MIT Press, Cambridge, MA, 2017.



