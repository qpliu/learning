## 基本概念

### 什么是 QDII

Qualified Domestic Institutional Investor (合格境内机构投资者), also known as QDII, is a scheme relating to the capital market set up to allow financial institutions to invest in offshore markets such as securities and bonds. It is a transitional arrangement which provides limited opportunities for domestic investors to access foreign markets at a stage where a country/territory’s currency is not traded or floated completely freely and where capital is not able to move completely freely in and out of the country.

### 什么是 C 类基金

买入没有手续费，持有期间会收取管理费、销售服务费等，卖出费用随持有时间变化，如果持有超过一定时间，可能免手续费，因为持有时间越长，被提取的管理费用越多。

### 什么是 Real Estate Investment Trust (REIT)

以投资房地产为主的基金，例如投资商场、写字楼、公寓、酒店、医院、养老院、仓库。

例如：

![reit](pic/reits.png)



