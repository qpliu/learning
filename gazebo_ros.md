# URDF

Unified Robot Description Format: an XML format for representing a robot model

我们可以只用 URDF 编写 model，也可以额外用一个 macro语言叫做 xacro 来组织 URDF 文件，使其更具可读性，并且减少代码冗余。

练习条件：安装 urdf_tutorial package。

- 方法1：`sudo apt install ros-kinetic-urdf-tutorial`
- 方法2：(推荐) git 下载 urdf_tutorial package 文件到 ros workspace 中。后续的操作就都在 urdf_tutorial 文件夹下完成



## 常见结构

- link: 标准几何形状

```xml
<link name="left_back_wheel">
    <visual>
        <origin rpy="1.57075 0 0" xyz="0 0 0"/>
        <geometry>
            <cylinder length="0.1" radius="0.035"/>
        </geometry>
        <material name="black"/>
    </visual>
</link>
```

- link: 外部导入的 mesh

```xml
<link name="left_gripper">
    <visual>
        <origin rpy="0.0 0 0" xyz="0 0 0"/>
        <geometry>
            <mesh filename="package://tutorial/meshes/finger.dae"/>
        </geometry>
    </visual>
</link>
```

- link: 加入 collision

```xml
<link name="base_link">
    <visual>
        <geometry>
        	<cylinder length="0.6" radius="0.2"/>
        </geometry>
    	<material name="blue">
        	<color rgba="0 0 0.8 1"/>
        </material>
    </visual>
	<collision>
    	<geometry>
        	<cylinder length="0.6" radius="0.2"/>
        </geometry>
    </collision>
</link>
```

- link 加入 inertial

```xml
<link name="base_link">
    <visual>
		...
    </visual>
	<collision>
		...
    </collision>
    <inertial>
    	<mass value="10" />  # 设定质量，kg
        <inertia ixx="0.4" ixy="0.0" ixz="0.0" iyy="0.4" iyz="0.0" izz="0.2" />  # 设定转动惯量矩阵
    </inertial>
</link>
```



- joint

```xml
<joint name="right_gripper_joint" type="fixed">
    <origin rpy="0 0 0" xyz="0.2 -0.01 0"/>
    <parent link="gripper_pole"/>
    <child link="right_gripper"/>
</joint>
```

## `<link>`

主要有三个 sub tag

1. **visual**

2. **collision**: 一般是需要 collision geometry and origin 与 visual 的相同，两种情况除外：

   - Quicker processing: collision detection for two meshes is a lot more computational complex than for two simple geometries.
   - Saft zones: To restrict movement close to sensitive equipment. 例如不想机器人的头部被碰到，则可以设定一个稍大的 collision geometry 包住机器人的头部

3. **physical properties**

   为了在 gazebo 中仿真，需要设定一些必要的属性，供 physics engine 使用。

   - 所有 simulated elements 在 link 中都需要一个 **inertial **tag，其中需要设定质量和转动惯量矩阵，从中可以分别计算线性和转动惯量。inertial 是 required for gazebo，其他的 tag 都是可选的。

   - 有时还需要在 link --> collision 下面描述两物体接触时的物理性质 contact efficients，包括 

     - mu - The friction coefficient 静态，与质量有关
     - kp - Stiffness coefficient
     - kd - Dampening coefficient 动态，与速度有关

   - 有时对于 joint 也需要添加类似的 joint --> dynamic，包括

     - friction: Static friction. For prismatic joints, the units are Newtons; For revolving joints, the units are Newton meters
     - damping: Damping value. For prismatic joints, the units are Newton seconds per meter; For revolving joints, Newton meter seconds per radian

     如果没有设定，则默认值为 0.

## `<joint>`

在 `<joint>` 中设置 `<origin xyz="0 -0.22 0.25">` ，这是相对于  parent's reference frame 来说的 offset，这里没写 rpy，就意味着角度与 parent 相同，只是位置处于 parent's y 轴 -0.22 ，z 轴 0.25 的地方。这里的 origin 就是 child link 的 origin。

如果在 child `<visual>` 中没有额外的 origin 设置，则 visual origin 与 link origin 重合

如果在 child `<visual>` 中也设置了 origin ，实际上是基于 joint origin 的 offset。

![joint_origin1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/joint_origin1.png)

除了 `fixed` type，还有如下几种

- type="continuous" ： 可以绕某个轴连续旋转，旋转角度取值可以是正负无穷大。此时需要额外指定旋转轴，例如

  `<axis xyz="0 0 1"`

  就是绕 z 轴旋转

- type="revolute"：与 continuous 几乎相同，只是对旋转加了限制。依然是要指定旋转轴

  `<axis xyz="0 0 1">`

  此外要限制作用力，转角大小和速度 in radians，如下：

  `<limit effort="1000.0" lower="0.0" upper="0.548" velocity="0.5">`

- type="prismatic": 不是沿轴旋转，而是沿轴方向移动，也是有限制，单位是米

  `<limit effort="1000.0" lower="-0.38" upper="0" velocity="0.5">`

## `<material>`

- 可以在所有 Link 外边定义好，然后在 link --> visual 内调用，例如

```xml
    <material name="white">
        <color rgba="1 1 1 1"/>  
    </material>

    <link name="base_link">
        <visual>
            <geometry>
                <cylinder radius="0.2" length="0.6"/>
            </geometry>
            <material name="blue"/>
        </visual>
    </link>
```

- 也可以在 link -- > visual 中内部直接定义，例如

```xml
<link name="link_name">
    <visual>
        <geometry>
            <box size="0.0 0.0 0.0"/>
        </geometry>
        <material name="red">
            <color rgb="1.0 0.0 0.0 1.0"/>
        </material>
    </visual>
</link>
```

这样定义的 material 可以被其他 link 使用。只不过结构没那么清晰了。

## Xacro

### 使用方法

xacro 是一种 macro 语言，可以理解成按照一定的规则替换命令语句，就像 c++ 中的预处理： `#define WEIGHT 10`， 将文件中所有的 WEIGHT 用 10 替换。

当 robot_description 以 xacro 文件的形式呈现时，文件头部要加上一些指示，如

```xml
<?xml version="1.0"?>
<robot name="myrobot" xmlns:xacro="http://www.ros.org/wiki/xacro">
# 这个 url 指向了 ros xacro package
```

可以用 xacro 的方式书写 robot_description，然后用如下两种方式转化成 urdf

- 直接命令行转化，输出 urdf 文件

  ```bash
  rosrun xacro xacro model.xacro > model.urdf
  # 如果不在当前目录下，还可以 rospack 查找
  rosrun xacro xacro "`rospack find robot1_decription`/urdf/robot1.xacro" > "`rospack find robot1_decription`/urdf/robot1_processed.urdf"
  ```

- 在 launch 文件中用 param 中的 command 转化

  ```xml
  <param name="robot_description"
  command="$(find xacro)/xacro '$(find pr2_description)/*.xacro'" />
  ```

  其中 command 返回的是 xacro 转化之后的 urdf 文件，上述命令与下面直接给 urdf 相同

  ```xml
  <arg name="model" default="$(find prius_description)/prius.urdf"/>
  
  <param name="robot_description" textfile="$(arg model)"/>
  ```

  也可以对 urdf 文件用 xacro，只不过没有任何替换过程发生，例如

  ```xml
    <arg name="model" default="$(find urdf_tutorial)/urdf/01-myfirst.urdf"/>
  
    <param name="robot_description" command="$(find xacro)/xacro --inorder $(arg model)" />
  # inorder 是指替换过程按照正常阅读的顺序依次替换。在 Indigo 以后的版本中都是默认的，不必再加。
  ```

用 roslaunch 命令指定 model 参数时，用如下命令：

```bash
roslaunch pkg file_name.launch model:=urdf/*.urdf
或者
roslaunch pkg file_name.launch model:=urdf/*.xacro
```

### 书写方法

#### 简单 constant 替换

- 常数替换

原 urdf 语言

```xml
<link name="base_link">
    <visual>
        <geometry>
        	<cylinder length="0.6" radius="0.2"/>
        </geometry>
    	<material name="blue"/>
    </visual>
	<collision>
    	<geometry>
        	<cylinder length="0.6" radius="0.2"/>
        </geometry>
    </collision>
</link>
```

将上边的常数值设定成变量，然后可以重复调用

```xml
<xarco:property name="width" value="0.2" />
<xarco:property name="bodylen" value="0.6" />

<link name="base_link">
    <visual>
        <geometry>
        	<cylinder length="${width}" radius="${bodylen}"/>
        </geometry>
    	<material name="blue"/>
    </visual>
	<collision>
    	<geometry>
        	<cylinder length="${width}" radius="${bodylen}"/>
        </geometry>
    </collision>
</link>
```

这个 xarco 文件经过 xarco 预处理之后，得到的 urdf 文件与前边的文件完全相同。

- 替换任意内容

不仅仅是常数替换，所有的内容都可以这样批量替换，例如

```xml
<xacro:property name="robotname" value="marvin" />

<link name="${robotname}s_leg" />
```

- 替换数学表达式

总之，`${***}` 会自动查找、替换 curly bracket 里面的内容，如果是运算，则返回运算结果，可以是任意复杂的四则运算的组合，例如

```xml
<cylinder radius="${wheeldiam/2}" length="0.1" />
<origin xyz="${reflect*(width+.02)}  0  0.25" />
```

再如

```xml
<link name="${5/6}" />
```

实际上就对应

```xml
<link name="0.83333333" />
```

#### macro 替换

上述 constant 替换都是简单定义一个 `< xacro:property name="" value="" /> ` 然后在正文中批量替换

macro 替换则可以替换更多的内容，例如替换整个 tag 中的内容

```xml
<xacro:macro name="default_origin">
	<origin xyz="0  0  0" rpy="0  0  0"/>
</xacro:macro>

<xacro:default_origin /> # == <origin xyz="0  0  0" rpy="0  0  0"/>
```

#### parameterized macro

不仅可以 macro 替换，还可以在 macro 中设置参数，这样每次调用 macro 替换时，可以送入不同的参数

- 单个参数

```xml
<xacro:macro name="default_inertial" params="mass" >
	<inertial>
    	<mass value="${mass}"/>
        <inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="1.0" iyz="0.0" izz="1.0" />
    </inertial>
</xacro:macro>

<xacro:default_inertial mass="10"/>  # 替换成上边的 <inertial> block
```

- 多个参数

如下是机器人的两条腿的设计，几乎相同，只有稍微的区别。visual 部分是完全一样的，只是 joint 部分区分了左右腿。

```xml
<xacro:macro name="leg" params="prefix reflect">  # 2 个参数
    <link name="${prefix}_leg">
        <visual>
            <geometry>
                <box size="${leglen} 0.1 0.2"/>
            </geometry>
            <origin xyz="0 0 -${leglen/2}" rpy="0 ${pi/2} 0"/>
            <material name="white"/>
        </visual>
        <collision>
            <geometry>
                <box size="${leglen} 0.1 0.2"/>
            </geometry>
            <origin xyz="0 0 -${leglen/2}" rpy="0 ${pi/2} 0"/>
        </collision>
        <xacro:default_inertial mass="10"/>
    </link>

    <joint name="base_to_${prefix}_leg" type="fixed">
        <parent link="base_link"/>
        <child link="${prefix}_leg"/>
        <origin xyz="0 ${reflect*(width+.02)} 0.25" />
    </joint>
</xacro:macro>
<xacro:leg prefix="right" reflect="1" /> # 右腿
<xacro:leg prefix="left" reflect="-1" /> # 左腿
# 几乎都一样，只是名字前缀不同，并且 joint origin 对称，用正负号调节。
```

- 把 block 作为参数

```xml
<xacro:macro name="blue_shape" params="name *shape">#block para has *
    <link name="${name}">
        <visual>
            <geometry>
                <xacro:insert_block name="shape" /> # 用 insert_block
            </geometry>
            <material name="blue"/>
        </visual>
        <collision>
            <geometry>
                <xacro:insert_block name="shape" />
            </geometry>
        </collision>
    </link>
</xacro:macro>

<xacro:blue_shape name="base_link"> #第一个参数的赋值跟迁移相同，block不同
    <cylinder radius=".42" length=".01" />
</xacro:blue_shape>
```

### 参考

原本 urdf 文件有 417 行 https://github.com/ros/urdf_tutorial/blob/master/urdf/07-physics.urdf

改写成 xacro 文件之后只有 238 行 https://github.com/ros/urdf_tutorial/blob/master/urdf/08-macroed.urdf.xacro

可以发现， xacro 主要是用在相似 link 和 joint 的设置中，例如左右腿，四个轮子等。

## 检测 urdf 正确性

判断一个 urdf 文件语法是否正确，可以用 `check_urdf` 命令，如下：

```bash
check_urdf robot1.urdf
```

![check_urdf1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/check_urdf1.png)

通过 `urdf_to_graphiz` 可以把 robot 对应的 Joint 和 link 用 graph 表示出来，会产生一个 pdf 文件

```bash
urdf_to_graphiz robot1.urdf
```

![urdf_graphiz](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/urdf_graphiz.png)

## 用 rviz 显示 robot

为了用 rviz 显示 robot，joint_state_publisher 和 robot_state_publisher 是必须的。

- joint_state_publisher: 发布各个 joint 运动的信息到 joint_states topic 上，type 为 sensor_msgs/JointState。

  另外，如果在 roslaunch 时将 param  use_gui 设置为 true, 则连带启动一个 joint_state_publisher 的 gui。这个 gui 首先解析 URDF 文件，找到所有 non-fixed joint 和它们的 limit，然后用 gui 呈现出来，里面包含了 sliders， 可以手动滑动调节各个 joint 的数值，这个数值就被 publish 到 joint_states topic 上，驱动机器人的 joint 和连带的机器人本身的运动。

- robot_state_publisher: 接收 joint_states topic 上的信息，然后计算各个部件之间的位置变化，发布 tf。理论上，每个 link 都应该有 tf frame。rviz 就靠这些 tf frame 确定各个 link 的位置。

  先确定 parent link 的 origin，然后通过 joint 确定其 child 的 origin，同理，确定 child 的 child 的 origin。

  然后visual 的 origin 就是前述各 link origin 加上各自的 offset

  如果某个 link 没有 tf，推断不出来其具体位置，则在 rviz 中就被放置在 grid origin 位置，颜色为白色。

![joint_state_publish1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/joint_state_publish1.png)

- 另外，为了让 rviz 显示 robotModel， 还需要设置 robot_description param。

  因此，就算 gazebo 是用 sdf 形式启动 world 和 robot 的，不需要用 spawn，当然不需要调用 ros_description 文件，为了rviz 中显示 model， 这里的 robot_description 依然需要设置。

  ![robot_descritption1](pic/robot_description1.png)


用 rviz 展示 robot 的 display.launch 文件如下：

```xml
<?xml version="1.0"?>
<launch>
	<arg name="model" />
	<arg name="gui" default="False" />
	<param name="robot_description" textfile="$(arg model)" />
	<param name="use_gui" value="$(arg gui)"/>
	<node name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher" ></node>
	<node name="robot_state_publisher" pkg="robot_state_publisher" type="state_publisher" />
	<node name="rviz" pkg="rviz" type="rviz" args="-d $(find urdf_tutorial)/urdf.rviz" />
</launch>
```

然后在命令行中输入如下命令：

```bash
roslaunch robot_description display.launch model:="`rospack find robot_description`/urdf/robot.urdf"
```

另一个 display.launch 文件

```xml
<launch>
	<arg name="model" />
    
	<param name="robot_description" command="$(find xacro)/xacro.py 
	$(find mastering_ros_robot_description_pkg)/urdf/diff_wheeled_robot.xacro" />
	<param name="use_gui" value="true"/>
    
	<!-- Starting Joint state publisher node which will publish the joint values -->
	<node name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher" />
	<!-- Starting robot state publish which will publish tf -->
	<node name="robot_state_publisher" pkg="robot_state_publisher" type="state_publisher" />
    
	<node name="rviz" pkg="rviz" type="rviz" args="-d $(find mastering_ros_robot_description_pkg)/urdf.rviz" required="true" />
</launch>
```

在 rviz 里面不仅可以展示 robot 的外观，也可以展示其动作，通过 joint_state_publisher gui 可以操控各个 movable joint，在 rviz 中就可以运动了。

但是如果要在 Gazebo 中仿真，则还需要额外的设置，例如碰撞、惯性等。

# Gazebo

## 安装特定版本的 Gazebo

Gazebo 主要存放在两个repo中： packages.ros.org 和 packages.osrfoundation.org。

- ROS kinetic 默认是安装 Gazebo 7 的，按照 ROS 官网的步骤，从 ROS repository 安装即可。

- 如果要在 ROS kinetic 上安装 Gazebo 8，需要一些特殊设置：

  Add the osrfoundation repository to your sources list

  ```
  sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
  
  ```

  查看一下

  ```
  cat /etc/apt/sources.list.d/gazebo-stable.list
  # output: deb http://packages.osrfoundation.org/gazebo/ubuntu-stable xenial main
  
  ```

  setup keys

  ```
  wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
  
  ```

  update

  ```
  sudo apt update
  
  ```



## 历史及现状

- Gazebo 目前已经从 ROS 中分离出来。

- Gazebo 完全可以独立运行，不依赖于 ROS 的调用，也可以实现仿真功能。

- 如果要从 ROS 调用 Gazebo，需要一些额外设置，感觉是将 Gazebo 连接到 ROS 中，用 ROS 的那一套命令，加上 Gazebo 接口命令，并且还需要在 Gazebo 文件中加入 ros plugin 以便 publish topic。

- 为了实现 ROS 与 Gazebo 的通讯，ROS 提供了一组 Packages, 称为 gazebo_ros_pkgs，扮演了接口的角色。安装命令：

  ```
  sudo apt-get install ros-kinetic-gazebo-ros-pkgs 
  # 帮助 gazebo 中的 Model 发送和接收 topic
  
  sudo apt-get install ros-kinetic-gazebo-ros-control  
  # 添加高层的控制模块 
  
  ```

- 可以通过 gazebo 本身启动 gazebo `gazebo`

- 也可以通过 ros 启动 gazebo `rosrun gazebo_ros gazebo` 或者only server: `rosrun gazebo gzserver` 

- 如果要启动 gazebo world，可以用 roslaunch 命令。后边会提到。

- Gazebo 底层有一些高性能的 physics engine，例如 open dynamics engine (ODE)，尽量逼近现实物理效果

- Gazebo 用 object-oriented graphics rendering engine (OGRE) 实现 3D 图像的呈现。

  ![gazebo1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/gazebo1.png)

  以上仅包含 gazebo_ros_pkgs。

  另外还有 ros_control packages 提供了通用的 control 接口。



  **Data flow of ros_control and Gazebo**

  ![ros_control1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/ros_control1.png)

## URDF 与 SDF 的区别

- Gazebo 用的描述机器人和环境的语言是 SDF，后缀 .sdf，出现的较晚，书写简单

- ROS 用的是 URDF，后缀 .xacro，只能描述单个机器人。目前由于 ROS 的普及， URDF 书写的文件还比较多，为了在 Gazebo 中使用这些 URDF 文件，可以在 URDF 中加入 gazebo tag ， 从而可以被 Gazebo 识别并自动转化成 SDF 文件. 

- 在修改完 URDF 文件之后，可以通过命令检查能否转化成 SDF `gz sdf -p modified.xacro` 其中 -p 表示 Print 信息

- 目前常见的运行模式是

  - 在 roslaunch 中一部分是调用 world 文件，world 文件用 SDF 语言书写

  - 另外一部分是调用关键 model, 用 URDF 语言书写，里面添加了额外的 tag，常见格式如下：

    ```xml
      <param name="robot_description" command="$(find xacro)/xacro.py '$(find mybot_description)/urdf/mybot.xacro'"/>
    
      <node name="mybot_spawn" pkg="gazebo_ros" type="spawn_model" output="screen"
       args="-urdf -param robot_description -model mybot" />
    ```

## Gazebo启动时的环境设置

- 从下边的环境、 worlds、models 可以看出来，Gazebo 软件本身没有任何 ROS 的印记。
- 与 ROS 相关的 packages 都在 ROS 路径下，例如 gazebo_ros 和各种启动 ROS 用到的 launch 文件

gazebo的环境设置在文件`/usr/share/gazebo-8/setup.sh`中，可以看到里面有如下内容：

```
export GAZEBO_MASTER_URI=http://localhost:11345
export GAZEBO_MODEL_DATABASE_URI=http://models.gazebosim.org
export GAZEBO_RESOURCE_PATH=/usr/share/gazebo-8:${GAZEBO_RESOURCE_PATH}
export GAZEBO_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/gazebo-8/plugins:${GAZEBO_PLUGIN_PATH}
export GAZEBO_MODEL_PATH=/usr/share/gazebo-8/models:${GAZEBO_MODEL_PATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/lib/x86_64-linux-gnu/gazebo-8/plugins
export OGRE_RESOURCE_PATH=/usr/lib/x86_64-linux-gnu/OGRE-1.9.0

```

## Gazebo 中的 built-in worlds

world 文件后缀名是 .world，语法格式是 SDF 格式，因为 URDF 只能描述单个机器人，无法描述环境信息。built-in world 都放在了`/usr/share/gazebo-8/`中，如下：

```
acronis@acronis-G501VW:/usr/share/gazebo-8/worlds$ ls
actor.world                       empty_sky.world             joints.world                   
animated_box.world                empty.world                 lensflare_plugin.world         
attach_lights.world               everything.world            lift_drag_demo.world          
blank.world                       fiducial.world              lights.world                   
blink_visual.world                flocking.world              linear_battery_demo.world      
cafe.world                        force_torque_demo.world     logical_camera.world           
camera.world                      friction_demo.world         lookat_demo.world              
cart_demo.world                   gripper.world               magnetometer.world             

```

## Gazebo中的 built-in models

在每个 world 中，一般都会放置一些物体，就是models。

当运行某个 world 时，如果model已经下载到本地了，则直接加载，如果没有，则从`http://models.gazebosim.org` 上下载，因此，某些 world 运行时初始加载会需要一些时间。

下载的model都放在了 `~/.gazebo/models/`中。

## 通过 gui 构造简单 world

world 是 model 的集合，也包含了全局参数，如阳光，天空等。

static entities: all objects which are not meant to move. They only have collision geometry.

dynamic entities: which have both inertia and a collision geometry.

通过 `gazebo` 命令启动一个空的 world.

两种添加 model 的方法

1. 用 window 上方的工具栏，添加简单的几何体，包括立方体，球体和圆柱体，each shade is of unit size: 1 meter.

   ![buildworld1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/buildworld1.png)

2. 通过 model database 添加。通过路径可以知道 model 是在本地还是在 online database. 如果在 Online database, 点击完之后需要稍等一会，等它下载下来就可以拖到 world 中去了。下载下来的 model 都存放在了 ~/.gazebo/models 文件夹下。

   ![buildworld2](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/buildworld2.png)

- 添加 model 之后，可以通过窗口上方的工具栏调整其位置、大小、角度。对于某些 online database 下载下来的模型，大小好像不能调节。

- 删除 model，可以选中它，直接按 `delete` 键。

- 保存 world 就是普通的保存

- 装载 world 需要在启动 gazebo 的时候选择要启动的 world，要给定路径或者是在当前文件夹下

  `gazebo myworld`

- 修改scene properties 场景属性

在左侧栏中有world --> scene 环境（较低的部分）和背景（较高的部分，貌似就是天空的部分），主要是调节其颜色。



## Gazebo 与 ROS 结合

### 常见 package 架构

一般的文档架构是：在 ROS workspace 下至少包含两个 package

- myrobot_description: 包含了 robot model description，主要是 ROS 中的文件和必要的 Gazebo plugin 
- myrobot_gazebo: 包含了 world file 以及启动 Gazebo 必要的文件

例如：

```
../catkin_ws/src
    /MYROBOT_description
        package.xml
        CMakeLists.txt
        /urdf
            MYROBOT.urdf
        /meshes
            mesh1.dae
            mesh2.dae
            ...
        /materials
        /cad
    /MYROBOT_gazebo
        /launch
            MYROBOT.launch
        /worlds
            MYROBOT.world
        /models
            world_object1.dae
            world_object2.stl
            world_object3.urdf
        /materials
        /plugins

```

### 实例

#### launch file

借用 empty_world.launch 文件，只修改其中的 world 文件，构造新的 gas.launch file

```xml
<launch>
    <include file="$(find gazebo_ros)/launch/empty_world.launch">
        <arg name="world_name" value="$(find gas_gazebo)/worlds/gas.world"
    </include>
</launch>
```

#### world file

向 world file 中添加简单的 Model  

```xml
<?xml version="1.0"?>
<sdf version="1.4">
    <world name="default">
        <include>
            <uri>model://ground_plane</uri>
        </include>
        <include>
            <uri>model://sun</uri>
        </include>
        <include>
            <uri>model://gas_station</uri>
            <name>gas_station</name>
            <pose>-2.0 7.0 0 0 0 0</pose>
        </include>
    </world>
</sdf>

```

最后用 roslaunch 命令启动 launch 文件，就可以调用 world 文件了

```bash
roslaunch gas_gazebo gas.launch 
```

尽管 launch 文件不再 gas_gazebo package 的直接目录下，而是在 package/launch 文件夹下，依然可以自动搜索到。如果在 Package 中有两个 launch 文件，就无法自动补齐了，而是需要指定。

## 向 sdf model 中添加 ROS Plugin	

所有 gazebo ros plugin 的源代码都可以在 gazebo_ros_pkgs 中查看，例如 diff_drive 源文件的路径如下

`gazebo_ros_pkgs/gazebo_plugins/src/gazebo_ros_diff_drive.cpp`

### 添加 depth camera plugin 

gazebo 中可用的 depth camera 有很多，这里选择 Microsoft kinect。设置过程对于其他 depth camera 也是相同的。

由于 gazebo 与 ROS 是完全独立的两个系统，谁都可以不依赖谁。 gazebo 的 plugin 本身并没有包含 ROS 接口。因此，我们需要对基本的 gazebo plugin 改造，加入 ROS plugins，使其 publish topic to ROS.

1. 下载 gazebo 基础 model kinect 至 /.gazebo/models 文件夹

2. 将 kinect 文件夹改成 meaningful name，例如 kinect_ros, 同时要修改名字的地方有.config文件和 .sdf 文件

3. 将以下的 plugin 加入 .sdf 文件中的 `<sensor> `中，紧跟在 `</camera>` 后边

   ```xml
   <plugin name="camera_plugin" filename="libgazebo_ros_openni_kinect.so">
             <baseline>0.2</baseline>
             <alwaysOn>true</alwaysOn>
             <!-- Keep this zero, update_rate as the parent <sensor> -->
             <updateRate>0.0</updateRate>
             <cameraName>camera_ir</cameraName>
             <imageTopicName>/camera/depth/image_raw</imageTopicName>
             <cameraInfoTopicName>/camera/depth/camera_info</cameraInfoTopicName>
             <depthImageTopicName>/camera/depth/image_raw</depthImageTopicName>
       	<depthImageInfoTopicName>/camera/depth/camera_info</depthImageInfoTopicName>
             <pointCloudTopicName>/camera/depth/points</pointCloudTopicName>
             <frameName>camera_link</frameName>
             <pointCloudCutoff>0.05</pointCloudCutoff>
             <distortionK1>0</distortionK1>
             <distortionK2>0</distortionK2>
             <distortionK3>0</distortionK3>
             <distortionT1>0</distortionT1>
             <distortionT2>0</distortionT2>
             <CxPrime>0</CxPrime>
             <Cx>0</Cx>
             <Cy>0</Cy>
             <focalLength>0</focalLength>
             <hackBaseline>0</hackBaseline>
   </plugin>
   ```

   其中，

   - updateRate 设置为0， rate 就是 sensor 的 rate，不再额外加限制
   - topicName 和 frameName 可以任意设置，但是最好跟大众命名以及实际硬件相同，移植比较方便
   - distortion 的参数应该与 parent sensor 匹配，如果 Parent 中没有提到 distortion, 则全部设置为 0

4. 在 gazebo 中 insert camera，设置模拟场景。在 rostopic list 中应该能看到刚才设置的 topic names

5. 启动 rviz： `rosrun rviz rviz`，设置 Fixed frame == frameName in plugin, 添加 PointCloud2 和 Image  数据类型，分别接收两个 topic。

   注意：rviz 中的 Fixed Frame 对应的那个 frame 原点就是 rviz 中地面 grid 的原点，一般设置成 map 或者 world。target frame 是镜头跟随的那个 frame，如果设置成 base_link，镜头就跟随 robot base_link frame 移动，或者说车辆始终在屏幕/镜头中心，而环境是在变动的。比如在 autoware 的设置中，fixed frame == world, base_link frame == base_link，得到的就是跟随车辆运动的镜头。

### 添加 camera

```xml
      <plugin name="camera_controller" filename="libgazebo_ros_camera.so">
        <alwaysOn>true</alwaysOn>
        <updateRate>0.0</updateRate>
        <cameraName>anyname/camera1</cameraName>
        <imageTopicName>image_raw</imageTopicName>
        <cameraInfoTopicName>camera_info</cameraInfoTopicName>
        <frameName>camera_link</frameName>
        <hackBaseline>0.07</hackBaseline>
        <distortionK1>0.0</distortionK1>
        <distortionK2>0.0</distortionK2>
        <distortionK3>0.0</distortionK3>
        <distortionT1>0.0</distortionT1>
        <distortionT2>0.0</distortionT2>
      </plugin>
```

## 向 urdf model 中添加 Gazebo plugin

### 基本的添加

为了让 ROS 的 urdf 文件在 gazebo 中仿真，要满足以下条件：

**Required**:

- 每个 link 都要有 inertial 
- 每个 link 都要有 collision

**Optional**: 为 robot, link, joint 添加 gazebo tag

- 为每个 link 添加 gazebo tag，这是为了
  - convert visual colors to Gazebo format
  - convert STL file (只支持 color) to dae file (既支持 color 又支持 texture) for better textures
  - **add sensor plugins**
- 为每个 joint 添加 gazebo tag，这是为了
  - set proper damping dynamic
  - **add actuator control plugins**
- 为 robot tag 添加一个 gazebo tag

上述 gazebo tag 的主要目的就是添加在 sdf 中存在但 urdf 中不存在的 properties.

另外，如果希望 model 固定在 world/base_link 中，就像那个双摆，就需要再定义一个 link 名为 world，然后将 model 通过 fixed joint 固定到 world 上，如下

```xml
<link name="world"/>

<joint name="fixed" type="fixed">
    <parent link="world"/>
    <child link="link1"/>
</joint>

<link name="link1" >
	...
</link>
```

**注意**： 上述的添加都不影响原本 ROS 的功能，包括 rviz 的功能，这是很好的一方面。

### 常见架构

四个关键文件：

1. launch 文件：启动 world + model

   例如：

   ```xml
   <launch>
     <arg name="paused" default="false"/>
     <arg name="use_sim_time" default="true"/>
     <arg name="gui" default="true"/>
     <arg name="headless" default="false"/>
     <arg name="debug" default="false"/>
   
     <include file="$(find gazebo_ros)/launch/empty_world.launch">
       <arg name="world_name" value="$(find rrbot_gazebo)/worlds/rrbot.world"/>
       <arg name="debug" value="$(arg debug)" />
       <arg name="gui" value="$(arg gui)" />
       <arg name="paused" value="$(arg paused)"/>
       <arg name="use_sim_time" value="$(arg use_sim_time)"/>
       <arg name="headless" value="$(arg headless)"/>
     </include>
   
     <!-- Load the URDF into the ROS Parameter Server -->
     <param name="robot_description"
       command="$(find xacro)/xacro --inorder '$(find rrbot_description)/urdf/rrbot.xacro'" />
   
     <!-- Run a python script to send a service call to gazebo_ros to spawn a URDF robot -->
     <node name="urdf_spawner" pkg="gazebo_ros" type="spawn_model" respawn="false" output="screen"
       args="-urdf -model rrbot -param robot_description"/>
   
   </launch>
   ```

2. world 文件：包括环境和 model 之外的物体

   例如：

   ```xml
   <?xml version="1.0" ?>
   <sdf version="1.4">
   
     <world name="default">
       <include>
         <uri>model://ground_plane</uri>
       </include>
   
       <!-- Global light source -->
       <include>
         <uri>model://sun</uri>
       </include>
   
       <!-- Focus camera on tall pendulum -->
       <gui fullscreen='0'>
         <camera name='user_camera'>
           <pose>4.927360 -4.376610 3.740080 0.000000 0.275643 2.356190</pose>
           <view_controller>orbit</view_controller>
         </camera>
       </gui>
   
     </world>
   </sdf>
   ```

3. model.xacro 文件：model 文件，其中 include model.gazebo 文件

   例如：

   ```xml
   <?xml version="1.0"?>
   <robot name="rrbot" xmlns:xacro="http://www.ros.org/wiki/xacro">
   
     <!-- Import all Gazebo-customization elements, including Gazebo colors -->
     <xacro:include filename="$(find rrbot_description)/urdf/rrbot.gazebo" />
   
    # 这个 materials 文件没什么用，Gazebo 中需要重新设定每个 link 的 color
     <xacro:include filename="$(find rrbot_description)/urdf/materials.xacro" />
   
     <!-- Used for fixing robot to Gazebo 'base_link' -->
     <link name="world"/>
   
     <joint name="fixed" type="fixed">
       <parent link="world"/>
       <child link="link1"/>
     </joint>
   ```

   其中

   ```xml
     <link name="world"/>
   
     <joint name="fixed" type="fixed">
       <parent link="world"/>
       <child link="link1"/>
     </joint>
   ```

   就是设定 model 永远固定于 world frame 上，也就是 ground plane 。

   如果是移动 model，则不需要这一部分设定。

4. model.gazebo 文件：包含了所有的 gazebo tag，例如关于 robot 全体的 plugin，关于颜色的 plugin，关于 link/joint 的 sensor/actuator plugins 等。

   例如：

   ```xml
   <?xml version="1.0"?>
   <robot>
   
     <!-- ros_control plugin -->
     <gazebo>
       <plugin name="gazebo_ros_control" filename="libgazebo_ros_control.so">
         <robotNamespace>/rrbot</robotNamespace>      <robotSimType>gazebo_ros_control/DefaultRobotHWSim</robotSimType>
       </plugin>
     </gazebo>
   
     <!-- Link1 -->
     <gazebo reference="link1">
       <material>Gazebo/Orange</material>
     </gazebo>
   
     <!-- Link2 -->
     <gazebo reference="link2">
       <mu1>0.2</mu1>
       <mu2>0.2</mu2>
       <material>Gazebo/Black</material>
     </gazebo>
   ```

### `<gazebo>`

如果 gazebo tag 中如果没有标明 reference=""，就认为其中的 elements 对于整个 robot 都适用，如果表明了 reference="link1" 那么只对那个 reference 有效。

这个网页给出来了一些 element 的例子 http://gazebosim.org/tutorials/?tut=ros_urdfelement 。有些 element 是解释性质的，方便 urdf 向 sdf 转化，有些  element 则直接塞进 sdf 的 `<model>`  tag 中，成为了 sdf 的 element. 

### plugins

#### RGB camera

需要添加的主要是两部分

1. 在 robot_description package 的 model 描述文件中 (例如 model.xacro)，添加 camera link 以及它的 fixed joint

   ```xml
   <joint name="camera_joint" type="fixed">
       <axis xyz="0 1 0" />
       <origin xyz="${camera_link} 0 ${height3 - axel_offset*2}" rpy="0 0 0"/>
       <parent link="link3"/>
       <child link="camera_link"/>
   </joint>
   
   <!-- Camera -->
   <link name="camera_link">
       <collision>
           <origin xyz="0 0 0" rpy="0 0 0"/>
           <geometry>
               <box size="${camera_link} ${camera_link} ${camera_link}"/>
           </geometry>
       </collision>
   
       <visual>
           <origin xyz="0 0 0" rpy="0 0 0"/>
           <geometry>
               <box size="${camera_link} ${camera_link} ${camera_link}"/>
           </geometry>
           <material name="red"/>
       </visual>
   
       <inertial>
           <mass value="1e-5" />
           <origin xyz="0 0 0" rpy="0 0 0"/>
           <inertia ixx="1e-6" ixy="0" ixz="0" iyy="1e-6" iyz="0" izz="1e-6" />
       </inertial>
   </link>
   ```

2. 依然是 robot_description package 的 gazebo plugin 描述文件中 (例如 model.gazebo)，添加 gazebo plugin

   ```xml
   # 关于外观的设定
   <gazebo reference="camera_link">
       <mu1>0.2</mu1>
       <mu2>0.2</mu2>
       <material>Gazebo/Red</material>
   </gazebo>
   
   <!-- camera -->
   <gazebo reference="camera_link">
       <sensor type="camera" name="camera1">  # name must be unique
           <update_rate>30.0</update_rate>  # frames per sec
           <camera name="head">  # 设定参数，尽量贴近硬件实际
               <horizontal_fov>1.3962634</horizontal_fov> 
               # FOV=field of view，一般以角度表示，视角范围
               <image>
                   <width>800</width>
                   <height>800</height>
                   <format>R8G8B8</format>
               </image>
               <clip>
                   <near>0.02</near>
                   <far>300</far>
               </clip>
               <noise>
                   <type>gaussian</type>
                   <!-- Noise is sampled independently per pixel on each frame.That pixel's noise value is added to each of its color
   channels, which at that point lie in the range [0,1]. -->
                   <mean>0.0</mean>
                   <stddev>0.007</stddev>
               </noise>
           </camera>
           <plugin name="camera_controller" filename="libgazebo_ros_camera.so">
               <alwaysOn>true</alwaysOn>
               <updateRate>0.0</updateRate>
               <cameraName>rrbot/camera1</cameraName>
               <imageTopicName>image_raw</imageTopicName>
               <cameraInfoTopicName>camera_info</cameraInfoTopicName>
               # 最后的 topic 是 rrbot/camera1/image_raw
                                rrbot/camera1/camera_info
               <frameName>camera_link</frameName>
               # camera 的 coordinate frame in tf
               <!-- setting hackBaseline to anything but 0.0 will cause a misalignment between the gazebo sensor image and the frame it is supposed to be attached to -->
               <hackBaseline>0.0</hackBaseline>
               <distortionK1>0.0</distortionK1>
               <distortionK2>0.0</distortionK2>
               <distortionK3>0.0</distortionK3>
               <distortionT1>0.0</distortionT1>
               <distortionT2>0.0</distortionT2>
               <CxPrime>0</CxPrime>
               <Cx>0.0</Cx>
               <Cy>0.0</Cy>
               <focalLength>0.0</focalLength>
           </plugin>
       </sensor>
   </gazebo>
   ```

#### hokuyo laser scanner

步骤与 camera 相同

1. 在 description 文件中添加 Link 和 joint

   ```xml
   <joint name="hokuyo_joint" type="fixed">
       <axis xyz="0 1 0" />
       <origin xyz="0 0 ${height3 - axel_offset/2}" rpy="0 0 0"/>
       <parent link="link3"/>
       <child link="hokuyo_link"/>
   </joint>
   
   <!-- Hokuyo Laser -->
   <link name="hokuyo_link">
       <collision>
           <origin xyz="0 0 0" rpy="0 0 0"/>
           <geometry>
               <box size="0.1 0.1 0.1"/>
           </geometry>
       </collision>
   
       <visual>
           <origin xyz="0 0 0" rpy="0 0 0"/>
           <geometry>
               <mesh filename="package://rrbot_description/meshes/hokuyo.dae"/>
           </geometry>
       </visual>
   
       <inertial>
           <mass value="1e-5" />
           <origin xyz="0 0 0" rpy="0 0 0"/>
           <inertia ixx="1e-6" ixy="0" ixz="0" iyy="1e-6" iyz="0" izz="1e-6" />
       </inertial>
   </link>
   ```

2. 在 .gazebo 文件中添加 plugin

   ```xml
   <!-- hokuyo -->
   <gazebo reference="hokuyo_link">
       <sensor type="gpu_ray" name="head_hokuyo_sensor">
           <pose>0 0 0 0 0 0</pose>
           <visualize>false</visualize>  # 激光射线是否可见
           <update_rate>40</update_rate>
           <ray>
               <scan>
                   <horizontal>
                       <samples>720</samples>
                       <resolution>1</resolution>
                       <min_angle>-1.570796</min_angle>
                       <max_angle>1.570796</max_angle>
                   </horizontal>
               </scan>
               <range>
                   <min>0.10</min>
                   <max>30.0</max>
                   <resolution>0.01</resolution>
               </range>
               <noise>
                   <type>gaussian</type>
                   <!-- Noise parameters based on published spec for Hokuyo laser achieving "+-30mm" accuracy at range < 10m.  A mean of 0.0m and stddev of 0.01m will put 99.7% of samples within 0.03m of the true reading. -->
                   <mean>0.0</mean>
                   <stddev>0.01</stddev>
               </noise>
           </ray>
           <plugin name="gazebo_ros_head_hokuyo_controller" filename="libgazebo_ros_gpu_laser.so">
               <topicName>/rrbot/laser/scan</topicName>
               <frameName>hokuyo_link</frameName>
           </plugin>
       </sensor>
   </gazebo>
   ```

#### IMU

```xml
<gazebo reference="imu_link">
    <gravity>true</gravity>
    <sensor name="imu_sensor" type="imu">
        <always_on>true</always_on>
        <update_rate>100</update_rate>
        <visualize>true</visualize>
        <topic>__default_topic__</topic>
        <plugin filename="libgazebo_ros_imu_sensor.so" name="imu_plugin">
            <topicName>imu</topicName>
            <bodyName>imu_link</bodyName>
            <updateRateHZ>10.0</updateRateHZ>
            <gaussianNoise>0.0</gaussianNoise>
            <xyzOffset>0 0 0</xyzOffset>
            <rpyOffset>0 0 0</rpyOffset>
            <frameName>imu_link</frameName>
        </plugin>
        <pose>0 0 0 0 0 0</pose>
    </sensor>
</gazebo>
```

#### Differential drive

两轮差分驱动，可以接受来自高层的 cmd_vel 命令，然后转化成左右轮的转动，输出 odom 信息。

注意：必须有一个 well-defined 的 differential driving 的 robot 才能使用这个，并不是随便一个轮式机器人加上去就能用的。这个可以参考后边的 Project 1 中 differential wheeled robot 的构造方式。

```xml
<gazebo>
    <plugin name="differential_drive_controller" filename="libgazebo_ros_diff_drive.so">
        <alwaysOn>true</alwaysOn>
        <updateRate>${update_rate}</updateRate>
        <leftJoint>base_link_right_wheel_joint</leftJoint>
        <rightJoint>base_link_left_wheel_joint</rightJoint>
        <wheelSeparation>0.5380</wheelSeparation>
        <wheelDiameter>0.2410</wheelDiameter>
        <torque>20</torque>
        <commandTopic>cmd_vel</commandTopic>
        <odometryTopic>odom</odometryTopic>
        <odometryFrame>odom</odometryFrame>
        <robotBaseFrame>base_footprint</robotBaseFrame>
    </plugin>
</gazebo>
```

#### skid steering drive

四轮差分驱动

```xml
<gazebo>
  <plugin name="skid_steer_drive_controller" filename="libgazebo_ros_skid_steer_drive.so">
    <updateRate>100.0</updateRate>
    <robotNamespace>/</robotNamespace>
    <leftFrontJoint>front_left_wheel_joint</leftFrontJoint>
    <rightFrontJoint>front_right_wheel_joint</rightFrontJoint>
    <leftRearJoint>back_left_wheel_joint</leftRearJoint>
    <rightRearJoint>back_right_wheel_joint</rightRearJoint>
    <wheelSeparation>0.4</wheelSeparation>
    <wheelDiameter>0.215</wheelDiameter>
    <robotBaseFrame>base_link</robotBaseFrame>
    <torque>20</torque>
    <topicName>cmd_vel</topicName>
    <broadcastTF>false</broadcastTF>
  </plugin>
</gazebo>
```

这两个 drive plugin 本质上就是接收 /cmd_vel topic，然后移动一定的距离，然后 publish /odom topic

## ros_control package

原本是 pr2 中的 Packages，后来被重写，对所有 robot 通用。

包含了一系列的 packages，如 

- controller interface
- controller manager
- transmission
- hardware_interface

安装

```bash
sudo apt-get install ros-kinetic-ros-control ros-kinetic-ros-controllers
```

![ros_control2](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/ros_control2.png)

## 用 roslaunch 启动 world

自定义 gazebo 仿真环境可以依照如下步骤：

- 采用 sdf 文件构造 world 和 model

roslaunch --> 修改 launch 文件，替换其中的 world -->  修改 world 文件，替换其中的 model 文件 --> 修改 model.sdf 文件，添加 ROS plugin，从而可以 publish topic

- 采用 sdf 文件构造 world,  urdf 文件构造 model

roslaunch --> 修改 launch 文件，替换其中的 world 同时修改 model 对应的 urdf 文件。相当于  world 与 model 分开加载。其中 urdf 文件需要加载 gazebo tag 和 plugin

后一种情况的常见格式如下：

```xml
<launch>

  <arg name="paused" default="false"/>
  <arg name="use_sim_time" default="true"/>
  <arg name="gui" default="true"/>
  <arg name="headless" default="false"/>
  <arg name="debug" default="false"/>
  <arg name="model" default="$(find urdf_tutorial)/urdf/08-macroed.urdf.xacro"/>

  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="debug" value="$(arg debug)" />
    <arg name="gui" value="$(arg gui)" />
    <arg name="paused" value="$(arg paused)"/>
    <arg name="use_sim_time" value="$(arg use_sim_time)"/>
    <arg name="headless" value="$(arg headless)"/>
  </include>
    
    
# world 与 model 分开加载， model 不再是 world 中的一部分
  <param name="robot_description" command="$(find xacro)/x  acro $(arg model)" /># 老版本中用 $(find *)/xacro.py，现在直接用 $(find *)/xacro即可
# 不论 gazebo 以 sdf 还是 urdf 方式启动 robot, 这里的 robot_description 都是必须设置的，因为 rviz 需要它！
    
  <!-- spawn robot in gazebo -->
  <node name="urdf_spawner" pkg="gazebo_ros" type="spawn_model"
        args="-z 1.0 -unpause -urdf -model robot -param robot_description" respawn="false" output="screen" />

    
  <node pkg="robot_state_publisher" type="robot_state_publisher"  name="robot_state_publisher">
    <param name="publish_frequency" type="double" value="30.0" />
  </node>  
# robot_state_publisher 用来接收 joint_state 信息，然后 Publish tf 信息
    
  <node name="rviz" pkg="rviz" type="rviz" args="-d $(find rrbot_description)/launch/rrbot.rviz"/>
  # rviz  -d  *.rviz   -d 表示 display-config，就是加载 config 文件

</launch>
```

上述文件主要包含 3 部分：

1. **启动 empty_world**，这里没有选用其他 world，直接用了最基本的

2. **spawn model**: spawn 文件是个 python 文件，在 /opt/ros/kinetic/lib/gazebo_ros 下，如果单独启动这个程序，可以用如下命令：

   ```bash
   rosrun gazebo_ros spawn_model -file `rospack find myrobot_description`/urdf/myrobot.urdf -urdf -x 0 -y 0 -z 1 -model myrobot  # 注意：不是单引号，是左上角那个
   ```

   注意：单独启动这个程序并不能打开 gazebo，正确的方式是先运行 gazebo，把 world 建立起来，然后运行这个程序，可以在 world 中实时的产生 robot model.

   如果放入 launch 文件，则对应的语句为

   ```xml
   <node name="spawn_urdf" pkg="gazebo_ros" type="spawn_model"
           args="-file $(find myrobot_description)/urdf/myrobot.urdf -urdf -x 0 -y 0 -z 1 -model myrobot" />
   ```

   其常用参数如下：

   - -urdf/sdf : 指明导入的 xml 文件是 urdf 格式还是 sdf 格式
   - -file/param + file_name/param_name: xml 文件来源，文件或者存在了 param 中
   - -model + model_name: 生成的 model 名字
   - -x/y/z/R/P/Y: 初始 pose

3. **启动 robot_state_publisher** node 发布 tf 信息



### 常用 world 的 launch 文件

- empty world 

```
roslaunch gazebo_ros empty_world.launch

```

启动 empty world, 什么都没有，只有交于原点的三个轴线。

**似乎所有的 launch 文件都是以 empty_world.launch 为模板，很多复杂的变量都直接继承，主要修改 world 文件名字。**

- other worlds

```
roslaunch gazebo_ros willowgarage_world.launch  # 一个类似办公室的环境，很多小隔间

```

![willowgarage](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/willowgarage1.png)

```
roslaunch gazebo_ros mud_world.launch  # 3 个双摆

```

![mud1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/mud1.png)

```
roslaunch gazebo_ros shapes_world.launch  # 几何体

```

![shapes1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/shapes1.png)

```
roslaunch gazebo_ros rubble_world.launch  # 一堆瓦砺材料

```

![rubble1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/rubble1.png)





### 常见参数

一般调用格式  `parameter:=value`

- **paused (false)**: 是否以暂停的状态启动，即暂时不进行 dynamics engine 的作用
- **use_sim_time (true)**: get Gazebo-published simulation time, published over the ROS topic `/clock`，默认 true。但似乎有些情况下 sim time 和 real time 是一样的，所以不管 true or false， /clock 中的数值都是一样的。
- **gui (true)**: launch the user interface window of Gazebo
- **recording (false)**: 老版本中是 headless, enable gazebo state log recording
- **debug (false)**: start gzserver in debug mode
- **verbose (false)**: printing errors and warnings to the terminal

一般情况下， default value 就是我们需要的。

### launch 文件分析

- empty.launch 文件中设置比较多。
- 其他 .launch 文件大多是 empty_world.launch 基础上替换了 world name，例如下边的是用 mud.world. 
- gazebo_ros 自带的 launch 文件后边跟的那些 arg 也是相同的
- 可以认为下边这个 launch 文件就是自己写 launch 的模板。对于简单的仿真环境，一般只改 world name 就行。
- 模板 1： 来自 gazebo_ros pkg 自带的各类 .launch 文件

```xml
<launch>

  <!-- We resume the logic in empty_world.launch, changing only the name of the world to be launched -->
  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="world_name" value="worlds/mud.world"/> 
    <!-- Note: the world_name is with respect to GAZEBO_RESOURCE_PATH environmental variable。如果是我们新建的 world 文件，放在了自己的 workspace 中，则可以用 $(find package_name) 设置路径“$(find *)/worlds/*.world” -->
    <arg name="paused" value="false"/>
    <arg name="use_sim_time" value="true"/>
    <arg name="gui" value="true"/>  # 是否打开 gazebo 窗口，一般都是 true
    <arg name="headless" value="false"/> <!-- Inert - see gazebo_ros_pkgs issue #491 -->
    <arg name="recording" value="false"/>
    <arg name="debug" value="false"/>
  </include>

</launch>
```

- 模板 2：来自网上的实例，似乎更接近工程中的实际标准。有如下几个特点：
  - 文件头添加了 xml 版本标记，感觉更加完整
  - 在给 Include 进来的文件设定参数时，并不是直接设置具体参数，而是先统一定义参数变量的默认值，然后再给各个参数赋值。这样的好处是，用户可以指定数值，如果不指定就用默认值，更加灵活。所以一般来说，只要 launch 文件中用到了 arg ，都应该用这种形式定义，因为之所以有这个 arg 就是有可能被用户额外设置的参数。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<launch>

  <arg name="world" default="empty"/> 
  <arg name="paused" default="false"/>
  <arg name="use_sim_time" default="true"/>
  <arg name="gui" default="true"/>
  <arg name="headless" default="false"/>
  <arg name="debug" default="false"/>
  
  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="world_name" value="$(find mybot_gazebo)/worlds/mybot.world"/>
    <arg name="paused" value="$(arg paused)"/>
    <arg name="use_sim_time" value="$(arg use_sim_time)"/>
    <arg name="gui" value="$(arg gui)"/>
    <arg name="headless" value="$(arg headless)"/>
    <arg name="debug" value="$(arg debug)"/>
  </include>

</launch>
```



### world 文件分析

以 mud.world 为例

```xml
  <sdf version="1.4">
    <world name="default">
      <include>
        <uri>model://sun</uri>
      </include>
      <include>
        <uri>model://ground_plane</uri>
      </include>
      <include>
        <uri>model://double_pendulum_with_base</uri>
        <name>pendulum_thick_mud</name>
        <pose>-2.0 0 0 0 0 0</pose>
      </include>
      ...
    </world>
  </sdf>
```

上述片段中 include 3 个 model，启动时首先在本地 model database 中搜索，如果没有就自动从 Gazebo 在线 database 下载。

# 常见传感器的仿真

注意，这里是仿真，不是如何连接传感器与 ROS。后者在 ROS 有专门介绍。

## Velodyne Lidar

1. 首先安装模拟器： `sudo apt install ros-kinetic-velodyne-simulator`
2. 在 Gazebo 中启动一个简单的例子：`roslaunch velodyne_description example.launch`

## laser scanner

1. 安装 package： `sudo apt install ros-kinetic-hector-gazebo-plugins`。  `hector-gazebo-plugins`package 包含了多种 sensor 的 gazebo 插件，有 laser, IMU, GPS, sonar等, 安装它之后就可以在 gazebo 中调用 sensor 了
2. 借助`sensor_sim_gazebo` package，可以在 gazebo 中启动，也可以同时启动 rviz 查看 data

`roslaunch sensor_sim_gazebo laser.launch`

## camera

1. 单目： `roslaunch sensor_sim_gazebo camera.launch`
2. 立体： `roslaunch sensor_sim_gazebo stereo_camera.launch`

## GPS

`roslaunch sensor_sim_gazebo gps.launch`

## IMU

`roslaunch sensor_sim_gazebo imu.launch`

## Ultrasonic sensor

`roslaunch sensor_sim_gazebo sonar.launch`

# ROS + Gazebo: sensor + manually move

from 《effective robotics programming with ros - third edition》

这一部分是一个具体的 project, 关于 URDF 文件在 gazebo 中仿真，实现 gazebo 与 ros 闭环系统需要加入的环节，以及常见的步骤。

这里以  **laser scanner** 为例。

## 预装

- gazebo

- `sudo apt install ros-kinetic-ros-pkgs ros-kinetic-gazebo-ros-control`

  注意：这里是 gazebo_ros_control 而不是 ros_control，因此不需要用到 transmission plugin

## 步骤

1. 修改 urdf 添加 collision 和 inertial. 可以用 xarco格式改写。

2. 创建 launch 文件如下：

   ```xml
   <?xml version="1.0"?>
   <launch>
   
     <arg name="paused" default="true"/>  # 初始状态是 pause 的
     <arg name="use_sim_time" default="false"/>
     <arg name="gui" default="true"/>
     <arg name="headless" default="false"/>
     <arg name="debug" default="true"/>
   
      <include file="$(find gazebo_ros)/launch/empty_world.launch"> 
        <arg name="world_name" value="$(find robot1_gazebo)/worlds/robot.world"/>
       <arg name="debug" value="$(arg debug)" />
       <arg name="gui" value="$(arg gui)" />
       <arg name="paused" value="$(arg paused)"/>
       <arg name="use_sim_time" value="$(arg use_sim_time)"/>
       <arg name="headless" value="$(arg headless)"/>
     </include>
   
     <arg name="model" />
     <param name="robot_description" 
   	 command="$(find xacro)/xacro.py $(arg model)" />
   
      <node name="urdf_spawner" pkg="gazebo_ros" type="spawn_model" respawn="false" output="screen"
   	args="-urdf -model robot1 -param robot_description -z 0.05"/> 
   
   </launch>
   
   ```

3. 用如下命令启动 launch 文件

   ```bash
   roslaunch robot1_gazebo gazebo.launch model:="`rospack find robot_description`/urdf/robot1_base_01.xacro"
   ```

   目前为止，可以看到 gazebo 中的 robot 了。现在 gazebo 中的 robot 是没有 texture 的。尽管在 urdf 中设置了 material，并且 rviz 中可见，但是这些并不能被  gazebo 识别，需要额外设置。

4. 创建 robot.gazebo 文件，把各个 link 对应的 material 重新设置，如下

```xml
     <!-- materials -->
     <gazebo reference="base_link">
       <material>Gazebo/Orange</material>
     </gazebo>
   
    <gazebo reference="wheel_1">
           <material>Gazebo/Black</material>
    </gazebo>
   
    <gazebo reference="wheel_2">
           <material>Gazebo/Black</material>
    </gazebo>
   
    <gazebo reference="wheel_3">
           <material>Gazebo/Black</material>
    </gazebo>
   
    <gazebo reference="wheel_4">
           <material>Gazebo/Black</material>
    </gazebo>
```

   然后将该 gazebo 文件加入到主 xacro 文件中，如下

```xml
   <xacro:include filename="$(find robot1_description)/urdf/robot.gazebo" />
```

   此时再 Launch gazebo 就可以看到带有 texture 的 robot 了。

1. 添加  sensor. 要在主 xacro 文件中添加 Link 信息，同时在 robot.gazebo 文件中添加 plugin libgazebo_ros_laser.so.

2. 驱动 robot in gazebo。这里添加的 skid steer drive，针对四轮的: 

   ```xml
     <!-- Drive controller -->
   <gazebo>
     <plugin name="skid_steer_drive_controller" filename="libgazebo_ros_skid_steer_drive.so">
       <updateRate>100.0</updateRate>
       <robotNamespace>/</robotNamespace>
       <leftFrontJoint>base_to_wheel1</leftFrontJoint>
       <rightFrontJoint>base_to_wheel3</rightFrontJoint>
       <leftRearJoint>base_to_wheel2</leftRearJoint>
       <rightRearJoint>base_to_wheel4</rightRearJoint>
       <wheelSeparation>4</wheelSeparation>
       <wheelDiameter>0.1</wheelDiameter>
       <robotBaseFrame>base_link</robotBaseFrame>
       <torque>1</torque>
       <topicName>cmd_vel</topicName>
       <broadcastTF>0</broadcastTF>
     </plugin>
   </gazebo>
   ```

3. 采用 teleop_twist_keyboard package 中的 teleop_twist_keyboard.py 文件，直接产生键盘到 /cmd_vel 的转换，实现键盘对 robot 的控制. 

**注意**：手动驱动 robot 时不需要记录 robot 相对于地图的位置，没有 map, Odometry 之类的东西

# ROS Navigation stack

## Stack == metapackage

metapackage 就是 several packages in a group

在 ROS Fuerte 版本中，这种多个 package 的组合称为 stack，现在 stack 的概念不再使用，由  metapackage 扮演这个角色。

在Navigation stack 就是一种 metapackage。

metapackage 中关键的只有一个 package.xml 文件，里面的内容与普通 package.xml 文件几乎相同。一个区别是只指明 `<run_depend>`，另一个就是在末尾多了一个 export tag，例如：

```xml
<?xml version="1.0"?>
<package>
  ...
  <buildtool_depend>catkin</buildtool_depend>

  <run_depend>roscpp_tutorials</run_depend>
  <run_depend>rospy_tutorials</run_depend>
  <run_depend>turtlesim</run_depend>

  <export>
    <metapackage/>
  </export>
</package>
```

如果要查看系统中的 metapackage，命令与 package 类似，如下：

```bash
rosstack list
rosstack find pkg_name
```

尽管 stack 已经改名成 metapackage 了，但是命令依然没有改变。

## 安装

Navigation stack 并不是随 ROS full 一起安装的，需要另外安装

```
sudo apt-get install ros-kinetic-navigation

```

Navigation stack 中包含了很多 package，如下

> **navigation**: 
>
> amcl | base_local_planner | carrot_planner | clear_costmap_recovery | costmap_2d | dwa_local_planner | fake_localization | global_planner | map_server | move_base | move_base_msgs | move_slow_and_clear | nav_core | navfn| robot_pose_ekf | rotate_recovery | voxel_grid

## Aim

To move a robot from the start position to the goal position, without making any collision with the environment.

## Standard algorithms

SLAM, A*, Dijkstra, AMCL (Adaptive Monte Carlo Localization), ...

## Input and output

- input: The robot or operator should provide the following data to Navigation stack
  - goal position 
  - odometry data from sensors such as wheel encoders, IMU, GPS (to figure out its own position and velocity)
  - other sensor data streams such as laser scanner data, 3D point cloud from sensors like Kinect (to figure out the environment, i.e., mapping)
  - tf information
  - optional: map
- output
  - **velocity commands** which will drive the robot to the given goal position

## Requirements

- The mobile robot should be controlled by sending velocity commands in the form: x, y, theta. (x,y 线速度, theta 角速度)
- The robot should mount a planar laser which is used to build the map of the environment.

## Basic building blocks

![navigation1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/navigation1.jpeg)

For configuring the Navigation packages for a custom robot, we must provide functional blocks to interact with the Navigation packages.

### External functional blocks

- **Odometry**: The odom value should publish to the Navigation stack with a message type of `nav_msgs/Odometry`, including position and velocity. odometry  is the distance of something relative to a point. 一般来说，就是 base_link 到 odom frame 中一个固定点的位置。如果在 gazebo 中采用了 diffdrive_plugin，the driver publishes the odometry generated in the simulated world. 实际上， diffdrive 的源文件里面就是 c++ 写的关于 publish odom 的程序。
- **Sensor data**: Laser scan data or point cloud data are needed for mapping the environment. This data, along with odometry, combines to build the global and local map of the robot. 
  - Laser Range finders, of type sensor_msgs/LaserScan 
  - Kinect 3D sensors, of type sensor_msgs/PointCloud
- **tf**: The robot should publish the relationship between the robot coordinate frame of sensors, links, etc., using ROS tf
- **base controller**: To convert the output from Navigation (which is of type geometry_msgs/Twist, i.e. linear and angular velocity) into corresponding motor velocities of the robot. 

The optional nodes of the Navigation stack are amcl and map server, which allow localization of the robot and help to save/load map. 

Even though there is no map obtained directly from the map server, the sensor source node can also provide map after mapping the environment.

### Internal functional blocks

- global-planner: receive the goal position from topic "move_base_simple/goal". Planning the optimum path from the current position of the robot to the goal position, with respect to the robot map (either from the map server or from the mapping based on sensor data). It has implementation of path finding algorithms such as A*, Dijkstra, ... for finding the shortest path
- local-planner: To navigate the robot in a section of the global path. It will take the odometry and sensor reading, and send a velocity command to the controller. It is an implementation of the trajectory rollout and dynamic window algorithms
- rotate-recovery: This package helps the robot to recover from a local obstacle by performing a 360 degree rotation.
- clear-costmap-recovery: This package is also for recovering from a local obstacle by clearing the costmap by reverting the current costmap used by the Navigation stack to the static map.
- costmap-2D: Robot can only plan a path with respect to a map. It creates 2D or 3D occupancy grid maps, which is a representation of the environment in a grid of cells. Each cell has a probability value which indicates whether the cell is occupied or not. The costmap-2D package can build the grid map of the environment by subscribing sensor values of the laser scan or point cloud and also the odometry values.

### Optional functional blocks

- map-server: allows us to save and load the map generated by the costmap-2D package
- AMCL : A method to localize the robot in map. This approach uses particle filter to track the pose of the robot with respect to the map. In the ROS system, AMCL can only work with maps which were built using laser scans. 也就是 2D map.
- gmapping: The gmapping package is an implementation of an algorithm called Fast SLAM which takes the laser scan data and odometry to build a 2D occupancy grid map.

## How the entire system works

1. Localization

   The robot localizes itself on the map. The AMCL package will help here.

1. Sending a goal and path planning

   After getting the current position of the robot, we can send a goal position. The goal planner will plan a path from current position to goal position.

   This plan is with respect to the global costmap which is feeding from the map server. The global planner will send this path to the local planner, which executes each segment of the global plan.

   The local planner gets the odometry and sensor value, and finds a collision free local plan for the robot. The local planner is associated with the local costmap, which can monitor the obstacles around the robot. 

2. Collision recovery behavior

   If the robot is stuck somewhere, the Navigation packages will trigger recovery behavior such as the clear costmap recovery or rotate recovery.

3. Sending the command velocity

   The local planner generates command velocity in the form of a twist message which contains linear and angular velocity (`geometry_msgs/Twist`), to the robot base controller. The robot base controller converts the twist message to the equivalent motor speed.

## Project 1 : mapping and  autonomous navigation

### 用 urdf 构造一个 differential wheeled robot.

一般来说，differential wheeled robot 有两个装在 robot chassis 两边的轮子以及一个或两个 caster wheel 组成。

两个轮子可以分别控制，通过速度的不同组合实现 robot 的移动、转向。

示意图：2 wheels and 2 casters

![differential_wheeled1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/differential_wheeled1.jpeg)

《mastering ros for robotics programming》这本书中提到了 differential wheeled robot 设计，里面也需要用到 transmission 的。如果有需要，可以以此为模板修改。

构造完之后，还没有涉及 gazebo 的内容，只是 robot 自身的构造，此时可以通过 rviz 可视化该 robot.

以上这些文件都放在了 robot_description 文件夹中。

注意： 在这里没有把 gazebo plugins 单拿出来放在一个 robot.gazebo 文件中，所有的东西都在 robot.xacro 中。

### Gazebo 中的仿真

launch 文件如下：

```xml
<launch>

  <arg name="paused" default="false"/>
  <arg name="use_sim_time" default="true"/>
  <arg name="gui" default="true"/>
  <arg name="headless" default="false"/>
  <arg name="debug" default="false"/>

  <!-- We resume the logic in empty_world.launch -->
  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="debug" value="$(arg debug)" />
    <arg name="gui" value="$(arg gui)" />
    <arg name="paused" value="$(arg paused)"/>
    <arg name="use_sim_time" value="$(arg use_sim_time)"/>
    <arg name="headless" value="$(arg headless)"/>
  </include>
###### 上边 world 部分完全没有修改，就是 empty_world
    
  <!-- urdf xml robot description loaded on the Parameter Server-->
  <param name="robot_description" command="$(find xacro)/xacro.py '$(find mastering_ros_robot_description_pkg)/urdf/diff_wheeled_robot.xacro'" /> 

  <!-- Run a python script to the send a service call to gazebo_ros to spawn a URDF robot -->
  <node name="urdf_spawner" pkg="gazebo_ros" type="spawn_model" respawn="false" output="screen"
	args="-urdf -model diff_wheeled_robot -param robot_description"/> 

</launch>
```

用  gazebo_ros package 中的 spawn_model 程序在 world 中孵化出 robot.

### 添加 laser scanner

```xml
# link 部分
<link name="hokuyo_link">
    <visual>
        <origin xyz="0 0 0" rpy="0 0 0" />
        <geometry>
            <box size="${hokuyo_size} ${hokuyo_size} ${hokuyo_size}"/>
        </geometry>
        <material name="Blue" />
    </visual>
</link>

# joint 部分
<joint name="hokuyo_joint" type="fixed">
    <origin xyz="${base_radius - hokuyo_size/2} 0 ${base_height+hokuyo_size/4}" rpy="0 0 0" />
    <parent link="base_link"/>
    <child link="hokuyo_link" />
</joint>

# gazebo plugin 部分
<gazebo reference="hokuyo_link">
    <material>Gazebo/Blue</material>
    <turnGravityOff>false</turnGravityOff>
    <sensor type="ray" name="head_hokuyo_sensor">
        <pose>${hokuyo_size/2} 0 0 0 0 0</pose>
        <visualize>false</visualize>
        <update_rate>40</update_rate>
        <ray>
            <scan>
                <horizontal>
                    <samples>720</samples>
                    <resolution>1</resolution>
                    <min_angle>-1.570796</min_angle>
                    <max_angle>1.570796</max_angle>
                </horizontal>
            </scan>
            <range>
                <min>0.10</min>
                <max>10.0</max>
                <resolution>0.001</resolution>
            </range>
        </ray>
        <plugin name="gazebo_ros_head_hokuyo_controller" filename="libgazebo_ros_laser.so">
            <topicName>/scan</topicName>
            <frameName>hokuyo_link</frameName>
        </plugin>
    </sensor>
</gazebo>
```

### 添加驱动 plugin

采用如下的 Gazebo plugin 去驱动一个差分驱动的小车

```xml
<gazebo>
    <plugin name="differential_drive_controller" filename="libgazebo_ros_diff_drive.so">
        <rosDebugLevel>Debug</rosDebugLevel>
        <publishWheelTF>false</publishWheelTF>
        <robotNamespace>/</robotNamespace>
        <publishTf>1</publishTf>
        <publishWheelJointState>false</publishWheelJointState>
        <alwaysOn>true</alwaysOn>
        <updateRate>100.0</updateRate>
        <leftJoint>front_left_wheel_joint</leftJoint>
        <rightJoint>front_right_wheel_joint</rightJoint>
        <wheelSeparation>${2*base_radius}</wheelSeparation>
        <wheelDiameter>${2*wheel_radius}</wheelDiameter>
        <broadcastTF>1</broadcastTF>
        <wheelTorque>30</wheelTorque>
        <wheelAcceleration>1.8</wheelAcceleration>
        <commandTopic>cmd_vel</commandTopic> # 接收 sensor_msgs/Twist 
        <odometryFrame>odom</odometryFrame> 
        <odometryTopic>odom</odometryTopic> 
        <robotBaseFrame>base_footprint</robotBaseFrame>
    </plugin>
</gazebo> 
```

至此，基本的 gazebo launch 文件就结束了，如果希望 gazebo 仿真和 rviz 同时打开，则还需要把之前 rviz launch 中用到的 joint_state_publisher 和 robot_state_publisher 加到现在的 gazebo launch 中，最终结果如下：

```xml
<launch>
    <arg name="paused" default="false"/>
    <arg name="use_sim_time" default="true"/>
    <arg name="gui" default="true"/>
    <arg name="headless" default="false"/>
    <arg name="debug" default="false"/>

    <include file="$(find gazebo_ros)/launch/empty_world.launch">
        <arg name="debug" value="$(arg debug)" />
        <arg name="gui" value="$(arg gui)" />
        <arg name="paused" value="$(arg paused)"/>
        <arg name="use_sim_time" value="$(arg use_sim_time)"/>
        <arg name="headless" value="$(arg headless)"/>
    </include>

    <!-- urdf xml robot description loaded on the Parameter Server-->
    <param name="robot_description" command="$(find xacro)/xacro.py '$(find mastering_ros_robot_description_pkg)/urdf/diff_wheeled_robot.xacro'" /> 


    <node name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher" ></node> 
    <node pkg="robot_state_publisher" type="state_publisher" name="robot_state_publisher" output="screen" >
        <param name="publish_frequency" type="double" value="50.0" />
    </node>


    <!-- Run a python script to the send a service call to gazebo_ros to spawn a URDF robot -->
    <node name="urdf_spawner" pkg="gazebo_ros" type="spawn_model" respawn="false" output="screen"
          args="-urdf -model diff_wheeled_robot -param robot_description"/> 

</launch>
```

### 添加手动驱动的 teleop node

截至到目前，我们可以通过手动 pub 信息到  /cmd_vel 从而驱动 robot 了。

下面我们启动一个 node，可以将键盘的敲击命令转化成 /cmd_vel 上的信息，从而更见简单的驱动 robot.

有两种方式实现键盘到 /cmd_vel 的转化

1. 采用 turtlesim package 中的 turtle_teleop_key 程序。

   ```xml
   rosrun turtlesim turtle_teleop_key 
   ```

   此时键盘的命令转化到了 topic /turtle1/cmd_vel，所以还需要 remap 一下到 /cmd_vel。 我们可以在 launch 文件中调用这个程序，适当修改名字即可

2. 采用 teleop_twist_keyboard package 中的 teleop_twist_keyboard.py 文件，直接产上键盘到 /cmd_vel 的转换，这个可能需要安装 teleop_twist_keyboard package. 

### 创建地图 

Two nodes are involved in mapping: slam_gmapping and move_base

- slam_gmapping:  安装 `sudo apt install ros-kinetic-slam-gmapping`

  用于创建 2D 地图。

  安装完之后有package `gmapping`. The ROS gmapping package is a wrapper of open source implementation of SLAM called OpenSLAM

  The package contains a node called `slam_gmapping`, which is the implementation of SLAM which helps to create a 2D occupancy grid map.

  slam_gampping subscribes

  - The laser data of type `sensor_msgs/LaserScan`
  - tf

  publish

  - The occupancy grid map data of type `nav_msgs/OccupancyGrid`

- move_base: The main parameters needed to configure are the global and local costmap parameters, the local planner, and the `move_base` parameters. The parameters list is very lengthy. We are representing these parameters in several YAML files. Each parameter is included in the `param` folder inside the `diff_wheeled_robot_gazebo` package.

The basic hardware requirement for doing SLAM is a laser scanner which is horizontally mounted on the top of the robot, and the robot odometry data. 

上述两个 Nodes 可以通过 differential ... 中的 gmapping.launch 文件启动，其中的参数已经设置好。

创建地图的步骤：

1. 启动 gazebo，其中包含了 world 和 mobile robot，如下

   ![mapping1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/mapping1.png)

2. 启动 gmapping package 中的 slam_mapping node 和 move_base package 中的 move_base node.

3. 启动 teleop，手动驱动 robot 绘制地图，可以通过 rivz 接收 topic /map 监视地图的绘制过程

4. 保存地图 

   ```bash
   rosrun map_server map_saver -f willo # 保存到文件 willo 中
   ```

   生成两个文件：

   - willow.yaml：包含 map metadata，即基本信息描述
   - willow.pgm：image which has the encoded data of the occupancy grid map

### 利用 ACML package 和 static map 自动导航

AMCL package 可以帮助 robot 在 static map 上定位。

subscribe:

- laser scan data
- laser scan based maps
- tf 

publish:

- estimate position of the robot

结合 AMCL 和 move_base 就可以实现自动导航。

 launch 文件，启动 AMCL 和 move_base，并提供通过 map_server 提供 static map：

```xml
<launch>

    <!-- Map server -->
    <arg name="map_file" default="$(find diff_wheeled_robot_gazebo)/maps/test1.yaml"/>
    <node name="map_server" pkg="map_server" type="map_server" args="$(arg map_file)" />

    # 实际上就是 include 另外一个 launch 文件
    <include file="$(find diff_wheeled_robot_gazebo)/launch/includes/amcl.launch.xml">
        <arg name="initial_pose_x" value="0"/>
        <arg name="initial_pose_y" value="0"/>
        <arg name="initial_pose_a" value="0"/>
    </include>

    # 也是启动 move_base 的 launch 文件，其中包含了所有的参数配置
    <include file="$(find diff_wheeled_robot_gazebo)/launch/includes/move_base.launch.xml"/>

</launch>
```

启动刚才的完整 gazebo launch 文件，然后启动 amcl launch 文件.

**在 RVIZ 中的设置**

- fixed frame 用 map
- Laser scan: 可以看到激光边界
- RobotModel: 可以看到 robot 自身
- global_plan
- local_plan
- goal
- path
- global_costmap
- local_costmap
- pose array --> /partical_cloud 可以看例子滤波的定位

最终的 ros graph

![navigation1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/navigation2.png)



