# 自动驾驶的目的

自动驾驶的目标按重要程度划分有三个：

1. 安全
2. 高效
3. 节能







# Possible Class 1: Automobile robot

Just like the content in AI for robotics.

Teach the necessary knowledge about automobile robot, such as localization, perception, path planning, and control.



# Possible Class 2: Deep learning for self-driving cars

Focus on deep learning methods, self-driving car is just an application.

The content includes basic knowledge about deep learning ，最终 project 可以设定在 

- end-to-end learning in Udacity simulator
- Deep reinforcement learning (A3C) on the race car
- the 3 projects from MIT self-driving car course



![mit_self_driving1](pic/mit_self_driving1.png)





# 自动驾驶的历史

汽车早期发展史：

1980年代，汽车子系统中开始引入电控单元 (electronic control unit， ECU)，用来控制一个或多个子系统，包括 engine control units, anti-lock braking systems.

紧接着，为了满足不同 ECU 之间的通信，产生了 controller area network (CAN) bus.

1990年代，车载传感器出现，可以检测车辆周围的信息，基于这些车载传感器，可以实现诸如 lane keeping, car following 等功能。

发展到当代，人们尝试实现完全自动驾驶汽车，最重要的事件就是 DARPA 举办的一系列自动驾驶汽车竞赛。

- In 2004, DARPA (Defense Advanced Research Projects Agency, an agency of the United States Department of Defense responsible for the development of emerging technologies for use by the military) conducted a challenge called the DARPA Grand Challenge to encourage autonomous vehicle technology.

  The aim of the challenge was to autonomously drive for 150 miles (240 km) through a desert roadway.

  In 2004, more than 100 teams registered, but no team was able to complete the goal. Carnegie Mellon University's Red Team and their car Sandstorm traveled the farthest distance, completing 11.78 km (7.32 mi) of the course.

- In 2005, five vehicles successfully completed the course. An interesting thing in 2005 competition is that the head of Stanford Racing Team, Sebastian Thrun, was previously a faculty member at Carnegie Mellon and colleague of Red Whittaker, head of the CMU team.

  [A document video of 2005 DARPA Grand Challenge](video/9.The Great Robot Race.mp4)

|Vehicle|Team Name|Team Home|Time Taken (h:m)|Result|
|--|--|--|--|--|
|Stanley|Stanford Racing Team|Stanford University|6:54|First place|
|Sandstorm|Red Team|Carnegie Mellon University|7:05|Second place|
|H1ghlander|Red Team|Carnegie Mellon University|7:14|Third place|
|Kat-5|Team Gray|The Gray Insurance Company|7:30|Fourth place|
|TerraMax|Team TerraMax|Oshkosh Truck Corporation|12:51|Fifth place|

   **Stanley**
![stanley](pic/stanley.jpg)
​    
- In 2007, DARPA conduct the third and last autonomous vehicle chagllege, known as "Urban Challenge". The course involved a 60 mile (96 km) urban area course, to be completed in less than 6 hours. Rules included obeying all traffic regulations while negotiating with other traffic and obstacles and merging into traffic.

  The six teams that successfully finished the entire course:
  
|Team Name|Vehicle|Type|Team Home|Time Taken (h:m:s)|Result|
|--|--|--|--|--|--|
|Tartan Racing|Boss|2007 Chevy Tahoe|Carnegie Mellon University|4:10:20|1st Place;averaged approximately 14 mph (22.53 km/h)|
|Stanford Racing|Junior|2006 Volkswagen Passat Wagon|Stanford University|4:29:28|2nd Place; averaged about 13.7 mph (22.05 km/h)|
|VictorTango|Odin|2005 Ford Hybrid Escape|Virginia Tech|4:36:38|3rd Place; averaged slightly less than 13 mph (20.92 km/h)|
|MIT|Talos|Land Rover LR3|MIT|Approx. 6 hours|4th Place|
|The Ben Franklin Racing Team|Little Ben|2006 Toyota Prius|University of Pennsylvania|No official time|One of 6 teams to finish course|
|Cornell|Skynet|2007 Chevy Tahoe|Cornell University|No official time|One of 6 teams to finish course|


![boss1](pic/boss1.jpg)



- In 2009, Google started to develop their self-driving car project, now known as Waymo, which was lead by Sebastian Thrun, the former director of the Standford Artificial Intelligence Laboratory.

![waymo](pic/waymo.jpeg)

- In 2015, Tesla introduced a semi-autonomous autopilot feature in their electric cars. It enables hands-free driving mainly on highways. 

- In 2016, Nvidia introduced their own self-driving car, built using their AI car computer called NVIDIA-DGX-1. This computer was specially designed for the self-driving car and is the best for training autonomous driving neural network models.

- A lot of self-drving car related startups appear in recent year. 



# 自动驾驶的等级

由国际汽车工程师协会 (**SAE International** == **Society of Automotive Engineers**) 制定了 level 0 ~ level 5 六个等级。

**SAE J3016**

- Level 0: Completely manual. Most old cars belong in this category.
- Level 1: A driver assistance system helps the human driver on single function, such as steering, acceleration, brake, using information from the environment.
- Level 2: Multiple functions can be conducted by the driver assistance system.
- Level 3: All tasks are autonomous, but it is expected that a human will intervene whenever required.
- Level 4: There is no need for a driver. Works autonomously in specified conditions
- Level 5: Full automation, work on any road and any weather condition.



Some researchers argue that, Level 3 is a quite dangerous situation.

> Victoria A. Banks, Alexander Eriksson, Jim O'Donoghue, and Neville A. Stanton. Is partially automated driving a bad idea? Observations from an on-road study. Applied Ergonomics, 68, 138-145, 2018.





# 硬件 Components of a self-driving car

下图是典型的自动驾驶汽车的传感器和控制PC

![component_car](pic/component_car.jpg)

sensors 有单独notebook介绍，这里只是简单综述一下整个框架。

- GPS + IMU + wheel encoder: 这是典型的定位组合。其中 wheel encoder 轮速计也叫 DMI，distance measurement indicator. 2007 挑战赛中很多汽车都用下边这个产品
<img src="pic/RT3000.png" alt="RT3000" width=300 height=300 align="center">

- Camera: 用于行人检测，信号等检测等， 2007挑战赛冠军Boss上边用的 Point Grey Firefly
<img src="pic/pgf.jpg" alt="pgf" width=300 height=300 align="center">
- Ultrasonic sensors: 主要用在 parking of vehicle, avoid obstacles in blind spots.
- Lidar and Radar: Lidar 可以提供精准的 3D 环境， 主要应用是构建地图，避碰，障碍物检测。目前应用比较多的是Velodyne 系列
<img src="pic/velodyne_series.png" alt="velodyne_series" width=300 height=300 align="center">

除了 Velodyne 系列，还有其他类型的 Lidar，例如 SICK Laser Measurement System (LMS) 可提供 180 度扫描， Hokuyo 也是比较常见的。
 <img src="pic/laser.png" alt="laser" width=300 height=300 align="center">

Radar 主要采用 Doppler principle 检测物体，用于避碰。一般来说，Radar 装载汽车的最前边。

## compare of the sensors

用于感知外部环境的 sensor 主要有四种：

- Lidar
- Radar
- Ultrasonic
- Passive visual

每种 sensor 都有它的优缺点

![compare_sensor7](pic/compare_sensor7.png)



![compare_sensor2](pic/compare_sensor2.png)



![compare_sensor3](pic/compare_sensor3.png)



![compare_sensor4](pic/compare_sensor4.png)



![compare_sensor5](pic/compare_sensor5.png)



## Tesla less expensive compromise

Tesla argues that Lidar is not necessary. However, Lidar is the primary sensor in Google's self-driving car.

![compare_sensor6](pic/compare_sensor6.png)







- 车载 PC： 一般采用高端处理器和GPU，实时处理传感器送来的大量数据，发出控制信号，操控 steering angle, throttle, and braking。

# Software block diagram

![software_block](pic/software_block.png)

- sensor interface modules: 负责 sensors 与 汽车之间的通信，是汽车PC得到需要的数据
- perception modules: 基于 sensor 数据感知外部环境和汽车自身定位
- navigation modules: 基于感知信息，规划运行路径，给出高层控制信号
- vehicle interface: 高层控制信号通过 drive-by-wire (DBW) 接口送到 CAN bus，驱动底层的控制。只有特殊设计的汽车才会开放 DBW 接口，比较常见的包括 Lincoln MKZ, VW Passat Wagon 等
- user interface: provide controls to the user. It can be a touch screen to set the destination and an emergency button
- global services: 用来保存行车记录，提供必要的基础服务 



# 如何验证 self-driving car 的安全性

## 五级测试场景

- T1： 最基础的笔直道路，只有红路灯等简单交通设置（车辆只需要纵向控制）
- T2：简单城市场景，自动驾驶汽车能够转向
- T3： 常见城市场景，包括立交桥
- T4： 复杂城市场景，包括隧道、林荫道等
- T5： 特殊城市场景，包括雨雾、湿滑路面等复杂天气

## 国家智能汽车与智慧交通 (京冀) 示范区海淀基地

这是北京首个自动驾驶封闭测试场。目前可以实现 T1 至 T3 场景测试。自动驾驶汽车需要在这里闯三关，并累计 5000 公里以上，才能获得北京市自动驾驶路测牌照。测试时驾驶室内要乘坐 1 到 2 名驾驶员，只有特殊情况时才干预

路测机制并不是随机设定的，而是根据工信部、公安部、交通运输部共同发布的《智能网联汽车道路测试管理规范（试行）》设置的。

**测试要测试以下几个能力**：

- 基本交通设施检测与响应能力：主要是信号灯、交通标志识别及遵守
- 前方车道内动态、静态目标识别与响应能力：感知车辆、行人、障碍物等，实现安全跟随、避让等操作
- 遵守规则行车能力：包括超车、并道、通过交叉路口等
- 安全接管与应急制动能力：靠边停车与起步、应急车道内停车、人工接管等
- 综合能力：文明驾驶、复杂环境通行、车车协同、车路协同等



其他信息可以参考文章：

Erik Coelingh, Jonas Nilsson, and Jude Buffum. Driving tests for self-driving cars, IEEE Spectrum, 55(3): 40-45, 2018. [文章链接](https://ieeexplore.ieee.org/abstract/document/8302386/)



## 





# 搭建仿真环境

## 为什么需要仿真？

根据兰德公司报告，Given that current traffic fatalities and injuries are rare events compared with vehicle miles traveled, we show that fully autonomous vehicles would have to be driven hundreds of millions of miles and sometimes hundreds of billions of miles to demonstrate their safety in terms of fatalities and injuries. 

即一个自动驾驶汽车需要运行数亿至数千亿英里的路程，才能比较可靠的证明它的安全性。

尽管各大公司不断积累行驶里程，但是距离这一数量级还差的很远。

仿真平台可以模拟现实中的各种复杂场景，比如雨天、光照变化等，快速的，高效率的验证汽车安全性。这更节约资源和时间。

## 仿真的基本思路

1. 有些情况下是要得到数据集去训练神经网络：
  - **真车情况下**： 在神经网络学习中，原本是基于实际车辆和实际驾驶员的操作数据，即面对什么样的场景，驾驶员的正确操作是什么。神经网络需要这些数据集去学习、训练，直到给它一个场景，能够作出类似于人的正确操作。

  - **仿真情况下**： 操作者通过操作模拟的车辆在模拟环境中运行，得到场景 VS 正确操作的数据集，把它送给神经网络去学习。

1. 有些情况下是要验证某些算法的有效性，以定位算法为例： 
  - **真车情况下**：从车载传感器获取检测数据，通过算法计算估计位置，并得到相应的控制量，送回到真车中。
  
  - **仿真环境下**：仿真车也会配备各种传感器在仿真环境中检测周围信息，这些信息传给算法，计算估计位置，得到控制量，然后返回给仿真车，实现系统的闭环控制。

一般来说，模拟车与真车的外部接口是相同的，都是送出传感器检测信号，接收控制系统的控制信号，所以一套自动驾驶软件系统如果在仿真环境中表现很好，几乎可以不用修改（因为接口完全相同），直接连接到真车上去进一步测试。

## 基于 docker 的仿真环境

#### install
参考网址： https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository

由于 docker 需要 root 权限运行，所以为了避免每次 sudo，可以新建一个高级权限的group，将当前用户加入到该 group 中。

`sudo groupadd docker`

`sudo usermod -aG docker $USER`

log out, log back in

测试一下：`docker run hello-world`

#### 将 app 封装成 docker image
参考网址： https://docs.docker.com/get-started/part2/#build-the-app


#### 常用命令
- `docker info`: 显示整个 docker 系统的详细信息
- `docker build -t image_name`: 创建 Image file
- `docker image ls`: 列出所有的 image
- `docker container ls`: 列出所有的 container

一个 image 一旦运行起来就是一个 container，可以说一个 container 就是 image 的一个具体运行实现。一个 image 可以在多个 container 中运行。

- `docker run image_name`: 运行一个 Image
- `docker -p 4000:80 image_name`: 将 container 中的端口 80, 对应到实际系统中的 4000
- `docker -d image_name`: 在后台运行
- `docker container stop container_ID`: 结束一个 container

- `docker login`: 在 terminal 中登录 docker hub 系统
- `docker tag image_name repo:tag`: 给 Image 打上标签，以备推送到相应的 repo 中
- `docker push repo:tag`: 将 Image 推送到相应的 repo 中

在其他机子上，只要安装了 docker，并且登录到 repo 中（或者下载到本地了），就可以运行 image 了

`docker run repo:tag`

### 安装 nvidia-docker
<img src="pic/nvidia_docker.png" alt="nvidia_docker" width=500 height=500 align="left">

1. 如果还没有 CUDA 驱动，则需要安装： 参考 Machine-learning notebook 中的安装方法
2. 安装 nvidia-docker, 注意，在 nvidia-docker 的官网上让安装 docker2，但是 docker2 与 ros 不匹配。必须安装 nvidia-docker.      - 如果已经安装了 nvidia-docker2，就要先卸载：`sudo apt remove --purge nvidia-docker2`. 
   - 然后再安装 nvidia-docker: `sudo apt install nvidia-docker`. 
   - 如果后边出问题了，再尝试安装`nvidia-modprobe package by apt command`.

### 安装 osrf/car_demo

提供了完整的 gazebo simulation for self-driving car.

#### 条件1： ros kinetic + gazebo 8

由于 ros kinetic full 版本默认安装 gazebo 7, 所以在安装 ros 时只安装 base 版本，然后在安装 gazebo 8 和其他必须的组件。

1. 如果已经安装了 ros kinetic full， 先卸载： `sudo apt remove ros-kinetic-*`
2. 按照 ros 官网指导，安装 ros kinetic base 版本： http://wiki.ros.org/kinetic/Installation/Ubuntu
3. 按照 gazebo 官网指导，安装 gazebo 8 版本，这是从 osrf repo 安装的：
   - 添加 osrf repo: `sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'`
   - setup key: `wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -`
   - update: `wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -`
   - 安装 gazebo 8： `sudo apt install ros-$ROS_DISTRO-gazebo8-ros-pkgs`,该package 包括建立在 gazebo 8 及之上的pkg
   - 确认一下 ros 和 gazebo 版本正确。
4. 现在 ros 系统里面只有 base 和 gazebo8，还需要手动安装其他必要的组件
   - catkin: `sudo apt-get install ros-kinetic-catkin`
   - rviz: `sudo apt-get install rviz`
   - other pkg: `sudo apt-get install ros-kinetic-controller-manager ros-kinetic-joint-state-controller ros-kinetic-joint-trajectory-controller ros-kinetic-rqt ros-kinetic-rqt-controller-manager ros-kinetic-rqt-joint-trajectory-controller ros-kinetic-ros-control ros-kinetic-rqt-gui`
   - `sudo apt-get install ros-kinetic-rqt-plot ros-kinetic-rqt-graph ros-kinetic-rqt-rviz ros-kinetic-rqt-tf-tree`
   - `sudo apt-get install ros-kinetic-gazebo8-ros ros-kinetic-kdl-conversions ros-kinetic-kdl-parser ros-kinetic-forward-command-controller ros-kinetic-tf-conversions ros-kinetic-xacro ros-kinetic-joint-state-publisher ros-kinetic-robot-state-publisher`

#### 条件2：  X server


#### 条件3： docker + nvidia-docker

#### 运行仿真程序

   - 如果有问题，就先启动服务： `sudo service nvidia-docker start`
   - 运行程序：` ./run_demo.bash`



# 常用数据集

- Cityscapes: 主要用于像素级和实例级的语义图像分割
- ApolloScape: 可用于各种感知任务，例如场景解析、车道分割、自定位、轨迹估计、目标检测与跟踪
- KITTI ：提供了用于立体和流量估计、目标检测和跟踪、道路分割、里程估计、语义图像分割的可视化数据集
- 6D-vision: 使用立体摄像机感知三维环境，提供立体、光流和语义图像分割的数据集



# kinematic bicycle  model

在进行机器人建模时，经常会提到 holonomic 和 nonholonomic 这个概念。

如果一个物体的位置可以表示如下：
$$
f(q_1, q_2, \cdots, q_n, t) = 0
$$
其中变量为物体的 n 个坐标分量，则称物体满足 holonomic constraints. 

例如，在球体表面运动的质点，它的位置可以表示为
$$
r^2 - a^2 = 0
$$
下边的这个单摆系统也是 holonomic 的，它的位置满足
$$
x^2 + y^2 - L^2 = 0
$$
![holonomic1](pic/holonomic1.svg)

holonomic 系统可以减少独立变量数目，例如
$$
f(q_1, q_2, \cdots, q_n, t) = 0
$$
其中某个位置分量可以表示为
$$
q_i = g_i(q_1, q_2, \cdots, q_{i-1}, q_{i+1}, \cdots, t)
$$
所有不满足 holonomic 条件的系统都是 noholonomic 系统。

直观上来说，对于轮式机器人，

- 如果可以在平面上任意方向移动，则是满足 holomonic 约束
- 如果只能在限制方向范围内移动，则是 nonholomonic 约束。一般的轮式机器人都是 nonholomonic 的，除非是那种麻花轮子的。

### kinematic model 与 dynamic model 的区别
- 运行的改变本质上是由力的作用造成的，dynamic model 在建模过程中考虑了这些力，依赖牛顿定律推导力与运动之间的关系。
- kinematic model 则不显式地考虑力，主要关注速度、位置等，通过系统各部分之间的几何约束描述运行过程。

简单的说，如果建模过程中用到了牛顿第二定律或者旋转力矩，就是 dynamic model，否则就是 kinematic model. 

### 双轮机器人的 kinematic model

![2_wheel1](pic/2_wheel1.png)

#### 输入为线速度与角速度

$$
\dot{x} = v\cos\theta \\
\dot{y} = v\sin\theta\\
\dot{\theta} = \omega
$$

其中，输入为

- 线速度  $v$
- 角速度  $\omega$

#### 输入为左右轮子的转速

如果机器人只能接受更加底层的左右轮转速的话，需要将上述 model 进一步推导，将线速度和角速度用左右轮的转速 $w_1, w_2$ 表示出来。

![2_wheel2](pic/2_wheel2.png)

**在线速度方面**，每个轮子的线速度是
$$
v_i = rw_i
$$
机器人的最终线速度是两个轮子速度的均值

![2_wheel3](pic/2_wheel3.png)
$$
v = \frac{v_1+v_2}{2} = \frac{rw_1+rw_2}{2}
$$
其中 ICR ： instantaneous center of rotation

**在角速度方面**

![2_wheel4](pic/2_wheel4.png)

注意：转角方向的正负号设定。

最终的 model 为
$$
\dot{x} = \frac{rw_1+rw_2}{2}\cos\theta \\
\dot{y} = \frac{rw_1+rw_2}{2}\sin\theta \\
\dot{\theta} = \frac{rw_1-rw_2}{2l}
$$
对应的离散形式为
$$
x_{k+1} = x_{k} + \Delta t\frac{rw_{1,k}+rw_{2,k}}{2}\cos\theta_k  \\
y_{k+1} = y_{k} + \Delta t\frac{rw_{1,k}+rw_{2,k}}{2}\sin\theta_k  \\
\theta_{k+1} = \theta_k + \Delta t\frac{rw_{1,k}-rw_{2,k}}{2l}
$$

### kinematic bicycle model 的一些设定
1. 两个前轮合并， 两个后轮合并
2. 前后轮都可以转动，对于 front-wheel-only steering， 可以设置后轮转角始终为0
3. 车子只在水平面上运动
4. 前后轮的运动方向就是轮子的指向，没有侧滑

对于车辆中心位置的选择，也会推导出不同的模型。常见的三种车辆中心位置选择

![bicycle_model1](pic/bicycle_model1.png)

- 前轮轴中心
- 后轮轴中心
- 车辆的重心

### model 1：(x,y) 位置在车辆重心

#### 示意图
<img src="pic/kinematic_model.png" width=500 align=center>

#### 推导
![kinematic1](pic/kinematic1.png)
![kinematic2](pic/kinematic2.png)
![kinematic3](pic/kinematic3.png)
![kinematic4](pic/kinematic4.png)

上述 kinematic model 设定前后轮都可以转动，输入变量为前后轮转角和速度。

当设定后轮不转动，并且以加速度代替速度为输入时，即输入变量为前轮转角和加速度，方程如下
![kinematic5](pic/kinematic5.png)

#### 以 线速度和前轮变化率 为输入变量

上述公式中，控制量是 加速度和前轮转角，还可以改变一下，直接以速度作为一个输入，转角变化率作为另外的输入，这样的话，状态变量就去掉了速度，加入了前轮转角，如下

![bicycle_model4](pic/bicycle_model4.png)



### model 2: (x,y) 位置在后轮中心

#### 示意图

在没有轮胎侧滑的情况下，前后轮的速度应该是相同。

![bicycle_model2](pic/bicycle_model2.png)

另一种图片表示，同样的内容

![kinematic6](pic/kinematic6.png)

Shin 在其 Doctoral Thesis 中提到如果 x-y 坐标系中 x/y 轴与车后轮轴共线，则参考点的的速度与 steering angle 是解藕的，参考点速度始终指向车体方向。《High Performance Tracking of Explicit Paths by Roadworthy Mobile Robots》1990.

#### kinematic model 

首先有

![kinematic7](pic/kinematic7.png)

最终模型：
$$
\dot{x} = v \cos\phi \\
\dot{y} = v \sin\phi \\
\dot{\phi} = \frac{v}{L} \tan\delta \\
\dot{v} = a
$$
同样是将前轮转角和加速度作为输入

### model 1 与 model 2 之间的关系

model

- 两个模型中的速度 v 是不同的，model 1中，v 是车辆重心的速度，其方向并不一定为车身方向； model 2 中， v 是车身后部的速度，其方向始终与车身方向相同。两个大小关系 $v_1\cos\beta = v_2$
- 由于上述速度的差别，model 1 和 model 2 中的位置 (x,y) 与速度的关系也是不同的，model 1 中位置取决与 v， $\beta$，$\psi$，而 model 2 中位置仅取决于 v , $\psi$ (即 $\phi$)
- 尽管 $\dot{\psi}$ (即 $\dot{\phi}$) 在两个 model 中的表现形式不同，但由于是用不同的方式表现同一个量，即车身角度，其本质应该是相同的。的确，这两个表现形式之间是等价的，可以互相推导。
  - 令 model 1 中： $\dot{\psi} = \frac{v_1}{l_r}\sin(\beta)$
  - 令 model 2 中： $\dot{\phi} = \frac{v_2}{L}\tan(\delta)$
  - 由关系 $v_1\cos\beta = v_2$ 可得
  
    $\dot{\psi} = \frac{v_1}{l_r}\sin(\beta) = \frac{v_2}{l_r\cos\beta}\sin(\beta) = \frac{v_2}{l_r}\tan(\beta)$
    
    再由关系 $\beta = \tan^{-1}(\frac{l_r}{l_f+l_r}\tan(\delta_f))$ 可得
    
    $\dot{\psi} = \frac{v_2}{l_r}\tan(\beta) = \frac{v_2}{l_f + l_r}\tan(\delta_f) = \dot{\phi}$

### model 3: (x,y) 位置也在后轮中心

前两个模型都是连续时间模型，当在编写程序时，一般要写成离散形式（除非用 ODE 来求解连续函数，就像我们程序中那样），可能存在误差。下面这个模型直接建成了离散形式，可能更加准确。

#### 示意图

![model3](pic/model3.png)

#### 推导

中心思想是已知当前位置 (x, y), 计算旋转圆心的位置 ($C_x$, $C_y$)，然后依靠圆心，计算新的位置 (x', y'). 推导过程可见上图。

上边的式子对转弯情况比较精确，但是无法表达沿直线前进，因为直线时，曲率 R 为无穷。因此需要单独考虑直线情况。也很简单，就是判断 $\beta$ 的大小，比如设定条件，如果 $\beta<0.001$， 则认为是直线前进，采用方程，d 就是直线前进的距离。

![model3_1](pic/model3_1.png)



### model 4: (x,y) 位置在前轮中心

![bicycle_model3](pic/bicycle_model3.png)

### 车辆中心在前、中、后的公式之间的关系

实际上前、后两种情况是中心情况的特例，即中心模型的参数取某些特殊值时，得到另外两个模型。

- 中

  ![kinematic5](pic/kinematic5.png)

- 后
  $$
  \dot{x} = v \cos\phi \\
  \dot{y} = v \sin\phi \\
  \dot{\phi} = \frac{v}{L} \tan\delta \\
  \dot{v} = a
  $$

以 中心 模型为基础，对于 前 模型，相当于 $l_r = L$, $\beta = \delta_f$. 方程退化成 前 模型。

对于 后 模型，可以先把 中 模型的角度变化等价改写成如下形式
$$
\dot{\psi} = \frac{v}{L}\tan\delta_f\cos\beta
$$
然后令 $\beta=0$ 即得到 后 模型。 

# dynamic model

为什么需要 dynamic model:

下边几个典型的场景中，dynamic model 是很必要的

- 当汽车高速运动出现侧滑时，需要考虑牵引力、摩擦力等因素，才能更好的预测、判断车子运动状态。
- 当考虑油门对车子运动状态的影响时，要建立一个从油门到轮子转速的关系式，这里面肯定要考虑力、加速度、转矩等。

在 dynamic model 中主要有两大类系统：

- 位移传动系统 translational system: 包括力和力矩，主要用牛顿第二定律。采用这类建模方式的例如减震器 shock absorber
- 旋转传动系统 rotational system: 包括转动贯量，扭转力矩等，描述轮胎转动的模型就是这种模型。

dynamic model 并不像 kinematic bicycle model 那样有一组相对统一的方程。

dynamic model 里面包含对车辆各个部分的建模，可以单独使用这些独立的模型，也可以合并成一个更庞大的模型。

full vehicle dynamic 3D model 非常复杂，但也是一个热门的研究领域。

在实际中，常常将 full vehicle model 用两个 2D model 表示

- longitudinal dynamic model
- lateral dynamic model

这样独立的考虑横向、纵向 dynamic model，要简单很多。

## longitudinal dynamic model

![longitudinal1](pic/longitudinal_model1.png)

考虑 x-z 平面上汽车的运动。

### 整体受力情况

![longitudinal3](pic/longitudinal_model3.png)

从整体来看，车辆在纵向所受合力如上所示。

上述公式可以进一步简化，

![longitudinal2](pic/longitudinal_model2.png)

### 阻力部分的建模

![longitudinal4](pic/longitudinal_model4.png)



### 牵引力部分的建模

#### 从引擎转速到车速的建模

![longitudinal5](pic/longitudinal_model5.png)

上图解释了从引擎转速 $\omega_e$ 到车轮转速和线速的关系式。

简单来说，转速之间就是 Gear 转化的关系。

#### 从引擎转矩到引擎转速的建模

下面就是进一步推到引擎转矩与引擎转速 $\omega_e$ 的关系，这一部分比较麻烦，如下图所示，不过稍后会简化

![longitudinal5](pic/longitudinal5.png)



上式描述了引擎转矩 $T_{Engine}$ 与 $\omega_e$ 之间的关系，但包含了总牵引力在里面，进一步推导，已知总牵引力的表达式

![longitudinal6](pic/longitudinal6.png)

上式是引擎转矩 $T_{Engine}$ 与 $\omega_e$ 之间的最终的关系式。

接下来，需要找到关于引擎转矩  $T_{Engine}$ 和油门、刹车的关系式，即完成了整个系统从油门、刹车到轮子转速之间的关系式，基于这个关系式，就可以对自动驾驶汽车设定合适的油门、刹车力度，从而得到期望的行驶速度。

#### 从执行器到引擎转矩的建模

##### 油门的传动

![longitudinal9](pic/longitudinal9.png)

在不同速度条件下，同样的油门深度会得到不同的转矩。下图展示了最大转矩与速度的关系

![longitudinal7](pic/longitudinal7.png)

下边以 gasoline engine 为例进行介绍，其最大转矩与转速的关系大体呈抛物线。另外，在同样转速的条件下，油门深度基本上与转矩成线性关系，因此有下图

![longitudinal8](pic/longitudinal8.png)

##### 刹车的传动

![longitudinal10](pic/longitudinal10.png)

刹车传动不需要经过中间引擎、变速箱等的转换，直接作用在刹车片上。

![braking1](pic/braking1.png)

所以从刹车踏板到刹车转矩这部分的建模很简单，可以用线性模型，如下

![braking2](pic/braking2.png)





## lateral dynamic model

![lateral1](pic/lateral_model1.png)





# Planning

Planning 包括两个层次（from Udacity robotics）：
- 高层的称为 routing, 目的是找到从初始点到目标点的最优路径，较粗糙。
- 底层的称为 trajectory generating, 产生最终的 waypoints 

但好像分成三层是更普遍的认识：

- 路网层面的 planning，即 routing
- behavioral decision, 考虑周围感知信息的行为决策，例如跟车、换道、避让等
- motion planning，也叫 trajectory planning，要给出控制环节具体可行的 reference，包括给出从 A 点到 B 点之间的的路径点，以及到达每个路径点时，无人车的速度、朝向、加速度、车轮转向等。

## Routing

 几乎所有的 routing 算法都是先把 map 抽象成 graph 或者 grid，然后用经典的图搜索算法查找两点之间的路径。这里就用到了 breadth first, depth first, cheapest first, A* , Dijkstra 算法等。

### 基本搜索算法

#### 基本思想

![path_planning1](pic/path_planning1.png)

#### 程序实现


```python
# ----------
# User Instructions:
# 
# Define a function, search() that returns a list
# in the form of [optimal path length, row, col]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

'''
grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]
'''
grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1

delta = [[-1, 0], # go up
         [ 0,-1], # go left
         [ 1, 0], # go down
         [ 0, 1]] # go right

delta_name = ['^', '<', 'v', '>']

def search(grid,init,goal,cost):
    
    closed = [[0 for col in range(len(grid[0]))] for row in range(len(grid))]
    closed[init[0]][init[0]] = 1
    expand = [[-1 for col in range(len(grid[0]))] for row in range(len(grid))]
    
    # 记录每次的移动， a --> b, 把动作记录在 b
    action = [[-1 for col in range(len(grid[0]))] for row in range(len(grid))]
    
    
    x = init[0]
    y = init[1]
    g = 0
    
    open = [[g, x, y]]

    found = False 
    resign =  False # flag that cannot reach the goal
    count = 0
     
    while found is False and resign is False:  # 还没搜索到，并且还有可以搜索的空间
        
        if len(open) == 0:
            resign = True
            print('fail')
        else:
            open.sort()
            open.reverse()
            next = open.pop()
            x = next[1]
            y = next[2]
            g = next[0]
            expand[x][y] = count
            count += 1
            if x == goal[0] and y == goal[1]:
                found = True
                print('Got it: ', next)
            else:
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >= 0 and y2 < len(grid[0]):
                        if closed[x2][y2] == 0 and grid[x2][y2] == 0:
                            g2 = g + cost
                            open.append([g2, x2, y2])
                            closed[x2][y2] = 1
                            action[x2][y2] = i
    
    # 显示搜索顺序，以便后边与 A* 对比
    print('\n The order of search: ')
    for i in range(len(expand)):        
        print(expand[i])
    
    
    # 令路径显示的更直观
    # 一般 policy 是 位置 到 动作的映射， (x,y) ---> action
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    x = goal[0]
    y = goal[1]
    policy[x][y] = '*'

    while x != init[0] or y != init[1]:
        x2 = x - delta[action[x][y]][0]
        y2 = y - delta[action[x][y]][1]
        policy[x2][y2] = delta_name[action[x][y]]
        x = x2
        y = y2
    print('\n The optimal path: ')
    for i in range(len(policy)):
        print(policy[i])
        
search(grid, init, goal, cost) 


```

    ('Got it: ', [11, 4, 5])
    
     The order of search: 
    [0, -1, 14, 18, -1, -1]
    [1, -1, 11, 15, 19, -1]
    [2, -1, 9, 12, 16, 20]
    [3, -1, 7, 10, 13, 17]
    [4, 5, 6, 8, -1, 21]
    
     The optimal path: 
    ['v', ' ', ' ', ' ', ' ', ' ']
    ['v', ' ', ' ', ' ', ' ', ' ']
    ['v', ' ', ' ', ' ', ' ', ' ']
    ['v', ' ', '>', '>', '>', 'v']
    ['>', '>', '^', ' ', ' ', '*']

### Dijkstra 算法

这是图论中常见的最短路径搜索算法，由 E.W.Dijkstra 于 1959 年提出。给定图中的源节点，可以搜索该源节点到其他所有节点的最短路径。

参考刘少山等人《第一本无人驾驶技术书》P81



### A*

#### 基本思想

注意在上述搜索过程中，从 expand 矩阵可以看到，在找到目标位置之前，所有的 cell 都被搜索到了。基本搜索是没有方向性的，盲目的搜索。

A* 算法则引入了 heuristic function h, 每个 cell 的 h value 就是在不考虑障碍物时，该 cell 到目标位置的 cost，尽管粗糙，但是这一 h value 提供了一些额外信息，尤其是搜索的大方向，协助搜索。

如果与 breadth first, depth first 类比的话，A* 可以称为 Best estimated total path cost first. 

![a_star](pic/a_star.png)

在搜索路径中的node时有两个参数，
1. 从当前位置到某个node的cost,用 g 表示,一般设置对角运动为$\sqrt{2}$倍的水平或垂直运动。
2. 从某个node 到目标的 cost，用 h 表示，即 heuristic function, 一般是取在不考虑obstacle的情况下，该 node 到目标 node距离。 

搜索时，当前 node 先确定周围 node 的 g 和 h 值， 然后选择 f = g + h 最小的 node， 作为下一步的 node，然后重复。 

**实例1**

![a_star1](pic/a_star1.png)

**实例2**

![a_star2](pic/a_star2.png)

#### 程序实现


```python
# ----------
# User Instructions:
# 
# Define a function, search() that returns a list
# in the form of [optimal path length, row, col]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space


grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
# 添加 heuristic function 
heuristic = [[9, 8, 7, 6, 5, 4],
             [8, 7, 6, 5, 4, 3],
             [7, 6, 5, 4, 3, 2],
             [6, 5, 4, 3, 2, 1],
             [5, 4, 3, 2, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1

delta = [[-1, 0], # go up
         [ 0,-1], # go left
         [ 1, 0], # go down
         [ 0, 1]] # go right

delta_name = ['^', '<', 'v', '>']

def search(grid,init,goal,cost):
    
    closed = [[0 for col in range(len(grid[0]))] for row in range(len(grid))]
    closed[init[0]][init[0]] = 1
    expand = [[-1 for col in range(len(grid[0]))] for row in range(len(grid))]
    
    # 记录每次的移动， a --> b, 把动作记录在 b
    action = [[-1 for col in range(len(grid[0]))] for row in range(len(grid))]
    
    
    x = init[0]
    y = init[1]
    g = 0
    h = heuristic[x][y]
    f = g + h
    
    
    open = [[f, g, h, x, y]]

    found = False 
    resign =  False # flag that cannot reach the goal
    count = 0
     
    while found is False and resign is False:  # 还没搜索到，并且还有可以搜索的空间
        
        if len(open) == 0:
            resign = True
            print('fail')
        else:  # 扩展搜索
            open.sort()
            open.reverse()
            next = open.pop()
            x = next[3]
            y = next[4]
            g = next[1]
            expand[x][y] = count
            count += 1
            if x == goal[0] and y == goal[1]:
                found = True
                print('Got it: ', next)
            else:
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >= 0 and y2 < len(grid[0]):
                        if closed[x2][y2] == 0 and grid[x2][y2] == 0:
                            g2 = g + cost
                            h2 = heuristic[x2][y2]
                            f2 = g2 + h2
                            open.append([f2, g2, h2, x2, y2])
                            closed[x2][y2] = 1
                            action[x2][y2] = i
    
    # 显示搜索顺序，以便后边与 A* 对比
    print('\n The order of search: ')
    for i in range(len(expand)):        
        print(expand[i])
    
    
    # 令路径显示的更直观
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    x = goal[0]
    y = goal[1]
    policy[x][y] = '*'

    while x != init[0] or y != init[1]:
        x2 = x - delta[action[x][y]][0]
        y2 = y - delta[action[x][y]][1]
        policy[x2][y2] = delta_name[action[x][y]]
        x = x2
        y = y2
    print('\n The optimal path: ')
    for i in range(len(policy)):
        print(policy[i])
        
search(grid, init, goal, cost) 
```

    ('Got it: ', [11, 11, 0, 4, 5])
    
     The order of search: 
    [0, -1, -1, -1, -1, -1]
    [1, -1, -1, -1, -1, -1]
    [2, -1, -1, -1, -1, -1]
    [3, -1, 8, 9, 10, 11]
    [4, 5, 6, 7, -1, 12]
    
     The optimal path: 
    ['v', ' ', ' ', ' ', ' ', ' ']
    ['v', ' ', ' ', ' ', ' ', ' ']
    ['v', ' ', ' ', ' ', ' ', ' ']
    ['v', ' ', ' ', '>', '>', 'v']
    ['>', '>', '>', '^', ' ', '*']

#### A* 找到最优解的条件

h <= true cost，即 h is never overestimated, 也称为 h admissable.

对于一般的问题，都会有一些限制条件，比如移动或者搜索的限制。在设计 h 值时，可以放松这些限制，得到宽松条件下的 cost，比如不考虑障碍物，不考虑是否有可达路径等，这样得到的 cost 会比实际的小，就是 admissable 的。

### Dynamic programming

#### 基本思想

上述 基本搜索算法 和 A* 都是找出给定初始位置到目标位置的最短路径。

Dynamic programming 则是找到空间中 所有位置 到目标位置的最短路径。它是从目标位置出发，发散寻找，不预设起始位置。

这个算法的好处是方便应对条件变动，可以很快找到新的路径。

![dynamic_programming](pic/dynamic_programming1.png)

#### 程序实现


```python
# ----------
# User Instructions:
# 
# Create a function compute_value which returns
# a grid of values. The value of a cell is the minimum
# number of moves required to get from the cell to the goal. 
#
# If a cell is a wall or it is impossible to reach the goal from a cell,
# assign that cell a value of 99.
# ----------

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1 # the cost associated with moving from a cell to an adjacent one

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

def compute_value(grid,goal,cost):
    # ----------------------------------------
    # insert code below
    # ----------------------------------------
    value = [[99 for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    change = True
    while change:
        change = False
        # 其实在第一次 while 全面循环之后，只有 goal 位置（及其之后的 cell，在此例中，没有这一类 cell）
        # value 有改变, 其他位置都不变.
        # 每次 while 全面循环，改变都是逐渐往外扩散，一次全面循环，扩散一层
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                if x == goal[0] and y == goal[1]:
                    if value[x][y] > 0:
                        value[x][y] = 0
                        policy[x][y] = '*'
                        change = True
                elif grid[x][y] == 0:
                    for a in range(len(delta)):
                        x2 = x + delta[a][0]
                        y2 = y + delta[a][1]
                        if x2 >= 0 and x2 < len(grid) and y2 >= 0 \
                        and y2 < len(grid[0]) and grid[x2][y2] == 0:
                            v2 = value[x2][y2] + cost
                            if v2 < value[x][y]:
                                change = True
                                value[x][y] = v2
                                policy[x][y] = delta_name[a]
    # make sure your function returns a grid of values as 
    # demonstrated in the previous video.
    return value, policy 

compute_value(grid,goal,cost)
```




    ([[11, 99, 7, 6, 5, 4],
      [10, 99, 6, 5, 4, 3],
      [9, 99, 5, 4, 3, 2],
      [8, 99, 4, 3, 2, 1],
      [7, 6, 5, 4, 99, 0]],
     [['v', ' ', 'v', 'v', 'v', 'v'],
      ['v', ' ', 'v', 'v', 'v', 'v'],
      ['v', ' ', 'v', 'v', 'v', 'v'],
      ['v', ' ', '>', '>', '>', 'v'],
      ['>', '>', '^', '^', ' ', '*']])

#### 一个更加实际的程序

交叉路口，左转受限

先用 DP 搜索所有最优路径，然后给定出发点，确定最终的路线

![dp_turn_left1](pic/dp_turn_left1.png)


```python
# ----------
# User Instructions:
# 
# Implement the function optimum_policy2D below.
#
# You are given a car in grid with initial state
# init. Your task is to compute and return the car's 
# optimal path to the position specified in goal; 
# the costs for each motion are as defined in cost.
#
# There are four motion directions: up, left, down, and right.
# Increasing the index in this array corresponds to making a
# a left turn, and decreasing the index corresponds to making a 
# right turn.

forward = [[-1,  0], # go up
           [ 0, -1], # go left
           [ 1,  0], # go down
           [ 0,  1]] # go right
forward_name = ['up', 'left', 'down', 'right']

# action has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']

# EXAMPLE INPUTS:
# grid format:
#     0 = navigable space
#     1 = unnavigable space 
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]

init = [4, 3, 0] # given in the form [row,col,direction]
                 # direction = 0: up
                 #             1: left
                 #             2: down
                 #             3: right
                
goal = [2, 0] # given in the form [row,col]

cost = [2, 1, 20] # cost has 3 values, corresponding to making 
                  # a right turn, no turn, and a left turn

# EXAMPLE OUTPUT:
# calling optimum_policy2D with the given parameters should return 
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
# ----------

# ----------------------------------------
# modify code below
# ----------------------------------------

def optimum_policy2D(grid,init,goal,cost):
    value = [[[999 for col in range(len(grid[0]))] for row in range(len(grid))],
             [[999 for col in range(len(grid[0]))] for row in range(len(grid))],
             [[999 for col in range(len(grid[0]))] for row in range(len(grid))],
             [[999 for col in range(len(grid[0]))] for row in range(len(grid))],]
    policy = [[[' ' for col in range(len(grid[0]))] for row in range(len(grid))],
             [[' ' for col in range(len(grid[0]))] for row in range(len(grid))],
             [[' ' for col in range(len(grid[0]))] for row in range(len(grid))],
             [[' ' for col in range(len(grid[0]))] for row in range(len(grid))],]
    policy2D = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    
    change = True
    while change:
        change = False
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                for orientation in range(4):
                    if goal[0] == x and goal[1] == y:
                        if value[orientation][x][y] > 0:
                            change = True
                            value[orientation][x][y] = 0
                            policy[orientation][x][y] = '*'
                    elif grid[x][y] == 0:
                        for i in range(3):
                            o2 = (orientation + action[i]) % 4
                            # choose steering first, then moves
                            x2 = x + forward[o2][0]
                            y2 = y + forward[o2][1]
                            if x2 >=0 and x2 < len(grid) and y2 >= 0 and y2 < len(grid[0]) \
                               and grid[x2][y2] == 0:
                                v2 = value[o2][x2][y2] + cost[i]
                                if v2 < value[orientation][x][y]:
                                    value[orientation][x][y] = v2
                                    policy[orientation][x][y] = action_name[i]
                                    change = True
    # 前边计算出来了所有 cell 的最优动作，这里只是选定出发点，依次显示出来
    x = init[0]
    y = init[1]
    orientation = init[2]
    
    policy2D[x][y] = policy[orientation][x][y]
    while policy[orientation][x][y] != '*':
        if policy[orientation][x][y] == '#':
            o2 = orientation
        elif policy[orientation][x][y] == 'R':
            o2 = (orientation - 1) % 4
        elif policy[orientation][x][y] == 'L':
            o2 = (orientation + 1) % 4
        x = x + forward[o2][0]
        y = y + forward[o2][1]
        orientation = o2
        policy2D[x][y] = policy[orientation][x][y]

    return policy2D

optimum_policy2D(grid,init,goal,cost)
```




    [[' ', ' ', ' ', 'R', '#', 'R'],
     [' ', ' ', ' ', '#', ' ', '#'],
     ['*', '#', '#', '#', '#', 'R'],
     [' ', ' ', ' ', '#', ' ', ' '],
     [' ', ' ', ' ', '#', ' ', ' ']]

#### 考虑 stochastic motion

实际中，机器人可能并不能完全按照预想的路线前进，如果预设路线很贴近障碍物，则有可能发生碰撞。此时，可以在 DP 中考虑一定程度的随机性，使机器人离障碍物稍微远一些

![dp_stochastic](pic/dp_stochastic1.png)


```python
# --------------
# USER INSTRUCTIONS
#
# Write a function called stochastic_value that 
# returns two grids. The first grid, value, should 
# contain the computed value of each cell as shown 
# in the video. The second grid, policy, should 
# contain the optimum policy for each cell.
#
# --------------
# GRADING NOTES
#
# We will be calling your stochastic_value function
# with several different grids and different values
# of success_prob, collision_cost, and cost_step.
# In order to be marked correct, your function must
# RETURN (it does not have to print) two grids,
# value and policy.
#
# When grading your value grid, we will compare the
# value of each cell with the true value according
# to this model. If your answer for each cell
# is sufficiently close to the correct answer
# (within 0.001), you will be marked as correct.

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>'] # Use these when creating your policy grid.

# ---------------------------------------------
#  Modify the function stochastic_value below
# ---------------------------------------------

def stochastic_value(grid,goal,cost_step,collision_cost,success_prob):
    failure_prob = (1.0 - success_prob)/2.0 # Probability(stepping left) = prob(stepping right) = failure_prob
    value = [[collision_cost for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    
    change = True
    while change:
        change = False
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                if x == goal[0] and y == goal[1]:
                    if value[x][y] > 0:
                        value[x][y] = 0
                        policy[x][y] = '*'
                        change = True
                elif grid[x][y] == 0:
                    for a in range(len(delta)):
                        v2 = cost_step
                        for i in range(-1, 2):
                            a2 = (a + i) % len(delta)
                            x2 = x + delta[a2][0]
                            y2 = y + delta[a2][1]
                            if i == 0:
                                p2 = success_prob
                            else:
                                p2 = (1-success_prob)/2
                            if x2 >= 0 and x2 < len(grid) and y2 >= 0 \
                            and y2 < len(grid[0]) and grid[x2][y2] == 0:
                                v2 += p2 * value[x2][y2]
                            else:
                                v2 += p2 * collision_cost
                        if v2 < value[x][y]:
                            change = True
                            value[x][y] = v2
                            policy[x][y] = delta_name[a]
    
    return value, policy

# ---------------------------------------------
#  Use the code below to test your solution
# ---------------------------------------------

grid = [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0]]
goal = [0, len(grid[0])-1] # Goal is in top right corner
cost_step = 1
collision_cost = 1000
success_prob = 0.5

value,policy = stochastic_value(grid,goal,cost_step,collision_cost,success_prob)
for row in value:
    print row
for row in policy:
    print row

# Expected outputs:
#
#[471.9397246855924, 274.85364957758316, 161.5599867065471, 0],
#[334.05159958720344, 230.9574434590965, 183.69314862430264, 176.69517762501977], 
#[398.3517867450282, 277.5898270101976, 246.09263437756917, 335.3944132514738], 
#[700.1758933725141, 1000, 1000, 668.697206625737]


#
# ['>', 'v', 'v', '*']
# ['>', '>', '^', '<']
# ['>', '^', '^', '<']
# ['^', ' ', ' ', '^']

```

    [471.9397246855924, 274.85364957758316, 161.5599867065471, 0]
    [334.05159958720344, 230.9574434590965, 183.69314862430264, 176.69517762501977]
    [398.3517867450282, 277.5898270101976, 246.09263437756917, 335.3944132514738]
    [700.1758933725141, 1000, 1000, 668.697206625737]
    ['>', 'v', 'v', '*']
    ['>', '>', '^', '<']
    ['>', '^', '^', '<']
    ['^', ' ', ' ', '^']

### path smooth

#### 基本思想

上述各种搜索算法在 grid 中找到的 path 往往是直角转弯的形式，实际上车很难作出那种动作，需要对 path 做平滑处理

![smooth_path1](pic/smooth_path1.png)

步骤：
1. 每个原始路径节点 $x_i$ copy 出一个新节点 $y_i$， path ---> newpath，可以不考虑首尾节点的平滑
2. 对每个新节点做如下两个优化（横纵坐标可以分别优化，互不影响）：
   - $y_i = y_i + \alpha (x_i - y_i)$ # 使新节点尽量接近原节点
   - $y_i = y_i + \beta (y_{i-1} + y_{i+1} - 2 y_i)$ # 新节点之间尽量靠近
   - 在实际操作中，上述两式合二为一： $y_i = y_i + \alpha (x_i - y_i) + \beta (y_{i-1} + y_{i+1} - 2 y_i)$
3. 重复优化步骤，直到两次优化之后的 y_i 值变动小于给定阈值。

#### 程序实现


```python
# -----------
# User Instructions
#
# Define a function smooth that takes a path as its input
# (with optional parameters for weight_data, weight_smooth,
# and tolerance) and returns a smooth path. The first and 
# last points should remain unchanged.
#
# Smoothing should be implemented by iteratively updating
# each entry in newpath until some desired level of accuracy
# is reached. The update should be done according to the
# gradient descent equations given in the instructor's note
# below.
# -----------

from copy import deepcopy

# thank you to EnTerr for posting this on our discussion forum
def printpaths(path,newpath):
    for old,new in zip(path,newpath):
        print '['+ ', '.join('%.3f'%x for x in old) + \
               '] -> ['+ ', '.join('%.3f'%x for x in new) +']'

# Don't modify path inside your function.
path = [[0, 0],
        [0, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        [4, 2],
        [4, 3],
        [4, 4]]

def smooth(path, weight_data = 0.5, weight_smooth = 0.1, tolerance = 0.000001):

    # Make a deep copy of path into newpath
    newpath = deepcopy(path)

    #######################
    ### ENTER CODE HERE ###
    #######################
    
    change = tolerance
    while change >= tolerance:
        change = 0
        for i in range(1, len(path)-1):
            for j in range(len(path[0])):
                aux = newpath[i][j]
                newpath[i][j] += weight_data * (path[i][j] - newpath[i][j]) \
                + weight_smooth * (newpath[i-1][j] + newpath[i+1][j] - (2 * newpath[i][j]))
                change += abs(aux - newpath[i][j])
    
    return newpath # Leave this line for the grader!

printpaths(path,smooth(path))

```

    [0.000, 0.000] -> [0.000, 0.000]
    [0.000, 1.000] -> [0.021, 0.979]
    [0.000, 2.000] -> [0.149, 1.851]
    [1.000, 2.000] -> [1.021, 1.979]
    [2.000, 2.000] -> [2.000, 2.000]
    [3.000, 2.000] -> [2.979, 2.021]
    [4.000, 2.000] -> [3.851, 2.149]
    [4.000, 3.000] -> [3.979, 3.021]
    [4.000, 4.000] -> [4.000, 4.000]

## trajectory generating

![trajectory1](pic/trajectory1.png)

结合 trajectory and the result from prediction module, we know whether a waypoint is reachable as expected.

path-velocity decoupled planning，即路线规划与速度规划可以独立进行。
### path generation and selection:

1. 把道路分成小段
2. 每段撒点
3. 把点随机串起来就得到可能的路径
4. 基于cost function，选择最优路径

![path_generation](pic/path_generation.png)

### cost function 

在具体设计 trajectory 时，要考虑一些实际的约束
- obstacle free, 即不能撞到任何物体
- 乘客舒适性，即加速减速转角都应尽量平缓
- 物理可实现，即高速运动时，无法大角度转弯，否则容易翻车
- 遵守交通规则，不能为了超车而速度太快，不能压实线， 尽量保持在中线，不要 deviation from the center line

在评价各种可能的 trajectory 时，可以借助 cost function

![trajectory2](pic/trajectory3.png)

### Frenet coordinate

在描述车的运动时，往往采用 Frenet 坐标系，它比传统的 Cartesian 坐标系更方便

![frenet](pic/frenet1.png)

### ST graph

横坐标为时间，纵坐标为Frenet coordinate 中的 longitudinal axis

![st_graph](pic/st_graph1.png)

在确定速度时，要考虑周边环境的影响，例如前方有车占路，如何调整速度，避免碰撞

![speed_profile1](pic/speed_profile1.png)

由于在实际上是分段进行最优化求解，得到的路径和速度可能不平滑，此时要采用 quadratic programming 进一步得到平滑的路径和速度。

# PID control

## 基本思想

P 是最基础的，可能出现振荡

D 相当于加入阻尼， 加入 D 可以提前减速，减轻振荡

PD 无法克服 system bias，例如，车轮应该稍向右转，以接近期望路径，但是车轮角度有向左的系统性误差，指定右转时，车子实际上是直行。在这种情况下，车永远是直行，永远希望向右转，车子的运行路径跟期望路径存在稳态误差。

## 程序实现


```python
# -----------
# User Instructions
#
# Implement a P controller by running 100 iterations
# of robot motion. The desired trajectory for the 
# robot is the x-axis. The steering angle should be set
# by the parameter tau so that:
#
# steering = -tau * crosstrack_error
#
# You'll only need to modify the `run` function at the bottom.
# ------------

import random
import numpy as np
import matplotlib.pyplot as plt

# ------------------------------------------------
# 
# this is the Robot class
#

class Robot(object):
    def __init__(self, length=20.0):
        """
        Creates robot and initializes location/orientation to 0, 0, 0.
        """
        self.x = 0.0
        self.y = 0.0
        self.orientation = 0.0
        self.length = length
        self.steering_noise = 0.0
        self.distance_noise = 0.0
        self.steering_drift = 0.0

    def set(self, x, y, orientation):
        """
        Sets a robot coordinate.
        """
        self.x = x
        self.y = y
        self.orientation = orientation % (2.0 * np.pi)

    def set_noise(self, steering_noise, distance_noise):
        """
        Sets the noise parameters.
        """
        # makes it possible to change the noise parameters
        # this is often useful in particle filters
        self.steering_noise = steering_noise
        self.distance_noise = distance_noise

    def set_steering_drift(self, drift):
        """
        Sets the systematical steering drift parameter
        """
        self.steering_drift = drift

    def move(self, steering, distance, tolerance=0.001, max_steering_angle=np.pi / 4.0):
        """
        steering = front wheel steering angle, limited by max_steering_angle
        distance = total distance driven, most be non-negative
        """
        if steering > max_steering_angle:
            steering = max_steering_angle
        if steering < -max_steering_angle:
            steering = -max_steering_angle
        if distance < 0.0:
            distance = 0.0

        # apply noise
        steering2 = random.gauss(steering, self.steering_noise)
        distance2 = random.gauss(distance, self.distance_noise)

        # apply steering drift
        steering2 += self.steering_drift

        # Execute motion
        turn = np.tan(steering2) * distance2 / self.length

        if abs(turn) < tolerance:
            # approximate by straight line motion
            self.x += distance2 * np.cos(self.orientation)
            self.y += distance2 * np.sin(self.orientation)
            self.orientation = (self.orientation + turn) % (2.0 * np.pi)
        else:
            # approximate bicycle model for motion
            radius = distance2 / turn
            cx = self.x - (np.sin(self.orientation) * radius)
            cy = self.y + (np.cos(self.orientation) * radius)
            self.orientation = (self.orientation + turn) % (2.0 * np.pi)
            self.x = cx + (np.sin(self.orientation) * radius)
            self.y = cy - (np.cos(self.orientation) * radius)

    def __repr__(self):
        return '[x=%.5f y=%.5f orient=%.5f]' % (self.x, self.y, self.orientation)

############## ADD / MODIFY CODE BELOW ####################
# ------------------------------------------------------------------------
#
# run - does a single control run
robot = Robot()
robot.set(0.0, 1.0, 0.0)

def run(robot, tau_p, tau_d, tau_i, n=100, speed=1.0):
    x_trajectory = []
    y_trajectory = []
    
    crosstrack_error = robot.y
    int_crosstrack_error = 0.0
    for i in range(n):
        diff_crosstrack_error = robot.y - crosstrack_error
        crosstrack_error = robot.y
        int_crosstrack_error += crosstrack_error
        steer = -tau_p * crosstrack_error - tau_d * diff_crosstrack_error \
        - tau_i * int_crosstrack_error
        robot.move(steer, speed)
        x_trajectory.append(robot.x)
        y_trajectory.append(robot.y)    
    
    return x_trajectory, y_trajectory

x_trajectory, y_trajectory = run(robot, 0.1, 1.0, 0.00008)
n = len(x_trajectory)

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 8))
ax1.plot(x_trajectory, y_trajectory, 'g', label='P controller')
ax1.plot(x_trajectory, np.zeros(n), 'r', label='reference')
```




    [<matplotlib.lines.Line2D at 0x7f73ada26fd0>]




![png](output_75_1.png)

## 用 twiddle 算法调节 PID 参数

依次调节三个参数，如果增加效果不好就减小，如果增大减小效果都不好，就缩小 $\delta$.多次调节，直到 $\delta$ 缩小到阈值以下

![twiddle1](pic/twiddle1.png)


```python
# ----------------
# User Instructions
#
# Implement twiddle as shown in the previous two videos.
# Your accumulated error should be very small!
#
# Your twiddle function should RETURN the accumulated
# error. Try adjusting the parameters p and dp to make
# this error as small as possible.
#
# Try to get your error below 1.0e-10 with as few iterations
# as possible (too many iterations will cause a timeout).
# No cheating!
# ------------

import random
import numpy as np

# ------------------------------------------------
# 
# this is the Robot class
#

class Robot(object):
    def __init__(self, length=20.0):
        """
        Creates robot and initializes location/orientation to 0, 0, 0.
        """
        self.x = 0.0
        self.y = 0.0
        self.orientation = 0.0
        self.length = length
        self.steering_noise = 0.0
        self.distance_noise = 0.0
        self.steering_drift = 0.0

    def set(self, x, y, orientation):
        """
        Sets a robot coordinate.
        """
        self.x = x
        self.y = y
        self.orientation = orientation % (2.0 * np.pi)

    def set_noise(self, steering_noise, distance_noise):
        """
        Sets the noise parameters.
        """
        # makes it possible to change the noise parameters
        # this is often useful in particle filters
        self.steering_noise = steering_noise
        self.distance_noise = distance_noise

    def set_steering_drift(self, drift):
        """
        Sets the systematical steering drift parameter
        """
        self.steering_drift = drift

    def move(self, steering, distance, tolerance=0.001, max_steering_angle=np.pi / 4.0):
        """
        steering = front wheel steering angle, limited by max_steering_angle
        distance = total distance driven, most be non-negative
        """
        if steering > max_steering_angle:
            steering = max_steering_angle
        if steering < -max_steering_angle:
            steering = -max_steering_angle
        if distance < 0.0:
            distance = 0.0

        # make a new copy
        # res = Robot()
        # res.length = self.length
        # res.steering_noise = self.steering_noise
        # res.distance_noise = self.distance_noise
        # res.steering_drift = self.steering_drift

        # apply noise
        steering2 = random.gauss(steering, self.steering_noise)
        distance2 = random.gauss(distance, self.distance_noise)

        # apply steering drift
        steering2 += self.steering_drift

        # Execute motion
        turn = np.tan(steering2) * distance2 / self.length

        if abs(turn) < tolerance:
            # approximate by straight line motion
            self.x += distance2 * np.cos(self.orientation)
            self.y += distance2 * np.sin(self.orientation)
            self.orientation = (self.orientation + turn) % (2.0 * np.pi)
        else:
            # approximate bicycle model for motion
            radius = distance2 / turn
            cx = self.x - (np.sin(self.orientation) * radius)
            cy = self.y + (np.cos(self.orientation) * radius)
            self.orientation = (self.orientation + turn) % (2.0 * np.pi)
            self.x = cx + (np.sin(self.orientation) * radius)
            self.y = cy - (np.cos(self.orientation) * radius)

    def __repr__(self):
        return '[x=%.5f y=%.5f orient=%.5f]' % (self.x, self.y, self.orientation)

############## ADD / MODIFY CODE BELOW ####################
# ------------------------------------------------------------------------
#
# run - does a single control run


def make_robot():
    """
    Resets the robot back to the initial position and drift.
    You'll want to call this after you call `run`.
    """
    robot = Robot()
    robot.set(0.0, 1.0, 0.0)
    robot.set_steering_drift(10.0 / 180.0 * np.pi)
    return robot

# NOTE: We use params instead of tau_p, tau_d, tau_i
def run(robot, params, n=100, speed=1.0):
    x_trajectory = []
    y_trajectory = []
    err = 0
    # TODO: your code here
    prev_cte = robot.y
    int_cte = 0
    for i in range(2 * n):
        cte = robot.y
        diff_cte = cte - prev_cte
        int_cte += cte
        prev_cte = cte
        steer = -params[0] * cte - params[1] * diff_cte - params[2] * int_cte
        robot.move(steer, speed)
        x_trajectory.append(robot.x)
        y_trajectory.append(robot.y)
        if i >= n:
            err += cte ** 2
    return x_trajectory, y_trajectory, err / n


# Make this tolerance bigger if you are timing out!
def twiddle(tol=0.2): 
    # TODO: Add code here
    # Don't forget to call `make_robot` before you call `run`!
    p = [0.0, 0.0, 0.0]
    dp = [1.0, 1.0, 1.0]
    robot = make_robot()
    x_trajectory, y_trajectory, best_err = run(robot, p)

    it = 0
    while sum(dp) > tol:
        # print("Iteration {}, best error = {}".format(it, best_err))
        for i in range(len(p)):
            p[i] += dp[i]
            robot = make_robot()
            x_trajectory, y_trajectory, err = run(robot, p)

            if err < best_err:
                best_err = err
                dp[i] *= 1.1
            else:
                p[i] -= 2 * dp[i]
                robot = make_robot()
                x_trajectory, y_trajectory, err = run(robot, p)

                if err < best_err:
                    best_err = err
                    dp[i] *= 1.1
                else:
                    p[i] += dp[i]
                    dp[i] *= 0.9
        it += 1
    return p, best_err

twiddle()
```




    ([2.9331227688652457, 10.326589894591526, 0.49316041639454505],
     6.4862484209746587e-16)

## Put together


```python
# -----------
# User Instructions
#
# Familiarize yourself with the code below. Most of it
# reproduces results that you have obtained at some
# point in this class. Once you understand the code,
# write a function, cte, in the run class that
# computes the crosstrack
# error for the case of a segmented path. You will
# need to include the equations shown in the video.
#
 
from math import *
import random


# don't change the noise pameters

steering_noise    = 0.1
distance_noise    = 0.03
measurement_noise = 0.3


class plan:

    # --------
    # init: 
    #    creates an empty plan
    #

    def __init__(self, grid, init, goal, cost = 1):
        self.cost = cost
        self.grid = grid
        self.init = init
        self.goal = goal
        self.make_heuristic(grid, goal, self.cost)
        self.path = []
        self.spath = []

    # --------
    #
    # make heuristic function for a grid
        
    def make_heuristic(self, grid, goal, cost):
        self.heuristic = [[0 for row in range(len(grid[0]))] 
                          for col in range(len(grid))]
        for i in range(len(self.grid)):    
            for j in range(len(self.grid[0])):
                self.heuristic[i][j] = abs(i - self.goal[0]) + \
                    abs(j - self.goal[1])



    # ------------------------------------------------
    # 
    # A* for searching a path to the goal
    #
    #

    def astar(self):


        if self.heuristic == []:
            raise ValueError, "Heuristic must be defined to run A*"

        # internal motion parameters
        delta = [[-1,  0], # go up
                 [ 0,  -1], # go left
                 [ 1,  0], # go down
                 [ 0,  1]] # do right


        # open list elements are of the type: [f, g, h, x, y]

        closed = [[0 for row in range(len(self.grid[0]))] 
                  for col in range(len(self.grid))]
        action = [[0 for row in range(len(self.grid[0]))] 
                  for col in range(len(self.grid))]

        closed[self.init[0]][self.init[1]] = 1


        x = self.init[0]
        y = self.init[1]
        h = self.heuristic[x][y]
        g = 0
        f = g + h

        open = [[f, g, h, x, y]]

        found  = False # flag that is set when search complete
        resign = False # flag set if we can't find expand
        count  = 0


        while not found and not resign:

            # check if we still have elements on the open list
            if len(open) == 0:
                resign = True
                print '###### Search terminated without success'
                
            else:
                # remove node from list
                open.sort()
                open.reverse()
                next = open.pop()
                x = next[3]
                y = next[4]
                g = next[1]

            # check if we are done

            if x == goal[0] and y == goal[1]:
                found = True
                # print '###### A* search successful'

            else:
                # expand winning element and add to new open list
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(self.grid) and y2 >= 0 \
                            and y2 < len(self.grid[0]):
                        if closed[x2][y2] == 0 and self.grid[x2][y2] == 0:
                            g2 = g + self.cost
                            h2 = self.heuristic[x2][y2]
                            f2 = g2 + h2
                            open.append([f2, g2, h2, x2, y2])
                            closed[x2][y2] = 1
                            action[x2][y2] = i

            count += 1

        # extract the path



        invpath = []
        x = self.goal[0]
        y = self.goal[1]
        invpath.append([x, y])
        while x != self.init[0] or y != self.init[1]:
            x2 = x - delta[action[x][y]][0]
            y2 = y - delta[action[x][y]][1]
            x = x2
            y = y2
            invpath.append([x, y])

        self.path = []
        for i in range(len(invpath)):
            self.path.append(invpath[len(invpath) - 1 - i])




    # ------------------------------------------------
    # 
    # this is the smoothing function
    #

  


    def smooth(self, weight_data = 0.1, weight_smooth = 0.1, 
               tolerance = 0.000001):

        if self.path == []:
            raise ValueError, "Run A* first before smoothing path"

        self.spath = [[0 for row in range(len(self.path[0]))] \
                           for col in range(len(self.path))]
        for i in range(len(self.path)):
            for j in range(len(self.path[0])):
                self.spath[i][j] = self.path[i][j]

        change = tolerance
        while change >= tolerance:
            change = 0.0
            for i in range(1, len(self.path)-1):
                for j in range(len(self.path[0])):
                    aux = self.spath[i][j]
                    
                    self.spath[i][j] += weight_data * \
                        (self.path[i][j] - self.spath[i][j])
                    
                    self.spath[i][j] += weight_smooth * \
                        (self.spath[i-1][j] + self.spath[i+1][j] 
                         - (2.0 * self.spath[i][j]))
                    if i >= 2:
                        self.spath[i][j] += 0.5 * weight_smooth * \
                            (2.0 * self.spath[i-1][j] - self.spath[i-2][j] 
                             - self.spath[i][j])
                    if i <= len(self.path) - 3:
                        self.spath[i][j] += 0.5 * weight_smooth * \
                            (2.0 * self.spath[i+1][j] - self.spath[i+2][j] 
                             - self.spath[i][j])
                
            change += abs(aux - self.spath[i][j])
                






# ------------------------------------------------
# 
# this is the robot class
#

class robot:

    # --------
    # init: 
    #	creates robot and initializes location/orientation to 0, 0, 0
    #

    def __init__(self, length = 0.5):
        self.x = 0.0
        self.y = 0.0
        self.orientation = 0.0
        self.length = length
        self.steering_noise    = 0.0
        self.distance_noise    = 0.0
        self.measurement_noise = 0.0
        self.num_collisions    = 0
        self.num_steps         = 0

    # --------
    # set: 
    #	sets a robot coordinate
    #

    def set(self, new_x, new_y, new_orientation):

        self.x = float(new_x)
        self.y = float(new_y)
        self.orientation = float(new_orientation) % (2.0 * pi)


    # --------
    # set_noise: 
    #	sets the noise parameters
    #

    def set_noise(self, new_s_noise, new_d_noise, new_m_noise):
        # makes it possible to change the noise parameters
        # this is often useful in particle filters
        self.steering_noise     = float(new_s_noise)
        self.distance_noise    = float(new_d_noise)
        self.measurement_noise = float(new_m_noise)

    # --------
    # check: 
    #    checks of the robot pose collides with an obstacle, or
    # is too far outside the plane

    def check_collision(self, grid):
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 1:
                    dist = sqrt((self.x - float(i)) ** 2 + 
                                (self.y - float(j)) ** 2)
                    if dist < 0.5:
                        self.num_collisions += 1
                        return False
        return True
        
    def check_goal(self, goal, threshold = 1.0):
        dist =  sqrt((float(goal[0]) - self.x) ** 2 + (float(goal[1]) - self.y) ** 2)
        return dist < threshold
        
    # --------
    # move: 
    #    steering = front wheel steering angle, limited by max_steering_angle
    #    distance = total distance driven, most be non-negative

    def move(self, grid, steering, distance, 
             tolerance = 0.001, max_steering_angle = pi / 4.0):

        if steering > max_steering_angle:
            steering = max_steering_angle
        if steering < -max_steering_angle:
            steering = -max_steering_angle
        if distance < 0.0:
            distance = 0.0


        # make a new copy
        res = robot()
        res.length            = self.length
        res.steering_noise    = self.steering_noise
        res.distance_noise    = self.distance_noise
        res.measurement_noise = self.measurement_noise
        res.num_collisions    = self.num_collisions
        res.num_steps         = self.num_steps + 1

        # apply noise
        steering2 = random.gauss(steering, self.steering_noise)
        distance2 = random.gauss(distance, self.distance_noise)


        # Execute motion
        turn = tan(steering2) * distance2 / res.length

        if abs(turn) < tolerance:

            # approximate by straight line motion

            res.x = self.x + (distance2 * cos(self.orientation))
            res.y = self.y + (distance2 * sin(self.orientation))
            res.orientation = (self.orientation + turn) % (2.0 * pi)

        else:

            # approximate bicycle model for motion

            radius = distance2 / turn
            cx = self.x - (sin(self.orientation) * radius)
            cy = self.y + (cos(self.orientation) * radius)
            res.orientation = (self.orientation + turn) % (2.0 * pi)
            res.x = cx + (sin(res.orientation) * radius)
            res.y = cy - (cos(res.orientation) * radius)

        # check for collision
        # res.check_collision(grid)

        return res

    # --------
    # sense: 
    #    

    def sense(self):

        return [random.gauss(self.x, self.measurement_noise),
                random.gauss(self.y, self.measurement_noise)]

    # --------
    # measurement_prob
    #    computes the probability of a measurement
    # 

    def measurement_prob(self, measurement):

        # compute errors
        error_x = measurement[0] - self.x
        error_y = measurement[1] - self.y

        # calculate Gaussian
        error = exp(- (error_x ** 2) / (self.measurement_noise ** 2) / 2.0) \
            / sqrt(2.0 * pi * (self.measurement_noise ** 2))
        error *= exp(- (error_y ** 2) / (self.measurement_noise ** 2) / 2.0) \
            / sqrt(2.0 * pi * (self.measurement_noise ** 2))

        return error



    def __repr__(self):
        # return '[x=%.5f y=%.5f orient=%.5f]'  % (self.x, self.y, self.orientation)
        return '[%.5f, %.5f]'  % (self.x, self.y)






# ------------------------------------------------
# 
# this is the particle filter class
#

class particles:

    # --------
    # init: 
    #	creates particle set with given initial position
    #

    def __init__(self, x, y, theta, 
                 steering_noise, distance_noise, measurement_noise, N = 100):
        self.N = N
        self.steering_noise    = steering_noise
        self.distance_noise    = distance_noise
        self.measurement_noise = measurement_noise
        
        self.data = []
        for i in range(self.N):
            r = robot()
            r.set(x, y, theta)
            r.set_noise(steering_noise, distance_noise, measurement_noise)
            self.data.append(r)


    # --------
    #
    # extract position from a particle set
    # 
    
    def get_position(self):
        x = 0.0
        y = 0.0
        orientation = 0.0

        for i in range(self.N):
            x += self.data[i].x
            y += self.data[i].y
            # orientation is tricky because it is cyclic. By normalizing
            # around the first particle we are somewhat more robust to
            # the 0=2pi problem
            orientation += (((self.data[i].orientation
                              - self.data[0].orientation + pi) % (2.0 * pi)) 
                            + self.data[0].orientation - pi)
        return [x / self.N, y / self.N, orientation / self.N]

    # --------
    #
    # motion of the particles
    # 

    def move(self, grid, steer, speed):
        newdata = []

        for i in range(self.N):
            r = self.data[i].move(grid, steer, speed)
            newdata.append(r)
        self.data = newdata

    # --------
    #
    # sensing and resampling
    # 

    def sense(self, Z):
        w = []
        for i in range(self.N):
            w.append(self.data[i].measurement_prob(Z))

        # resampling (careful, this is using shallow copy)
        p3 = []
        index = int(random.random() * self.N)
        beta = 0.0
        mw = max(w)

        for i in range(self.N):
            beta += random.random() * 2.0 * mw
            while beta > w[index]:
                beta -= w[index]
                index = (index + 1) % self.N
            p3.append(self.data[index])
        self.data = p3

    



    

# --------
#
# run:  runs control program for the robot
#


def run(grid, goal, spath, params, printflag = False, speed = 0.1, timeout = 1000):

    myrobot = robot()
    myrobot.set(0., 0., 0.)
    myrobot.set_noise(steering_noise, distance_noise, measurement_noise)
    filter = particles(myrobot.x, myrobot.y, myrobot.orientation,
                       steering_noise, distance_noise, measurement_noise)

    cte  = 0.0
    err  = 0.0
    N    = 0

    index = 0 # index into the path
    
    while not myrobot.check_goal(goal) and N < timeout:

        diff_cte = - cte


        # ----------------------------------------
        # compute the CTE

        # start with the present robot estimate
        estimate = filter.get_position()

        ### 计算 CTE
        dx = spath[index+1][0] - spath[index][0]
        dy = spath[index+1][1] - spath[index][1]
        drx = estimate[0] - spath[index][0]
        dry = estimate[1] - spath[index][1]
        
        u = (drx * dx + dry * dy) / (dx * dx + dy * dy) 
        # u is the robot estimate projects onto the path segment
        
        cte = (dry * dx - drx * dy) / (dx * dx + dy * dy)
        # cte is the crosstrack error
        
        if u > 1.0:  # 如果行驶出了当前 segment， 则选择下一段 segment
            index += 1

        # ----------------------------------------


        diff_cte += cte

        steer = - params[0] * cte - params[1] * diff_cte 

        myrobot = myrobot.move(grid, steer, speed)
        filter.move(grid, steer, speed)

        Z = myrobot.sense()
        filter.sense(Z)

        if not myrobot.check_collision(grid):
            print '##### Collision ####'

        err += (cte ** 2)
        N += 1

        if printflag:
            print myrobot, cte, index, u

    return [myrobot.check_goal(goal), myrobot.num_collisions, myrobot.num_steps]
    # return [True, 0, 145]: 找到了 goal, 碰撞的次数， 运行步数 


# ------------------------------------------------
# 
# this is our main routine
#

def main(grid, init, goal, steering_noise, distance_noise, measurement_noise, 
     weight_data, weight_smooth, p_gain, d_gain):

    path = plan(grid, init, goal)
    path.astar()
    path.smooth(weight_data, weight_smooth)
    return run(grid, goal, path.spath, [p_gain, d_gain])

    


# ------------------------------------------------
# 
# input data and parameters
#


# grid format:
#   0 = navigable space
#   1 = occupied space

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 0],
        [0, 1, 0, 1, 0, 0],
        [0, 0, 0, 1, 0, 1],
        [0, 1, 0, 1, 0, 0]]


init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]


steering_noise    = 0.1
distance_noise    = 0.03
measurement_noise = 0.3

weight_data       = 0.1
weight_smooth     = 0.2
p_gain            = 2.0
d_gain            = 6.0

    
print main(grid, init, goal, steering_noise, distance_noise, measurement_noise, 
           weight_data, weight_smooth, p_gain, d_gain)




def twiddle(init_params):
    n_params   = len(init_params)
    dparams    = [1.0 for row in range(n_params)]
    params     = [0.0 for row in range(n_params)]
    K = 10

    for i in range(n_params):
        params[i] = init_params[i]


    best_error = 0.0;
    for k in range(K):
        ret = main(grid, init, goal, 
                   steering_noise, distance_noise, measurement_noise, 
                   params[0], params[1], params[2], params[3])
        if ret[0]:
            best_error += ret[1] * 100 + ret[2]
        else:
            best_error += 99999
    best_error = float(best_error) / float(k+1)
    print best_error

    n = 0
    while sum(dparams) > 0.0000001:
        for i in range(len(params)):
            params[i] += dparams[i]
            err = 0
            for k in range(K):
                ret = main(grid, init, goal, 
                           steering_noise, distance_noise, measurement_noise, 
                           params[0], params[1], params[2], params[3], best_error)
                if ret[0]:
                    err += ret[1] * 100 + ret[2]
                else:
                    err += 99999
            print float(err) / float(k+1)
            if err < best_error:
                best_error = float(err) / float(k+1)
                dparams[i] *= 1.1
            else:
                params[i] -= 2.0 * dparams[i]            
                err = 0
                for k in range(K):
                    ret = main(grid, init, goal, 
                               steering_noise, distance_noise, measurement_noise, 
                               params[0], params[1], params[2], params[3], best_error)
                    if ret[0]:
                        err += ret[1] * 100 + ret[2]
                    else:
                        err += 99999
                print float(err) / float(k+1)
                if err < best_error:
                    best_error = float(err) / float(k+1)
                    dparams[i] *= 1.1
                else:
                    params[i] += dparams[i]
                    dparams[i] *= 0.5
        n += 1
        print 'Twiddle #', n, params, ' -> ', best_error
    print ' '
    return params


#twiddle([weight_data, weight_smooth, p_gain, d_gain])

```

    [True, 0, 132]

# SLAM

### Linear Graph SLAM
#### 基本思想
通过约束矩阵计算可能的机器人路径位置和 landmark 位置
![slam1](pic/slam1.png)

![slam2](pic/slam2.png)

#### 程序实现


```python
# ------------
# User Instructions
# 
# In this problem you will implement SLAM in a 2 dimensional
# world. Please define a function, slam, which takes five
# parameters as input and returns the vector mu. This vector
# should have x, y coordinates interlaced, so for example, 
# if there were 2 poses and 2 landmarks, mu would look like:
#
#  mu =  matrix([[Px0],
#                [Py0],
#                [Px1],
#                [Py1],
#                [Lx0],
#                [Ly0],
#                [Lx1],
#                [Ly1]])
#
# data - This is the data that is generated with the included
#        make_data function. You can also use test_data to
#        make sure your function gives the correct result.
#
# N -    The number of time steps.
#
# num_landmarks - The number of landmarks.
#
# motion_noise - The noise associated with motion. The update
#                strength for motion should be 1.0 / motion_noise.
#
# measurement_noise - The noise associated with measurement.
#                     The update strength for measurement should be
#                     1.0 / measurement_noise.
#
#
# Enter your code at line 509

# --------------
# Testing
#
# Uncomment the test cases at the bottom of this document.
# Your output should be identical to the given results.

 
from math import *
import random


#===============================================================
#
# SLAM in a rectolinear world (we avoid non-linearities)
#      
# 
#===============================================================


# ------------------------------------------------
# 
# this is the matrix class
# we use it because it makes it easier to collect constraints in GraphSLAM
# and to calculate solutions (albeit inefficiently)
# 

class matrix:
    
    # implements basic operations of a matrix class

    # ------------
    #
    # initialization - can be called with an initial matrix
    #

    def __init__(self, value = [[]]):
        self.value = value
        self.dimx  = len(value)
        self.dimy  = len(value[0])
        if value == [[]]:
            self.dimx = 0

    # ------------
    #
    # makes matrix of a certain size and sets each element to zero
    #

    def zero(self, dimx, dimy):
        if dimy == 0:
            dimy = dimx
        # check if valid dimensions
        if dimx < 1 or dimy < 1:
            raise ValueError, "Invalid size of matrix"
        else:
            self.dimx  = dimx
            self.dimy  = dimy
            self.value = [[0.0 for row in range(dimy)] for col in range(dimx)]

    # ------------
    #
    # makes matrix of a certain (square) size and turns matrix into identity matrix
    #

    def identity(self, dim):
        # check if valid dimension
        if dim < 1:
            raise ValueError, "Invalid size of matrix"
        else:
            self.dimx  = dim
            self.dimy  = dim
            self.value = [[0.0 for row in range(dim)] for col in range(dim)]
            for i in range(dim):
                self.value[i][i] = 1.0
    # ------------
    #
    # prints out values of matrix
    #

    def show(self, txt = ''):
        for i in range(len(self.value)):
            print txt + '['+ ', '.join('%.3f'%x for x in self.value[i]) + ']' 
        print ' '

    # ------------
    #
    # defines elmement-wise matrix addition. Both matrices must be of equal dimensions
    #

    def __add__(self, other):
        # check if correct dimensions
        if self.dimx != other.dimx or self.dimy != other.dimy:
            raise ValueError, "Matrices must be of equal dimension to add"
        else:
            # add if correct dimensions
            res = matrix()
            res.zero(self.dimx, self.dimy)
            for i in range(self.dimx):
                for j in range(self.dimy):
                    res.value[i][j] = self.value[i][j] + other.value[i][j]
            return res

    # ------------
    #
    # defines elmement-wise matrix subtraction. Both matrices must be of equal dimensions
    #

    def __sub__(self, other):
        # check if correct dimensions
        if self.dimx != other.dimx or self.dimy != other.dimy:
            raise ValueError, "Matrices must be of equal dimension to subtract"
        else:
            # subtract if correct dimensions
            res = matrix()
            res.zero(self.dimx, self.dimy)
            for i in range(self.dimx):
                for j in range(self.dimy):
                    res.value[i][j] = self.value[i][j] - other.value[i][j]
            return res

    # ------------
    #
    # defines multiplication. Both matrices must be of fitting dimensions
    #

    def __mul__(self, other):
        # check if correct dimensions
        if self.dimy != other.dimx:
            raise ValueError, "Matrices must be m*n and n*p to multiply"
        else:
            # multiply if correct dimensions
            res = matrix()
            res.zero(self.dimx, other.dimy)
            for i in range(self.dimx):
                for j in range(other.dimy):
                    for k in range(self.dimy):
                        res.value[i][j] += self.value[i][k] * other.value[k][j]
        return res


    # ------------
    #
    # returns a matrix transpose
    #

    def transpose(self):
        # compute transpose
        res = matrix()
        res.zero(self.dimy, self.dimx)
        for i in range(self.dimx):
            for j in range(self.dimy):
                res.value[j][i] = self.value[i][j]
        return res

    # ------------
    #
    # creates a new matrix from the existing matrix elements.
    #
    # Example:
    #       l = matrix([[ 1,  2,  3,  4,  5], 
    #                   [ 6,  7,  8,  9, 10], 
    #                   [11, 12, 13, 14, 15]])
    #
    #       l.take([0, 2], [0, 2, 3])
    #
    # results in:
    #       
    #       [[1, 3, 4], 
    #        [11, 13, 14]]
    #       
    # 
    # take is used to remove rows and columns from existing matrices
    # list1/list2 define a sequence of rows/columns that shall be taken
    # if no list2 is provided, then list2 is set to list1 (good for 
    # symmetric matrices)
    #

    def take(self, list1, list2 = []):
        if list2 == []:
            list2 = list1
        if len(list1) > self.dimx or len(list2) > self.dimy:
            raise ValueError, "list invalid in take()"

        res = matrix()
        res.zero(len(list1), len(list2))
        for i in range(len(list1)):
            for j in range(len(list2)):
                res.value[i][j] = self.value[list1[i]][list2[j]]
        return res

    # ------------
    #
    # creates a new matrix from the existing matrix elements.
    #
    # Example:
    #       l = matrix([[1, 2, 3],
    #                  [4, 5, 6]])
    #
    #       l.expand(3, 5, [0, 2], [0, 2, 3])
    #
    # results in:
    #
    #       [[1, 0, 2, 3, 0], 
    #        [0, 0, 0, 0, 0], 
    #        [4, 0, 5, 6, 0]]
    # 
    # expand is used to introduce new rows and columns into an existing matrix
    # list1/list2 are the new indexes of row/columns in which the matrix
    # elements are being mapped. Elements for rows and columns 
    # that are not listed in list1/list2 
    # will be initialized by 0.0.
    #
    
    def expand(self, dimx, dimy, list1, list2 = []):
        if list2 == []:
            list2 = list1
        if len(list1) > self.dimx or len(list2) > self.dimy:
            raise ValueError, "list invalid in expand()"

        res = matrix()
        res.zero(dimx, dimy)
        for i in range(len(list1)):
            for j in range(len(list2)):
                res.value[list1[i]][list2[j]] = self.value[i][j]
        return res

    # ------------
    #
    # Computes the upper triangular Cholesky factorization of  
    # a positive definite matrix.
    # This code is based on http://adorio-research.org/wordpress/?p=4560
    #
    
    def Cholesky(self, ztol= 1.0e-5):

        res = matrix()
        res.zero(self.dimx, self.dimx)

        for i in range(self.dimx):
            S = sum([(res.value[k][i])**2 for k in range(i)])
            d = self.value[i][i] - S
            if abs(d) < ztol:
                res.value[i][i] = 0.0
            else: 
                if d < 0.0:
                    raise ValueError, "Matrix not positive-definite"
                res.value[i][i] = sqrt(d)
            for j in range(i+1, self.dimx):
                S = sum([res.value[k][i] * res.value[k][j] for k in range(i)])
                if abs(S) < ztol:
                    S = 0.0
                try:
                   res.value[i][j] = (self.value[i][j] - S)/res.value[i][i]
                except:
                   raise ValueError, "Zero diagonal"
        return res 
 
    # ------------
    #
    # Computes inverse of matrix given its Cholesky upper Triangular
    # decomposition of matrix.
    # This code is based on http://adorio-research.org/wordpress/?p=4560
    #
    
    def CholeskyInverse(self):

        res = matrix()
        res.zero(self.dimx, self.dimx)

        # Backward step for inverse.
        for j in reversed(range(self.dimx)):
            tjj = self.value[j][j]
            S = sum([self.value[j][k]*res.value[j][k] for k in range(j+1, self.dimx)])
            res.value[j][j] = 1.0/ tjj**2 - S/ tjj
            for i in reversed(range(j)):
                res.value[j][i] = res.value[i][j] = \
                    -sum([self.value[i][k]*res.value[k][j] for k in \
                              range(i+1,self.dimx)])/self.value[i][i]
        return res
    
    # ------------
    #
    # computes and returns the inverse of a square matrix
    #
    def inverse(self):
        aux = self.Cholesky()
        res = aux.CholeskyInverse()
        return res

    # ------------
    #
    # prints matrix (needs work!)
    #
    def __repr__(self):
        return repr(self.value)

# ------------------------------------------------
# 
# this is the robot class
# 
# our robot lives in x-y space, and its motion is
# pointed in a random direction. It moves on a straight line
# until is comes close to a wall at which point it turns
# away from the wall and continues to move.
#
# For measurements, it simply senses the x- and y-distance
# to landmarks. This is different from range and bearing as 
# commonly studied in the literature, but this makes it much
# easier to implement the essentials of SLAM without
# cluttered math
#

class robot:

    # --------
    # init: 
    #   creates robot and initializes location to 0, 0
    #

    def __init__(self, world_size = 100.0, measurement_range = 30.0,
                 motion_noise = 1.0, measurement_noise = 1.0):
        self.measurement_noise = 0.0
        self.world_size = world_size
        self.measurement_range = measurement_range
        self.x = world_size / 2.0
        self.y = world_size / 2.0
        self.motion_noise = motion_noise
        self.measurement_noise = measurement_noise
        self.landmarks = []
        self.num_landmarks = 0


    def rand(self):
        return random.random() * 2.0 - 1.0

    # --------
    #
    # make random landmarks located in the world
    #

    def make_landmarks(self, num_landmarks):
        self.landmarks = []
        for i in range(num_landmarks):
            self.landmarks.append([round(random.random() * self.world_size),
                                   round(random.random() * self.world_size)])
        self.num_landmarks = num_landmarks


    # --------
    #
    # move: attempts to move robot by dx, dy. If outside world
    #       boundary, then the move does nothing and instead returns failure
    #

    def move(self, dx, dy):

        x = self.x + dx + self.rand() * self.motion_noise
        y = self.y + dy + self.rand() * self.motion_noise

        if x < 0.0 or x > self.world_size or y < 0.0 or y > self.world_size:
            return False
        else:
            self.x = x
            self.y = y
            return True
    

    # --------
    #
    # sense: returns x- and y- distances to landmarks within visibility range
    #        because not all landmarks may be in this range, the list of measurements
    #        is of variable length. Set measurement_range to -1 if you want all
    #        landmarks to be visible at all times
    #

    def sense(self):
        Z = []
        for i in range(self.num_landmarks):
            dx = self.landmarks[i][0] - self.x + self.rand() * self.measurement_noise
            dy = self.landmarks[i][1] - self.y + self.rand() * self.measurement_noise    
            if self.measurement_range < 0.0 or abs(dx) + abs(dy) <= self.measurement_range:
                Z.append([i, dx, dy])
        return Z

    # --------
    #
    # print robot location
    #

    def __repr__(self):
        return 'Robot: [x=%.5f y=%.5f]'  % (self.x, self.y)

######################################################

# --------
# this routine makes the robot data
#

def make_data(N, num_landmarks, world_size, measurement_range, motion_noise, 
              measurement_noise, distance):


    complete = False

    while not complete:

        data = []

        # make robot and landmarks
        r = robot(world_size, measurement_range, motion_noise, measurement_noise)
        r.make_landmarks(num_landmarks)
        seen = [False for row in range(num_landmarks)]
    
        # guess an initial motion
        orientation = random.random() * 2.0 * pi
        dx = cos(orientation) * distance
        dy = sin(orientation) * distance
    
        for k in range(N-1):
    
            # sense
            Z = r.sense()

            # check off all landmarks that were observed 
            for i in range(len(Z)):
                seen[Z[i][0]] = True
    
            # move
            while not r.move(dx, dy):
                # if we'd be leaving the robot world, pick instead a new direction
                orientation = random.random() * 2.0 * pi
                dx = cos(orientation) * distance
                dy = sin(orientation) * distance

            # memorize data
            data.append([Z, [dx, dy]])

        # we are done when all landmarks were observed; otherwise re-run
        complete = (sum(seen) == num_landmarks)

    print ' '
    print 'Landmarks: ', r.landmarks
    print r


    return data
    
####################################################

# --------------------------------
#
# print the result of SLAM, the robot pose(s) and the landmarks
#

def print_result(N, num_landmarks, result):
    print
    print 'Estimated Pose(s):'
    for i in range(N):
        print '    ['+ ', '.join('%.3f'%x for x in result.value[2*i]) + ', ' \
            + ', '.join('%.3f'%x for x in result.value[2*i+1]) +']'
    print
    print 'Estimated Landmarks:'
    for i in range(num_landmarks):
        print '    ['+ ', '.join('%.3f'%x for x in result.value[2*(N+i)]) + ', ' \
            + ', '.join('%.3f'%x for x in result.value[2*(N+i)+1]) +']'

# --------------------------------
#
# slam - retains entire path and all landmarks
#

############## ENTER YOUR CODE BELOW HERE ###################

def slam(data, N, num_landmarks, motion_noise, measurement_noise):
    #
    #
    # Add your code here!
    #
    #
    dim = 2 * (N + num_landmarks)
    
    # make the constraint information matrix and vector
    Omega = matrix()
    Omega.zero(dim, dim)
    Omega.value[0][0] = 1.0
    Omega.value[1][1] = 1.0
    
    Xi = matrix()
    Xi.zero(dim, 1)
    Xi.value[0][0] = world_size / 2.0
    Xi.value[1][0] = world_size / 2.0
    
    for k in range(len(data)):
        n = k * 2
        measurement = data[k][0]
        motion = data[k][1]
        
        for i in range(len(measurement)):
            m = 2 * (N + measurement[i][0])
            
            # update constraint matrix and vector based on the measurement
            for b in range(2):
                Omega.value[n+b][n+b] += 1.0 / measurement_noise
                Omega.value[m+b][m+b] += 1.0 / measurement_noise
                Omega.value[n+b][m+b] += -1.0 / measurement_noise
                Omega.value[m+b][n+b] += -1.0 / measurement_noise
                Xi.value[n+b][0] += -measurement[i][1+b] / measurement_noise
                Xi.value[m+b][0] +=  measurement[i][1+b] / measurement_noise
            
        
        # update constraint matrix and vector based on the robot motion
        for b in range(4):
            Omega.value[n+b][n+b] += 1.0 / motion_noise
        for b in range(2):
            Omega.value[n+b][n+b+2] += -1.0 / motion_noise
            Omega.value[n+b+2][n+b] += -1.0 / motion_noise
            Xi.value[n+b][0] +=  -motion[b] / motion_noise
            Xi.value[n+b+2][0] +=  motion[b] / motion_noise
    mu = Omega.inverse() * Xi
    
    return mu # Make sure you return mu for grading!
        
############### ENTER YOUR CODE ABOVE HERE ###################

# ------------------------------------------------------------------------
# ------------------------------------------------------------------------
# ------------------------------------------------------------------------
#
# Main routines
#


num_landmarks      = 5        # number of landmarks
N                  = 20       # time steps
world_size         = 100.0    # size of world
measurement_range  = 50.0     # range at which we can sense landmarks
motion_noise       = 2.0      # noise in robot motion
measurement_noise  = 2.0      # noise in the measurements
distance           = 20.0     # distance by which robot (intends to) move each iteratation 

data = make_data(N, num_landmarks, world_size, measurement_range, motion_noise, measurement_noise, distance)
result = slam(data, N, num_landmarks, motion_noise, measurement_noise)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
print_result(N, num_landmarks, result)

# -------------
# Testing
#
# Uncomment one of the test cases below to compare your results to
# the results shown for Test Case 1 and Test Case 2.

test_data1 = [[[[1, 19.457599255548065, 23.8387362100849], [2, -13.195807561967236, 11.708840328458608], [3, -30.0954905279171, 15.387879242505843]], [-12.2607279422326, -15.801093326936487]], [[[2, -0.4659930049620491, 28.088559771215664], [4, -17.866382374890936, -16.384904503932]], [-12.2607279422326, -15.801093326936487]], [[[4, -6.202512900833806, -1.823403210274639]], [-12.2607279422326, -15.801093326936487]], [[[4, 7.412136480918645, 15.388585962142429]], [14.008259661173426, 14.274756084260822]], [[[4, -7.526138813444998, -0.4563942429717849]], [14.008259661173426, 14.274756084260822]], [[[2, -6.299793150150058, 29.047830407717623], [4, -21.93551130411791, -13.21956810989039]], [14.008259661173426, 14.274756084260822]], [[[1, 15.796300959032276, 30.65769689694247], [2, -18.64370821983482, 17.380022987031367]], [14.008259661173426, 14.274756084260822]], [[[1, 0.40311325410337906, 14.169429532679855], [2, -35.069349468466235, 2.4945558982439957]], [14.008259661173426, 14.274756084260822]], [[[1, -16.71340983241936, -2.777000269543834]], [-11.006096015782283, 16.699276945166858]], [[[1, -3.611096830835776, -17.954019226763958]], [-19.693482634035977, 3.488085684573048]], [[[1, 18.398273354362416, -22.705102332550947]], [-19.693482634035977, 3.488085684573048]], [[[2, 2.789312482883833, -39.73720193121324]], [12.849049222879723, -15.326510824972983]], [[[1, 21.26897046581808, -10.121029799040915], [2, -11.917698965880655, -23.17711662602097], [3, -31.81167947898398, -16.7985673023331]], [12.849049222879723, -15.326510824972983]], [[[1, 10.48157743234859, 5.692957082575485], [2, -22.31488473554935, -5.389184118551409], [3, -40.81803984305378, -2.4703329790238118]], [12.849049222879723, -15.326510824972983]], [[[0, 10.591050242096598, -39.2051798967113], [1, -3.5675572049297553, 22.849456408289125], [2, -38.39251065320351, 7.288990306029511]], [12.849049222879723, -15.326510824972983]], [[[0, -3.6225556479370766, -25.58006865235512]], [-7.8874682868419965, -18.379005523261092]], [[[0, 1.9784503557879374, -6.5025974151499]], [-7.8874682868419965, -18.379005523261092]], [[[0, 10.050665232782423, 11.026385307998742]], [-17.82919359778298, 9.062000642947142]], [[[0, 26.526838150174818, -0.22563393232425621], [4, -33.70303936886652, 2.880339841013677]], [-17.82919359778298, 9.062000642947142]]]
test_data2 = [[[[0, 26.543274387283322, -6.262538160312672], [3, 9.937396825799755, -9.128540360867689]], [18.92765331253674, -6.460955043986683]], [[[0, 7.706544739722961, -3.758467215445748], [1, 17.03954411948937, 31.705489938553438], [3, -11.61731288777497, -6.64964096716416]], [18.92765331253674, -6.460955043986683]], [[[0, -12.35130507136378, 2.585119104239249], [1, -2.563534536165313, 38.22159657838369], [3, -26.961236804740935, -0.4802312626141525]], [-11.167066095509824, 16.592065417497455]], [[[0, 1.4138633151721272, -13.912454837810632], [1, 8.087721200818589, 20.51845934354381], [3, -17.091723454402302, -16.521500551709707], [4, -7.414211721400232, 38.09191602674439]], [-11.167066095509824, 16.592065417497455]], [[[0, 12.886743222179561, -28.703968411636318], [1, 21.660953298391387, 3.4912891084614914], [3, -6.401401414569506, -32.321583037341625], [4, 5.034079343639034, 23.102207946092893]], [-11.167066095509824, 16.592065417497455]], [[[1, 31.126317672358578, -10.036784369535214], [2, -38.70878528420893, 7.4987265861424595], [4, 17.977218575473767, 6.150889254289742]], [-6.595520680493778, -18.88118393939265]], [[[1, 41.82460922922086, 7.847527392202475], [3, 15.711709540417502, -30.34633659912818]], [-6.595520680493778, -18.88118393939265]], [[[0, 40.18454208294434, -6.710999804403755], [3, 23.019508919299156, -10.12110867290604]], [-6.595520680493778, -18.88118393939265]], [[[3, 27.18579315312821, 8.067219022708391]], [-6.595520680493778, -18.88118393939265]], [[], [11.492663265706092, 16.36822198838621]], [[[3, 24.57154567653098, 13.461499960708197]], [11.492663265706092, 16.36822198838621]], [[[0, 31.61945290413707, 0.4272295085799329], [3, 16.97392299158991, -5.274596836133088]], [11.492663265706092, 16.36822198838621]], [[[0, 22.407381798735177, -18.03500068379259], [1, 29.642444125196995, 17.3794951934614], [3, 4.7969752441371645, -21.07505361639969], [4, 14.726069092569372, 32.75999422300078]], [11.492663265706092, 16.36822198838621]], [[[0, 10.705527984670137, -34.589764174299596], [1, 18.58772336795603, -0.20109708164787765], [3, -4.839806195049413, -39.92208742305105], [4, 4.18824810165454, 14.146847823548889]], [11.492663265706092, 16.36822198838621]], [[[1, 5.878492140223764, -19.955352450942357], [4, -7.059505455306587, -0.9740849280550585]], [19.628527845173146, 3.83678180657467]], [[[1, -11.150789592446378, -22.736641053247872], [4, -28.832815721158255, -3.9462962046291388]], [-19.841703647091965, 2.5113335861604362]], [[[1, 8.64427397916182, -20.286336970889053], [4, -5.036917727942285, -6.311739993868336]], [-5.946642674882207, -19.09548221169787]], [[[0, 7.151866679283043, -39.56103232616369], [1, 16.01535401373368, -3.780995345194027], [4, -3.04801331832137, 13.697362774960865]], [-5.946642674882207, -19.09548221169787]], [[[0, 12.872879480504395, -19.707592098123207], [1, 22.236710716903136, 16.331770792606406], [3, -4.841206109583004, -21.24604435851242], [4, 4.27111163223552, 32.25309748614184]], [-5.946642674882207, -19.09548221169787]]] 

##  Test Case 1
##
##  Estimated Pose(s):
##      [49.999, 49.999]
##      [37.971, 33.650]
##      [26.183, 18.153]
##      [13.743, 2.114]
##      [28.095, 16.781]
##      [42.383, 30.900]
##      [55.829, 44.494]
##      [70.855, 59.697]
##      [85.695, 75.540]
##      [74.010, 92.431]
##      [53.543, 96.451]
##      [34.523, 100.078]
##      [48.621, 83.951]
##      [60.195, 68.105]
##      [73.776, 52.932]
##      [87.130, 38.536]
##      [80.301, 20.506]
##      [72.797, 2.943]
##      [55.244, 13.253]
##      [37.414, 22.315]
##  
##  Estimated Landmarks:
##      [82.954, 13.537]
##      [70.493, 74.139]
##      [36.738, 61.279]
##      [18.696, 66.057]
##      [20.633, 16.873]


##  Test Case 2
##
##  Estimated Pose(s):
##      [49.999, 49.999]
##      [69.180, 45.664]
##      [87.742, 39.702]
##      [76.269, 56.309]
##      [64.316, 72.174]
##      [52.256, 88.151]
##      [44.058, 69.399]
##      [37.001, 49.916]
##      [30.923, 30.953]
##      [23.507, 11.417]
##      [34.179, 27.131]
##      [44.154, 43.844]
##      [54.805, 60.919]
##      [65.697, 78.544]
##      [77.467, 95.624]
##      [96.801, 98.819]
##      [75.956, 99.969]
##      [70.199, 81.179]
##      [64.053, 61.721]
##      [58.106, 42.626]
##  
##  Estimated Landmarks:
##      [76.778, 42.885]
##      [85.064, 77.436]
##      [13.546, 95.649]
##      [59.448, 39.593]
##      [69.262, 94.238]



### Uncomment the following three lines for test case 1 ###

#result = slam(test_data1, 20, 5, 2.0, 2.0)
#print_result(20, 5, result)
#print result


### Uncomment the following three lines for test case 2 ###

#result = slam(test_data2, 20, 5, 2.0, 2.0)
#print_result(20, 5, result)
#print result



```


    Landmarks:  [[69.0, 49.0], [42.0, 97.0], [79.0, 66.0], [14.0, 85.0], [96.0, 44.0]]
    Robot: [x=64.95099 y=56.34479]
    
    Estimated Pose(s):
        [49.999, 49.999]
        [62.322, 33.671]
        [73.900, 16.347]
        [53.685, 22.646]
        [34.297, 29.205]
        [14.910, 35.763]
        [1.094, 20.517]
        [15.540, 33.463]
        [29.986, 46.409]
        [44.580, 58.366]
        [60.751, 72.499]
        [75.099, 86.661]
        [54.227, 87.256]
        [32.544, 89.374]
        [12.233, 90.190]
        [30.037, 83.213]
        [47.752, 76.258]
        [64.771, 69.128]
        [83.961, 61.492]
        [64.190, 58.476]
    
    Estimated Landmarks:
        [68.353, 49.555]
        [41.357, 97.755]
        [77.205, 66.714]
        [13.489, 85.759]
        [95.518, 46.256]


### Online SLAM
#### 基本思想
随着机器人行走路径的增长，constraint matrix 关于路径定位那部分会持续增加，导致计算越来越慢。为了解决这个问题，提出了 online SLAM.
![slam3](pic/slam3.png)


#### 程序实现


```python
# ------------
# User Instructions
#
# In this problem you will implement a more manageable
# version of graph SLAM in 2 dimensions. 
#
# Define a function, online_slam, that takes 5 inputs:
# data, N, num_landmarks, motion_noise, and
# measurement_noise--just as was done in the last 
# programming assignment of unit 6. This function
# must return TWO matrices, mu and the final Omega.
#
# Just as with the quiz, your matrices should have x
# and y interlaced, so if there were two poses and 2
# landmarks, mu would look like:
#
# mu = matrix([[Px0],
#              [Py0],
#              [Px1],
#              [Py1],
#              [Lx0],
#              [Ly0],
#              [Lx1],
#              [Ly1]])
#
# Enter your code at line 566.

# -----------
# Testing
#
# You have two methods for testing your code.
#
# 1) You can make your own data with the make_data
#    function. Then you can run it through the
#    provided slam routine and check to see that your
#    online_slam function gives the same estimated
#    final robot pose and landmark positions.
# 2) You can use the solution_check function at the
#    bottom of this document to check your code
#    for the two provided test cases. The grading
#    will be almost identical to this function, so
#    if you pass both test cases, you should be
#    marked correct on the homework.

from math import *
import random


# ------------------------------------------------
# 
# this is the matrix class
# we use it because it makes it easier to collect constraints in GraphSLAM
# and to calculate solutions (albeit inefficiently)
# 

class matrix:
    
    # implements basic operations of a matrix class

    # ------------
    #
    # initialization - can be called with an initial matrix
    #

    def __init__(self, value = [[]]):
        self.value = value
        self.dimx  = len(value)
        self.dimy  = len(value[0])
        if value == [[]]:
            self.dimx = 0
            
    # -----------
    #
    # defines matrix equality - returns true if corresponding elements
    #   in two matrices are within epsilon of each other.
    #
    
    def __eq__(self, other):
        epsilon = 0.01
        if self.dimx != other.dimx or self.dimy != other.dimy:
            return False
        for i in range(self.dimx):
            for j in range(self.dimy):
                if abs(self.value[i][j] - other.value[i][j]) > epsilon:
                    return False
        return True
    
    def __ne__(self, other):
        return not (self == other)

    # ------------
    #
    # makes matrix of a certain size and sets each element to zero
    #

    def zero(self, dimx, dimy):
        if dimy == 0:
            dimy = dimx
        # check if valid dimensions
        if dimx < 1 or dimy < 1:
            raise ValueError, "Invalid size of matrix"
        else:
            self.dimx  = dimx
            self.dimy  = dimy
            self.value = [[0.0 for row in range(dimy)] for col in range(dimx)]

    # ------------
    #
    # makes matrix of a certain (square) size and turns matrix into identity matrix
    #

    def identity(self, dim):
        # check if valid dimension
        if dim < 1:
            raise ValueError, "Invalid size of matrix"
        else:
            self.dimx  = dim
            self.dimy  = dim
            self.value = [[0.0 for row in range(dim)] for col in range(dim)]
            for i in range(dim):
                self.value[i][i] = 1.0
                
    # ------------
    #
    # prints out values of matrix
    #

    def show(self, txt = ''):
        for i in range(len(self.value)):
            print txt + '['+ ', '.join('%.3f'%x for x in self.value[i]) + ']' 
        print ' '

    # ------------
    #
    # defines elmement-wise matrix addition. Both matrices must be of equal dimensions
    #

    def __add__(self, other):
        # check if correct dimensions
        if self.dimx != other.dimx or self.dimy != other.dimy:
            raise ValueError, "Matrices must be of equal dimension to add"
        else:
            # add if correct dimensions
            res = matrix()
            res.zero(self.dimx, self.dimy)
            for i in range(self.dimx):
                for j in range(self.dimy):
                    res.value[i][j] = self.value[i][j] + other.value[i][j]
            return res

    # ------------
    #
    # defines elmement-wise matrix subtraction. Both matrices must be of equal dimensions
    #

    def __sub__(self, other):
        # check if correct dimensions
        if self.dimx != other.dimx or self.dimy != other.dimy:
            raise ValueError, "Matrices must be of equal dimension to subtract"
        else:
            # subtract if correct dimensions
            res = matrix()
            res.zero(self.dimx, self.dimy)
            for i in range(self.dimx):
                for j in range(self.dimy):
                    res.value[i][j] = self.value[i][j] - other.value[i][j]
            return res

    # ------------
    #
    # defines multiplication. Both matrices must be of fitting dimensions
    #

    def __mul__(self, other):
        # check if correct dimensions
        if self.dimy != other.dimx:
            raise ValueError, "Matrices must be m*n and n*p to multiply"
        else:
            # multiply if correct dimensions
            res = matrix()
            res.zero(self.dimx, other.dimy)
            for i in range(self.dimx):
                for j in range(other.dimy):
                    for k in range(self.dimy):
                        res.value[i][j] += self.value[i][k] * other.value[k][j]
        return res

    # ------------
    #
    # returns a matrix transpose
    #

    def transpose(self):
        # compute transpose
        res = matrix()
        res.zero(self.dimy, self.dimx)
        for i in range(self.dimx):
            for j in range(self.dimy):
                res.value[j][i] = self.value[i][j]
        return res

    # ------------
    #
    # creates a new matrix from the existing matrix elements.
    #
    # Example:
    #       l = matrix([[ 1,  2,  3,  4,  5], 
    #                   [ 6,  7,  8,  9, 10], 
    #                   [11, 12, 13, 14, 15]])
    #
    #       l.take([0, 2], [0, 2, 3])
    #
    # results in:
    #       
    #       [[1, 3, 4], 
    #        [11, 13, 14]]
    #       
    # 
    # take is used to remove rows and columns from existing matrices
    # list1/list2 define a sequence of rows/columns that shall be taken
    # is no list2 is provided, then list2 is set to list1 (good for symmetric matrices)
    #

    def take(self, list1, list2 = []):
        if list2 == []:
            list2 = list1
        if len(list1) > self.dimx or len(list2) > self.dimy:
            raise ValueError, "list invalid in take()"

        res = matrix()
        res.zero(len(list1), len(list2))
        for i in range(len(list1)):
            for j in range(len(list2)):
                res.value[i][j] = self.value[list1[i]][list2[j]]
        return res

    # ------------
    #
    # creates a new matrix from the existing matrix elements.
    #
    # Example:
    #       l = matrix([[1, 2, 3],
    #                  [4, 5, 6]])
    #
    #       l.expand(3, 5, [0, 2], [0, 2, 3])
    #
    # results in:
    #
    #       [[1, 0, 2, 3, 0], 
    #        [0, 0, 0, 0, 0], 
    #        [4, 0, 5, 6, 0]]
    # 
    # expand is used to introduce new rows and columns into an existing matrix
    # list1/list2 are the new indexes of row/columns in which the matrix
    # elements are being mapped. Elements for rows and columns 
    # that are not listed in list1/list2 
    # will be initialized by 0.0.
    #

    def expand(self, dimx, dimy, list1, list2 = []):
        if list2 == []:
            list2 = list1
        if len(list1) > self.dimx or len(list2) > self.dimy:
            raise ValueError, "list invalid in expand()"

        res = matrix()
        res.zero(dimx, dimy)
        for i in range(len(list1)):
            for j in range(len(list2)):
                res.value[list1[i]][list2[j]] = self.value[i][j]
        return res

    # ------------
    #
    # Computes the upper triangular Cholesky factorization of  
    # a positive definite matrix.
    # This code is based on http://adorio-research.org/wordpress/?p=4560

    def Cholesky(self, ztol= 1.0e-5):

        res = matrix()
        res.zero(self.dimx, self.dimx)

        for i in range(self.dimx):
            S = sum([(res.value[k][i])**2 for k in range(i)])
            d = self.value[i][i] - S
            if abs(d) < ztol:
                res.value[i][i] = 0.0
            else: 
                if d < 0.0:
                    raise ValueError, "Matrix not positive-definite"
                res.value[i][i] = sqrt(d)
            for j in range(i+1, self.dimx):
                S = sum([res.value[k][i] * res.value[k][j] for k in range(i)])
                if abs(S) < ztol:
                    S = 0.0
                try:
                   res.value[i][j] = (self.value[i][j] - S)/res.value[i][i]
                except:
                   raise ValueError, "Zero diagonal"
        return res 
 
    # ------------
    #
    # Computes inverse of matrix given its Cholesky upper Triangular
    # decomposition of matrix.
    # This code is based on http://adorio-research.org/wordpress/?p=4560

    def CholeskyInverse(self):
    # Computes inverse of matrix given its Cholesky upper Triangular
    # decomposition of matrix.
        # This code is based on http://adorio-research.org/wordpress/?p=4560

        res = matrix()
        res.zero(self.dimx, self.dimx)

    # Backward step for inverse.
        for j in reversed(range(self.dimx)):
            tjj = self.value[j][j]
            S = sum([self.value[j][k]*res.value[j][k] for k in range(j+1, self.dimx)])
            res.value[j][j] = 1.0/ tjj**2 - S/ tjj
            for i in reversed(range(j)):
                res.value[j][i] = res.value[i][j] = \
                    -sum([self.value[i][k]*res.value[k][j] for k in \
                              range(i+1,self.dimx)])/self.value[i][i]
        return res
    
    # ------------
    #
    # comutes and returns the inverse of a square matrix
    #

    def inverse(self):
        aux = self.Cholesky()
        res = aux.CholeskyInverse()
        return res

    # ------------
    #
    # prints matrix (needs work!)
    #

    def __repr__(self):
        return repr(self.value)

# ######################################################################

# ------------------------------------------------
# 
# this is the robot class
# 
# our robot lives in x-y space, and its motion is
# pointed in a random direction. It moves on a straight line
# until is comes close to a wall at which point it turns
# away from the wall and continues to move.
#
# For measurements, it simply senses the x- and y-distance
# to landmarks. This is different from range and bearing as 
# commonly studies in the literature, but this makes it much
# easier to implement the essentials of SLAM without
# cluttered math
#

class robot:

    # --------
    # init: 
    #   creates robot and initializes location to 0, 0
    #

    def __init__(self, world_size = 100.0, measurement_range = 30.0,
                 motion_noise = 1.0, measurement_noise = 1.0):
        self.measurement_noise = 0.0
        self.world_size = world_size
        self.measurement_range = measurement_range
        self.x = world_size / 2.0
        self.y = world_size / 2.0
        self.motion_noise = motion_noise
        self.measurement_noise = measurement_noise
        self.landmarks = []
        self.num_landmarks = 0


    def rand(self):
        return random.random() * 2.0 - 1.0

    # --------
    #
    # make random landmarks located in the world
    #

    def make_landmarks(self, num_landmarks):
        self.landmarks = []
        for i in range(num_landmarks):
            self.landmarks.append([round(random.random() * self.world_size),
                                   round(random.random() * self.world_size)])
        self.num_landmarks = num_landmarks

    # --------
    #
    # move: attempts to move robot by dx, dy. If outside world
    #       boundary, then the move does nothing and instead returns failure
    #

    def move(self, dx, dy):

        x = self.x + dx + self.rand() * self.motion_noise
        y = self.y + dy + self.rand() * self.motion_noise

        if x < 0.0 or x > self.world_size or y < 0.0 or y > self.world_size:
            return False
        else:
            self.x = x
            self.y = y
            return True
    
    # --------
    #
    # sense: returns x- and y- distances to landmarks within visibility range
    #        because not all landmarks may be in this range, the list of measurements
    #        is of variable length. Set measurement_range to -1 if you want all
    #        landmarks to be visible at all times
    #

    def sense(self):
        Z = []
        for i in range(self.num_landmarks):
            dx = self.landmarks[i][0] - self.x + self.rand() * self.measurement_noise
            dy = self.landmarks[i][1] - self.y + self.rand() * self.measurement_noise    
            if self.measurement_range < 0.0 or abs(dx) + abs(dy) <= self.measurement_range:
                Z.append([i, dx, dy])
        return Z

    # --------
    #
    # print robot location
    #

    def __repr__(self):
        return 'Robot: [x=%.5f y=%.5f]'  % (self.x, self.y)


# ######################################################################

# --------
# this routine makes the robot data
#

def make_data(N, num_landmarks, world_size, measurement_range, motion_noise, 
              measurement_noise, distance):

    complete = False

    while not complete:

        data = []

        # make robot and landmarks
        r = robot(world_size, measurement_range, motion_noise, measurement_noise)
        r.make_landmarks(num_landmarks)
        seen = [False for row in range(num_landmarks)]
    
        # guess an initial motion
        orientation = random.random() * 2.0 * pi
        dx = cos(orientation) * distance
        dy = sin(orientation) * distance
    
        for k in range(N-1):
    
            # sense
            Z = r.sense()

            # check off all landmarks that were observed 
            for i in range(len(Z)):
                seen[Z[i][0]] = True
    
            # move
            while not r.move(dx, dy):
                # if we'd be leaving the robot world, pick instead a new direction
                orientation = random.random() * 2.0 * pi
                dx = cos(orientation) * distance
                dy = sin(orientation) * distance

            # memorize data
            data.append([Z, [dx, dy]])

        # we are done when all landmarks were observed; otherwise re-run
        complete = (sum(seen) == num_landmarks)

    print ' '
    print 'Landmarks: ', r.landmarks
    print r

    return data
    
# ######################################################################

# --------------------------------
#
# full_slam - retains entire path and all landmarks
#             Feel free to use this for comparison.
#

def slam(data, N, num_landmarks, motion_noise, measurement_noise):

    # Set the dimension of the filter
    dim = 2 * (N + num_landmarks) 

    # make the constraint information matrix and vector
    Omega = matrix()
    Omega.zero(dim, dim)
    Omega.value[0][0] = 1.0
    Omega.value[1][1] = 1.0

    Xi = matrix()
    Xi.zero(dim, 1)
    Xi.value[0][0] = world_size / 2.0
    Xi.value[1][0] = world_size / 2.0
    
    # process the data

    for k in range(len(data)):

        # n is the index of the robot pose in the matrix/vector
        n = k * 2 
    
        measurement = data[k][0]
        motion      = data[k][1]
    
        # integrate the measurements
        for i in range(len(measurement)):
    
            # m is the index of the landmark coordinate in the matrix/vector
            m = 2 * (N + measurement[i][0])
    
            # update the information maxtrix/vector based on the measurement
            for b in range(2):
                Omega.value[n+b][n+b] +=  1.0 / measurement_noise
                Omega.value[m+b][m+b] +=  1.0 / measurement_noise
                Omega.value[n+b][m+b] += -1.0 / measurement_noise
                Omega.value[m+b][n+b] += -1.0 / measurement_noise
                Xi.value[n+b][0]      += -measurement[i][1+b] / measurement_noise
                Xi.value[m+b][0]      +=  measurement[i][1+b] / measurement_noise


        # update the information maxtrix/vector based on the robot motion
        for b in range(4):
            Omega.value[n+b][n+b] +=  1.0 / motion_noise
        for b in range(2):
            Omega.value[n+b  ][n+b+2] += -1.0 / motion_noise
            Omega.value[n+b+2][n+b  ] += -1.0 / motion_noise
            Xi.value[n+b  ][0]        += -motion[b] / motion_noise
            Xi.value[n+b+2][0]        +=  motion[b] / motion_noise

    # compute best estimate
    mu = Omega.inverse() * Xi

    # return the result
    return mu

# --------------------------------
#
# online_slam - retains all landmarks but only most recent robot pose
#

def online_slam(data, N, num_landmarks, motion_noise, measurement_noise):
    #
    #
    # Enter your code here!
    #
    #
    # Set the dimension of the filter
    dim = 2 * (1 + num_landmarks) 

    # make the constraint information matrix and vector
    Omega = matrix()
    Omega.zero(dim, dim)
    Omega.value[0][0] = 1.0
    Omega.value[1][1] = 1.0

    Xi = matrix()
    Xi.zero(dim, 1)
    Xi.value[0][0] = world_size / 2.0
    Xi.value[1][0] = world_size / 2.0
    
    # process the data

    for k in range(len(data)):

        measurement = data[k][0]
        motion      = data[k][1]
    
        # integrate the measurements
        for i in range(len(measurement)):
    
            # m is the index of the landmark coordinate in the matrix/vector
            m = 2 * (1 + measurement[i][0])
    
            # update the information maxtrix/vector based on the measurement
            for b in range(2):
                Omega.value[b][b] +=  1.0 / measurement_noise
                Omega.value[m+b][m+b] +=  1.0 / measurement_noise
                Omega.value[b][m+b] += -1.0 / measurement_noise
                Omega.value[m+b][b] += -1.0 / measurement_noise
                Xi.value[b][0]      += -measurement[i][1+b] / measurement_noise
                Xi.value[m+b][0]      +=  measurement[i][1+b] / measurement_noise

        # expand the matrix and vector by one new position
        list = [0,1] + range(4, dim+2)
        Omega = Omega.expand(dim+2, dim+2, list, list)
        Xi = Xi.expand(dim+2, 1, list, [0])
        
        # update based on robot motion
        for b in range(4):
            Omega.value[b][b] += 1.0 / motion_noise
        for b in range(2):
            Omega.value[b][b+2] += -1.0 / motion_noise
            Omega.value[b+2][b] += -1.0 / motion_noise
            Xi.value[b][0] += -motion[b] / motion_noise
            Xi.value[b+2][0] += motion[b] / motion_noise
            
        newlist = range(2, len(Omega.value))
        a = Omega.take([0,1], newlist)
        b = Omega.take([0,1])
        c = Xi.take([0,1], [0])
        Omega = Omega.take(newlist) - a.transpose() * b.inverse() * a
        Xi = Xi.take(newlist,[0]) - a.transpose() * b.inverse() * c
        
    mu = Omega.inverse() * Xi
    
    return mu, Omega # make sure you return both of these matrices to be marked correct.

# --------------------------------
#
# print the result of SLAM, the robot pose(s) and the landmarks
#

def print_result(N, num_landmarks, result):
    print
    print 'Estimated Pose(s):'
    for i in range(N):
        print '    ['+ ', '.join('%.3f'%x for x in result.value[2*i]) + ', ' \
            + ', '.join('%.3f'%x for x in result.value[2*i+1]) +']'
    print 
    print 'Estimated Landmarks:'
    for i in range(num_landmarks):
        print '    ['+ ', '.join('%.3f'%x for x in result.value[2*(N+i)]) + ', ' \
            + ', '.join('%.3f'%x for x in result.value[2*(N+i)+1]) +']'
        
# ------------------------------------------------------------------------
#
# Main routines
#

num_landmarks      = 5        # number of landmarks
N                  = 20       # time steps
world_size         = 100.0    # size of world
measurement_range  = 50.0     # range at which we can sense landmarks
motion_noise       = 2.0      # noise in robot motion
measurement_noise  = 2.0      # noise in the measurements
distance           = 20.0     # distance by which robot (intends to) move each iteratation 


# Uncomment the following three lines to run the full slam routine.

data = make_data(N, num_landmarks, world_size, measurement_range, motion_noise, measurement_noise, distance)
result = slam(data, N, num_landmarks, motion_noise, measurement_noise)
print_result(N, num_landmarks, result)

# Uncomment the following three lines to run the online_slam routine.

#data = make_data(N, num_landmarks, world_size, measurement_range, motion_noise, measurement_noise, distance)
result = online_slam(data, N, num_landmarks, motion_noise, measurement_noise)
print_result(1, num_landmarks, result[0])

##########################################################

# ------------
# TESTING
#
# Uncomment one of the test cases below to check that your
# online_slam function works as expected.

def solution_check(result, answer_mu, answer_omega):

    if len(result) != 2:
        print "Your function must return TWO matrices, mu and Omega"
        return False
    
    user_mu = result[0]
    user_omega = result[1]
    
    if user_mu.dimx == answer_omega.dimx and user_mu.dimy == answer_omega.dimy:
        print "It looks like you returned your results in the wrong order. Make sure to return mu then Omega."
        return False
    
    if user_mu.dimx != answer_mu.dimx or user_mu.dimy != answer_mu.dimy:
        print "Your mu matrix doesn't have the correct dimensions. Mu should be a", answer_mu.dimx, " x ", answer_mu.dimy, "matrix."
        return False
    else:
        print "Mu has correct dimensions."
        
    if user_omega.dimx != answer_omega.dimx or user_omega.dimy != answer_omega.dimy:
        print "Your Omega matrix doesn't have the correct dimensions. Omega should be a", answer_omega.dimx, " x ", answer_omega.dimy, "matrix."
        return False
    else:
        print "Omega has correct dimensions."
        
    if user_mu != answer_mu:
        print "Mu has incorrect entries."
        return False
    else:
        print "Mu correct."
        
    if user_omega != answer_omega:
        print "Omega has incorrect entries."
        return False
    else:
        print "Omega correct."
        
    print "Test case passed!"
    return True

# -----------
# Test Case 1

testdata1          = [[[[1, 21.796713239511305, 25.32184135169971], [2, 15.067410969755826, -27.599928007267906]], [16.4522379034509, -11.372065246394495]],
                      [[[1, 6.1286996178786755, 35.70844618389858], [2, -0.7470113490937167, -17.709326161950294]], [16.4522379034509, -11.372065246394495]],
                      [[[0, 16.305692184072235, -11.72765549112342], [2, -17.49244296888888, -5.371360408288514]], [16.4522379034509, -11.372065246394495]],
                      [[[0, -0.6443452578030207, -2.542378369361001], [2, -32.17857547483552, 6.778675958806988]], [-16.66697847355152, 11.054945886894709]]]

answer_mu1         = matrix([[81.63549976607898],
                             [27.175270706192254],
                             [98.09737507003692],
                             [14.556272940621195],
                             [71.97926631050574],
                             [75.07644206765099],
                             [65.30397603859097],
                             [22.150809430682695]])

answer_omega1      = matrix([[0.36603773584905663, 0.0, -0.169811320754717, 0.0, -0.011320754716981133, 0.0, -0.1811320754716981, 0.0],
                             [0.0, 0.36603773584905663, 0.0, -0.169811320754717, 0.0, -0.011320754716981133, 0.0, -0.1811320754716981],
                             [-0.169811320754717, 0.0, 0.6509433962264151, 0.0, -0.05660377358490567, 0.0, -0.40566037735849064, 0.0],
                             [0.0, -0.169811320754717, 0.0, 0.6509433962264151, 0.0, -0.05660377358490567, 0.0, -0.40566037735849064],
                             [-0.011320754716981133, 0.0, -0.05660377358490567, 0.0, 0.6962264150943396, 0.0, -0.360377358490566, 0.0],
                             [0.0, -0.011320754716981133, 0.0, -0.05660377358490567, 0.0, 0.6962264150943396, 0.0, -0.360377358490566],
                             [-0.1811320754716981, 0.0, -0.4056603773584906, 0.0, -0.360377358490566, 0.0, 1.2339622641509433, 0.0],
                             [0.0, -0.1811320754716981, 0.0, -0.4056603773584906, 0.0, -0.360377358490566, 0.0, 1.2339622641509433]])

#result = online_slam(testdata1, 5, 3, 2.0, 2.0)
#solution_check(result, answer_mu1, answer_omega1)


# -----------
# Test Case 2

testdata2          = [[[[0, 12.637647070797396, 17.45189715769647], [1, 10.432982633935133, -25.49437383412288]], [17.232472057089492, 10.150955955063045]],
                      [[[0, -4.104607680013634, 11.41471295488775], [1, -2.6421937245699176, -30.500310738397154]], [17.232472057089492, 10.150955955063045]],
                      [[[0, -27.157759429499166, -1.9907376178358271], [1, -23.19841267128686, -43.2248146183254]], [-17.10510363812527, 10.364141523975523]],
                      [[[0, -2.7880265859173763, -16.41914969572965], [1, -3.6771540967943794, -54.29943770172535]], [-17.10510363812527, 10.364141523975523]],
                      [[[0, 10.844236516370763, -27.19190207903398], [1, 14.728670653019343, -63.53743222490458]], [14.192077112147086, -14.09201714598981]]]

answer_mu2         = matrix([[63.37479912250136],
                             [78.17644539069596],
                             [61.33207502170053],
                             [67.10699675357239],
                             [62.57455560221361],
                             [27.042758786080363]])

answer_omega2      = matrix([[0.22871751620895048, 0.0, -0.11351536555795691, 0.0, -0.11351536555795691, 0.0],
                             [0.0, 0.22871751620895048, 0.0, -0.11351536555795691, 0.0, -0.11351536555795691],
                             [-0.11351536555795691, 0.0, 0.7867205207948973, 0.0, -0.46327947920510265, 0.0],
                             [0.0, -0.11351536555795691, 0.0, 0.7867205207948973, 0.0, -0.46327947920510265],
                             [-0.11351536555795691, 0.0, -0.46327947920510265, 0.0, 0.7867205207948973, 0.0],
                             [0.0, -0.11351536555795691, 0.0, -0.46327947920510265, 0.0, 0.7867205207948973]])

#result = online_slam(testdata2, 6, 2, 3.0, 4.0)
#solution_check(result, answer_mu2, answer_omega2)



```


    Landmarks:  [[86.0, 1.0], [90.0, 72.0], [62.0, 43.0], [81.0, 10.0], [95.0, 59.0]]
    Robot: [x=81.78493 y=45.74943]
    
    Estimated Pose(s):
        [50.000, 50.000]
        [33.582, 40.420]
        [16.157, 30.196]
        [18.158, 49.975]
        [20.158, 69.755]
        [22.159, 89.535]
        [2.512, 94.002]
        [7.813, 74.666]
        [13.114, 55.329]
        [18.415, 35.992]
        [23.716, 16.655]
        [28.790, 35.852]
        [34.571, 54.909]
        [40.415, 73.879]
        [46.258, 92.850]
        [61.065, 78.451]
        [74.780, 63.601]
        [89.020, 49.906]
        [101.903, 35.823]
        [83.892, 44.517]
    
    Estimated Landmarks:
        [89.289, 2.275]
        [90.756, 72.212]
        [63.121, 44.250]
        [84.691, 9.346]
        [95.899, 58.650]
    
    Estimated Pose(s):
        [83.892, 44.517]
    
    Estimated Landmarks:
        [89.289, 2.275]
        [90.756, 72.212]
        [63.121, 44.250]
        [84.691, 9.346]
        [95.899, 58.650]



```python
list = [0,1] + range(4, 8)
```


```python
list
```




    [0, 1, 4, 5, 6, 7]



# what is Ackerman steering

If front wheels run at the same angle and same speed, there must be slipping because the geometry of the 2 wheels does not satisfy the whole process.

![Ackerman1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/Ackerman1.png)

To avoid the slipping, Ackerman steering is used, where different angles, different trajectory, and as a result, different speeds are applied for the front wheels.

By the way, the idea was proposed by a german, and Ackerman was just a lawyer who helped the german to apply for the patent.

![Ackerman2](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/Ackerman2.png)

![Ackerman3](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/Ackerman3.png)

![Ackerman4](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/Ackerman4.png)

# Rotation matrix derivation

## 没有位移的情况

![rotation_matrix1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/rotation_matrix1.png)
![rotation_matrix2](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/rotation_matrix2.png)

反过来，很容易由上述式子计算从 $x', y'$ 到 $x,y$ 的公式为
$$
x = x'\cos\theta - y'\sin\theta \\
y = x'\sin\theta + y'\cos\theta
$$
即矩阵为
$$
R'(\theta) = \left[
\begin{matrix}
\cos\theta & -\sin\theta \\
\sin\theta & \cos\theta 
\end{matrix}
\right]
$$
是 $R(\theta)$ 的转置也是逆。

## 有位移的情况

![rotation_matrix3](pic/rotation_matrix3.png)

先假设两个坐标系原点相同，用前边那种没有位移的方式计算，然后再考虑两坐标系之间的位移。

## Homogeneous coordinate form

上述两种情况可以统一用 homogeneous coordinate form 表示
$$
P_E = [C_{BE}(\theta)~~ O_{BE}]\left[
    \begin{matrix}
    P_B \\
    1
    \end{matrix}
\right]
$$


# particle filter

```python
from math import *
import random

landmarks  = [[20.0, 20.0], [80.0, 80.0], [20.0, 80.0], [80.0, 20.0]]
world_size = 100.0

class robot:
    def __init__(self):
        self.x = random.random() * world_size
        self.y = random.random() * world_size
        self.orientation = random.random() * 2.0 * pi
        self.forward_noise = 0.0;
        self.turn_noise    = 0.0;
        self.sense_noise   = 0.0;
    
    def set(self, new_x, new_y, new_orientation):
        if new_x < 0 or new_x >= world_size:
            raise ValueError, 'X coordinate out of bound'
        if new_y < 0 or new_y >= world_size:
            raise ValueError, 'Y coordinate out of bound'
        if new_orientation < 0 or new_orientation >= 2 * pi:
            raise ValueError, 'Orientation must be in [0..2pi]'
        self.x = float(new_x)
        self.y = float(new_y)
        self.orientation = float(new_orientation)
    
    
    def set_noise(self, new_f_noise, new_t_noise, new_s_noise):
        # makes it possible to change the noise parameters
        # this is often useful in particle filters
        self.forward_noise = float(new_f_noise);
        self.turn_noise    = float(new_t_noise);
        self.sense_noise   = float(new_s_noise);
    
    
    def sense(self):
        Z = []
        for i in range(len(landmarks)):
            dist = sqrt((self.x - landmarks[i][0]) ** 2 + (self.y - landmarks[i][1]) ** 2)
            dist += random.gauss(0.0, self.sense_noise)
            Z.append(dist)
        return Z
    
    
    def move(self, turn, forward):
        if forward < 0:
            raise ValueError, 'Robot cant move backwards'         
        
        # turn, and add randomness to the turning command
        orientation = self.orientation + float(turn) + random.gauss(0.0, self.turn_noise)
        orientation %= 2 * pi
        
        # move, and add randomness to the motion command
        dist = float(forward) + random.gauss(0.0, self.forward_noise)
        x = self.x + (cos(orientation) * dist)
        y = self.y + (sin(orientation) * dist)
        x %= world_size    # cyclic truncate
        y %= world_size
        
        # set particle
        res = robot()
        res.set(x, y, orientation)
        res.set_noise(self.forward_noise, self.turn_noise, self.sense_noise)
        return res
    
    def Gaussian(self, mu, sigma, x):
        
        # calculates the probability of x for 1-dim Gaussian with mean mu and var. sigma
        return exp(- ((mu - x) ** 2) / (sigma ** 2) / 2.0) / sqrt(2.0 * pi * (sigma ** 2))
    
    
    def measurement_prob(self, measurement):
        
        # calculates how likely a measurement should be
        
        prob = 1.0;
        for i in range(len(landmarks)):
            dist = sqrt((self.x - landmarks[i][0]) ** 2 + (self.y - landmarks[i][1]) ** 2)
            prob *= self.Gaussian(dist, self.sense_noise, measurement[i])
            # 这里的概率，即下次采样的权重，就是贝叶斯公式中的 p(z|x)
        return prob
      
    def __repr__(self):
        return '[x=%.6s y=%.6s orient=%.6s]' % (str(self.x), str(self.y), str(self.orientation))



def eval(r, p):
    sum = 0.0;
    for i in range(len(p)): # calculate mean error
        dx = (p[i].x - r.x + (world_size/2.0)) % world_size - (world_size/2.0)
        dy = (p[i].y - r.y + (world_size/2.0)) % world_size - (world_size/2.0)
        err = sqrt(dx * dx + dy * dy)
        sum += err
    return sum / float(len(p))

#myrobot = robot()
#myrobot.set_noise(5.0, 0.1, 5.0)
#myrobot.set(30.0, 50.0, pi/2)
#myrobot = myrobot.move(-pi/2, 15.0)
#print myrobot.sense()
#myrobot = myrobot.move(-pi/2, 10.0)
#print myrobot.sense()

####   DON'T MODIFY ANYTHING ABOVE HERE! ENTER/MODIFY CODE BELOW ####
myrobot = robot()
myrobot = myrobot.move(0.1, 5.0)
Z = myrobot.sense()
N = 1000
T = 10 #Leave this as 10 for grading purposes.

p = []
for i in range(N):   # 产生 1000 个粒子，每个粒子都代表机器人可能的位置
    r = robot()
    r.set_noise(0.05, 0.05, 5.0)
    p.append(r)

for t in range(T):
    myrobot = myrobot.move(0.1, 5.0)
    Z = myrobot.sense()   # 这里是真正的 robot, 它的作用是提供观测数据，该 robot 的位置原则上无法获得

    p2 = []
    for i in range(N):
        p2.append(p[i].move(0.1, 5.0)) # 粒子也依照 robot 接受到的控制移动一次
    p = p2

    w = []
    for i in range(N):
        w.append(p[i].measurement_prob(Z)) # 基于观测数据和例子预测的位置，计算该粒子的权重

    p3 = []
    index = int(random.random() * N)
    beta = 0.0
    mw = max(w)
    for i in range(N):
        beta += random.random() * 2.0 * mw
        while beta > w[index]:
            beta -= w[index]
            index = (index + 1) % N
        p3.append(p[index])    # 依权重重新采样，保持粒子数量不变
    p = p3
    print(eval(myrobot, p))  # 评价该步结束之后粒子往真是位置聚集的程度
    
 



```

```
4.32245125673
5.53693010626
4.78154924833
4.14364384168
6.20936433619
7.7457019268
6.81595043841
5.93257147215
5.2119488158
3.99470828929
```

0.75**10

