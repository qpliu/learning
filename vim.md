## 基本命令

vim 主要有两个 mode: insert 和 normal。前者就是普通的文字输入模式，后者是控制模式。

### 进入 insert mode

- i：在当前光标位置前进入 insert 模式
- I : 在当前行的开头进入 insert 模式
- a：在当前光标位置后进入 insert 模式
- A ：在当前行的末尾进入 insert 模式 
- o：在下一行进入 insert
- O ：在上一行进入 insert



### 进入控制模式移动光标

在 insert mode 中按 ESC 键即可进入控制模式。

在控制模式中最常用的就是光标的移动

- h 左
- j  下
- k  上
- l  右

还有基于 word 的移动，这里一个 word 是一个连续的字符串，包括文字和紧挨着的符号

- w ： 下一个开头，以 word 为单位，遇到符号就认为新的 word
- capital W ： 下一个开头，以大 WORD 为单位，遇到空格才认为是新 WORD
- e ： 下一个结尾，不是下一个 word 结尾，而是下一个结尾，如果当前位置是在 word 中部，则就是当前 word 的结尾
- capital E ： 下一个结尾，以大 WORD 为单位，遇到空格才认为是新 WORD
- b：本 word 的开头，如果已经在开头，则到前一个 word 的开头

**number 加上述移动操作，就相当于按了 number 次那个键。**

也可以输入 number 次某个字符串，例如 3igo ESC，就输入了 3 次 go

- 0: 到本行头部第一个字符 

- $: 到本行末尾最后一个字符

- gg: 到整篇文章的开头

- G: 到整篇文章的结尾

- number + G : 到文章的第 number 行

  **上下翻页**

- control + b: 上翻页，back

- control + f : 下翻页，forward

  

  **文件操作**

- ：w 保存
- ：q  退出
- ：q! 强制退出，不保存



### 查找字符

- f + <字符> 是向后查找某个字符
- F + <字符> 是向前查找某个字符
- number + 上边的命令是向前/向后查找第 number 个字符
- / ： search forward
- ? ： search backward
- 上述两个查找命令可以通过 n 和 N 
- 查找之后可能会一直高亮显示搜索词，即使我们已经结束了搜索。此时输入命令 `:nohlsearch`可以结束高亮。
- 在程序中，可以用 % 查找配对的括号
- *: 当前 word 下一次出现的位置
- #: 当前 word 上一次出现的位置



### 文本操作：替换、删除、粘贴

- r：替换一个字符

- R ：替换当前位置开始的多个字符

- c + 位置信息：删除从当前位置到给定位置的字符，并进入 insert 模式

- C : 删除从当前位置到行尾的字符，并进入 insert 模式

- x : 等价于普通的 del —— 删除当前位置字符

- X : 等价于普通的 backspace —— 删除前一字符

  相比于上述的 x, 其实 d 的功能更丰富，可以指定删除的范围

- d + 位置信息：删除从当前位置到给定位置的字符

- dw : 删除从当前位置到本 word 结束，光标放在下一个 word 开头

- de : 与 dw 类似，删除之后放在本 word 的结尾

  上边这两个删除命令，比较容易记的方式是，w 或 e 原本要跳到哪里，删除之后光标就会落到哪里。

  原本 w 是到下一个 word 开头，所以删除的时候，就一直删到下一个 word 开头，光标停在那里

  原本 e 是到下一个结尾，由于整个 word 都删了，所以往后挪一个，到了下一个 word 前边的一个位置

- D ：删除从当前位置到行尾的字符

- 似乎，d 与 c 的区别仅仅是删除之后是否进入 insert 模式

- y + 位置信息：复制从当前位置到给定位置的字符

- Y == yy ：复制整行

- p：粘贴到当前位置后边，如果是整行内容，则粘贴到下一行

- P : 粘贴到当前位置的前边，如果是整行内容，则张贴到上一行

- 如果不是一整行，但是也想粘贴到下一行，而不是当前位置后边，可以用命令 `:put`

  查找并替换

- `：1,4s/word1/word2/g` ： 1,4 是指明搜索的行号，从第1行到第4行，/g 是指明每行如果有多个搜索结果，则都要替换掉

- `：%s/word1/word2/g`： % 是指明全文搜索

- `~` 改变当前位置字母的大小写

- u : undo

- ctrl + r : redo

- .  : 重复之前的操作



### visual mode

除了前边的 normal mode 和 insert mode，还有一个 visual mode，允许选取一定范围的文字，然后进行操作

- v ： 进入 visual mode



### 其他命令

- 文件操作
  - 保存新文件：`:w newfile.txt`
  - 打开/创建文件：`:e newfile.txt`
- 窗口操作
  - 命令分窗口：`:split` 默认上下分，如果希望左右分，用命令：`:vsplit`
  - 快捷键分窗口： `ctrl+w s`, `ctrl+w v`
  - 分完窗口之后，默认各个窗口的内容都是相同的，他们对应了同一个buffer，可以在某窗口中打开新的文件
  - 在窗口之间移动：`ctrl+w  hjkl` 
  - 关闭某窗口：将光标移动到该窗口，然后用命令：`:q`，或者 `ctrl+w c`
- tab 操作
  - 新建 tab :`:tabnew`
  - 在 tab 间切换：`:tabp` 切换到 previous ， `:tabn` 切换到 next
  - 关闭当前 tab : `:tabclose`
- line mark
  - 标记某一行 `m + <char>`，例如 `mr`
  - 光标到某一标记行 `' + <char>`，例如 `'r` 
  - 注意：这个标记并不是与行数绑定，而是随那一行的文字移动的



## 如何实现 vim 与系统粘贴板之间的交互？
- 要用 vim-gtk 版本，基本的 vim 版本不行
- 从 vim 复制到系统中
  - 用命令： `"+y` 进行复制，然后就可以在系统中粘贴了
- 从系统中复制到 vim
  - vim 在 insert 模式下，`ctrl + shift + v` 进行粘贴

## 如何加载 plugin

### 不借助 plugin manager 的手动加载
(此方法或许不正确，没有实验成功，不知道问题出在哪里)
1. 找到 home 下的 .vim 文件夹，如果没有就创建一个
2. 在 .vim 文件夹下创建 bundle 文件夹  
   - 其实所有这些文件夹路径都是约定俗成的，都不是固定的，后边可以设定
3. 拷贝 plugin 文件到 bundle 文件夹下
   - 如果 plugin 文件是托管在 github 上的，就用 git clone 下载下来
   - 如果是单个的 .vim 文件，最好再创建一个 plugin 文件夹存放所有的 .vim 文件
4. 告诉 vim 去哪里寻找这些 plugin 文件
   - 先看看当前的搜索路径：打开 vim，输入命令 `:set runtimepath`，会显示当前设置的搜索路径
   - 设置 runtimepath: 在 home 下的 .vimrc 文件中添加希望的路径 `set runtimepath^=~/.vim/bundle/ctrlp.vim`
5. 重新加载
   - 可以在 vim 中用命令 `:source ~/.vimrc`
   - 也可以关掉 vim，再重新打开 vim




### 另一种打开 .vimrc 的方法 
在 vim 中打开 `$MYVIMRC` 文件，即 `:e $MYVIMRC`

## 编程有关的快捷键
- `%` 在配对的括号之间跳转，包括各种括号
- `>` ，`<` 缩进。可以与范围命令一起使用，缩进若干行. `5 + > + >`
- 如果我们不知道某种语言该如何缩进比较好，可以让 vim 决定， `= + =` 也可以加范围命令, `= G`。
- 与前述括号配对命令合用，可以达到比较满意的缩进效果。


## `:set` 命令 
- `:set nu`  显示行号
- `:set nonu` 不显示行号
- `:set rnu` 显示相对行数
- `:set autoindent` 自动缩进
- `:set colorcolumn=80` 将第 80 列颜色显示。该命令并不会强制换行，只是提醒每一行的长度是否太长
- `:set ruler` 在窗口下部显示当前行数与列数
- `;set bg=dark/light` 当前颜色显示的色深
- `:colors <theme>` 设置颜色主题

## 配置 python 开发环境
   1. 做些准备工作： `sudo apt-get install vim git-core wget`
   2. 下载 vimrc： 
   ```
   wget https://raw.githubusercontent.com/thesheff17/youtube/master/vim/vimrc
   mv vimrc .vimrc
   ```
   3. 创建几个目录存放文件
   ```
   mkdir .vim  
   cd .vim/
   mkdir autoload bundle colors ftplugin
   ```
   4. autoload 目录中
   ```
   wget https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim
   ```
   5. bundle 目录中
   ```
   git clone https://github.com/tpope/vim-sensible.git && git clone https://github.com/kien/ctrlp.vim.git && git clone https://github.com/scrooloose/nerdtree && git clone --recurse-submodules https://github.com/klen/python-mode.git && git clone https://github.com/Lokaltog/vim-powerline.git && git clone https://github.com/jistr/vim-nerdtree-tabs.git
   ```
   上述 python-mode repo 可能有些 submodule，如果在后续运行的时候提示错误，可以将 submodule 下载一遍
   6. colors 目录中
   ```
   wget https://raw.githubusercontent.com/thesheff17/youtube/master/vim/wombat256mod.vim
   ```
   7. ftplugin 目录中
   ```
   wget https://raw.githubusercontent.com/thesheff17/youtube/master/vim/python_editing.vim
   ```

## 配置 julia 开发环境
只需要在上述 python 环境中，把配置文件放进去 bundle 目录中即可
```
git clone git://github.com/JuliaEditorSupport/julia-vim.git
```
