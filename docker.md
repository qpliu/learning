# Docker 简介

## 为什么用 Docker

编写软件遇到的一个问题是，在自己机子上可以运行的程序，在别人那里不一定可以运行，因为系统环境不一样。那么能不能让软件自带系统环境呢？在安装软件的时候连带当初的系统环境一起安装了。这就是 Docker 要解决的问题。

## 与虚拟机的区别

虚拟机太庞大，能够模拟整个操作系统，占用资源多，启动慢。附带了很多运行软件不需要的累赘。

针对虚拟机的上述缺陷， Linux 发展出了另一种虚拟化技术：Linux 容器 （Linux Container, 即 LXC）。Docker 是当今最流行的 Linux 容器解决方案。

Docker 将应用程序和环境以来打包在一个文件中，运行这个文件就运行了一个虚拟容器。程序在这个虚拟容器中运行，这样就隔离了应用程序和实际的机子环境，不用在担心环境支持的问题了。

# Docker install

1. 按照官网教程安装即可。 https://docs.docker.com/install/linux/docker-ce/ubuntu/

   官方推荐使用 reporsitory 方式。具体如下：

   - 如果已经安装了旧版本，要先卸载：

     ```bash
     $ sudo apt-get remove docker docker-engine docker.io containerd runc
     ```

   - apt update

     ```
     $ sudo apt-get update
     ```

   - 安装一些 package，以便允许 apt 通过 HTTPS 使用 repository

     ```
     $ sudo apt-get install \
         apt-transport-https \
         ca-certificates \
         curl \
         gnupg-agent \
         software-properties-common
     ```

     下边才是核心步骤：

   - 添加 Docker 官方 GPG key

     ```bash
     $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
     ```

     搜索并确认一下 key 

     ```
     $ sudo apt-key fingerprint 0EBFCD88
     
     pub   4096R/0EBFCD88 2017-02-22
           Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
     uid                  Docker Release (CE deb) <docker@docker.com>
     sub   4096R/F273FCD8 2017-02-22
     ```

     

   - 添加 Docker repository

     ```bash
     $ sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
     ```

   - apt update

     ```bash
     $ sudo apt-get update
     ```

   - 安装最新版本的 docker

     ```bash
     $ sudo apt-get install docker-ce docker-ce-cli containerd.io
     ```

     或者 安装特定版本的 docker

     先看看有哪些版本

     ```bash
     $ apt-cache madison docker-ce
     
       docker-ce | 5:18.09.1~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
       docker-ce | 5:18.09.0~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
       docker-ce | 18.06.1~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
       docker-ce | 18.06.0~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
       ...
     ```

     再安装对应版本的 docker

     ```bash
     $ sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
     ```

     

   - 确认 docker 成功安装了

     运行 hello-world image 试试，看是否有显示信息

     ```bash
     $ sudo docker run hello-world
     ```

     

2. 上述方法安装完之后，需要用 sudo 才能运行 docker，比较麻烦。再进一步设置一下

   - 创建 docker 用户组 

     ```bash
     sudo groupadd docker
     ```

   - 将当前用户添加进去

     ```bash
     sudo usermod -aG docker $USER
     ```

   - log out 再 log in ，不需要 sudo 就可以启动 docker 了。



# Dock	er Plugin provided by NVIDIA

如果想在 docker container 中使用 NVIDIA GPU，则需要安装由 NVIDIA 提供的 docker plugin。

NVIDIA 与 Docker 系统架构如下：

![nvidia_docker](pic/nvidia_docker.png)



首先确认在本机上已经安装了 CUDA 驱动和 Docker.

1. 如果安装了旧版本的 nvidia-docker 1.0，则需要先卸载掉

   ```bash
   docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
   
   sudo apt-get purge -y nvidia-docker
   ```

2. 添加 reporsitory

   ```bash
   curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
     sudo apt-key add -
     
   distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
   
   curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \ 
   	sudo tee /etc/apt/sources.list.d/nvidia-docker.list
     
   sudo apt-get update
   ```

3. 安装 nvidia-docker2 并重新加载 Docker daemon configuration

   ```bash
   sudo apt-get install -y nvidia-docker2
   
   sudo pkill -SIGHUP dockerd
   ```

4. 检查安装成功

   在 nvidia 官方给的 image  nvidia/cuda:9.0-base 中运行 nvidia-smi 

   ```bash
   docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
   ```



# Docker 常用运行命令 

docker 命令中有些 options, arguments，位置是有限制的，例如只能出现在 image 前，或只能在后.

例如：

```bash
Usage:	docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

如果要用 option，必须放在 image 之前

如果用 command，必须在 image 之后

## image 相关

- 列出已有 image

  ```
  docker image ls
  ```

- 删除 image

  ```
  docker image rm image_name
  ```

- 从网上下载 image

  ```
  docker image pull library/hello-world
  ```

  其中 library 是 image 所在的组。实际上，docker 官方提供的 image 都放在了 library 组中，它是默认组，因此上边的命令可以直接写成

  ```
  docker image pull hello-world
  ```

  现在再用 docker image ls 就可以看到新的 image 了

## container 相关

- 从 image 生成 container 实例

  ```
  docker container run hello-world
  ```

  实际上， docker container run 可以自动抓取 image，如果本地没有 Image，就从网上仓库下载，因此前边的 docker image pull 命令也可以省略了。

  旧版本命令

  ```
  docker run [--name=something] <image_name>
  ```

  有相同的效果

  其中，可以设定生成的 container 的名字。

- 关闭一个 container

  ```
  docker container kill container_ID
  ```

  kill 命令发送的是 SIGKILL 信号，强制关闭程序，也可以更缓和一些，用 stop 命令

  ```
  docker container stop container_ID
  ```

  这个 stop 命令发送的是 SIGTERM 信号，让程序自己优雅的结束。

  

  由 image 生成的 container 实例本身也是文件，称为容器文件。也就是说，一旦容器生成，就应该存在两个文件，一个是 image 文件，一个是容器文件。关闭正在运行的容器并不会删除容器文件，只是退出运行而已。

- 运行已经存在的容器文件

  前边的 run 命令每次都会生成一个 container 文件，如果想运行已经存在的容器文件，可以用 start 命令。要注意的是，run 后边跟 image 的名字， start 后边跟 container_ID，这就意味着 start 只能针对已有的 container 运行。

  ```
  docker container start container_ID
  或
  docker container start -i container_ID 进入interactive环境，这里就不需要 -t /bin/bash 了
  ```

  **容器文件中保存了上一次运行时对 container 内部的修改。**

  **所以，如果想保存多个版本，可以每次从 image 运行，然后给每个 container 容易辨认的名字，然后保留 container 文件。**

  

- 列出运行中的 container

  ```
  docker container ls
  ```

- 列出所有的 container，包括停止的

  ```
  docker container ls --all
  ```

  在最初的时候， docker 的命令比较松散，例如用 docker ps 是 list containers。后来感觉这样定义命令不太好记，就引入了新的命令，将原本比较松散的命令组织起来，更容易记忆和阅读

  - docker container ls  == docker ps

  - docker container rm ... == docker rm ...
  - docker image rm ... == docker rmi ...

  也就是把所有命令分门别类（例如关于 container 的，关于 image 的，关于 network 的等等）的组织起来，比较有系统性。

- 彻底删除 container 文件

  ```
  docker container rm <container_name or container_ID>
  ```

  也可以在运行 image 产生 container 的时候就加上 --rm 参数，告诉 docker 只要这个 container 一退出，就删除它。所以可以见到很多如下格式的命令：

  ```bash
  docker run -it --rm ...
  ```

  -it 后边会介绍， --rm 就是为了自动删除 container.

- 从 运行的 container 中拷贝文件到本机

  ```
  docker container cp container_ID:path_to_file  .
  ```

  上述命令是拷贝到当前目录

- 查看 container 中 shell 输出

  要想在 image 运行的时候进入 container 的 shell，用那些常用的 linux 操作，那么需要 -it flag

  -i  ： interactive 表示可以与 container 交互，输入命令

  -t ： pseudo terminal 提供交互的 terminal 界面

  所以一般运行 image 时，可以用

  ```
  docker run -it <image_name> /bin/bash
  ```

  就进入了 image 环境中的 shell，然后就可以输入命令互动了。

  一般来说，必须加 -it，否则运行 image 之后马上就退出，如果程序没有设定输出的话，什么都不会留下，就像是闪退一样。

  

  如果在运行 image 的时候没有用 -it 参数，则可以通过 logs 命令查看 container 中的输出

  ```
  docker container logs container_ID
  ```

- 进入一个正在运行的 container

  ```
  docker container exec -it container_ID  /bin/bash 或者只写 bash
  ```

  如果在运行 image 的时候没有用 -it 参数，那么可以用上述命令进入 container 的 shell 去执行命令了。

- 后台运行

  如果程序开启之后是一直在运行的，但不希望老是占着 terminal，就可以设置成后台运行

  ```bash
  docker container run  -d  <image_name>
  ```

## network 相关

安装好 docker 之后，可以查看现有的 network

```bash
docker network ls

NETWORK ID          NAME                DRIVER              SCOPE
c749b1b83091        bridge              bridge              local
a23ca96b3291        host                host                local
74d0c66e9190        none                null                local
```

一般会有上述三个 network。

如果不设定 network，则默认是第一个 bridge network. 此时，如果 container expose 某个 port 例如 8080，则 bridge network 中的其他程序可以通过这个 port 与它通信。如果 host network 中程序想与它通讯，必须设置一个端口映射 -p 5000:80，这样所有与 host network 8080 端口通讯的都可以被映射到 container 的 80 端口。

例如

```bash
docker run -p 5000:80 nginx
```



也可以让 container 直接运行在 host  network 中，这就是为什么在很多运行 image 的命令中，都有 --net=host 设置。这样的话，就可以直接通过 host 的端口进行通讯了，不需要桥接映射。



## 文件共享

在启动 image 的时候，可以用 -v /host_path:/container_path  来设置两个系统之间的共享文件夹

例如

```bash
docker run -v ~/nginxlogs:/var/log/nginx  <image_name>
```

记住，总是先写 host 的文件路径或者上边提到的 port 端口。



# 制作 image 文件

基本流程是先写一个 Dockerfile，本质上是一个配置文件，用来配置 image，然后 Docker 根据 Dockerfile 生成二进制的 image 文件。

下边的例子是制造一个 Docker image，使用户可以在容器中运行 Koa.

1.  下载源码

   ```
   git clone https://github.com/ruanyf/koa-demos.git
   ```

   进入文件夹 koa-demos

2. **.dockerignore 文件：**如果需要排除一些路径，不包含在 image 文件中，可以写一个 .dockerignore 文件，包含如下内容

   ```
   .git
   node_modules
   npm-debug.log
   ```

   这就意味着这三个路径不打包进入 image。

   如果没有需要排除的，可以不写这个 .dockerignore 文件

3. **Dockerfile 文件**：这是 image 的配置文件，包含如下内容

   ```
   FROM node:8.4
   COPY . /app
   WORKDIR /app
   RUN npm install --registry=https://registry.npm.taobao.org
   EXPOSE 3000
   ```

   - 第一行表示 image 继承自官方给的 node image，版本号是 8.4。一般来说，自己的 image 没有从零开始写的，都是在现有 image 基础上修改的。
   - 将当前目录中的所有文件拷贝到 image 的 /app 目录中。
   - 指定接下来的工作路径为 /app
   - 在 /app 目录中运行 npm install 命令，安装依赖，所有的依赖都打包进入 image
   - 将容器的 3000 端口暴露出来，允许外部连接这个端口。

4. 基于 Dockerfile 创建 image 文件

   ```
   docker image build -t koa-demo[:0.0.1] .
   ```

   -t 后边跟 image 的名字。冒号后边跟版本号，如果没有，默认标签是 latest。最后那个 . 表示Dockerfile 所在目录，即当前目录

5. 测试。如果上述命令运行成功，则会生成 image koa-demo。用下述命令运行 image

   ```
   docker container run -p 8000:3000 -it koa-demo /bin/bash
   ```

   -p 参数将容器的 3000 端口映射到本机的 8000 端口

   -it 参数将容器的 shell 映射到本机的 shell

   /bin/bash 是容器启动之后执行的第一个命令，保证用户直接进入 shell 环境

   如果一切顺利，将进入如下界面：

   ```
   root@66d80f4aaf1e:/app#
   ```

   这就表明已经在容器内部了，而且是在 shell 命令中。然后输入命令

   ```
   root@66d80f4aaf1e:/app# node demos/01.js
   ```

   koa 框架就运行起来了，打开浏览器，访问http://127.0.0.1:8000，网页显示"Not Found" 证明成功了。

6. 附加命令。在上述过程中 `node demos/01.js` 这个命令是在容器启动之后手动输入的，也可以在制作 image 的时候预先设置好，让 image 启动之后，自动运行命令。Dockerfile 文件改成如下形式

   ```
   FROM node:8.4
   COPY . /app
   WORKDIR /app
   RUN npm install --registry=https://registry.npm.taobao.org
   EXPOSE 3000
   CMD node demos/01.js
   ```

   只是多了一行 CMD ，就是让容器启动之后自动运行后边的命令。

   有了 CMD 命令，在运行 image 时就不能再附加命令了，例如 /bin/bash 否则会覆盖掉 CMD 命令，现在启动容器的命令是

   ```
   docker container run -p 8000:3000 -it koa-demo
   ```

# 发布 image 文件

如果要将 image 分享到网上，需要在 https://hub.docker.com 中建立一个账户。

我的 ID 是 qipengliu2019，对应的邮箱是 gmail.

- 登录 docker hub

  ```
  docker login
  ```

- 为本地 image 打标签

  ```
  docker image tag [image_name] [username]/[repository_name]:[tag]
  ```

  例如

  ```
  docker image tag koa-demo:latest qipengliu2019/test_koa:001
  ```

  看来这个 image 的名字如果要上传到 hub 的话必须是这种格式的，名字中对应了 repository. 

- 发布到 hub 中

  ```
  docker image push [username]/[repository]:[tag]
  ```

  然后就可以在 docker hub 中自己的 repository 中看到了。

