# 车路协同

车路协同的最终目的依然是自动驾驶。

要实现自动驾驶，除了车辆本身智能化不断提高，也可以通过智能道路加以辅助，更安全有效的实现自动驾驶。

这就在智能汽车之外，构想了智能道路，从而实现车路协同，以及最终的自动驾驶。



车路协同需要三方面的配合：

1. 车端
2. 路侧
3. 通信协议



寻求智能道路的辅助，一方面意味着单纯让电脑达到人的驾驶水平比较困难，短期内无法实现，需要更多原本人类驾驶不需要的辅助才行。但这也不代表自动驾驶的失败，自动驾驶原本也没有强调必须是在人类目前的道路上开。目前的道路及指示牌都是给人看的，让电脑来辨认就比较困难，车应该有自己的道路系统。更先进的智能交通系统在人类驾驶时代是没法使用的，只有在自动驾驶的年代才能发挥作用。



## 基本概念

### OBU

On-Board Unit, a device inside the vehicle that allows its wireless communication with other OBUs and RSUs.

### RSU

Road-Side Unit, a device on the roadside that communicates with OBUs.

### VRU

Vulnerable Road Users,  such as pedestrians, can be considered through vehicle-to-pedestrian (V2P) communications.

## 可研究的方向

可以大致分为三个方面：发送什么，怎么发，接受到之后如何处理

- 发送什么，这个似乎与应用场景关系紧密，不同的应用需要的信息可能不同

- 怎么发，涉及到网络协议方面，不擅长

- 接受到信息之后的应用方面，可以做一下，例如协同建图、定位、协调等。



## VANET 通讯协议

### 基于 802.11P 的 DSRC 通讯

#### IEEE 方面

IEEE 在 IEEE 1609 标准中 发布了 Wireless Access for Vehicle Environments (WAVE) 技术规范，包括了使用基于 IEEE 802.11P 的 dedicated short range communication (DSRC) 以及相关架构和功能.

DSRC 的消息更新频率是 10 Hz.

DSRC 中包括多组数据，例如 

- basic safety message (BSM) : contains GPS position, speed, heading angle, yaw rate
- signal phase and timing (SPaT) : 当前信号灯状态以及剩余时间。

![vanet1](pic/vanet1.png)

#### SAE 方面

The Society of Automotive Engineers (SAE) 在标准 SAE J2735, J3067, J2945 等具体化了消息数据元素和格式，以支持 DSRC 在智能交通场景中的应用。

### cellular 通讯

3rd Generation  Partnership Project (3GPP) 正在开发一种 Long Term Evolution (LTE) 网络，以支持 V2X。

ISO 通过 ISO 21217 发布了 Communications Access for Land Mobiles (CALM) 规范，允许不同的无线电通讯技术与 ITS-Stations 进行通讯。



## 车路协同应用场景

总的来说，车路协同就是为自动驾驶车辆提供了**上帝视角**，可以获取更大范围、更准确的信息，从而优化自动驾驶决策。

### 车速调节过红路灯

可以让车辆减速或者少加速，使其到达红路灯路口，正好变成绿灯。这样减少加速、减速，可以节省能源。

### 协同制图与定位 SLAM

#### 协同定位 论文重现

2017 CMU Master thesis : Reliable navigation for autonomous vehicles in connected vehicle environments by using multi-agent sensor fusion. (by Suryansh Saxena)

**理论步骤：**

1. 车辆首先基于自身传感器对周围环境和障碍物进行检测，这里用 LIDAR 检测得到点云，然后进行 data segmentation，确定障碍物。这里用的是 SCNN (sequential compatible nearest neighbor).
2. 基于LIDAR 感知结果，进行 EKF SLAM。结果得到一张地图，上边保存了到各个障碍物的距离信息。
3. 设置通信数据，其中每个障碍物有对应的距离和不确定性，实际上就是每个障碍物都对应一个高斯分布信号。通过 DSRC (dedicated short range communication) 发布给周围的车辆。
4. 将接收到的数据通过 Euclidean 变换转换到自身坐标系中。转换之后，再对外来的数据进行分簇，区分到底对应哪个障碍物，这里用到 K-nearest neighbors 算法。
5. 分簇之后，每一簇中的数据（除了本地数据之外）融合一下。由于所有的数据都是高斯分布，他们的融合就是简单的加权平均，以协方差为权重。
6. 对比本地数据与外来融合之后的数据
   - 如果外来融合中表明了有某个障碍物，但是本地数据表明没有，则很可能是 false negative，此时要接受外部数据。
   - 如果外来融合表明没有，但是本地表明有，则还是保留这个障碍物，overestimate 好过 underestimate.
   - 即使两者都表明了有障碍物，还有看一下他们概率分布的差别，用 Maximum Deviation Test 计算两者差别。
     - 如果概率分布比较一致，则接受本地估计
     - 如果概率分布差别较大，比较他们的 variance。
       - 如果本地 variance 较小，说明 overestimate 障碍物，处于安全考虑，接受这个 overestimate
       - 如果外部的 variance 较小，则说明本地 underestimate 了， 此时接受外部的估计。

**实验步骤：**

SUMO (simulation of urban mobility) 允许外部软件通过 Python 脚本 TRACI (traffic control interface) 实时修改道路网络、交通流参数、车辆参数。这种精细到车的实时控制是非常必要的。

有很多基于 SUMO 的通讯仿真软件，这里选择了 Veins (vehicles in network simulation)，可以仿真 DSRC 框架，并且实现 SUMO 中 V2X 的通讯。

基于这些平台之上，最核心的那些数据产生、处理过程都在 ROS 中完成。整个仿真环境如下： 

![sumo+ros](pic/sumo+ros1.png)



### 多车协调过路口

通过事先协商，确定过路口的顺序，从而避免不必要的减速、停车以及可能的碰撞事故。

#### 基本概念

- **intersection**: consists of a number of approaches and the crossing area
- **approach**: one or more lanes (streams) in each side of the intersection
- **compatible streams**: can safely cross the intersection simultaneously
- **antagonistic streams**: other than compatible streams
- **signal cycle**: a whole series of the signals
- **signal time**: the duration of a signal cycle
- **stage/phase**: a part of the signal cycle, during which one set of streams has the right of way
- **lost times**: a few seconds that are necessary between stages to avoid interference between antagonistic streams of consecutive stages.

#### 传统的控制方案

除了 fixed-time strategies，目前用的比较多的是 traffic-responsive strategies, make use of real-time measurements provided by inductive loop detectors that are usually located some 40 m upstream of the stop time, to execute some more or less sophisticated vehicle-actuation logic.

- **vehicle-interval method**: 一般用于 2 stage 情况，每个 stage 设置最小绿灯时间。如果在此期间没有车通过，则在最小绿灯时间结束之后转入另一个 stage。如果被检测到有车通过，则每过一辆车，延长一些绿灯时间，如果到时间了，再也没有车通过了，则转入另一个 stage。但是绿灯最长不超过某个数值。

  稍微改进一些的算法是同时考虑另一条车道上的车辆需求，如果那边已经堆积了很多车了，也要适当的放行。

  我感觉 NTU 学校门口的红绿灯就是这种控制方式。

- **Miller's strategy**: 包含在了控制工具 MOVA 中。这实际上是基于全局网络延迟最小的目标函数。控制器每隔 T 秒计算一次，如果延迟 $k\cdot T$ 秒 ($k = 1, 2, ...$)，全局的时间获益是增大还是减小了，用 $J_k$ 表示，然后令 $J = \max J_k$. 如果 $J<0​$, 则不再延迟，转换到下一个 stage，如果还能有收益，就再延续到下一个决策时刻。



### 道路预警

前车经过某些危险路段，如路滑、陡弯等，都可以给后车发预警信号。

另外，视线外的障碍物也可以由其他车辆提前通知，增加反应时间。



### 车队编排 platoon 

在高速公路上，大型货车编队行进，一方面增加道路吞吐量，还可以降低风阻，节能。