
## 关于控制理论这门学科
搞控制理论的人，首先要立足于某一具体研究领域和研究对象，从实际问题中提炼科学问题，这样才能保持控制科学的生命力。

Identifying a new, important application area and spending the time to immerse yourself in it that you can work fluidly with the domain experts represent the real future directions in the system and control field.
<div style="text-align: right"> ---- Jonathan P. How. IEEE Control Systems Magazine, 37(5), 2017 </div>

## 我的研究方向
- 分布式估计（distributed estimation）
  - 潜在应用：传感器网络、多源信息融合等
  - 研究思路：将传统估计方法置于网络环境中，单个传感器能力有效，无法通过收集到的数据给出可接受的估计。设计信息融合算法使得多个传感器通过合作估计到真实状态。
- 网络传播（diffusion over network）
  - 潜在应用：产品推广与病毒营销、疫苗接种等
  - 研究思路：可以从影响力最大化入手，设计算法，寻找最佳种子节点。也可以研究网络结构对传播效果的影响。还可以设计传播策略，提升传播效果。或者与实际社交网络数据结合，做一些大数据分析的研究。
- 异常检测（anomaly detection）
  - 潜在应用：心电图、信用卡交易行为、航天器状态信号、网络入侵等
  - 研究思路：通过机器学习找到输入信号与异常分类之间的映射关系，实际上就是学习一个从输入到分类的复杂函数。然后用到运行中的系统中，得到新的输入，判断是否异常。因此，所有机器学习和函数拟合的方法都可以拿来用。也可以通过模型预测的方式，借助状态估计器，比较期望输出与实际输出的差别。
- 自动驾驶汽车（autonomous vehicle）
  - 研究思路：从构造简单系统出发，环境检测-决策-控制-行动，构成闭环。然后在各个环节逐步加入更加复杂、实际的需求。可现在计算机中仿真，技术成熟之后转入硬件实现。

***理论基础：***
- 图论与复杂网络
- 概率统计

## 正则表达式

### 概述

正则表达式可以用来指定某种字符串模式，从而可以从字符串中搜索特定字符串，进而执行替换、删除、提取等操作。

一般来说，RegEx执行速度比较快，但是对于复杂的字符串匹配任务，RegEx可能也比较复杂，不易理解，不如直接编写python程序实现字符分析和匹配，尽管后者的执行速度可能会慢一些。

1956 年, 一位叫 Stephen Kleene 的数学家在 McCulloch 和 Pitts 关于神经网络的早期工作基础上，发表了一篇标题为"神经网事件的表示法"的论文，引入了正则表达式的概念。正则表达式就是用来描述他称为"正则集的代数"的表达式，因此采用"正则表达式"这个术语。随后，发现可以将这一工作应用于使用 Ken Thompson 的计算搜索算法的一些早期研究，Ken Thompson 是 Unix 的主要发明人。正则表达式的第一个实用应用程序就是 Unix 中的 qed 编辑器。从那时起直至现在正则表达式都是基于文本的编辑器和搜索工具中的一个重要部分。


### 基本语法

我们可以通过正则表达式的实例来一点一点学习它的语法规则。

- 最直接的匹配：按字面意思匹配
  如果要匹配 ‘test’， 就可以直接用 ‘test’ 作为匹配模式，数字也是类似的 ‘391’ 就是指明匹配 字符串中的 '391'。  

- 元字符：当出现元字符（metacharacters）时，意味着匹配模式并不像看起来那么直接。
  元字符只有这几个：  . ^ $ * + ? { } [ ] \ | ( ) 


- [...] 匹配括号中的一个字符

| 实例 | 描述 |
| :--: | :--: |
| [amk] | 匹配 'a'，'m'，或 'k'|
| [Pp]ython | 匹配 'Python' 或 'python' |
| rub[ye] | 匹配 'ruby' 或 'rube' |
| [0-9] | 匹配任何一个数字，相当于 [0123456789] |
| [a-z] | 匹配任意小写字母 |
| [A-Z] | 匹配任意大写字母 |
| [a-zA-Z0-9] | 匹配任意字母及数字 |
| [abc$&#124;] | 一般来说 `$` 是元字符，具有特殊意义，但在 [ ] 中仅仅是普通的 `$` 字符, 元字符 &#124; 也被当作普通字符。另外，\\$, \&#124; 也是表示对应的普通字符 |
| [\s,.] | 匹配任意一个空白字符，或者 `,` 或者 `.` |

- [^...] 匹配除了括号中的字符： 注意，只有在 [ ] 中的 ^ 才有取非的意思，在 [ ] 之外的 ^ 仅仅是普通的 ^ 字符 

| 实例 | 描述 |
| :--: | :--: |
| [^aeiou] | 匹配除了aeiou之外的所有字符 |
| [^0-9] | 匹配除了数字之外的字符 |


- 其他特殊字符： 其中一部分由 `\` 引导，简便地指代一些常用的字符串模式，例如任意数字、空格等等。

| 实例 | 描述 |
| :--: | :--: |
| \d  | 匹配一个数字字符，相当于 [0-9] |
| \D | 匹配一个非数字字符，相当于 [^0-9] |
| \s | 匹配任何空白字符，包括空格、制表符、换页符等，相当于 [\f\n\r\t\v] 这五种空白字符，其中 \f 换页符，\n 换行符，\r 回车， \t 制表符，\v 垂直制表符。|
| \S | 与上式相反，匹配非空白符，相当于 [^\f\n\r\t\v] |
|\w | 匹配数字字母及下划线，相当于 [A-Za-z0-9_]，也就是一般变量名字中允许出现的字符 |
| \W | 与上式相反，匹配非数字字母及下划线，相当于 [^A-Za-z0-9_] |
| .  | 匹配除 \n 之外的任意单个字符。如果要匹配包括 \n 的单个字符，可以使用 [.\n] |
| \b | 匹配一个边界字符，即检查当前位置是否为边界，前有空格或者后有空格，例如 er\b 可以匹配 never 中的 er，因为它在边界，但不能匹配 verb 中的 er。这个特殊字符可以用来区分一组字符是独立的单词，还是某个单词中的一部分，例如 re.compile(r'\bclass\b') 成功匹配 'no class at all'， 但是不会匹配 'one subclass is' 或者 'the declassified algorithm' |
| \B | 与上述相反，匹配非边界字符，例如 er\B 可以匹配 verb 中的 er，当不能匹配 never
| \A | 匹配字符串开始 |
| \Z | 匹配字符串结束，如果最后是换行符，则只匹配到换行前的字符串 |
| \z | 匹配字符串结束，无论最后是否为换行符 |
| a &#124; b | 匹配 a 或 b |
| re* | 匹配0个或多个由前边的表达式 re 定义的字符串模式，例如 [0-9]\* 匹配0个或多个数字； ca\*t 可以匹配 ct, cat, caaaat 等。 \* 这种任意次匹配的模式是贪婪的，即总是尝试匹配尽量长的字符。例如，对字符串 `abcbd` 采用 a[bcd]\*b 模式匹配，首先匹配 a，然后 [bcd]\* 可以一直匹配到字符串末尾，然后再匹配 b ，发现无法匹配，此时 [bcd]\* 再往回退，直到能够匹配上 b 为止，因此结果为 `abcb`。
| re+ | 与 re* 唯一的区别就是不允许匹配 0 次的情况出现，即匹配1个或多个的 re 定义的字符串模式。例如， ca+t 可以匹配 cat, caaaat,但是不能匹配 ct |
| re? | 匹配0个或1个 re 定义的字符串模式。例如， home-?brew 匹配 homebrew 或者 home-brew |
| re{m,n} | 匹配 m 到 n 次 re 定义的字符串模式，即 re 至少出现 m 次，最多出现 n 次。例如， a/{1,3}b 可以匹配 a/b, a//b, a///b，不能匹配 ab, a////b。实际上，前边的三种repeat模式都可以统一由 re{m,n} 来表示： re* 就是 re{0,} ，re+ 就是 re{1,}， re? 就是 re{0,1} 。但还是提倡用前边的三种方式，更短、更易读 |
| re{,n} 或 re{m,} | 省略 m 或者 n, 即不指定下界或者上界， 默认下界为 0 ， 上界为 $\infty$ |
| ^ | 检查是否处于整个字符串开头, print re.search('^From', 'Reciting From Memory') 将返回 None（没有匹配的结果），因为字符串中的 From 并不在开头 |
| $ | 检查是否处于整个字符串尾部 |

### 在Python中使用正则表达式

不同的编程语言的正则表达式有略微差别。下面以Python为例，介绍基本语法和相关的模块`re`，该模块使Python拥有全部的正则表达式功能。通过 re.compile() 函数导入正则表达式，其中正则表达式是字符串形式。
```Python
import re
p = re.compile('ab*')
```
- Backslash Plague 问题 和 raw string
  例如：若要查找latex文件中 \section 这一字符串，需要用正则表达式 `\\section` ， 从而避免 \s 被当成匹配空白字符的模式。这一正则表达式若要以字符串的形式导入python，则还需要对每个backslash做变换，最后的字符串为 `'\\\\section'`。
  ```
  待查找的字符串： '\section' 
  正则表达式： \\section 
  最后输入python的字符串： '\\\\section'  
  ```
  可见，为了匹配一个backslash，最后往python里面输入的正则表达式字符串需要有4个backslash，这显然太不方便了。
  解决方案是采用 Python 的 raw string 的方法。在字符串前边加上 r , 例如 r'\n' 就是普通的字符串'\' + 'n'，转义字符不起作用。
  ```
  待查找的字符串： '\section' 
  正则表达式： \\section 
  最后输入python的字符串： r'\\section'  
  ```
  可见，采用 raw string 之后，正则表达式用引号扩起来，前边加上 r 就可以直接送给 re.compile() 函数。

- **re.compile()**
   首先是为正则表达式创建一个object。例如
   ```python
   import re
   p = re.compile('[a-z]+')   
   ```
   p 就是正则表达式 [a-z]+ 对于的object，它有如下method
   
   | Method  | Purpose |
   | :---: | :---: |
   | match() | 检查给定字符串的开头是否匹配正则表达式 |
   | search() | 检查整个字符串，看是否匹配 |
   | findall() | 将所有匹配的字符串返回，组成 list |
如果找不到匹配的字符串，就返回 None，例如 p.match (" ")。 如果找到了，则也返回一个object，例如 m = p.match("tempo")，m 中就存放了匹配的结果，可以通过以下 method 查看
   | Method  | Purpose |
   | :---: | :---: |
   | group() | 返回匹配的字符串 |
   | start(), end() | 返回匹配的首、尾位置 |
   | span() | 返回首、尾位置组成的tuple: （start, end）|
继续上边的例子，m.group() 就会返回 'tempo'； m.start() 就会返回 0； m.span() 就会返回 (0, 5)

- **match()** 和 **search()** 的对比: 前者只匹配字符串的开头，后者查找整个字符串

```python
>>> print p.match(':::message')
None
>>> m = p.search(':::message')
>>> m.group()
'message'
>>> m.span()
(4,11)
```
- **findall()**

```python
>>> p = re.compile('\d+')
>>> p.findall('12 drummers drumming, 11 pipers piping, 10 lords a-leaping')
['12', '11', '10']
```
- **re.method()**: 除了先用re.compile()构造正则表达式对应的object，然后调用object.method()进行匹配，还可以用re.method()匹配正则表达式和字符串。例如：

```python
>>>print re.match(r'\From\s+', 'Fromage amk')
None
```
match(), search(), findall() 都适用。一般来说，如果是一次性的正则表达式查找，用 re.method() 比较方便，如果用将一个正则表达式一个你用于多个字符串查找，则先构造一个正则表达式object更方便调用。

- ** re.compile() 的参数设置 **: 

```python
>>> p = re.compile('abc', re.IGNORECASE)
```
| Flag, short form | Meaning |
| :---: | :---: |
| DOTALL, S | . 匹配所有字符，包括 \n |
| IGNORECASE, I | 不区分大小写 |
| LOCALE， L | 令系统识别英语之外的字符，如法语字符 |
| MULTILINE, M | 原本 ^ 和 $ 只检查整个字符串的头、尾（如果有\n，则在\n之前），加上此命令，也允许处于某一行的头、尾。 本来 \A 与 ^ 功能相同， re.M 只影响 ^ ， \A 依然只匹配整个字符串开头|
| VERBOSE， X | 可以为正则表达式添加comment，更方便理解 |

- **re.sub(RegEx, repl, 原始字符串): 将匹配成功的字符串替换为 repl **

```python
phone = '2004-959-559 # 这是一个国外电话号码'  
num = re.sub(r'#.*$', '', phone)
# num = 2004-959-559
num = re.sub(r'\D', '', phone)
# num = 2004959559
```
上述 repl 还可以是某个函数

```python
def func(matched):
     return ...
re.sub(pattern, func, 字符串)
```

- ** re.split(pattern, 字符串) ** : 以匹配的字符来分割字符串
在对文本进行处理时，如垃圾邮件分类，需要先进行数据处理。首先拿到的数据可能是一大串字符串，包含了整个邮件内容和各种符号，可以用re.split进行分割，得到想要的字符串列表的形式。
```python
>>> import re
>>> re.split('[a-f]+', '0a3B9', flags = re.IGNORECASE) # 简单情况
['0', '3', '9']
>>> listOfTokens = re.split(r'\W*', String) # 遇到非数字、字母、下划线的字符就分割
>>> [tok.lower() for tok in listOfTokens if len(tok) > 2] # 返回的字符串一律变成小写，并且去掉长度过短没有意义的字符
```
