### 关于 include 头文件

- 头文件的扩展名不一定是 .hpp，任何扩展名都可以，不影响。
- #include 命令没有什么特别的，只是将引用的文件内容原封不动的复制过来
- 两种不同的查找顺序：
  - 头文件`“ ”`： 
    1. 先搜索当前目录
    2. 然后搜索 -I 参数指定的目录： `gcc -I /usr/local/include/node a.c` 
    3. 再搜索gcc/g++的环境变量CPLUS_INCLUDE_PATH（C程序使用的是C_INCLUDE_PATH）
    4. 最后搜索gcc/g++的内定目录，这个内定目录不是由系统 PATH 变量指定的，而是 gcc/g++ 内部设置的，用如下命令查看：
      ```
      $ `g++ -print-prog-name=cc1plus` -v 
      ```
      得到类似下面的结果：
    ```
      ignoring nonexistent directory "/usr/lib/gcc/x86_64-linux-gnu/7/../../../../x86_64-linux-gnu/include"
      #include "..." search starts here:
      #include <...> search starts here:
      /usr/include/c++/7
      /usr/include/x86_64-linux-gnu/c++/7
      /usr/include/c++/7/backward
      /usr/lib/gcc/x86_64-linux-gnu/7/include
      /usr/local/include
      /usr/lib/gcc/x86_64-linux-gnu/7/include-fixed
      /usr/include
      End of search list.
    ```
    其中，包括了 /usr/local/include 和 /usr/include 

  - 头文件`< >`：
    1. 先搜索 -I 参数指定的目录
    2. 然后搜索gcc/g++的环境变量CPLUS_INCLUDE_PATH
    3. 最后搜索gcc/g++的内定目录

 所以对比来看，**" " 方式调用头文件，只是多了一个搜索路径：当前文件夹**。

知道了搜索机制，实际上我们可以更灵活的设置头文件地址，例如用相对路径。比如，a.c需要包含/usr/local/include/node/v8.h，由于/usr/local/include是系统的默认搜索路径，所以在a.c中可以用相对路径包含，`#include<node/v8.h>`。

- 用户自定义.hpp头文件时，一般是将与任务直接相关的命令放在主文件中，不直接相关或者铺垫性的命令放在.hpp头文件中，包括各种lilbrary,namespace等，例如 

    头文件main.hpp中：
```c++
#include <iostream>
using namespace std;
```
    主文件中:
```c++
#include "main.hpp"
int main(){
    cout << "hello, I use header files!";
    return 0;
}
```
- 用header文件的一个好处是将那些需要被其他函数调用的函数声明放到一起，方便调用和修改。否则，一个一个添加，一个一个修改，比较麻烦。
- 常见头文件：
     ​    <iostream>: 只要用到cout 或 cin，都需要用到 iostream
     ​    <string>: 只要用到字符型变量，都要用到string头文件。但有时其他头文件（如iostream）已经包含了string头文件，所以即使不指明加入 string头文件也是可以正确编译的。但是好的编程习惯是把所有需要用到的头文件都加上，以免某些头文件移除之后造成编译问题。
     ​    <cmath>: 包括很多运算函数，如三角函数，对数，取整等, 指数pow(base, exponent), M_PI。
     ​    <cstring>: 包括常见的字符串操作，如复制，连接，比较，长度等。
- 为什么用 #ifndef  #define  #endif，例如：​              
```c++
#ifndef {Filename} 
#define {Filename} 

//{Content of head file} 

#endif
这样才能保证头文件被多个其他文件引用(include)时，内部的数据不会被多次定义而造成错误
```

### 常见 project 结构

- core 函数的头文件，例如名为Circle.h的头文件，它的内容如下：

```c++
#ifndef CIRCLE_H
#define CIRCLE_H

class Circle
{
private:
    double r;//半径
public:
    Circle();//构造函数
    Circle(double R);//构造函数
    double Area();//求面积函数
};

#endif
```
注意到开头结尾的预编译语句。在头文件里，并不写出函数的具体实现。

- core 函数的cpp文件，要给出Circle类的具体实现，它的内容如下：

```c++
#include "Circle.h"

Circle::Circle()
{
    this->r=5.0;
}

Circle::Circle(double R)
{
    this->r=R;
}

double Circle:: Area()
{
    return 3.14*r*r;
}
```
需要注意的是：开头处包含了Circle.h，事实上，只要此cpp文件用到的文件，都要包含进来！这个文件的名字其实不一定要叫Circle.cpp，但非常建议cpp文件与头文件相对应，主要是方便人类程序员理解。

- 建一个main.cpp来测试我们写的Circle类，它的内容如下：

```c++
#include <iostream>
#include "Circle.h"
using namespace std;

int main()
{
    Circle c(3);
    cout<<"Area="<<c.Area()<<endl;
    return 1;
}
```
注意到开头时有#include "Circle.h"的声明，证明我们使用到了我们刚才写的Circle类。

### 命名空间 namespace
主要用来区分不同库中的同名函数和变量。用 namespace 指明到底是来自哪里的，例如 
 - `using namespace std;` 指明相关的函数或者变量是来自 std 这个命名空间的，即如果 std 中有这个函数或变量，肯定是用 std 中的这个。
 - 如果不指明命名空间，则每次使用都需要指明，如 `std::cout`
 - 也可以只加载部分函数或变量 `using std::cout；`，那么在使用的时候 cout 可以不加前缀，但是其他的函数和变量还是要加前缀，如
     `cout << "something" << std::endl;`
 - 如果用到了 string，则也要 using namespace std，否则要写成 std::string

### 产生随机数
调用头文件 `<cstdlib>` ，函数为 `rand()`。这是伪随机数，为了避免每次都一样，需要设置种子 `srand(8)`，不同的种子得到不同的随机数。在实际中，一般用时间为种子赋值， `srand(time(0))`，其中`time(0)` 返回当前时间的秒数，需要调用头文件 `<ctime>` 。

### 用户键盘输入操作
- cin 是最简单的用户输入方式：
```c++
int age = 0;
cout << "Enter your age: " << endl;
cin >> age;
cout << "Your age is " << age << endl;
```
但是cin有个问题：遇到空格就认为输入结束了，用户的输入如果包含空格，则只能保存空格之前的部分。这在输入字符串（例如用户名字）时会出现问题。

- std::getline 以string的形式接受所有用户输入，直到遇到'\n'回车键。
```c++
string name;
cout << "Enter your name: " << endl;
getline(cin, name);
cout << "Your name is " << name << endl;
```
getline 的问题是只能接受字符性变量，需要用stringstream函数进一步转化成其他类型。
```c++
#include <iostream>
#include <sstream>
using namespace std;
int main()
{
    string stringLength;
    float inches = 0;
    cout << "Enter the lengh of the string: " << endl;
    getline(cin, stringLength);
    stringstream(streingLength) >> inches;
    cout << "So the string length is " << inches << endl;
    return 0;
} 
```

### 参数以reference的形式传递
- 基本类型变量： int, float

```c++
#include<iostream>

void increment(int &input); //Note the addition of '&'

int main()
{
    int a = 34;
    std::cout<<"Before the function call a = "<<a<<"\n";
    increment(a);  //调用的时候依然是普通变量的形式
    std::cout<<"After the function call a = "<<a<<"\n";
    return 0;
}
void increment(int &input)// 但在函数中是以变量地址的形式接收变量
{
    input++; //**Note the LACK OF THE addition of '&'**
    std::cout<<"In the function call a = "<<input<<"\n";
}
```
- array: 没的选，本来就不能直接传数据，只能以pointer的形式传送，三种方式：
    1. void functionName(variableType *arrayName)
    2. void functionName(variableType arrayName[length of array])
    3. void functionName(variableType arrayName[])

```c++
#include<iostream>
#include<iomanip>

//Pass the array as a pointer
void arrayAsPointer(int *array, int size);
//Pass the array as a sized array
void arraySized(int array[3], int size);
//Pass the array as an unsized array
void arrayUnSized(int array[], int size);

int main()
{
    const int size = 3;
    int array[size] = {33,66,99};
    //We are passing a pointer or reference to the array
    //so we will not know the size of the array
    //We have to pass the size to the function as well
    arrayAsPointer(array, size);  //在调用的时候没什么特别的，直接送入array变量名
    arraySized(array, size);
    arrayUnSized(array, size);
    return 0;
}

void arrayAsPointer(int *array, int size)
{
    std::cout<<std::setw(5);
    for(int i=0; i<size; i++) 
        std::cout<<array[i]<<" ";
    std::cout<<"\n";
}

void arraySized(int array[3], int size)
{
    std::cout<<std::setw(5);
    for(int i=0; i<size; i++)
        std::cout<<array[i]<<" ";
    std::cout<<"\n";  
}

void arrayUnSized(int array[], int size)
{
    std::cout<<std::setw(5);
    for(int i=0; i<size; i++)
        std::cout<<array[i]<<" ";
    std::cout<<"\n";  
}
```

### 安装与卸载 opencv

#### 简介
OpenCV is an image processing library created by Intel and later supported by Willow Garage and now maintained by Itseez. 

OpenCV means Intel® Open Source Computer Vision Library. It is a collection of C functions and a few C++ classes that implement some popular Image Processing and Computer Vision algorithms. Some of these modules include Face Recognition, RGB-Depth processing, Image Registration, Saliency, Structure From Motion, Tracking, and much more. OpenCV is Available on Mac, Windows, Linux.

Opencv 安装包有两部分： opencv 和 opencv_contrib

#### 安装步骤
** Step 1. Updating Ubuntu **

```
$ sudo apt-get update
 
$ sudo apt-get upgrade
```
** Step2. Install dependencies **

```
$ sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

$ sudo apt-get install python3.5-dev python3-numpy libtbb2 libtbb-dev

$ sudo apt-get install libjpeg-dev libpng-dev libtiff5-dev libjasper-dev libdc1394-22-dev libeigen3-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev sphinx-common libtbb-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libopenexr-dev libgstreamer-plugins-base1.0-dev libavutil-dev libavfilter-dev libavresample-dev
```

** Step 3. Get Opencv **

```
$ sudo -s   (进入 super user 模式)
 
$ cd /opt

### 下载最新的版本
/opt$ git clone https://github.com/Itseez/opencv.git  
/opt$ git clone https://github.com/Itseez/opencv_contrib.git

### 或者下载特定版本 （以3.2版本为例）
wget https://github.com/opencv/opencv/archive/3.2.0.tar.gz

tar -xvzf 3.2.0.tar.gz

wget https://github.com/opencv/opencv_contrib/archive/3.2.0.zip

unzip 3.2.0.zip

### 为了方便起见，以上解压的文件夹改名字为 opencv 和 opencv_contrib，命令省略
```

** Step 4. Build and install **

```
/opt$ cd opencv
 
/opt/opencv$ mkdir release
 
/opt/opencv$ cd release
 
/opt/opencv/release$ cmake -D BUILD_TIFF=ON -D WITH_CUDA=OFF -D ENABLE_AVX=OFF -D WITH_OPENGL=OFF -D WITH_OPENCL=OFF -D WITH_IPP=OFF -D WITH_TBB=ON -D BUILD_TBB=ON -D WITH_EIGEN=OFF -D WITH_V4L=OFF -D WITH_VTK=OFF -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=/opt/opencv_contrib/modules /opt/opencv/
 
/opt/opencv/release$ make -j4
 
/opt/opencv/release$ make install
 
/opt/opencv/release$ ldconfig  # 更新动态链接库

(可用如下命令查看动态链接库中是否有了 opencv ): 

ldconfig -p | grep opencv  

-p 是print

/opt/opencv/release$ exit
 
/opt/opencv/release$ cd ~
```

** Step 5. Check **

```
$ pkg-config --modversion opencv

### 结果应为
3.2.x
```

#### 卸载 opencv

进入前边的 release 文件夹，也就是之前 make 命令作用的文件夹。

刚才用了命令`make install`，卸载只需要 `make uninstall`。

然后删掉所有的 opencv 和 opencv_contrib 文件夹即可。

### vector 操作
- 基本语法：

```c++
#include <iostream>
#include <vector>  //关键点1：Need to include the vector library! 这也是c++标准库中的一部分

int main ()
{
  //creating a vector of integers
  std::vector<int> vectorInts;   // 关键点2： <×××> 指定 vector 中元素类型
  std::cout<<"vectorInts has "<<vectorInts.size()<<" elements\n";  // 关键点3： .size() 函数可以返回vector长度
  
  //Changing the size of vectorInts to 6
  vectorInts.resize(6);  // 关键点4： .resize() 函数可以重设vector长度
  std::cout<<"\n\nvectorInts now has "<<vectorInts.size()<<" elements\n";
 
  return 0;
}
```
- iterator和assign

```c++
#include <iostream>
#include <vector>

int main ()
{
  //creating a vector of integers
  std::vector<int> vectorInts;  
  //creating an iterator for the vector
  std::vector<int>::iterator it;  // 关键点1： iterator 类型变量用来存放 vector 中元素的location
  
  std::cout<<"vectorInts has "<<vectorInts.size()<<" elements\n";
  
  std::cout<<"\n\nAdding four elements to the vector\n";
  //assigning the value 3 to 4 elements of the vector
  vectorInts.assign(4,3);    // 关键点2： assign 在 vectorInts 中存放 4 个 值为 3 的元素
  std::cout<<"vectorInts has "<<vectorInts.size()<<" elements\n";
  
  //printing the contents of vectorInts
  std::cout<<"VectorInts has these elements:\n";
  for (it = vectorInts.begin(); it != vectorInts.end(); ++it)
    std::cout<<*it<<" ";    // 关键点3： 如此读取 vector 中的元素

  return 0;
}
```
- assign 会完全重写 vector，用 push_back则是从后边加入

```c++
vectorInts.push_back(24);
vectorInts.push_back(25);
vectorInts.push_back(26);
vectorInts.push_back(27);
```

- insert 可以在指定位置之前插入，例如：

```c++
it  = vectorInts.begin() + 1;
vectorInts.insert(it, -1);
```
    emplace也具有类似的功能，不过底层实现过程更高效一些。

```c++
it  = vectorInts.begin() + 1;
vectorInts.emplace(it, -1);
```
- clear 清除所有元素 `vectorInts.clear();`
- erase 清楚特定元素 `vectorInts.erase(vectorInts.begin()+4);` 或指定范围内的元素 `vectorInts.erase(vectorInts.begin()+1, vectorInts.begin()+3);`

#### 实现 vector  运算更好的库 Eigen



### 类 (class)

#### class 与普通数据结构 (data structure) 的关系

class 可以看作是带有 function 的数据结构，另外还可以设定变量的 accessibility 。

在定义类的对象时，也可以看成是定义某一数据结构的变量，例如：

```c++
int main()
{
    int integer1;     // 定义 int type 的变量
    float float1;     // 定义 float type 的变量
    Student student1; // 定义 Student 类的对象，对比上边两行，形式上很相似
    
    integer1 = 4; //assign a value to integer1
    float1 = 4.333; //assign a value to float1

    student1.setName("Catherine Gamboa"); //assign a value to the student name
    student1.setId(54345); //assign a value to the student id number
    student1.setGradDate(2017); //assign a value to the student grad date
}
```

对于普通的数据类型，如果想定义array，可以用

```c++
float num[3][2];
```

同样地，自定义的 class 也可以定义成 array 的形式：

```c++
Student student[10]; //定义一个Student类型的array，名为 student，里面包含10个元素，每个元素都是Student类型的
student[0].setId(1000);
student[1].setId(1111);
student[2].setId(2222);
...
```

#### public，protected, private 类型

class 中的变量和函数统称为 member，每个 member 都有一种 access specifier，设定了变量能在什么范围内可以被获取。一般格式如下：

```c++
class class_name {
  access_specifier_1:
    member1;
  access_specifier_2:
    member2;
  ...
} ;
```

access specifier 有 3 种：

- public：只要 object 可见，其 public 的变量就可见
- protected： 同 class 中其他 member 可见，derived class 也可见
- private：只有同 class 中其他 member 可见。

类中的 member 默认是 private 的，即只要没有其他额外声明，定义的 member 都是 private 的，要想定义其他类型，必须特别声明。例如：

```c++
class Rectangle {
    int width, height;
  public:
    void set_values (int,int);
    int area (void);
}
```



为了获取这些 private data，可以通过类中的函数，并且是public的函数。例如为了设置类中的data数值以及获取其中的数据，用如下形式的类函数

```c++
class Student  //convention: 首字母大写，如果是多个词，则用 StudentName形式
{
        // declare members
        // convention: 先写private members，如果把private members放在public后边再定义，则需要额外声明是private 
        string name;
        int id;
        int gradDate;

    public:
        // declare functions
        void setName(string nameIn);
        void setId(int idIn);
        void setGradDate(int dateIn);
        string getName();
        int getId();
        int getGradDate();
        void print();
};  // 别忘了大括号外边的这个分号！
```



#### 类中函数的声明与具体定义

类中的函数可以只声明，在类外边再定义，**注意别忘了前缀class的名字**，例如：

```c++
class Student
{
        string name;
        int id;
        int gradDate;

    public:
        void setName(string nameIn);
        void setId(int idIn);
        void setGradDate(int dateIn);
        string getName();
        int getId();
        int getGradDate();
        void print();
};

void Student::setName(string nameIn)
{
     name = nameIn;
}

void Student::setId(int idIn)
{
     id = idIn;
}
. . .
    
```

#### constructor
- 类的function中有一个比较特别的，叫做 constructor，每次创建类的对象时执行，declare如下：

```c++
class Cats
{
    string name;
    string breed; 
    int age;
public:
    Cats(); //declaring the constructor，与类同名，没有任何数据类型，连void都没有
    void setName(string nameIn);
    void setBreed(string breedIn);
    void setAge(int ageIn);
    string getName();
    string getBreed();
    int getAge();
    void printInfo();
};
```
- 类中可以有**多个constructor**，对应不同的情形，这本质上就是用到了overloading方式，如下：

```c++
class Dog
{
    private: //这个 private 的声明应该是不需要的
        int license;
    public:
        Dog();
        Dog(int licenseIn);
        void setLicense(int licenseIn);
        int getLicense();
};

Dog::Dog()  // 如果单纯的用 Dog dog1;这种方式生成对象，则执行该constructor
{
   license = 0;
}

Dog::Dog(int licenseIn)  // 如果用 Dog dog1(123)，则用该constructor
{
   license = licenseIn; 
}
```
- declare之后用如下方式定义：

```c++
Cats::Cats()   // define constructor，与普通函数比起来，没有返回数据类型
{
    cout<<"Assigning inital values in the constructor\n";
    name = "Unknown";
    breed = "Unknown"; //the initial value of the breed
    age = 99; //the initial value of the age
}
void Cats::setName(string nameIn)
{
    name = nameIn;
}

void Cats::setBreed(string breedIn)
{
    breed = breedIn;
}
```

#### deconstructor
当类产生的对象不再需要时，如main函数执行完了，则可以用deconstructor释放内存。declare和define的方式与constructor完全相同，但是在前边多了`~`，例如：
```c++
class Dog
{
    private:
        int license;
    public:
        Dog();
        Dog(int licenseIn);
        void setLicense(int licenseIn);
        int getLicense();
        ~Dog();
};

Dog::Dog()
{
   license = 0;
}

Dog::~Dog()
{
    cout<<"\nDeleting the dog";  //当该类的对象执行完所有任务后，被删除时会显示 `Deleting the dog`
}
Dog::Dog(int licenseIn)
{
   license = licenseIn; 
}
```

#### 重载 overloading
- 普通函数的重载：可以用同样的函数名，只要保证参数不同即可，例如参数type不同，参数数目不同，都可以
- 运算符的重载：例如

```c++
class Shape 
{
    private:
      int length;     // Length of a box
      int width;
      
    public:
      // Constructor definition
      Shape(int l = 2, int w = 2) 
      {
         length = l;
         width = w;
      }
		
      double Area() 
      {
         return length * width;
      }
      
      int operator + (Shape shapeIn)   // 定义运算符的重载，必须有operator这个关键字
      {
          return Area() + shapeIn.Area();  // 这里如果要获取shapeIn的length信息，一般不能直接用shapeIn.length，而要定义一个public函数getLength，然后调用 shapeIn.getLength()
      }
};

int main(void) 
{
   Shape sh1(4, 4);    // Declare shape1
   Shape sh2(2, 6);    // Declare shape2
   
   int total = sh1 + sh2;  // 重载的运算符使用方法
   cout << "\nsh1.Area() = " << sh1.Area();
   cout << "\nsh2.Area() = " << sh2.Area();
   cout << "\nTotal = "<<total;
   return 0;
}

```

#### 模板 template 与 泛型函数 generic function
- overloading 可以让我们用同一个函数名对不同的参数进行操作，但是还是要事先设定好参数类型，对于int要写一个函数，对于float还要写一个函数。
- template则更进一步，可以不事先设定数据类型，用抽象的T来代替。此时函数称为generic function。例如：

```c++
#include<iostream>

using namespace std;

//Our generic function
template <typename T>  //tell the compiler we are using a template
T findSmaller(T input1,T  input2);  // 每次用到 generic function 时一定要在前边指明我们用 template

int main()
{
    int a = 54; 
    int b = 89;
    float f1 = 7.8;
    float f2 = 9.1;
    char c1 = 'f';
    char c2 = 'h';
    string s1 = "Hello";
    string s2 = "Bots are fun";
    
    //Wow! We can use one function for different variable types
    cout<<"\nIntegers compared: "<<findSmaller(a,b);
    cout<<"\nFloats compared: "<<findSmaller(f1,f2);
    cout<<"\nChars compared: "<<findSmaller(c1,c2);
    cout<<"\nStrings compared: "<<findSmaller(s1,s2);   
    return 0;
}

template <typename T>
T findSmaller(T  input1,T  input2)   // 定义 generic function 时也要指明我们在用 template
{
    if(input1 < input2)
        return input1;
    else
        return input2;
}
```

- 实际上， 在指明了 T 是 template 之后，我们就可以将 T 当成普通的 type 来使用了，例如

```c++
template <typename T>
T sumTwo(T  a,T  b)
{
    //Note  that the sum is a variable type T. This means whatever 
    //variable type is passed in
   //will become the variable type for sum.
    T  sum; // 定一个 T 类型的变量，存储两个 T 类型变量的和
    sum = a + b;
    return sum;
}
```
- 前边只涉及到一种 template type T，也可以用不同的 type，例如：

```c++
template <typename T, typename U>
T getBigger(T input1, U input2);  //可以接受两个不同type的变量，返回结果与第一个变量的类型相同。

int main()
{
    int a = 5;
    float b = 6.334;
    int bigger;
    cout<<"Between "<<a<<" and "<<b<<" "<<getBigger(a,b)<<" is bigger.\n";

    cout<<"Between "<<a<<" and "<<b<<" "<<getBigger(b,a)<<" is bigger.\n";    
    return 0;
}

template <typename T, typename U>
T getBigger(T input1, U input2)
{
    if(input1 > input2)
        return input1;
    return input2;
}
```

#### template 与 class
- 在class中也可以用 template，例如：

```c++
//tell compiler this class uses a generic value
template <class T>
class StudentRecord
{
    private:
        const int size = 5;
        T grade;
        int studentId;
    public:
        StudentRecord(T input);
        void setId(int idIn);
        void printGrades();
};

template<class T>   // 注意：如果class用到了template，则所有function前面都要指明template，哪怕该函数中没有用到template
StudentRecord<T>::StudentRecord(T input)
{
    grade=input;
}

template<class T>
void StudentRecord<T>::setId(int idIn)  // 注意：在class名字的后边要加一个<T>
{
    studentId = idIn;
}

template<class T>
void StudentRecord<T>::printGrades()
{
    cout<<"ID# "<<studentId<<": ";
    cout<<grade<<"\n ";
    cout<<"\n";
}
```

- 在具体调用 class时，要指明具体的type，例如：
```c++
int main()
{
    //StudentRecord is the generic class
    //The constructor sets the grade value
    StudentRecord<int> srInt(3);  //创建一个对象，里面的template类型为int，其他方法与普通class相同
    srInt.setId(111111);
    srInt.printGrades();
 
    StudentRecord<char> srChar('B');
    srChar.setId(222222);
    srChar.printGrades();

    StudentRecord<float> srFloat(3.333);
    srFloat.setId(333333);
    srFloat.printGrades();
    
    StudentRecord<string> srString("B-");
    srString.setId(4444);
    srString.printGrades();
    
    return 0;
}
```

#### 继承 inheritance 
- public 方式： 假如已经有个一个class Student:
```c++
class Student
{
    private:
        int id;
    public:
        void setId(int idIn);
        int getId();
        Student();
        void setId();
};
```

在此基础上构造一个GradStudent类，常见语法:
```c++
class GradStudent : public Student //public意味着原本Student里面的所有public函数都被继承了，默认放在了public系列中。
{
    private:
        string degree;
    public:
        GradStudent();
        void setDegree(string degreeIn);
        string getDegree();
};

```
在生成对象 GradStudent gs1 后，可以用 gs1.getId 获得 student ID. 

- private 方式：

```c++
class GradStudent : private Student  // 用private方式继承，此时base class中的public 函数不会出现在 derived class中，如果要引用base class中的函数，需要另外构造函数去调用，然后设置成 public 的
{
    private:
        string degree;
    public:
        GradStudent();
        void setDegree(string degreeIn);
        string getDegree();
        void setStudentId(int idIn);
        int getStudentId();
};

GradStudent::GradStudent()
{
    degree = "undelcared";
}

int GradStudent::getStudentId()
{
    return Student::getId();
}
```

#### 多继承 multiple inheritance
可以从多个已有的类继承，包含了各基础类中所有变量和函数，例如：

```c++
class Staff
{
    private:
        string title;
    public:
        Staff();
        void setTitle(string input);
        string getTitle();
};

class GradStudent
{
    private: 
        int studentId;
    public:
        GradStudent();
        void setId(int input);
        int getId();
    
};

class TA: public Staff, public GradStudent  // 多继承
{
    private:
        string supervisor;
    public:
        TA();
        void setSupervisor(string input);
        string getSupervisor();
};

int main()
{
    TA t1;   // TA 的对象中包含了 Staff 和 GradStudent 中的变量与函数
    t1.setSupervisor("Dr. Caohuu");
    t1.setId(55555);
    t1.setTitle("Adjunct Prof.");
    cout<<t1.getSupervisor()<<" "<<t1.getId()<<" "<<t1.getTitle();
    return 0;
}
```







### 变量可见范围

- 如果是在所有函数之外定义的函数，则全局可见。例如在Class中定义的memebers，对该Class中所有的函数都可见。
- 如果是在某一函数内部定义的函数，仅在此函数内可见，连它调用的子函数都不可见，例如在main中定义的变量，在被调用的add函数中不可见

```c++
#include <iostream>

using namespace std;

void modifyNum(int Num);

int b = 10; //在函数之外定义的变量，全局可见

int main()
{
    int a = 54; //在main函数内部定义的变量，仅main内部可见，对其调用的函数modifyNum也是不可见
    modifyNum(9);
    cout << "\nb " << b;
    return 0;
}


void modifyNum(int Num)
{
    b = Num;
}
```



### Preprocessor directives 预处理指令

一般是以 # 开头的语句，他们不属于编译器解析的语句，而是编译之前的预处理指令。它早于所有的 c++ 语法检查。

这些预处理指令默认以行为单位，只要该行结束，本条预处理指令就结束。末尾不需要加分号表示指令结束。

如果需要用到多行，则需要在末尾用 `\` 表示指令未结束。

常见的预处理指令包括如下几种：

- 头文件调用
- 宏定义 macro definition (#define, #undef)
- 条件编译

#### 头文件调用

通过 `#include` 命令实现。

例如 c++ 文件开头最常见的头文件调用 

```
#include <header>
#include "file" 
```

这种调用实际上就是单纯把指定的文件内容原封不动的加载进来。

关于 #include 的详细介绍，可以参考 “关于 include 头文件调用” 小节。

#### 宏定义/替换

基本格式如下：

```c++
#define identifier replacement
```

预处理器在编译开始之前，对变量做替换，而且只是**对该 #define 指令之后的部分做替换，如果遇到 #undef 则结束替换**。

预处理器不理解 C++ 语法规则，仅仅是简单替换。

##### 常数替换

例如

```c++
#define TABLE_SIZE 100
int table1[TABLE_SIZE];
int table2[TABLE_SIZE];
```

经过预处理器处理之后，程序等价于

```c++
int table1[100];
int table2[100];
```

当使用 #undef 时

```c++
#define TABLE_SIZE 100
int table1[TABLE_SIZE];
#undef TABLE_SIZE
#define TABLE_SIZE 200
int table2[TABLE_SIZE];
```

程序等价于：

```c++
int table1[100];
int table2[200];
```

##### 函数替换

例如

```c++
// function macro
#include <iostream>
using namespace std;

#define hi(a,b) ((a)>(b)?(a):(b))

int main()
{
  int x=5, y;
  y= hi(x,2);
  cout << y << endl;
  cout << hi(7,x) << endl;
  return 0;
}
```

output:

```c++
5
7
```

函数替换还可以接受两个特殊符号 `#`  和 `##`。

例如 `#`的使用

```c++
#define hi(x) #x   // 将 hi(*) 用 * 替换掉，而且是 * 的字符串形式
cout << hi(test);
```

等价于

```c++
cout << "test";
```

`##`的使用

```c++
#define hi(a,b) a ## b  // 将 hi(*, *)用两个参数替换掉，参数之间不留空格，不转化成字符串
hi(c,out) << "test";
```

等价于

```c++
cout << "test";
```

注意：宏定义可能会影响程序的可读性，因为看到的程序可能会被替换成另外的语句，尤其是函数宏，一般把函数的名字设定的比较有意义，反映出宏替换的效果。比如把上边第一个 hi(a,b) 换成 getmax(a,b)，把第二个 hi(x) 换成 str(x)，第三个 hi(a,b) 换成 glue(a,b)。

#### 条件编译

根据条件在编译时包含或者舍弃某些程序语句。

主要判断语句有 `#ifdef, #ifndef, #if, #endif, #else, #elif`

##### `#ifdef`

例如

```c++
#ifdef TABLE_SIZE
int table[TABLE_SIZE];
#endif 
```

中间的语句只有在 TABLE_SIZE 通过 #define 定义之后才会包含在随后的编译中。

##### `#ifndef`

与前边正好相反，例如

```c++
#ifndef TABLE_SIZE
#define TABLE_SIZE 100
#endif
int table[TABLE_SIZE];
```

如果没有定义 TABLE_SIZE 就定义它且赋值为 100

如果已经定义了，就用原来的值，不做任何变动。

##### `#if, #else, #elif`

条件判断现有 constant expression 是否成立

例如

```c++
#if TABLE_SIZE>200
#undef TABLE_SIZE
#define TABLE_SIZE 200   // 超过 200 就设置为 200
 
#elif TABLE_SIZE<50
#undef TABLE_SIZE
#define TABLE_SIZE 50   // 小于 50 就设置为 50
 
#else
#undef TABLE_SIZE
#define TABLE_SIZE 100   // 在两者之间的就设置为 100
#endif     
 
int table[TABLE_SIZE];
```







- 固定变量：如果希望程序中的变量值固定不变（通常用大写的变量名），有两种方法
  - const 类型:  `const int WEIGHT = 10;` 。这样 WEIGHT 的值在整个作用范围内都不能改变，这也意味着必须在变量声明的时候就赋值。
  - 预处理： `#define WEIGHT 10`

  它们的主要区别是，预处理是编译之前对变量做替换，将所有的 WEIGHT 替换为 10。而 const 则是被编译系统识别的，可以参与编译过程。

- 静态变量：static 类型的变量可以在程序生命周期内一直存在，而不是每次调用和离开时创建和销毁变量。这样一个结果就是每次调用程序，该变量会沿用之前的值。例如
```cpp
void Func(void)
{
	static int i = 5;
    i++;
}
```
上述程序中，每次调用 Func 函数，变量 *i* 并不是重新赋值 5, 而是沿用之前的值，每调用一次，值就会增加 1. 也就是说，static 变量只接收一次声明和初始化，以后的声明和初始化均被忽略！
- 识别符：在 C++ 中，识别符（identifier）是一种统称，它包括变量名、函数名、类名、module 名等。所有识别符的命名规则都是相同的，也就是变量名的命名规则。
- 全局变量：在函数内部定义的是局部变量，在所有函数外部定义的是全局变量。
  全局变量对它之后的函数可见，因此，如果希望全局变量在整个程序中起作用，一定要定义在最前边。

- 不同文件之间调用变量和函数： 用 extern，例如
```cpp
# main.cpp
int count;  
extern void write_extern(void); # 声明一个其他文件中的函数
int main()
{
	count = 5;
    write_extern();
}
```
```cpp
# support.cpp
extern int count;  # 声明一个其他文件中的变量
void write_extern(void)
{cout << count << endl;}
```
以上两个文件显示 support.cpp调用 main.cpp 的变量 count，进行自己函数 write_extern的定义，而 main.cpp 则反过来调用 support.cpp 的函数 write_extern()
编译的时候要把两个文件编译在一起：`g++ main.cpp support.cpp -o write`

- 声明函数：可以在声明函数的时候紧接着定义函数，这是最常见的一种定义函数的情况，也可以先声明，以后再定义。如果是后一种情况，则在声明的时候，参数名称可以先不写，只写类型，例如 `int max(int, int)`

- 指针的值，不论指向 int, char, double . . .，本质上都是一个地址，是一个十六进制数 `0x????????`

### 文件读写操作
```c++
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main () {
    string line;
    //create an output stream to write to the file
    //append the new lines to the end of the file
    ofstream myfileI ("input.txt", ios::app);
    if (myfileI.is_open())
    {
        myfileI << "\nI am adding a line.\n";
        myfileI << "I am adding another line.\n";
        myfileI.close();
    }
    else cout << "Unable to open file for writing";
  
    //create an input stream to write to the file
    ifstream myfileO ("input.txt");
    if (myfileO.is_open())
    {
        while ( getline (myfileO,line) )
        {
            cout << line << '\n';
        }
        myfileO.close();
    }
    
    else cout << "Unable to open file for reading";
    
    return 0;
}
```



### C++ 编译

#### 基本编译流程

- 编译的基本过程： 编译.cpp源文件得到目标文件.obj，一个.cpp对应一个.obj。一个.exe文件可能来自多个cpp源文件及对应的多个obj文件。目标文件之间可能有依赖关系，可能互相依赖，也可能依赖系统的library。将这些目标文件和依赖的文件通过连接器都link起来，得到最后的可执行文件.exe。

  ![cppCompile2](pic/cppCompile2.png)

  或者

  ![link](pic/cppCompile.png)

  

  也可以再细分如下：

  ![GCC_CompilationProcess](./pic/GCC_CompilationProcess.png)

  上述详细的编译流程依次为 预处理 -- 编译 -- 汇编 -- 链接：

  1. Pre-processing: via the GNU C Preprocessor (cpp.exe), which includes the headers (#include) and expands the macros (#define).

     ```
     > cpp hello.c > hello.i   # 这本身还是源码形式
     ```

     The resultant intermediate file "hello.i" contains the expanded source code.

  2. Compilation: The compiler compiles the pre-processed source code into assembly code for a specific processor.

     ```
     > gcc -S hello.i   # 这里生成了汇编语言形式
     ```

     The `-S` option specifies to produce assembly code, instead of object code. The resultant assembly file is "hello.s".

  3. Assembly: The assembler (as.exe) converts the assembly code into machine code in the object file "hello.o".

     ```
     > as -o hello.o hello.s    # 这里从汇编语言生成了机器码，这个机器码才是目标文件
     ```

  4. Linker: Finally, the linker (ld.exe) links the object code with the library code to produce an executable file "hello.exe".

     ```
     > ld -o hello.exe hello.o ...libraries...   # 目标文件 + 库文件 = 可执行文件
     						   				    # 目标文件已经是机器码了，经过 linking 之后 											# 生成可执行的机器码
     ```




- 最简单的情况： 假设已有 .cpp 源文件，则用如下命令编译，可直接通过**编译**和**链接**得到可执行文件：
  `g++  源文件  -o  可执行文件`
  其中，-o 后边跟可执行文件的名字，上述命令也可以写成
  `g++ -o 可执行文件  源文件`
  如果不指定输出文件的名字，则默认生成 a.out 可执行文件。
  
- 也可以将**编译**和**链接**分开，先编译，得到 obj 目标文件，再得到最后的可执行文件，分解动作如下

  ```bash
  // Compile-only with -c option
  > g++ -c Hello.cpp
  
  // Link object file(s) into an executable
  > g++ -o Hello Hello.o
  ```

  这种分步编译的方式在编译多个源文件时很有用。

  例如程序包含两个源文件 file1.cpp 和 file2.cpp，可以一次性编译，直接得到可执行文件

  ```bash
  g++ -o myprog.exe file1.cpp file2.cpp 
  ```

  也可以分别生成目标文件，最后一起生成可执行文件

  ```bash
  g++ -c file1.cpp
  g++ -c file2.cpp
  g++ -o myprog.exe file1.o file2.o
  ```

  这样分步的好处是，当一个源文件改动时，只需要重新编译它自己，生成新的目标文件即可，其他源文件不需要重新编译。



#### library 库

##### 分类

库是预编译的目标文件的集合，可以在 linking 阶段与自己的程序链接。常用的库文件例如 `prinft()` 和 `sqrt()`

库可以分成两类

- **static library 静态库**：Unix-like 系统中扩展名为 `.a` (archive file)，Windows 系统中扩展名为 `.lib` (library)。当自己的程序与静态库链接时，库文件被复制到最终的可执行文件中。要创建一个静态库，可以用 archive 程序 `ar.exe`。

- **shared library 共享库**：扩展名为 `.so` (shared objects) in Unix-like system，或者 `.dll` (dynamic link library) 在 Windows 中。在链接阶段，只在最终的可执行文件生成一个 table。在执行时，系统才加载具体的机器码，这被成为 dynamic linking。

  用共享库的好处

  - 节省硬盘存储空间：可执行文件较小，因为一个库可以被多个程序共享
  - 节省内存空间：大部分操作系统都允许共享载入内存的共享库 (在内存层面，而不仅仅是文件层面)。
  - 不需要重新编译：由于可执行文件中仅包含共享库的 table，而不是具体的内容，因此在共享库更新时，只要 table 没有问题，不需要重新编译可以执行文件。

  正是由于 dynamic linking 这些优点，GCC 在编译时如果发现有 shared library 可用，就首选 shared library.

##### 搜索和调用

在整个编译过程中，除了程序本身，还需要涉及到外部的头文件 (编译阶段) 和 library (linking 阶段) 。

需要一些设定才能让编译器找到这些外部文件。

- header files ： 对于每一个头文件，编译器要查找 include-paths，可以参考本文第一个条目的介绍。搜索路径可以通过参数设置 `-Idir` 中间不需要有空格。由于头文件的名字已经在程序中给除了，所以不需要再给出文件名，只需要给出搜索路径即可。
- library files ： 在 Linking 阶段需要查找外部 library，此时也要给出搜索路径，通过命令 `-Ldir`，另外还要指明 library name。在 Unix-like 系统中，如果要用 `libxxx.a`，则指定 `-lxxx` 。在 windows 中，给出全名如`-lxxx.lib`。

通过如下命令可以查看 header file 的搜索路径

```
cpp -v     # cpp 即 GNU C Preprocessor

Using built-in specs.
COLLECT_GCC=cpp
Target: x86_64-linux-gnu
Configured with: ...
Thread model: posix
gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.11) 
COLLECT_GCC_OPTIONS=...
#include "..." search starts here:
#include <...> search starts here:
 /usr/lib/gcc/x86_64-linux-gnu/5/include
 /usr/local/include
 /usr/lib/gcc/x86_64-linux-gnu/5/include-fixed
 /usr/include/x86_64-linux-gnu
 /usr/include
End of search list.
```

在编译文件是可以用 verbose 模式，显示 library 路径和 link 的 library 名字

```
g++ -v -o hello  hello.cpp

...
End of search list.
GNU C++ (Ubuntu 5.4.0-6ubuntu1~16.04.11) version 5.4.0 20160609 (x86_64-linux-gnu)
	compiled by GNU C version 5.4.0 20160609, GMP version 6.1.0, MPFR version 3.1.4, MPC version 1.0.3
GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
Compiler executable checksum: 8c2b43f572cd3f060e41aeab70254763
COLLECT_GCC_OPTIONS=...
COMPILER_PATH=...
LIBRARY_PATH=/usr/lib/gcc/x86_64-linux-gnu/5/:/usr/lib/gcc/x86_64-linux-gnu/5/../../../x86_64-linux-gnu/:...
COLLECT_GCC_OPTIONS=... -L/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/5/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/5/../../.. /tmp/cch7UNDT.o -lstdc++ -lm -lgcc_s -lgcc -lc -lgcc_s -lgcc /usr/lib/gcc/x86_64-linux-gnu/5/crtend.o /usr/lib/gcc/x86_64-linux-gnu/5/../../../x86_64-linux-gnu/crtn.o
```

其中用到了几个 library 

- -lm == libm.a
- -lgcc_s == libgcc_s.a
- -lc == libc.a



与 GCC 搜索路径相关的三个环境变量

1. PATH：搜索可执行文件和 shared libraries
2. CPAH：搜索头文件，这一路径会在 `-Idir` 路径之后被搜索，也可以明确指定 C 和 C++ 的搜索路径 C_INCLUDE_PATH 和 CPLUS_INCLUDE_PATH
3. LIBARARY_PATH：搜索 libraries，其中的路径在 `-Ldir` 之后被搜索



#### 两种编译体系

GNU C++ 体系下：编译器为 g++， debugger 为 gdb

LLVM C++ 体系下：编译器为 clang++，debugger 为 lldb

实际上，这两个 debugger gdb 和 lldb 可以在任意一种编译器中使用，并不是对立、不相通的，甚至这个两 debugger 的大部分命令也是相同的。



#### GCC 简介

首先要注意，这个 GCC 不是 gcc，gcc 只是 GCC 套装中的一个组件。

原本 GCC 表示 GNU C Compilter，由 GNU 开源项目（1984年启动）的发起人 Richard Stallman 开发。

随着时间的推移，GCC 包含了越来越多语言的编译器，例如 

- C (gcc)
- C++ (g++)
- Objective-C
- Objective-C++
- Java (gcj)
- Fortran (gfortran)
- Ada (gnat)
- Go (gccgo) 等。

现在 GCC 表示 GNU Compiler Collection，是一个编译器的套装，包含上述各类语言的编译器，最新版本是 GCC 7.3，于 2018.01.25 发布。GCC 是大部分 Unix-like 平台的默认编译器。

GCC 本身属于 GNU Toolchain 的一部分，这个 Toolchain 的主要功能就是面向软件开发的，包括以下组成部分

- GCC 编译器套装
- GNU Make 方便编译的自动化工具
- GNU Binutils 这是二进制工具套装，包括 linker 和 assembler 
- GNU Debugger —— GDB
- GNU Autotools 自动化编译系统包括 Autoconf, Autoheader, Automake, Libtool
- GNU Bison 一个  parser generator 解析生成器



GCC 的版本发展：

- 1987 version 1：仅支持 C
- 1992 version 2：支持 C++
- 2001 version 3
- 2005 version 4
- 2015 version 5
- 2016 version 6
- 2017 version 7



目前 C++ 的标准如下：

- C++98
- C++11 (即 C++0x)
- C++14 (即 C++1y)
- C++17 (即 C++1z)
- C++2a (预计 2020 年发布)

在 6.1 版本之前 GCC 默认的 C++ 标准是 C++98，6.1 版本及以后默认是 C++14.



GCC 中 C 语言编译器称为 gcc， C++ 语言的编译器称为 g++



可以通过 `gcc --version `或者 `g++ --version`查看本机的 GCC 版本，例如

```bash
$ gcc --version

gcc (Ubuntu 5.4.0-6ubuntu1~16.04.11) 5.4.0 20160609
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

$ g++ --version

g++ (Ubuntu 5.4.0-6ubuntu1~16.04.11) 5.4.0 20160609
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

也可以通过 `gcc/g++ -v` 查看更多信息

```bash
$ gcc -v

Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/5/lto-wrapper
Target: x86_64-linux-gnu
Configured with: ...
Thread model: posix
gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.11) 

$ g++ -v

Using built-in specs.
COLLECT_GCC=g++
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/5/lto-wrapper
Target: x86_64-linux-gnu
Configured with: ...
Thread model: posix
gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.11) 
```



在编译时可以强制指定 C++ 标准，例如

```
-std=c++98

-std=gnu++98  (这是带有 GNU 扩展的 c++98 )
```



#### 常见编译参数

- `-c` ：只编译，例如 `g++ -c hello.cpp`，然后要生成可执行文件，就对编译之后的目标文件 `hello.o`进行操作: `g++ -o hello hello.o`

  这里单纯的从目标文件到可执行文件GCC 调用单独的linker ——  ld.exe 来实现 link 操作。

- `-o`：后跟输出的可执行文件

- `-v`：显示详细的编译过程 `g++ -v -o hello hello.cpp` 会显示整个编译流程。



#### 常用编译工具

- `file` 可以查看文件类型

  ```
  $ g++ -c hello.cpp 
  hello.cpp  hello.o
  
  $ g++ -o hello hello.o
  hello  hello.cpp  hello.o
  
  $ file hello.cpp 
  hello.cpp: C source, ASCII text
  
  $ file hello.o
  hello.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
  
  $ file hello
  hello: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/l, for GNU/Linux 2.6.32, BuildID[sha1]=ed63cdb1e281291a945a89190e2a89f672dcf0d1, not stripped
  acronis@acronis-G501VW:~/compile_C++$ 
  ```

- `nm` 常用来查看某个目标文件中是否定义了某函数，只能用在机器码文件中，如目标文件，可执行文件

  ```
  $ nm hello.o 
                   U __cxa_atexit
                   U __dso_handle
  0000000000000065 t _GLOBAL__sub_I_main
  0000000000000000 T main
  0000000000000027 t _Z41__static_initialization_and_destruction_0ii
                   U _ZNSolsEPFRSoS_E
                   U _ZNSt8ios_base4InitC1Ev
                   U _ZNSt8ios_base4InitD1Ev
                   U _ZSt4cout
                   U _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
  0000000000000000 b _ZStL8__ioinit
                   U _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
                   
  
  $ nm hello
  0000000000601060 B __bss_start
  0000000000601170 b completed.7594
                   U __cxa_atexit@@GLIBC_2.2.5
  0000000000601050 D __data_start
  0000000000601050 W data_start
  0000000000400780 t deregister_tm_clones
  0000000000400800 t __do_global_dtors_aux
  0000000000600e08 t __do_global_dtors_aux_fini_array_entry
  0000000000601058 D __dso_handle
  0000000000600e18 d _DYNAMIC
  0000000000601060 D _edata
  0000000000601178 B _end
  0000000000400934 T _fini
  0000000000400820 t frame_dummy
  ...
  ```

  其中 `T` 表示本文件中定义了该函数，`U` 表示没有定义，需要 Linker 从外部调用。

- `ldd` 列出可执行文件中需要用到的 shared libraries，只适用于可执行文件。

  ```
  $ ldd hello
  
  	linux-vdso.so.1 =>  (0x00007ffe9b717000)
  	libstdc++.so.6 => /usr/lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f97d6754000)
  	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f97d638a000)
  	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f97d6081000)
  	/lib64/ld-linux-x86-64.so.2 (0x00007f97d6ad6000)
  	libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f97d5e6b000)
  ```



#### make 命令与 Makefile 文件

`make` 本身与前边的 `file`, `nm` 等一样，都是编译常用工具，它可以使编译过程更加自动化，批量化。

`make` 需要用到 makefile/Makefile 文件，其中包含了编译的配置。

```makefile
all: hello.exe
hello.exe: hello.o
	g++ -o hello.exe hello.o
hello.o: hello.cpp
	g++ -c hello.cpp
clean:
	rm hello.o hello.exe
```

运行上述 makefile 文件

```
$ make

# 显示
g++ -c hello.cpp
g++ -o hello.exe hello.o

# 生成 hello.o 和 hello.exe 文件
```



makefile 文件中基本结构为

```
target: pre-req-1 pre-req-2
	command
```

其中 command 前边必须是 TAB ，不能用空格。

默认执行 `all`，有如果有需要，执行命令，生成它的 req，一步一步的生成，最终完成 all 的执行。



在时间方面，如果某个 pre-req 并没有更新过，即 pre-req 比 target 时间早，则不执行命令。

例如连续两次执行 make

```
$ make
g++ -c hello.cpp
g++ -o hello.exe hello.o

$ make
make: Nothing to be done for 'all'.
```

由于文件没有更新，所以也不再重新编译。



可以指定要执行的 target

```
$ make clean
rm hello.o hello.exe
```



makefile 的书写可以非常复杂，它有自己的一套体系，比较常用的是

- $@ 表征目标文件
- $< 表征第一个 pre-req 文件

前述 makefile 也可以写成

```makefile
all: hello.exe
hello.exe: hello.o
	g++ -o $@ $<
hello.o: hello.cpp
	g++ -c $<
clean:
	rm hello.o hello.exe
```



#### cmake 编译方法

1. 首先写好 .cpp 源程序，例如 helloworld.cpp
2. 编写CMakeLists.txt 文件，最简单的情况如下：
```
cmake_minimum_required(VERSION 2.8)
project(hello)
add_executable(hello  helloworld.cpp)
```
3. 想在什么文件夹下产生 build file 就进入那个文件夹，运行
    `cmake  file_path`
    其中 file_path 为含有 .cpp 和 CMakeList.txt 的文件夹路径
    如果就在当前文件夹中，则用命令 `cmake .`

  为了整洁，一般建立一个 build 文件夹，进入其中，运行 cmake 命令。
  **cmake 命令会产生一个 Makefile 文件**

4. 在 Makefile 的文件夹中运行 make 命令，即可生成可执行文件 hello





### 函数定义
- 不一定非要有大括号，如果只有一句话，可以不加大括号，例如：

```c++
int findSmallerInt(int input1, int input2)
{
    if(input1<input2)
        return input1;   // 不加大括号。 另外，只要遇到return，函数就返回，结束。
    return input2;
}
```

### enum 类型变量

```c++
#include <iostream>
using namespace std;

enum week { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday };
# 该类型变量中包含 7 个可选项，默认以此对应 0, 1, ..., 6 个整数

int main()
{
    week today;
    today = Wednesday;
    cout << "Day " << today+1;
    return 0;
}
```

output:

```c++
Day 4
```

也可以不用默认数值，自己赋值：

```c++
#include <iostream>
using namespace std;

enum seasons { spring = 34, summer = 4, autumn = 9, winter = 32};

int main() {

    seasons s;

    s = summer;
    cout << "Summer = " << s << endl;

    return 0;
}
```

output:

```c++
Summer = 4
```

### 内存分配与 new, delete 

#### 内存分配的 3 种方式

c++ 中有三种内存分配方式：

1. static memory allocation: 对于 static 和 global variable，内存只分配一次，并一直存在，直到程序结束。
2. automatic memory allocation: 对于局部变量和函数变量，只有当程序运行到该区域才分配内存，离开之后内存就释放。
3. dynamic memory allocation

前两者都需要在编译时就知道需要分配的内存大小。

在有些情况下，这有些困难，例如：变量中存放用户输入的数据，到底多长才能满足要求。太小可能会报错，太大了会浪费内存空间。而且大多数变量是存放在内存中的一个区域叫做 stack，一般来说这是一个很小的空间，在 visual studio 中默认 stack size 为 1MB。例如在 VS 中，编译下边简单的程序就会有问题：

 ```c++
int main()
{
    int array[1000000]; // 保存 1 million 整数，大概需要 4MB 内存，超过默认值 1MB
}
 ```

幸好我们有 dynamic memory allocation

- 它不需要在编译的时候提前分配内存，只在需要的时候请求系统分配
- 从操作系统管理的 heap 中分配内存，而不是 stack，heap 的容量是操作系统实际内存相当。

#### 如何实现 dynamic memory allocation

一般请求动态内存的方式如下：

```c++
int *ptr = new int;  // 分配一个 int 的内存空间，将其地址存在指针ptr 中
*ptr = 7; // 将值存入内存空间中

// 或者直接初始化
int *ptr = new int (7);
```

只要系统允许了内存分配的请求，程序就可以随心所欲的使用那一部分内存了。

既然是向系统申请内存，在很极端的情况下可能系统没有内存可以分配了，这个时候就会报错 bad_alloc。

为了应对这种情况，可以设定如果没有可用内存，就返回空指针

```c++
int *value = new (std::nothrow) int;  
```

此时即使没有可用内存，也不会报错了。要么得到了实际内存的地址，要么得到空指针。如果更严谨一些，要判断是哪一种情况，然后进行下一步的动作。

```c++
int *str = new (std::nothrow) int;
if (!str)   
{
    // do error handling
    std::cout << "Could not allocate memory!";
}
```

#### 如何归还内存

与 static、automatic 分配方式不同，在内存使用完毕的时候，程序有责任主动告诉 c++ 内存使用完毕，可以交还了，系统不会自动回收。交还内存方式如下：

```c++
delete ptr;    // 交还 ptr 指向的内存空间，此时系统可以把内存分配给其他程序
ptr = nullptr;  // 将 ptr 设定为空指针
```

delete 只是说明系统有权利再次分配该内存了。在绝大多数情况下，在交还内存的时候，其中仍然保留这使用过的数据，而 ptr 也依然指向该内存，此时的 ptr 称为 dangling pointer。

所以安全起见，应该设置 ptr 为空指针，彻底切断联系。

注意：只有当指针指向动态内存时才可以 delete，否则可能会出问题。

#### memory leak 内存丢失

如果程序不主动归还内存，系统会一直认为内存在使用中，不会允许其他程序使用，直到程序主动归还或者程序结束。

如果程序自己不小心丢掉了内存地址，那么这一部分内存无法主动归还，别的程序也不能使用。这种情况称为 memory leak.

造成 memory leak 的几种常见原因

-  在函数中定义指针指向动态内存，函数调用完成，指针变量不可见了，pointer out of scope。

  ```c++
  void doSomething()
  {
      int *ptr = new int;
  }
  ```

  一定要在函数内部加上 delete 命令。

- 除了上述 pointer out of scope，比较常见的还有把 ptr 再次指向其他变量，前边的地址就丢失了

  ```c++
  int value = 5;
  int *ptr = new int;
  ptr = &value;  // ptr 之前保存的内容丢失
  ```

  为了避免这种情况，一定要在重新赋值之前，delete 一下

  ```c++
  int value = 5;
  int *ptr = new int;
  delete ptr;
  ptr = &value;
  ```

  另一个常见错误是把 ptr 重新指向新的动态内存

  ```c++
  int *ptr = new int;
  ptr = new int;  // old addess lost
  ```

#### 对 array 进行操作

static 和  automatic array 都需要在编译时指定 array 的长度。

dynamic array 则不需要。例如，可以在用户运行程序时手动输入：

```c++
#include <iostream>
 
int main()
{
    std::cout << "Enter a positive integer: ";
    int length;
    std::cin >> length;
 
    int *array = new int[length]; // use array new.  Note that length does not need to be constant!
 
    array[0] = 5; 
 
    delete[] array; // use array delete to deallocate array
 
    // we don't need to set array to nullptr/0 here because it's going to go out of scope immediately after this anyway
 
    return 0;
}
```

上述程序中，array 的长度就是用户指定的。

要生成一个 dynamic array，需要用 `new[]` ，尽管书写时 `[]`并没有紧跟在 new 的后边，删除用 `delete[]`。

要生成一个元素全为 0 的 array

```c++
int *array = new int[5]();
```

如果要在定义时赋初值，可以用下边的形式

```c++
int *array = new int[5] {9, 3, 1, 5, 4};
```

注意：对于 static 和 automatic array，定义的时候可以不指明长度，由赋的值推测出来

```c++
int fixedArray[] = {9, 8, 3, 1, 4};
```

但是对于 dynamic array，必须指明长度。下边的例子是错误的

```c++
int *dynamicArray = new int[] {3, 1, 6, 4, 7};
```



### 常见错误

- statement结束之后一定要加分号 ;
- if 后边要用（ ）把表达式括起来
- 定义class时，最后要在{ }后边加分号 ;
- 在calss外定义函数时，一定要在函数前边前缀class的名字，如 Student::getGrade()

