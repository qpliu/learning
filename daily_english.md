## 读音比较

- cash 与 cache 读音完全相同 [kæʃ]

##### concern

- concerned做形容词，意思为：有关的，放在描述对象的后边 sth concerned

 People concerned have been invited here to discuss what to do next. （有关人士）

 You may skip through a book, reading only those passages concerned. （有关段落）

 I’m sending you the book along with the material concerned. （书和有关材料）


- be concerned表示关心、关注（concerned with + 名词; concerned + 句子）

  In the preceding chapters we have been concerned primarily with analysis.

  As far as I am concerned, 就我所知

  I am very concerned how we could……我很关心……

  My partner was more concerned with profit

- concerning用法与about 相同

  He heard nothing concerning this matter.
  
  I have a question concerning the Nobel Prize for Peace. 

##### a lot of
后边既可以跟可数名词，也可以跟不可数名词。但一般不用在否定句中。

- a lot of money

##### doubt + 被怀疑的事情
- I doubt they would go to your wedding. 
- I doubt the statement of the government.
- I highly doubt that

##### a+动词
一般a开头的动词都是做表语，例如 asleep, alive, awake.

##### maximum 的复数
maximum 的复数形式是maxima /mæksimə/

    They are the local maxima of the potential function.

类似的词还有

    optimal 最优的，形容词； optimum 最优结果，名词； optima 复数。

##### 谚语与俗语
- saying, proverb 都是谚语，用法如下：
  - As the proverb /'prɔvə:b/ goes /ɡəus/,  actions speak louder than words.
  - There is an old saying, "...". It's not a great saying.
- know sth/sb like the back of my hand 很了解某事或某人
  - I know her like the back of my hand.

##### A differs from B

    A differs from B in serval respects.

#####  过来 come over 

    wait for her to come over
    
    come over here

##### should 在句首
有时见到should 在句首，但整个句子却不是疑问句，意思是“如果”，用于条件句，表示事情发生的可能性

    Should it rain tomorrow, the meeting would be put off.

##### would 的用法
1. 用在假想的、不太可能出现的情况中
   - What would you do if you were attacked by a shark? (would + if + 过去时)
   - Answer: I'd (i.e., would) probably try to hit it on the nose.
   - What would you do if you could read people's thoughts?
   - I would help you if I could.
   - I would study English every day, but I don't have time
   - If I won the lottery, I would ...  
   - If she was here right now, would you tell her?
   - 总结：常用框架就是在一个 imagary situation, I would do something.
   - If 过去式， sb would do sth  
   - 也可以稍微变化一下，不加 if 假想的情况，而是直接说跟假想对立的情况
   - She would be here, (if she had time,) but she has too much work at the moment.
2. 对已经发生的事情假想另一种可能，这更是不可能出现的，would 跟现在完成时 ，if 跟过去完成时
  - What would you have said if she'd (i.e., had) seen you
  - I wouldn't have come if I'd known it was going to go on so late
  - 也可以不带 if，后边就不用过去完成时，用过是就行
  - I would have gone out with my friends, but I was swamped with work
  - He wouldn't have been much help, anyway


3. 在 request 中，为了更加 polite，这与之前的虚拟语境完全无关
  - Can you tell me the time? --> Would you mind telling me the time?
  - Will you get me a sandwich? --> Would you get me a sandwich?
  - **I want something to eat. --> I'd like something to eat.**

4. 在叙述过去发生的事情时，用 would 替代原文中的 will
  - “I'll be on time. I promise! I'll get an earlier bus, which will get to the certer by 3.”
  - My friend promised she'd be on time. She said she'd get an earlier but, which she said would arrive by 3. Of course, when she turned up it was nearly 4!

5. will 和 would 都用在表示 refuse to do sth (本可以做，但是不愿意做)
  - I‘ll ask him, but he won't help me. (= he will refuse to help me) 未来的事情
  - I asked him, but he wouldn't help me. (= he refused to help me) 已经发生的事情
  - They wouldn't give me my money back, even though I had the receipt. (refused to give back)
  - My car would't start this morning.(=refuse to start)
  - When he tried to show me, the file wouldn't open.

6. 过去经常做的事情，现在不做了 (repeated action in the past, 必须是 action)
  - My mum would walk us to school when we were little.
  - When I was training for the marathon, I would go running for two hours every day after work.
  - I would go out with my friends most evenings when I was a student.
  - Wrong: I would live in a small flat (not a repeated action) with my friends when I was a student.

# Email related

## An example of invitation letter

```
Dear All, # "All" should be in upper case.

Your are cordinally invited to our upcoming *** event titled *** on 24 September 2018, 10 am - 12 pm.

You can refer to the attached announcement for further details. 

Feel free to forward this announcement to colleagues who have research interest in ***

We look forward to your participation!

For any enquiries, please contact *** at qpliu@ntu.edu.sg.
or
Any queries please write to tedxntu@gmail.com

Thank you.

Best Regards,
****
```

## An example letter of preparing a meeting

```
Dear Prof Wang, Prof Liu and Prof Lyu

Unfortunately we are unable to get the majority's availability on 11 Oct. Hence we would like to revert back to 24 Sep.

Can Prof Wang and Prof Lyu confirm your availability?
Prof Liu has confirmed previously he was available, is that still the case?

Thank you all. Sorry for the inconvenience.
Regards
Ariel
```



#####  interest 

    I'm interested in sth.
    
    sth interests me.

##### respectable 体面的，正派的

    a highly respectable neighbourhood  体面的社区
    
    a respectable married man  正派的已婚男子
    
    Go and make yourself look respectable 去把自己弄得体面点

##### worry

- 作动词用： 
  I worry that
  Don't worry
  He is always worrying about his weight
- 形容词，或者被动形态
  I'm worried 
  He is worried about

##### exclaim  由于强烈的感情而呼喊、惊叫

    ‘It isn't fair!’, he exclaimed angrily. （可以有话语）
    
    She opened her eyes and exclaimed in delight at the scene. (可以单纯的叫出声)

##### concerned 形容词

- 担心的，忧虑的

  I was surprised and concerned.

  Concerned parents held a meeting.

  The President is deeply concerned about this issue.

  She was concerned that she might miss the turning and get lost

- 感兴趣的，关注的

  They were more concerned with how the other women had dressed

##### agreeable 令人喜欢的

    We spent a most agreeable day together.
    
    He seemed extremely agreeable.
    
    此外，还有字面意思，可同意的，可接受的
    
    The deal must be agreeable to both sides

##### (dis)approve

- 当表示同意时，一般是vi, 后跟 of

 Do you approve of my idea?

 She doesn't approve of me leaving school this year.


- 当表示比较正式的批准审核时，用vt

 The committee unanimously approved the plan.

 The course is approved by the Department for Education

##### no wonder == now I understand why

no wonder 可以i单用，后边什么都不加，表示 ”原来如此“

A: This lamp doesn't work.

B: You didn't plug it in!    插上电 plug it in    无需插电 requiring no plug-in 

A: No wonder it doesn't work.

A: I'm tired!

B: Why?

A: We made 143 deliveries   送货 make delivery 

B: No wonder you're tired!




##### 相近词对比
- possible,  probable, likely 
  - possible的可能性最小, 指客观上有可能，往往含有“希望很小”的意味。
  - probable可能性最大,表示“很可能，十有八九”。  
  - likely表示外表迹象表明的有可能。
  - likely常用于以下两种结构中：
    - sb / sth. is likely to do sth.
      - He is likely to come. 他可能要来。
      - It isn’t likely to rain. 不大像要下雨。
    - It is likely + that 
      - It is likely that he will succeed. 他有可能成功。
  - possible  常用于以下结构中：
    - It is possible (for sb.) to do sth.
      - It is possible for the train to be late.
    - It is possible that…
      - It is possible that the train is late. 火车有可能晚点。
  - probable用法：同样不用人做主语。只用于下列句型
    - It’s probable that clause…
      - It is probable that it will rain today. Bring your umbrella with you. 今天极有可能要下雨。带上雨伞吧。
    
  - 注意：possible,与probable都一般不用人做主语。
    - 例如：我们可以说：
      - It is possible for him to lend us the car.
      - It is possible /probable that he will lend us the car.
    - 但是我们不说：
      - He is possible / probable to lend us the car.

- much more 与 many more
  - much more…than后接形容词或不可数名词，much more water, much more beautiful
  - many more…than后接可数名词 many more people

- talk to 与 talk with
  - talk to sb 意思是对某人讲话,侧重于表示一个人对另一个人讲话,但是不表示对方也需要讲话,主要是强调告诉某人什么事， 当别人插嘴的时候，可以说：我没跟你说话！ I'm not talking to you.
  - talk with sb 则表达的是和某人一起谈论什么事物,与某人一起交谈,双方都要发表言论,相互沟通

- lovely 可爱美丽  vs  lively 活泼活力
  - a lovely yound lady  美丽的女孩
  - an intelligent and lively young woman 聪明活泼的女孩

- program 与 programme
  - program和 programme 两者发音完全相同。
  - 在美式英语中只有program
  - 在英式英语中，跟计算机程序、编程相关的环境都用program，其他的环境，如计划，表单等都用programme.

- tall 与 high
  - tall 常指人或动物.  He is tall
  - high常指物体. High mountain

- ill 与 sick 
  - ill只做表语  I am ill.  
  - Sick可以做定语和表语  a sick boy.

- no 与 not any
  - 经常困惑用there exists no agent 还是 there does not exist any agent。实际上都可以，因为
  no=not a/any，如  no friend = not a/any friend,   no water = not any water

- toward 与 towards
  - 美式英语多用toward，英式英语多用towards (可能出于美国人实用的角度)
  - 与之类似的还有其他以ward或wards结尾的词，如backward/backwards, forward/forwards

- live on 与 live by
  - Live on 以……为主食  live on fish
  - Live by 靠……谋生   live by fishing.

- at the end 与 in the end
  - "At the end" is mainly physical 
    - at the end of the page
    - at the end of the road (although one can also say at the end of the day).
  - "In the end" translates as finally, eventually. 
    - In the end, after hours of discussion, we decided to compromise.

- change for 与 change into
  - Change for 调换   change the shirt for a bigger one. 
  - Change into 变成   water changes into ices.

- join 与 join in
  - join 一般指加入某个组织并成为其中一员  
    - join the army
    - join us
    - join our party (加入某一party 政党)
  - join in 参加活动    
    - join in our party (这里是参见party聚会，显然不是要成为聚会的一部分)

- sound 与 voice
  - Sound 可以指各种声音
  - Voice 指人的嗓音。

- pleasant 与 pleased
  - Pleasant 常作定语  a pleasant trip.   
  - Pleased 常作表语，且主语为人  I am pleased with sth.

- work 与 job
  - Work 不可数  What interesting work it is!
  - job 可数   a good job.

- police 与 policeman
  - police 指警察全体，后接复数形式。
  - 若想用单数，则用 policeman\policewoman

- next year 与 the next year
  - next year 指以现在为基准的未来时间，it means the year after this one (the one that is actually going on now).
  - the next year指任意时态中的未来时间，多用做过去时态中的未来时间
    - He said he would go abroad the next year.
    - Due to poor harvests and problems with the Indians, it was not possible to celebrate Thanksgiving the next year.

- lie 与 lay
  - lie 主要有两个意思：撒谎和躺下
  - lie /lai/ 作撒谎的过去式时，lied读音不变 /laid/，动名词lying /'laiiŋ/，说谎的人liar/laiə/, 总之，有“撒谎”意思的词，一律是“lai……”的音。
  - lie /lai/ 作躺下时，过去式和过去分词与撒谎都不同，过去式 lay /lei/, 过去分词 lain
  - 另外，单词 lay 有放置的意思，过去是和过去分词都是 laid 
  - 注意lie (躺下) 和 lay (放置) 的区别
    - lie the book on the table $\times$ 
    - lay the book on the table $\checkmark$
    - lay down on the couch $\times$
    - lie down on the couch $\checkmark$

- inherent 与 inherit
  - inherent /in'hiərənt/ 天生的，固有的，内在的. 中间部分读音就是here 
  - inherit /in'herit/  动词：继承 

- dynamic 与 dynamical
  - dynamic 是动态的，如果说 dynamic network，意思就是网络结构是变化的。
  - dynamical 是动力学的，dynamical network，就是说网络中包含有动力学的分析对象。

- opposite, oppose, opponent
  - opposite /'ɔpəzit/名词，形容词（上来就重读！！！）对立面；对立的（强调与之相反的）
    1. opposite of sth。 The Polya urn model is the “opposite” of the model of ……
    2. the opposite (代词)  The opposite is true.
  - oppose \ə'poz\ 动词，反对
  - opponent \ə'pəunənt\ 名词，形容词，（重读在次位置）对手；敌对的 (强调敌意)
  - 对手还可以说rival： Sounds like you get a rival /'raivəl/.

- execution 与 executive
  - execution /,eksi'kju:ʃən/ 名词：执行，死刑
  - executive /iɡ'zekjutiv/ 执行的，执行者（前半部分读音完全相同，可是读音差这么多）

- fast 与 quickly
  - fast常指人或物体运动速度快，是看得见摸得着的很形象的快。  Run fast 
  - quickly则主要指事情完成的快，较为抽象。  Answer the question quickly.

- call on, call up
  - 拜访 call on someone.  I am going to call on one of my former classmates.
  - 号召 call on someone to do sth.  I call on everyone to renounce the use of violence and armed struggle.
  - 给谁打电话 call up
    - When I'm in Pittsburgh, I call him up.
    - call up Daisy
    - call up everyday  

- specific 与 specified

  - specific 具体的，更加具体些

    He said he wanted dessert. I told him "Be more specific. Do you want apple pie, chocolate cake or vanilla pudding?"

    The lawyer wanted to know the specifics of the case before it went to trial.

    I think it is this specific dress you don't like. You have other dresses you like to wear.

  - specified 作为动词的过去时用，或者形容词指定的

    He specified that he did not want apple pie.

- motive 与 motivation
  - Motives are the specific reasons for performing a specific action. The negative connotation /kɔnəu'teiʃən/(内涵) attached to motive is probably because it is often used in a TV/film crime situation to understand why a crime is committed or who might want to do it。
    - I killed Brian to get revenge on him for cheating with my wife. (motive revenge)
  - Motivation is more what drives you, at a deeper level, to 'want' to do a certain action, or more commonly a longer lasting project or job.
    - I became a vet so that I could help relieve suffering in animals (motivation: to relieve suffering in animals) （论文中一般用motivation）

- decorate 与 furnish
  - decorate /'dekəreit/ vt装饰  decoration /dekə'reiʃən/ n 装饰
  - furnish /'fə:niʃ/vt 主要指家具方面的装饰，名词 furniture。家具商furnisher

- varied 与 various
  - 都是多样的意思，如果用来做表语，就是varied，如果是定语，就用various.
  - The reasons are varied.
  - Uncleanliness is the root of various diseases.

- concept 与 conception

  - 概念一般用concept即可，conception 多指怀孕，也可以是新思想。

- wish (祝愿)与 hope (希望)
  - hope 只有两种句型： hope (that) ， hope to
    - I hope you will come to China again.
    - They hope to achieve their goal by peaceful means.
  - wish 句型更多样，可以接名词、代词、不定式或that从句（用虚拟语气），可以接宾语再加不定式，但不能接动名词。
    - I wish you to come to China again.

- verify 证实 与 falsify 证伪

  - The new tools allow experiments to be conducted to verify or falsify hypotheses.

- Ltd 公司 与 Inc 公司
  - Ltd. = Limited （有限的）有限责任公司
  - Inc. = Incorporated （合并的）股份有限公司
  - 两者的主要相似与区别：
    - 相似：都是有限责任公司，只以对公司的投入作为责任，不能连带个人其他财产。
    - 区别：有限责任公司规模小于50人，股份有限公司人数无限制。

- different than 与 different from 

  - different than这种用法是美式的，在美语中可以接受，但是英语中或者更标准的用法还是different from

- decline 与 refuse
  - decline 婉拒（ politely），refuse 断然拒绝（strongly）
  - 婉拒的场景：如果同事邀请你吃饭，但是你有其他事情要做，就要说 I'm sorry, but I will have to decline.
  - 断然拒绝的场景：如果别人以超低的报价想收购你们公司，你们感觉受到侮辱 I am afraid we will have to refuse.

- specified 与 specific 
  - 你指定的 You have specified /'spesifaid/ 
  - 你能说的更具体/明确点么？Could you be more specific?   She declined  /di'klain/ to be more specific. 

- to date 与 up to date
  - to date: 副词，迄今为止，到目前为止。 To date, more than 500 million Barbie Dolls have been sold.
  - up to date 是形容词，最新的。  Everything is up to date. It is an up to date invention.

##### query

- 问，等同于 ask

    ‘Who will be leading the team?’ queried Simon.

- 质疑

    We queried the bill as it seemed far too high.

    I'm not in a position to query their decision.

##### entertain

- A entertains B: A逗B开心

 He entertained us for hours with his stories and jokes

- 招待:

  Barbecues are a favourite way of entertaining friends


##### pastime 消遣，娱乐
    Playing badminton is a popular pastime

##### prospective 潜在的，未来的，预期的
- Prospective employers like to see what you have written. 
- The story should act as a warning to other prospective buyers.

##### nevertheless
- 副词： The display was not as spectacular as the fireworks celebrating the king's marriage, but it was impressive nevertheless.（做副词，居然可以与but同时出现）
- 连词： The display was not as spectacular as the fireworks celebrating the king's marriage. Nevertheless, it was impressive.

##### 没有成功 have no success with doing sth
     She has no success with guessing his reasons.

##### 不知不觉地，不明真相地 unknowingly
    People unknowingly move into more contaminated areas of the river.
    
    Art often imitates nature unknowingly

##### separate A from B
    The spirit would be separated from the body.  
    The Lord separated himself from mankind. 
    Man would be separated forever from God.

##### thus
1. 如果后边要跟完整的句子，必须跟and连用
 - They never change and thus I can trust them.
 - She misinterpreted the implications of his letter and thus misunderstood his intentions
2. 后边可以跟动名词成分
 - Agriculture has developed rapidly, thus providing light industry with ample raw materials.

##### for the sake
- for the sake 为了什么的缘故  

      for the sake of her health 为了她健康着想   
      for the truth's sake 为了真理的缘故
  
- forsake 抛弃 My God, you forsakes me.
- sake还有日本清酒的意思，读音变为\sa:ki\


##### manifest
- 形容词、副词：明显的，显然的

      the manifest failure of the policies.
      She manifestly failed to ……

- 及物动词：把……表现出来

      She manifested a pleasing personality on stage.
      The virus needs two weeks to manifest itself.

##### shed 
过去式同形，使流出；车棚 (常见搭配，流泪、流血)

    The lamb shed his blood in place of the firstborn.
    Put your bike under the shed when it rains.
    Crocodiles often appear to shed tears.

##### hostile
敌对的，有敌意的 hostile /'hɔstail/ 读音跟host不同，后边跟file相同

##### bet
I bet this clothes looks great on you. 我相信，我敢打赌

##### 感激 be grateful to sb / for sth / to do sth
- to sb: I am extremely grateful to all the teachers for their help.
- for sth: We would be grateful for any information you can give us.
- to do sth: She seems to think I should be grateful to have a job at all.

##### esteem
/i'sti:m/ 尊重，认可
1. 名词：Modern readers have little esteem for the dramatists. 
  - 自尊 self-esteem /'selfis'ti:m/  
  - 社会认可 social esteem
2. -ed 形容词：The dramatists are little esteemed today.
 - an esteemed group 受人尊敬的

##### 几组反义词 antonyms /'æntəunim/
often---seldom    defend---attack   

<img src="pic/antonym.png"  width=700  align="center">

##### at length  最终，详尽地
- 最终：At length my father went into the house.
- 详尽地：They spoke at length, reviewing the entire incident.

##### hang
- hang out 四处逛逛，出去逛
- hang up 挂断电话 
- hang on 打电话时，让对方等一下 Hold on please, just a second.

##### hurry vt,vi动词，名词
- vi: She jumped from the carriage and hurried into the hall.

      Hurry up! We are going to be late.
- vt: I don't want to hurry you, but we close in twenty minutes.
- n: Take your time. There's no hurry.

##### 英语与美语的差别
1. 部分以re结尾的词在美语中以er结尾，如: 
metre/meter, centimetre/centimeter, theatre/theater, centre/center等等
2. 英语部分our在美语中的拼写为or，如：
colour/color, favourite/favorite等等。
3. 英语中的-ise动词在美语中拼写为-ize，如：
organise/organize, actualise/actualize, realise/realize, 这些词的衍生也因此而异： organisation/organization

##### 看的分类
- 目不转睛的看stare at. 比如被漂亮女孩子吸引 Don’t stare at me like that.
- 凝视gaze.有意味深长的意思。He gazed into my face and said I will never let you go.
- peek和peep同样是偷窥，区别是peek是本人在明处，比如两人在同一间房里换衣服，可以告诉对方不准偷看；peep则是本人躲在暗处的偷窥。
- 门上的窥视孔就叫做peephole

##### de facto
实际上的，事实上的 de facto /di:'fæktəu/ (来自法语，读音很标准) 

It becomes a de facto standard.（事实上的标准）

##### 美国警察的分类
- 联邦调查局：Federal Bureau /'bjuərəu/ of 	Investigation (FBI)
- 州立警察队：State Patrol /pə'trəul/
- 每个城市：City Police.
- 每个county（相当于县）的警察队：Sheriff

##### you 的古英语
- thou /ðau/ you
- thee /ði:,/ you的宾格
- Thy /ðai/  your    Thy gifts    
- thine /θain/ yours 你的，你的东西

##### per se
perse /pə:'si:/ 本身  

    This is not a control text per se.

##### daunting  
/dɔ:nting/ 令人畏惧的，令人却步的: This question is more daunting than last one.

##### 表达压力
- 感到压力I am stressed.
- 不想让你感到压力 I don’t want to put any pressure on you.

##### 理发店的红、白、蓝三色
理发店 barber shop， 标志是红、白、蓝三色，因为早期理发店还可以做些小手术，比如放血，放血时，病人要用力握一根棍子，让血管暴露。为了掩盖棍子上的血迹，所以把棍子做成红色，另外，手术用的白色绷带也常跟棍子放在一起，就有了红白两色，后来，美国人将蓝色加进去，表示国旗的颜色。

##### 国家、民族有关的
- 波兰
  - 波兰 Poland
  - 波兰的，波兰人 Polish
  - 波兰人也可以用 the Poles
- 芬兰
  - Finnish /'finiʃ / 芬兰人，芬兰的。读音与finish完全一样
  - Finland 芬兰
- 希腊
  - Greece 希腊
  - Greek  n. 希腊人，希腊语 adj. 希腊的，希腊人的，希腊语的
- 德国
  - Germany 德国
  - German n. 德国人，德语  adj. 德国的，德国人的，德语的
    

从以上领悟与国家相关单词的构词规则: 
  1. 国家是一个词
  2. 该国的人以及该国家的形容词是同一个词

- 国歌 national anthem 'ænθəm
- 英国人习惯将教名和中间名全部缩写，如M. H. Thatcher； 美国人则习惯只缩写中间名，如Ronald W. Reagan.

##### 不同的笑
- 咯咯地笑，轻轻的笑 chuckle /'tʃʌkl/
- 窃喜，偷偷地笑 snicker /'snikə/

##### 排队
- 在不确定被人是否在排队时，说 Are you in the line?
- 看到别人插队时，说 I am sorry. You cut the line.
- 想插队怎么说呢？ May I jump in for ……?

##### 植物相关
- grain 谷物，其中的一种是 wheat /wit/ 小麦 (不可数)
  - Since early Roman times some grain——usually wheat——has been associated with ……
- grain 也有颗粒的意思： grains of wheat 
- whole-wheat flour（全麦面粉）包含了麦子的一切，即麸、糠、籽粒等。
- white, refined flour 只是由籽粒磨成的。
- 丝瓜 loofah /'lu:fə/
- veggie/'vedʒi/在口语中是个好东西，不同的语境下可以有不同的意思，既可以指蔬菜（vegetable），又可以指素食的(vegetarian)，还可以指素食主义者(vegetarian/,vedʒi'tεəriən/)。例如My girl friend is a veggie, so we only go to veggie restaurants
- vegan /'vedʒən/ 绝对素食主义者，严守素食主义的人

##### 人体相关
- 当说一个人身体状况时，一定要一人做主语，这是英语文化的特点。
  - You have a strong body
  - You are very healthy
  - I have a sore throat. 
  - 玛丽两只耳朵都聋了 Mary is deaf in both ears.
  - 腿疼 I hurt in my leg.
![headache](pic/headache.png)
- 双手交叉在胸前 with arms folded across his chest.
- 胸毛 chest hair 
- 太阳穴 temple，更常见的意思是：庙宇
- 眉毛eyebrow /'aibrau/
- The skin sinks into a hollow between the high cheekbone and the jaw.
- 睫毛eyelash /'ailæʃ/, lash是鞭子、鞭打的意思，读音跟flash后边完全相同。
- 酒窝 dimple /dimpl / ： You got a cute dimple in your cheek. 与此只有一字只差的 pimple意思是”丘疹，青春痘”, 挤青春痘就是squeeze the pimple.
- 五指：thumb /θʌm/, index finger/forefinger, middle finger, ring finger, little finger
- 子宫，医学术语是 uterus /'ju:tərəs/  普通用词是 womb /wu:m/ 
- 阴道 vagina /və'dʒainə/
- 阴茎在医学上正规叫法：penis /'pi:nis/
- 阴囊 scrotum /'skrəutəm/ 
- 睾丸 testicle \'testikl\
- 口语中一般说：dick / prick, 同时也都是可以用来骂人的
- 各种“屎”
  - 鼻屎booger 'buɡə挖鼻屎 pick one’s nose
  - 耳屎earwax 'iəwæks挖耳屎 pick one’s ear. Your ears are full of earwax.
  - 眼屎没有专有名词，可以说eye discharge. You have to wash your face to clean the eye discharge.
- 心跳 heart jumping in my chest
   - I could hear my own heart jumping in my chest (箱子；胸腔). 

##### 饮食相关
- 餐厅的分类
  - 通称是restaurant
  - 细分有deli /'deli/（熟食店，熟食品）：供应三明治、沙拉等不需要自己在烹饪就可以吃的。这里的熟食跟国内说的熟食不同，不是那种肉食品。
- 常见的饮料
  - coke
  - sprite /spraɪt/
  - iced tea
  - lemonade /lemə'neid/ 柠檬汽水
  - just water
- 喝茶与喝汤
  - 描述这两种液体的形容词不同，浓茶是strong tea，浓汤是thick soup
  - 动词不同，喝茶drink tea，喝汤 eat soup(可能是因为老外不是一饮而尽的喝，而是像吃饭一样用勺一点一点的喝。)
- 牛排几分熟
  - raw 生的
  - rare 三分熟
  - medium-rare 四分熟
  - medium 五分熟
  - medium-well 七分熟
  - well-done 全熟
- 荷包蛋怎么煎，几分熟
  - sunny-side-up 太阳蛋，底部蛋白，上面蛋黄
  - over-light 两面都煎，三分熟
  - over-easy 两面都煎，四分熟
  - over-medium 两面都煎，五分熟
  - over-hard 煎到全熟
- under + 形容词或动词表示“不足，不符合标准” over+形容词或动词就是“过头”
  - under-cooked 火候不足的  over-cooked 蒸煮过度的
  - under-feed 喂食过少  over-feed 摄食过多
  - under-paid 报酬过低的 over-paid 报酬过高的
- 火候问题
  - 做熟一点 The steak is still bloody inside. Maybe we should cook it through.
  - 熟过了 overcooked
  - 烤焦了 burned 。Turn down the fire. The steak is burned.
- bake 与 broil
  - bake是通过包围食物的热空气，是食物烘熟（应该是上下加热同时）。温度可选，因为食物周围的热空气温度是可变的，可以100度也可300度。bake不会把食物烤焦，多用来烤饼干
  - broil类似碳烤，通过红外线（类似明火提供的温度）把食物烤熟，相当于把炭烤架中的木炭火放到了食物的下方。broil不可选温度，因为红外线的温度是不可选的。broil有可能会把食物烤焦、烤糊，多用来做烧烤食物比如牛排。
- 买单
  - 我请客： 
    - This one is on me
    - Next round is on me
    - My treat
    - Please let me buy you lunch 
  - 平均摊： Let’s split the check. 或者说 split the bill.
  - 自己付自己的那一份：Let’s go dutch \dʌtʃ\
- 西方食物
  - 提拉米苏 tiramisu /ti'rɑ:misju:/ 一种意大利奶油点心，从外观上看就是奶油蛋糕撒上可可粉。
  
  ![tiramisu](pic/tiramisu.png)
- 咖啡种类：
  1. Espresso意式特浓  
      Espresso是一个意大利单词，指的是一种用咖啡机在短时间内急速萃取的浓烈咖啡，几乎称得上是所有花式咖啡的基础，也是全世界咖啡馆的必备。Espresso很小杯，通常只有30毫升左右，味道很苦，表面浮着一层厚厚的油脂，会与一杯清水同上，可以选择加糖。在咖啡馆中点Espresso的客人，要么是真正的咖啡爱好者，想品尝到咖啡最本真的滋味；要么纯粹是因为困极了急需提神。想在咖啡馆闲适地打发一下午的人，就不太适合点这个哟。有些咖啡馆的Esprssso可以选择“Single单份”或者“Double双份”，双份可不是两杯的意思，而是指用双倍的咖啡粉萃取，会更苦。   
    
  2. Americano 美式咖啡    
   很多咖啡馆的“当日咖啡”其实就是美式咖啡，这通常也是咖啡馆菜单上最便宜的一种。美式咖啡说白了，就是小半杯Esprssso兑上大半杯白开水，也有咖啡馆会使用滴滤式咖啡壶冲泡。美式咖啡味道淡、颜色浅，微酸微苦，通常咖啡馆会提供牛奶和砂糖。如果服务生给的是奶精，不妨要求换成鲜牛奶。

  3. Caffé Latte 拿铁咖啡 
      拿铁是Espresso与牛奶的经典混合，杯底先倒入少量Espresso，然后加入大量牛奶，顶端是浓密的一层泡沫，技术好的咖啡师会在奶泡上拉出各种各样的图案。拿铁在中国咖啡馆中非常受欢迎，价格中等，性价比也高。好的拿铁咖啡味和奶味都很足，不太过关的则可能就像在喝一杯咖啡味儿的热牛奶了。经典的拿铁是70%牛奶+20%奶沫+10%咖啡
    
  4. 欧蕾咖啡  
   受法国人喜爱的欧蕾咖啡，是将牛奶和咖啡同时倒入杯中，两者在第一时间碰撞、混合，上加两勺打成泡沫的奶油，这在法国人的早餐中十分常见，几乎是国民饮料。

  5. 美式拿铁  
      就是在热牛奶上再加一些打成泡沫的冷牛奶。底部是意大利浓缩咖啡，中间是70度左右的牛奶，最上边是不超过一厘米的冷的牛奶泡沫。

  6. 冰拿铁  
      利用果糖和牛奶的混合增加牛奶的比重，使它跟比重较轻的咖啡不会混合，形成黑白分明的两层，形成如鸡尾酒般曼妙的效果，再加入冰块。
    
  7. Cappuccino 卡布奇诺  (**就是重奶泡版的 Latte**)  
      这种咖啡的特点——泡沫多。卡布奇诺和拿铁咖啡的成分一样，都是Espresso+牛奶+奶泡，不同之处仅在于卡布奇诺奶泡比牛奶多，拿铁则是牛奶比奶泡多。同等价位的卡布奇诺，通常比拿铁要小杯，但咖啡味更浓郁。卡布奇诺是意大利咖啡与牛奶的经典之作，咖啡的颜色就像卡布奇诺教会修士深褐色外衣上覆的头巾一样，咖啡因此得名。你可根据自己口味调整牛奶与咖啡的比例，牛奶味重称为湿卡布奇诺，咖啡味重称为干卡布奇诺。

  8. Caffé Mocha /'moʊkə/ 摩卡咖啡 (**巧克力味盖住了咖啡香味，太甜，我不喜欢！**)  
      摩卡的配方成分就相对比较复杂了，在Espresso和牛奶的基础上，还有巧克力酱，顶端不是奶泡，而是打发的鲜奶油，还往往会挤上巧克力酱，或者撒上可可粉、肉桂粉，别有一番风味。由于“内容丰富”，通常售价较高，也比较大杯。巧克力和奶油都有甜味，因此摩卡咖啡是苦甜结合的典范。

  9. Macchiato 玛奇朵  
      Macchiato又被很多咖啡馆翻译为“玛奇雅朵”，简单说就是往一杯Espresso里舀入几勺奶泡。咖啡味道依旧浓烈，还能享受到细腻绵密的奶泡。我们经常会听到“焦糖玛奇朵”，据说这是星巴克的独创，其做法是在牛奶中加入香草糖浆，与Espresso咖啡混合，再于奶泡上覆盖一层焦糖，口味层次很丰富。

- 咖啡功效
  1. 咖啡含有维他命B，烘焙后的咖啡豆含量更高。
  2. 咖啡对皮肤有益处。促进代谢机能，活络消化器官，对便秘有很大功效。使用咖啡粉洗澡是一种温热疗法，有减肥作用。
  3. 咖啡有解酒的功能。酒后喝咖啡，分解成水和二氧化碳而排出体外。
  4. 咖啡可以消除疲劳。
  5. 一日三杯咖啡可预防胆结石。最新美国哈佛大学研究人员发现，每天喝两到三杯咖啡的男性，得胆结石的机率低于40%。
  6. 常喝咖啡可防止放射线伤害。
  7. 咖啡的保健医疗功能。具有抗氧化及护心、强筋骨、利腰膝、开胃促食、消脂消积、利窍除湿、活血化淤、息风止痉等作用。
  8. 咖啡对情绪的影响力。实验表明，一般人一天吸收300毫克（约3杯煮泡咖啡）的咖啡因，对一个人的机警和情绪会带来良好的影响。
- 酒
  - 一般说的wine是指葡萄酒。加二氧化碳的葡萄酒最著名的是香槟。香槟之所以跟其他加二氧化碳的葡萄酒有区别是因为用来造香槟的葡萄一定来自法国的东北部地区champagne.
  - 白酒（烈酒）一般叫做liquor（美式）或者spirit（英式）
  - 下面列出常见的非葡萄酒：
    1. Gin (杜松子酒)  跟伏特加类似，也是低酒精浓度的酒。无色透明，粮食做成
    2. Vermouth (苦艾酒) 由加度葡萄酒制成的酒，味道强烈，一般用来做调料。
    5. Tequila(特奎拉)，就是龙舌兰做成的酒，一种墨西哥酒
    6. Kahlua(咖啡酒)，一种墨西哥酒，深棕色，味道像甜甜的咖啡
    7. Brady (白兰地)  记住：白兰地不是葡萄酒！
    8. Cognac，一种法国白兰地酒
    9. Armagnac，一种法国白兰地酒
    10. Whisky (威士忌) /'hwiski/ 也叫做 Scotch Whisky \ Scotch苏格兰威士忌，谷物制成。 scotch也可以引申为与谁一起喝酒 scotch with Barney.
    11. Bourbon，一种美国威士忌酒
    13. Vodka (伏尔加)，俄国酒 /'vɔdkə/  无色，粮食做成。是相对低酒精浓度的酒（light liquor）
    14. Liqueur，一种性烈甜酒，也叫 Cordial
    15. Schnapps，荷兰杜松子酒
    16. Daiquiris，得其利酒
  - 鸡尾酒是几种酒或者酒和饮料的混合物，对于喝酒刚刚入门的人来说，在酒吧要一杯cocktail是明智的。
    1. Martini，是Gin（杜松子酒）或伏特加与Vermouth（苦艾酒）的混合。通常加有橄榄或柠檬片。Martini也有很多种，chocolate martini, apple martini.
    2. Tequila /tə'ki:lə/ sunrise /'sʌnraiz/： Penny给Leonard调的酒就是这个。
    3. Black Russian==Vodka+kahlua
    4. White Russian==Black Russian+牛奶
    5. Greyhound==vodka+西柚汁（grapefruit juice）
    6。 Long island ice tea==gin+rum+tequila+vodka+柠檬汁
  - on the rock 是在酒里加冰。不加冰就是 straight up.  这种说法只是针对含酒精饮料。
  - 如果是软饮料，比如水，汽水之类的，就说 with ice，without ice.
  - 酒精度为8：  8 percent alcohol 
  - 酒吧侍者，调酒师 bartender /'bɑ:,tendə/  tender 意为看管人，更常见的意思是柔弱的(比如说肉嫩，就是tender)，
  - 托盘、杯垫 coaster
  - 调配，混合而成 concoct \kən'kɔkt\ ，  调和物  concoction \kən'kɔkʃən\
  - draft beer 生啤；扎啤
    - 一般情况下，不区分生啤和扎啤。生啤是相对于熟啤而言的。
    - 听装或瓶装的熟啤是经过巴斯高温消毒之后的啤酒，去除了大部分的酵母菌，适于长期保存。
    - 生啤则不经过巴斯高温消毒，仅通过微孔过滤除菌，会残留酵母菌，不宜长期保存，但是由于未经过加热，口感较清爽，而且酵母菌也有促消化的作用。
    - 另外，还有纯·生啤酒，则是生啤的一种，但过滤较严格，残留酵母菌极少，可长期保存。

- 燕麦 oat 与 燕麦片 oatmeal
  - Oats are the seeds of a grain, much the same as wheat. 
  - Oatmeal can refer to the product of cooking oats. However, for most people the definition is that it is the type of oats you buy to make the cooked cereal. 

| Oats | Oatmeal |
| :--- | :---  |
|Unprocessed oats | Oats processed for easier cooking |
|Whole or steel cut oats | Oats sliced thinly and rolled |
|Typically fed to livestock | Often pre-cooked for instant cereal |
- 山葵酱 wasabi  与 芥末酱 mustard 
  - 日本寿司中用的气味很冲的绿色膏状调料是 wasabi， 是由山葵根研磨成细泥。
  - 但是，真正的山葵酱挥发性比较强，不容易保存，日本的一些寿司店会给顾客一个山葵根茎和工具，自己边吃边研磨。

 <img src="pic/wasabi.png" width=300 align=center>

  - 而且山葵酱比较贵。国内的一些店实际上使用辣根（淡黄色）染成绿色之后包装成山葵酱的，二者味道很相似。
<img src="pic/lagen.png" width=300 align=center>

  - 另外常说的芥末酱 mustard 是芥菜的种子研磨而成的，味道相比与 wasabi 要轻很多。而且 mustard 很早就作为调料在世界范围内使用，例如涂到热狗上的，而 wasabi 只是日本近代发明的。
<img src="pic/mustard.png" width=300 align=center>

##### 圣诞节有关
- 圣诞老人 santa /'sæntə/ claus 简写 santa or Mr. Claus,
- 用与撒旦satan /seitən/完全一样的字母，就是n的位置不同。
- 邪恶的，魔鬼的 satanic \sei'tænik\ 前边读音不变。

##### 机构组织相关
- the State Department 美国国务院（直属美国政府管理的外事机构，相当于外交部，首脑是国务卿，相当于外交部长，与中国的国务院不同）
- Congress 国会，代表大会
- capital  n. 首都，大写字母，资本；adj. 重要的，大写的。 这里的 capital letter是口语中的大写字母，书面语中可以用 upper case letters ， 小写就是 lower case letters
- 上边换一个字母，发音完全相同！capitol 意义比较狭窄，指国会大厦，美国国会的办公大楼，建在一处海拔为83英尺的高地上，故名国会山Capitol Hill。
- embassy 大使馆 /'æmbəsi/， ambassador 大使 /æm'bæsədə/， embarrass  /im'bærəs/  使尴尬

##### 交通出行有关
**汽车**
- drop off 送下车
  - They dropped my daughter off with Jessey's parents.
- 超车 overtake
  - overtake a truck
  - overtake on a bend 弯道超车

- 靠边停车pull over：  
  - Please pull over. I am throwing up.
  - Pull over at the next intersection.
- 把车停进某个地方
  - pull into a gas station
  - pull into a rest area to sleep
- pull off the side of the road

实际上关键的是 pull，后边加不同的介词

- 汽油： gasoline \'ɡæsəli:n\
- 停车场： parking lot
- 路障： road block

**火车**

- 铁轨： railroad tracks


**飞机**

- 转机： transfer at
   - I transfered at Shanghai for a flight to Singapore. 
- 坠毁： Unfortunately, on the way the plane crashed and they were killed

##### 家庭生活相关
- 打扫卫生 sweep (using broom)
  - sweep the floor
  - The showroom has been emptied and swept clean
  - Sweep the leaves up into a pile

##### 读音有关
- 音节 syllable /'siləbl/  重音 stress
  
      The word has two syllables, and the first syllable is stressed.
- casual /'kæʒjuəl/ 音节一个也不能漏掉，“开日有儿”
- niece（侄女，外甥女）后部发音与piece相同。
- cloth /klɔθ/ 一块布， clothes /kləuðz/ 衣服
- 同音：plane /plein/飞机，平面  plain 普通，平常。Plane 后半部分和lane读音是完全相同的！相同的还有chain /tʃein/. Markov chain。可是planet /'plænit /读音变化很大
- Korea 居然和 career/kə'riə/是同音！
- sore 与sour 'sauə读音相似，但是后者没有疼得意思，指味道酸
- bald  bɔ:ld光秃的就是在ball的读音后边读d. bold  bəuld大胆的，黑体的
- bath , path 后边都是/ɑ:θ/, bath是洗浴的名词，动词是bathe /beið/, breath 是呼吸的名词，动词是 breathe
- 车闸 brake 与 break同音，都是\breɪk\
- affair ə'fεə风流韵事, 读音比较：fair \fεə\ 公平的； fire \'faiə\ 火后者口型要大一点。
- stationary 固定  stationery 文具同音
- 东方明珠the Oriental Peal/pə:l/ Tower， tower/'tauə/读音跟power后边完全一样，类似读音的还有shower，take a shower, baby shower。连”owar”都是/auə/的读音coward /'kauəd/懦夫
- pawn \pɔ:n\ 典当，抵押物；当掉，以……担保  。 spawn spɔ:n产卵（后边读音与前边相同，就是在前边加了s）
- resume 作为简历时，为法式读音/’rezju:mei/，重音在前。当作为动词意思重新开始时，读音为英语，重音在后边
- 钢琴piano   钢琴家pianist   前半部分发音相同
- cause 引发 course 过程读音差别在于一个是kɔ:z一个是kɔ:s
- 获得，得到 derive /dɪˈraɪv/ 发音与drive  /draɪv/ 完全相同，除了那个多出来的e。
- reign 统治 n,vi  /rein/ 音同 rain 
- compliment （赞美）与 complement（补充）是同音 /'kɔmplimənt/
- desert /'dezət/做名词，沙漠,  /di'zə:t/ 做动词，遗弃。做动词的时候跟点心dessert同音。
- carp kɑ:p鲤鱼（单复同形）， carpet /'kɑ:pit/ 地毯， harp 竖琴
- envelop /in'veləp/ 信封，读音及重音与develop完全相同
- Abraham 的儿子Isaac，这个单词其实就是牛顿的姓：艾泽克/'aizək/
- 动词： verb /və:b/ ,  副词：adverb /'ædvə:b/, 形容词：adjective /'ædʒiktiv/ d不发音！很像adjacent /ə'dʒeisənt/ matrix 邻接矩阵
- spot 跟sport读音很像，只是一个是短音，一个是长音。
- 讲话之前的清嗓子 clear throat /θrəut/  后部读音跟boat完全一样
- 哀悼、忧伤mourn /mɔ:n/，它的动名词形式mourning读音和morning完全相同。
- which 与 witch （女巫）同音。
- 好像 stle 在词尾就读做 /sl/
  - apostle /ə'pɔsl/ 使徒 
  - castle /'kɑ:sl/ 城堡  
  - wrestle /'resl/ 摔跤
- 注意 relate /ri'leit/ 和 relative /'relətiv/ (亲戚) 发音不同。
- method --> Methodist 卫理公会教徒，读音很标准
- 订书机 stapler /'steplə/ ，读音前边跟step完全相同，后边是lə“了”。
- revolution （革命） 后部发音与 evolution （演化）完全相同。
- revelation 启示，与上边仅有一个音不同
- glisten /'ɡlisən/ 闪光，后半部分读音跟listen相同。

      Their faces glisten with sweat. 
      The leaves glisten with dew /dju:/
- 欠某人的帐 I am in debt to you!  debt \det\ 中间的b不发音，末尾”b”不发音的有：lamb, thumb、子宫 womb/wu:m/ 、坟墓tomb/tu:m/，梳子comb/kəum/, honeycomb 蜂巢
- thesis /'θi:sis/ 论文。分析与综合：analysis and synthesis /'sinθisis/ 后部可以认为读音与thesis基本相同
- 跟恶魔有关的几个词 
  - evil /'i:vəl/ 罪恶（的），不幸（的）  evil spirit 恶灵
  - devil /'devəl/ 魔鬼（虽然后部拼写相同，但读音差别很大）。
  - demon/'di:mən/ 也是恶魔的意思。
- 痣 mole 读音可参照hole
- either /'aiðə/  neither/'naiðə/ 这是美式读音，以后就按这个读吧
- shield （盾）, field, yield 后部读音相同
- heir /εə/ 子嗣，继承人。与 air 发音相同。这么多年一直发音错误 their 后边就是heir的发音，还有there, where
- pathetic /pə'θetik/ 可怜的  
  sympathetic /simpə'θetik/ 同情的（读音后部完全相同）名词是 sympathy /'simpəθi/
- locus /'ləukəs]/ 轨迹，读音与  lotus /'ləutəs/ 莲花相似. 后边加个t就是：蚂蚱、蝗虫 locust \'ləukəst\
- 之前一直以为prior很难发音，其实它是完全符合发音规则的prior /'praiə/
- **问题：** a unique 还是 an unique ?

 u在单词开头一般有两种读音，第一类是/ju/，例如unique, university；第二类是/ʌ/，例如umbrella, unusually. 
​     
 在第一类读音前用冠词a，在第二类之前用冠词an.

 其实，用a还是an，不取决于首字母是元音还是辅音，而是读音是元音还是辅音。一些常见但易错的用法

    a European （貌似/ju/开头的读音都加a）
    a one-way street

 ![vowel](pic/vowel.png)

  an还用在发音以元音开头的单个字母前面：
​    
​    an L-plate, an MP, an SOS, an "X", an n-player game.

- 名清动浊： 有些词通过改变词尾的清浊音来改变词类，有的拼法无变化，有的拼法也跟着变化。
  - 不变：
    - Abuse        Abuse 
    - Excuse        Excuse 
    - House        House 
    - Use           Use 
    - lives( laiv)     lives 
  - 变：
    - Grief        Grieve
    - Half        Halve
    - Relief        Relieve
    - Shelf        Shelve
    - Thief        Thieve
    - Advice        Advise
    - Belief         Believe

##### 日常生活常见单词
- 词汇怎么说：I will teach you some vocabulary before we get into today’s course.
- 小龙虾 crayfish == cray 
- 大龙虾 lobster
- 番茄酱 ketchup /'ketʃəp/
- 鲶鱼 catfish
- tap /tæp/ water 自来水
- bottled water 瓶装水
- 干洗店 a dry cleaning shop
- 初中 junior high school 或者简单说成 junior high. 与之对应的，高中 senior high school
- 冰箱 refrigerator \ri'fridʒəreitə\ or freezer, 制冷的名词就是 refrigeration
- 谜语 riddle /'ridl/
- date 北非和西亚附近的海枣， Chinese date 是我们常说的枣
- 雨靴 rainboot
- 涂鸦 doodle /'du:dl/ 既可做动词，也可作名词
- timing机会，机遇The only thing you need it timing.
- SINOPEC: China Petro-Chemical Corporation 中国石油化工总公司
- 符号：
  - 井号（#）pound sign
  - 星号（$*$）asterisk
  - 惊叹号（！）exclamation  /eksklə'meiʃən/
  - 句号（.）period
  - 逗号（，）comma 'kɔmə
  - 冒号（：）colon 'kəulən
  - 分号（；）semicolon semi'kəulən
  - 问号（？）question mark
- monotonous\ monotonic两个单调重音不同，适用的场合也不同：
  - monotonous mə'nɔtənəs单调的,无生气的，无变化的（主要用来表明枯燥的意思）
  - 单调增函数 monotonically /,mɔnə'tɔnik/ increasing function, 函数在某个区间内是递增的： f(x) is an increasing function 
- 后进生 underachiever
- it pays to …做…是有回报的
- 兵马俑 Terracotta Warriors
- 嘉年华=狂欢 carnival 'kɑ:nəvəl
- 获得成功，取得预期的效果 
  - My hard work paid off.  
  - You honesty /ɑnəsti/ and hard work are paying off.
- 时间表；课程表 timetable 
- 食欲 appetite /'æpitait/
- 开胃食品 appetizer /'æpitaizə/
- 外包 outsource  They were happy to outsource it to other firms.
- 场景 scene /si:n/   风景 scenery /'si:nəri/  风景优美的 scenic
- appropriate adj 恰当的 v 擅用，占用
- found是find的过去式，同时自身也是一个动词：创立，建立 The firm is founded in 1890.
- 病假：I need a sick leave for two days
- 事假：I’m asking for a three-day personal leave for my wife’s labor.
- reed ri:d芦苇
- weed 是杂草！动词是铲除的意思。去除错误观念： weed out incorrect beliefs.
- 降落伞 Parachute \'pærə,ʃu:t\
- 毒蛇 viper \'vaipə\, 眼镜蛇 cobra \'kəubrə\ , 后边读音类似的如，斑马 zebra \'ziːbrə\
- 帝国 empire /empaiə/ 发音很标准，后边是fire的发音
- therapy 'θerəpi治疗；therapist 'θerəpist一般指心理治疗师，physical therapist 就是物理治疗师。
- 短的是连字符 hyphen  长的是破折号 dash
- sow /səu/ 大母猪， swine /swain/ 大公猪 （爱喝酒的大公猪） 
- prescription medicine 'medisin， non-prescription medicine = over-the-counter medicine (OTC)
- 帝王 emperor /'empərə/ 发音很标准
- “像、类似”的高级用法：resemble [vt]
  - Broccoli /'brɔkəli/ most closely resembles cauliflower /'kɔli,flauə]/ 西兰花很像菜花！
  - As we will see, despite the uncertainty embedded in the topology of our random network at each time instance, one can still prove a number of results that resemble those for the static case.
- 好吃的东西：yum  好吃的：yummy. 难吃的东西：yuck  难吃的：yucky
- 
- 铁锹 shovel \'ʃʌvəl\
- sitcom==situation comedy 情景喜剧
- 那种装扎啤的壶叫做 pitcher（水壶）. 来一壶最便宜的啤酒：One pitcher, cheap stuff.
- 农民peasant /'pezənt/ 有乡下人、未受过教育的意思，褒义应该用farmer.
- 头盔 helmet \'helmit\ 
- 面甲 visor \'vaizə\ 读音与supervisor (监督人) 完全相同。
- 风车 windmill /'windmil/
- 防晒霜 sunblock
- 避孕套 condom /'kɔndəm/ (公寓condo里面有MM，就需要condom)。 用于出售的公寓condo /'kɔndəu/，如果是用于出租的就是apartment.
- 电视观众 viewer
- 路边小饭馆 diner /'dainə/ 跟晚餐dinner /'dinə/ 很像
- 石头剪刀布 rock, paper, scissors /'sizəz/.
- cafeteria/kæfi'tiəriə/ 就是自助式餐厅，只要是没有那种点菜服务的都算是自助式的。自助餐只是其中的一种。学校食堂也算是cafeteria。
- 馄饨 /'wɔn'tɔn/
- 天上有云说cloudy, 如果全都被云覆盖了，就是overcast
- 建筑师architect
- architecture建筑学，建筑风格, architect相关的字词里面就没有名词“建筑”
- LED （light-emitting diode \'daiəud\）发光二极管
- 我们常说的T-shirt，其实是tee-shirt的简写。tee的意思就是T字形的，T形物。
- 吹牛 brag /bræɡ/ 或者 boast of my talent
- 天花 smallpox \'smɔ:l pɔks\ （读音很标准）
- 喷嚏，打喷嚏 sneeze /sni:z/
- 同伴，同盟：ally /'ælai/, 小巷子 alley /ˈæli/
- 跳蚤 flea \fi:\
- stub 名词：树桩，票根，烟蒂，断株.  电影票根：movie stub
- 引起争议的东西 This is a controversial film. / ,kɔntrə'və:ʃəl/
- 用拳打是punch，用掌打是slap
- 火山 volcano /vɔl'keinəu/
- 岩浆 magma /'mæɡmə/
- 绞刑时用的绳套、或者捕捉动物的圈套 noose \nu:s\
- 贵宾 honored guest
- 受伤和伤口： You are injured. Fortunately, the wound is not deep.
- 收考卷：Please turn in the tests.考卷就是叫做test，不要说成test sheet，exam sheet.
- 相声演员 stand-up comedian /kə'mi:djən/
- 让人喜爱的小老鼠是mouse, 让人讨厌的大老鼠是rat, rat还有叛徒、背叛的意思. Who rat me out ? 谁告的密？
- 留位置： We are saving the seats for .../   This seat is saved.
- 老鼠和虱子居然有相同的变化规则
  - 单数mouse /maus/    复数 mice /mais/
  - 单数 louse /laus/   复数 lice /lais/
- 忙碌的状态，纷纷攘攘  abuzz /ə'bʌz/ 。主要用作表语 Everybody is abuzz.
- 被子 quilt /kwilt/ 或者是美式叫法 comforter
- 蹲 squat /skwɔt/
- 三部曲 trilogy /'trilədʒi/  star wars trilogy
- 主要嫌疑犯 primary suspect
- 花瓣 petal /'petəl/  玫瑰花瓣 rose petal
   古龙香水	 cologne /科隆/ 
- 秋千 swing
- 未婚夫 Fiancé /'fi:ɑ:nsei/, 未婚妻 Fiancee 读音相同！有“昂”音。
- 零花钱 allowance ， 也可以用 pocket money
- 委员会 council /'kaunsəl/ ， 前边读音跟count完全一样
- 慰问卡 get-well card， get-well 表达恢复健康愿望的...
- 业余者，外行 amateur /'æmətə/
- bun 小圆面包 a toasted bun
- 出租车上司机跟乘客之间的挡板叫做partition.
- 门上的球形手柄 nob /nɔb/
- 保镖 bouncer
- 夜宵midnight snack.
- chat是正常的聊天，chatter是唠唠叨叨的聊。
- paste /peist/ 糊状物，用浆糊粘。电脑上的粘贴就是这个动词
- pasta /'pɑ:stɑ/ 面团，面食
- 细长的豆角叫做pole bean.  这两个词反过来就是beanpole瘦长苗条之人。
- 双肩背包  backpack
- 来源于日语的老师 sensei /sen'sei]/
- sucker是指吸管(另外一个吸管是 straw 稻草)，也指易受骗的人
- VCR: video cassette/kæ'set/ recorder /ri'kɔ:də/ 录像机
- 粽子的英文就是zongzi.
- 圣诞节Christmas为什么会简写成Xmas。因为Christ是基督的意思，古代曾用X代替Christ，因此Christmas也顺便简写成了Xmas.
- 大提琴 cello/'tʃeləu/ 读音不标准，跟Jerfferson的故居一样 Monticello.
- 圆珠笔 ball-point pen
- plum 李子，经常吃的一种紫黑色水果就是黑布林 black plum (也就是黑布朗)
- 
- 蟑螂cockroach /'kɔkrəutʃ/
- 男校友 alumnus /ə'lʌmnəs/  复数形式是 alumni /ə'lʌmnai/ 
- 女校友 alumna /ə'lʌmnə/  复数形式 alumnae /ə'lʌmni:/
- 一次性筷子 disposable chopsticks
- 公路上向西行的一边 westbound  
- 星际迷航/星际争霸 Star Trek ,  trek 艰苦跋涉
- 尿床 bed-wetting 名词
- 爆竹 firecracker 
- 象征 symbolize /'simbəlaiz/   What do the Olympic rings symbolize?
- 羊有不同的英文单词：ram成年公羊   lamb 羊羔
- 圣代冰淇淋 sundae /'sʌndei/ 读音很像 Sunday \'sʌndi\，而且拼写只差最后一个字母。即将果酱、奶油、樱桃等加在冰淇淋上。

![sundae](pic/sundae.png)

- 甜甜圈 donut == doughnut,其中 dough 就是生面团的意思，读音就是\dəu\ 
- 批萨 Pizza 有一种是pepperoni /pepə'rəuni/ 意大利辣香肠, pepper 是胡椒的意思， peppermint 是薄荷。跟pizza很像的一个单词plaza/'plɑ:zə/意为广场。
- 带轮子的床 gurney/'gɜːnɪ/ 
![gurney](pic/gurney.png)
- 运动员穿的那种短袖运动衫叫做 jersey。 就是 New Jersey 的后半部分。
- 栖息、居住 inhabit /in'hæbit/ 后部读音就是habit. 可以做及物动词，居住于：

      Spirits are thought to inhabit animals’ bodies.
      the environment they inhabit.
- 前言，引言 preface
- 飞盘 frisbee \'frɪz'bi\
- 梨 pear /pεə/ ，  spear /spiə/ 矛，读音差距挺大的。 pearl /pə:l/ 珍珠， 读音与pear差距很大
- 拖拉机 tractor
- 塑料、塑料的， 名词形容词都是 plastic
- 直梯 elevator， 扶梯 escalator \'ɛskə'letɚ\
- 芭比娃娃 Barbie doll 
- 捕食者 predator \'predətə \
- 被捕食者prey \prei\
- 矩阵 matrix \'meitriks\ 注意读音，是梅...
- 绿洲 oasis \əu'eisis\ 读音很简单
- 忠实粉丝，铁杆粉丝 a big fan of / a huge fan of 
- 好望角 Cape of Good Hope
- 手淫 masturbate \'mæstəbeit\  很像 master baker
- 母乳喂养 breastfeed (n,adj,vt) She will breastfeed her son.
- 水母、海蜇 jellyfish
- 代词 pronoun （读音与pronounce前部完全一样）
- 红灯区 red light district
- 安抚 appease /ə'pi:z/   It’s not likely to appease everyone.
- 夏威夷人带的那种花环 lei (读音就是按照字面)
- 出纳 casher
- 狱友 cellmate
- 口语，白话 colloquialism /kə'ləukwiəlizəm/ 名词，  colloquial /kə'ləukwiəl/ 形容词
- 皇冠 crown， 小丑 clown 读音后部是一样的
- 好色之徒 lecher /'letʃə/  n. 读音很像 lecture，所以以后读 lecture 的时候要注意发音
- 好色的 horny
- stock是股票的意思，stocking是长袜的意思，fishnet stocking 是渔网袜 
- 爬行动物，爬行类的 reptile \'reptil\  The crocodile is a reptile. 
- 胭脂 rouge / ruːʒ /
- 联盟 league /li:ɡ/   Ivy League 常春藤联盟   colleague /'kɔli:ɡ/ 同事，后边发音是完全相同的
- 细微差别 nuance /'nju:ɑ:ns/
- 30摄氏度： 30 degree Celsius, 30 华氏度： 30 degree Fahrenheit /ˈfærənˌhaɪt/
- 好心人 well-meaning people
- 小狗 puppy /'pʌpi/ ，  puppet /'pʌpit/ 木偶、傀儡， 读音完全相同，就是后边多了一个t
- 马克杯，一般是指带半圆形手柄的杯子 mug， mug 既有杯子的意思，又有抢劫的意思。
- 替补演员 understudy
- 手铐 cuff /kʌf/  
- 阳台 balcony \'bælkənɪ\ 与露天平台 deck \dek\ 的区别
 - balcony

 ![balcony](pic/balcony.png)

 - deck 

 ![deck](pic/deck.png)

- 前廊 porch
   ![porch](pic/porch.png)
- lamp post 灯杆.  把自行车锁在灯杆上、把宠物系在灯杆上，都是用to。 tie your dog to the lamp post.
- 绕口令 tongue twister
- 什么东西的钥匙，用介词to  the key to the door, the keys to the cuffs
- 凯撒 Caesar /'si:zə/ （读音好奇怪，怎么会翻译成“凯撒”呢，应该是“西泽”）
- 礼仪，惯例，礼节 ritual \'ritʃuəl\
- 原油 crude oil
- 翡翠，碧玉 jade /dʒeid/ 读音很标准
- 幻灯片是slide
- slice /slais/ 
  - 名词是薄片：  I want a slice of cake.  three slices of pizza. 
  - 动词是切成薄片： when a fresh onion is sliced,……
- 比赛中的犯规 foul /faul/  That was a foul, isn't?
- 技术犯规 technical,  Watch it, coach, or I'll give you a technical.
- 讨厌鬼 douche /du:ʃ/ 
- 轻而易举的事 cakewalk , This is a cakewalk.
- 瘦肉 lean meat, 瘦肉，瘦的 lean （就是“依靠”那个词）  You describe the turkey as extremely lean.
- 传记 biography /bai'ɔɡrəfi/  传记作者 biographer /bai'ɔɡrəfə/ 前部读音完全一样
- 
- 染色 dye \daɪ\ 读音与 die 完全相同。 The umbrellas are dyed black.
- meatloaf 肉糕
   meatloaf is made of ground meat (磨碎的肉) forming into a loaf shape.
    ![meatloaf](pic/meatloaf.png)
- 少年，幼稚的 juvenile/'dʒu:vənail/
- 青春期 adolescence /,ædəu'lesəns/
- 存钱罐 piggy bank  (直译：小猪银行)
- haven /'heivən/ 避难所， heaven /'hevən/ 天堂（有e，不但可以进去，还可以出来，和谐了），人间天堂 heaven on earth
- 停电通知： There will be a power outage from 1-5pm tomorrow.
- strip /strip/ （读音后边就是trip 旅途）主要用两个意思：
  1. 动词：剔除，脱衣服；stripper 脱衣舞女
  2. 名词：条状，带状；  strips of dried beef. 牛肉干有专有名词 beef jerky. jerk 古怪的人
- strip是带状，条状，stripe \straip\ 是条纹、斑纹的意思. strip 和 stripe意思相近，但有明显的区分。
- hydr-'haidr水的词根
  - hydrogen 'haidrədʒən 氢
  - dehydrate 使脱水
  - carbohydrate 碳水化合物
  - hydrant 'haidrənt消防栓

##### 日常生活短语
- wanna==want to .  I wanna pee    

- gotta==have got to.  我要走了。 I gotta go.

- gonna==going to  . I am gonna eat food.

- ain’t \eɪ n t\ == is not, are not, am not, have not, has not.     

- a year and a half  或者说  one and a half years
  ​    I become a PhD student a year and a half ago.

- 致力于 we are dedicated/devoted/commited to sth / doing sth.都是将自己奉献给某事

  也可以用形容词，很敬业的 Barney is a very dedicated wingman.

- rain check 改天 Can I take a rain check? 改天再做。 

- 我也不太清楚：I don’t know for sure.
  - A: Could you tell me how to get to the town hall(市政厅)?
  - B: I don’t know for sure. Maybe you could ask the policeman over there.

- 以防万一：just in case （后可以跟完整的句子） Just in case someone steals it.

- 宅的indoorsy。 I am indoorsy.

- 人生最糟糕的时刻 the low point/moment of my life.

- gather rice  收割稻子

- 颐指气使 boss around。boss不但有名词意思，还有动词指使的意思。boss me around就是对我颐指气使。

- 嗨，你骗我 You tricked me.

- BC==Before Christ(基督，救世主)（英文）

- AD==Anno Domini（拉丁文）：意为“主的年”

- gripe about ɡraip抱怨  She always gripes about one thing or another!

- 热情的 passionate /'pæʃənit/ 这个词只有形容词的意思

   - He was passionate about stamp collecting.

- 容光焕发　Your face is glowing

- hit指的是去开始做某件事。
  - 当乐队人员准备就绪，主唱就会说：let’s hit it. 
  - 去洗澡：hit a shower
  - 去睡觉：hit the hay==go to bed  （不是hit the bed)
  - 上路：hit the road

- social learning will be our central focus in this paper.   focus 既是动词，又是名词。

- whereby的经典用法：Much of it takes place through a process of “social learning”, whereby individuals obtain information and update their beliefs as a result of their own experences.（在这个过程中……）

- fit的两个重要用法：
  - 适合干某事（形容词） He is fit to do sth.  
  - 合身（动词）I don’t think this one will fit me.

- 什么对我不是问题： Something is not an issue for me.   Your age is not an issue for me.

- 关系疏远： drift apart
  - An argument caused him to drift apart from his family.
  - We shall never drift apart. I am with you all the time.

- 白手起家的故事 rags-to-riches story

- 时间正好：Time is up. (监考时常用)  在口语中，简洁的用法就是直接说“time!”

- 不大自然，拘束 self-conscious. The speaker looked self-conscious. I still feel a little self-conscious. 

- 谁是谁的几倍:  some dam discharge three times as much water as some dam。

- turn on 在口语中表示感兴趣，受吸引(一般指性方面)，例如 I was turned on by that lovely lady. turn-on 或者turn-off则是喜欢或者不喜欢的东西. She is a turn-on.

- 简单地说  In a nutshell, ...

  In a nutshell, the developer writes a test before writing any code.

  nutshell 本身的意思就是字面意思，坚果的外壳

- pay up 付清，偿还  I will take you to court (/kɔ:t/ 字母u不发音) unless you pay up immediately.

- enroll 使加入  Why his son was enrolled in so many classes?

- bide my time 等待时机

- back-to-back紧接着的 (所谓的“背靠背”). I just had two back-to-back date

- 报了一个班sign us up for a English class

- 正大光明的(地) fair and square. 
  - I think I am always fair and square.  
  - I won the game fair and square.

- 不久前的某一天 the other day

- 我要做什么来补偿你： What can I do to make it up to you?

- Make up for everything 补偿一切   Oh, good. That makes up for everything.

- 在事情进行中，如果要问还要多长时间做好：How much longer will it take you to get all of this stuff done?

- 浪费squander /'skwɔndə/== waste。 Do you love life? Then do not squander time, for that is the stuff life is made of.

- 白金唱片 platinum /platinum/  album (一般指销量超过100万的唱片)。 platinum 化学元素铂，白金，形容词为：唱片销量为100万张的

- 饱了可以说I'm full. 或者 I'm stuffed.

- 左侧常与邪恶联系起来，sinister \'sinistə\ 意思是灾难性的，邪恶的。这个词在Latin语中就是左边的意思。有不少俗语遵循左边是邪恶的思路，如一个人脾气很坏就说他gotten up on the wrong side of the bed，实际上wrong side就是left side。还有，当碰到装盐的瓶子的时候，被认为有魔鬼靠近，要往左后方撒几粒盐（a few grains of salt），也是因为左边与邪恶联系在一起。还有，在有关天使、魔鬼的图片中，魔鬼在左肩，天使在右肩

- 每隔几天：every other。每隔一天 every other day，每隔两天every other two day.

- 想单独说句话：Can I get you alone?

- on earth 究竟. What on earth is going on with your ……？

- rough 和 tough的区别： Life is rough(艰苦), but you are tough(坚强).

- 换句话说： or to put it another way 或者另起一句 Put differently, ……

- assembly /ə'sembli/line 装配线

- 与knowledge相关的短语：
  - 积累知识 build up knowledge.
  - Beyond sb’s knowledge 某人不知道的
  - Without sb’s knowledge 不告诉某人
  - Build up/ Absorb knowledge 积累/吸收知识
  - Common/General knowledge 常识

- 晚回来，可以跟室友说： Don’t wait up.

- “拍手”“跺脚”， Clap your hands / Stomp [stɔmp] your feet.

- 闰年、闰月都用leap这个词，leap year, leap month

- 关于"支持"的分类：
  - 上级对下级：I back you.
  - 平级：I support you.
  - 下级对上级：I second you.

- 钱来之不易： Money does not grow on trees.

- pull my leg不是“拖后腿”，是“愚弄”. You are pulling my leg!==You must be kidding.

- 注定要发生 This was bound to happen eventually.

- gay一般做形容词，所以说: He is gay.如果想说名词，可以说 He is a gay man.

- lesbian既可以做形容词也可以做名词，因此 She is lesbian，或者 She is a lesbian都可以。

- plank厚木板，常具有修饰作用 plank bed 木板床，plank table，plank bridge

- 形容一个人金发，可是说 with blond hair

- 办个party 是throw a party

- 放狗！ Release the dogs.

- 射击的时候，说的“准备，瞄准，射击”: ready, aim, fire

- 别对新人太苛刻 Go easy on the new people. 

- 对某人态度不好：He was so hard on me last night.

- 清楚clear，如果要强调完全明白、完全清楚，就说crystal clear. Everything is crystal clear. 就像用brand new表示崭新一样！

- 在当前及可预见的未来：Unfortunately, at the present time (and in the foreseeable future), there is no support for...

- 完全的 head-over-heels   a complete head-over-heels idiot

- Do not screw it up. 别把它搞砸了！

- 我帮somebody占座： I am saving this seat for sb./ save you a seat./ It is saved.

- 别再提了 Just drop it!  中文说：别说了，可以说成“别提了”，有趣的是，英文中也有类似的用法：Just drop it.

- screw it! 去他的！

- 问女孩子的status:  Are you seeing anybody at the moment?

- gold digger 傍大款的女人

- 小心 heads up

- 祝贺毕业就说： congratulations on graduating.

- 在禁飞名单上 on the no-fly list

- I can talk with women when I am sober /'səubə/.  Howard跟Raj说的！

- 百里挑一的，极稀有的人或事  Something is one in a million.

- bid /bid/ 出价: Anyone else bid?

- You can count on me.　你可以信赖我，你可以依靠我。

- 做某事需要勇气 It takes guts to do sth.

- 生不如死 the living will envy the dead

- 受宠若惊，非常荣幸 Oh, thank you. I’m so flattered./ I feel flattered by your invitation.

- 帮人家照管店铺.Oh, don’t worry. I will watch over the shop until you come back.

- Promising 有希望的。 Promising candidate 有希望的候选人， promising customer

- 轮班 shift，   I end my shift early.

- 一般不用naughty形容小孩子淘气，因为里面有色情的意思。一般说mischievous /'mistʃivəs/

- Some new information has come to light on sth (新信息被曝光出来了)

- compare A to B  把A比作B.  要区别开：compare A with B（把A和B相比）

- on the one hand …… on the other hand …… 别忘了 the

- neck and neck 并驾齐驱，不分上下

- 黑马 dark horse (暗马，不是黑black)  

      It happens sometimes that a dark horse candidate gets elected in great election.
  另外，一个类似的是black sheep，害群之马。
  ​    

      There are still some black sheep in our society.

- 他们过着水火不容的生活： The husband and his wife are always quarreling, and they live a cat and dog life.

- 穷困潦倒的生活 a dog's life: The man lived a dog's life.

- 绝交信 dear John letter . Smith recieved a dear John letter from his girlfriend because he had broken her heart.

- Yet在句首常常这样用： Stranger yet, ……更奇妙的是，递进描述。

- 爱情中一方背叛另一方，用cheat on： He cheated on his girlfriend with another beautiful girl.
  或者说 betray his girlfriend

- 后续的 You invited him back for a follow-up interview.

- 作报告的时候，如果不想被提问打断：Please hold all questions to the end of the presentation.

- beware==be aware   一般后边跟很危险的情况或事物 beware of danger 

- Great minds think alike. 英雄所见略图

- hit me在口语中经常表示再次给我的意思，比如玩21点的时候要牌，在酒吧让服务生添酒。火影忍者中，鸣人吃拉面的时候说：Hit me with another big bowl

- 性骚扰 sexually harass /hærəs/   名词 sexual harassment

- It's easier said than done. 说的比做的简单

- 是否介意： Would you mind doing/ not doing sth.或者加if从句， Would you mind if I make a call.
  如果回答不介意，就说 No, please do. 或者 Not at all, please do.

- 烤肉的时候，把肉翻一面怎么说呢？ We have to flip over this chicken leg.

- 工作完成了不要老说Done, Finished，简单点可以说 set / All set, sir.

- 收听广播 tune in next time to see what happens.

- 牵线搭桥set sb up with sb.  

      I set him up with a girl. 
      I didn’t throw this party to set you up with Carlos.

- 享受特权的 privileged /'privilidʒd/。 社会底层的 underprivileged：     These underprivileged children.

- 理发 get a haircut.   

- 剪指甲 get a manicure /'mænikjuə/

- dirty looks 一方面指瞪眼，另一方面也指脸色难看

      Teacher gives me a dirty look when I am talking in class. 瞪了我一眼
      People have dirty looks on their faces.

- 对something垂涎三尺： drool (流口水) over something. 例如I am drooling over the Kindle DX.

- At your command 静候您的处理，听候您的命令。例如：at the king’s command

- 结果相反 
  ​    These results are reversed \rɪ'vɝst\
  ​    However, for physically meaningful numbers of monkeys typing for physically meaningful lengths of time the results are reversed.

- 直说了吧 cards on the table 

- cute主要形容人可爱，如果说东西可爱，比如鞋子漂亮，可以用delightful You shoes are delightful.

- 安排他们见面 arrange for them to meet

- 首先并且最重要的 first and foremost

- 向某人脱帽致敬 hats off to somebody.

- 在friends里一集，瑞秋跟Ross说，I am over you.(我跟你完了)。还有另外一个比较相似的短语：all over you。意思是非常的着迷。等于 have a crush on you.

- 超越，度过 I can’t get over it so easily.

- slack /slæk/放松，松懈的，后边读音跟lack完全一样， 放某人一马 cut somebody some slack，注意，一定要用some slack

- 就算是平局吧 Let’s call it a tie.

- quite a few 相当多的，不少

- suck吸，在口语中最常见的意思是糟糕的，是动词！！！就像smell，stink一样. 口语中在用suck的时候要注意时态语法，比如说昨晚的电影很糟糕 The movie sucked. You suck. She sucks. 把什么弄糟、不擅长什么事情，可以说 I suck at something.

- 犯罪 crime /kraim/， 罪犯 criminal /'kriminəl/

- I love you 和I am in love with you的区别： 前者可以用在亲人之间，对亲情的爱，友情的爱，后者只能用在恋人之间。 所以如果对对方没有了感觉，就可以说 I am still love you, but I am not in love with you.

- 辣的程度： hot spicy, medium spicy, mild spicy

- 把调料单独放在一个小瓶里，不要淋上去 I want salad with dressing on the side

- 挑逗 hit on.  He is hitting on her.

- hit the point: 正好！

       The rain really hits the point. 
       The tender, juicy steak really hits the point.

- 拍某人马屁：kiss up to somebody.  I don’t like to kiss up to my boss.

- 说曹操，曹操到  speak of the devil. 或者 talk of the devil.

- 到了别人家，夸别的房子有家的感觉： This house gives me the warm nest feeling.

- 机遇青睐有准备的头脑 Chance favors the prepared mind.

- 一饮而尽 do a shot.

- 外国人的鼻子大，不是big nose,而是long nose

- play hardball （俚语）硬碰硬，采取极端方法

- 谁跟谁打赌： Lily bets Marshal that ……

- 坚持做某事：insist on doing  or  insist that

      Mr. Tang insists on continuing his candidacy(候选状态，资格)./'kændidəsi/

- 介词
  - 在床上 in the bed
  - 在沙发上 on the couch  
  - 坐在椅子上 sit in a chair 
  - 坐在树上sit in a tree

- 眼镜度数 How strong are your glasses?

- 查一查 look it up   
  When you see a new word, look it up in a dictionary.

- go through: 仔细检查 go through the checklist 仔细检查清单

- 刷牙的时候，看到牙膏盒上写着：For best results, squeeze tube /tju:b/ from the bottom and flatten as you go up. 

- 贵族 nobility \nəu'biləti\

- 告诉别人坏消息的时候说，there is no easy way to say this. 没办法轻松地说这件事

- 对某事的态度
  ​    where do you stand on sth? 
  或者用 stand 的名词形式 stance
  ​    the agent's stance on a given issue.

- 围一起讨论战术，动词 huddle up， 名词 huddle

- 从……开始： Start from + 名词;  start by + 动名词

- 下棋的时候的将死 checkmate 动词、名词

- 衣服破了不要说broken, 那是打碎的意思。要说it is torn.

- how come 引导比较口语话的疑问句： 
  - how come + (that) we've never thought of that before?
  - How come + (that) you are looking so bad?

- 不见不散 be there or be square.

- 我们都帮老板做了校对工作，他在鸣谢里要说：someone helped me proofread parts of the book.

      proofread / 'pru:f,ri:d/ vt. vi 校对，校勘。
      proofreader 就是校对员。

- 转身 turn around

- 死亡人数固定用法是 death toll

      Authorities announced the death toll at a news conference Sunday.

- 毫无疑问地 unquestionably 

- 有营养的早餐　a nutritious /njuˈtrɪʃəs/ breakfast

- 为自己辩护  in my defence

- 在某种意义上 in a sense，还可以加入一些形容词，如in a narrow sense 在狭义上

- 把衣服撕掉 rip her clothes off

- 让我们很丢脸 make us look bad.  让我们很有面子 make us look good

- 长话短说 long story short

- same ... as:  I once met a girl who was about the same age as me.

- 禁止进入 off limits. Most of the two-story house is off limits to visitors.

- 起义 uprising. Dalai fled to India during a failed uprising against Chinese rule.

- 10人失踪： Ten people are missing.

- 渴望她回来： I yearn of her return.

- 在说victim时可以加上 hapless /'hæplis/ victims 可以作为固定用法“不幸的受害者”。 I hope that everyone can pray for the hapless victims in the disaster.

- 英文中也有“和时间赛跑”这种用法： race against the clock。 The rescue team raced against the clock to find survivors.

 ##### 颜色相关
 - 朱红色，朱红色的 vermilion /və'miljən/ 后边的发音与百万million相同。
 - violet  /'vaiələt/ 紫色的。 Violate \'vaiəleit\ 动词，违反。 violent /'vaiələnt/ 形容词，暴力的
 - 紫外线就是 ultraviolet

##### 医药相关
- 剂量
  - A: What is the dose /dəus/ ?
  - B: There are two pills per dose. The side effect is that it may make you feel drowsy /'drauzi/ . 昏昏欲睡的。

##### 介词相关
- by
  - by 经常表示在附近的意思, by the table 在桌边
  - by the trash can 在垃圾桶旁边, 垃圾箱是trash bin
  - by也可以表示在某时间之前 by this Friday, by 6 o’ clock
  - by then 到那时候（就是by 时间用法的延续）

##### 数学符号相关
- 正弦 sin = sine \sain\
- 余弦 cos = cosine \'kəusain\ 我们平时的读音就是完整拼写的读音。 



# 运动相关

- jog: 动词慢跑  He began to jog along the road.  
- jogging: 名词

- 扑克牌
  - 扑克牌或者拨火棍，用棍戳的人 poker /'pəukə/
  - 扑克牌还可以说 playing cards  
  - 如果说一副牌，可以说 a deck of playing cards. 这里数量名词deck是固定用法。
  - 扑克牌的四个花色：红桃 heart，黑桃(铁锹) spade \'speid\，方块 diamond，梅花(俱乐部) club
  - 如果是颜色的话，就是red 和black
  - 打牌用语：洗牌 shuffle /'ʃʌfl/  发牌 deal  发牌的人 dealer /'di:lə/
  - a pictured card : a king, a queen， or a jack
  - 21点： blackjack.
- 打麻将或者多米诺，一张牌叫做 tile 读音参照file 本意是瓷砖、瓦片
- 独木舟 canoe /kə'nu:/  类似ca-noo
- 曲棍球 hockey /'hɔki/   
- 冰球 ice hockey
- 保龄球
  - 保龄球的瓶子 pin ，最前边的那个瓶子是pin称为kingpin，也可以引申为大人物。
  - 保龄球把瓶子打翻 \ 撞倒 knock over pins
- 溜冰
  - 溜冰 skate, skating
  - 溜冰鞋 skates, 或者 ice skates， roller skates
  - 溜冰场 ice rink, 或者 skating rink
- 撑杆跳 pole-vault/'pəulvɔ:lt/  vault: 把东西弯成拱形。 撑杆跳运动员就是 pole-vaulter
- 台球 billiards 
  - Billiards is a game played on a large table, in which you use a long stick called a cue to hit balls against each other or against the walls around the sides of the table.
  - billiards 复数是名词
  - billiard 单数是形容词
  - 台球杆： a billiard cue 
  - 台球桌： a billiard table
  - 一局台球： a game of billiards

# 电脑相关

- 敲击键盘 （名词）： keystroke

  to capture keystrokes 

# 动物相关

## 爬行类

- alligator /ˈalɪɡeɪtə/ 淡水鳄，crocodile 咸水鳄 

  ![alligator1](pic/alligator1.JPG)

- tortoise /ˈtɔːtəs/ : 陆龟
- chameleon /kəˈmiːlɪən/ ：变色龙
- gecko /ˈɡɛkəʊ/： 壁虎
- lizard /ˈlɪzəd/ ： 蜥蜴
- boa /ˈbəʊə/ ： 树蟒

## 节肢动物

- crab /krab/ 也是 "啊"的音： 螃蟹
- goldfish:金鱼earthworm /... wəːm/： 蚯蚓， silkworm 蚕

- scorpion /ˈskɔːpɪən/ : 蝎子
- centipede /ˈsɛntɪpiːd/ ：蜈蚣
- cockroach /ˈkɒkrəʊtʃ/ : 蟑螂
- cricket /ˈkrɪkɪt/ : 蟋蟀
- grasshopper : 蚱蜢，即蝗虫
- mosquito /mɒˈskiːtəʊ/: 蚊子

## 鸟类

- dove /dʌv/ ：鸽子

- kingfisher : 翠鸟 an often brightly colored bird with a large head and long sharp beak, typically diving for fish from a perch.

- kiwi ：奇异果，奇异鸟 

  ![kiwi1](pic/kiwi1.png)

- beak 尖细的鸟嘴， bill 宽扁的鸟嘴  The duck goes into the drugstore to buy some chapstick (润唇膏, chap 有皴裂的意思), and the cashier asks how he’s going to pay for it and the duck says, “put it on my bill”
- woodpecker 啄木鸟 (peck 用鸟嘴啄)

- peacock : 雄孔雀
- peahen : 雌孔雀
- peafowl /ˈpiːfaʊl/:  孔雀统称
- 如果说“公鸡”，最好说rooster。不要说cock，因为在英语俚语中，cock表示penis
- penguin /ˈpɛŋɡwɪn/ : 企鹅
- crane /kreɪn/ ： 鹤，起重机
- pigeon /ˈpɪdʒ(ə)n/ :  鸽子

- ostrich /ˈɒstrɪtʃ/ : 鸵鸟
- emu /ˈiːmjuː/ : 鸸鹋，类似鸵鸟的大鸟
- owl /aʊl/ : 猫头鹰

- parrot /ˈparət/：鹦鹉
- bald-eagle /bɔːld/ : 白头海雕 (也叫秃头鹰，美国国鸟）
- hawk /hɔːk/ : 可以统称 eagle 之外的所有的鹰，一般体型没有 eagle 大
- crow /krəʊ/ (后边读音就是 row): 乌鸦

## 哺乳类

- rabbit /ˈrabɪt/ "啊"的音:    兔子

- camel /ˈkam(ə)l/ 也是 “啊” 的音： 骆驼

- cow /kaʊ/ (读音长大嘴巴): 泛指牛， 尤其是母牛， bull: 未阉割 (un-castrated /ˈkastreɪt/) 公牛，ox ： 阉割的公牛， dairy cow: 奶牛

- hippopotamus /ˌhɪpəˈpɒtəməs/: 河马

- squirrel /ˈskwɪr(ə)l/ : 松鼠， chipmunk /ˈtʃɪpmʌŋk/:花栗鼠( 有条纹，尾巴小)

  ![chipmunk](pic/chipmunk.jpg)

- hamster /ˈhamstə/ ： 仓鼠
- koala /kəʊˈɑːlə/: 考拉
- cheetah /ˈtʃiːtə/ ： 猎豹
- hyena /hʌɪˈiːnə/ ： 鬣狗，也称土狼

- otter /ˈɒtə/ : 水獭
- polar bear :北极熊
- mammoth /ˈmaməθ/ ： 猛犸象
- rhinoceros /rʌɪˈnɒs(ə)rəs/ : 犀牛
- antelope /ˈantɪləʊp/ ：羚羊
- giraffe /dʒɪˈrɑːf/ ：长颈鹿
- cattle /ˈkat(ə)l/ ： 牛的统称，本身是复数形式。
- beaver : 海狸

## 鱼类

- eel /iːl/： 鳗鱼
- jellyfish : 水母
- oyster /ˈɔɪstə/ ：牡蛎，包括生蚝
- scallop /ˈskɒləp/ ： 扇贝
- clam /klam/ : 蛤蜊
- salmon /ˈsamən/ ： 三文鱼，也叫鲑鱼
- tuna /ˈtjuːnə/ ： 金枪鱼
- herring /ˈhɛrɪŋ/ ：鲱鱼
-  toad /təʊd/ : 蟾蜍，即瘌蛤蟆
- goldfish：金鱼， 不是 golden fish（金色的鱼）



# 常用口语

- 有没有姐弟？ Do you have any siblings?  Yes, I do. I have one elder brother and one younger sister.
- running late : 要迟到了   Rachel, are't you running late?
- 走到三岔路口：come to a fork road. 

## 钱相关

- 纸币、钞票： banknote

- 如果专指一张纸币，可以说 bill

  Is the $1 bill the only banknote with George Washington's picture on it?

- 找零钱：have change == have smaller units of money. 注意 change 不用复数形式，have change for ...

  A: Do you have change for $10?

  B: Yes, here are nine ones and some **small change**.  小面值的硬币、零钱

  A: Here is **a 50 dollar bill**。

  B: Sorry, I don't have change for a fifty.

## 打电话相关

- 信号差： bad signal

- "Hello, hello, are you there ? "

- 打着打着断线了 get cut off == lose the connection 

  There is something wrong with my phone. I often get cut off.

  Hi. This is John again. We **got cut off just now**, so I'm calling you back. 刚刚

- 要挂机了： I gotta go.  gotta == have got to 



## 饮食相关

- 出去吃饭，跟谁 How often do you eat out? Who do you go with?

- 感兴趣，不太感兴趣  I am interested in Asian food, Western food is not my thing.

- 你喜欢吃辣吗？ Do you enjoy spicy food ? 

- 尝试过什么吗？ Have you ever tried Italian food ?

- 你关心 calorie 吗？ Are you concerned about calories when eating out ?

- 节食 Yes, I am. I'm on diet now, so this really matters to me.

- 年轻人喜欢快餐 The youth (泛指年轻人群体) in my country are big fans of fast food.

- 点餐 order 

  A: Waiter! **I ordered soup** 30 minutes ago!

  B: I'm sorry, Sir. I'll bring it **right away**.  马上

## 气候相关

- freezing == very very cold

  My office is freezing. 

## 阅读相关

- 从书中能学到什么 Books can broaden my horizon /həˈrʌɪzən/ about many things around the world.

- 读一本书需要多长时间 How long does it take you to finish a book ?

  Well, it depends on the length of the book, but it usually takes me a week to finish a 300-page book

- 旅行的时候带书吗？ Do you usually bring books when you travel ?

  When I'm at the airport or bus station, I read books to kill time.

- 调查问卷： questionnaire 

## 情绪\感觉相关

- 感到压力：under a lot of pressure,  pressure == stress. feeling stress.

  Students are under a lot of pressure at exam time.

  We are under a lot of pressure because we have to finish this job by Friday.

- 忘了  I forgot == It slipped my mind.  slip == escape from 后边直接跟...

  A. Did you mail my letter?  **mail** 在这里是动词，邮寄东西， mail the coupon.

  B. Oh, it slipped my mind. I'll do it tomorrow

  I was suppose to meet Fred last night, but I forgot. It slipped my mind. 


## 旅行\交通相关

- 旅行去过多少地方  How many places have you traveled to?

- 最喜欢的旅游景点 What is your favorite tourist attraction? That would be Venice /ˈvenəs/ city in Italy. I love riding the gondola /ˈgändələ/ (贡多拉) along the canals /kəˈnal/ while watching Italian people live their daily lives.

- 旅游的时候都干什么？ I often go sightseeing, take pictures, mingle with the local people and sample the local cuisine.

- 旅游的时候带什么 I usually pack my suitcase with some necessary items such as medicine, a map and a camera.

  pack 当名词时，是一包的意思，例如 a pack  of cigarette 一包烟, 打火机 lighter, 烟盒  cigarette package

  当动词时，是 fill 的意思，尤其是出远门的时候塞满箱子

- 乘什么交通工具 I prefer planes although it can be a little expensive. Planes are much faster than any other mode of transport.

- 个人或组团 Do you prefer traveling along or joining a guided tour?  I love backpacking with my friends who share the same interests as me.

- 交通高峰期 rush hour

  - 早高峰 morning rush hour
  - 晚高峰 evening rush hour

- 堵车了 be stuck in traffic  == be in a traffic jam

  Sorry I'm late. I was stuck in traffic for an hour.

  There's **no point in driving downtown** during rush hour. You'll be stuck in traffic.

  no point in doing sth 做某事毫无意义

  - There's no point in arguing with an idiot.

- 

##### 宗教相关

- 佛教
  - buddhist /'budist/ 佛教徒，佛教的 
  - temple 寺庙 
- 基督教 Christianity /kristi'ænəti/
  - Christian /'kristʃən/ 基督教徒，基督教的 
  - church 教堂
- 伊斯兰教
  - Muslim /muzlim/ 伊斯兰教徒（即穆斯林），伊斯兰教的 
  - mosque /mɔsk/ 清真寺

##### 结婚相关
- maid of honor 伴娘  
- best man 伴郎

##### prefer的用法
- 后边如果跟名词，就是prefer sth，如果跟动词，就是prefer to do sth 或 prefer doing sth， 

      I would prefer (doing)to do …… instead of doing……
  
  不会出现prefer to sth 的表述
- 有对比含义时，常用prefer A to B

##### live 的用法
- 作为动词时读/liv/， live a happy life.
- 做形容词时读/lʌɪv/, play the songs in a live performance
- live 固定搭配: 
  - live on 以…为生 Live on one’s salary. 

  - live with 忍受 I just had to learn to live with pain. 比较大的长期的忍受

    put up with 忍受一些琐事。

    - Your neighbor's dog is so noisy! How can you put up with the noise?

    - My office is freezing, but I put up with it because I love my job.

  - live for 为…而活着 Now I have someone to live for.

##### favor 的用法 
- do me a favor 帮忙 Could you do us a little favor?

- in favor of  有利于，赞成 Not everyone was in favor of this bill.

- favor 偏爱 Chance favors the prepared mind.






## 衣服、首饰、化妆品相关

- down 绒毛== fur,  down jacket 羽绒服
- pants 裤子  , 其中 pant 喘息，气喘
- panties 女士内裤
- underwear 内衣
- underpants 内裤
- 女用手袋 purse  /pə:s/
![purse](pic/purse.png)
- 男女用钱包 wallet \'wɔlit\，前部读音就是wall
![wallet](pic/wallet.png)
- 编织围巾 knit /nit/ a scarf /skɑ:f/
- flip-flop（或flip-flopper）专指人字拖。
- 凉鞋是sandal/'sand(ə)l/
- 拖鞋是slipper
- 眼线笔 eyeliner 我没画眼线 I am not wearing eyeliner tonight.
- 口红 lipstick

## 属相与星座

- 属相 China's zodiac
![shuxiang](pic/shuxiang.png)
野兔hare与hair同音
- 星座
![xingzuo](pic/xingzuo.png)

##### 描述人的品质
- He is so funny（有趣）
- She is very hard-working (勤勉). 
- serious (严肃认真)
- Charming (迷人的，可爱的)
- 说人朴实、单纯 pure-hearted

**20 adjectives to describe yourself**
- diligent: work hard on your work
- motivated == driven: I am motivated to learn english
- ambitious: strong motivation to succeed.
- collaborative: working together to achieve certain purpose.
- punctual: be punctual == on time 
- efficient: working quickly
- detail-oriented: pay close attention to details
- personable: easy to approach and each to talk to
- enthusiastic: showing a lot of excitement
- adaptable == flexible: ability to change according situations
- innovative: able to create new method to solve a problem
- organized: arrange things in a systematic way.
- experienced: have a lot of knowledge in a field for a long time
- methodical: doing thing according to standard process
- responsible: answer to something that you have control over
- reliable: can be account for
- insightful: showing deep understanding
- articulate: can speak intelligently and express very well
- resourceful: can easily over come problems
- proactive: instead of just reacting to the problem

##### 军队相关
- 美国军种
  - 陆军 the Army
  - 海军 the Navy/'neivi/ 
  - 空军 the Air Force
  - 海军陆战队 the Marine /mə'ri:n/ Corps
- 盖世太保 Gestapo \ɡes'tɑ:pəu\  德国纳粹的秘密警察
- 手铐 handcuff：可作名词和动词. The police handcuffed him.

##### 与大小便相关
- I would like to go number two. 我想去大便
- 大便的名词 bowel movement （BM）
- 尿 urine /'juərin/ 名词  urinate /'juərineit/ 不及物动词
- 撒尿 urination /[,juəri'neiʃən/ 名词

##### 与度量衡相关
- 温度
  - Fahrenheit /'færəhait/ temperature华氏温度。标准大气压下，水的沸点为212℉，冰点为32℉。 180个华氏温标长度等于100个摄氏温度 （Celsius /'selsiəs/）。摄氏=（华氏-32）*5/9。日常换算时，摄氏大概就是（华氏-30）的一半。
  - 1 克水升高 1 摄氏度所需的能量为 1 卡

- 西方国家常见长度： 12 inches = 1 foot = 1/3 yard
  - inch (简写为in. ) 约为2.54 厘米，最初来源于大拇指的长度；
  - foot （简写为ft .）约为 30.48 厘米，最初来源于脚的长度；
  - yard (简写为yd. ) 约为0.9144米。最初为从鼻子到伸展的指尖的距离。

![length](pic/length.png)

![weight](pic/weight.png)

##### dare三个重要用法
- dare to do something.  You are the only one who dares to admit mistakes.
- How dare you +(to) 不定式!  (已经做过了，你胆子真大啊！)
- Don’t you dare +(to) 不定式……（还没做，你敢！）

##### 带有强烈中国特色的词语
- 女士们，先生们，请大家起立，升中华人民共和国国旗，奏中华人民共和国国歌！

    Ladies and gentlemen, please rise for the national flag and the national anthem of the People's Republic of China!

- 今天,很高兴跟大家一起庆祝中华人民共和国成立54周年
  ​    It Is a great pleasure to join you all today to celebrate the 54th anniversary of the founding of the PRC.
  
- 全面建设小康社会 
  ​    build a fairly well-off society in an all-around manner.
  
- 前所未有的机遇 an unprecedented opportunity

- 繁荣稳定 prosperity /prɔs'periti/ and stability
- 改善通货膨胀和飞涨的房地产市场： tame inflation and a soaring housing market.
- 文化大革命： Cultural Revolution
- 抢救 salvage /'sælvidʒ/. The government should salvage its credibility(公信力)./kredi'biləti/
- 煽动，鼓动：agitate /'ædʒiteit/ Dalai Lama agitates for an independent Tibet /tɪ'bet/ .
- 自焚，自我牺牲：self-immolation /iməu'leiʃən/ The government blames Dalai for the self-immolations of about two dozen Tibetans.
- 腐败 corrupt /kə'rʌpt/ 动词、形容词. 
  
      腐败的官员: a corrupt officer.
      腐败的政府： a corrupt government

##### 来自英美剧
- 你认为我小气吗？ Do you think I am cheap? 小气的其他表述 stingy / 'stindʒi/ 
- Penny做花的时候，Sheldon说如果优化制作工艺，或许这能成为”viable /'vaiəbl/business” 可行的，可养活的
- dibs /dibz/ 权利： I pay for the beer so I have dibs on the last bottle. 这是名词，而且必须加s.对于something的dibs 用 have dibs on sth 或者 call dibs on sth. 如果想逃避责任，就指着鼻子说 no dibs

##### 类似的变形模式
- 貌似有不少以ing结尾的动词的过去式都是变成ung
  - 唱歌 sing --> sung    
  - 蚊虫的叮、蜇  sting ---> stung  (读音类似死党)

- 如果是连词结尾是个形容词，并且该连词做形容词来用，则保持原型，例如state-dependent communication, self-important 自高自大的
- 如果结尾是名词，则要加ed，做形容词，如warm-blooded 和蔼的，性情温和的，和善的 good-natured   She is a good-natured person


##### invite over
Invite over 邀请到家里
​    
​    Let’s invite Tony/some friends over for dinner.  
​    I would like to invite you over for dinner / to play video games.

##### expect的用法
1. expect sb/sth to do sth

       It is not realistic to expect individuals to adopt Bayesian learning in the real world.
       With convergence to an ideal state, you can expect the system to be in or near that ideal state.
2. expect sth

       I do not expect much success in this sphere \sfiə\.
3. expect that ……

       They are in trouble, but I expect they will muddle through 应付过去，混过去.
4. 不要在expect后边加 ing 形式
