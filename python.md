
## Why is Python so popular?

- 1991 年 Dutch programmer  Guido van Rossum 设计了 Python

- python 的名字并非来自于蟒蛇，而是英国一个喜剧团体 Monty Python. Guido 很喜欢他们的表演，喜剧的内涵和文化也被融入到了 Python 中，比如语法简约，尽量用自然语言而不是符号，减少不必要的复杂性，文档轻松。这从 Zen of python 就可以看出来。

  ```python
  import this
  
  The Zen of Python, by Tim Peters
  
  Beautiful is better than ugly.
  Explicit is better than implicit.
  Simple is better than complex.
  Complex is better than complicated.
  Flat is better than nested.
  Sparse is better than dense.
  Readability counts.
  . . .
  ```


- 用英语关键字，而不是用符号，例如 and, or 而不是 && , ||
- 用缩进设定 block 关系，而不是花括号，连 end 都省掉了。这些简洁的书写风格从与 JAVA，C++ 对比中可以清楚的感受到。

- 易上手，使得 python 成为初学编程人士的首选
- python 可以不考虑变量类型之类的细节（但有时很重要），将精力集中在解决问题而不是语法，可以高效的完成初始模型验证工作，很适合专业领域使用，例如机器学习，数据分析。
- python 内核部分功能不强，但是有很好的可扩展性。经过众人的努力，有各种各样的扩展函数库可供调用，解决特定的问题。

似乎一切成功都来源它的简洁易用，有越来越多的人采用它，为它贡献代码，功能越来越强大，形成良性循环。



## python 版本切换

1. 查看目前系统中有多少个 python 版本： `ls /usr/bin/python*`

一般会得到类似如下结果
```
/usr/bin/python 
/usr/bin/python2 
/usr/bin/python2.7 
/usr/bin/python3 
/usr/bin/python3.4 
/usr/bin/python3.4m 
/usr/bin/python3m
```

2. 查看当前用的哪个版本： `python --version`

###  本地用户层面的切换
只需要修改 .bashrc 文件，添加一个 alias

`alias python='/usr/bin/python3.4'`

这样再输入  python 就指向了 `/usr/bin/python3.4`

### 系统层面的切换
先看看系统中有哪些 python 版本
`ls /usr/bin/python*`
一般会有 python2.7, python3.5 等。

用 update-alternatives 命令，先查看 python 对应的几个替代

`update-alternatives --list python`

最初可能没有结果，需要额外设置，如下

`sudo update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1`

`sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.5 2`

此时在用 update-alternatives 查看 python 对应的几个替代

`update-alternatives --list python`

一般会有 python2, python3,然后设置希望的版本

`update-alternatives --config python` 

这个命令可以打开一个 python 版本的列表，选择对应的编号即可



## Python 与其他语言的区别

- 不同于 C++， python 没有 `x++` , `x--`，`++x`, `--x` 这类的操作，Julia 也没有。

- python 在进行逻辑运算“与”，“或”时，不是用符号 `&&`, `||`，而是直接用英语单词 `and`, `or`, `not`

- python 中的 list 与其他语言中的数组不同，list 中可以包含不同类型的元素

- 在基本 python 中，没有 array 的概念，只有在 numpy 中才定义了 array 及相关运算函数

- python 是彻底的面向对象编程，程序中定义的所有变量都是 object,例如

  ```python
  len("Hello")  # 这个操作实际上是查找 “Hello” 这个 object 的 length attribute，并返回
  
  d1 = {"first": 1, "second": 2, "third": 3}
  d1.items() # 这个操作实际上是查找 "d1" 这个 object 的 .item() method，并执行
  ```

  “Hello” 和 d1 分属于 str, dict 类，对于这一类变量，都包含了这些 attribute 和 method



## 什么是 abstraction (抽象)

例如用 import 调用 module 中的函数，我们并不需要知道该函数的具体实现过程，只要知道怎么用它就行。这种就是 abstraction。它的好处是可以让我们专心自己的工作，不去深究被调用函数的实现细节，通过 doc 了解其功能即可。



## 什么是 inheritance (继承)

在构造子类时，可以借助父类，从而减轻重复劳动。


```python
class Parent():
    def __init__(self, last_name, eye_color):
        self.last_name = last_name     
        # 这个 self 指代的是生成的 object, 可以不用 self，用其他的名字，只不过大家约定俗成都叫 self
        self.eye_color = eye_color
       
    
class Child(Parent):
    def __init__(self, last_name, eye_color, number_of_toys):
        Parent.__init__(self, last_name, eye_color) # 继承父类的某些变量和函数，外加特有的变量和函数
        self.number_of_toys = number_of_toys
        
        
miley_cyrus = Child("Cyrus", "Blue", 5)
print(miley_cyrus.number_of_toys)  # output 5
```



## 什么是 method overriding (函数重写)

就是在子类中重新定义父类中的函数和变量




## 安装特定版本 opencv 2.4 并导入虚拟环境


- 从 opencv 官网下载源码：http://opencv.org/releases.html， 并解压 `unzip opencv2.4.10.zip`
- 安装依赖

```
sudo apt-get install build-essential

sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
```
- 在解压的目录里，新建 release 文件夹，并在其中进行编译操作

```
cd opencv2.4.10

mkdir release

cd release

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..

```
- 安装

```
sudo make -j4

sudo make install
```
- 至此，系统 python 环境中应该可以调用 cv2 module 了
- 导入新建的 python 虚拟环境: 将 /release/lib/cv2.so 文件拷贝到 virtual_env/lib/python2.7/ 路径下。这样就可以在虚拟环境的 python 中调用 cv2 module 了。如果提示 numpy 未安装，则安装即可。



## built_in functions

Python 中的函数大体分三类：

1. built_in functions: 这些 func 是 python 自带的，只要启动了 python 就一直可用，不必 import 额外的 module。

![built_in_function1](pic/built_in_function1.png)

2. Python Standard Library: 不随 python 启动，需要 import 相应的 module 才能被调用
3. Third party Library:  不能直接 import，要先安装，一般用 pip 或者 easy_install。 装完之后就可以 import。



## 安装 module

### 一般安装方式

easy_install 和 pip 是最常用的两种安装工具。

- easy_install 是 python 自带的，直接`sudo easy_install <module>`
- pip 需要另外安装，先 `sudo easy_install pip` 或 `sudo apt install python-pip` ，再 `sudo pip install <module>`

pip 默认是把 module 安装到 `/usr/local/lib/python3.5/dist-packages/` 文件夹中，但只能超级用户才有权限，所以直接用 `pip install ` 会报错，权限不够。有两种解决办法：
1. 用超级用户 sudo. 一般不建议，不要把本地用户的东西装入系统路径中，要尽量保持系统 python 的简洁。
2. 后边加 `--user` 安装到当前用户某个文件夹下，一般默认是 `/home/qpliu/.local/lib/python3.5/site-packages
`

### pip 升级的问题

在用 pip 安装 module 时经常会出现提示 pip 有更新版本。此时，按照提示通过 `pip install --upgrade pip` 可能没法更新 pip。

面对这个问题，可以用 virtualenv 解决。用 virtualenv 设置新的环境，用 Python3。在新的环境中应该没有问题了。

### 常用 module 的安装

#### matplotlib

- 用 pip 安装： `pip install matplotlib`
- 用 apt 安装： `sudo apt-get install python3-matplotlib`

#### PIL 和 Pillow

都是关于图像处理的库，PIL比较早，Pillow是 PIL 的衍生版本，更好用。

两者不能同时安装。建议只用 Pillow

- python2: `pip install pillow`
- python3: `pip3 install pillow`

#### openCV

`pip install opencv_python`



## 很实用的 module

- webbrowser

  ```python
    import webbrowser
    webbrowser.open("http://url") # 可以在浏览器中打开指定网页。注意，一定要加 http
  ```

  - urllib: 包含了很多与  url 相关的操作，例如 urllib.urlopen，用法跟文件打开文命令 open( ) 类似，open( ) 是打开文件等待随后操作，urllib.urlopen( ) 是打开 url 链接等待随后操作。

    ```python
    import urllib
    connection = urllib.urlopen("some_url")
    output = connection.read()
    connection.close()
    ```


- time

  ```python
  import time
  time.ctime() # 显示当前时间和日期
  time.sleep(n_sec) # 暂停 n 秒
  ```

- os

  ```python
  import os
  # 查看当前路径
  saved_path = os.getcwd()
  # get file names from a folder
  file_list = os.listdir("path")
  # 改变路径
  os.chdir("path")
  # 文件改名
  os.rename(file_name, new_name)
  ```

- turtle 画图

  ```python
    import turtle
    window = turtle.Screen() # 生成一个窗口对象
    window.bgcolor("red") # 设置窗口背景颜色
    
    brad = turtle.Turtle() # 生成一个对象
    brad.shape("turtle") # 设置 turtle 外形，默认是箭头形状
    brad.color("yellow")
    brad.speed(2)
    
    brad.forward(100) # 前进
    brad.right(90) # 右转 90 度
  ```



## PEP8 python 命名风格

PEP stands for Python Enhancement Proposal.

PEPs are documents that can cover a wide range of topics, including proposed new features, style, and philosophy. 

涉及到变量命名的风格如下：

- package_name
- ClassName
- method_name
- field_name
- _private_something
- self.__really_private_field   
- _global
- 4 space indentation



## 带有下划线的变量

### 前置单下划线 `_var`

例如 Class 中定义变量 `self.__really_private_field` ，或者 函数名字 `_function()`。

C++ 中，在定义类时可以设定 private 和 public，Python 中没有这些概念，所以为了同样的区分，在定义变量名字的时候暗示别人这个变量应该当作 private 变量来使用。

一般来说，命名风格不会影响变量的使用，但有些特殊情况会的！

假设有 `my_module.py` 文件内容如下：

```python
# This is my_module.py:

def external_func():
    return 23

def _internal_func():
    return 42
```

在调用该 module 时如果用 `import my_module` 则一切 OK，命名无影响

```python
>>> import my_module
>>> my_module.external_func()   # 返回 23
>>> my_module._internal_func()  # 返回 42
```

但是如果用 `from my_module import *` 这种 wildcard import 的方式，则无法调用 underscore 开头的函数

```python
>>> from my_module import *
>>> external_func()   # 返回32
>>> _internal_func()  # 报错，not define
```

实际上，PEP8 中也建议了不要用 wildcard import 的方式，会把当前 namespace 搞乱。

### 后置下划线 `var_`

主要目的是避免与内置 keyword 冲突。

例如有些变量名字已经被内置 keyword 占用了，直接用肯定会报错，所以加个后置的 underscore 来避免冲突，这也是 PEP8 建议的

```python
>>> def make_object(name, class):
SyntaxError: "invalid syntax"

>>> def make_object(name, class_):
...     pass
```

### 前置双下划线 `__var`

上边介绍的前置、后置单下划线基本上只是约定俗称的，命名方式不会影响变量自身的使用。

但是前置双下划线不同。

在 Class 中如果 attributes （包括 variable 和 method）有前置双下划线，则在生成对象时自动在前边加上单下划线以及 Class 的名字，这是为了避免该 attribute 被子类覆盖。

例如，定义一个 Class 

```python
class Test:
    def __init__(self):
        self.foo = 11
        self._bar = 23
        self.__baz = 23
```

在用它生成对象时

```python
>>> t = Test()
>>> dir(t)
['_Test__baz', '__class__', '__delattr__', '__dict__', '__dir__',
 '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__',
 '__gt__', '__hash__', '__init__', '__le__', '__lt__', '__module__',
 '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__',
 '__setattr__', '__sizeof__', '__str__', '__subclasshook__',
 '__weakref__', '_bar', 'foo']
```

可以看到原本的 `foo`, `_bar` 两个变量都在，但是 `__baz` 名字变成 `_Test__baz`。

而且调用变量时也应该调用新名字，原本的变量不存在。

在定义 sub Class 时，该变量就不会被改写。例如

```python
class ExtendedTest(Test):
    def __init__(self):
        super().__init__()
        self.foo = 'overridden'
        self._bar = 'overridden'
        self.__baz = 'overridden'
```

生成对象之后

```python
>>> t2 = ExtendedTest()
>>> dir(t2)
['_ExtendedTest__baz', '_Test__baz', '__class__', '__delattr__',
 '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__',
 '__getattribute__', '__gt__', '__hash__', '__init__', '__le__',
 '__lt__', '__module__', '__ne__', '__new__', '__reduce__',
 '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__',
 '__subclasshook__', '__weakref__', '_bar', 'foo', 'get_vars']

>>> t2.foo
'overridden'
>>> t2._bar
'overridden'
>>> t2._ExtendedTest__baz
'overridden'
>>> t2._Test__baz
42
```

原本的 `_Test__baz` 还存在，而且值没有被覆盖。

### 前后双下划线 `__var__`

这种情况下，反倒没有任何影响了。

```python
class PrefixPostfixTest:
    def __init__(self):
        self.__bam__ = 42

>>> PrefixPostfixTest().__bam__
42
```

不过不建议使用这种命名方式，它可能会有其他的含义，例如 Class 中 `__init__` 就是 object constructor，`__call__` 可以让 object callable。

### 单独一个下划线 `_`

一般来说，这是一个普通变量的名字。用法跟普通变量相同。

一般用来表示这个变量不重要，根本不值得起名字

```python
>>> for _ in range(32):
...     print('Hello, World.')
```

又比如，某个位置必须要有个变量，否则语法错误，就用 `_` 来占位

```python
>>> car = ('red', 'auto', 12, 3812.4)
>>> color, _, _, mileage = car

>>> color
'red'
>>> mileage
3812.4
>>> _
12
```

但在 REPL 环境中，`_` 指代前一个 expression 的结果

```python
>>> 20 + 3
23
>>> _
23
>>> print(_)
23

>>> list()
[]
>>> _.append(1)
>>> _.append(2)
>>> _.append(3)
>>> _
[1, 2, 3]
```

如果 `_` 已经被明确赋值了，那么就不再指代前一个 expression 结果了

```python
>>> 4+5
9
>>> _
9
>>> _ = 1
>>> _
1
>>> 3+5
8
>>> _
1
```

### 总结

|                  Pattern                   |  Example  |                           Meaning                            |
| :----------------------------------------: | :-------: | :----------------------------------------------------------: |
|       **Single Leading Underscore**        |  `_var`   | Naming convention indicating a name is meant for internal use. Generally not enforced by the Python interpreter (except in wildcard imports) and meant as a hint to the programmer only. |
|       **Single Trailing Underscore**       |  `var_`   | Used by convention to avoid naming conflicts with Python keywords. |
|       **Double Leading Underscore**        |  `__var`  | Triggers name mangling when used in a class context. Enforced by the Python interpreter. |
| **Double Leading and Trailing Underscore** | `__var__` | Indicates special methods defined by the Python language. Avoid this naming scheme for your own attributes. |
|           **Single Underscore**            |    `_`    | Sometimes used as a name for temporary or insignificant variables (“don’t care”). Also: The result of the last expression in a Python REPL. |



## 注释

单行用 #, 多行时开头用 """ ，结尾也是 """，例如

```python
# 这里是单行注释
"""
这里是多行
注释
"""
```

严格地说，python 没有多行注释。上边的 triple quote 本身是 string，可以被赋值给变量的，例如

```python
a = """ this is a multiple
line string 
"""
```

在这种赋值操作情况下，它就是普通的string，只不过是多行的。

如果不是赋值，它确实起到了注释的作用。可以当成多行注释来用。



## 换行

有时程序太长， 按照编程习惯，一行程序最好不要超过80列。标准的换行实例：

```python
def callback(self, data): 
    msg = 'I have received : (%s), and I interpret it to (Hello World)' \               
        % data.data                                                              
    self.pub.publish(msg)   
```

- 用 `\` 表示本行命令未结束
- 下一行要比上一行缩进



## python 路径

进入python环境，然后

```python
import sys
print('\n'.join(sys.path))
```

会显示如下结果：

/home/qpliu/miniconda3/lib/python36.zip

/home/qpliu/miniconda3/lib/python3.6

/home/qpliu/miniconda3/lib/python3.6/lib-dynload

/home/qpliu/.local/lib/python3.6/site-packages

/home/qpliu/miniconda3/lib/python3.6/site-packages

如果提示哪个module找不到，就可以将module的文件夹ln -s 到这里某个路径下。



## 接受用户输入


```python
user_input = raw_input("Please enter something: ") # 接受的数据是 str 类型，即使用户输入的是数字

Please enter something: hel
    
print(user_input)
print(type(user_input))

# output
hel
<type 'str'>
```




## filter 的使用

list 每个元素都带入 lambda 方程中，成立的保留，不成立的剔除


```python
new_list = [1,2,3,4,5,6]

filter(lambda x: x%3 == 0, new_list
# 返回 [3, 6]
       
```



## assert

基本语法：

`assert condition`


效果等同于：

```
if not condition:
   raise AssertionError()
```

如果 condition == True，没有任何反应

如果 condition == False, 报错，同时可以返回一些设定的 msg


```python
assert False, "oh no! It failed!"
# 返回
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AssertionError: oh it is failed!
```




## print output format 

#### curl brace


```python
print("Shepered {} is {} years old".format('Mary', 32))

print('{} loss: {:.4f} Acc: {:.4f}'.format(1.23, 2.344, 12.343356)) # 可以控制显示格式
```

    Shepered Mary is 32 years old
    1.23 loss: 2.3440 Acc: 12.3434


#### 老式的 `%` 


```python
a = 100.001

print("%d, %f, %.3f" %(a, a, a))
```

    100, 100.001000, 100.001


- d: integer
- f: float
- .3f: 保留小数点后3位



## operations on string

### split

对 string 按照某个指定的分隔符进行分割，默认分隔符是 any whitespace，例如空格、Tab 等，返回 list

- 以默认分割符进行分割

  ```python
  txt = "welcome to the jungle"
  x = txt.split()
  print(x)
  ## result
  ['welcome', 'to', 'the', 'jungle']
  ```

- 自定义分隔符

  ```python
  txt = "hello, my name is Peter, I am 26 years old"
  x = txt.split(", ")  # 以 ", " 作为分隔符
  print(x)
  ## result
  ['hello', 'my name is Peter', 'I am 26 years old']
  ```

- 指定分隔出来的项数或者说是分割次数： split(" ", max)

  ```python
  txt = "apple#banana#cherry#orange"
  # setting the max parameter to 1, will return a list with 2 elements!
  x = txt.split("#", 1)
  print(x)
  ## result
  ['apple', 'banana#cherry#orange']
  ```

  





## operations on list

### list 与  numpy.ndarray 的区别
list 可以存放不同 type 的元素，但是 numpy.ndarray 类型就不可以，会强制转化成统一的类型

- list

  ```python
  a = [1, 'y', 2.3, "heko"]
  print(a) # [1, 'y', 2.3, 'heko']
  type(a) # list
  ```

- numpy.ndarray

  ```python
  import numpy as np
  np_a = np.array([1,2,3,4.2])
  np_b = np.array([1,2,3,"heelo"])
  print(np_b) # ['1' '2' '3' 'heelo']
  print(type(np_b)) # <type 'numpy.ndarray'>
  ```

### 添加元素

#### append

如果只添加一个，则用 .append( )


```python
a = [1,2,3]
a.append([4,5,6]) # append 只能将 argument 当成整个元素添加进去，所以最常见的形式是添加单个元素
print(a) # [1, 2, 3, [4, 5, 6]]
a.append(7)
print(a) #  [1, 2, 3, [4, 5, 6], 7] 
```


#### extend

如果要把另外一个 iterable 中的元素提取出来添加进去，则用 .extend( )


```python
a = [1,2,3]
b = [4,5,6]
a.extend(b)
print(a) # [1, 2, 3, 4, 5, 6]
```


#### insert

在指定 index 之前插入，跟 append 类似，以单个元素的形式插入


```python
a = [1,2,3,4]
a.insert(0, 9)
print(a) # [9, 1, 2, 3, 4]
```


### 删除元素

#### remove 

删除 list 中第一个与指定元素相同的元素


```python
a = [1,2,3,4,5]
a.remove(3)  # 指明被删的元素
print(a) # [1, 2, 4, 5]
```


#### pop
删除指定 index 的元素，如果不指定，就默认删除最后一个


```python
b = [1,2,3,4,5]
b.pop(2)  # 指明被删元素的 Index
print(b) # [1, 2, 4, 5]
```

#### del

```python
c = [1,2,3,4,5]
del c[2]  # 也可以 del 函数删掉某个元素
print(c) # [1, 2, 4, 5]
```


#### clear
清空 list，相当于 del a[:]


```python
a = [1,2,3,4]
a.clear()
print(a) # []
```


### list 当作堆栈和队列使用

- 从上述 append + pop 方法可以看出， list 可以天然地构造成 stack 堆栈，即 last-in, first-out，用 append 和 pop 就可以实现。效率很高。

- 但是用 list 构造 queue，即 first-in, first-out 效率就不高了，因为要从头插入，或者从头 pop 都需要依次改变其他元素的 index。此时用专门的 deque class 比较高效。

  deque = double end queue ，即两边都是 end，从哪边进入都可以。
  
  ```python
  from collections import deque
  queue = deque(["Eric", "John", "Michael"])
  queue.append("Terry")   # Terry arrives
  print(queue)            # deque(['Eric', 'John', 'Michael', 'Terry'])
  queue.append("Graham")  # Graham arrives
  print(queue)            # deque(['Eric', 'John', 'Michael', 'Terry', 'Graham'])
  queue.popleft()         # The first to arrive now leaves
  print(queue)            # deque(['John', 'Michael', 'Terry', 'Graham'])
  queue.pop()             # The last to arrive now leaves
  print(queue)            # deque(['John', 'Michael', 'Terry'])
  
  # 也可以 expend 一个 list
  
  d = deque([1,2,3,4,5])
  d.extendleft([0])
  d.extend([6,7,8])
  print(d)                # deque([0, 1, 2, 3, 4, 5, 6, 7, 8])
  
  # deque 还可以设置最大长度，如果超出了长度，就从入口的另外一边丢弃元素，例如
  d = deque(maxlen=30)
  ```
  
  


### 查找元素


```python
a = [1,2,3,4,5,1,2,3,4,2]
print(a.index(3))  # 返回第一个元素 3 所在的 index，即 2
print(a.count(2))  # 返回 list 中某个元素出现的次数，即 3
```


### 排序


```python
a = [1,4,2,1,5,7,2,4,7,3,2]
a.reverse()  # 把原 list 倒序排列
print(a)  # [2, 3, 7, 4, 2, 7, 5, 1, 2, 4, 1]
a.sort()  # sort, 默认升序
print(a)  # [1, 1, 2, 2, 2, 3, 4, 4, 5, 7, 7]
a.sort(reverse=True)  # sort，降序
print(a)  # [7, 7, 5, 4, 4, 3, 2, 2, 2, 1, 1]
```

### copy

由于 list 的特殊性，最好不要直接  a=b 这样赋值，两变量之间会有牵连

用 list 自带的 copy( ) 函数


```python
a = [1,2,3,4,5]
b = a.copy()
print(a)  # [1, 2, 3, 4, 5]
print(b)  # [1, 2, 3, 4, 5]
b[1] = 7   
print(a)  # [1, 2, 3, 4, 5]
print(b)  # [1, 7, 3, 4, 5]
```


### for 循环遍历

一般推荐用下面的形式进行遍历。
```python
for x in v1:
```

用 index 进行遍历的情况只有一种，即 list 被修改的情况，此时一般用 range 构造 index list
```python
for i in range(len(v1)):
    v1[i] += 1
```



### 在循环中对 list 的元素做删减操作

这个很重要，也很容易出错。

可以用两种方法：

1. 创建新函数： somelist = [x for x in somelist if not determine(x)]
2. 在原函数位置上修改：somelist[:] = [x for x in somelist if not determine(x)]

例如：

```python
x = ['foo','bar','baz']
y = x
x = [item for item in x if determine(item)]
# 此时，x 被赋予了新的 list，但是 y 依然指向就的 list

x = ["foo","bar","baz"]
y = x
x[:] = [item for item in x if determine(item)]
# 此时，修改是在原 list 地址上，x 和 y 都指向同一个修改后的 list
```



### 高维 list

对于高维 list，可以看作嵌套的 list，一层一层的去操作` list[1][2]` ，没有 `list[1,2]` 这种表达式！



## operations on Dictionary

### 返回 dict 的各项


```python
a = {"first": 1, "second":2, "third": 3}
print(a.items())  # 以 list of tuple 返回各项: [('second', 2), ('third', 3), ('first', 1)]
print(a.keys())  # 以 list 的形式返回各 key: ['second', 'third', 'first']
print(a.values())  # 以 list 的形式返回各 value: [2, 3, 1]
```

总之上述返回的都是 list 的形式。

以上语法只适用于 python 2.7 及之前版本

python 3 之后 a.keys( ) 返回的是 dict_keys([1, 2, 3]) 类型数据，并不是纯粹的 list。要想得到 list，可以用

```python
list(a.keys())
```




### 删除元素


```python
a = {"first": 1, "second":2, "third": 3}
del a["first"]     # 没有 method, 只能用额外的函数形式
print(a)  # {'second': 2, 'third': 3}
```





## list 和 dict 操作的执行速度对比

list 和 dict 操作速度本质上是存放形式的不同，dict 的 key 是hashed，只要知道了 Key，对其 value 的操作没有任何遍历查找的过程，直接进行操作。

- 查找操作：对于 lookup 查找操作，list 是顺序查找，最坏的情况是要遍历整个 list，而在 dict 中 key 是已 hash 形式存放的，如果已知了 key，查找对应的值是直接就找到了，不需要遍历。

- get, set 操作对于 list 来说都是 O(1), del 操作是 O(n)；而对于 dict 来说，这三种操作复杂度都是 O(1)



## operations on set

### 由 list 构造集合

```python
a = set([1,2,3,1,2])  # set([1,2,3]) 自动删掉了重复的元素
```

### 集合的并

```python
b = set([1,2,3,4])
a | b   # set([1,2,3,4])
```



## mutable variable, 以 list 为例

### 要分清 mutation （修改） 与 assign （赋值） 的区别。

- assign: 对于所有的变量，不管是 mutable 还是 immutable，赋值操作都是产生新的数据，赋给变量，这里面不涉及变量之间的牵连. 例如 p = [list]， q = p, p = [newlist]。此时，q 和 p 完全无关， p 指向 newlist, q 指向 list.
- mutable: 对于 immutable 变量，即如下 4 种： int, float, boolean, string，也不会存在变量的牵连，他们实际上没有 mutation，只有赋值操作，就是指向新的变量。但是对于 mutable 变量，如 list 和 dict，mutation 操作是改变现有指向的数据组，那么所有指向该数据组的其他变量都会受牵连。 

![list_mutation1](pic/list_mutation1.png)

![list_mutation2](pic/list_mutation2.png)

### 同理适用于 list of list 中的修改

![list_mutation3](pic/list_mutation3.png)

![list_mutation4](pic/list_mutation4.png)

另外，需要注意的是 `print data * 3` 这种操作，如果是要连接 str 或者 list 的话，这 3 个 data 实际上是指向同一个 data，因此，在后续操作中，改变一个就会连带改变其他两个。一定要注意！



## namespace

### 什么是 name

- Name (also called identifier) is simply a name given to objects. 
- Everything in Python is an object. 
- Name is a way to access the underlying object.

For example, when we do the assignment `a = 2`, here 2 is an object stored in memory and `a` is the name we associate it with. We can get the address (in RAM) of some object through the built-in function, `id()`. Let's check it.

```python
a = 2
print('id(2) = ' , id(2)) # ('id(2) = ', 19026272)
print('id(a) = ' , id(a)) # ('id(a) = ', 19026272)
```

我们再做一个实验，令 `a = a+1`，看如何变化

```python
a = 2
print('id(a) =', id(a)) # ('id(a) =', 19026272)
a = a+1
print('id(a) =', id(a)) # ('id(a) =', 19026248)
print('id(3) =', id(3)) # ('id(3) =', 19026248)
b = 2
print('id(2) =', id(2)) # ('id(2) =', 19026272)
```

上述过程可以用如下图示表示
![name_in_python](pic/name_in_python.jpg)

### 什么是 namespace

To simply put it, namespace is a collection of names. 主要有3个级别的 namespace

1. **The namespace containning build-in names**: A namespace containing all the built-in names is created when we start the Python interpreter and exists as long we don't exit. This is the reason that built-in functions like id(), print() etc. are always available to us from any part of the program.
2. **Module global namespace**: Each module creates its own global namespace. These different namespaces are isolated. Hence, the same name that may exist in different modules do not collide.
3. **Function and Class local namespace**: Modules can have various functions and classes. A local namespace is created when a function is called, which has all the names defined in it. Similar, is the case with class. 

![namespace](pic/namespace.jpg)


### Variable Scope

变量的 Scope 就是从程序的哪些地方可以直接访问该变量，即它的作用范围多大。与 namespace 对应， scope 也相应地分为 3 个，作用范围逐渐增大：

1. Scope of the current function which has local names
2. Scope of the module which has global names
3. Outermost scope which has built-in names

搜索顺序：先小后大，先在 local 函数中搜索，再去 module 中搜索，还找不到就去 build-in space 中搜索。

如果本地有变量，则用本地的，如果没有，就往外搜，如果想直接用外部的，则用 global 命令

#### global 与 local 变量例子

- 典型错误1：

  ```python
  Money = 2000
  def AddMoney():
      Money = Money + 1
      print Money
  AddMoney()
  print Money
  ```

  这样会报错，因为在函数中语句 `Money = Money +1` 让程序认为本地定义了 Money 变量，所以就用本地的，不会去外部搜索。但是本地的 Money 变量的定义又是有问题的，没有事先定义它的值，不能使用。

  可以指明 Money 就用 global 的，就不会有问题了：

  ```python
  Money = 2000
  def AddMoney():
      global Money
      Money = Money + 1
      print Money
  AddMoney()
  print Money
  ```

- 典型错误2：

  ```python
  SPLIT = False
  def fun_a():
      if something and SPLIT==False:
          do something
          SPLIT = True
  ```

  问题跟前边类似，不能直接在 等号 左边用 全局变量，否则程序会认为是在函数内部定义了该变量。

#### 查看 global 与 local names

用 globals() 和 locals() 命令可以查看当前 namespace 中的全局和局部变量、函数、类等，返回的是 directory 型的变量。



## Module

一个 Module 实际上就是一个 .py 文件，里面包含了一些具有相关性的函数、类和变量。这样调用起来比较方便。如果主文件中用到了 class，则一个好的编程习惯是将 class 定义在一个文件中，封装成 module，然后在主文件中调用。这样主文件比较简洁，而且程序各模块比较独立，条例清晰。在 ROS 中编写 node 文件时也可以尝试这样做。

### 构造和调用 Module 的简单例子

建立如下的 module 文件，命名 support.py

```python
def print_func(par):
	print "Hello: ", par
```

这个 module 只包含一个函数 print_func, 这里还没有 class。下面调用 module 中的函数

```python
import support   # module 的名字就是 module 的文件名
support.print_func("Zara")  # Hello: Zara
```

### 两种调用 Module 的方式

- 语法1

  ```python
  import module1[, module2, ... moduleN]
  ```

  用 import 调用 module 的时候，module 的名字就是 .py 文件的名字，只是不加扩展名。
  同一个 module 只加载一次，即使出现多次 import 的情况，也仅加载一次。因此，如果修改了 module 中的内容，需要重新加载，就要特别强调重新加载，用 `reload(module_name)`。 
  使用 module 中的函数时，**需要前缀该 module 的名字**，即`module.func()`。
  这样调用后，如果需要经常使用某一函数，**也可以重新命名一下** `newfunc = module.func`，这样就可以用 `newfunc()`代替了。

- 语法2

  ```python
  from module import attribute1[, attribute2, ... attributeN]
  ```

  这种调用仅仅加载 module 中指定的 attribute （函数，变量，类等），例如

  ```python
  from fib import fibonacci  # 从 fib 这个 module 中加载 fibonacci 进入当前工作空间
  ```

  类似的也可以从 module 中加载全部 attribute 

  ```python
  from fib import *  # 加载除 '__' 开头的所有 attribute
  ```

  这种调用是直接将 module 中的 attribute 引入到当前的 namespace 中，如果名字有冲突则会报错。

- 比较： 
  - import 方式的调用是将 module 的名字引入当前 namespace
  - from + import 的方式是将 module 中的内容引入当前 namespace



### 查看 module 内容，以 math 为例

用 dir() 可以查看 module 中包含的内容，例如

```python
import math
content = dir(math)
print content
```

上述命令会得到一个 list

```python
['__doc__', '__file__', '__name__', 'acos', 'asin', 'atan', 
'atan2', 'ceil', 'cos', 'cosh', 'degrees', 'e', 'exp', 
'fabs', 'floor', 'fmod', 'frexp', 'hypot', 'ldexp', 'log',
'log10', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 
'sqrt', 'tan', 'tanh']
```

其中

- `__name__` 存储了该 module 的name。

- `__file__` 存放了该 module 的路径


### package

package 本质上是一个文件夹，里面包含多个 module 文件或者其他 sub-package。通过调用 package 可以一次性的调用多个 module。例如
在文件夹 Phone 的路径下定义一个 module 文件 Pots.py 如下：

```python
def Pots():
	print "I am Pots Phone"
```

类似的，定义另外两个 module 文件 Isdn.py (定义了函数 Isdn()) 和 G3.py (定义了函数 G3()).
此外，定义一个特殊的文件 `__init__.py`，内容如下

```python
from Pots import Pots
from Isdn import Isdn
from G3 import G3
```

这样就构造了一个 Phone package，可以通过调用这个 package，一次性的调用三个 module 中的函数，例如

```python
import Phone
Phone.Pots()
Phone.Isdn()
Phone.G3()
```



## 为什么用  if \__name__ == '\__main__'

`__name__` is a built-in variable that returns the name of the module.

可以通过例子来看 `if __name__ == ‘__main__'` 的作用

- 假设有两个函数 foo.py 和 bar.py，有同样的内容

  ```python
  def main():
      print(__name__)
  
  main()
  ```

  运行这两个程序都会返回同样的结果，即变量 `__name__` 的值 `__main__`

- 修改 foo.py 函数，令它调用 bar.py，即

  ```python
  import bar
  
  def main():
      print(__name__)
      
  main()
  ```

  运行 foo.py 返回结果:

  ```python
  bar
  __main__
  ```

  这里可以看出来    
   - 当程序以主程序的形式直接运行时，`__name__` 是 `__main__`
   - 当程序以 module 的形式被其他程序调用时，`__name__` 就是这个 module 的名字

- 如果我们希望程序的 main 函数只在作为主程序时才运行（比较常见的情形是 module 程序调试时，会用一个 main 函数调用自定义的其他子函数，当这个 module 被其他程序调用是，自然是不希望这个 main 函数运行的），如果被调用，则希望只提供定义的函数，main 程序不要运行，则可以对 bar.py 做如下修改：

  ```python
  def main():
      print(__name__)
      
  if __name__ == '__main__':
      main()
  ```

  此时再运行 foo.py ，则 bar.py 中的 main() 函数不会运行，输出结果为：`__main__`



## try  ... except

主要用来 handle errors。下面用例子来说明：

假设我们有个程序需要用户输入数字

```python
number = int(input("Enter a number: "))
print(number)
```

类似于这种用户输入的情形，我们能够预料到用户可能不会乖乖地按照我们的意图输入，如果用户输入 `dadfafa`, 则程序会报错，类似于 `ValueError: ...`。

在整个程序中我们不希望这么一个小的、常见的输入错误就引发程序报错并退出，既然是这么常见的错误，我们应该可以恰当的应付，这就是 handle error。我们可以将程序修改一下：

```python
try:
    number = int(input("Enter a number: "))
    print(number)
except:
    print("Invalid Input")
```

此时，如果用户再输入不恰当的字符，程序不会报错退出，而是按照 except block 中的命令，打印出 Invalid Input.

但是这里的 except 部分对所有的 error 都一视同仁，只要 try block 里面的命令出了问题，都会有相同的应对措施。这无法反映具体的 error 到底处在哪里，我们也无法进行后续的维护工作。例如

```python
try:
    answer = 10/0
    number = int(input("Enter a number: "))
    print(number)
except:
    print("Invalid Input")
```

上述程序不能正确反映出错的本质原因。实际上是 10/0 的问题，所以更合适的程序应该是 handle 更加具体的 error，例如

```python
try:
    answer = 10/0
    number = int(input("Enter a number: "))
    print(number)
except ZeroDivisionError:
    print("Divided by zero")
except ValueError:
    print("Invalid input")
```

我们也可以不另外设定报错提醒，而是将报错的信息原样输出，例如

```python
try:
    answer = 10/0
    number = int(input("Enter a number: "))
    print(number)
except ZeroDivisionError as err:
    print(error)
except ValueError:
    print("Invalid input")
```

有时还会看到 pass，例如

```python
try:
    something
except: 
    pass
```

这里 pass 就是什么都不做



## file I/O

打开文件，本质上是返回了一个 object of the File type，所以后边才会有 object.read(), object.close() 等对象的常见操作。

###  open( ) and close( )

对文件的读写操作，一种方法是基于 open, close 模式的。

简单的说，先 open 一个文件，进行完读、写操作，最后 close

```python
file_str = open('file_path', 'mode')
```

其中, mode 有四种选择：

- r : read only , 默认
- w : 写入，会把之前的内容覆盖
- a : 添加到之前内容的尾部
- r+ : read + write

例如：

```python
file = open("path")
contents_of_file = file.read()
print(contents_of_file)
file.close()
```

写操作

```python
file_str.write('abd')
file_str.write('123') # 默认是写在同一行，如果要换行，需要在末尾加 \n 
file_str.close() # 程序最后必须有这个函数才能将内容实际写入文件。
```

读操作

```python
file_str.read() # 一次性读出句柄 file_str 指代文件中的所有内容，如果后边再用 .read() 则不会有任何内容读出
file_str.read(10) # 读出开头的10个字符
file_str.read(10) # 再读出后边的10个字符
file_str.readline() # 一行一行的读
file_str.readlines() # 读出所有内容，并以行为单位构成 list
for line in file_str:
    print line     # 这样也可以取出各行
for index, line in enumerate(file_str):
    if index == 25:
        print line  # 只取出第 26 行
```

### with / as 操作
上述 open, close 操作一定要记得最后的 close 操作。

相对安全的方式是用 with/as，可以自动 close



## 类与对象

### 类中变量

可以分为 2 类：

1. 与具体 object 相关的变量，称为**实例变量（instance variable）**，通过 self.variable 设置

2. 与具体 object 无关的变量，即不论生成哪个 object，只要是由该 class 生成的，大家都 share 这个相同的变量，称为**类变量（class variable）**，可以直接在 class 中定义

   ```python
   class Moive():
       """The content here will be shown in __doc__"""
       RANK = 1   # class variable 
       def __init__(self, title):
           self.title = title # instance variable
   ```

   在使用时，实例变量通过生成的实例调用

   ```python
   toy_story = Moive("toy_story")
   print(toy_story.title) # 通过 object.variable 调用
   ```

   类变量可以通过实例调用，也可以直接通过 class 的名字调用

   ```python
   toy_story.RANK  # 通过生成的实例调用
   module.Moive.RANK  # 直接通过 class 调用
   module.Moive.__doc__ # __doc__ 中的内容就是紧靠着 class Moive() 下面定义的那个 docstring。这个 docstring 如果是多行的，就用 triple quote，如果是单行的，普通的 string 定义就可以。注意，class 中有 docstring，module 内部函数中也有 docstring，前者用 __doc__ 的方式可以显示，后者用 help() 的方式可以显示。
   ```

### python 2 与 3 中 class 的区别

以下内容主要来自《Think Python》(2012) 书中 15、16、17 章

A user-defined type is also called a class.  所以类没什么神秘的，本质上是用户定义的一种类型，与 int, float, string 相似。

- python 2 中有两类 class

  - classic class: 定义方式

  ```python
  class Classic: 
  ```

  其中默认的 attribute 为

  ```python
  print(dir(Classic))
   ['__doc__', '__module__']
  ```

  - new-style class: 定义方式

  ```python
  class NewStyle(object):   # 从 object 这个类衍生而来
  ```

  其中默认的 attribute 为

  ```python
  print(dir(NewStyle))
  ```

  ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__']

  ```
  一般我们都希望定义一个 new-style class，所以在 python 2 中定义 classs 时，一般都是需要将 `object` 作为一个 argument 的
  
  ```

- python 3 中只有一个类，就是之前的 new-style class，而且不需要再指明 `object` 了，定义方式为：

  ```python
  class Foo():
  ```

### empty class 空类与添加 attributes

- 定义空类
  可以定义空类，即只有一行生命，没有任何实质内容，但是并不能什么都不写，常见的定义方式：

  - 用 pass 占位

    ```python
    class Foo():
        pass
    ```

  - 用 docstring 注释占位

    ```python
    class Foo():
        """This is an empty class"""
    ```

    注意： 如果仅写第一行，则会报错

- 向空类的对象中添加 attributes。

  假设定义了如下空类：

  ```python
  class Point():
      """Represent a point in 2-D space."""
  ```

  产生 Point 类的一个对象 blank 并添加 attributes

  ```python
  blank = Point()
  blank.x = 3.0
  blank.y = 4.0
  print(blank.x)  # 3.0
  print(blank.y)  # 4.0
  ```

  

### 关于对象的基本操作

#### 查看对象附带的函数

dir( ) 不仅可以查看 module 的内容，还可以查看任意 object 的内容，比如其附带的 method，例如
```python
shout = 'heelo'
dir(shout)
```
会返回如下结果
```python
['__add__', '__class__', '__contains__', '__delattr__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__getslice__', '__gt__', '__hash__', '__init__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '_formatter_field_name_split', '_formatter_parser', 'capitalize', 'center', 'count', 'decode', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'index', 'isalnum', 'isalpha', 'isdigit', 'islower', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']

```

#### 查看附带函数的作用
```python
help(shout.islower)
```
会返回对该函数的简要说明。而这个简要的说明就是 module 中 docstring 里面的内容



## python2  与 python3 的区别

1. 前边提到的，定义 class 时 python2 要特意指明继承 object 类， python3 则不需要

2. python2 中 print "something" (以 keyword 的形式出现) 或者 print("something") （以函数的形式出现）都可以，python3 中必须 print("something")

3. python2 中 3/2 = 1，即 Int/Int = Int，python3 中不会这么死板， 3/2 = 1.5

4. python2 中 range( ) 函数可以直接返回一个 list，而 xrange( ) 则比较懒，在需要的时候才产生 list 中的一个数; python3 中 range( ) == xrange( )，即都是需要的时候才产生，避免占用太多内存。在 python3 中

   ```python
   type(range(3))  # range
   ```

   可以通过 Index 读取 range type 变量中的数据

   ```python
   range(3)[1]  # 1
   ```

   slicing 操作返回的也是 range type

   ```python
   range(3)[1:2]  # range(1, 2)
   ```

如果想在 python2 中使用上边 Python3 中 print 和 division 的功能，可以通过调用 `__future__` package 实现：


```python
from __future__ import print_function, division
```

即在 python 2 中以 python 3 的形式使用这些功能。



## NumPy + SciPy + Matplotlib + Pandas
- NumPy = Numerical Python. 是 python 中科学计算的核心 library, 提供了丰富的高维向量和矩阵运算函数。它是用 C 写的编译好的程序，所以运行速度很快。所以要实现某种操作，尽量用 NumPy 提供的函数，而避免在 python 中写 for 循环之类的程序。
- SciPy = Scientific Python. 在NumPy的基础上提供了丰富的优化、回归、傅立叶变换等函数，很多是特定领域使用的函数，如图像处理领域等。
- Matplotlib 提供丰富的画图、可视化函数
- Pandas 提供了丰富的数据结构、数据表处理、时间序列函数  (Python data analysis library)

安装时，要先装NumPy，再在此基础上安装SciPy.

## NumPy

### array

- 基本的 python 中没有 array 的概念。在 numpy 中为了向量和矩阵运算才构造了 array，可以理解成二维数组，用 tuple 指定其中的元素。
- array 中的元素必须是相同类型的，而原本 list 则可以存放不同类型的数据
- np.array(*) 是函数，用来构造 np.ndarray type 的数据。所以 np.array 不是数据类型，只是构造 ndarray 类型数据的方式、函数
- array 的 rank 就是有几个维度
- array 的 shape 就是一个 tuple，包含了各维度上的长度

### 由 list 转化成 array

#### 例子：对比 list 与 array

```python
import numpy as np # 可以去掉 "as np"，下边写函数时用 “numpy.array” 也一样，用 np 更方便些
```

```python
# 创建一个 list 保存若干摄氏温度值
cvalues = [20.1, 20.8, 21.9, 22.5, 22.7, 22.3, 21.8, 21.2, 20.9, 20.1]
type(cvalues) # list
```

**list 在运算中的缺陷:** list 中的元素可以读取，但是不能直接将运算施加于 list 之上，例如要将 list 中的摄氏温度转化成华氏温度，只能针对每个元素进行操作：

```python
fvalues = [x*9/5 + 32 for x in cvalues] 
```

将上述 list 转换成 numpy array 之后就可以直接对向量进行操作，应用向量的运算法则

```python
# 将上述 list 转换成一维的 numpy array
C = np.array(cvalues)  # np.array(list)
print(type(C)) # numpy.ndarray 此为数据类型
print(C.shape) # (10, )
```

```python
# 对于 numpy array 可以进行标准的向量操作
fC = C * 9 / 5 + 32
```

```python
# 时间对比
v = np.random.randint(0,100,1000)
%timeit v+1   # 985 ns ± 22.9 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
lst = list(v)
%timeit [val+2 for val in lst] # 130 µs ± 197 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)
```

numpy array 在内存占用和运算时间方面都优于原有的 python list.

#### 0 维向量

```python
x = np.array(42)
print("x: ", x)  # x:  42
print("The type of x: ", type(x))  # The type of x:  <class 'numpy.ndarray'>
print("The dimension of x: ", np.ndim(x))  # The dimension of x:  0
```

#### 1 维向量

```python
F = np.array([1, 1, 2, 3, 5, 8, 13, 21])
V = np.array([3.4, 6.9, 99.8, 12.8])
print("F: ", F)  # F:  [ 1  1  2  3  5  8 13 21]
print("V: ", V)  # V:  [ 3.4  6.9 99.8 12.8]
print("Type of F: ", F.dtype)   # F.dtype 返回 F 中元素的 type： Type of F:  int64
print("Type of V: ", V.dtype)   # Type of V:  float64
print("Dimension of F: ", np.ndim(F)) # Dimension of F:  1
print("Dimension of V: ", np.ndim(V)) # Dimension of V:  1
```

#### 2 维及多维矩阵

```python
# 以 list 嵌套的形式构造多维矩阵
A = np.array([[3.4, 8.7, 9.9], [1.1, -7.8, -0.7], [4.1, 12.3, 4.8]])
print(A) 
# [[ 3.4  8.7  9.9]
#  [ 1.1 -7.8 -0.7]
#  [ 4.1 12.3  4.8]] 
print(A.ndim) # 2
```

```python
B = np.array([[[111, 112], [121, 122]], 
              [[211, 212], [221, 222]], 
              [[311, 312], [321, 322]]])
print(B)
# [[[111 112]
#   [121 122]]
#  [[211 212]
#   [221 222]]
#  [[311 312]
#   [321 322]]]
print(B.ndim) # 3
```

```python
# np.shape可以返回多维矩阵中各维的长度
x = np.array([[67, 63, 87], 
              [77, 69, 59],
              [85, 87, 99],
              [79, 72, 71],
              [63, 89, 93],
              [68, 92, 78]])
print(np.shape(x))  # (6, 3)
print(x.shape)  # array 对象也有一个 property 保存 shape： (6, 3)
y = x.reshape(2,9)  # reshape 可以用来改变 array 的 shape
print(x)
#[[67 63 87]
# [77 69 59]
# [85 87 99]
# [79 72 71]
# [63 89 93]
# [68 92 78]]
print(y)
#[[67 63 87 77 69 59 85 87 99]
# [79 72 71 63 89 93 68 92 78]]
```

```python
print(B.shape) # (3, 2, 2)
```


### 直接生成 array 

#### arange

arange([start,] stop[, step], [, dtype=None])

注意区分：在基本的 python 中是 range(a, b)

```python
a = np.arange(1,10)
print(a) # [1 2 3 4 5 6 7 8 9]
```

```python
# 可以设定开始、结束、步长 
c = np.arange(0.5, 10.4, 0.8)
print(c) # [ 0.5  1.3  2.1  2.9  3.7  4.5  5.3  6.1  6.9  7.7  8.5  9.3 10.1]
```


对比 python 中的命令 range
- python3 中是 range 类型，需要时才产生。 Python2 中直接生成 list。 arange 也是直接生成 list。
- **range 只能接受整数 arg, 而 arange 可以接受 float arg**

```python
b = range(1,10) 
print(b)  # range(1, 10)
print(list(b))  # [1, 2, 3, 4, 5, 6, 7, 8, 9]
```

#### linspace

`linspace(start, stop, num=50, endpoint=True, retstep=False)  `

其中 retstep == return step

linspace 做的事情与 arange 几乎相同，主要区别有两点：
1. linspace 产生的 list 中包含 end point
2. linspace arg 中设定了 list 的元素数目，而不是步长，这与 arange 正好相反

```python
print(np.linspace(1,10)) 
```

```
[ 1.          1.18367347  1.36734694  1.55102041  1.73469388  1.91836735
  2.10204082  2.28571429  2.46938776  2.65306122  2.83673469  3.02040816
  3.20408163  3.3877551   3.57142857  3.75510204  3.93877551  4.12244898
  4.30612245  4.48979592  4.67346939  4.85714286  5.04081633  5.2244898
  5.40816327  5.59183673  5.7755102   5.95918367  6.14285714  6.32653061
  6.51020408  6.69387755  6.87755102  7.06122449  7.24489796  7.42857143
  7.6122449   7.79591837  7.97959184  8.16326531  8.34693878  8.53061224
  8.71428571  8.89795918  9.08163265  9.26530612  9.44897959  9.63265306
  9.81632653 10.        ]

```

```python
# 如果设定了 retstep，可以同时返回间隔长度
samples, spacing = np.linspace(1, 10, retstep=True)
print(samples)
print(spacing)
```

```
[ 1.          1.18367347  1.36734694  1.55102041  1.73469388  1.91836735
  2.10204082  2.28571429  2.46938776  2.65306122  2.83673469  3.02040816
  3.20408163  3.3877551   3.57142857  3.75510204  3.93877551  4.12244898
  4.30612245  4.48979592  4.67346939  4.85714286  5.04081633  5.2244898
  5.40816327  5.59183673  5.7755102   5.95918367  6.14285714  6.32653061
  6.51020408  6.69387755  6.87755102  7.06122449  7.24489796  7.42857143
  7.6122449   7.79591837  7.97959184  8.16326531  8.34693878  8.53061224
  8.71428571  8.89795918  9.08163265  9.26530612  9.44897959  9.63265306
  9.81632653 10.        ]
0.1836734693877551

```

#### 全0 ， 全1 ， 单位矩阵

```python
# 默认是 float 类型，可以通过 dtype 设置为其他类型
E = np.ones((2, 3))  # 注意：是以 tuple 的形式给进去的
print(E)
F = np.ones((3, 4), dtype=int)
print(F)
# np.zeros 同理
```

```
[[1. 1. 1.]
 [1. 1. 1.]]
[[1 1 1 1]
 [1 1 1 1]
 [1 1 1 1]]
```

```python
# 还可以生成与某一矩阵 shape 相同的 0 或 1
x = np.array([2,5,18,14,4])
E = np.ones_like(x)
print(E)
F = np.zeros_like(x)
print(F)
g = np.empty_like(x) # 生成相同 shape 的空 array
```

```
[1 1 1 1 1]
[0 0 0 0 0]
```

```python
# eye 可以生成单位矩阵，完全形式：  eye(N, M=None, k=0, dtype=float)
np.eye(4)  # 如果只想生成一个简单的4×4方阵
```

```
array([[1., 0., 0., 0.],
       [0., 1., 0., 0.],
       [0., 0., 1., 0.],
       [0., 0., 0., 1.]])
```

```python
# 如果想产生更加特殊的单位矩阵
np.eye(5, 8, k=1, dtype=int)
```

```
array([[0, 1, 0, 0, 0, 0, 0, 0],
       [0, 0, 1, 0, 0, 0, 0, 0],
       [0, 0, 0, 1, 0, 0, 0, 0],
       [0, 0, 0, 0, 1, 0, 0, 0],
       [0, 0, 0, 0, 0, 1, 0, 0]])
```

![eyeMatrix](pic/eyeMatrix.png)

```python
# Exercises
v = np.array([0, 1, 2, 3, 4, 5]) # create an 1D array "v"
print(v)
v_odd = v[1::2]  # create a new array consisting of the odd indices of "v"
print(v_odd)
v_back = v[::-1] # create a new array in backwards ordering from "v"
print(v_back)
m = np.array([[11,12,13,14],
              [21,22,23,24],
              [31,32,33,34]]) # create an 2D array "m"
print(m)
m_back = m[:,::-1]   # create a new array with elements of each row in reverse order
print(m_back)
```

```
[0 1 2 3 4 5]
[1 3 5]
[5 4 3 2 1 0]
[[11 12 13 14]
 [21 22 23 24]
 [31 32 33 34]]
[[14 13 12 11]
 [24 23 22 21]
 [34 33 32 31]]
```

#### 特殊构造

- numpy.vstack() 可以将两个向量或矩阵垂直堆在一起。

  ```python
  a = np.array([1, 2, 3])  # 可以是 array，也可以是普通的 list
  b = np.array([2, 3, 4])
  print(np.vstack((a,b)))  # 注意，vstack 只接受一个 argument，两个 list or array 要以 tuple 的形式送进去
  # output
  array([[1, 2, 3],
         [2, 3, 4]])
  
  a = np.array([[1], [2], [3]])
  b = np.array([[2], [3], [4]])
  np.vstack((a,b))
  array([[1],
         [2],
         [3],
         [2],
         [3],
         [4]])
  ```

- 类似地，numpy.hstack() stack arrays in sequence horizontally

### 读取 array 中数据

#### 各维依次提取

```python
F = np.array([1,1,2,3,5,8,13,21])
print(F[0])  # 1
print(F[-1]) # 21
A = np.array([[3.4, 8.7, 9.9], 
              [1.1, -7.8, -0.7],
              [4.1, 12.3, 4.8]])
print(A[1][0])  # 1.1
```

#### 各维同时提取

上面读取多维矩阵的方法并不高效，它相当于先提取出了 array A[1]，然后从中取出 index 为 0 的元素，过程如下

```python
temp = A[1]
print(temp) # [ 1.1 -7.8 -0.7]
print(temp[0]) # 1.1
```

一种更好的方法是同时指定各维 index

```python
print(A[1, 0]) # 1.1
```

#### slicing

```python
# 为了选取多个元素，可以用 slicing 的方法，index_a : index_b 包括头，不包括尾
S = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print(S[2:5])  # [2 3 4]
print(S[:4])   # [0 1 2 3]
print(S[6:])   # [6 7 8 9]
print(S[:])    # [0 1 2 3 4 5 6 7 8 9]
```

```python
# 对于多维矩阵的 slicing，就是在每一维上 slicing， 互不干涉
A = np.array([[11, 12, 13, 14, 15],
              [21, 22, 23, 24, 25],
              [31, 32, 33, 34, 35],
              [41, 42, 43, 44, 45],
              [51, 52, 53, 54, 55]])
print(A[:3, 2:])
```

```
[[13 14 15]
 [23 24 25]
 [33 34 35]]
```

**注意：** 

- 如果用 slicing 提取一行或一列数据，得到的 array 与原 array 维度相同

- 如果标量提取，则新的 array 会降维

  ```python
  a = np.array([[1,2,3,4], 
                [5,6,7,8], 
                [9,10,11,12]])
  row_r1 = a[1, :]    # Rank 1 view of the second row of a
  row_r2 = a[1:2, :]  # Rank 2 view of the second row of a
  print(row_r1, row_r1.shape)  # Prints "[5 6 7 8] (4,)"
  print(row_r2, row_r2.shape)  # Prints "[[5 6 7 8]] (1, 4)"
  # 列提取同样适用
  ```

#### slicing 内存共享问题

slicing 在 numpy 中是 view 而不是 copy，这大概也是它运算高效的一个原因吧，并没有新建一个 array

```python
# 用 slicing 提取出来的 sub array 在 numpy 与 python 中是不一样的
A = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
S = A[2:6]  # S 还是指向了原 array A 中的元素，即 S 和 A 共享了部分元素
S[0] = 22
S[1] = 23
print(A)   # [ 0  1 22 23  4  5  6  7  8  9]
print(S)   # [22 23  4  5]
A[4] = 111 
print(S)   # [ 22  23 111   5]
np.may_share_memory(A, S) # 用 np.may_share_memory() 可以判断两个 array 是否有内存重叠: True
```

```python
# 在原 python 中的 list，不会出现这样的问题
lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
lst2 = lst[2:6]
lst2[0] = 22
lst2[1] = 23
print(lst)  # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
np.may_share_memory(lst, lst2)  # False
```

#### integer array indexing 

可以用 array 的形式指定提取哪几行哪几列

```python
a = np.array([[1,2], [3, 4], [5, 6]])
print(a[[0, 1, 2], [0, 1, 0]])  # Prints "[1 4 5]"
# 相当于
print(np.array([a[0, 0], a[1, 1], a[2, 0]]))  # Prints "[1 4 5]"
```

#### boolean array indexing

常用来提取满足一定条件的元素

```python
a = np.array([[1,2], [3, 4], [5, 6]])
bool_idx = (a > 2)   # Find the elements of a that are bigger than 2;
                     # this returns a numpy array of Booleans of the same
                     # shape as a, where each slot of bool_idx tells
                     # whether that element of a is > 2.

print(bool_idx)      # Prints "[[False False]
                     #          [ True  True]
                     #          [ True  True]]"

print(a[bool_idx])  # Prints "[3 4 5 6]"

# We can do all of the above in a single concise statement:
print(a[a > 2])     # Prints "[3 4 5 6]"
```


### dtype 哪里来的？

在前边生成 array 时经常会有参数选择 dtype。如果不设置，系统会自己判断，也可以主动设置，得到希望的数据类型的元素。这里可以送进去的参数都是 np.dtype 类型的数据类型，包括系统原有的 int, float，和 numpy 自定义的 np.int16, np.int32 等。

```python
# 生成一个dtype 为 int16的 array
lst = [[3.4, 8.7, 9.9],
       [1.1, -7.8, -0.7],
       [4.1, 12.3, 4.8]]
A = np.array(lst, dtype=np.int16)
print(A)
print(A.dtype)
```

```
[[ 3  8  9]
 [ 1 -7  0]
 [ 4 12  4]]
int16

```

### array copy

```python
# x.copy 即可
x = np.array([[42, 22, 12],
             [44, 53, 66]])
y = x.copy()  # 完全新东西
z = x
x[0,0] = 1001
print(x)
print(y)
print(z)
print(np.may_share_memory(x,y))
print(np.may_share_memory(x,z))
```

```
[[1001   22   12]
 [  44   53   66]]
[[42 22 12]
 [44 53 66]]
[[1001   22   12]
 [  44   53   66]]
False
True
```

### numpy 中的运算

#### array 与标量

```python
# 向量+标量 相当于加到每一个元素
lst = [2, 3, 7.9, 3.3, 6.9, 0.11, 10.3, 12.9]
v = np.array(lst)
v = v + 2
print(v)  # [ 4.    5.    9.9   5.3   8.9   2.11 12.3  14.9 ]
# 对于标量的减、乘、除同理
```

#### array 与 array

```python
# 对应元素的运算，所以一定要保证元素数目相同
A = np.array([[11,12,13],
              [21,22,23],
              [31,32,33]])
B = np.ones((3,3))
print(A+B)
print(A*(B+1))  # 注意，这里的 array 的乘是对应元素相乘，不是标准的矩阵乘法。只有把 array 转化成 matrix 之后才能对两个 matrix 用 × 乘法。
```

```
[[12. 13. 14.]
 [22. 23. 24.]
 [32. 33. 34.]]
[[22. 24. 26.]
 [42. 44. 46.]
 [62. 64. 66.]]
```

```python
# 为了实现 array 之间向量或矩阵法则的运算，可以用 dot 函数
a = np.array([1,2,3])
b = np.array([2,3,4])
print(np.dot(a,b))  # 也可以用 a.dot(b)
print(np.dot(A,B))
```

```
20
[[36. 36. 36.]
 [66. 66. 66.]
 [96. 96. 96.]]
```

```python
# m*n 维与 n*k 维，例如 2×3 与 3×4
a = np.array([[1,2,3],
              [2,3,4]])
print(a.shape)
b = np.array([[1,2,3,4],
              [2,3,4,5],
              [3,4,5,6]])
print(b.shape)
c = np.dot(a,b) # 如果用 (b,a) 会报错，维度不匹配
print(c)
print(c.shape)
```

```
(2, 3)
(3, 4)
[[14 20 26 32]
 [20 29 38 47]]
(2, 4)
```

**array 之间乘法的总结**：

- 两个一维 array 相乘，得到标量
- 一维 array 和二维 array 相乘，得到一维 array
- 两个二维 array 相乘，得到二维 array

```python
v = np.array([9,10])
w = np.array([11, 12])

x = np.array([[1,2],[3,4]])
y = np.array([[5,6],[7,8]])

# Inner product of vectors; both produce 219
print(v.dot(w))
print(np.dot(v, w))

# Matrix / vector product; both produce the rank 1 array [29 67]
print(x.dot(v))
print(np.dot(x, v))

# Matrix / matrix product; both produce the rank 2 array
# [[19 22]
#  [43 50]]
print(x.dot(y))
print(np.dot(x, y))
```

#### 转化成矩阵乘法

```python
#  numpy 不但提供了 np.dot 实现矩阵相乘，还提供了 np.mat() 将 array 转化成真正的矩阵，从而之间运算
A = np.array([[1,2,3],
              [2,2,2],
              [3,3,3]])
B = np.array([[3,2,1],
              [1,2,3],
              [-1,-2,-3]])
print(type(A))
MA = np.matrix(A)  # 转化成 matrix
print(type(MA))
MB = np.matrix(B)
print(MA*MB)
```

```
<class 'numpy.ndarray'>
<class 'numpy.matrixlib.defmatrix.matrix'>
[[ 2  0 -2]
 [ 6  4  2]
 [ 9  6  3]]
```

```python
# np.matrix 是 np.array 的一个派生类，可以用 np.matrix 直接生成一个 2D 的矩阵（也只能是 2D），进行矩阵操作
x = np.matrix([[2,3],
               [3,5]])
y = np.matrix([[1,2],
               [5,-1]])
x*y
```

```
matrix([[17,  1],
        [28,  1]])
```

#### 转置

一般的矩阵用 `T` 即可

```
x = np.array([[1,2], [3,4]])
print(x)    # Prints "[[1 2]
            #          [3 4]]"
print(x.T)  # Prints "[[1 3]
            #          [2 4]]"

# Note that taking the transpose of a rank 1 array does nothing:
v = np.array([1,2,3])
print(v)    # Prints "[1 2 3]"
print(v.T)  # Prints "[1 2 3]"
```

#### 求和

- 对于向量来说，求和就是简单的把元素相加
- 对于矩阵来说
  - a.sum( ) 得到的是矩阵中所有元素的和
  - a.sum(axis=0) 上下相加求和，得到一维向量
  - a.sum(axis=1) 左右相加求和，得到一维向量

#### array 之间的比较运算和逻辑运算

```python
# 默认情况下，array 之间的比较是元素之间的比较
A = np.array([ [11, 12, 13], [21, 22, 23], [31, 32, 33] ])
B = np.array([ [11, 102, 13], [201, 22, 203], [31, 32, 303] ])
A == B
```

```
array([[ True, False,  True],
       [False,  True, False],
       [ True,  True, False]])
```

```python
# 为了整体比较两个 array，需要用专门的函数 np.array_equal()
print(np.array_equal(A,B))  # False
print(np.array_equal(A,A))  # True
```

```python
# 在用逻辑运算 and, or, not 时，对于 array 一般我们是希望元素之间运算的，可以用 np.logical_or() 和 np.logical_and()
a = np.array([ [True, True], [False, False]])
b = np.array([ [True, False], [True, False]])
print(np.logical_or(a, b))
print(np.logical_and(a, b))
```

```
[[ True  True]
 [ True False]]
[[ True False]
 [False False]]
```


#### broadcasting

前边说 array 的基本运算，都要求 array 的 shape 相同才能对应元素运算。

实际上，在某些条件下，不同 shape 的 array 也可以运算，采用 broadcasting 的方式

```python
A = np.array([[11, 12, 13], [21, 22, 23], [31, 32, 33]])
B = np.array([1, 2, 3])
print("Multiplication with broadcasting: ")
print(A * B) # 用 B×A 也是可以的，结果相同。 把维数小的 array 反复乘上去
print("... and now addition with broadcasting: ")
print(A + B)
C = np.array([[1],
              [2],
              [3]])
print("... and now try another one")
print(A * C)
print(C.shape)  
print(C.transpose().shape)  # array 转置可以用 x.transpose() 的方式
```

```
Multiplication with broadcasting: 
[[11 24 39]
 [21 44 69]
 [31 64 99]]
... and now addition with broadcasting: 
[[12 14 16]
 [22 24 26]
 [32 34 36]]
... and now try another one
[[11 12 13]
 [42 44 46]
 [93 96 99]]
(3, 1)
(1, 3)
```

**直观的理解**： shape 比较小的那个 array 会尝试通过复制的方式补齐元素数目，它会自动判断应该是复制行还是复制列

![broadcasting](pic/broadcasting.png)

```python
# 另一个比较极端的例子，一列 与 一行 做运算
A = np.array([[10, 20, 30]])  # 2D 1行 3列
print(A)
B = np.array([[1,2,3]]).transpose() # 2D 3行 1列
print(B)
print(B*A)
```

```
[[10 20 30]]
[[1]
 [2]
 [3]]
[[10 20 30]
 [20 40 60]
 [30 60 90]]
```


**基本原则**

1. If the arrays do not have the same rank, prepends the shape of the lower rank array with 1s until both shapes have the same length.
2. The two arrays are said to be *compatible* in a dimension if they have the same size in the dimension, or if one of the arrays has size 1 in that dimension.
3. The arrays can be broadcast together if they are compatible in all dimensions.
4. After broadcasting, each array behaves as if it had shape equal to the elementwise maximum of shapes of the two input arrays.
5. In any dimension where one array had size 1 and the other array had size greater than 1, the first array behaves as if it were copied along that dimension



### 概率与随机数

#### python 本身就有 random module

似乎 random module 里面有的函数，numpy 中都有对应的，而且效率更高，有更多可设置的参数

```python
# python 有产生随机数的 module，但是效率不高
import random  
random_number = random.random()   # 产生 [0,1) 之间的随机数。 注意这样产生的随机数并不是真正的随机，所以不适用于安全加密场合
print(random_number)  # 0.612380929208925

crypto = random.SystemRandom()
real_random_number = crypto.random()   # 这样才能产生真正的随机数
print(real_random_number)  # 0.5341978704487873
```

#### 用 numpy 产生一系列随机数，ndarray 格式

```python
# 我们还是要用 numpy 更高效的产生随机数，也更方便计算
random_num = np.random.random(10)  # 这里的随机数也是伪随机的
print(type(random_num))
print(random_num)
```

```
<class 'numpy.ndarray'>
[0.49534908 0.32273438 0.50794688 0.73915369 0.13342109 0.92728746
 0.25110986 0.20037795 0.51605003 0.80027842]
```

#### 产生和为 1 的随机概率分布

```python
# 产生和为 1 的概率分布
list_of_random = np.random.random(10)
sum_of_values = list_of_random.sum()
print(sum_of_values)  # 4.87290758749084
normalized_values = list_of_random / sum_of_values
print(normalized_values.sum())  # 0.9999999999999998
```

#### 产生一定范围内的随机整数

```python
print(np.random.randint(1,7)) # 有头， 无尾, 标量，即 0 维
print(np.random.randint(1,7, size=1)) # 1 维向量，一个元素
print(np.random.randint(1,7, size=10)) # 1 维向量，10 个元素
print(np.random.randint(1,7, size=(5,4))) # 2 维向量
```

```
3
[4]
[6 4 2 3 4 4 2 3 6 4]
[[4 5 5 2]
 [1 3 3 6]
 [2 3 1 6]
 [3 6 6 5]
 [1 1 4 5]]
```

#### 从特定 list 中选择 choice()

```python
# import numpy as np
# 或者 
# from numpy.random import choice  # 这样就可以直接用 choice()，不加前缀
possible_canteens = ["can1", "can9", "can11", "can13", "can14", "can16", "can22"]
weights = np.array([2, 3, 1, 3, 2, 2, 3])
normalized_weights = weights / weights.sum()
np.random.choice(possible_canteens, p=normalized_weights) # 如果不设置 p 就完全随机: 'can22'
```

```python
# 还可以多选
print(np.random.choice(possible_canteens, size=3))
print(np.random.choice(possible_canteens, size=(2,3), replace=False)) # replace = False 表示不放回，因此会不重复
```

```
['can13' 'can16' 'can1']
[['can9' 'can14' 'can13']
 ['can11' 'can16' 'can1']]
```

### 用 numpy 实现简单的神经网络

可以通过这里的程序，熟悉简单网络的构造原理。但在实际中，还是用 Pytorch 直接调用各种函数命令更方便

numpy 不能用 GPU 加速，这是它的问题。


```python
import numpy as np
N, D_in, H, D_out = 64, 1000, 100, 10
x = np.random.randn(N, D_in)
y = np.random.randn(N, D_out)
w1 = np.random.randn(D_in, H)
w2 = np.random.randn(H, D_out)
learning_rate = 1e-6
for t in range(500):
    # Forward
    h = x.dot(w1)  # h.shape = (64, 100)
    h_relu = np.maximum(h, 0)  # 与 h 同 shape，每个元素与 0 比较，取较大值
    y_pred = h_relu.dot(w2) # y_pred.shape = (64, 10)
    
    # loss
    loss = np.square(y_pred - y).sum()
    print(t, loss)
    
    # backprop
    grad_y_pred = 2.0 * (y_pred - y)
    grad_w2 = h_relu.T.dot(grad_y_pred)
    grad_h_relu = grad_y_pred.dot(w2.T)
    grad_h = grad_h_relu.copy()
    grad_h[h < 0] = 0
    grad_w1 = x.T.dot(grad_h)
    
    # update weights
    w1 -= learning_rate * grad_w1
    w2 -= learning_rate * grad_w2
```

    0 35782238.6197
    1 32849272.0008
    2 29887950.2406
    3 23842402.4916
    4 16108957.9089
    5 9509226.27194
    ...
    94 1052.99163766
    95 992.34088235
    96 935.280477341
    97 881.625902661
    98 831.172870716
    99 783.689004262
    ...
    496 1.58726991097e-06
    497 1.5155235739e-06
    498 1.44703771538e-06
    499 1.38164911846e-06



## SciPy

SciPy 提供了很多专业领域使用的函数，下面以图像处理为例

```python
import scipy.misc import imread, imsave, imresize
img = imread('assets/cat.jpg') # 将图片读入为 numpy array
print(img.dtype, img.shape) # uint8, (400,248, 3) 大小为 400 * 248, 3个颜色各占一层
img_tinted = img * [1, 0.95, 0.9] # 颜色减弱
img_tinted = imresize(img_tinted, (300, 300)) # 300 * 300 pixels
imsave('assets/cat_tinted.jpg', img_tinted)
```



## matplotlib

在 matplotlib 中最重要的是 pyplot，可以用来画 2D 数据。

### 基本画图命令


```python
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline  

# Compute the x and y coordinates for points on a sine curve
x = np.arange(0, 3 * np.pi, 0.1)
y = np.sin(x)

# Plot the points using matplotlib
plt.plot(x, y)
plt.show()  # You must call plt.show() to make graphics appear.
```


### 画多条曲线，并添加修饰


```python
# Compute the x and y coordinates for points on sine and cosine curves
x = np.arange(0, 3 * np.pi, 0.1)
y_sin = np.sin(x)
y_cos = np.cos(x)

# Plot the points using matplotlib
plt.plot(x, y_sin, 'bo')  # blue circle
plt.plot(x, y_cos, 'r+')  # red plus



plt.xlabel('x axis label')
plt.ylabel('y axis label')
plt.title('Sine and Cosine')
plt.legend(['Sine', 'Cosine'], loc="upper center")
# 关于 legend 的设置可以参考 https://pythonspot.com/matplotlib-legend/

plt.show()

#也可以以矩阵的形式将待画图的向量。矩阵的**每一列**依次作为 y 。
```

修饰线条的参考文献 https://matplotlib.org/api/_as_gen/matplotlib.pyplot.plot.html

基本格式为 `'[color][marker][line]'`. 

注意： 这三个属性修改必须在其他修改 (例如 linewidth) 之前.

**Colors**

| character | color   |
| --------- | ------- |
| `'b'`     | blue    |
| `'g'`     | green   |
| `'r'`     | red     |
| `'c'`     | cyan    |
| `'m'`     | magenta |
| `'y'`     | yellow  |
| `'k'`     | black   |
| `'w'`     | white   |

**Markers**

| character | description           |
| --------- | --------------------- |
| `'.'`     | point marker          |
| `','`     | pixel marker          |
| `'o'`     | circle marker         |
| `'v'`     | triangle_down marker  |
| `'^'`     | triangle_up marker    |
| `'<'`     | triangle_left marker  |
| `'>'`     | triangle_right marker |
| `'1'`     | tri_down marker       |
| `'2'`     | tri_up marker         |
| `'3'`     | tri_left marker       |
| `'4'`     | tri_right marker      |
| `'s'`     | square marker         |
| `'p'`     | pentagon marker       |
| `'*'`     | star marker           |
| `'h'`     | hexagon1 marker       |
| `'H'`     | hexagon2 marker       |
| `'+'`     | plus marker           |
| `'x'`     | x marker              |
| `'D'`     | diamond marker        |
| `'d'`     | thin_diamond marker   |
| `'|'`     | vline marker          |
| `'_'`     | hline marker          |

**Line Styles**

| character | description         |
| --------- | ------------------- |
| `'-'`     | solid line style    |
| `'--'`    | dashed line style   |
| `'-.'`    | dash-dot line style |
| `':'`     | dotted line style   |

**其他属性**

Here is a list of available [`Line2D`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D) properties:

| Property                                                     | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [`agg_filter`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_agg_filter.html#matplotlib.artist.Artist.set_agg_filter) | a filter function, which takes a (m, n, 3) float array and a dpi value, and returns a (m, n, 3) array |
| [`alpha`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_alpha.html#matplotlib.artist.Artist.set_alpha) | float                                                        |
| [`animated`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_animated.html#matplotlib.artist.Artist.set_animated) | bool                                                         |
| [`antialiased`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_antialiased) | bool                                                         |
| [`clip_box`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_clip_box.html#matplotlib.artist.Artist.set_clip_box) | [`Bbox`](https://matplotlib.org/api/transformations.html#matplotlib.transforms.Bbox) |
| [`clip_on`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_clip_on.html#matplotlib.artist.Artist.set_clip_on) | bool                                                         |
| [`clip_path`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_clip_path.html#matplotlib.artist.Artist.set_clip_path) | [([`Path`](https://matplotlib.org/api/path_api.html#matplotlib.path.Path), [`Transform`](https://matplotlib.org/api/transformations.html#matplotlib.transforms.Transform)) | [`Patch`](https://matplotlib.org/api/_as_gen/matplotlib.patches.Patch.html#matplotlib.patches.Patch) \| None] |
| [`color`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_color) | color                                                        |
| [`contains`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_contains.html#matplotlib.artist.Artist.set_contains) | callable                                                     |
| [`dash_capstyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_dash_capstyle) | {'butt', 'round', 'projecting'}                              |
| [`dash_joinstyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_dash_joinstyle) | {'miter', 'round', 'bevel'}                                  |
| [`dashes`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_dashes) | sequence of floats (on/off ink in points) or (None, None)    |
| [`drawstyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_drawstyle) | {'default', 'steps', 'steps-pre', 'steps-mid', 'steps-post'} |
| [`figure`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_figure.html#matplotlib.artist.Artist.set_figure) | [`Figure`](https://matplotlib.org/api/_as_gen/matplotlib.figure.Figure.html#matplotlib.figure.Figure) |
| [`fillstyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_fillstyle) | {'full', 'left', 'right', 'bottom', 'top', 'none'}           |
| [`gid`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_gid.html#matplotlib.artist.Artist.set_gid) | str                                                          |
| [`in_layout`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_in_layout.html#matplotlib.artist.Artist.set_in_layout) | bool                                                         |
| [`label`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_label.html#matplotlib.artist.Artist.set_label) | object                                                       |
| [`linestyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_linestyle) | {'-', '--', '-.', ':', '', (offset, on-off-seq), ...}        |
| [`linewidth`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_linewidth) | float                                                        |
| [`marker`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_marker) | unknown                                                      |
| [`markeredgecolor`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_markeredgecolor) | color                                                        |
| [`markeredgewidth`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_markeredgewidth) | float                                                        |
| [`markerfacecolor`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_markerfacecolor) | color                                                        |
| [`markerfacecoloralt`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_markerfacecoloralt) | color                                                        |
| [`markersize`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_markersize) | float                                                        |
| [`markevery`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_markevery) | unknown                                                      |
| [`path_effects`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_path_effects.html#matplotlib.artist.Artist.set_path_effects) | [`AbstractPathEffect`](https://matplotlib.org/api/patheffects_api.html#matplotlib.patheffects.AbstractPathEffect) |
| [`picker`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_picker) | float or callable[[Artist, Event], Tuple[bool, dict]]        |
| [`pickradius`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_pickradius) | float                                                        |
| [`rasterized`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_rasterized.html#matplotlib.artist.Artist.set_rasterized) | bool or None                                                 |
| [`sketch_params`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_sketch_params.html#matplotlib.artist.Artist.set_sketch_params) | (scale: float, length: float, randomness: float)             |
| [`snap`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_snap.html#matplotlib.artist.Artist.set_snap) | bool or None                                                 |
| [`solid_capstyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_solid_capstyle) | {'butt', 'round', 'projecting'}                              |
| [`solid_joinstyle`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_solid_joinstyle) | {'miter', 'round', 'bevel'}                                  |
| [`transform`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_transform) | matplotlib.transforms.Transform                              |
| [`url`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_url.html#matplotlib.artist.Artist.set_url) | str                                                          |
| [`visible`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_visible.html#matplotlib.artist.Artist.set_visible) | bool                                                         |
| [`xdata`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_xdata) | 1D array                                                     |
| [`ydata`](https://matplotlib.org/api/_as_gen/matplotlib.lines.Line2D.html#matplotlib.lines.Line2D.set_ydata) | 1D array                                                     |
| [`zorder`](https://matplotlib.org/api/_as_gen/matplotlib.artist.Artist.set_zorder.html#matplotlib.artist.Artist.set_zorder) | float                                                        |

### subplot


```python
# Compute the x and y coordinates for points on sine and cosine curves
x = np.arange(0, 3 * np.pi, 0.1)
y_sin = np.sin(x)
y_cos = np.cos(x)

# Set up a subplot grid that has height 2 and width 1,
# and set the first such subplot as active.
plt.subplot(2, 1, 1)

# Make the first plot
plt.plot(x, y_sin)
plt.title('Sine')

# Set the second subplot as active, and make the second plot.
plt.subplot(2, 1, 2)
plt.plot(x, y_cos)
plt.title('Cosine')

# Show the figure.
plt.show()
```


### 显示 image


```python
from scipy.misc import imread, imresize

img = imread('pic/a3c1.png')
img_tinted = img * [1, 0.95, 0.9]

# Show the original image
plt.subplot(1, 2, 1)
plt.imshow(img)  # 不再 plot 画图了，而是显示 image

# Show the tinted image
plt.subplot(1, 2, 2)

# A slight gotcha with imshow is that it might give strange results
# if presented with data that is not uint8. To work around this, we
# explicitly cast the image to uint8 before displaying it.
plt.imshow(np.uint8(img_tinted))
plt.show()
```



## Pandas

### 读取 CSV

```python
import pandas as pd
df = pd.read_csv("csv_file")
# will read the data into df, 类型为 dataframe. 可以理解成是 array，但是不同列的元素 type 可以不同。
df.shape  # 可以显示该 data array 的 shape
```

### 分析数据

#### 取值计数

```python
df.<column>.value_counts()
# 返回该 column 中取值数目的排序，比如值为 0 的有 100 个， 值为 4 的有 29 个
df.<column>.hist(bins=5)
# 将取值数目用 histogram 方式画出来，表明各取值区间有多少。bins 指定划分区间的数量。默认大概是10
```

#### 统计数据

```python
df.describe()
# 返回对所有数值的统计分析，包括该列包含多少值，即总行数减去 null 的行, 均值，标准差，最小，最大等
df.<column>.mean()
# 返回指定 column 中取值的均值
```

#### 提取部分行数据

```python
df[df.<column> == <specific_data>]
# 提取 df 中指定 column 对应内容为 specific_data 的所有行数据
df[df.<column1> == <specific_data>].<column2>.value_counts()
# 提取之后的子表使用方式跟普通的表相同。
df[(df.<column1> < 15) & (df.<column2> >4)]
# 可以设置多个条件
```

#### 处理 NaN

```python
df[df.<column>.isnull()]
# 提取 df 中该 column 中无值（显示为 NaN）的所有行
df.<column>.fillna(value=<specific_value>)
# 把该定 column 中的 NaN 值替换掉
```

### 文本分析

#### 句子分割成单词


```python
docA = "the cat sat on my face"
docB = "the dog sat on my bed"
bowA = docA.split(" ") # 以空格来分割，得到 bag of words
bowB = docB.split(" ")
# bowA = ['the', 'cat', 'sat', 'on', 'my', 'face']
# bowB = ['the', 'dog', 'sat', 'on', 'my', 'bed']
wordSet = set(bowA).union(set(bowB))
# wordset = {'the', 'cat', 'sat', 'on', 'my', 'face', 'dog', 'bed'}
# all words in all bags

line = 'abcde\r\n'
print(line.strip())  # .strip() 提剔除 str 两端的回车、换行字符: abcde

line = 'ab\tcde\tfg\t'
print(line.split('\t'))  # 返回 list, 元素由 '\t' 分割开: ['ab', 'cde', 'fg', '']

```



# NetworkX

## NetworkX Module 中的命名规则

- class 用 CamelCase
- function, method, variable 用 lower_case_underscore

## 调用 module

```python
# import module
import networkx as nx
```

```python
# networkx module 提供了 4 种基本的网络 classes
G1 = nx.Graph() # 无向，无重边，可有自环
G2 = nx.DiGraph() # 有向，无重边，可有自环   
G3 = nx.MultiGraph() # 无向，可有重边，可有自环
G4 = nx.MultiDiGraph() # 有向，可有重边，可有自环
```

在这些 graph中，数据存放形式为 dict of dicts：

```python
{node1:{neighbor1: edge_attribute, neighbor2: edge_attribute, ...},
 node2:{neighbor1: edge_attribute, neighbor2: edge_attribute, ...},
 ...
}
```

这种 dict of dicts 的结构可以快速添加、删除和查找节点、连边属性。

## 创建 network

有三种方法创建网络：

1. 创建经典网络模型，如星型网络，小世界网络，随机网络
2. 从文件导入
3. 向空白网络添加点和边

### 创建经典网络

```python
petersen = nx.petersen_graph()
tutte = nx.tutte_graph()
maze = nx.sedgewick_maze_graph()
tet = nx.tetrahedral_graph()

K_5 = nx.complete_graph(5)
K_3_5 = nx.complete_bipartite_graph(3, 5)
barbell = nx.barbell_graph(10, 10)
lollipop = nx.lollipop_graph(10, 20)

er = nx.erdos_renyi_graph(100, 0.15)
ws = nx.watts_strogatz_graph(30, 3, 0.1)
ba = nx.barabasi_albert_graph(100, 5)  # exponent 为 -3, 与 n,m 无关
red = nx.random_lobster(100, 0.9, 0.9)
```

### 从文件读取

```python
#  edge lists, adjacency lists, GML, GraphML, pickle, LEDA and others.
mygraph = nx.read_gml("path.to.file")
```

```python
# 写入文件
nx.write_gml(red, "path.to.file") 
```

### 从空白网络添加

- 添加点

  ```python
  import networkx as nx
  G = nx.Graph()
  G.add_node(1)  # 添加一个点
  G.add_nodes_from([12,13]) # 添加多个点
  G.add_node('spam') # 添加一个名为 spam 的点
  G.add_nodes_from('spam') # 添加 4 个点，分别为 's', 'p', 'a', 'm'
  H = nx.path_graph(10) # 还可以从其他 network 中提取点来添加, 仅提取出点，不要连边
  G.add_nodes_from(H)
  ```

- 添加边及其属性

  ```python
  G.add_edge(1,2)  # 添加一条边
  G.add_edge(2,3,weight=0.9)
  # 如果想给边加权重，则用 'weight'关键词，这是可以以后做运算的。例如 Dijkstra’s algorithm 可以直接
  # 使用这里的 weight 进行搜索
  # 其他类型的边属性可以用任意的关键词
  G.add_edge(1,2,color='red',weight=0.84,size=300)  # 给边添加属性
  G.add_edges_from([(1,2),(3,4),(2,5)])  # 添加多条边
  elist=[('a','b',5.0),('b','c',3.0),('a','c',1.0),('c','d',7.3)] # 添加多条带权重的边
  G.add_weighted_edges_from(elist)
  ```

- 节点名字可以是任意 Hashable，远不止于 数字，字符等

  ```python
  # 节点的名字不一定是数字编号，可以是字符，甚至是函数
  import math
  G.add_edge('y','x',function=math.cos)  # 给 y, x 的连边添加 attribute, 值为一个函数
  G.add_node(math.cos)  # 节点也可以是函数，文件，甚至可以是另一个网络，这就比较方便构造网络的网络
  ```

- 对于无向、无重边图来说，添加往返两条边实际上是一条边

  ```python
  G = nx.Graph()
  G.add_edge(1,2)
  G.add_edge(2,1)
  print(G.number_of_nodes())  # 2
  print(G.number_of_edges())  # 1
  ```

- 实际上，network, node, edge 都可以被附上 attribute

  ```python
  G = nx.Graph(day='friday')   # 赋值的时候，尽管 key 是字符，但是不加引号
  G.graph  # {'day': 'friday'}
  G.add_node(1, time='5pm')
  G.add_nodes_from([3], time='2pm')
  G.node[1]['room'] = 714  # node 1 有两个属性
  print(G.nodes(data=True))  # 连数据一起显示: [(1, {'room': 714, 'time': '5pm'}), (3, {'time': '2pm'})]
  ```

### remove node

remove node 的语法跟前边是类似的

```python
G.remove_node(1)
G.clear() # remove 所有的点和边
```

## 获取网络属性

```python
print(G.adj) 
# 这里可以看出来，网络结构是由 dict of dicts of dicts 表示的
# {'a': {}, 1: {2: {}}, 2: {1: {}, 5: {}}, 3: {4: {}}, 4: {3: {}}, 5: {2: {}}, 'spam': {}, 'm': {}, 12: {}, 13: {}, 'p': {}, 's': {}}
G.number_of_nodes()  # 返回节点数: 9
G.number_of_edges()  # 返回边数: 3
print(G.nodes())  # 返回节点 List: ['a', 1, 2, 'spam', 'm', 12, 13, 'p', 's']
G.adj # AdjacencyView({'a': {}, 1: {2: {}}, 2: {1: {}}, 'spam': {}, 'm': {}, 12: {}, 13: {}, 'p': {}, 's': {}})
```

上述输出表明 Node 的标记可以很随意，只要能区分即可。也可以将其转化为比较传统的数字编号方式，连边关系不变

```python
NEWG = nx.convert_node_labels_to_integers(G)
print(NEWG.nodes()) # [0, 1, 2, 3, 4, 5, 6, 7, 8]
NEWG.adj # AdjacencyView({0: {}, 1: {2: {}}, 2: {1: {}}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}})
print(G.edges())  # 返回连边 List: [(1, 2)]
print(G[1])  # 返回节点 1 对应的 dict: {2: {}}
print(G.neighbors(1)) 
# G.neighbors(n) 返回的是 iterator 的形式，时间、空间占用比较小，
# <dictionary-keyiterator object at 0x7fd10a9f10a8>

# 在 for 循环中 iterator 可以跟 list 一样使用
for i in G.neighbors(1): 
    print(i)  # 2
    print(type(i))  # <type 'int'>
```

## 有向网络

- 可以从无向网络构造有向网络

  ```python
  G = nx.Graph()
  G.add_edge(1,2)
  nx.draw(G)
  print(G.edges()) # [(1, 2)]
  DG = nx.DiGraph(G)
  nx.draw(DG)
  print(DG.edges())  # 将原本的无向边转化成两条有向边: [(1, 2), (2, 1)]
  ```

- 也可以直接创建有向网络

  ```python
  DG = nx.DiGraph([(1,2), (3,4) ,(4,1)])  # 直接给定连边方向
  print(DG.edges()) # [(1, 2), (3, 4), (4, 1)]
  nx.draw(DG)
  ```

- 有向网络的属性与无向网络有些不同

  ```python
  # 入度 和 出度
  DG=nx.DiGraph()
  DG.add_weighted_edges_from([(1,2,0.5), (3,1,0.75)])
  print(DG.out_degree(1,weight='weight'))  # 0.5
  print(DG.in_degree(1,weight='weight'))   # 0.75
  print(DG.degree(1,weight='weight')) # degree = out_degree + in_degree: 1.25
  for i in DG.successors(1):
      print i  # 2
  
  for i in DG.neighbors(1):  # 如果只说 neighbors，则默认是 out neighbors
      print i  # 2
  ```

- 有向 转化成 无向

  ```python
  print(DG.edges())  # [(1, 2), (3, 1)]
  G = nx.Graph(DG)   # 转化成无向网络
  DG.successors(1)
  print(G.edges())   # [(1, 2), (1, 3)]
  nx.draw(DG)
  nx.draw(G)
  ```

## 网络可视化

- 不同的布局

  ```python
  G=nx.cubical_graph()
  nx.draw(G) 
  nx.draw(G,pos=nx.spectral_layout(G), nodecolor='r',edge_color='b')
  nx.draw_random(G)
  nx.draw_circular(G)
  ```

- 保存图片

  ```python
  plt.savefig(p,"path.png")
  ```

# xml 分析

可以使用 xml.etree.ElementTree 这个 module 进行 xml 文件读取和其他操作。

## 示例文件

假设有 xml 文件 *country_data.xml* 内容如下：

```xml
<?xml version="1.0"?>
<data>
    <country name="Liechtenstein">
        <rank>1</rank>
        <year>2008</year>
        <gdppc>141100</gdppc>
        <neighbor name="Austria" direction="E"/>
        <neighbor name="Switzerland" direction="W"/>
    </country>
    <country name="Singapore">
        <rank>4</rank>
        <year>2011</year>
        <gdppc>59900</gdppc>
        <neighbor name="Malaysia" direction="N"/>
    </country>
    <country name="Panama">
        <rank>68</rank>
        <year>2011</year>
        <gdppc>13600</gdppc>
        <neighbor name="Costa Rica" direction="W"/>
        <neighbor name="Colombia" direction="E"/>
    </country>
</data>
```

## 读取与查找操作

### 读取 xml 文件

```python
import xml.etree.ElementTree as ET
tree = ET.parse('country_data.xml')
root = tree.getroot()  # root 是最高外层 node
```

### 查看 tag 和 attribute

```python
root.tag  # 'data'
root.attrib  # {} , attribute 是以 dict 的形式保存的，因为里面可能有多个 attributes
```

### 查看 children nodes 的 tag 和 attribute

```python
for child in root:  # root 的下一级 node
    print(child.tag, child.attrib)
```

返回结果

```python
country {'name': 'Liechtenstein'}
country {'name': 'Singapore'}
country {'name': 'Panama'}
```

### 查看 nodes 中的具体内容用 text

```python
root[0][1].text  # '2008'， 注意：返回的是 string 类型的
```

其中是以嵌套的形式一层层选取 node， `root[0]` 为第一个 country node，`root[0][1]` 为第一个 country node 里面第二个 node，即 `year` node。用 text 读取其文本内容。

### 查找整个 xml 中的某个 node

例如要找整个文件中所有的 neighbor node，不论在哪一层

```python
for neighbor in root.iter('neighbor'):
    print(neighbor.attrib)
```

返回所有 neighbor node 中的 attribute 

```python
{'direction': 'E', 'name': 'Austria'}
{'direction': 'W', 'name': 'Switzerland'}
{'direction': 'N', 'name': 'Malaysia'}
{'direction': 'W', 'name': 'Costa Rica'}
{'direction': 'E', 'name': 'Colombia'}
```

### 查找下一层某个感兴趣的 node

仅在下一层查找，没有遍历更下层

```python
for country in root.findall('country'):
    rank = country.find('rank').text
    name = country.get('name')
    print(name, rank)
   
# 返回
Liechtenstein 1
Singapore 4
Panama 68
```

其中 

- findall( ) 是返回所有满足条件的子 node 
- find( ) 是返回第一个满足条件的子 node 
- get( ) 是对 attribute 进行操作，返回某个 attribute 的值

## 写入与修改操作 

### 修改与添加

假设要让所有的 rank 加 1，并且给 rank 添加一个 attribute: updated='yes'

```python
for rank in root.iter('rank'):  # 先找到所有的 rank node
    new_rank = int(rank.text) + 1  # 原本内容是 string 类型，要先转化成 int 再操作
    rank.text = str(new_rank)  # 转换回 string，并重新赋值
    rank.set('updated', 'yes')  # 添加 attribute

tree.write('output.xml')  # 做完以上修改别忘了写入
```

修改之后的 xml 文件

```xml
<?xml version="1.0"?>
<data>
    <country name="Liechtenstein">
        <rank updated="yes">2</rank>
        <year>2008</year>
        <gdppc>141100</gdppc>
        <neighbor name="Austria" direction="E"/>
        <neighbor name="Switzerland" direction="W"/>
    </country>
    <country name="Singapore">
        <rank updated="yes">5</rank>
        <year>2011</year>
        <gdppc>59900</gdppc>
        <neighbor name="Malaysia" direction="N"/>
    </country>
    <country name="Panama">
        <rank updated="yes">69</rank>
        <year>2011</year>
        <gdppc>13600</gdppc>
        <neighbor name="Costa Rica" direction="W"/>
        <neighbor name="Colombia" direction="E"/>
    </country>
</data>
```

### 删除

假设要删除所有 rank 超过 50 的 country，用 remove( ) 即可

```python
for country in root.findall('country'):
    rank = int(country.find('rank').text)
    if rank > 50:
        root.remove(country)

tree.write('output.xml')
```

修改后的文件

```xml
<?xml version="1.0"?>
<data>
    <country name="Liechtenstein">
        <rank updated="yes">2</rank>
        <year>2008</year>
        <gdppc>141100</gdppc>
        <neighbor name="Austria" direction="E"/>
        <neighbor name="Switzerland" direction="W"/>
    </country>
    <country name="Singapore">
        <rank updated="yes">5</rank>
        <year>2011</year>
        <gdppc>59900</gdppc>
        <neighbor name="Malaysia" direction="N"/>
    </country>
</data>
```



# Statistics module

官网  <https://docs.python.org/3/library/statistics.html>

主要函数

## Averages and measures of central location

These functions calculate an average or typical value from a population or sample.

| function         | description                                  |
| ---------------- | -------------------------------------------- |
| mean()           | Arithmetic mean (“average”) of data.         |
| harmonic_mean()  | Harmonic mean of data.                       |
| median()         | Median (middle value) of data.               |
| median_low()     | Low median of data.                          |
| median_high()    | High median of data.                         |
| median_grouped() | Median, or 50th percentile, of grouped data. |
| mode()           | Mode (most common value) of discrete data.   |

## Measures of spread

These functions calculate a measure of how much the population or sample tends to deviate from the typical or average values.

| function    | description                            |
| ----------- | -------------------------------------- |
| pstdev()    | Population standard deviation of data. |
| pvariance() | Population variance of data.           |
| stdev()     | Sample standard deviation of data.     |
| variance()  | Sample variance of data.               |



# argparse

这是管理输入参数的利器，很多比较正规的代码都用到了这个工具。

## 例 1

假设我们有个 `videos.py` 文件，作用是将输入的 video 转换成 images 。其中需要用户设定输入 video 的路径和输出 image 的路径。可以用如下 argparse 工具实现：

```python
# videos.py
import argparse
parser = argparse.ArgumentParser(description='Videos to images')
parser.add_argument('indir', type=str, help='Input dir for videos')
parser.add_argument('outdir', type=str, help='Output dir for image')
args = parser.parse_args()
print(args.indir)
```

- 调用 argparse library
- 创建一个 parser 对象，可以初始化对它的描述
- 向 parser 中添加参数，基本内容包括：参数名称，类型，描述
- 由设置好的 parser 对象生成 args 变量，其中已经包含了外界输入的参数

运行命令：

```bash
$ python videos.py --help
usage: videos.py [-h] indir outdir

Videos to images

positional arguments:
  indir       Input dir for videos
  outdir      Output dir for image

optional arguments:
  -h, --help  show this help message and exit
```

可以看到这里显示了刚才我们设置的所有信息，包括这个程序的描述，两个 positional 参数及描述

对于 **positional 参数**，在赋值时可以指明参数名称，这样的话赋值顺序无所谓。

如果不指明参数名称，则必须按照定义参数的先后顺序赋值。

而且，positional 参数必须有赋值！否则报错 `too few arguments` 之类的。

## 例 2

在实际中程序中，用的比较多的参数是 optional 参数。

例如如下程序：

```python
# my_example.py
import argparse
parser = argparse.ArgumentParser(description='My example explanation')
parser.add_argument(
    '--my_optional',
    default=2,
    help='provide an integer (default: 2)'
)
my_namespace = parser.parse_args()
print(my_namespace.my_optional)
```

定义了 **optional 参数** `my_optional`，注意这类参数前边可以用 `--` 加参数名称，或者用 `-` 加一个或多个字母作为缩写名称。例如

```python
parser.add_argument('-tb', '--tensorboard-log', help='Tensorboard log dir', default='', type=str)
```

optional 参数之所以是可选的，是因为可以设置 default 值，如果没有外加赋值，就用 default.

运行结果如下：

```bash
$ python my_example.py --help
usage: my_example.py [-h] [--my_optional MY_OPTIONAL]

My example explanation

optional arguments:
  -h, --help            show this help message and exit
  --my_optional MY_OPTIONAL
                        provide an integer (default: 2)
                        
$ python my_example.py 
2

$ python my_example.py --my_optional 6
6
```



# thread 和 event

通过定义标记性的 event 可以使用多个进程运行程序，通过 event 的标记进行互相通讯。

## 例 1

```python
import threading
import time

class TestThread(threading.Thread):
    def __init__(self, name, event): # 每个 thread 对象应该有名字和 event
        super(TestThread, self).__init__()
        self.name = name
        self.event = event
    
    def run(self):   # 这个函数名字必须是 run，否则后边的 thread.start() 不起作用
        print('Thread: ', self.name, ' start at:', time.ctime(time.time()))
        self.event.wait()   # 所有的 thread 到了这一步都必须等待 event 标记变为 True
        print('Thread: ', self.name, ' finish at:', time.ctime(time.time()))
        
def main():
    event = threading.Event()  # 首先定义一个 event，作为标记，方便 thread 开启和关闭
    threads = []
    for i in range(1, 10):
        threads.append(TestThread(str(i), event))
    print('main thread start at: ', time.ctime(time.time()))
    event.clear()  # 开始之前清除 event 的标记，即设置为 false.
    for thread in threads:
        thread.start()  # 以此开启所有的 thread
    print ('sleep 5 seconds.......')
    time.sleep(5)
    print ('now awake other threads....')
    event.set()  # 将 event 标记设置为 True

main()
```

## 例 2

```python
import threading
import random
import time


class VehicleThread(threading.Thread):
    def __init__(self, threadName, event):  # 参数依然是名字和 event
        threading.Thread.__init__(self, name=threadName)
        self.threadEvent = event

    def run(self):
        """Vehicle waits unless/until light is green"""
        # stagger arrival times
        time.sleep(random.randrange(1, 10))  # 模拟随机到来的车
        # prints arrival time of car at intersection
        print ("%s arrived at %s" % \
              (self.getName(), time.ctime(time.time())))
        # wait for green light
        self.threadEvent.wait()    # 如果当前是绿灯，直接过去，否则就要等 event.set()
        # displays time that car departs intersection
        print ("%s passes through intersection at %s" % \
              (self.getName(), time.ctime(time.time())))


greenLight = threading.Event()   # 定了 event，名字为 greenLight
vehicleThreads = []

# creates and starts ten Vehicle threads
for i in range(1, 11):
    vehicleThreads.append(VehicleThread("Vehicle" + str(i),
                                        greenLight))

for vehicle in vehicleThreads:
    vehicle.start()

while threading.activeCount() > 1:  # 只要还有车来，就再运行一遍
    # sets the Event's flag to false -- block all incoming vehicles
    greenLight.clear()   # 开启红灯
    print ("RED LIGHT! at", time.ctime(time.time()))
    time.sleep(3)    # 红灯亮 3 秒

    # sets the Event's flag to true -- awaken all waiting vehicles
    print ("GREEN LIGHT! at", time.ctime(time.time()))
    greenLight.set()  # 转换成绿灯
    time.sleep(1)  # 绿灯亮 1 秒
```

