## 区分：terminal, shell, bash

- terminal：这是一种窗口程序，启动之后会打开一个窗口，让用户向 shell 中输入命令。现在 terminal 程序多种多样，ubuntu 中默认的是 gnome-terminal, 另外还有 xterm, eterm, konsole 等等。这些程序可能提供了外形不同的窗口界面，但功能都是一样的，就是提供了用户与 shell 交互的界面。
- shell：接受用户的输入命令，送给操作系统执行。在早期，这是唯一的用户交互渠道，现在又多了 GUI： Graphical user interfaces。shell 也是一种程序，而不是一个具体的程序，早期有 Bourne shell (sh), C shell (csh), 和 Korn shell (ksh). 现在 Ubuntu 默认是 bash: Bourne Again shell，算是 Bourne shell 的扩展，完全后向兼容，并增加和增强了很多特性。

总之，shell 不是具体的程序，而只是一个抽象的概念，或者是一类程序的统称，在 /bin 文件夹中可以看到 sh 实际上只是一个 Link，默认指向了 bash 程序. 

shell 可以执行用户输入的命令（实际上是解释给操作系统去执行），如果是 shell 自带的命令，就直接执行，如果不是内置命令，就在 $PATH 指定的路径中去查找对应的程序。

## 环境变量相关 .bashrc 和 profile

PATH 是环境变量之一，用的比较多，系统中还有其他很多环境变量。

列出系统中所有的环境变量

```bash
env
```

部分结果如下：

```bash
XDG_VTNR=7
ORBIT_SOCKETDIR=/tmp/orbit-acronis
XDG_SESSION_ID=c1
CLUTTER_IM_MODULE=xim
IBUS_DISABLE_SNOOPER=1
XDG_GREETER_DATA_DIR=/var/lib/lightdm-data/acronis
TERMINATOR_UUID=urn:uuid:d410f11b-5514-44d0-987c-8c4246e02334
GPG_AGENT_INFO=/home/acronis/.gnupg/S.gpg-agent:0:1
TERM=xterm
SHELL=/bin/bash
ROS_ROOT=/opt/ros/kinetic/share/ros
ROS_PACKAGE_PATH=/home/acronis/acronis/acronis_ws/src:/home/acronis/autoware-ntu-fork/ros/src:/opt/ros/kinetic/share
QT_LINUX_ACCESSIBILITY_ALWAYS_ON=1
ROS_MASTER_URI=http://localhost:11311
WINDOWID=96468996
UPSTART_SESSION=unix:abstract=/com/ubuntu/upstart-session/1000/1254
GNOME_KEYRING_CONTROL=
ROS_VERSION=1
GTK_MODULES=gail:atk-bridge:unity-gtk-module
USER=acronis
. . .
```

可以返回某个环境变量的值，例如

```bash
echo $SHELL  # 返回  /bin/bash
```

也可以设置某个环境变量的值，例如

```bash
PATH=$HOME/scripts:$PATH
```



### PATH 变量的本质

PATH 变量决定了 shell 将在哪些目录中寻找命令或程序，PATH 的值是一系列目录，格式如下

`PATH=$PATH:PATH1:PATH2:PATH3:...:PATH N`

### 几个配置文件

#### 系统级

- /etc/profile： 用户登陆系统时被执行

- /etc/bashrc：用户运行 bash shell 时被执行

#### 用户级

- ~/.bash_profile：该文件仅在用户登陆时执行一次，调用用户的 .bashrc 文件
- ~/.bashrc：每次打开新的 shell 就被执行。

### 三种修改方式

对于环境变量 path, pythonpath 等的修改从影响范围看有三种方式：

1. 影响当前 terminal

   直接在当前 terminal 中修改。例如修改 path，可以用命令

   `export PATH=$PATH:<新的path1>:<新的path2>`

   这种修改只在当前 terminal 中有效，关闭 terminal 之后再打开就不行了。

2. 影响当前用户

   修改 /home/username/.bashrc 文件，例如修改 path

   `export PATH=<新的 path1>:$PATH`

   再比如修改 PYTHONPATH

   `export PYTHONPATH=<新的 path>:$PYTHONPATH`

   修改完之后要重新打开 terminal 或者 source .bashrc 才能生效。

3. 影响所有用户

   修改 /etc/profile 文件。系统登录时会执行 /etc/profile 文件， 在当前环境中引入指定变量。这个文件是对所有 user 都有效的。例如要修改 path，在 profile 文件中加入如下语句

   `export PATH=<新的 path>:$PATH`

   可能需要再次登录才行。

当修改变量后，特定变量引入到当前空间，其他程序也就是可以使用它们。

### 删除变量

用 `unset <变量> `命令删除 **当前 shell**  中的指定变量

例如，通过 source 加载了 PATH 之后，如果要删除其中的某些变量，就可以用 unset

unset PYTHONPATH # 删除 python 路径变量

## 常见 bug

### 安装ubuntu时freezes
1. 安装启动到选择页面时，选择install ubuntu，按下E键，可以修改安装命令。
2. 在linux开头的命令末尾加上 nouveau.modeset=0
3. 按F10启动

### 关机或重启时的死机问题
这是 NVIDIA 显卡驱动导致的问题。更新驱动即可：

Software & Updates --> Additional Drivers, Change the driver from open source to nvidia

### 查看版本信息

`lsb_release -a`  

上述命令中 lsb 是 Linux Standard Base 的缩写

### 查看系统及硬件信息

`hardinfo`

可能需要安装，用如下命令：

`sudo apt-get install hardinfo`

### 部分电脑的噪声问题
1. 在文件 /etc/modprobe.d/alsa-base.conf 加一句：options snd-hda-intel model=dell-headset-multi 即指定声卡驱动
2. 若此时无声音，则在命令行中输入 alsamixer，找到最右边的 loopback 调节一下试试。

### 无法加载windows10 NTFS分区
这是由于windows10为了快速启动，创建了休眠文件，ubuntu检测到这个文件会认为windows没有完全shutdown。

解决方法：
- 步骤1： 关闭windows的fast startup功能： Control Panel > Power Options > Choose what the power buttons do > change settings that are currently unavailable >  uncheck "turn on fast startup"
- 步骤2： 在ubuntu中打开 Disks app，找到 win10所在分区，在设置中选择 Edit Mount Options，关闭Automatic Mount Options，在下边文本框中加一个remove_hiberfile。

### 开机自动加载分区
网上给了一个非常复杂的修改 /etc/fstab 的方法，实际上对 fstab 文件的修改完全可以通过 GUI 程序轻松实现。
1. 运行 Disks GUI程序
2. 选择需要自动加载的分区
3. 点击下方的齿轮按钮
4. 选择‘Edit Mount Options’
5. off ‘Automatic Mount Options’
6. 完成。可以查看 /etc/fstab 文件，之前的修改实际上就是在文件中添加了相应的内容。

**注意**：修改之后之前的超链接文件可能无法运行，删除，然后重新建立即可。



## 常用基本命令

### 移动除某几个文件之外的所有文件

- 如果是想排除一个文件或文件夹，就在前边加 ！

  `mv !(file1) new_folder`

- 如果是想移除多个文件或文件夹，就用 | 隔开

  `mv !(file1|file2|file3) new_folder` 

### 回到之前的路径

- `cd -`: 反复执行这个命令，结果是在两个路径之间跳转，不会继续追溯更早的路径。  

### 查找用过的命令
- `history`：显示本 terminal 曾经用过的命令，如果是分屏的，则只显示本部分执行过的命令
- `ctrl + r` : 搜索命令，找到之后回车就立即执行
- `ctrl + c` : 退出搜索
- `ctrl + r again` : 查找下一个符合条件的命令
- `left/right 方向键`： 选择命令，但不执行

### 查找文件与文件夹

- **find** 
  - 区分大小写： `find <dir> -name <file>`
  - 不区分大小写： `find <dir> -iname <file>`
  - 查找文件夹： 前边都一样，后边加参数 `-type d`，例如 `find <dir> -name <folder> -type d`
- **locate**
  - `locate <file>`
- find 与 locate 的区别
  - find 是在指定目录下逐个文件搜索
    - 优点：参数更多，可以更精确的搜索文件。
    - 缺点：比较慢
  - locate 是在 database 中搜索，这个 database 每天自动更新一次，也可以手动更新 `sudo updatedb`。
    - 优点：速度快
    - 缺点：如果文件比较新，还没被 database 索引，则可能搜不到文件

- 查找可执行文件的位置： which exe_name，例如 which python, which ls

### grep 查找字符串

grep = **g**lobally search a **r**egular **e**xpression and **p**rint

- grep 可以搜索文件中的字符串，格式为 

  ```bash
  grep <pattern> [file]
  ```

  例如：

  ```bash
  grep printf /usr/include/stdio.h
  ```

  会返回 stdio.h 文件中关于 printf 字符串的搜索结果

- grep 更常见的一个用法是与 pipe 联合，在某一个命令的执行结果中搜索

  例如：

  ```bash
  ls /usr/lib | grep -i python
  ```

  加上参数 `-i` 表示不区分大小写

###  压缩与解压
 - tar文件
    - 仅归档不压缩：` tar cf *.tar file1, file2, ...  `
    - 压缩：` tar cfz *.tar.gz file1, file2, ... `
    - 从归档中提取： `tar xf *.tar`
    - 解压缩: `tar xfz *.tar.gz`，如果用 `xfvz` 就是`verbose`模式，显示更多信息
 - rar文件
    - 首先要确认安装了unrar: `sudo apt install unrar`
    - 主要参数可以通过在terminal中输入`unrar`查询
    - 解压缩: `unrar x -r *.rar`
 - zip文件
    - 确认安装了unzip
    - 解压缩： unzip  *.zip
 - 7z文件
    - 安装：sudo apt install p7zip-full
    - 解压缩： 7z x *.zip

### 建立文件和文件夹的超链接
注意：源文件和文件夹的位置必须是绝对路径！超链接存放的位置可以是相对路径。

- 文件：     
    - `ln -s source_file target_diretory`: 会在target_directory下建立与source同名的超链接文件
    - `ln -s source_file target_directory/new_name`：会在target_directory下建立new_name文件链接到source_file

- 文件夹：
    - `ln -s source_directory  target_directory`：会在target_directory下建立与source同名的超链接文件
    - `ln -s source_directory target_directory/new_name`: 会在target_directory下建立new_name文件链接到source_directory
    

实际上，不管是文件的超链接还是文件夹的超链接，都是一样的超链接文件，系统能够自动识别。如下图所示
![soft_link](pic/soft_link.png)

- 对于文件的超链接，可以用打开文件的命令
- 对于文件夹的超链接，可以用进入文件夹的命令

如果目标文件被改名、移动、删除，则超链接依然存在，但无法使用，会显示 error

### process management

#### jobs

在程序运行时，比如 gedit 占据了 terminal 窗口，通过 `ctrl+z` 可以让它进入后台并 stop，把 terminal 让出来。

`jobs`: 显示后台运行的 jobs，例如

`[1]+  Stopped  roslaunch urdf_sim_tutorial 09-joints.launch`

其中 1 为 job number

`fg %job_number` 将 Job 切换至 foreground

`bg %job_number`  将 job 切换至 background。这里切换到后台是继续运行，而不像上边那样 stop

`kill  %job_number` 可以关闭指定的 job.

####  ps

`ps`: only show basic processes

**`ps au`**: show all processes and output in user friendly formate

`ps au | grep some_name`: list the processes that contain the `some_name`

`top`: 产生详细的 process 列表，以及 CPU 和内存的占用

#### kill

在说 kill 命令之前，先要了解两个终止程序的信号：SIGTERM 和 SIGKILL。

- SIGTERM 信号可以让程序比较优雅的结束，告诉它要结束了，它可以无视，也可以很听话的保存状态，释放内存，干净的结束。

- SIGKILL 则是告诉操作系统，强制停止某个程序，这是程序本身不能掌控的。



`kill pid`: kill 2987,  kill the process of pidnumber 2987. (PID: process ID)。 这个命令实际上就是给程序本身发了一个 SIGTERM 信号，让它尽量优雅的结束。

注意： kill 后便直接加数字，表示该数字是 process PID，如果用 %num，则该数字是 job 的编号。

实际上，在一个程序接收到 SIGTERM 信号之后，常常发生以下三种情况：

1. 立即结束
2. 做些清理工作之后再结束
3. 无视，继续运行

大部分程序都是第二种情况。但是，程序本身是可以预先设置如何应对 SIGTERM 信号的，甚至可以设置成当收到 SIGTERM 信号的时候，做些其他任意设置的工作。

本质上来说 SIGTERM 只是一个比较特殊的信号，激发起程序的响应机制，但到底怎么响应，程序说了算。

CTRL + C 发送的是 SIGINT 信号，作用几乎与 SIGTERM 相同。可以不加区分。



**`kill -9 pid`**: kill the process forcefully. When you run kill -9, you're not telling the application to terminate itself, instead you're telling the OS to stop running the program, no matter what the program is doing.

这是当程序没有响应 SIGTERM 信号如愿退出时，需要更加强硬的方式结束程序。上述 kill -9 命令就是告诉 kill 发送一个 #9 信号，就是 SIGKILL 信号。实际上程序完全没有机会获取并应对这个 SIGKILL 信号，这个信号是直接作用在系统层面的。

`killall -9 processName`: killall 可以批量 terminate processes, based on given criteria such as process group, process age or user ownership. 例如 `killall -9 gzserver`可以解决 gazebo 的启动问题。

**基本上，ps au  +  kill -9 pid 足够应付绝大多数程序无响应的情况了。**



SIGKILL 也不是万能的，在某些特殊情况下，也是无法强制关闭程序，那么只能通过重启系统来清除这些程序了。



## 安装程序的常见方法

### 源码编译安装

1. 下载压缩包并解压
2. 运行 `.configure` 文件，生成 makefile 文件
3. 命令 `make` 进行编译。为了调用多核系统资源，可以加上参数 `make -j<N>` 同时运行 N 个任务。 

### 从 repository 安装

例如：

1. 先将软件所在地址加入 source.list 中，这样就可以让计算机去该地址获取软件了
    `sudo sh -c 'echo "网址 $(lsb_release -sc) main" >> /etc/apt/source.list'`

    原本 sh 命令主要是用来执行类似 .sh 的可执行文件的，默认后边跟可执行文件，用 `-c` 参数是告诉 sh 后边跟的不是一般的可执行文件，而是 command string，即一个字符串，里面包含了待执行的命令。

2. 导入 key ： `sudo apt-key adv the_long_key`

   （前边的两条也有另外比较常见的形式：
   `sudo add-apt-repository ppa:ppa_name`）

3. 更新库 cache： `sudo apt-get update`

   如果有必要，也可以在库 cache 中先搜一下有没有要安装的 app

   ```bash
   apt-cache search terminal | grep -i drop-down
   ```

4. 安装指定应用程序：`sudo apt-get install apt_name`

- 如果要删掉某个ppa库，很简单，从保存ppa库的文件夹` /etc/apt/sources.list.d`中删掉对应的ppa即可。
- 有时在 `sudo apt update` 的时候会提示找不到某个 url ，可能是因为那个网址以及废弃了。只需要将 /etc/apt/source.list 中对应的行去掉就行。

这种安装方式的优点是，已经链接上程序的 repository，以后 sudo apt update 的时候可以自动更新，一般最好这样安装。

### 从 deb 文件安装

这种方式适合那些不能联网的机器。

### 从 script sh 文件安装

这是开发者预先写好的安装批处理文件，基本上是一键安装。但是中间流程可能没有考虑全面，在不同的环境下安装有可能会出错。

## 安装 OpenCV (3.2.0)

**Step 1. Updating Ubuntu **

```
$ sudo apt-get update
 
$ sudo apt-get upgrade
```

**Step2. Install dependencies **

```
$ sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

$ sudo apt-get install python3.5-dev python3-numpy libtbb2 libtbb-dev

$ sudo apt-get install libjpeg-dev libpng-dev libtiff5-dev libjasper-dev libdc1394-22-dev libeigen3-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev sphinx-common libtbb-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libopenexr-dev libgstreamer-plugins-base1.0-dev libavutil-dev libavfilter-dev libavresample-dev
```

**Step 3. Get Opencv **

```
$ sudo -s   (进入 super user 模式)
 
$ cd /opt

### 下载最新的版本
/opt$ git clone https://github.com/Itseez/opencv.git  
/opt$ git clone https://github.com/Itseez/opencv_contrib.git

### 或者下载特定版本 （以3.2版本为例）
wget https://github.com/opencv/opencv/archive/3.2.0.tar.gz

tar -xvzf 3.2.0.tar.gz

wget https://github.com/opencv/opencv_contrib/archive/3.2.0.zip

unzip 3.2.0.zip

### 为了方便起见，以上解压的文件夹改名字为 opencv 和 opencv_contrib，命令省略
```

** Step 4. Build and install **

```
/opt$ cd opencv
 
/opt/opencv$ mkdir release
 
/opt/opencv$ cd release
 
/opt/opencv/release$ cmake -D BUILD_TIFF=ON -D WITH_CUDA=OFF -D ENABLE_AVX=OFF -D WITH_OPENGL=OFF -D WITH_OPENCL=OFF -D WITH_IPP=OFF -D WITH_TBB=ON -D BUILD_TBB=ON -D WITH_EIGEN=OFF -D WITH_V4L=OFF -D WITH_VTK=OFF -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=/opt/opencv_contrib/modules /opt/opencv/
 
/opt/opencv/release$ make -j4
 
/opt/opencv/release$ make install
 
/opt/opencv/release$ ldconfig  # 更新动态链接库

(可用如下命令查看动态链接库中是否有了 opencv ): 

ldconfig -p | grep opencv  

-p 是print

/opt/opencv/release$ exit
 
/opt/opencv/release$ cd ~
```

**Step 5. Check **

```
$ pkg-config --modversion opencv

### 结果应为
3.2.x
```



## sudo apt -parameters
- sudo apt **-y** install vim: assume "yes" as answer to all prompts 

## /bin, /usr/bin, /usr/local/bin

1. `/bin` (and `/sbin`) were intended for programs that needed to be on a small `/` partition before the larger `/usr`, etc. partitions were mounted. These days, it mostly serves as a standard location for key programs like `/bin/sh`.

   /bin 存放基础的小程序

2. `/sbin`, as distinct from `/bin`, is for system management programs (not normally used by ordinary users) needed before `/usr` is mounted.

   /sbin 存放基础的系统程序，一般用户用不到的

3. `/usr/bin` is for distribution-managed normal user programs.

   /usr/bin 存放由 software manager 管理的程序，一般是通过 debian 方式安装的

4. There is a `/usr/sbin` with the same relationship to `/usr/bin` as `/sbin` has to `/bin`.

5. `/usr/local/bin` is for normal user programs *not* managed by the distribution package manager, e.g. locally compiled packages. You should not install them into `/usr/bin` because future distribution upgrades may modify or delete them without warning.

   /usr/local/bin 存放源文件编译的程序，而不是 debian 安装的

6. `/usr/local/sbin`, as you can probably guess at this point, is to `/usr/local/bin` as `/usr/sbin` to `/usr/bin`.

In addition, there is also `/opt` which is for monolithic non-distribution packages, although before they were properly integrated various distributions put Gnome and KDE there. Generally you should reserve it for large, poorly behaved third party packages such as Oracle.

/opt 里面存放的是比较大型的程序。



## source 命令

source FILE 本质上是在本 shell 中加载并执行 FILE 中的程序。

因此，source .bashrc 就是在当前 shell 中加载 .bashrc ，并执行其中的程序。确实， .bashrc 中的内容都是命令语句，包括 PATH=...,  export ...。

source FILE 还可以简写成 . FILE

需要注意的是，source 是在本 shell 中加载，所以才会在每次修改了 .bashrc，用 source .bashrc 刷新一下本 shell 中的环境变量设置。



## shebang line

在 linux 系统中，脚本文件的第一行经常用来指定运行该文件的程序，一般以 `#!`开头。

程序开头的这一行称为 shebang /ʃəˈbæŋ/ line。一种解释是 shebang = hash (#) + bang (!)。在 Unix 类的系统中，如 linux, mac等，当文件被执行时，如果文件的第一行的前两个符号是 `#!` 就知道这是一个解释性脚本程序，于是调用 shebang line后边指定的解释器来解释程序。



例如 Python 文件中，第一行可以是

```python
#!/usr/bin/env python
```

这一行指明寻找 PATH 中遇到的第一个 python，不管它在那个文件夹下，不管什么版本。
或者

```python
#!/usr/bin/python
```

这一行指明调用 `/usr/bin`下的 python 。


这个 shebang line 在 python 中尤其重要，因为 python 版本的多样性，有些情况下可能需要指定用哪个版本的 python 去执行程序。

例如：

```shell
$ /usr/local/bin/python -V    # 假设 /usr/local/bin 下版本为2.6.4
Python 2.6.4
$ /usr/bin/python -V          # 假设 /usr/bin 下版本为2.5.1
Python 2.5.1
$ cat my_script.py
#!/usr/bin/env python      # shebang line 指明用 PATH 中第一个 python
import json
print "hello, json"
$ PATH=/usr/local/bin:/usr/bin  # PATH 中第一个 python 应为 2.6.4
$ ./my_script.py 
hello, json                     # 运行程序，正常
$ PATH=/usr/bin:/usr/local/bin   # 将 PATH 中第一个 python 改为 2.5.1
$ ./my_script.py 
Traceback (most recent call last):
  File "./my_script.py", line 2, in <module>
    import json
ImportError: No module named json    # 会报错，因为 2.5.1中没有 json 
```



另外，在 shebang line 后边可以再加一行，例如
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
```
这样文件中就可以用 non-ASCII 编码的字符了。



注意：当直接运行脚本文件时，shebang line 可以告诉系统调用哪个程序执行该文件。

但是当在命令行中指明了用哪个程序执行文件时，如 `sh hello_world.sh`，则 shebang line 的设置不再起作用。



## shell 编程

shell 编程就是对一系列 shell 命令的批处理，其中加上一些逻辑控制，可以实现强大的功能。

原本需要一条一条语句执行，用了 shell 编程就可以通过执行脚本文件，一次性完成所有操作步骤。

主要：在 Bash 中，双引号包裹的字符串会被解读，而单引号不会，例如

```bash
$ echo "$(echo "upg")"
upg

$ echo '$(echo "upg")'
$(echo "upg")
```

### 变量

shell 编程中可以使用变量，实际上 terminal 一条一条的shell 命令中也是可以使用的。

这里的变量包括 环境变量 和 自定义变量 两种。

- 环境变量：系统自带的，如 PATH, HOME 等
- 自定义变量：用户自定义的，如 greeting="Hello, world!" 就新定义了变量 greeting

包含变量的 shell 脚本程序如下：

``` shell
#!/bin/bash
#使用环境变量
echo $PATH
#自定义变量hello
hello="hello world"  # 等号两边不能有空格
echo $hello # echo 表示返回后边的内容，如果不用 echo，就是直接执行后边的内容，当成了程序了，一般会报错。
```



### 保存命令的返回结果

这个功能在普通的 shell 命令中也是可以用的。

两种方式取出程序执行结果

- $( ) :  美元符号 + 括号
- \` \` :  反单引号 

```shell
#!/bin/bash
path=$(pwd)   # 第一种方式
files=`ls -al` # 第二种方式
echo current path: $path
echo files: $files
```



### 数据运算

这也是在 shell 中一行一行命令可以实现的。

- 四则运算： +, -, *, /
- 求余运算：%
- 比较运算： ==, !=, >, <, >=, <=，如果为真，则返回 1，否则返回 0

实现数据运算的方式有两种

- expr : 

  ```shell
  #!/bin/bash
  #输出13
  expr 10 + 3
  
  #输出10+3
  expr 10+3   # 运算符两边必须有空格，否则直接当成字符串返回
  
  #输出7
  expr 10 - 3
  
  #输出30
  expr 10 \* 3  # 乘号要转义，另外 >, <, >=, <= 都需要转义，因为原本为重定向符号
  
  #输出3
  expr 10 / 3
  
  #输出1
  expr 10 % 3
  
  #将计算结果赋值给变量
  num1=$(expr 10 % 3)
  
  #将计算结果赋值给变量
  num2=`expr 10 % 3`
  ```

  

- $[ ]：上述 expr 方式不太直观，有很多需要注意的地方。这种方法就很简单，不用记那么多特殊设置。

  ```bash
  #!/bin/bash
  num1=10
  num2=3
  #输出num1 + num2=13
  echo "num1 + num2=$[$num1 + $num2]"  
  
  #输出num1+num2=13
  echo "num1+num2=$[$num1+$num2]"  # 空不空格都可以
  
  #输出num1 - num2=7
  echo "num1 - num2=$[$num1 - $num2]"
  
  #输出num1 * num2=30
  echo "num1 * num2=$[$num1 * $num2]"  
  
  #输出num1 > num2=1
  echo "num1 > num2=$[$num1 > $num2]"
  
  #输出num1 < num2=0
  echo "num1 < num2=$[$num1 < $num2]"  # 所有符号直接用，不需要转义
  
  #将运算结果赋值给变量，输出num3=3
  num3=$[$num1 / $num2]
  echo "num3=$num3"
  ```

上边默认都是整数运算，所以除法都是返回整数值。

为了做浮点运算，需要用 bc 函数，如下

`variable=$(echo "options; expression" | bc)`

如果只计算，不保存，可以更简单一些

`echo "options; expression" | bc`

其中 

- options 是 bc 的一些选项，具体的参数可以通过 man bc 查看
- expression 是要计算的表达式

例子：

```bash
#!/bin/bash
#表示 10/3， 保留2位小数，将结果赋值给了num, 输出3.33
num=$(echo "scale=2; 10 / 3" | bc)
echo $num

echo "scale=2; 10/3" | bc  # 返回 3.33 
```



### 条件选择

一般格式为

```bash
if 判断语句
then 
	执行语句
elif 
	判断语句
then 
	执行语句
else 
	执行语句
fi
```

其中判断语句有两种情况：

- 命令，此时如果命令正常运行（正常退出，退出码为 0），则执行后边的语句，否则不执行

  例如：

  ```bash
  #!/bin/bash
  #这儿由于pwd是linux内置的命令，因此执行后会正常退出（状态码0），所以会执行then中的语句
  #如果此处替换为一个不存在的命令（例如: pw），那么就会非正常退出，不会执行then中的语句
  if pwd
  then
     echo 执行then里面的语句
  fi
  ```

  或者可以简写

  ```bash
  #!/bin/bash
  if pwd; then echo 执行then里面的语句; fi
  ```

  

- 逻辑结果，这是其他语言里面比较常用的。

  这里又有不同的比较，

  - 数值比较：使用双括号将比较的表达式括起来，例如

    ```bash
    #!/bin/bash
    num1=100
    num2=200
    if (( num1 > num2 )) 
    then
        echo "num1 > num2"
    else 
        echo "num2 <= num2" 
    ```

  - 字符串比较：使用中括号括起来，例如

    ```bash
    #!/bin/bash
    var1=test
    var2=Test
    if [[ $test < $test2 ]]
    then
        echo "test1 < test2"
    else
        echo "test1 >= test2"
    fi    
    ```

除了 if then 格式，当情况比较多时，可以用 case 格式，如下

```bash
#!/bin/bash
case $num in
1)
    echo "num=1";;
2)
    echo "num=2";;
3)
    echo "num=3";;
4)
    echo "num=4";;
*)
    echo "defaul";;
esac 
```



### 循环语句

#### for 循环

基本格式为 

```bash
for var in list
do
	*
done
```

例如

```bash
for str in a b c d e
do
    echo $str
done 
```

或者先定义  list 变量

```bash
list="a b c d e"
for str in $list
do
    echo $str
done 
```

在默认情况下，list 中的元素以空格、tab、换行进行分割的。

这个设置可以在环境变量 IFS (Internal Field Separator) 中修改。例如

```bash
#!/bin/bash
#定义一个变量oldIFS保存未修改前的IFS的值
oldIFS=$IFS
#修改IFS值，以逗号为分隔符
IFS=$','
list=a,b,c,d,e
for var in $list
do
    echo $var
done
#还原IFS的值
IFS=$oldIFS
```

除了 for  *  in * 这种 python 风格的形式，也可以用更加传统的 C 语言风格的形式，例如

```bash
#!/bin/bash
for (( i = 0; i <= 10; i++ ))
do
    echo $i
done   
```



#### while 循环

知道了前边的 for 循环，while 循环也很好理解，例如

```bash
flag=0
while (( $flag <= 10 ))
do
    echo $flag
    flag=$[$flag + 1]
done
```



#### until 循环

这个可以看作 while 的相反情况，只有情况不成立时，才执行，例如

```bash
#!/bin/bash
flag=0
until (( $flag > 10 ))
do
    echo $flag
    flag=$[ $flag + 1 ]
done
```



#### break

跳出循环，例如

```bash
#!/bin/bash
for (( flag=0; flag <= 10; flag++ ))
do
    if (( $flag == 5 ))
    then
        break   # flag 变量为 5 时，跳出循环
    fi
    echo $flag
done
```

注意，默认情况下，break 只是跳出最内层的循环，如果要跳出外边若干层的循环，可以在后边加数字，例如

```bash
#!/bin/bash
flag=0
while (( $flag < 10 ))
do
    for (( innerFlag=0; innerFlag < 5; innerFlag++ ))
    do
        if (( $innerFlag == 2 ))
        then
            break 2  # 跳出 次内层 的循环
        fi
        echo "innerFlag=$innerFlag"
    done
    echo "outerFlag=$flag"
done
```



#### continue

用法与 break 完全相同，不是跳出循环，而是执行下一次循环。也可以加数字。



### 命令行参数处理

有时在执行脚本程序时，希望送入一些参数，例如 `./testinput.sh 12 34`

在脚本中可以通过 \$1 到 \$9 获取 9 个命令行参数，另外 \$0 为脚本文件的名字。如果超过 9 个参数，就用 \${ } 来获取。例如

```bash
#!/bin/bash
#testinput.sh
echo "file name: $0"  # $0 返回的名字包含不必要的路径
echo "base file name: $(basename $0)"  # 先用 basename 命令处理一些，得到纯粹的文件名
echo "param1: $1"
echo "param2: ${2}"
```

那么执行 `./testinput.sh 12 34`，结果为

```bash
file name: ./testinput4.sh
base file name: testinput4.sh
param1: 12
param2: 34
```



为了一次性获取所有参数，可以用 `$*` 和`$@` 两种形式：

```bash
#!/bin/bash

echo "-- \$* 演示 ---"
for i in "$*"; do
    echo $i
done

echo "-- \$@ 演示 ---"
for i in "$@"; do
    echo $i
done
```

运行

```bash
chmod +x test.sh 
./test.sh 1 2 3
```

结果为

```bash
-- $* 演示 ---
1 2 3
-- $@ 演示 ---
1
2
3
```



### 用户输入处理

在需要读取用户输入的情况下，用 read 命令，例如

```bash
#!/bin/bash
echo -n "yes or no(y/n)?"   # -n 表示不换行
read choice   # 将用户输入赋给 choice 变量
echo "your choice: $choice"
```

多个输入

```bash
#!/bin/bash
read -p "what's your name?" first last   # 相当于前边的 echo -n + read 不换行效果
echo first: $first
echo last: $last
```

另外还可以设置用户输入超时

```bash
#/bin/bash
if read -t 5 -p "Please enter your name: " name  # -t 5 超过 5 秒没输入就非正常退出。
then
    echo "Hello $name"
else
    echo "Sorry, timeout! "
fi
```



## 安装 nvidia 驱动，cuda，cudnn

### nvidia 驱动

1. 将 nvidia repository 加入 ppa: 

   ```bash
   sudo add-apt-repository ppa:graphics-drivers/ppa
   ```

2. 在 terminal 中更新 ppa 并安装 (例如 410 版本)　

   ```bash
   sudo apt update
   sudo apt install nvidia-410
   ```

3. 看看附加驱动是否换成了 410，可能需要重启

### cuda 驱动

按照官网指示安装即可

安装之后查看　cuda 版本　：｀nvcc -V｀ 如果提示找不到，很可能是路径没有设置好

nvcc 程序在　/usr/local/cuda/bin 文件夹中，可以在　.bashrc 文件中添加　path

`export PATH=$PATH:/usr/local/cuda/bin`

### cudnn 驱动

也是按照官网指示安装

要安装所有的三个　deb ，然后做测试．



## chrome 的 keyring 问题

在开机首次登录 chrome 的时候，总会提示输入密码，很烦。

这是由于chrome 浏览器将网站密码都保存了起来，相当于搞了一个钥匙环 （keyring）保存了很多钥匙。然后给这个钥匙环设置了密码，只要输入了这个密码，就可以查看里面所有的钥匙。

如果不想每次登录 chrome 都输入密码，可以把钥匙环的密码删掉。

- 搜索系统的应用程序 passwords and keys, 在 password 中有 keyring，把它删掉。
- 再次启动 chrome，提示设置密码，设置为空即可。

没有 keyring 密码，则任何人都可以打开 chrome，然后查看 chrome 中明文保存的各网站密码，有潜在安全风险！



## SSH

### 基本安装与设置

SSH 是一种网络协议，用于计算机之间的加密登录。如果一个用户从本地计算机通过 SSH 协议登录远程计算机

，一般认为这个登录是安全的，不用担心中途截获泄密的问题。



SSH 只是一种协议，它有很多实现，Linux 下比较常用的是 OpenSSH，Windows 下用 SSH 需要软件 PuTTY.

默认情况下，SSH 远程登录用的是手动输入密码方式。

经过配置，也可以使用更加方便的密钥登录方式，这样就可以不必每次登录都输入密码了！



默认情况下，Ubuntu 安装了 SSH 的客户端程序，但是没有服务器端程序。这样的话，可以连到其他 SSH 服务器，但是别人连不进来。

1. 查看本机是否安装了 SSH 服务器端程序

```
pidof sshd
```

sshd 是 SSH 服务器端程序，监听外部连接的。

或者通过 systemctl 查看

```
sudo systemctl status ssh
```



2. 如果通过上述命令发现没有 SSH 服务端程序，则需要额外安装

   ```
   sudo apt install openssh-server
   ```

   如果安装成功，则上述 pidof 命令可以显示 sshd 程序的 pid

   

### 常用命令

- 查看状态，及开、关、重启，都用 systemctl 程序，这是 ubuntu 16 及更新的版本推荐的

  ```
  sudo systemctl status ssh  # 查看状态，一般应该是 active 
  sudo systemctl stop ssh  # 关闭 ssh 服务，现在再查看状态，应该是 inactive，同时 pidof sshd 也没有了
  sudo systemctl start ssh  # 开启 ssh 服务，现在又成了 active
  sudo systemctl restart ssh # 重启 ssh 服务
  sudo systemctl enable ssh  # 设置开机启动
  ```

- 远程连接

  ```
  ssh  user_name@IP_addr  # 需要输入远程机子的用户名和IP
  ```

  1. 这一步之后，如果是第一次登录，则会让确认远端没有安全问题
  2. 如果用户确认没有问题，则会在用户目录中的 .ssh/known_hosts 文件中存储远程系统的公钥，同时也会把本地的公钥发过去。以后再次登录时就不再提示确认安全了。此后所有的消息传递都是用对方的公钥加密的，不会被第三方破解。
  3. 然后就是会要求输入 user_name 对应的密码，输入即可登录。这里输入的密码通过加密的方式传到远端，而 telnet 是不加密的，因此 ssh 方式更安全，不用担心密码泄漏。

- 一次性连接

  ```
  ssh user_name@IP_addr "cmd_to_conduct"  # 将要执行的命令放入双引号中
  ```

  上述命令不登录系统，只是在远端执行命令，返回结果。

- 文件复制

  ```
  scp source_user_name@IP:path_to_file  target_user_name@IP:path_to_file
  ```

  当前所在端（本地或远端）可以直接写文件路径。



### 密钥登录方式

基本操作就是把本地的 id_rsa.pub 传到远端，将其中的内容写入远端 .ssh/文件夹下的 authorized_keys 文件中，以后再登录远端，就会自动检测本地私钥与远端公钥是否匹配。

具体操作如下：

1. 如果本地还没有密钥，就先生成一组

   ```
   ssh-keygen
   ```

   上述命令会在 ~/.ssh 文件夹中生成一对公钥 id_rsa.pub 和私钥 id_rsa。

   默认是 RSA 算法。也可以指定加密算法：

   ```
   ssh-keygen -t [dsa | ecdsa | ed25519 | rsa | rsa1]
   ```

   其中 ecdsa 就是 ECC。目前 ECC 只在较新版本的 SSH 中才能使用，但是大势所趋。

   

2. 通过 scp 将公钥 id_rsa.pub 复制到远端

   ```
   scp ~/.ssh/id_rsa.pub  user_name@IP:~/
   ```

3. 在远端，将此公钥的内容写入 .ssh/authorized_keys 文件中

   ```
   cat id_rsd.pub >> ~/.ssh/authorized_keys
   ```

以后再登录远端就不再需要输入密码了！

实际上，为了确认登录的是可靠用户，每次登录的时候两端会进行如下的验证：

1. 服务器端生成一个随机数，用客户端的公钥加密，发给客户端
2. 客户端用自己的私钥解密，发还给服务器
3. 服务器端确认客户端持有正确的私钥，允许登录



### 可能有用的额外设置

一般来说，密钥比密码安全性更高。所以在设置了密钥登录之后，可以禁用掉密码登录方式

在远端的 /etc/ssh/sshd_config 文件中，确认

```
RSAAuthentication yes
PubkeyAuthentication yes
```

表明可以密钥登录。

然后禁用密码登录

```
PasswordAuthentication no
```

这样就只能用密钥，不能用密码登录了。

不过一般不需要这么严格，保持默认即可。



## 加密算法

### MD5， SHA

SHA： **Secure Hash Algorithm**

这类算法的目的是保证数据内容的真实性，没有被篡改。

这两种都是数据的数字指纹 MessageDigest (消息摘要)算法，即对一个数据进行计算，得到唯一的指纹号。

MessageDigest 有两个特点：

- 两个不同的数据，很难生成相同的指纹号
- 对于给定的指纹号，很难逆向计算原始数据

目前在网络中下载软件的时候，很多比较严谨的网站会提供软件对应的指纹号，以免下载的软件被人篡改，加入恶意代码。用户下载到软件之后，用某些MD5/SHA 生成器，就可以的到软件对应的指纹号，对比官方给定的指纹号，即可知道软件内容是否被篡改。除非黑客非常牛逼，把软件篡改之后，用对应的指纹号替换官网给出的指纹号而不被发现。



### DES

DES： Data Encryption Standard 

这类算法的目的是保证数据的隐私性，不被第三方知晓。

发送方采用密钥进行数据加密，信息的接收方用同一个密钥解密。

这种单密钥算法是一个对称算法，双方用同一个密码加密或解密。

缺点：在多用户情况下，密钥被很多人知晓，密钥的安全保管是一个问题。



### RSA

目的也是为了消息不被第三方知晓而做的加密。这是一种非对称加密算法。

RSA 是1977年发明的，以三位作者的名字首字母命名 （Ron **R**ivest, Adi **S**hamir, and Leonard **A**dleman）。

RSA的安全性基于大整数的分解。

作为非对称加密算法，RSA 有公钥和私钥两部分。

- 公钥用于加密，是广播给他人的
- 私钥用于解密，保留在本地

例如： A 向 B 发送数据，首先 A 用 B 的公钥加密，发送给 B，B 收到之后用私钥解密，获取数据。在这个过程中，即使传输的数据被第三方获得，由于缺少私钥，无法解密数据。这就保证了消息不被第三方知晓。

### DSA 

在上述非对称加密中，可以有效防止信息泄漏的问题，第三方攻击者无法获得真实信息。这就是解决了信息泄漏的问题。

但是还存在一个信息真实性的问题： B 如何确认消息是来自 A 而不是其他人？这里就需要数字签名算法了。



最常见的数字签名算法是 DSA: Digital Signature Algorithm

- 生成数字签名速度很快，验证速度很慢。

- 加密时更慢，解密很快。

因此 DSA 一般只用来做数字签名，不做大量数据的加密。



实际上，数字签名算法可以认为是一种加密的DigitalMessage。以下图为例：

![DSA](pic/DSA.png)

流程如下：

1. A 将 EMAIL 执行哈希运算得到 hash 值，即 MessageDigest，消息摘要，记做 h1
2. A 用自己的私钥对 h1 加密，生成数字签名
3. A 将 EMAIL 和数字签名 一起用 B 的公钥加密，发送给 B
4. B 收到数据之后用私钥解密，得到 EMAIL 和 加密的 h1
5. B 再用 A 的公钥解密 h1，得到 hash 值，记做 h2
6. 对比 h1 和 h2，如果相同，则确定数据是 A 发送过来的。



### ECDSA 算法

ECDSA ：Elliptic Curve Digital Signature Algorithm

相比于 RSA 和 DSA ，ECC 有明显的优点：

1.  相同密钥长度，ECC 安全性更高。160位 ECC 与 1024 位 RSA， DSA 有相同的安全强度
2. 计算量小，处理速度快，在私钥方面（解密和签名）ECC 远比 RSA， DSA 快的多
3. 占用存储空间小
4. 带宽要求低

