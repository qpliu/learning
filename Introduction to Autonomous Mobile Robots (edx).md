**Introduction to Autonomous Mobile Robots (edx)**

# Lecture overview

## All in one: The see-think-act cycle

![see_think_act](pic/see_think_act_cycle1.png)

##  Perception

![perception](pic/perception1.png)



## Cognition and path planning

![cognition1](pic/cognition1.png)

![cognition](pic/cognition2.png)



## Motion control

How to control the robot to reach a desired state (speed, position, acceleration)?

![motion_control1](pic/motion_control1.png)

# Perception

## Sensors can be described here

# Localization

## Basic concepts of probability theory

##  Many filters

### The general Bayes filter

### Markov localization 

also known as histogram localization. 针对 discrete space or grid 

### Kalman filter

## SLAM





# Kinematics

Motion can be described from many respects. Here we only consider kinematics.

## Definition of kinematics

Description of the motion of points, bodies, or systems of bodies **without consideration of the cause of motion**.

## The 3 kinematics equations can be described here





