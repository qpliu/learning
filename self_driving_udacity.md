# Self-driving Car Nanodegree Course (Udacity)

## 课程结构

- Term 1
  - Computer vision
  - Deep learning
  - Sensor fusion
- Term 2
  - Localization
  - Path planning
  - Control
  - System integration

