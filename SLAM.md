

# SLAM 分类

SLAM (Simultaneous localization and mapping，同步定位和制图) 的概念自 1986 年提出以来，得到了业内人士的广泛关注。

目前比较常见的 SLAM 技术包括

- 基于滤波的 SLAM ：由贝叶斯滤波器得到，通过迭代估计自动驾驶汽车的状态，并更新地图。最常用的滤波器有扩展卡尔曼滤波器，无迹卡尔曼滤波器，信息滤波器，例子滤波器。
- 基于优化的 SLAM：通过寻找新观测数据与地图之间的对应关系来确定问题约束，然后更新汽车状态，并更新地图。目前有两个分支：
  - bundle adjustment (BA)：利用牛顿-高斯法、梯度下降等优化技术，通过最小化误差函数，联合优化地图与车辆姿态
  - graph SLAM：将定位问题建模为图形表示问题，通过寻找不同的车辆姿态的误差函数来求解。







# visual SLAM

The term Visual SLAM comprises all SLAM approaches that take image-like data as input.

So there are two kinds of SLAM: 

-  Visual SLAM:

  From whether CNN is used

  - classical Visual SLAM
  - CNN based Visual SLAM

  From whether pixels are used directly

  - feature based
  - use image pixels directly 

- SLAM based on other sensors

## why use visual SLAM 

Due to the high resolution of cameras compared to other sensors like RADAR or  LIDAR, situations that require a detailed knowledge about the environment or generate ambiguous signals from other sensors are dedicated for the application of Visual SLAM.

# Fundamental Pipeline of VSLAM

The fundamental pipeline for Visual SLAM is composed of 

- tracking
- mapping
- global optimization
- re-localization

## Tracking

 tracking between consecutive camera images is utilized in order to generate a local camera trajectory as well as depth information.

Usually, this task ends up in a non-linear optimization problem. In most approaches, so called **key frames** are used as a base for tracking. Once tracking indicates that there is not enough overlap between the current camera frame and the key frame, a new key frame is created.

## Mapping

This step is where the main difference between feature based and direct methods is located.

- feature based: sparse feature maps
- direct method: dense point maps

in some of the approaches, key frames are stored in a graph with the edges representing the transformation between key frames.

## Global optimization

This is needed for correcting the global map with drift errors.

The optimization step relies on recognizing a place that has been seen and mapped before and therefore detecting a loop closure. Based on this detection, all camera poses can be optimization

## Re-localization

This is the procedure of placing the sensor at an unknown pose in the map and trying to estimate the pose. This is usually done by comparing the current sensor data with the map.

![vslam1](pic/vslam1.jpg)

# Feature based Visual SLAM

This approach utilize descriptive image features for tracking and  depth estimation. This results in sparse feature maps.

## MonoSLAM

Proposed in 

> A. J. Davison, I. D. Reid, N. D. Molton, and O. Stasse. Monoslam: Real-time single camera slam. IEEE Trans. Pattern Anal. Mach. Intell., 29(6):1052–1067, June 2007. 1, 3  

This is the first Visual SLAM approach. It uses EKF-based feature tracking. There is no loop closure detection and in order to achieve real-time performance, only few feature points per frame are considered.

## PTAM 

Parallel tracking and mapping.

Extends the approach of Davison by parallelizing the feature point matching part in order to improve real-time performance.

For optimization, it uses bundle adjustment (BA), and it can handle more feature points which increases robustness.

## ORB-SLAM

Extends PTAM by adding loop closure detection and global pose graph optimization. 

It also relies on the ORB feature descriptor which is known to be robust while having low computational cost.

# Direct SLAM

Not rely on features but on the whole image. This gives the chance to acquire a dense environment model.

## DTAM

Dense tracking and mapping.

> R. A. Newcombe, S. J. Lovegrove, and A. J. Davison. Dtam: Dense tracking and mapping in real-time. In Proceedings of the 2011 International Conference on Computer Vision, ICCV ’11, pages 2320–2327, Washington, DC, USA, 2011. IEEE Computer Society.  

This is the first direct method published. 

While lacking features like loop closure detection or global optimization, it introduces tracking on key frames based on minimization of the photometric error.

The mapping space is discretized into a 3D grid which limits the maximum size of the map.

## LSD-SLAM

Large-scale semi dense SLAM.

Also based on the minimization of the photometric error.

It extends the functionality to large scale by building a pose frame graph and global optimization including loop closure detection.

## DSO

Direct Sparse Odometry.

Extend the minimization model of LSD-SLAM by taking the geometric error into account as well as exposure time and lens distortion.

Although being a direct method, the map generated is sparse in order to achieve real-time performance.

# Comparison of ORB and DSO

![compare_slam](pic/compare_slam.jpg)

The above figure shows the comparison for stereo versus monocular SLAM. 

Although the stereo results are acceptable, the monocular results are weak and unacceptable for automated driving. 

From that we derive lots of potential using deep learning techniques to improve.

# Types of HD map

- Dense semantic point cloud maps
- Semantic landmark based maps

## Dense Semantic Point Cloud maps

This is the high-end version where all the semantics and dense point cloud are available at high accuracy. 

Google and TomTom adopt this strategy. 

It is expensive to cover the entire world and needs large memory requirements.

## Landmark based Maps

This is based on semantic objects instead of generic 3D point clouds. Thus it works primarily for camera data.

Mobileye and HERE follow this strategy.





