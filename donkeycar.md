## 参考哪个 docs 文件？

从香港 robocarstore 买到小车之后，附带的说明里面给的参考文章是 https://docs.robocarstore.com

但是这个参考的软件部分应该是比较旧的，装完 donkey 软件之后打开的版本是 2.**。

应该用 donkey car 的官网指导 docs.donkeycar.com，这个更新一些，版本是 3.**



## 硬件组成

硬件安装这一部分，包括零件清单，可以参考 [robocar store](http://docs.robocarstore.com/guide/build_hardware)的文档，这里介绍的比较详细。

主要部件包括：

- 车体

  从香港寄过来的车型号是 [HSP 94186 RC car](<https://www.robocarstore.com/products/hsp-94186-for-donkey-car>)，对应了 donkey car 官网上的 Exceed Magnet 型号。
  
  除了比较常用的四种，只要满足一定条件，任何的 RC car 都可以作为 donkey car 的车体。

- 电池：NiMH Battery

- 树莓派板子 

- 伺服驱动板子 16-Channel Servo Driver PCA9685
- 直流电压转换 DC-DC 5V/2A Voltage Converter 



## Raspberry Pi 3 B+



![raspberry_pi_circuit](./pic/raspberry_pi_circuit.jpg)

Raspberry Pi 3 Model B+ 技术参数如下：

- Broadcom BCM2837B0, Cortex-A53 (ARMv8) 64-bit SoC @ 1.4GHz quad-core 
- 1GB LPDDR2 SDRAM
- 2.4GHz and 5GHz IEEE 802.11.b/g/n/ac wireless LAN, Bluetooth 4.2, BLE
- Gigabit Ethernet over USB 2.0 (maximum throughput 300 Mbps)
- Extended 40-pin GPIO header
- HDMI
- 4 USB 2.0 ports
- CSI camera port for connecting a Raspberry Pi camera
- DSI display port for connecting a Raspberry Pi touchscreen display
- 4-pole stereo output and composite video port
- Micro SD port 
- 5V/2.5A DC power input
- Power-over-Ethernet (PoE) support (requires separate PoE HAT)





## 软件安装

包括在 PC 上的安装和在 raspberry pi 上的安装。

PC 的主要作用是远程控制小车，训练神经网络；

raspberry pi 的主要作用是收集数据，传给 PC 去训练。

### PC 上的安装

基本上严格按照[官方文档](http://docs.donkeycar.com/guide/host_pc/setup_ubuntu/)安装即可。

 注意在下边这一步安装命令中

```bash
pip install -e .[pc]
```

就是要原样写成 `.[pc]` 的形式，这是对应 PC 上的安装参数。 

### raspberry pi 上的安装

这里官方文档默认用户没有显示屏连接到树莓派板子上，所以很多工作都是先在 PC 上修改 SD 卡中的文档，然后拿到树莓派上去用。

方便起见，不妨通过 HDMI 把树莓派连接到显示器上，然后再插上鼠标、键盘。这样就可以把树莓派板子当成一个小主机来用，系统配置更加方便。

树莓派系统的安装部分强烈建议参考[树莓派官网的教程](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up)。[官网主页](https://www.raspberrypi.org/downloads/raspbian/)只能下载到最新版本的 raspbian （当前为 Buster），如果想用旧版本的 Stretch, Jessie, Wheezy，可以在[这里](http://downloads.raspberrypi.org/)找，有 noobs 方式的安装文件，也有纯 Image 的。由于担心兼容性问题，我没有用最新的 Buster 版本，而是 Stretch 版本。

具体安装流程：

1. 先做个系统启动盘。在 PC 上从[树莓派官网下载  noobs](https://www.raspberrypi.org/downloads/) ，将 noobs 中的文件解压缩，直接拷贝到 SD 卡中。这样一个树莓派启动盘就做好了，而且是带有图形界面的。

2. 将 SD 卡插入树莓派。从前边的树莓派技术参数可以看出来，树莓派有内存 （1 GB） 但是没有硬盘。所有的文档包括系统文件都是存放在 SD 卡中的。板子本身没有开关，只要插电马上启动。小车本身的电池供电时间比较短，最好找个外部 USB 接口供电。

3. 按照[树莓派官网的教程](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up) 装好系统。

4. 配置系统：

   ```
   sudo raspi-config
   ```

   这里要启用 camera, i2c, ssh，如下

   ```
   enable Interfacing Options | Camera
   enable Interfacing Options | I2C
   enable Interfacing Options | SSH
   ```

5. 安装依赖包

   ```
   sudo apt-get install build-essential python3 python3-dev python3-virtualenv python3-numpy python3-picamera python3-pandas python3-rpi.gpio i2c-tools avahi-utils joystick libopenjp2-7-dev libtiff5-dev gfortran libatlas-base-dev libopenblas-dev libhdf5-serial-dev git
   
   sudo apt-get install libilmbase-dev libopenexr-dev libgstreamer1.0-dev libjasper-dev libwebp-dev libatlas-base-dev libavcodec-dev libavformat-dev libswscale-dev libqtgui4 libqt4-test
   ```

6. 设置虚拟环境

   ```
   python3 -m virtualenv -p python3 env --system-site-packages
   
   # 每次打开 terminal，自动加入虚拟环境
   echo "source env/bin/activate" >> ~/.bashrc
   
   source ~/.bashrc
   ```

7. 安装 donkeycar 应用

   ```
   git clone https://github.com/autorope/donkeycar
   
   cd donkeycar
   
   git checkout master
   
   # 注意这里与 PC 安装的区别，这里用的参数是 [pi]
   pip install -e .[pi]
   
   pip install tensorflow
   ```

   这里在安装 `tensorflow` 时如果提示 sha256 不匹配。可以先从网址上把 `tensorflow` 的 whl 文件下载到本地，再安装即可。

   安装完了测试一下

   ```
   python -c "import tensorflow"
   ```

   如果显示如下 warning msg 说明安装正确。

   ```
   /home/pi/env/lib/python3.5/importlib/_bootstrap.py:222: RuntimeWarning: compiletime version 3.4 of module 'tensorflow.python.framework.fast_tensor_util' does not match runtime version 3.5
     return f(*args, **kwds)
     
   /home/pi/env/lib/python3.5/importlib/_bootstrap.py:222: RuntimeWarning: builtins.type size changed, may indicate binary incompatibility. Expected 432, got 412
     return f(*args, **kwds)
   ```

8. 安装 opencv

   ```
   pip install opencv-python
   
   python -c "import cv2"
   ```

   如果没有报错，说明安装成功。

9. 创建 donkeycar 项目文件。在 PC 安装时，最后一步是创建了一个 donkeycar 项目文件，我们的所有工作都放在了这个文件夹中。类似的，在树莓派系统中，我们也要创建一个项目文件夹。

   ```
   donkey createcar --path ~/mycar
   ```

   我们的配置文件、小车收集的数据、控制文件等都在这里面。

10.  配置 I2C PCA9685 板子。

    ```
    sudo apt-get install i2c-tools
    
    sudo i2cdetect -y 1
    ```

    如果显示如下则配置成功

    ```
        0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:         -- -- -- -- -- -- -- -- -- -- -- -- --
    10:-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    20:-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    30:-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40:40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    50:-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    60:-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70:70 -- -- -- -- -- -- --
    ```

11. 校正小车。这一部分我简单做了一下，感觉参数与原本配置文件中的参数很接近，就没有修改。我猜因为小车是从官方渠道买的，参数都是比较标准的。如果想精确校正，可以参考[这里](http://docs.donkeycar.com/guide/calibrate/)。fine tuning 我没有做。

## 手动控制

这里的前提是 PC 与 树莓派系统之间可以通讯，我的做法是

>  PC 设置热点 hotspot，设置树莓派接入该热点网络，查看树莓派的 IP 地址，然后确保 PC 能够 Ping 通它，并且可以 ssh 远程登录它。

做完以上准备工作，就可以通过 PC 控制小车了。

步骤如下：

1. 在树莓派系统中运行如下命令

   ```
   cd ~/mycar
   
   python manage.py drive
   ```

2. 等上述命令完全启动，在 PC 浏览器中输入网址

   ```
   <donkeycar_ip>:8887
   ```

3. 如果顺利，应该可以出现操控的网页。这里可以通过鼠标点击、滑动控制小车的运动。注意，可以先把 max throttle 降到 20%，避免速度太快。也可以用 PC 键盘 "i j k l" 控制小车。

4. 也可以用手机控制小车。将手机接入同一热点网络，输入上述网址，操作方式与 PC 完全相同。



## 使用 ROS

### 安装方式1：源码编译

ROS 官网给出了[详细的安装过程](http://wiki.ros.org/ROSberryPi/Installing ROS Kinetic on the Raspberry Pi)。亲测，吃过晚饭6点左右开始安装，10点离开实验室的时候还没有编译结束，第二天再看已经安装完成，整个过程 >= 4 个小时，可能由于 raspberry Pi 3B+ 配置低的原因，编译过程中还出现了死机的情况。为了避免死机，建议在最后编译的时候用如下命令：

```bash
sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/kinetic -j1
```



### 安装方式2：镜像拷贝

网上有不少人分享了 Raspbian + ROS 和 Ubuntu + ROS 的系统镜像文件(img, iso 等)。可以直接做成系统启动盘，免去了自己编译的过程。



我用的第一种安装方式



### 基本操作：通过 /cmd_vel 控制小车

参考网址 https://github.com/tizianofiorenzani/ros_tutorials

从上边的网址下载两个 packge: donkey_car  和 ros-i2cpwmboard

放入本地 workspace 中，用  catkin_make 编译一下。

**注意1：**如果在编译时出现类似 `‘i2c_smbus_write_byte_data’ was not declared in this scope` 这样的错误，则需要安装如下模块

```bash
sudo apt-get install libi2c-dev
```

安装之后启动 donkey_car package 中的 `keyboard_demo.launch` 文件，查看一下 ROS graph 可以发现 donkey car 正等待接收 `/cmd_vel` 上的控制命令。至于如何产生这个命令就是 ROS 层面的操作了。



在远程操控小车时，可以在小车和 PC 上同时启用 ROS 并连接。从 PC 端产生 `/cmd_vel` 控制命令，驱动小车运动。



**注意2：**文件中的参数可能需要校正。在  donkey_car package 的 `low_level_control.py`文件中有如下初始化命令：

```python
class ServoConvert():
    def __init__(self, id=1, center_value=333, range=90, direction=1):
```

其中 `center_value=333` 可能需要修改。

可以看一下用初始参数运行时车轮是否朝向正前方，如果不行，通过向 `/cmd_vel`  发送不同转角控制命令，搜索正确的参数，默认情况下给定一个 `/cmd_vel` 数据，会有对应的转角参数显示出来，找到那个使车轮朝向正前方的参数，替换掉原本的 333 即可。



## 尝试 reinforcement learning 

参考网址 https://github.com/tawnkramer/learning-to-drive-in-5-minutes/blob/master/README_REAL_ROBOT.md

### raspberry pi 上的安装

- 为了避免跟之前的项目冲突，用了一个新的 sd 卡。

- 通过 noobs 方式安装好 Raspbian 系统，并更新

```bash
sudo apt-get update
sudo apt-get upgrade
```

- 设置 donkey car 上的接口

```bash
sudo raspi-config
### 启用如下接口
enable I2c
enable camera
exapand filesystem
change hostname
change default password for pi
Note: reboot after changing these settings
```

- 安装依赖

```bash
sudo apt-get update

sudo apt-get install build-essential python3 python3-dev python3-virtualenv python3-numpy python3-picamera python3-pandas python3-rpi.gpio i2c-tools avahi-utils joystick libopenjp2-7-dev libtiff5-dev gfortran libatlas-base-dev libopenblas-dev libhdf5-serial-dev git

sudo apt-get install libilmbase-dev libopenexr-dev libgstreamer1.0-dev libjasper-dev libwebp-dev libatlas-base-dev libavcodec-dev libavformat-dev libswscale-dev libqtgui4 libqt4-test
```

- 创建虚拟环境

```bash
python3 -m virtualenv -p python3 env --system-site-packages

echo "source env/bin/activate" >> ~/.bashrc

source ~/.bashrc
```

这中间可能会报错，与 pip 的版本有关，参考简书解决即可。

- 安装 tensorflow

```bash
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.10.0/tensorflow-1.10.0-cp35-none-linux_armv7l.whl

pip install tensorflow-1.10.0-cp35-none-linux_armv7l.whl
```

- 安装 donkey car application

```bash
git clone https://github.com/tawnkramer/donkey

pip install -e donkey[pi]
```

- 创建项目文件夹

```bash
donkey createcar --path ~/d2_rl --template manage_remote
cd ~/d2_rl
nano myconfig.py

## 添加内容
DONKEY_UNIQUE_NAME = "<your_name>"
MQTT_BROKER = "localhost"
```

- 安装 mosquitto mqtt broker service

```bash
sudu apt-get install -y mosquitto mosquitto-clients
```

- 最后 clone repo

```bash
git clone https://github.com/tawnkramer/learning-to-drive-in-5-minutes
```



### PC 上的安装

- 创建虚拟环境

```bash
virtualenv env -p python3
source env/bin/activate  # 后续操作都在此虚拟环境中进行
```

- 安装 tensorflow

```bash
pip install keras==2.2.2

pip install tensorflow-gpu==1.10.0 
#或
pip install tensorflow==1.10.0
```

- 安装 donkey car module

```bash
git clone https://github.com/tawnkramer/donkey
pip install -e donkey[pc]
```

- clone 主程序包并安装依赖

```bash
git clone https://github.com/tawnkramer/learning-to-drive-in-5-minutes

cd learning-to-drive-in-5-minutes

pip install -r requirements.txt
```

### 训练 VAE

- 应该在 `learning-to...` 文件夹目录下运行图像收集和训练任务。



## RL 训练框架解析

### 算法概述

目前在深度强化学习（DRL）方面主要有三类 model free 的算法

1. TRPO， PPO
2. DDPG 及其扩展 (D4PG, TD3 等)
3. Soft Q-Learning， Soft Actor-Critic



PPO 是目前比较主流的算法，适用于离散控制和连续控制，在 OpenAI Five 上取得了巨大成功。PPO 是一种 on-policy 的算法，所有的 on-policy 算法都面对一个困难：sample inefficiency ，需要大量的采样才能学习。这对于实际机器人的学习有时是不太现实的。



DDPG 是 DeepMind 开发的面向连续控制的 off-policy 算法，相对与 PPO 更 sample efficient。DDPG 训练的是一种确定性策略 deterministic policy，即每一个状态下只考虑最优的一个动作。DDPG 的扩展 D4PG 效果很好，但是没有开源，目前也没有人复现 DeepMind 的效果。



Soft Actor-Critic (SAC) 是基于 Maximum Entropy 的 off-policy 算法，与 DDPG 相比，SAC 采用随机策略 stochastic policy，这相比于确定性策略具有一定的优势。SAC 代码是开源的，在公开的 benchmark 上取得了非常好的效果，而且很适用于实际机器人的训练。



这就是 SAC 被选为 donkey car 训练算法的原因。



### SAC 详解

一般的深度强化学习 DRL 的目标就是学习到一个 policy 使累计 reward 的期望值最大，即
$$
\pi^* = \arg \max_{\pi} E_{(s_t, a_t)\sim\rho_{\pi}}[\Sigma_t R(s_t,a_t)]
$$
而基于 Maximum entropy 的深度强化学习，除了追求上边的目标，还要求 plicy 的每一次输出的 action  的熵最大
$$
\pi^* = \arg \max_{\pi} E_{(s_t, a_t)\sim\rho_{\pi}}[\Sigma_t \underbrace{R(s_t,a_t)}_{reward} + \alpha \underbrace{H(\pi(\cdot|s_t))}_{entropy}]
$$
这样做的目的是让 action 尽量随机化，而不是太集中于某一个 action 上。核心思想就是不遗漏任意一个有用的 action。而 DDPG 确定性的策略则是选择看起来是最好的，差一点的就忽略了，最后返回一条最有的策略路径，exploration 可能不充分。

优势1：更全面搜索，可能找到更好的策略

优势2：鲁棒性更强。由于已经对 state 和 action 空间充分探索了，几乎见过了所有的场景。因此，在遇到干扰时，面对干扰之后的 state，也能够更好的作出反应，因为已经探索过了。



### SAC 与 A3C 的区别

在训练 policy 时 A3C 也加上了 entropy 作为一个惩罚项，目的是让 policy 更随机。不过，A3C 中的整体训练目标依然是只考虑 reward，entropy 是实现 reward 最大化过程中的更好的 exploration 的工具，只存在于 policy 网络参数梯度更新的公式中。

所以 SAC 才是真正的最大熵 DRL 算法。



### RL 结构

1. **特征提取**。donkey car 摄像头默认采集图像维度为 120 × 160 × 3 （高 × 宽 × 颜色RGB） 。对原始图像做预处理，剪切掉画面下部的车体部分，得到维度为 80 * 160 * 3 的图像，然后通过变分自编码器 （Variational Auto-Encoder, VAE）压缩至长度为 32 的向量，即用 32 个元素的向量表征原本 80 * 160 * 3 个元素的图像。在降低维度的同时，尽量保留了原始图片的关键信息，从训练画面可以看出来，尽管维度降低了，通过解码器 decoder 几乎可以完美恢复原始图像。这说明经 VAE 编码压缩之后的输出以较小的空间保留了原始图片的关键特征。我们可以完全基于此降维的向量进行后续的 RL 训练。

   ![vae_1](./pic/vae_1.png)

   









1. 经过 VAE 压缩之后的包含 32 个元素的向量，加上最近的 10 个 action (包括 throttle 和 steering angle)，构成一个 52 个元素的向量，作为 policy network 的输入。
2. policy network 的结构非常简单，两个全连通层，分别包含 32 和 16 个神经元，采用 ReLU 激活函数。
3. policy network 的输出为 throttle 和 steering angle。为了操作的平滑，我们进一步对 throttle 和 steering angle 做了限制，使连续两个时刻的操作改变不会过大。
4. 设定一个 episode 为小车从开始自动驾驶到人工干预为止。在一个 episode 中自动驾驶状态每坚持一个 timestep 获得的 reward 就 +1，另外再加上一个相对速度 （当前速度/最大速度） 。这样设定 reward 是希望小车尽量长时间的保持高速自动驾驶状态。

