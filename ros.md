# `ROS different installations

1. **ros-kinetic-desktop-full**: ROS, generic robot libraries, rqt tools, rviz visualizer, simulators such as 2D stage and 3D Gazebo, the navigation stack, perception libraries using vision, lasers, or RGBD cameras.
2. **ros-kinetic-desktop**: ROS, generic robot libraries, rqt tools, rviz
3. **ros-kinetic-base**: ROS, generic robot libraries. 没有可视化工具，适合没有屏幕的情况。

以后如果想装什么 Pkg，一般有两种方法：

1. 通过系统安装，格式是

```
sudo apt install ros-kinetic-pkg_name
```

2. 如果希望这个 pkg 中调用本地自己开发的 Pkg，则可以从 github 把 source file 下载到 workspace 中，用 catkin_make 编译.

# python 编写 node 文件

本节主要目的是通过例子展示常见的 python node 的格式，总结基本框架和命令

## 例1： Publisher node

```python
#!/usr/bin/env python    
# shebang line, 指定运行该文件的程序。此处是说用从 PATH 环境中找到的第一个 python 程序执行该文件。
# 如果要更加具体地指定执行文件，可以用 #!<path>/python

import rospy   
# 调用 rospy module，这是所有 python node 需要加载的一个 module
from std_msgs.msg import String  
# 用到了 std_msgs/String 类型的 ROS 数据结构，所以要先调用相关的 module 中的 class，即 std_msgs.msgs.String 类。既然是类，所以在构造 msg 时，可以用 msg=String() 的方式初始化 fields
# 注意：用 python 编写的 node 程序本质上依然是 python 程序文件，里面用到的 import 等调用命令还是要去 PYTHONPATH 指定的路径寻找相应的 module。所以 ROS 的各种 module 必须在 PYTHONPATH 的路径中才行。其实在 source ROS 的各种 .bash 文件或者 install 各种 ROS package 的时候就已经将这些 module 放到 PYTHONPATH 中了。

def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10) 
    # 这句话放到后边更合适，以创建 node 为最早的命令
    # 创建一个 pub 小工具/对象，pub 对象有个 method: pub.publish() 可以被用来向 chatter 这个 topic 上发送类型为 String 的数据。这里就用到了 String class，所以需要在开头的时候调用响应的 module
    rospy.init_node('talker', anonymous=True)
    # 初始化 node，名字为 talker，有了名字，node 此时才开始与 master 以及其他 node 通信 
    # 如果 ROS graph 中可能存在多个重名的 node，例如多个 turtle node，当令 anonymous=True 时，重名 node 会自动在名字后边加上随机数以示区别。
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rate = rospy.Rate(10) 
    # 产生一个 rospy.Rate class 的对象 rate，它有一个 method rate.sleep()，可以控制 loop 的频率，比如每秒钟 loop 10次，如果一次 loop 小于 1/10 秒，则 sleep 等待，确保 1秒 完成10个 loop。当然这里的前提是，每个 loop 不能超过 1/10 秒，否则单靠 sleep 是无法达到期望的 loop 频率的 
    while not rospy.is_shutdown():
        # 监控着是否程序是否被关闭，例如 ctrl + c 等
        hello_str = "hello world %s" % rospy.get_time()
        # 这种普通的字符串就是 String 类型的。rospy.get_time() 可以返回当前时间
        rospy.loginfo(hello_str)
        # info 会被写到三个地方：1.屏幕，2.log 文件，3.rosout topic
        pub.publish(hello_str)
        rate.sleep()

if __name__ == '__main__':  # 当该程序作为主程序运行时，满足此条件
    try:
        talker()
    except rospy.ROSInterruptException:  
        pass
    # handle error, 看 talker() 程序是否能够正常运行，如果可以正常运行，则按照 talker() 中的命令运行；如果运行 talker() 程序报错（也有可能是rate.sleep() 在遇到 ctrl+c 时 throw 的一个exception），则执行 except block 中的命令，即 pass， 实际上是什么都不做，结束程序。此时会优雅的退出，很整洁，不会出现 error的信息。
    # 亲测，实际上，不用 try ... except ，就写成
    #   if __name__ == '__main__':
    #       talker()       
    #    
    # 也可以完好退出，没有任何报错
```

## 例2： Subscriber node

```python
#!/usr/bin/env python
# 第一句同样是 shebang line，指定运行该文件的程序
import rospy
from std_msgs.msg import String

def callback(data):
    # 尽管在 Subscriber 语句中没有给 callback 加参数，但是在执行中默认将接受到的 msg 作为参数传递过来
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    # 由于 String 类型的变量结构为 `string data`，所以 msg 的值是存储在 string 基本类型的 data field 中。 
def listener():
    rospy.init_node('listener', anonymous=True)
    # 初始化 node，名为 listener，如果有重名的就在后边加随机数，以示区分
    rospy.Subscriber("chatter", String, callback)
    # 监听 chatter topic 上的 std_msgs.msg.String 类型的数据，一旦收到数据，就调用 callback 函数，其中函数的第一个 argument 总是接受到的数据。

    rospy.spin()
    # 在 C++ 中，spin() 的作用是不断的循环，每次循环结束就令 callback 函数处理数据。
    # 但在 python 中 spin() simply keeps python from exiting until this node is stopped。 callback functions 不受 spin() 的影响，它们有自己的threads

if __name__ == '__main__':
    listener()
```

## 例3： 用类与对象的形式编写 Publisher node

```python
#!/usr/bin/env python

import rospy
from std_msgs.msg import String


class Talker():
    def __init__(self):
        self.pub = rospy.Publisher('chatter', String, queue_size=10)
        rate = rospy.Rate(10)
        msg = 'Msg from talker'
        while not rospy.is_shutdown():
            self.pub.publish(msg)
            rate.sleep()


if __name__ == '__main__':
    rospy.init_node('talker')
    try:
        Talker()
    except rospy.ROSInterruptException:
        pass
```

## 例4： 用类与对象的形式编写 interpreter node

```python
#!/usr/bin/env python

import rospy
from std_msgs.msg import String


class Interpreter():
    def __init__(self):
        rospy.Subscriber('chatter', String, self.callback)
        self.pub = rospy.Publisher('final/chatter', String, queue_size=10)
    # 把后边可能用到的 sub, pub 都弄好，callback 函数另外写

    def callback(self, data):
        msg = 'I have received : (%s), and I interpret it to (Hello World)' \
            % data.data
        self.pub.publish(msg)


if __name__ == '__main__':
    rospy.init_node('interpreter')
    try:
        Interpreter()
    except rospy.ROSInterruptException:
        pass
    rospy.spin()
    # 这里 spin() 唯一的作用是使程序不退出。只要程序不退出，callback 函数有自己的一套调用方法。
```



# msg 和 srv 文件

在有些复杂的程序中，可能需要自己构造特定的 msg 或者 srv 以适应特定的应用程序。msg 和 srv 无论是从文件内容上还是编译、使用的命令上都几乎是一样的。所以可以放在一起介绍。

一般在 package 中， msg 和 srv 是用单独的文件夹存放的，例如

![ros_package_architecture1](pic/ros_package_architecture1.png)

可见，在一个标准的 package 文件夹中，除了有 package.xml 和 CMakeLists.txt，还要有单独存放的 msg, srv, launch, src 文件夹。

当需要某个 Node 在接收到信息之后给出回复时，topic 无法做到这一点，只能用 service。

实际上， service 似乎就是为了返回应答而设计的

service 也是需要某种格式传递信息的，这就是与 msg 对应的 srv.

## 基本格式

本质上， msg 和 srv 都是文本文件，格式分别如下：
- 定义 my_msg.msg 文件如下
```
Header header
string child_frame_id
geometry_msgs/PoseWithCovariance pose
geometry_msgs/TwistWithCovariance twist
```
即用现有的基本 msg 类型构造一个比较特定的复合类型，格式： **Data_type  field**。
- 定义 my_srv.srv 文件如下
```
int64 A
int64 B
 ---
int64 Sum
```
srv 文件跟 msg 文件几乎一样，也是由简单类型构造而成，不过它分为两部分，上边为 request 部分，下边为 response 部分，即调用 srv 程序时，传递过去上半部分，srv 程序的返回值保存在下半部分。

## 构造 msg 的例子

1. 创建文件：在 ros package 中建个文件夹 msg，专门存放自定义的 msg 文件。
```
$ echo "int64 num" > msg/Num.msg
```
创建了一个简单的 msg 文件，它只有一个 int64 类型的名为 num 的 field

2. 编译文件： 
 - package.xml 中的修改：确保有如下 depend
 ```xml
 <build_depend>message_generation</build_depend>
 <exec_depend>message_runtime</exec_depend>
 ```
 - CMakeLists.txt 中的修改：
 ```
 find_package(catkin REQUIRED COMPONENTS
 message_generation
 )
 ```
 ```
 add_message_files(
 FILES
 Num.msg
 )
 ```
  ```
 generate_message(
 DEPENDENCIES
 std_msgs
 )
  ```
  ```
 catkin_package(
 CATKIN_DEPENDS message_runtime
 )
  ```
 最后一个修改似乎不是必须的！

 做完以上修改就可以用 catkin_make 编译文件。用 `rosmsg show package/Num` 就可以显示该 msg 的具体内容了。
 一般 msg 的名字为 package/msg.

## 构造 srv 的例子

1. 创建文件：在 package 中创建一个文件夹 srv， 专门存放 srv 文件。这里我们创建一个 AddTwoInts.srv 文件
 ```
 int64 a
 int64 b
 ---
 int64 sum
 ```
2. 编译文件：
 - package.xml 中的修改与前述 msg 完全相同
 - CMakeLists.txt 中的修改，也是几乎完全相同，唯一的区别是添加 srv 文件
 ```系统
 add_service_files(
 FILES
 AddTwoInts.srv
 )
 ```
 修改完之后，就可以 catkin_make 编译文件，然后就可以用 `rossrv show package/srv` 查看 srv 的内容了。

# ROS 的安全性问题

同名 node 被替换： 如果 ROS network 中已经有一个 talker node 了，然后又启动了一个名为 talker 的 node, 那么前一个 node 将被 shutdown, 新的 node register with the master. 
这样设置的好处是，当有 node 损坏时，可以很容易替换掉。
​                   坏处是，正常工作的 node 也很容易被恶意 node 替换。

# package 路径与命令

## package 路径设置

package 的路径可以用如下命令查看： 
```
echo $ROS_PACKAGE_PATH 
```

将新的 Package path 加入上述路径一般有两种方法：

1. 通过 `source .../setup.bash` 实现，一般我们建立新的 workspace 之后都是用这种方法添加路径的。
2.  直接在 .bashrc 中手动添加：

```
export ROS_PACKAGE_PATH=~/new_path:$ROS_PACKAGE_PATH
```

一般运行 package 中的程序时会用 `rosrun pkg node`格式，那么这个 Node 可执行文件到底存放在什么地方？

如果用 roscd pkg，可以进入某个 pkg 的目录，但是往往找不到该 pkg 目录下的 node 可执行文件。一般来说，pkg 目录是 `/opt/ros/kinetic/share`，而 node 可执行文件放在 `/opt/ros/kinetic/lib/` 文件夹下。

## package 相关命令

package 的绝对路径已经记录在了系统中，只要给定了 package 名字，系统会自动查找对应的路径，所以不管是 roscd, rosls, rosed, 提供的方便就是不需要输入 package 的路径信息，只给出 package 名字就可以了。

- **roscd pkg_name**: 进入某一 package
- **rosls pkg_name**: 查看某一 package 下的文件
- **rosed pkg_name**: 调用默认编辑器编辑 package 下的文件
  - 如果该 package 下面有多个重名的文件，则会提示选择某一个
- **rospack list**: 查看当前已安装的 package
- **rospack find pkg_name**查看某一 package 的路径
- **rospack depends1 pkg_name**: 查看某一 package 的直接依赖关系
- **rospack depends pkg_name**: 查看某一 package 的直接和所有间接依赖关系：

甚至连 rosrun package node 也是找到相应的 package，然后运行其中的可执行文件。例如写好了可执行文件 talker.py，可以用命令 `rosrun <package_name> talker.py` 运行 node，也可以直接找到 talker.py 所在位置，然后`./talker.py` 运行。



# catkin 编译系统

catkin 是 ROS 的编译系统，取代了之前的 rosbuild 编译系统。

## 创建 workspace 存放待编译的 package

1. 建个空文件夹，例如 `mkdir -p ~/catkin_ws/src`
其中， `-p` 是指明如果要创建的文件夹的上一层（parent）文件夹不存在，则一起创建。
2. 设置 workspace 的源文件存放位置。例如进入刚才创建的 src 文件夹，然后用命令 `catkin_init_workspace` 就可以产生相关的 CMakeLists.txt文件
3. 编译。退到 src 的上一层文件夹，运行 `catkin_make`，则会一次性编译 src 文件夹中所有的源文件，同时生成 devel 和 build 两个文件夹。
 - build 文件夹存放编译过程中的缓存文件和中间过渡文件. is where cmake and make are invoked 
 - devel 文件夹存放编译之后的目标文件和可执行文件。contains any generated files and targets, plus setup.(ba)sh files so that you can use it like it is installed. 只要source了那些.sh文件，就等同于将他们安装在了系统中环境中，可以直接调用了。等测试没有问题了，才会考虑 install 和 share with other developers.

 实际上，`catkin_make`并没有什么神奇的地方，它只是把一系列更底层的编译命令整合在了一起，一个`catkin_make`命令相当于：

    ```
    mkdir build  
    cd build    
    cmake ../src -DCATKIN_DEVEL_PREFIX=../devel    
    make
     ```
  **You can pass any arguments to catkin_make that you would normally pass to make and cmake.  **

 在src文件夹的上一层运行下面的 rosdep 命令可以自动安装src中package的依赖关系，其中的 rosdep 就是在最初安装 ROS 时随 ROS 一起安装的 rosdep，安装好了 ROS 我们还额外初始化了 rosdep: `sudo rosdep init &  rosdep update`

 `rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO`

 或 

 `rosdep install -y --from-paths src --ignore-src -r`

题外话：通过 `echo $ROS_DISTRO` 可以查看当前 ROS 版本。这里应该用 echo，如果只用 `$ROS_DISTRO`，相当于在 terminal 里面输入了 kinetic 作为运行命令，而不是像 echo 那样返回某个 object 的值，这样应该会报错。

4. 设置 workspace 的环境变量，用命令 
`echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc`

## 设置 package

ROS package 中至少包含两个文件： package.xml 和 CMakeLists.txt。

- package.xml: 包含了 package 描述信息和 dependencies, 主要是 `<build_depend>` 和 `<run_depend>`, 前者是在 package 编译时需要用到的 packages，后者是在运行时需要用到的 packages，例如：

  ![ros_package_xml1](pic/ros_package_xml1.png)

   

package 在 workspace 中存放的结构一般是这样的：
```
catkin_ws/src/CMakeLists.txt
              package1/package.xml, CMakeLists.txt
              package2/package.xml, CMakeLists.txt
              . . .
```

package 之间不能嵌套，每个 package 都是相对独立的包。

- 在 src 文件夹下创建 package 的命令：
` catkin_create_pkg pkg_name depend1, depend2, ...`
其中 depend 为有依赖关系的 package，这样创建之后，在新 package 的CMakeLists.txt 和 package.xml中会体现出这种依赖关系。

- 创建完 package 之后，就可以使用`rospack`相关的命令进行查找、进入等操作了（可能会有延时，可以先用 `rospack find <***>` 查找一下，或者用 `rospack list` 显示一下）。但此时 package 中的源文件还没有编译，在 src 上层 catkin_make 编译一下， 就可以执行了。

## 修改 package.xml 与 CMakeList.txt 文件

编写完了源文件，如果 depend package 都已经设置好了的话，就不需要修改 package.xml 了，只需要修改 CMakeList.txt 如下两个地方：
```xml
    add_executable(exe_file   cpp_file)
    
    target_link_libraries(exe_file  ${catkin_LIBRARIES})
```
## 只编译特定的package

- `catkin_make --pkg  pkg_name`
- 但有时此命令并不能只编译一个pkg，最有效的办法是使用 whitelist：
  - `catkin_make -DCATKIN_WHITELIST_PACKAGES="foo;bar"`，这样就只编译 foo 和 bar 这两个 pkg。 
- But be aware that you will have to run catkin_make -DCATKIN_WHITELIST_PACKAGES="" to be able to build all your packages again.



# 强制关闭死掉的 rosnode

- `rosnode kill` 是关闭一个运行中的 node
- `rosnode cleanup` 是将那些 rosmaster 无法联系到的 rosnode 的注册信息清除掉。注意，这里并不一定是将 rosnode 关闭，只是把它们的信息清理了。

由于 rosnode 本身也是一个可执行程序，可以用 kill 普通程序的方式强制关闭 rosnode。

例如有些时候，明明已经关闭了 roscore 或者 gazebo 之类的程序，但是在启动新程序的时候还是提示有未关闭的 roscore 等，可以用 killall 命令强制关闭：

```
killall -9 roscore
killall -9 rosmaster
```



# launch 文件

launch 文件可以一次性启动多个 node。很多package 都带有 *.launch 文件。

## launch 文件的运行命令

  - 通过 package 路径启动，命令如下：
` roslaunch package_name launch_file [可选args]`
    可以看出，这里也是用了 package 的便利，它的绝对路径已经告知系统了，所以可以通过在 package 下查找同名 launch文件。这与 rosrun, roscd 等类似。
  - 通过绝对、相对路径启动，命令如下：
` roslaunch path_of_launchfile [可选args]`

上述 args 比较常用的有

- --screen: Force all node output to screen. Useful for node debugging

- launch 文件中可以接受 args，则用 arg:=value 方式赋值，例如 

  - `roslaunch pkg  *.launch model:=urdf/myfile.urdf`

  - `roslaunch pkg  *.launch model:='$(find urdf_pkg)/urdf/myfile.urdf' `

    `$(find pkg)` 形式的 arg 需要加单引号

在运行 launch 文件时，如果 roscore 已经启动，则使用已有的 roscore；若没有，则启动 roscore。

## launch 文件的内容格式

roslaunch 文件不需要编译，写好之后可以直接用上面提到的方式运行。

为了 launch 文件比较容易读，可以在开头加
```xml
<?xml version="1.0"?>
```
这样 editor 可能会对 launch 文件关键词高亮显示

### `<launch>`, `<node>`

这两个是 launch 文件最基本的 element

```xml
<launch>
	<node pkg="package_name" type="executable_file" name="node_name1"/>
    <node pkg="another_package" type="another_executable" name="another_node"/>
    ...
</launch>
```

注意： roslaunch 不能保证 node 的启动顺序，因此 launch 文件中所有的 code 都应该对启动顺序有鲁棒性。

`<node>` 中可以设置更多参数，如下：

```xml
<launch>
	<node
    	pkg=""
        type=""
        name=""  
        respawn="true" 
        required="true"
        launch-prefix="xterm -e"
        output="screen"
        ns="some_namespace"
    />
</launch>
```
上述命令中，

- respawn： 若该节点 dies，是否自动重新启动
- required: 若该节点 dies，是否关闭其他所有节点
- launch-prefix: 是否在另外的窗口执行，这种情况出现在需要通过窗口进行机器人移动控制的时候
- output：默认情况下，launch 启动 node 的信息是存入下面的log文件的
  `/.ros/log/run_id/node_name-number-stdout.log`
  可以通过此处参数设置，令信息显示在屏幕上
- ns : 将 node 归入不同的 namespace。为了方便这类操作，在 node 源文件中定义 node name 和 topic name 时采用 relative name， 即不加slash符号 `/`.

### `<arg>`

Allows you to create more re-usable and configurable launch file by specifying values that are passed via the command-line, via an `<include>`, and so on.

Note: An arg declaration is specific to a single launch file. You must explicitly pass arg values to an included file. 如果 Include file 中有 arg，并且没有默认值，则必须要设置。所以经常会看到这样的情景

```xml
<include file="file_name.launch">
    <arg name="arg1" value="value1" />
    <arg name="arg2" value="value2" />
    ...
</include>
```

关于 `<arg>`三种常用方法:

- `<arg name="foo">`:  declare the existence of "foo". The value of "foo" must be passed in either by command-line or by `<include>` passing
- `<arg name="foo" default="1">`: declare "foo" and its value can be overridden by command-line or `<include>` passing
- `<arg name="foo" value="1">`: declare "foo" with constant value which cannot be overridden.

从 command-line 赋值

```
roslaunch package_name file_name.launch  arg1:=value1  arg2:=value2
```

### `<include>`

import another launch file into the current scope of the this launch. 与当前的 launch file 同一级的，相当于全都释放出来了，放在当前 launch 文件中。

```xml
<include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="world_name" value="$(find mybot_gazebo)/worlds/mybot.world"/> 
    <!-- 本来只能引用 /usr/share/gazebo-8 中的 world/*.world 文件，现在可以通过 find 命令额外设定文件。
    这个 find 命令也仅在 roslaunch 文件下有用，实际上是给 rospack 添加了个外壳-->
    <arg name="paused" value="$(arg paused)"/>
    <arg name="use_sim_time" value="$(arg use_sim_time)"/>
    <arg name="gui" value="$(arg gui)"/>
    <arg name="headless" value="$(arg headless)"/>
    <arg name="debug" value="$(arg debug)"/>
  </include>
```

### `<remap>` 

目前看来主要是用在改变 topic 。

remap 影响的是它的 scope (launch, node, group) s中后续的变量。

```xml
<node pkg="some" type="some" name="some">
	<remap from="origin" to="new" />
</node>
```

上述参数是将 node 中所有涉及到 topic "origin" 的地方全部替换为 "new"。这样，只需编写一个文件，其中 topic 可以先随便定义，占个位置，然后在不同的场合中，用 remap 修改 topic，从而可以 pub 或者 sub 特定的 topic。

如果在 command line 中通过 rosrun 启动的 node 也想修改 topic，则用

```bash
rosrun pkg node original_1:=new_1  original_2:=new_2
```

### `<param>` 与 `<rosparam>`

#### parameter server

提到 param 首先要知道 parameter server, 它实际上是一个共享的 dict，running inside of the ROS Master。

Nodes use this server to store and retrieve parameters at runtime. 

param server 设计时就不是针对高速存取的，因此一般只适合存放 static data，such as configuration parameters.

由于是 globally viewable, 我们可以很容易的查看其中的 param 并且修改。

param 的命名与 topic 和 node 相同，都是有 namespace hierarchy 结构的，这可以防止不同来源的名字冲突。它的存取也是体现了结构性，例如下边的 params

```
/camera/left/name: leftcamera
/camera/left/exposure: 1
/camera/right/name: rightcamera
/camera/right/exposure: 1.1
```

- 如果读取 param `/camare/left/name` 可以得到值为 `leftcamera`

- 如果读取 param `/camera/left`可以得到 dict 为

  ```
  {name: leftcamera, exposure: 1}
  ```

- 如果读取 param `/camera`，则得到 dict 为

  ```
  {left: {name: leftcamera, exposure: 1}, 
   right: {name: rightcamera, exposure: 1.1}} 
  ```

#### tag `<param>`

与 arg 不同，param 是共享的，并且它的取值不仅限于 value，还可以是文件，甚至是一行命令。

- 格式

  ```xml
  <param name="param_name" type="type1" value="val"/>
  	#type可以自动判断
  <param name="param_name" textfile="$(find pkg)/path/file"/>
      # 读取 file 存成 string
  <param name="param_name" command="$(find pkg)/exe '$(find pkg)/arg.txt'"/>
  例如：
  <param name="param" type="yaml" command="cat '$(find pkg)/*.yaml'"/>
  	# command 的结果 存成在 Param 中
  ```

param 可以是在 global scope 中，那么它的 name 就是 原本的 name.也可以在某个更小的 scope 中，比如 node，那么它在全局的名字就是 node/param 形式.

例如在 global scope 中定义如下 param

```xm
<param name="publish_frequency" type="double" value="10.0" />
```

再在 node scope 中定义如下 param

```xml
 <node name="node1" pkg="pkg1" type="exe1">
    <param name="param1" value="False"/>
  </node>
```

如果用 `rosparam list`列出server 中的 param，则有

```xml
/publish_frequency
/node1/param1   # 自动加上了 namespace 前缀
```

注意：虽然 param 名字前加了 namespace，但是依然可以 global viewable，还是共享的

#### tag `<rosparam>`

`<param>`只能对单个 param 操作，而且只有三种：value, textfile, command 形式，返回的是单个 param 的内容。

`<rosparam>`则可以批量操作，还有其他对参数设置的命令，如 dump, delete 等

- load : 从 YAML 文件中加载一批 parma，格式如下：

  ```xml
  <rosparam command="load" file="$(find rosparam)/example.yaml" />
  ```

- delete: 删除某个 param

  ```xml
  <rosparam command="delete" param="my_param" />
  ```

- 类似 `<param>` 的赋值操作（不用 YAML）

  ```xml
  <rosparam param="my_param">[1,2,3,4]</rosparam>
  ```

  ```xml
  <rosparam>
  a: 1
  b: 2
  </rosparam>
  ```

The `<rosparam>` tag can be put inside of a `<node>` tag, in which case the parameter is treated like a private name，即所有的 param 名字前边都加上 node namespace.

### ` <group>`

如果要对多个 node 进行同样的设置，比如都在同一个特定的 namespace，remap 相同的topic, 等，可以用 `<group>` tag。在 group 中可以 use any tag you would normally use

```xml
<group ns="wg2">
    <remap from="chatter" to="talker"/> # 对后续的所有 node 都有效
    <node ... />
    <node ... >
    	<remap from="chatter" to="talker11"/> #node中可以重新设置 remap
    </node>
</group>
```

### 变量替换 `$(find *)` `$(arg *)`

常用的变量替换形式有两个： `$(find pkg_name)` 和 `$(arg argument_name)`

- `$(find pkg)`： 例如`$(find rospy)/manifest.xml`. 强烈推荐这种基于 package 的路径设置

- `$(arg arg_name)`： evaluates to the value specified by an` <arg>` tag. 

  For example:

  ```xml
  <arg name="gui" default="true" />  
  # 先设置个默认值，如果没有额外的赋值，就用这个默认值了
  
  <param name="use_gui" value="$(arg gui)"/>
  ```

  Another example:

  ```xml
  
  <node name="add_two_ints_client" pkg="beginner_tutorials" type="add_two_ints_client" args="$(arg a) $(arg b)" />
  ```

  Then we can pass the values of a and b as parameters. The launch file can be called as follows:

  ```
  roslaunch beginner_tutorials launch_file.launch a:=1 b:=5
  ```

# rosbag 相关

## 记录 rosbag

- `rosbag record -a` : 记录所有 topic，不指定 bag 名字，默认是以日期和时间为名字的

- `rosbag record -O/-o filename topic1 topic2 ...`: 只记录指定的 topic1 和 topic2，保存到文件 filename.bag 中， capital O 后边跟的是 bag 的名字， 不需要加后缀 .bag，如果是 lowercase -o，则只是在日期名字前边加了前缀。

- 可以在 launch 文件中启动 rosbag record

  ```xml
  <node pkg="rosbag" type="record" name="bag_record" args="/topic1 /topic2"/>  # 默认存放目录是 /.ros
  ```


## 播放 rosbag

- `rosbag play <bagfile>` : 正常播放
- `rosbag paly -r 2 <bagfile>` : 以 2 倍速度播放
- `rosbag play -l <bagfile>`: 循环播放
- `rosbag play <bagfile> --topic /topitc1` ：播放指定 topic

播放期间，空格键可以暂停。

## python 分析 rosbag 中的数据

完整实例：

```python
import rospy
import rosbag
from geometry_msgs.msg import AccelStamped 
import matplotlib.pyplot as plt # 为了最后画图

bag = rosbag.Bag('compare_4_topics.bag')  # 读取 bag

coms_accel_value = [] # 生成空 list, 方便存放数据
coms_accel_t = []


for topic, msg, t in bag.read_messages(): # bag.read_messages() 得到一个 generator，每个元素包含(topic, msg, t)
    if topic == '/coms/accel':
        coms_accel_value.append(msg.accel.linear.x)  # 用 .append() 向 list 中填充数据
        coms_accel_t.append(msg.header.stamp.to_sec())

plt.plot(coms_accel_t, coms_accel_value)
```

## 通过 .csv 文件分析 bag 中的 topic

上述 python 方法功能比较强大。

但是对于比较简单的分析，我们可以直接把 topic 保存为 csv 文件，通过 excel 类的软件打开，直接画图对比曲线更方便。

从 rosbag 中提取某一 topic 为 .csv 文件： 
`rostopic echo -b file.bag -p /topic >  output_file`
其中， `-b` 后跟 rosbag 的名字， `-p` 指明以 plotting friendly format 的形式输出

# log 相关 

## log 类型

由轻到重依次为：
- DEBUG ： Information that you never need to see if the system is working properly. 
```
"Received a message on topic X from caller Y"
"Sent 20 bytes on socket 9".
```
- INFO： Small amounts of information that may be useful to a user. 
```
"Node initialized"
"Advertised on topic X with message type Y"
"New subscriber to topic X: Y"
```

- WARN： Information that the user may find alarming, and may affect the output of the application, but is part of the expected working of the system. 
```
"Could not load configuration file from <path>. Using defaults."
```

- ERROR： Something serious (but recoverable) has gone wrong. 
```
"Haven't received an update on topic X for 10 seconds. Stopping robot until X continues broadcasting."
"Received unexpected NaN value in transform X. Skipping..."
```

- FATAL： Something unrecoverable has happened. 
```
"Motors have caught fire!"
```

## 用法及记录存放/显示地点

所有 log 的用法是相同的
```python
rospy.logdebug(msg, args)
rospy.logwarn(msg, args)
rospy.loginfo(msg, args)
rospy.logerr(msg, args)
rospy.logfatal(msg, args)
```
例如：
```python
rospy.logerr("%s returned the invalid value %f", other_name, other_value) # python 中
logfatal("current_vel: %.3f, expect_vel: %.3f", vel1, vel2) # julia 中
```


不同程度的 log 记录存放/显示地点不同
```
stdout: loginfo
your Node's log file: all
the /rosout Topic: loginfo, logwarn, logerr, logfatal
```

默认情况下， debug 信息不会出现在 /rosout topic 中，如果希望出现，则在 node 初始化时设置一下：
```python
rospy.init_node('my_node', log_level=rospy.DEBUG)
```

如果用 roslaunch 启动，则默认不会显示在 stdout 即屏幕上，需要额外设置参数
```xml
<node name="bar1" pkg="foo_pkg" type="bar" output="screen" />
```
其中 `output="log|screen"(optional)`。 The default is 'log'.

也可以用 rqt_console 实时显示 Log 信息。用 rqt_console 可以记录和提取 rosout 中的 log 信息.

另外，原本在 rosrun 中会在屏幕显示的信息，如果用 roslaunch 启动，则不会显示。可以通过 rqt_console 显示。

# rqt 工具

有些 rqt 工具可以通过输入工具名字启动，如 rqt_console, rqt_graph，但是有些不行。

不过，所有的 rqt 工具都可以通过 `rosrun <rqt_tool_name> <rqt_tool_name> ` 的形式启动

- **rqt_console**:  实时显示 Log 信息

- **rqt_top**: 类似 ubuntu 的 top ( table of processes ) 命令，但这里不是监视 process，而是 node 的资源使用情况

  ![rqt_top1](pic/rqt_top1.png)

- **rqt_topic**: 监视 topic 的情况，包括 频率，带宽，内容等

  ![rqt_topic1](pic/rqt_topic1.png)

- **roswtf**: 分析 package 潜在的问题。要先 roscd 进入某个 package.

- **rqt_plot**: 画出 scalar. 可以在 gui 中选择，也可以在命令行里面指定，例如

  ```bash
  rosrun rqt_plot rqt_plot /topic_name  # 一个 topic
  rosrun rqt_plot rqt_plot /topic_name/1:2:3  # 多个topic
  ```

- **rqt_bag**: 分析 rosbag 中的数据，可以Plot

- **rqt_gui**: 上边所有的 rqt 都可以从 gui 里面选择显示

- **rqt_tf_tree**: 原本通过 tf 的方式查看坐标转换关系比较麻烦，需要先生成一个 pdf 文件，再打开查看。

  ```bash
  rosrun rqt_tf_tree  rqt_tf_tree
  ```

  可以直接产生 tf tree，非常方便！

# rospy time 系统

## 两种时间类型

- ROS 有两个内置的类型： time and duration, 在 rospy 中就是 rospy.Time and rospy.Duration classes. 
- A Time is a specific moment (e.g. "today at 5pm") whereas a Duration is a period of time (e.g. "5 hours"). Durations can be negative.
- Times and durations have identical representations:
```
int32 secs
int32 nsecs
```
- Instead of using Python's time.time module, you should use rospy's time routines for accessing the current time, which will work seamlessly with simulated Clock time as well as wall-clock time.

## 获取当前时间

- `rospy.Time.now()` and `rospy.get_rostime()` are equivalent.
```python
now = rospy.get_rostime()
rospy.loginfo("Current time %i %i", now.secs, now.nsecs)
```
- `rospy.get_time()` Get the current time in float seconds.
```python
seconds = rospy.get_time()
```

## 构建指定时间

- `rospy.Time(secs=0, nsecs=0)` Create a new Time instance. secs and nsecs are optional and default to zero.
```python
epoch = rospy.Time() # secs=nsecs=0
t = rospy.Time(10) # t.secs=10
t = rospy.Time(12345, 6789)
```
- `rospy.Time.from_sec(float_secs)` Create a new Time instance from a float seconds value 
```python
t = rospy.Time.from_sec(123456.789)
```

## 时间格式转换

 ROS time 格式(包括 Time 和 Duration 类型)与普通时间格式之间可以转化
- 从普通格式到 ROS time 格式 
```python
t = rospy.Time.from_sec(60.1)  
d = rospy.Duration.from_sec(60.1) # a minute and change
```
- 从 ROS time 格式到普通格式
```python
seconds = t.to_sec() # ROS 到 floating point
nanoseconds = t.to_nsec()
seconds = d.to_sec() # ROS 到 floating point
nanoseconds = d.to_nsec()
```

## sleep 和 Rate

- `rospy.sleep(duration)` duration can either be a rospy.Duration or seconds (float).  

```python
# sleep for 10 seconds
rospy.sleep(10.)

# sleep for duration
d = rospy.Duration(10, 0)
rospy.sleep(d)
```
- `rospy.Rate(hz)` 一般程序中都是用这个。rospy provides a rospy.Rate convenience class which makes a best effort at maintaining a particular rate for a loop.  For example:
```python
r = rospy.Rate(10) # 10hz
while not rospy.is_shutdown():
    pub.publish("hello")
    r.sleep()
```
In the above example, the Rate instance will attempt to keep the loop at 10hz by accounting for the time used by any operations during the loop. 

- Both sleep() and Rate.sleep() can throw a rospy.ROSInterruptException if the sleep is interrupted by shutdown. 这就是为什么经常见到程序中要 try ... except 这个 Exception



# TF

## TF broadcaster

```python
#!/usr/bin/env python 

import rospy
import tf
from geometry_msgs.msg import PoseStamped

def callback(data):
    br = tf.TransformBroadcaster()
    br.sendTransform(
        (0.1, 0.0, 0.2),
        tf.transformations.quaternion_from_euler(0.0, 0.0, 0.0),
        rospy.Time.now(),
        'base_laser', # child
        'base_link' # parent   # from parent to child
    )

if __name__ == '__main__':
    rospy.init_node('talker')
    rospy.Subscriber('input_topic', PoseStamped, callback)
    rospy.spin()
```

上述例子中，每次接收到 input_topic 上的 msg，都发布一个 tf，从 base_link 到 base_laser 的转换。

这个例子比较简单，发布的 tf 是个固定值，与接收到的 msg 没有任何关系，只是当作触发器。

更加一般的情况是， tf 与 msg 相关，比如 msg 是关于定位的信息，那么 tf 就可以包含物体与 map 的转换。

**static transform** 

实际上对于固定的 transform，有更简单的发布方式

- command line:

`rosrun tf static_transform_publisher x y z yaw pitch roll frame_id child_frame_id period_in_ms`  注意，这里角度是 yaw pitch roll

或四元组形式的 orientation

`rosrun tf static_transform_publisher x y z qx qy qz qw frame_id child_frame_id  period_in_ms`

- launch file

```
<launch>
<node pkg="tf" type="static_transform_publisher" name="link1_broadcaster" args="1 0 0 0 0 0 1 parent child 100" />  # 以 100ms 的频率发布
</launch>
```

通过 `rosrun tf tf_echo <parent> <child>` 可以实时查看 transform

## TF listener

假设已经有人在发布一个 static tf, 下面的程序就是查找这个 tf 并以 topic 的形式发布出来

```python
#!/usr/bin/env python 

import rospy
import tf
from geometry_msgs.msg import PoseStamped

if __name__ == '__main__':
    rospy.init_node('tf_listener')
    lt = tf.TransformListener()
    pub = rospy.Publisher('output_topic', PoseStamped, queue_size=1)

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            lt.waitForTransform('base_link', 'base_laser', rospy.Time(0),rospy.Duration(4.0)) # 先等 tf 发布
            (trans, quat) = lt.lookupTransform('base_link', 'base_laser', rospy.Time(0))
            # rospy.Time(0) return the latest available
        except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        msg = PoseStamped()
        msg.pose.position.x = trans[0]
        msg.pose.position.y = trans[1]
        msg.pose.position.z = trans[2]
        msg.pose.orientation.x = quat[0]
        msg.pose.orientation.y = quat[1]
        msg.pose.orientation.z = quat[2]
        msg.pose.orientation.w = quat[3]
        pub.publish(msg)

        rate.sleep()    
```

上述程序只是单纯的把 tf 中的信息取出来， tf 更强大的功能是实现不同 frame 之间的自动转换，有三种方式，分别实现 point, pose, orientation 的自动转化：

-  `transformPoint(target_frame, point_msg) -> point_msg`

Transforms a geometry_msgs/PointStamped message, i.e. point_msg, to the frame target_frame, returns the resulting PointStamped.

- `transformPose(target_frame, pose_msg) -> pose_msg`

Transforms a geometry_msgs/PoseStamped message, i.e. point_msg, to the frame target_frame, returns the resulting PoseStamped.

-  `transformQuaternion(target_frame, quat_msg) -> quat_msg`

Transforms a geometry_msgs/QuaternionStamped message, i.e. quat_msg, to the frame target_frame, returns the resulting QuaternionStamped.

下边的程序就实现了 base_laser 中的 pose 向 base_link 中 pose 的转移

```python
#!/usr/bin/env python 

import rospy
import tf
from geometry_msgs.msg import PoseStamped

if __name__ == '__main__':
    rospy.init_node('tf_listener')
    lt = tf.TransformListener()
    pub = rospy.Publisher('output_topic', PoseStamped, queue_size=1)

    laser_pose = PoseStamped()  # 设定 base_laser 中的点 pose
    laser_pose.header.frame_id = 'base_laser'
    laser_pose.pose.position.x = 1
    laser_pose.pose.position.y = 0
    laser_pose.pose.orientation.x = 0
    laser_pose.pose.orientation.y = 0
    laser_pose.pose.orientation.z = 0
    laser_pose.pose.orientation.w = 1

    base_pose = PoseStamped()

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            lt.waitForTransform('base_link', 'base_laser', rospy.Time(), rospy.Duration(4.0))
            base_pose = lt.transformPose('base_link', laser_pose)  # 将 base_laser 中的点转化到 base_link 坐标系中
        except(tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        pub.publish(base_pose)

        rate.sleep()    
```



# Euler angle 与 quaternion 之间的转换

## Quaternion 定义

$q = [q_w ~~q_x~~q_y~~q_z] $

一个恰当的转角 quaternion 必须满足归一化要求，即

$|q|^2 =q_w^2 + q_x^2 + q_y^2 + q_z^2 = 1 $

## Euler angle 定义

$\phi$: 绕 X 轴

$\theta$: 绕 Y 轴

$\psi$: 绕 Z 轴

三轴满足：右手伸开，四指朝向 X 轴，向 Y 轴方向弯曲，拇指的方向就是 Z 轴方向

## Euler 转换成 Quaternion 

$q$ ![euler2quat1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/euler2quat1.png)

有个小知识点：如果车辆只有 $\psi$ 数值，其他都为 0, 那么对应的 quaternion 中，只有 q.z, q.w 有数值，q.x, q.y 都是 0. 可以很容易从上边的转换式子看出来。实际上，这种情况下，
$$
q = \left[
\begin{matrix}
	q_w \\
	q_x \\
	q_y \\
	q_z 
\end{matrix}
	\right] =
\left[
    \begin{matrix}
 		\cos(\psi/2) \\
    	0 \\
    	0 \\
    	\sin(\psi/2)
    \end{matrix}
\right]
$$

## Quaternion 转换成 Euler

$$
\left[
    \begin{matrix}
    	\phi \\
    	\theta \\
    	\psi
    \end{matrix}     
\right] 
=
\left[
    \begin{matrix}
    	\text{atan2}(2(q_wq_x + q_yq_z), 1-2(q_x^2 + q_y^2)) \\
    	\text{asin}(2(q_wq_y - q_xq_z)) \\
    	\text{atan2}(2(q_wq_z + q_xq_y), 1-2(q_y^2 + q_z^2))
    \end{matrix}
\right]
$$

## 用 tf 自动转换

tf.transformations module 中包含两个函数

```
quaternion_from_euler(i, j, k) # 从 Euler 角转化成 quaternion
```

```
euler_from_quaternion([x,y,z,w]) # 从 quaternion 转化成 euler
```

可以在 command line 中执行上述 python 命令，例如第二个转换

`python -c "import tf; print(tf.transformations.euler_from_quaternion([0,0,0.38,0.92]))"`





# ROS 与硬件的互动

## 安装 perception stack

这个 Meta package 包含了所有 perception 相关的 packages，包括与 OpenCV, PCL 相关的 packages. 所以，不论是处理 image 还是 pointcloud，只需要安装这个 perception statck 就够了。

`sudo apt install ros-kinetic-perception`

## camera 

### 安装 driver package 

1. 先确定 ubuntu 是否已经支持了 webcam

   ```
   ls /dev | grep video
   ```

   如果返回 video0，则表示 a cam is available for use

2. 安装 ROS webcam driver `usb_cam`package，可以支持 ROS 调用这个可用的 camera，同时附带了如下的 launch 文件来开启 camera.

```xml
<?xml version="1.0"?>
<launch>
    <node name="usb_cam" pkg="usb_cam" type="usb_cam_node" 
    output="screen">
        <param name="video_device" value="/dev/video0"/>
        <param name="image_width" value="640"/>
        <param name="image_height" value="480" />
        <param name="pixel_format" value="yuyv"/>
        <param name="camera_frame_id" value="usb_cam"/>
        <param name="io_method" value="mmap"/>
    </node> # pub topic /usb_cam/image_raw

    <node name="image_view" pkg="image_view" type="image_view"
    respawn="false" output="screen">
        <remap from="image" to="/usb_cam/image_raw"/>
        <param name="autosize" value="true"/>
    </node>    # 接受 image 并显示
</launch>
```

3. 运行`roslaunch usb_cam usb_cam-test.launch `之后就会马上显示出来 camera 的影像，ros graph 如下：

![usb_cam1](pic/usb_cam1.png)

### camera calibration

- 为什么要进行 calibration

我们现在常用的摄像头是 pinhole camera，它的一个问题是 distortion。不过比较好的消息是这个 distortion 是固定的，并且可以被矫正，这就是 camera calibration.

## Kinetic

需要 OpenNI (Natural Interaction) driver library. 

```bash
sudo apt install ros-kinetic-openni-launch
```

也附带了 launch 文件.

```bash
roslaunch openni_launch openni.launch 
```

由于没有相应的硬件设备，暂时没有进一步的操作。

可参考《mastering ROS for robotics programming (2015)》

## Velodyne LIDAR

安装 velodyne driver

```bash
sudo apt install ros-kinetic-velodyne
```



