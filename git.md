## show git branch in terminal prompt

只需要改变 .bashrc 中 PS1 这个变量。

原本变量大概是如下形式：

```bash
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
```

修改成如下内容：

```bash
git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]$(git_branch):\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
```

其中的关键

1. 加入了 git_branch 这个变量
2. 将提示信息 ` $(git_branch) ` 加入原 PS1 中. 如果希望 git 提示信息有颜色，可以替换成如下内容

```bash
git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]\[\033[00;32m\]$(git_branch)\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
```



## git 整体框架

The relations between remote and local, branches are shown as follows:

![relation](pic/relation_git1.png)

#### Two ways to create a repository
github 帐号 ： 330469501@qq.com   密码： 你懂得

1. Using command `git init` will turn the current folder into a repository. That means, once you use this command, a hidden folder named `.git` will be created, which contains important information of this newly created repository. 
   - `git remote add origin git@***.git`: connect remote repository `git@***.git` to a local name `origin`. This name --`origin` can be changed to another, but traditionally it is called `origin`. 

  (If the local git repo is built before the remote repo, we need this method to connect the two repos. If we firstly have the remote repo, we can simply use `git clone ***` to set up the local repo. The latter would be much easier.)

2. Using command `git clone ***/gitfolder.git` will copy a remote repository `gitfolder.git` into local disk, and the folder `gitfolder` is the repository which contains `.git`. 

## git 系统的 3 个区域

1. 本地 working directory, 在这里完成文档编辑工作
2. 本地待更新区域 staging area。如果希望对 working directory 中某些文档的修改进行确认，就可以通过 `git add filenames` 将他们加入到 staging area，用 `git commit -m "comments" ` 进行本地最终更新确认
3. 服务器上 remote repository 远程仓库。里面存放了之前保存的版本，可以拷贝到本地来，也可通过 `git push origin master` 从本地提交到远程仓库中。

### 单 branch 基本命令

![git_command](pic/git.jpeg)

- `git status`: 查看当前文件状态，都有哪些改变. It will tell you which files are modified but not staged, which files are staged but not committed. Anyway, it tell the overall information in your workspace.

- `git add filename`: add a new file and a new modification into the staging area. 如果当前整个文件夹 and all of its subfolder 都需要监控，则用 `git add .`这样所有的增加、删减、修改都可以全面跟踪。但是它上层的文件修改就不能跟踪了。用 `git add -A` 则全部包括。

   **git 2× version**
   ![gitadd](pic/gitadd.jpg)

- `git commit -m "<message>"`：什么时候应该commit 一下？ 太长不行，因为会很难理解两个版本之间到底有什么具体的差别，太短不行，一行一commit会产生太多的commit 历史，也比较难查找有用的信息。最好是以logical change为单元进行 commit，比如这次修改是修改了color，另一次修改是改了一些typo。这样修改的目的比较清晰，容易查找。

   这跟 jupyter notebook中多长的内容归为一个 cell 类似，不能每行都是一个cell，也不能太长的内容合并在一个 cell 里面，也是应该以 logical function 为单位存放。

- `git clone repo_address`: copy the remote repository to local disk. In this way, we can only see the master branch. 

   `git clone -b branch_name  repo_address` : copy the specified branch instead of the default master branch

- 一个 repo 可能包含其他子 repo，叫做 submodule。上述简单的 git clone repo 命令只能拷贝 submodule 的空文件夹，无法完整 clone 所有内容。如果要同时 clone repo 以及它包含的其他 submodule repo，可用命令
  - `git clone --recurse-submodules repo_address`
  - 注意：在 git 2.13 版本之前是用 --recurse，现在已经替换成 --recurse-submodules
  - 如果一个 repo 以及存在了，再 clone 它的 submodules，可以用命令
    - `git submodule update --init --recursive`

##### `git diff`

- `git diff`: 默认是比较 working directory 与 staging area
- `git diff --staged`：比较staging area 与 HEAD commit
- `git diff commitID_old commitID_new`: the difference between two commitIDs

(In fact, there is a built-in command in Ubuntu called **diff** which can compare the difference of two files, and the form of result is quite similar. It works like: **diff -u file1  file2** . Here -u optiion is used to output result easy to read.)
(**git diff** is used to compare the difference between commits of a single file, not different files.)
(最相似的用法就是 **git diff commitID_old commitID_new**)

- `git diff filename`: show the difference between the file modified in the **workspace** and the file in the **staging area**.
- `git diff --staged/cached commitID`: the difference between **staging area** and **commitID**
- `git diff commitID filename`: the difference between **filename** and **commitID**
- `git show commitID`: 显示该 commitID 与其父节点的区别



##### `git log`

show the history of changes, including commitID, author, time, message. If we don't care who and when has made the change, we can add an argument `--pretty=oneline`

git log 显示的commit都是那些可以回溯过去的，如下图所示
![commit_appear](pic/new_commit_appear.png)

merge 之后如果用 git log 显示的话，各个分支的 commit 是按时间顺序混杂在一起显示的，所以最好用一个图显示他们之间的父子关系。

git log 还可以加一些参数，可以使显示效果更简介.

例如默认情况下，显示结果为：

```bash
commit 81e8c2f836794dd5c80e588deda9e5072a797709
Author: qipeng <qpliu@ntu.edu.sg>
Date:   Sat Mar 16 21:46:18 2019 +0800

    add vehicle dynamics.md

commit 594b2731a068adeeefc04d20b25a55b6a0058c0e
Author: qipeng <qpliu@ntu.edu.sg>
Date:   Sat Mar 16 17:13:09 2019 +0800

    add veins, sumo, omnet

commit 669d434247fc94745f1f6a25f71be49f0766fb34
Author: qipeng <qpliu@ntu.edu.sg>
Date:   Mon Mar 4 09:55:19 2019 +0800

    add SUMO

commit 4ad634a9354ade256ec0713f57c64c4c72ce543a
Author: qipeng <qpliu@ntu.edu.sg>
Date:   Tue Feb 26 10:39:00 2019 +0800

```

- git log --pretty=oneline  : 每个 log 只显示 commit ID 和 message，没有作者和时间信息

  ```bash
  81e8c2f836794dd5c80e588deda9e5072a797709 add vehicle dynamics.md
  594b2731a068adeeefc04d20b25a55b6a0058c0e add veins, sumo, omnet
  669d434247fc94745f1f6a25f71be49f0766fb34 add SUMO
  4ad634a9354ade256ec0713f57c64c4c72ce543a add shell programming
  bfe1e17fd2beaf67421cc63ba4c84acf8d4fdfdb add dynamic vehicle model
  be3630b6fedd6213cba2b37416b41205a1746f22 finish bicycle kinetic model
  9e3a00310fc3bc502cb1e18c9716dadde2e1369d regular update
  f338cdfa1442dba43f3e7f48304462b06d2d6862 change name of autonomousVehicle to cav
  f33c9b18105bce1171a11883414d9a2bc561ee48 add more to gps knowledge
  648070ae58ac09ebcb392ec9f8c4b3a79dddbc97 add git contribution and gps knowledge
  a4e1c523bf4bdfe6c174e81709ae7ac1f4ef15e3 add more to gps
  ```

- git log --abbrev-commit ：commit ID 只保留 7 位，其他都一样

  ```bash
  commit 81e8c2f
  Author: qipeng <qpliu@ntu.edu.sg>
  Date:   Sat Mar 16 21:46:18 2019 +0800
  
      add vehicle dynamics.md
  
  commit 594b273
  Author: qipeng <qpliu@ntu.edu.sg>
  Date:   Sat Mar 16 17:13:09 2019 +0800
  
      add veins, sumo, omnet
  
  commit 669d434
  Author: qipeng <qpliu@ntu.edu.sg>
  Date:   Mon Mar 4 09:55:19 2019 +0800
  
      add SUMO
  ```

- git log --pretty=oneline --abbrev-commit ：把两个参数的效果叠加，显示更精炼

  ```bash
  81e8c2f add vehicle dynamics.md
  594b273 add veins, sumo, omnet
  669d434 add SUMO
  4ad634a add shell programming
  bfe1e17 add dynamic vehicle model
  be3630b finish bicycle kinetic model
  9e3a003 regular update
  f338cdf change name of autonomousVehicle to cav
  f33c9b1 add more to gps knowledge
  ```

  

### 反悔命令


- `git reset --hard commitID`: return to the version of commitID. In fact, you can go to any version as long as you provide the commitID. The working directory, the stage, and the HEAD all the same as that in commitID. That also means all the changes in working directory will be discarded. 

- `git reflog`: show all the command you have made. From here you can also find all the commitIDs.

- `git checkout commitID`:  去某个commitID 指向的 commit. 可以前进，也可以后退，只要你知道那个commitID即可！

  同时git 会提示“You are in 'detached HEAD' state. You can look around, make experimental changes and commit them, and you can discard any commits you make in this state without impacting any branches by performing another checkout. 

  也就是说，你可以随便处理这个时候的 commit，试验一下之前的文件等等。

  注意：此时是temperorily 回到之前的commit，如果你要修改之后保存，会提醒你建个新branch吧。

  If you want to create a new branch to retain commits you create, you may do so (now or later) by using -b with the checkout command again. Example:

  如果你想保留当前的commit，可以从当前位置搞一个新的 branch，

  `git checkout -b <new-branch-name>` : 建立一个新的 branch，并且移过去

  HEAD is now at b0678b1... ”

- `git checkout -- filename`: discard changes in working directory, i.e., make **working directory** same to **stage**. Without `--` it will switch to another branch

 **注意：这里的working directory只是说那些被git管理的部分，如果有额外untracked files，这个还原命令并不会删除这些untracked files。如果要删除 untracked files，用 git clean 命令 **

- `git clean -n`: 并非真的删除，而是显示那些文件会被删掉，`would remove ...`

- `git clean -f`: Remove untracked files. If the Git configuration variable clean.requireForce is not set to false, git clean will refuse to run unless given -f.
- `git clean -f -d`: 删除文件夹 directory
- `git clean -f -f -d` : 如果文件夹是 repository， 必须再加一个 `-f` 才能删除
- `git reset HEAD filename`: discard staged changes, i.e., make **stage** same to **HEAD commit**
- `git push [-u] origin master`: submit the local master branch to the remote master branch of `origin`. For the first time, `-u` should be used. 
- `git pull origin master`: 用法与 push 类似，origin和 master 都可以省略
- `git remote [-v]`: show the connected remote repo. argument `-v` means verbose, will give more information, like fetch address and push address. Sometimes we clone other people's repo, so we can only fetch, but not able to push back, and we can not see the push address
- `git rm file_name`: 普通的 rm 命令之后还需要跟一个  git add 才行，git rm 相当于两个命令一起作用。另外，git rm --cached file_name 只是 untrack file，依然保留文件， git rm -f 则是强制删除。

## branch

### 基本命令

- `git branch`: show all branches

- `git branch -vv`: (means verbose) show branches, HEAD ID, and remote tracking branch，例如

  ```
  acronis@acronis-G501VW:~/autoware-ntu-fork$ git branch -vv
    develop       4439fcf  [origin/develop] Merge branch 'fix/cv_' into develop
  * feature/ntu   1aceb31  [origin/feature/ntu] ready for deliver
  ```

- `git branch dev`: create a new branch called dev

- `git checkout dev`: switch to dev branch.  

- `git checkout -b dev`: create a new branch dev and switch to it. That is the combination of the above two commands.

- 上述三个命令可以是完全本地化的，不考虑任何 remote branch 的存在。如果要 connect remote and local branch, we can further use 

  `git checkout -b local_dev origin/dev`  

  上述命令是新建了本地 branch 名为 local_dev 去对接远端的 dev branch，并且 checkout 过去 local_dev

- 如果不想特意设定本地名字，可以接受本地名字与远端名字相同，则用更简单的命令

  `git checkout dev`

  git 会自动查找远端与 dev 名字相同的 branch，并在本地建立 dev branch 去对接远端的 dev

- `git branch -d dev`: delete the dev branch

- `git branch -u <upstream>`: tracking remote branch

  Example:

```
acronis@acronis-G501VW:~/autoware-ntu-fork/ros$  git branch -vv
  develop     4439fcf [origin/develop] Merge branch 'fix/cv_' into develop
* feature/ntu 1aceb31 ready for deliver

acronis@acronis-G501VW:~/autoware-ntu-fork/ros$ git branch -u origin/feature/ntu 
Branch feature/ntu set up to track remote branch feature/ntu from origin.

acronis@acronis-G501VW:~/autoware-ntu-fork/ros$ git branch -vv
  develop     4439fcf [origin/develop] Merge branch 'fix/cv_' into develop
* feature/ntu 1aceb31 [origin/feature/ntu] ready for deliver
```

- `git branch --unset-upstream`: untracking remote branch

  Example:

```
acronis@acronis-G501VW:~/autoware-ntu-fork$ git branch -vv
  develop     4439fcf [origin/develop] Merge branch 'fix/cv_' into develop
* feature/ntu 1aceb31 [origin/feature/ntu] ready for deliver

acronis@acronis-G501VW:~/autoware-ntu-fork$ git branch --unset-upstream
acronis@acronis-G501VW:~/autoware-ntu-fork$ git branch -vv
  develop     4439fcf [origin/develop] Merge branch 'fix/cv_' into develop
* feature/ntu 1aceb31 ready for deliver
```

### branch 切换中的问题

如果当前 branch 的工作还没做完，但是要切换到其他 branch 做一些其他工作。怎么办？

- 如果当前 work space 中未 commit 的文件是 **untracked**，那么切换 branch 没有问题，untracked 文件会始终在 work space 中，不在 git 系统的看管之下，切换 git 的 branch 不影响那些 untracked 文件。
- 如果当前 work space 中 git add 了**新的文件** （另一个 branch 中没有的），也没有问题，切换之后这些新文件依然在 stage area 中
- 如果修改了两个 branch 共同 tracking 的文件，那么需要用到 git stash 命令，先隐藏一下
  - `git stash`: temporarily stash( means hide) the current working space and make it clean. So you can switch to other branchs and do other work in a clean workspace. 
  - `git stash list` : show all the stashed work space
  - `git stash pop` : pop out the stashed workspace.

#### fork

如果没有fork,想copy别人的repo首先要clone到本地，然后在上传到自己的repo中
![nofork](pic/nofork.png)

这里有两个问题：
1. 下载再上传比较麻烦
2. 两个repo之间的关联被打破，无法进行潜在的同步更新，无法跟踪源repo被复制的次数

![fork](pic/fork.png)

#### branch merge related commands (without conflicts)

In general, it’s very common that if you make a branch, either an experimental branch or to work on a new feature, you want to periodically merge master/dev into that branch. This is because master/dev usually contains the official version of the code, and it’s common to want experimental changes to include all of the changes to master/dev.

master和dev中的修改（例如其他人推送的修改）需要经常合并到其他分支中去，保证实时更新。当其他分支开发完之后再合并到master和dev中，推送到远端分享。



如果两个分支没有同时修改同一个文件，则不会出现conflict。哪怕是修改了同一文件，但是不涉及到相同区域（例如相同行），都不会产生conflict。
![merging_file](pic/new_merge_file.png)

如果都修改了同一个文件的同一区域，则无法自动 merge，需要两方面协商，最终的修改版本，手动处理完 conflict的部分。

不论有没有conflict，最终合并完了，都要要求加一个 commit message, to explain why this is necessary.


- `git merge dev`: merge dev branch to current branch. 

  (In the absent of diversion, if dev is in ahead, then current goes to ahead. If dev is in behind, nothing happens, because current branch is already up-to-date, it contains all information of dev branch. )
  
  从原始文件分支出来的两个文件merge的原则：揣测程序员意图
  - 原始文件在两个分支中都没有变动的，合并之后也要保留不变。
  - 原始文件有，后来在某一分支删掉的，说明删掉是主动的、需要的，合并之后也删掉
  - 原始文件没有，后来在某一分支加上的，说明加上的是主动的、需要的，合并之后也加上
![merging_files](pic/new_merge_file.png)



- `git merge --no-ff -m "merge with no-ff" dev`: merge with not fast forward mode. During merging, a new commit is created.

   If there is a conflict in merge, the branches will be shown even the dev branch is deleted. So we do not need to worry about the fast forward mode.
   
   If no conflict and merge in fast forward mode, then the graph could not show the whole process if the dev branch is deleted. Therefore, we use `--no-ff` mode in the merging. The history of branching will be retained. At least we know there used to be a merging.
   

![no-ff](pic/no-ff.png)

    Remember that git merge always merges all the specified branches into the currently checked out branch, creating a new commit for that branch.

  






#### branch merge related commands (with conflicts)

1. Suppose that we have `master` and `feature1` two branches, and we have changed the file `readme.txt` on both branches.

2. If we are now in `master` branch, and want to merge `feature1`, git will tell us there are conflicts, like
   
   > Auto-merging readme.txt
   
   > CONFLICT (content): Merge conflict in readme.txt
   
   > Automatic merge failed; fix conflicts and then commit the result.

3. Using `git status` we will know which file has conflicts. Note that we currently could not  switch to other branches but fix the conflicts. If you want to abort the merging, just use `git merge --abort`

4. Modify the file and fix the conflicts. 修改完之后效果如下：
![both_modified](pic/both_modified.png)


   Now we do not need to `git merge` again. After fix the conflicts, we only need to add the modification and commit it. 
![still_merging](pic/still_merging.png)

5. To see the graph of branchs, we can use  `git log --graph --pretty=oneline --abbrev-commit`

#### push conflict

When multiple people work on one branch, conflicts frequentlly occur when push from local to remote, because other people has changed the branch.

For example, we use ` git push origin dev ` to push local dev to remote dev branch.

We may get the following error notification. 

>To git@github.com:michaelliao/learngit.git

>! [rejected]        dev -> dev (non-fast-forward)

>error: failed to push some refs to 'git@github.com:michaelliao/learngit.git'

>hint: Updates were rejected because the tip of your current branch is behind

>hint: its remote counterpart. Merge the remote changes (e.g. 'git pull')

>hint: before pushing again.

>hint: See the 'Note about fast-forwards' in 'git push --help' for details.


**To solve this problem, we can follow the notification, use `git pull` to merge the remote branch into local, fix possible conflicts, commit, and push to remote again.** 

More precisely,

1. `git pull`: if it says that `There is no tracking information for the current branch.
Please specify which branch you want to merge with`, just follow the message to establish the connection between remote and local branchs. Then pull again.

2. fix conflicts. This is the same as fix conflicts in merging process.

3. commit the fixed conflicts and push again.



#### 更新本地版本

当在其他地方更新过 remote 文件之后，想在另外一个机子上继续工作，可以用如下命令：

- `>> git fetch`:  该命令仅仅是将 remote 上的信息取下来，i.e., update the remote repo infomation，将remote上的commit链接到本地对应的origin/master的后边，实际上是产生了branch 分支的效果，并不对本地文件做任何改动，可以通过 `git status` 与本地对比

- `>> git pull`: 该命令将 remote 上的信息取下来，并与本地文件合并，相当于 同时使用 `git fetch` 和 `git merge`。在不清楚本地与 remote 的差别时， 不要用 git pull，否则会把本地工作抹掉或者引起 merge conflicts。

![fetch_merge](pic/fetch_merge.png)

#### gitk 工具

![gitk](pic/gitk.png)

##### Top pane: commit information
- Local branch names are in a green background
- remote branch names are in a mixed orange/green background
- the currently checked out branch name is in bold
- a yellow dot marks the current HEAD

Click on any line segment and the diff pane changes to show details for that line segment -- all parents and children are shown.


Diff between two commits
1. left click on the "new" commit (先选中的是新的)
2. navigate using the scrollbar to some other commit
3. right-click and choose "Diff this -> selected"


##### Main menu:
reload: if you're doing stuff on the command line in another screen, and want those changes reflected in gitk.

##### Bottom left pane:
At any time, the bottom left pane shows a "diff", usually of the current commit shown in the top pane. 

The diff pane also shows all the branches to which this commit belongs. Click any of them to go to the head of that branch.


##### Bottom right pane:
The bottom right pane (by default) shows a list of files in this "diff".



## 打标签 tag

tag 本质上是指向某个  commit 的指针，所以 tag 的创建和删除都是对于指针的操作，瞬间完成。

既然有了 commit ，为什么还用 tag ？

主要是因为 commit 标记是一串数字，不好查找，而 tag 可以用比较有意义的名字。

- 查看已有标签

  ```bash
  git tag
  ```

- 打标签

  ```bash
  git tag <name> 
  # 例如
  git tag v1.0
  ```

默认标签是打在最新的 commit 上的。

如果相对某个历史 commit 打标签，可以找到该 commit ID，然后打上去

先查找某个 commit 

```bash
git log --pretty=oneline --abbrev-commit  
```

显示如下：

```bash
81e8c2f add vehicle dynamics.md
594b273 add veins, sumo, omnet
669d434 add SUMO
4ad634a add shell programming
bfe1e17 add dynamic vehicle model
be3630b finish bicycle kinetic model
9e3a003 regular update
```

例如要对 add shell programming 加标签，命令如下：

```bash
git tag v0.1 4ad634a
```

此时在用 git tag，会看到 tag v0.1 出现了

默认情况下， tag 按字母排列。

如果要查看某个 tag 读应的 commit 信息，用命令

```bash
git show <tag_name>   # 注意，这里不是 git tag show 
# 例如
git show v0.1
```

显示如下信息：

```bash
commit 4ad634a9354ade256ec0713f57c64c4c72ce543a
Author: qipeng <qpliu@ntu.edu.sg>
Date:   Tue Feb 26 10:39:00 2019 +0800

    add shell programming

diff --git a/.ipynb_checkpoints/python-checkpoint.ipynb b/.ipynb_checkpoints/python-checkpoint.ipynb
index 3f65415..9949405 100644
--- a/.ipynb_checkpoints/python-checkpoint.ipynb
+++ b/.ipynb_checkpoints/python-checkpoint.ipynb
@@ -1547,66 +1547,6 @@
    "cell_type": "markdown",
    "metadata": {},
    ...
```

可以给 tag 加 message，如下

```bash
git tag -a v0.2 -m "tag at add veins" 594b273
```

其中 -a 后边跟标签名， -m 后边跟说明文字

这样在 git show v0.2 的时候，显示多一些文字

```bash
tag v0.2
Tagger: qipengliu <qpliu@ntu.edu.sg>
Date:   Sat Mar 16 22:40:13 2019 +0800

tag at add veins

commit 594b2731a068adeeefc04d20b25a55b6a0058c0e
Author: qipeng <qpliu@ntu.edu.sg>
Date:   Sat Mar 16 17:13:09 2019 +0800
...
```

打完 tag 在 gitk 中显示如下：

![tag_gitk](pic/tag_gitk.png)

gitk 里面只能看到本地的 tag，要看远端的，还是要去 github 或者  bitbucket .

- 如果 tag 打错了，也可以删除

```bash
git tag -d <tag_name>  # 会提示删除了哪个 tag
```

tag 默认是打在本地，不会被推送到远端。

- 如果要推送 某个 tag，用如下命令：

```bash
git push origin v0.1
```

会提示

```bash
Total 0 (delta 0), reused 0 (delta 0)
To git@bitbucket.org:qpliu/learning.git
 * [new tag]         v0.1 -> v0.1
```

- 如果要一次性推送全部未推送的 tag 到远程

  ```bash
  git push origin --tags
  ```

如果已经推送到远端的，想要删除

首先删掉本地的 tag

```bash
git tag -d v0.1
```

然后删除远端的

```bash
git push origin :refs/tags/v0.1
```



## 常见问题

1. 当 repo 比较大时，下载的时候可能出现错误，一般是由于网速引起的，但有时也可以通过以下的设置或许可以解决问题。

   ```
   acronis@acronis-G501VW:~$ git clone git@bitbucket.org:acronis16/autoware-ntu-fork.git
   Cloning into 'autoware-ntu-fork'...
   remote: Counting objects: 38381, done.
   remote: Compressing objects: 100% (14087/14087), done.
   packet_write_wait: Connection to 104.192.143.1 port 22: Broken pipe   
   fatal: The remote end hung up unexpectedly
   fatal: early EOF
   fatal: index-pack failed
   ```

    此时做如下设置即可：

    `git config --global http.postBuffer 1048576000`



## 如何在 github 上做贡献？

别人的 repo 称为 upstream

自己 fork 过来的 repo 称为 origin

1.  fork 别人的 repo 到自己的 repo。

2.  git clone 下来

3. 作适当修改。为了保证修改的是最新的 upstream 中的 repo，一般把 upstream repo 添加为 upstream 之一，

   ```
   git remote add upstream [Upstream git URL]
   ```

    可以通过以下命令查看现有的 remote

   ```bash
   git remote -v
   ```

   如果现在要修改的不是最新的，还需要从 upstream  fetch 下来，检查一下

   ```bash
   git fetch upstream
   ```

4. 修改之后， push 到自己 fork 的 repo

5. 创建一个 new pull requrest ，等待别人的回应

有时，为了稳妥起见，先弄一个 Issue ，讨论一下问题，跟别人确认好之后，分配给自己一个任务，修改 code.