## GPS 信号构成

GPS 信号由三部分构成

1. 载波
2. gps code
3. navigation message

### 载波 carrier

所有的 gps 卫星都发射两种载波 (carrier)：

- L1 carrier: 1575.42 MHz, wavelength 19 cm
- L2 carrier: 1227.60 MHz, wavelength 24.4 cm

用两种载波的主要原因是为了纠正一种被称为电离层延迟的误差 (ionospheric delay)。后边会有介绍。

尽管每个卫星的载波相同，但是每个卫星的调制方式不同，这样可以减少彼此之间的干扰。

### GPS code

在 carrier 之上，还有 gps code，也分两种

- C/A code: coarse acquisition
- P code: precision

每一种 code 都是一组二值序列。这两种 code 也被统称为 PRN  (Pseudo-Random Noise) code 伪随机噪声，因为它们看起来就是完全随机的信号，但实际上是严格按照数学算法计算出来的。

目前 C/A code 只与 L1 carrier 调制在一起，P code 则分别与 L1，L2 carrier 调制。

#### C/A code

C/A code 长 1023，每 1 ms 重复一次。可以想象成是这个 1023 bits 长的 code 循环经过窗口，头尾相接。

这样算下来，每个 bit 要在 1 ms 时间内改变 1023 次，因此每个 bit 每秒改变 $1.023 * 10^6 $ 次，也称 chipping rate 为 1.023 Mbps. 再换算一下，也相当于每个 bit 约保持 $10^{-6}$ 秒，在这期间信号传播 300 m。

每个卫星有唯一的 C/A code，因此可以让 receiver 确认是哪个卫星发来的信号。

#### P code

P code 则是一个非常长的 code，每 266 天重复一次。同时，它的变化速度是 C/A code 的 10 倍，即 chipping rate 为 10.23 Mbps。通过计算可以得到， P code 的长度为 $ 2.35*10^{14}$ . 

这长 266 天的序列分成 38 个片段，每周一个片段。32 个 片段分给 32 个卫星，其余的备用。

也就是说，每个卫星上存放一周长度的 P code，在周六/周日临界点开始重复。

卫星常常用 P code 来指代，比如 ID 为 PRN 20 的卫星就是被分配了第 20 个周片段的的卫星。

在 1994年之前 P code 是可以直接获取的，Jan 31 1994 开始被加密，并且添加进了未知的 W code，最后形成 Y code。这一加密也被成为 antispoffing (AS). 

### Navigation message

导航信息被同时调制到 L1 和 L2 载波上。

长度为 25 frame, 每个 frame 1500 bits，即总共 37500 bits。

该信息 chipping rate 比较慢 50 bps. 所以传完一次完整的 msg 需要 750  秒, 即 12.5 分钟。

每个卫星包含的信息都不同。主要有所有卫星位置随时间的函数关系 （即卫星的位置，这在GPS 定位中很重要），本卫星的健康状况，时间矫正，星历之类等。同时也包含对其他卫星的一些估算，比如其他卫星的当前位置和健康状况。

### 或许已经采用的新技术

以上这些信号结构使用于 2003年之前的 GPS 信号。

随着新的卫星发生，似乎在信号方面有了改进。

- 在 L2 carrier 上也调制了 C/A code，这样在 L1 和 L2 上都有了 C/A code 信号，更好的纠正电离层偏差
- 在 L1 和 L2 上又增加了军用 code -- M code
- 增加一个 carrier L5 （1176.45 MHz），带宽最小 20 MHz，chipping rate 10.23 MHz。上边调制的 C/A code 更长。

## GPS 接收器介绍

根据可接受信号分类

- single-frequency code receiver：只检测 L1 频率信号上的 C/A code
- single-frequency carrier-smoothed code receiver: 也是只检测 C/A code，但是内置了高精度的 carrier 频率，可以协助提高 C/A code 的检测精度
- single-frequency code and carrier receiver: 同时检测 C/A code, L1 carrier 和 navigation message
- dual-frequency receiver：同时接收 L1 和 L2 频率信号。在 antispoofing 加密之前，这种 receiver 可以检测所有的 GPS 信号，包括 L1, L2 carrier, C/A code, P code, 和 navigation message. 但 antispoofing 之后，只能通过技术手段 (Z-tracking 或 cross-correlation) 复原 L2 carrier，Y code 不可测。

另外，还有不同的 tracking channel 数目，每个 channel 可以用来持续跟踪某一 GPS 卫星。

现在大多数 GPS 接收器有 9 - 12 个 channel.

## GPS 测距原理

### 伪距  pseudorange 方法

假设 GPS 卫星上的时间与地面的 receiver 是完全同步的。

GPS 发送 PRN code 时，地面的 receiver 也同步产生 PRN code。当 receiver 接受到 code 时，通过对比两方面信号的差别，可以确定信号传播的时间，乘以光速就是传播的距离。

![gps1](pic/gps1.png)

由于上述测量会有一些干扰，比如时间非严格同步，通过上述方法得到的距离并不太精确。由于这是通过 pseudocode 测量的距离，称为 pseudorange 伪距。

### 载波相位 carrier-phase 方法

通过 L1  carrier 也可以测量距离。从过计算从卫星到 receiver 的波数，乘上波长，就可计算出之间的距离，如下图所示

![gps2](pic/gps2.jpg)

而且由于 carrier 频率 1575.42 MHz 远高于 code 频率 1.023 MHz，波长短，可以更精确计算波数和处于波的哪个位置，因此由 carrier 计算的距离理论上来说精确度更高。

这种方法的问题在于，carrier 都是同样的正弦波，无法精确检测从 gps 卫星到 receiver 到底有多少个波长。

似乎有技术可以精确检测一部分，另外一部分无法精确检测，称为 initial cycle ambiguity. 似乎是初始阶段波长无法确定，后边就可以了。而且在后续的测量中，无法确定的那一部分保持不变，可以看作一个特殊的误差。这样就有可能通过 GPS 信号组合的方式消去。见后边的介绍。



## GPS 定位原理

前边介绍了测距方法，为了确定地球上某一点的位置，还需要合适的利用这些测得的距离。

定位方式分为两种

- point positioning: 只用一个 receiver，也称为 autonomous positioning，自主定位
- relative positioning: 同时用两个 receiver. 这实际上就是通过另一个固定的位置精确已知的 base 来提高定位

### point positioning

就是采用最基本的跟踪至少四个卫星，求解四个位置数：三维坐标+时间误差。如果超过四个卫星，则用最小二乘方法。

![gps_positioning1](pic/gps_positioning1.png)

### relative positioning

也就是 differential positioning, 差分定位。采用 base 的精确坐标去修正 receiver 的坐标。因为 receiver 和 base  如果距离比较近，定位中附带的误差也是相近的，所以可以通过 base 的误差去修正 receiver 的定位

![gps_relative](pic/gps_relative.png)

### RTK GPS

Real-time kinematic GPS,  属于 relative positioning 的一种，目前比较流行。

![gps_rtk](pic/gps_rtk.png)

采用 carrier 进行测距，精确性高一些。

在 RTK GPS 中，base receiver 静态且位置已知。base 配备一个无线发射器。rover receiver 配备无线接收器。**base 将自身的 gps 检测和坐标发送给 rover**。实时性稍微差一些，因为发送的东西比较多，打包需要些时间。

### DGPS: Real-time differential GPS

采用 code 进行测距，精确性不如 RTK，一般用在 米 级的定位需求中。

![dgps](pic/gps_dgps.png)

只要两个 receiver 相距在 几百公里以内，他们包含的误差是差不多的。

base 计算误差 psedusorange errors (也成称为 DGPS corretions)。 以标准格式 RTCM (Radio Technical Commission for Maritime Service) 发送给 rover。

rover 应用接收到的 corrections来修正自己检测到的 pseudorange，进而修正自身定位。



可见，无论 RTK 还是 DGPS，都需要从 base 到 rover 的 radio communication.

- RTK 传输速率 9.6 Kbps
- DGPS 传输速率 200 Kbps

## GPS 相关难题

### Cycle slips

就是 carrier phase 检测中出现的不连续或跳变，通常由信号丢失造成。

cycle slips 需要被识别且矫正，否则会对定位造成巨大偏差。

实际中比较流行的解决办法是 examing the so-called triple difference observable。这是变量是通过某种特定方式组合 GPS 观测信号得到的。

### GPS 信号组合方式消除误差

GPS 定位中的误差大概有下边 3 个来源

1. 卫星侧的误差，例如星历误差（卫星位置与时间的关系）
2. 传播干扰，主要是电离层误差 

3. receiver 侧的误差，主要是时间非同步误差。卫星用铯原子钟，receiver 只是石英钟。

通过 GPS 组合的方式可以消除很多的误差。

- 消除误差 1 和 2： 距离比较近的两个 receiver 获得的信号包含大致相同的卫星侧和传播误差，也就是说他们都含有一个相同的误差项，通过作差，可以将误差消掉。这种两 receiver 信号组合的方式称为 **between-receiver single difference**
- 消除误差 3：同一个 receiver接收到不同卫星的信号，其中在 receiver 侧的误差是相同的，可以通过作差，消去这一误差，这种 GPS 信号组合的方式称为 **between-satellite single difference**
- 同时消除误差 1, 2, 3： 如果有两个临近的 receiver 跟踪两个卫星，针对每个卫星都有一个 between-receiver single difference, 也就是消除了误差 1 和 2 的信号，两个 single difference 都只有误差 3 ，再次作差，就可以消去误差 1, 2, 3 的信号。这种 GPS 信号组合方式称为 **double difference**
- 消去 initial ambiguity cycels. 尽管外界误差可以通过上述 double difference 的方式消除，在计算距离时，还有不确定的 initial ambiguity cycles 存在。因此通过对两个不同时刻 doube difference 作差，消去他们共同的 ambiguity。这种组合称为 **triple difference **. 
- 检测 cycle slips。前边的 triple difference 方式也可以用来检测 cycle slips。cycle slips 出现时，会在 triple difference 序列中造成一个 spike 尖刺现象。

上述消除误差的组合方式可以应用在 L1 或 L2 上。如果同时应用，此时只用一个 receiver 就可以通过对这两个 frequency 信号作差，消去电离层误差。

### selective availability 

美国国防部在 1990 Mar 25 人为的给 C/A code 添加噪声，使得 C/A code 定位精度（百米级别的误差）低于军用 P code 信号精度。这被称为 selective availabilty (SA). 

可以通过距离很近的 receiver 来消除 SA 造成的误差，这应该就叫做 DGPS 吧，differential GPS.

2000 May 1，SA 结束。



## NMEA 格式

NMEA 是 接收到的GPS 信号的一个格式，主要包括经纬度和海拔。这些数据是 GPS 的基本信息内容。需要其他内容的话，要在此基础上转换。后边的坐标系就是转换的几种情况。

NMEA： National Marine Electronics Association.

这一组织在 GPS 出现之前就存在了。目前， NMEA 数据格式是所有 GPS 制造商都支持的格式。

首先熟悉一下基本概念：

- latitude 纬度
- longitude 经度
- altitude 海拔
- UTC: Coordinated Universal Time 协调世界时
- GMT: Greenwich Mean Time 格林威治标准时间，两者之间没有差别

就像上图所示，NMEA msg 不止一种，先看看最基本的 $GPGGA msg，例如

```
$GPGGA,181908.00,3404.7041778,N,07044.3966270,W,4,13,1.00,495.144,M,29.200,M,0.10,0000*40
```

- $ 表示 msg 开头
- GP 表示 GPS 信号，GL 表示 GLONASS 信号
- 181908.00 时间戳，UTC 时间下的时、分、秒
- 3404.7041778 纬度，格式为 DDMM.MMMMM: degrees, minutes, and decimal minutes
- N 北纬
- 07044.3966270 经度，格式为 DDDMM.MMMMM
- W 西经
- 4 表示信号质量
  - 1 Uncorrected coordinate
  - 2 Differentially correct coordinate (e.g., WAAS, DGPS)
  - 4 RTK fix coordinate
  - 5 RTK float 
- 13 用了几个卫星
- 1.00 HDOP (horizontal dilution of precision) 水平精度因子
- 495.144 天线海拔高度
- M 单位，Meters or Feet
- 29.2  geodial separation, subtract this from the altitude of the antenna to get Height Above Ellipsoid 
- M 单位
- 0.1 the age of the correction
- 0000 the correction station ID
- *40 the checksum

上述经纬度是可以在 google 地图上查询具体地点的，输入

```
35 14.1419383,137 0.2612263  # 将 度 单拿出来， 分 都放在一起
```

定位到日本名古屋郊区的守山 (Moriyama)



## GPS 三维坐标系统

### reference surface

地球不是光滑的椭球体，为了方便定位，需要用一个光滑的椭球体去近似它，实际上是近似它的海平面 （Geoid）对应的椭球面。这个近似的椭球面被成为  reference surface 或 reference ellipsoid.

![gps3](pic/gps3.png)



最常用的 reference surface 是 双轴椭球 biaxial ellipsoid，实际上就是最标准的椭球，如下图所示

![gps4](pic/gps4.png)

双轴椭球面就是普通的椭圆绕短轴旋转得到的。

一个双轴椭球可以用两个参数刻画：长半轴 semimajor $a$ 和短半轴 semiminor $b$ 。

也可以用长半轴 semimajor $a$ 和扁率 flattening $f=1-(b/a)$ 来刻画。扁率越大，球体越扁。

### geodetic datum 大地基准

在选定的 reference surface 基础之上，再进一步定义好 原点和两个轴的朝向，就形成了一个基准 datum。

geocentric geodetic datum 是一类以地球中心作为 原点的基准。

不同的轴朝向又对应了不同的基准。要想唯一的确定基准，需要8 个参数：

- 2 个参数来定义 reference surface，即长短轴或者长轴+扁率
- 3 个参数来定义 origin
- 3 个参数来定义三个轴的朝向

常用的三个 datum和他们对应的 reference surface

![gps_reference1](pic/gps_reference1.png)

### coordinate system 坐标系统

一般来说， coordinate system 就是通过一系列规则，指定某一点的位置（称为 坐标）。

这似乎跟 datum 没啥却别，如果说有区别的话，坐标系统是在 datum 框架基础上，再设定单位距离，这样才能给出某一点对应的坐标。

只要能确定一个点的位置就行，不局限于直角坐标系，极坐标系等。比较常见的有如下几个：

#### CTRS == ECEF

![gps_coordinate](pic/gps_3d_coordinate.png)

CTRS (Conventional Terrestrial Reference system) 坐标系与地球相对固定，也被称为 Earth-Centered, Earth_Fixed (ECEF) coordinate system.

- z 轴指向北极点
- x 轴为赤道面和本初子午线面的交线
- y 轴为 x 轴东转 90 度，以便使  x-y-z 构成右手坐标系

上边只是说了坐标系的框架，具体坐标取值并没有指定。目前采用比较广泛的一种 CTRS 坐标值设定系统是 ITRS (International Terrestrial Reference System), 对应的坐标系称为  ITRF (International Terrestrial Reference Frame).

#### WGS84

The World Geogetic System of 1984 是由美国制图局提出来的坐标系统，这是 GPS 的官方坐标系。WGS84 采用CTRS，数值方面有自己的体系，但是非常接近 ITRF.

在北美地区还有 NAD83 (North American Datum of 1983) 系统。

WGS 与 NAD 采用的 reference surface 几乎相同，但是圆心位置偏移了 2 m，这就导致了坐标数值的差别，但这个差别可以忽略不计。

GPS 系统采用 WGS84 coordinate system，给出每一个点的 latitude $\phi$, longitude $\lambda$, height above reference surface $h$，如下图 (a) 所示，地球表面以及非表面的点都可以通过这三个参数表示。

![gps_geodetic](pic/gps_geodetic.png)

上述 geodetic coordinate 还可以转化成 Cartesian coordinate，这里就是三维空间坐标系。这里需要知道 reference surface 的参数。

后边我们还会提到将经纬度转化到 rectangular grid coordinate，也就是我们自动驾驶中使用的 gnss_pose 坐标了。

### ENU 与 NED

ENU： East - North - Up

NED ： North - East - Down

都是基于布局区域的切面建立的坐标系，都是右手坐标系。

ENU 比较直观， x 轴： East,  y 轴：North， z 轴：Up。

之所以用 NED 是面向飞机，由于对于飞机来说，感兴趣的物体主要在其下方，所以有一个指向下的坐标轴比较方便，与之匹配的，为了满足右手坐标系，x 轴设为 North， y 轴设为 East。

![enu1](pic/enu1.svg)





## map projection

所谓 map projection 简单的说就是把球面上的点投影到平面上。

![gps_map](pic/gps_map.png)

从球面到平面，总会有失真，要么角度失真，要么区域缩放失真。下面介绍几个比较常用 map projection.

### Equirectangular projection 等距投影

也称为 geographic projection，大概于公元 100年左右就发明了。

![gps_equirectangular](pic/gps_equirectangular.jpg)

- 将经线投影成平行线，不论什么纬度上的经线间隔都相同
- 将纬线投影成平行线

原本经线间隔是越到高纬度越小的。equirectangular 这种投影方式扭曲了地球表明两点间的距离，越到高纬度，扭曲越严重。扭曲程度如下图所示，可见俄罗斯、南极、格陵兰岛等是被拉伸了的。

![gps_equirectangular1](pic/gps_equirectangular1.png)

由于扭曲，这种平面地图是不能用来精确导航的，但好处是比较直观，对于不那么精确的场合，可以作为辅助参考。

这种地图纬线之间的间距没有失真，经线间距失真。

这种地图在赤道附近的投影是最精确的，经线间距的失真可以忽略。这对于新加坡来说是个好消息，可以直接把经纬度与 x-y 坐标对应起来，不用考虑失真。

已知新加坡NTU Hall 7 (纬度 1.3410676543865188, 经度 103.68017346176953) 附近，

- 纬线间距 1 度，长为 110574.8808516026 米
- 经线间距 1 度，长为111289.17062452697 米

此网站可以计算世界各地经纬度对应的长度 （http://www.csgnetwork.com/degreelenllavcalc.html）。

假设选定新加坡某点 A 为 x-y 坐标系原点，它的经纬度分别为： A_lon, A_lat，对于新加坡另一点 B ，经纬度为 B_lon, B_lat，其 x-y 坐标为 

- x: (B_lon - A_lon) * 111289.17062452697 米
- y: (B_lat - A_lat) *  110574.8808516026 米



在 GPS 中，应用最广泛的是保角映射 （ conformal map projection），它保持了角度信息，但是区域被缩放了。常见的 conformal map projection 有

- transverse Mercator projection, also known as Gauss-Kruger projection
- universal transverse Mercator (UTM)
- Lambert conformal conic projections

### Transverse Mercator projection

1772年由 Johann Lambert 发明。

![gps_transverse](pic/gps_transverse.png)

用圆柱体内切球体于本初子午线，切线上所有属性没有任何失真，越远离本初子午线，失真越严重。

上述左图沿上下两边剪开，把圆柱面放平，就得到右图。

### UTM 

UTM, Universal Transverse Mercator。

采用圆柱与球体相割的方式，如下

![gps_utm](pic/gps_utm.png)

但是与之前 transverse 不同的是，UTM首先将地球左右分成 60 个 zone，每个 zone 都有自己的 central meridian。针对每个 zone 进行上述 割。这样分块投影失真要减少很多。

地球具体的分 zone方式如下：

地球的大陆范围全都包含在了南纬 80 到 北纬 84 度之间。在这个范围内，从180度经线（即国际日期变更线）开始向东，每6个经度为一个 Zone, 共 60 个。例如， Zone 1 覆盖了经度从 180 到 174 W，中心为 177 W， Zone 60 覆盖了经度从 174 E 到 180，中心在 177 E 。

在每个 Zone 中，为了进一步确定某一点的东西位置，将 Zone 中的中心经度线设定为 500 km，在该线西侧的数值按照实际距离减小，东侧的数值增加，这种标记方法称为 Easting，很显然，赤道附近数值变化范围比较大，为 167 km 到 833 km，而北纬 84 范围最小，为 465 km 到 515 km。

以上数据可以精确定义东西位置，为了确定南北位置，则设定赤道为 0 km，越往北越大，最大为 9300 km。往南则减小，但是为了避免出现负数，南半球定位时将赤道设定为 10000 km。这样往南减小到最南端，为 1100 km。不会出现负数。这种定位方式成为 Northing。

但是在南北定位时会出现一个问题，即不同的点可能有相同的 Northing 值。因此还需要额外的信息，有两种解决办法：

- grid zone： 从 80 S 到 84 N 每隔 8 度分 1 个区，标记从 C 开始，中间跳过 I 和 O，最后 X 包括 12 度。这样 17T 630084 4833438 就唯一确定了一个点的位置。
- 指明 S 或 N，即南半球或者北半球。例如，17N 630084 4833438 也可以唯一的确定一个位置。

注意： 单纯给出 “ 17N 630084 4833438 ”可能会有歧义，因为 “N” 可能是表示北纬，或者 Grid Zone 标记，因此需要事先指明到底采用的那一种标记方式。

### Lambert conical projection

也是 1772年由 Johann Lambert 发明。

前边两种都是左右失真加重，Lambert conical projection 则是切割线上下失真加重，如下图所示：

![gps_lambert](pic/gps_lambert.png)

沿切割线的投影没有失真，往上下，失真增加。

### Local arbitrary mapping systems

在较小的区域内，可以采用用户指定的平面来代替地球曲面，失真可以忽略不计。

为了建立这种 mapping，需要知道若干点在球面的坐标和在平面的坐标，计算之间的对应关系。

然后当获得新的曲面坐标时，可以转换到平面中。

![gps_local_mapping](pic/gps_local_mapping.png)





## 

























