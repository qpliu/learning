
## ROS网络结构示意图

节点 acceleration_ecu 接受twist_cmd中的线速度和角速度信息，转换成车辆的控制信息

![ecu](../pic/ecu.png)

## 接受 twist_cmd信号，获得期望速度与角速度

twist_cmd 信号的类型为 geometry_msgs/TwistStamped，结构如下
```
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
geometry_msgs/Twist twist
  geometry_msgs/Vector3 linear
    float64 x
    float64 y
    float64 z
  geometry_msgs/Vector3 angular
    float64 x
    float64 y
    float64 z
```

首先接受 twist_cmd 信号，将其中的线速度和角速度赋给相应的变量

```python
rospy.Subscriber('input_cmd', TwistStamped, self.callback_cmd) # 这里input_cmd映射到twist_cmd
```

callback函数如下：
```python
def callback_cmd(self,data):
        self.cmd_velocity = data.twist.linear.x
        self.cmd_steer = data.twist.angular.z
        self.last_cmd = rospy.Time.now()
```

## 接受/odom/gt信号，获得当前速度

```python
rospy.Subscriber('/odom/gt', Odometry, self.callback_gt)
```

callback 函数如下：
```python
 def callback_gt(self,data):
        self.current_velocity = math.sqrt( data.twist.twist.linear.x**2 + data.twist.twist.linear.y**2 + data.twist.twist.linear.z**2 )
```


## prius_msgs/Control 

具有如下结构：
```
uint8 NO_COMMAND=0
uint8 NEUTRAL=1
uint8 FORWARD=2
uint8 REVERSE=3
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
float64 throttle
float64 brake
float64 steer
uint8 shift_gears
```

这里的关键是如何从期望速度和角速度得到Control中的最后四个值.

- throttle 对应油门，如果加速度为正就对应踩油门
- brake 对应刹车，如果加速度为负就对应踩刹车
- steer 对应方向盘旋转百分比
- shift_gears 对应前进或后退档位，由速度的正负可知

## 计算加速度并确定油门和刹车

实际上就是比例控制器，由期望速度与当前速度之差确定对应的加速度
```python
cmd_acceleration = self.GAIN_P * ( abs(self.cmd_velocity) - self.current_velocity)
```

然后依据加速度方向，设置对应的throttle 和brake
```python
  if cmd_acceleration > 0:
            msg_cmd.throttle = cmd_acceleration
            msg_cmd.brake = 0
  elif cmd_acceleration < 0:
            msg_cmd.throttle = 0
            msg_cmd.brake = -cmd_acceleration
```

## 由期望速度确定档位

```python
   if self.cmd_velocity >= 0 :
            msg_cmd.shift_gears = Control.FORWARD
    else :
            msg_cmd.shift_gears = Control.REVERSE
```

## 计算车轮角度并确定方向盘转角比例

如果令角速度为$\dot{\theta}$，车轮转角为$\delta$，线速度$v$，车身长$L$，则其关系为：

$\dot{\theta} = \tan (\delta) v / L$

由此可以根据期望角速度计算车轮转角

$\delta = atan(\dot{\theta}*L/v)$

**注意：**车轮转角大小并不等于方向盘转角大小

给定车轮最大转角
```python
MAX_STEERING_ANGLE = 0.6458; # max steering angle of a wheel
```

以上车轮转角是在方向盘打满时的车轮角度，因此方向盘转角的百分比为
```python
    if self.cmd_velocity > 0:   
                ratio = self.WHEEL_BASE * self.cmd_steer /  self.cmd_velocity
                if ratio >= 1:
                    cmd_steering_wheel = 1
                elif ratio <= -1 :
                    cmd_steering_wheel = -1
                else:
                    cmd_steering_wheel = math.asin (ratio) / self.MAX_STEERING_ANGLE # 此处应为atan而不是asin
    else :
                cmd_steering_wheel = 0
```


## publish Control 信号

```python
self.pub_control = rospy.Publisher('/prius', Control, queue_size=1)
```
将上边得到的Control msg发布出去
```python
self.pub_control.publish(msg_cmd)
```

## 完整程序
```python
#!/usr/bin/env python
# P controller for acceleration pedal of the car, takes Twist message and translates it to Control message

import rospy
from std_msgs.msg import String, Float64
from geometry_msgs.msg import Twist, TwistStamped
from prius_msgs.msg import Control
from nav_msgs.msg import Odometry
import sys, math

class acceleration_ecu:
    GAIN_P = 10
    GAIN_I = 0
    GAIN_D = 0
    MAX_STEERING_ANGLE = 0.6458; # max steering angle of a wheel（车轮的最大转角）
    WHEEL_BASE = 2.86  #distance between front and rear axle
    TIMEOUT = rospy.Duration.from_sec(1);

    # 这里定义了6个变量，希望是在整个程序运行过程中保持不变的，所以按照习惯全部大写
    # P，I，D 是预先设置好不再改变的，随后的两个则先定义了初始值，如果程序运行后能够从 get_param 中获得指定值，则用新值
    # 新的赋值是在 __init__(self) 函数中完成的

    def __init__(self):

        rospy.Subscriber('input_cmd', TwistStamped, self.callback_cmd)
        rospy.Subscriber('/odom/gt', Odometry, self.callback_gt)
        # Subscriber class 都是不需要具体的 object 名字的，因为用不到
        
        self.pub_control = rospy.Publisher('/prius', Control, queue_size=1)

        # parameters
        self.MAX_STEERING_ANGLE = rospy.get_param("/coms/max_steering_angle", self.MAX_STEERING_ANGLE)
        self.WHEEL_BASE = rospy.get_param("/coms/wheel_base", self.WHEEL_BASE)

        print "Max steering angle: " + str(self.MAX_STEERING_ANGLE)
        print "Wheel base: " + str(self.WHEEL_BASE)

        # initial velocity and tire angle are 0
        # 这些变量并没有在外边定义，而是在这里首次加入的
        self.current_velocity = 0;   
        self.cmd_steer = 0;   
        self.cmd_velocity = 0;    
        self.last_cmd = rospy.Time.now();


    def callback_cmd(self,data):
        self.cmd_velocity = data.twist.linear.x
        self.cmd_steer = data.twist.angular.z
        # 直接从 twist_cmd 中接受线速度和角速度，这两个速度之间是关系是：角速度=线速度×曲率    
            
        self.last_cmd = rospy.Time.now()
        # 更新希望的线速度、希望的角速度和控制信号到达对应的时间


    def callback_gt(self,data):
        self.current_velocity = math.sqrt( data.twist.twist.linear.x**2 + data.twist.twist.linear.y**2 + data.twist.twist.linear.z**2 )


    def pid_iteration(self):
        msg_cmd = Control()  # 后边没有设置它的 header.stamp，

        if rospy.Time.now() - self.last_cmd > self.TIMEOUT:            
            cmd_acceleration = 0
            cmd_steering_wheel = 0      
            # 超过1秒没有新的控制数据到来，有问题了，加速度置为0
        else:
            cmd_acceleration = self.GAIN_P * ( abs(self.cmd_velocity) - self.current_velocity)

            #computation of steering angle from angular velocity of the car  
            if self.cmd_velocity > 0:   
                ratio = self.WHEEL_BASE * self.cmd_steer /  self.cmd_velocity
                if ratio >= 1:
                    cmd_steering_wheel = 1
                elif ratio <= -1 :
                    cmd_steering_wheel = -1
                else:
                    cmd_steering_wheel = math.asin (ratio) / self.MAX_STEERING_ANGLE 
                    # 得到车轮转角，百分比表示，这里有可能超过1
            else :
                cmd_steering_wheel = 0
        if self.cmd_velocity >= 0 :
            msg_cmd.shift_gears = Control.FORWARD
        else :
            msg_cmd.shift_gears = Control.REVERSE
        # 得到 Control msg 的第 4 个参数


        if cmd_acceleration > 1:
            cmd_acceleration = 1
        elif cmd_acceleration < -1:
            cmd_acceleration = -1

        if cmd_steering_wheel > 1:
            cmd_steering_wheel = 1
        elif cmd_steering_wheel < -1:
            cmd_steering_wheel = -1
        # 车轮转角与最大可能转角的百分比，在正负100% 之间

        if cmd_acceleration > 0:
            msg_cmd.throttle = cmd_acceleration
            msg_cmd.brake = 0
        elif cmd_acceleration < 0:
            msg_cmd.throttle = 0
            msg_cmd.brake = -cmd_acceleration
        # 得到 Control msg 的第 1, 2 个参数


        msg_cmd.steer = cmd_steering_wheel;
        # 得到 Control msg 的第 3 个参数

        self.pub_control.publish(msg_cmd)      


def main(argv):
    node = acceleration_ecu()
    rate = rospy.Rate(10)
    # 每秒转换 10 个控制信号
    while not rospy.is_shutdown():
        node.pid_iteration()
        rate.sleep()


if __name__ == '__main__':
    rospy.init_node('acceleration_ecu')
    # 主函数中第一个命令就是初始化 node 
    try:
        main(sys.argv[1:])
    except rospy.ROSInterruptException: 
        pass
```
