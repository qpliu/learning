
## Autoware 中的 ROS 网络结构图

![pure_pursuit](img/without_controller_attack.png)

[pure_pursuit_rosgraph](img/without_controller_attack.png)

## 历史发展

pure pursuit 算法是一种 path following 算法。

- 在路上机器人中应用最早由 Wallace 等人于 1985年提出，出现在了 CMU 的一份 report 中 《First Results in Robot Road-Following》。

- 后来 Amidi 在其 master's thesis 中对比了几种 path following 算法，认为 pure pursuit 算法最鲁棒、最可靠。

- 在 1992 年，另一份 CMU 的 report 中 Coulter 对其进行了详细介绍，并说明了其实现过程 《Implementation of the Pure-Pursuit Path Tracking Algorithm》。

## 原理解析

### where came the name ?

The name pure-pursuit comes from thinking of the vehicle as chasing a point on the path some distance ahead of it.

### 效果示意图

![pure-pursuit1](img/pure-pursuit-cycles.png)

### 公式推导

![pure-pursuit2](img/calcCurvature.png)

**推导目标**:将曲率 curvature $\gamma$ 用已知量 $x$,$y$,$l$表示。由于$\gamma = 1/r$，只需要求出$r$ 与已知量的关系即可。

已知 $r^2 = d^2 + y^2$, $x+d = r$, $x^2 + y^2 = l^2$，想办法消去 $d$即可。

整理：$r^2 = (r-x)^2 + y^2 = r^2 - 2xr + x^2 + y^2 = r^2 - 2xr + l^2 $

得到：$r = l^2 / 2x$.

考虑到转角方向性，左转为正，右转为负，总之与 $x$的符号相反，因此有 $r = -l^2 / 2x$

曲率 $\gamma = -2x / l^2 $

### 从比例控制器角度解读

若将 $x$ 看作误差，$-2 / l^2$ 看作系数，$\gamma = -2x / l^2 $ 就是一个比例控制器

较大的 $l$ 导致比例系数较小，转角比较平滑，但是跟踪精度较差。

较小的 $l$ 导致比例系数较大，可以快速降低跟踪误差，但是可能引起系统不稳定。

在实际中，可以令 $l$ 与速度相关，通过一个 ratio 联系起来。速度快的时候，看远一点；速度慢就可以看近一点。

### pure-pursuit 的特点

一般只要是求解当前位置和目标点之间弧线曲率的跟踪方式都可以称为 pure-pursuit，这是它的核心特征。至于如何得到目标点，不同的文献中有不同的方式。

例如，在1992年CMU 报告中，首先选择path 上离当前位置最近的点，然后沿该店向前搜索找到与当前位置距离为 look ahead distance的点，作为目标点。这个时候，可以认为目标点在路径上是连续的，总能找到与当前位置距离为 look ahead distance的点。

与之不同，Autoware文章中声称 path 由离散的 waypoints 构成，找到那个一定范围之外、离当前位置最近的waypoint，就当作是目标点。之所以设定一个最短距离，是为了防止转角太陡。

但在实际代码实现中，是从最终的waypoint往近处waypoint逐个搜索，只要找到第一个离当前点距离在look ahead distance之内，就把这个点当作下一个目标点。

### 计算曲率时的坐标系转换

按照上图计算曲率，必须知道 x，即目标点与当前位置的横向距离，这首先要把目标点放在汽车的坐标系中，才方便考察 x, y 的方向以及偏差。

计算出了曲率再映射到 steering wheel angle。

## 代码实现

### 源程序架构

- 每个功能的具体实现在pure_pursuit_core.cpp中，这是核心代码

- 在pure_pursuit.cpp中调用各个核心代码，实现系统性的功能

- 最后由pure_pursuit_node.cpp封装成node的形式

### 设置预瞄距离 look ahead distance

在Autoware的runtime_manager中有两个选择：dialog和waypoint，前者直接设定速度和预瞄距离，后者设定ratio和最小预瞄距离。

如果是dialog模式，则直接返回设定的预瞄距离。测试中 velocity = 15, lookahead_distance = 10，效果比较好。

如果是waypoint模式，则通过当前速度计算预瞄距离，并限制在上下限之间。

程序如下：

```c++
double PurePursuitNode::computeLookaheadDistance() const

{
  if (param_flag_ == enumToInteger(Mode::dialog))//从runtimemanage对话框获取数据
    return const_lookahead_distance_;

  double maximum_lookahead_distance = current_linear_velocity_ * 10;//最大预瞄距离为当前速度的10倍
  double ld = current_linear_velocity_ * lookahead_distance_ratio_;//通过lookahead_distance_ratio_调成预瞄距离的值

  return ld < minimum_lookahead_distance_ ? minimum_lookahead_distance_//最小预瞄距离在PurePursuitNode中被设置如下minimum_lookahead_distance_(6.0)
        : ld > maximum_lookahead_distance ? maximum_lookahead_distance
        : ld;
}
```

### 寻找下一个目标点

   ```c++
  // search next waypoint
  getNextWaypoint();//要比lookahead_distance_大才有效
  if (next_waypoint_number_ == -1)
  {
    ROS_INFO("lost next waypoint");
    return false;
  }
   ```

  其中， `getNextWaypoint()` 主要内容如下：
  ```c++
for (int i = path_size -1; i > 0; i--)
  {
    // if there exists an effective waypoint
    if (getPlaneDistance(current_waypoints_.at(i).pose.pose.position, current_pose_.position) < lookahead_distance_)
    {
      next_waypoint_number_ = i;
      return;
    }
  }

  ```

  如果`getNextWaypoint()`找到了满足条件的目标点 i，就设置 `next_waypoint_number_ = i`，如果没找到，程序返回-1，系统会显示`"lost next waypoint"`。

### 计算曲率

```c++
next_target_position_ = current_waypoints_.at(next_waypoint_number_).pose.pose.position;//获取下一路径点的position
 *output_kappa = calcCurvature(next_target_position_);//计算下一个路径点和机器人当前位置规划圆弧的曲率，即半径的倒数
```
计算曲率具体程序如下：
```c++
double PurePursuit::calcCurvature(geometry_msgs::Point target) const
{
  double kappa;
  double denominator = pow(getPlaneDistance(target, current_pose_.position), 2);
  double numerator = 2 * calcRelativeCoordinate(target, current_pose_).y;

  if (denominator != 0)
    kappa = numerator / denominator;
  else
  {
    if (numerator > 0)
      kappa = KAPPA_MIN_;
    else
      kappa = -KAPPA_MIN_;
  }
  ROS_INFO("kappa : %lf", kappa);
  return kappa;
}
```

### publish 线速度和角速度

```c++
geometry_msgs::TwistStamped ts;
  ts.header.stamp = ros::Time::now();
  ts.twist.linear.x = can_get_curvature ? computeCommandVelocity() : 0;//计算线速度
  ts.twist.angular.z = can_get_curvature ? kappa * ts.twist.linear.x : 0;//计算角速度
  pub1_.publish(ts);//发布话题"twist_raw"，从这里可以看出来，twist_raw中是线速度和角速度，而不是角度。注意，这里角速度不是车前轮旋转的角速度，而是线速度×曲率对应的角速度。
```

其中计算线速度函数 `computeCommandVelocity()`如下：
```c++
double PurePursuitNode::computeCommandVelocity() const
{
  if (param_flag_ == enumToInteger(Mode::dialog))  # 如果runtime_manager中已经设定了速度（千米每小时），就转换成米每秒，直接返回。
    return kmph2mps(const_velocity_);

  return command_linear_velocity_; #如果用户没有在runtime_manager里面设定速度，就需要从topic里面获取速度信息，下边详述
}

command_linear_velocity_通sub1_ = nh_.subscribe("final_waypoints", 10, &PurePursuitNode::callbackFromWayPoints, this);通过final_waypoints话题订阅获得，该话题为用户发布，这个final_waypoints不是最后那个waypoint，而是最终确定的一系列waypoints

void PurePursuitNode::callbackFromWayPoints(const waypoint_follower::laneConstPtr &msg)
{
  if (!msg->waypoints.empty())
    command_linear_velocity_ = msg->waypoints.at(0).twist.twist.linear.x;//获取用户设置路径点中的线速度
  else
    command_linear_velocity_ = 0;

  pp_.setCurrentWaypoints(msg->waypoints);//对pure_suit的成员变量std::vector<waypoint_follower::waypoint> current_waypoints_赋值值
  is_waypoint_set_ = true;//表明接收到了用户的路径设置点
}
```


### two topics: twist_raw and ctrl_cmd

pure-pursuit在publish 话题 twist_raw的同时还publish 话题ctrl_cmd，代码如下：

```c++
void PurePursuitNode::publishControlCommandStamped(const bool &can_get_curvature, const double &kappa) const
{
  if (!publishes_for_steering_robot_)
    return;

  autoware_msgs::ControlCommandStamped ccs;
  ccs.header.stamp = ros::Time::now();
  ccs.cmd.linear_velocity = can_get_curvature ? computeCommandVelocity() : 0;
  ccs.cmd.steering_angle = can_get_curvature ? convertCurvatureToSteeringAngle(wheel_base_, kappa) : 0;

  pub2_.publish(ccs);
}
```

话题ctrl_cmd中包含了线速度（与twist_raw中相同）和角度（由曲率和车身长度计算出来），其中角度计算如下：
```c++
double convertCurvatureToSteeringAngle(const double &wheel_base, const double &kappa)
{
  return atan(wheel_base * kappa);
}
```

但是这个ctrl_cmd并没有被使用，它是干什么的？**为什么需要这个额外的ctrl_cmd**。或许这是针对不同的控制机制：

- 如果机器人能够把期望的线速度和角速度自动转化为自身的控制量，那就给它线速度和角速度，也就是twist_raw

- 如果机器人只能接受比较底层的线速度和轮子偏转角度，就给它线速度和角度，也就是ctrl_cmd. 幸运的是，我们的 kinematic model 中的 $v$ 和 $\delta_f$ 就是这里 ctrl_cmd 的两个值，当作 EKF 的控制输入就可以了



**注意：**twist_raw中角速度是预期的，是希望车能够实现的线速度和角速度，同样的线速度可能会有不同的角速度，所以需要设定个预期。但是twist_raw中的这个角速度的积分绝对不是车轮的偏转角度，而是线速度×曲率，与车辆自身轮子的角度是两回事。ctrl_cmd中的角度才是车辆轮子的角度。如果令角速度为$\dot{\theta}$，轮子转角为$\delta$，线速度$v$，车身长$L$，则其关系为：$\dot{\theta} = \tan (\delta) v / L$
