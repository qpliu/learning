# 数据实例

一组 nmea_sentence topic 中 nmea_msgs/Sentence 结构的数据，包括的格式有

- WW03E
- $GNRMC
- $GPGGA
- $GNVTG
- QQ02C

注意，并不是同一时刻同时接收到这些种类的数据，而是异步接收的。每一时刻只有一个 msg 进来。

具体内容：

```
header: 
  seq: 155205
  stamp: 
    secs: 1427157653
    nsecs: 582847118
  frame_id: "/gps"
sentence: "WW03E,INSDAT,004038.40,2.314,-1.433,-9.447,2.869,-2.022,-13.123,@ED"
---
header: 
  seq: 155206
  stamp: 
    secs: 1427157653
    nsecs: 732773065
  frame_id: "/gps"
sentence: "$GNRMC,004038.60,A,3514.1419383,N,13700.2612263,E,2.3796,197.649,240315,7.320,E,D*1E"
---
header: 
  seq: 155207
  stamp: 
    secs: 1427157653
    nsecs: 749806880
  frame_id: "/gps"
sentence: "$GPGGA,004038.60,3514.1419383,N,13700.2612263,E,4,12,0.81,47.4489,M,38.4566,M,1.6,0556*4E"
---
header: 
  seq: 155208
  stamp: 
    secs: 1427157653
    nsecs: 759890079
  frame_id: "/gps"
sentence: "$GNVTG,197.649,T,204.970,M,2.3796,N,4.4070,K,D*3A"
---
header: 
  seq: 155209
  stamp: 
    secs: 1427157653
    nsecs: 769795894
  frame_id: "/gps"
sentence: "QQ02C,INSATT,V,004038.60,6.745,13.997,208.697,@66"

```



# nmea2tfpose_node.cpp

这个程序仅仅是构造一个 rosnode ，关键的内容都在 nmea2tfpose_core 中

```c++
#include <ros/ros.h>

#include "nmea2tfpose_core.h"

int main(int argc, char **argv)

{

  ros::init(argc, argv, "nmea2tfpose"); // 构造 ros node

  gnss_localizer::Nmea2TFPoseNode ntpn;

  ntpn.run();

  return 0;

}
```



# nmea2tfpose_core.h

``` c++
#ifndef NMEA2TFPOSE_CORE_H
#define NMEA2TFPOSE_CORE_H

// C++ includes
#include <string>
#include <memory>

// ROS includes
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <nmea_msgs/Sentence.h>
#include <tf/transform_broadcaster.h>

#include <gnss/geo_pos_conv.hpp>

namespace gnss_localizer
{
class Nmea2TFPoseNode
{
public:
  Nmea2TFPoseNode();
  ~Nmea2TFPoseNode();

  void run();

private:
    
    // 构造 node 句柄
  // handle
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;

    
    // 一个 pub, 一个 sub
  // publisher
  ros::Publisher pub1_;
  // subscriber
  ros::Subscriber sub1_;

  // constants
  const std::string MAP_FRAME_;
  const std::string GPS_FRAME_;

  // variables
  int32_t plane_number_;
  geo_pos_conv geo_;
  geo_pos_conv last_geo_;
  double roll_, pitch_, yaw_;
  double orientation_time_, position_time_;
  ros::Time current_time_, orientation_stamp_;
  tf::TransformBroadcaster br_;

  // callbacks
  void callbackFromNmeaSentence(const nmea_msgs::Sentence::ConstPtr &msg);

  // initializer
  void initForROS();

  // functions
  void publishPoseStamped();
  void publishTF();
  void createOrientation();
  void convert(std::vector<std::string> nmea, ros::Time current_stamp);
};  // 这里 class 结束

std::vector<std::string> split(const std::string &string);

}  // gnss_localizer
#endif  // NMEA2TFPOSE_CORE_H
```



# nmea2tfpose_core.cpp



```c++

#include "nmea2tfpose_core.h"

namespace gnss_localizer
{
// Constructor
Nmea2TFPoseNode::Nmea2TFPoseNode()
  : private_nh_("~")
  , MAP_FRAME_("map")
  , GPS_FRAME_("gps")
  , roll_(0)
  , pitch_(0)
  , yaw_(0)
  , orientation_time_(0)
  , position_time_(0)
  , current_time_(0)
  , orientation_stamp_(0)
{
  initForROS();
  geo_.set_plane(plane_number_);
}

// Destructor
Nmea2TFPoseNode::~Nmea2TFPoseNode()
{
}

void Nmea2TFPoseNode::initForROS()
{
    
    // plane 外部设置参数为 7 
  // ros parameter settings
  private_nh_.getParam("plane", plane_number_);

    
    // 接受 nmea_sentence topic 
  // setup subscriber
  sub1_ = nh_.subscribe("nmea_sentence", 100, &Nmea2TFPoseNode::callbackFromNmeaSentence, this);

    
    // 发送 gnss_pose topic
  // setup publisher
  pub1_ = nh_.advertise<geometry_msgs::PoseStamped>("gnss_pose", 10);
}

void Nmea2TFPoseNode::run()
{
  ros::spin();
}

    
    // 最后 pub pose
void Nmea2TFPoseNode::publishPoseStamped()
{
  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = MAP_FRAME_;  // map
  pose.header.stamp = current_time_;  
  pose.pose.position.x = geo_.y();
  pose.pose.position.y = geo_.x();
  pose.pose.position.z = geo_.z();
  pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll_, pitch_, yaw_);
  pub1_.publish(pose);
}

    
    // 最后 pub tf
void Nmea2TFPoseNode::publishTF()
{
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(geo_.y(), geo_.x(), geo_.z()));
  tf::Quaternion quaternion;
  quaternion.setRPY(roll_, pitch_, yaw_);
  transform.setRotation(quaternion);
  br_.sendTransform(tf::StampedTransform(transform, current_time_, MAP_FRAME_, GPS_FRAME_));  // map, gps
}

    
    // 当长时间没有接收到 orientation 信息时，放弃，直接手动计算 orientation
void Nmea2TFPoseNode::createOrientation()
{
  yaw_ = atan2(geo_.x() - last_geo_.x(), geo_.y() - last_geo_.y());
  roll_ = 0;
  pitch_ = 0;
}
    

    
    /* 这个 convert 函数的作用是：对于接受到的 nmea msg 进行分类处理，关注的类别包括：
         WW03E
         $GNRMC
         $GPGGA
         $GNVTG
         QQ02C
    */
void Nmea2TFPoseNode::convert(std::vector<std::string> nmea, ros::Time current_stamp)
{
  try
  {
    if (nmea.at(0).compare(0, 2, "QQ") == 0)  // 比较 vector 中第 0 个元素中，0 - 0+2 字符
    { // QQ里面的数据格式 "QQ02C,INSATT,V,004038.60,6.745,13.997,208.697,@66"
      orientation_time_ = stod(nmea.at(3));  // 获取角度信息的时间
      roll_ = stod(nmea.at(4)) * M_PI / 180.;
      pitch_ = -1 * stod(nmea.at(5)) * M_PI / 180.;
      yaw_ = -1 * stod(nmea.at(6)) * M_PI / 180. + M_PI / 2;
      orientation_stamp_ = current_stamp; // 时间按照 msg timestamp 的时间
      ROS_INFO("QQ is subscribed.");
    }
      // 这个没有
    else if (nmea.at(0) == "$PASHR")
    {
      orientation_time_ = stod(nmea.at(1));  
      roll_ = stod(nmea.at(4)) * M_PI / 180.;
      pitch_ = -1 * stod(nmea.at(5)) * M_PI / 180.;
      yaw_ = -1 * stod(nmea.at(2)) * M_PI / 180. + M_PI / 2;
      ROS_INFO("PASHR is subscribed.");
    }
      // 这个比较 3 - 3+3 ，正好有这部分数据，具体实例如下： "$GPGGA,004038.60,3514.1419383,N,13700.2612263,E,4,12,0.81,47.4489,M,38.4566,M,1.6,0556*4E"
    else if(nmea.at(0).compare(3, 3, "GGA") == 0)
    {
      position_time_ = stod(nmea.at(1));  // 获取位置信息的时间
      double lat = stod(nmea.at(2)); // 纬度 3514.1419383   
      double lon = stod(nmea.at(4)); // 经度 13700.2612263
      double h = stod(nmea.at(9));  // 海拔  47.4489
      geo_.set_llh_nmea_degrees(lat, lon, h);  // 将上述信息赋给 geo_，实际上 set 这个函数自身就带了转换的功能，可以通过 geo_.x() 等把转换之后的 x,y,z 提取出来。
/*例如
  	3514.0738120， 
	13700.3159224
	48.4194
可以转化成
    x: -14690.5864976
    y: -84905.6949755
    z: 48.4058
*/
      ROS_INFO("GGA is subscribed.");
    }      
      // 这个没有
    else if(nmea.at(0) == "$GPRMC")
    {
      position_time_ = stoi(nmea.at(1));
      double lat = stod(nmea.at(3));
      double lon = stod(nmea.at(5));
      double h = 0.0;
      geo_.set_llh_nmea_degrees(lat, lon, h); 
      ROS_INFO("GPRMC is subscribed.");
    }
  }catch (const std::exception &e)
  {
    ROS_WARN_STREAM("Message is invalid : " << e.what());
  }
} // 最后只有 QQ 和 $GPGGA 中的信息有用。
    // QQ 中设定了 roll_, pitch_, yaw_ 信息，
    // $GPGGA 中将经纬度信息加入 geo_ 中

    
    // 这里是接收到  nmea_sentence 之后调用的程序，很关键
void Nmea2TFPoseNode::callbackFromNmeaSentence(const nmea_msgs::Sentence::ConstPtr &msg)
{
    /* nmea_msgs/Sentence 的数据结构很简单
    	std_msgs/Header header
            uint32 seq
            time stamp
            string frame_id
		string sentence    
    */
  current_time_ = msg->header.stamp;
    
    /* split 函数把接收到的 msg sentence 那个长字符串，按照逗号，分割成字符串向量，即 split() 返回的是字符串。
    这个 convert 函数的作用是把 sentence 中的信息分门别类的存放到变量中，主要是设定了
     roll_, pitch_, yaw_ 信息，并将经纬度信息加入 geo_ 中
    */
  convert(split(msg->sentence), msg->header.stamp);

  double timeout = 10.0;
    // 如果 nmeas_sentence 中既有 orientation 又有 position，而且 orientation 频率正常，那么总是 orientation_stamp 几乎等于 header.stamp，不会触发下边这个 if 语句
    // 如果长时间没有 QQ*** orientation 信息过来，超过了 timeout 时间，则要手动计算 orientation
  if (fabs(orientation_stamp_.toSec() - msg->header.stamp.toSec()) > timeout)
  {
    double dt = sqrt(pow(geo_.x() - last_geo_.x(), 2) + pow(geo_.y() - last_geo_.y(), 2));
    double threshold = 0.2;
    if (dt > threshold)
    {
      ROS_INFO("QQ is not subscribed. Orientation is created by atan2");
      createOrientation();
      publishPoseStamped();
      publishTF();
      last_geo_ = geo_;
    }
    return;
  }

  double e = 1e-2;
    // 只有当来自 QQ 的 orientation 和 来自 $GPGGA 的 position 对应的时间差不多时，才作为同一时刻的 pose 数据输出
  if (fabs(orientation_time_ - position_time_) < e)
  {
    publishPoseStamped();
    publishTF();
    return;
  }
} // 这个程序表明，orientation 和 position 总是同时发布的，存在两种情况
    // 1. 如果长时间没有 orientation 信息，那么就手动计算，这样 position 和手动计算的 orientation 一起发布
    // 2. 如果 orientation 和 posistion 都在发布，那么挑选两者时间比较接近的信号，作为同一时刻的 pose 信息发布。

    
    // split 函数的作用就是把一长串 string 按照逗号分割成 string 向量。
std::vector<std::string> split(const std::string &string)
{
  std::vector<std::string> str_vec_ptr;
  std::string token;
  std::stringstream ss(string);

  while (getline(ss, token, ','))
    str_vec_ptr.push_back(token);

  return str_vec_ptr;
}

}  // gnss_localizer


```

