
## vehicle_status_interpreter 的作用
车身传递来的 /prius/status 中包括刹车、油门、转角等信息，vehicle_statue_interpreter 将这些信息转换成加速度和车轮转角信息

## ROS网络结构示意图

![vehicle_status_interpreter](../pic/vehicle_status_interpreter.png)

## 关于 msg 数据格式

### subscribe  topic /prius/status
这个topic 上面发布的信息类型是 acronis_common/VehicleStatus，其格式如下：
```
Header header
float64 accel_pedal_position #value [0,1]
float64 brake_position #value [0,1]
float64 steering_wheel_position # [-1,1] [right, left] turning in  base_link frame
float64 steering_wheel_speed
```

### publish topic /coms/steering
这个 topic 上发布的信息类型为 acronis_common/StampedFloat64， 结构如下：
```
Header header
float64 value
```

### publish topic /coms/accel
这个 topic 上发布的信息类型为 geometry_msgs/AccelStamped， 结构如下：
```
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
geometry_msgs/Accel accel
  geometry_msgs/Vector3 linear
    float64 x
    float64 y
    float64 z
  geometry_msgs/Vector3 angular
    float64 x
    float64 y
    float64 z
```

## 计算加速度

```c++
accel_msg.accel.linear.x = msg->accel_pedal_position * max_acceleration - msg->brake_position * max_brake_acceleration;
```
即油门踏板的位置对应的加速度减去刹车踏板对应的负加速度，就是实际的加速度

## 计算车轮转角

```c++
steering_msg.value = msg->steering_wheel_position * max_steering_angle;
```
即方向盘所在百分比位置对应到车轮的角度

**注意：**以上程序中的max_acceleration，max_brake_acceleration和max_steering_angle默认都是取值为1

## 完成程序

```c++
// interpret status message and publish it in appropriate topics


#include "ros/ros.h"
#include "acronis_common/VehicleStatus.h"
#include "acronis_common/StampedFloat64.h"
#include "geometry_msgs/AccelStamped.h"

using namespace std;

class Interpreter {
protected:
  ros::NodeHandle nh_;
  ros::Subscriber input_status;
  ros::Publisher output_acceleration;
  ros::Publisher output_steering;

  double max_steering_angle;
  double max_acceleration;
  double max_brake_acceleration;

public:
  Interpreter(ros::NodeHandle& nh) : nh_(nh), max_steering_angle(1), max_acceleration(1), max_brake_acceleration(1) {

    if (!nh_.getParam("/coms/max_steering_angle", max_steering_angle)) {
      max_steering_angle = 1;    }
    if (!nh_.getParam("/coms/max_acceleration", max_acceleration)) {
      max_acceleration = 1;     }
    if (!nh_.getParam("/coms/max_brake_acceleration", max_brake_acceleration)) {
      max_brake_acceleration = 1;     }
    # 从系统中获得参数                                                                        
    input_status = nh_.subscribe<acronis_common::VehicleStatus>("input_topic", 1, &Interpreter::inputCallback, this);

    output_acceleration = nh_.advertise<geometry_msgs::AccelStamped>("output_acceleration", 100);
    output_steering = nh_.advertise<acronis_common::StampedFloat64>("output_steering", 100);
    # 这两个 pub 类型还不一样，一个是标准的 geometry_msgs/AcceleStamped 类型，一个是自定义的 acronis_common/StampedFloat64 类型 

  }
  ~Interpreter() {}


  void inputCallback (const acronis_common::VehicleStatus::ConstPtr& msg) {
    geometry_msgs::AccelStamped accel_msg;
    acronis_common::StampedFloat64 steering_msg;

    accel_msg.header = msg->header;
    steering_msg.header = msg->header;
    # 这两个 msg 的头文件是直接从接受到的 msg 中提取的                                                                    

    accel_msg.accel.linear.x = msg->accel_pedal_position * max_acceleration - msg->brake_position * max_brake_acceleration;
    steering_msg.value = msg->steering_wheel_position * max_steering_angle;
    # 填充两个 msg 
    # 加速度 msg 只有 x 方向的线加速度

    output_acceleration.publish(accel_msg);
    output_steering.publish(steering_msg);
    
  }

  
};

int main (int argc, char *argv[]) { 
  ros::init(argc, argv, "vehicle_status_interpreter");
  ros::NodeHandle nh("~");

  Interpreter intr(nh);

  ros::spin();


} 
```
