
## Acronis 项目 - Track 2 部分

### 研究动机
确保自动驾驶汽车的安全性是其上路和量产的必要条件。除了常规设备故障，来自黑客的恶意攻击也是必须要考虑的问题。自动驾驶汽车的智能化为恶意攻击者提供了更多的入侵接口，包括传感器数据篡改、自动驾驶系统劫持、汽车底层CAN-BUS入侵等，其中针对传感器数据的攻击是最简单、最直接的攻击方式。传感器攻击首先会影响到汽车的定位和感知功能，进而使自动驾驶系统产生错误的决策和控制命令，造成灾难性的后果。因此，如何检测和应对传感器攻击是自动驾驶汽车安全性方面必须要考虑的问题。

### 研究目标
本课题主要针对传感器攻击，设计安全定位机制，使得自动驾驶汽车可以及时检测到传感器数据异常，并作出恰当的应对措施，完成正确定位功能。安全定位机制应在仿真环境和真车环境中测试其效果。

### 研究内容
1. 模拟传感器攻击：在设计安全定位机制之前，首先需要在自动驾驶汽车中实现传感器攻击，即人为制造攻击环境以方便后续的测试工作。此部分研究内容不需要完全重现攻击者入侵真车的过程，只需要实现攻击效果即可。本课题采用的方法是在自动驾驶汽车的底层操作系统（ROS）中劫持与传感器信号有关的话题（topic），修改其上的数据。目前可以实现GPS信息的欺骗（spoofing）攻击和激光雷达（LIDAR）信息的重放（replay）攻击。
2. 传感器异常检测：本部分研究内容主要基于扩展卡尔曼滤波（EKF）和CUSUM检测算法实现。首先针对自动驾驶汽车硬件信息构建数学模型，其中状态变量选为汽车在二维平面上的位置、角度和速度，控制变量为汽车的加速度和前轮转角。在汽车行进过程中采用EKF对位置信息进行预测，并与传感器数据对比，累加信息残差（residual）如果超过一定阈值，则认定传感器数据异常，即CUSUM检测。此部分研究的关键点一方面在于如何设定警报阈值从而尽可能避免误报和漏报，一般需要对汽车正常行驶中的信息残差有基本的认知，以此为基准设定警报阈值。另外一方面在传感器较少时，如何基于矛盾数据确定哪个传感器受到攻击。
3. 攻击隔离：在检测出被攻击的传感器之后，需要丢弃其数据。因此要为每条传感器数据通道设计开关机制，只有正常数据可以到达EKF进行数据融合。可以采用两种隔离方案：一个方案是一旦确认数据异常则彻底切断数据通道，后续定位不再考虑该传感器数据;另一个是暂时切断数据通道，并持续检测数据的异常状态，若发现数据回复正常则继续使用。
4. 安全定位机制测试：分为仿真环境测试和真车测试两部分。仿真环境采用自动驾驶开源软件Autoware和机器人仿真软件Gazebo。由于Gazebo中并没有现成的自动驾驶测试环境，而是模块化的仿真元素如道路、树木、行人、楼房等，因此需要自行搭建尽量接近实际道路状况和汽车型号的仿真环境。真车方面以Toyota COMS电动汽车为基础车身，预留控制系统接口。真车测试环境为南洋理工大学自动驾驶测试中心(CETRAN)。

### 研究进展
目前已经在仿真环境中测试了安全定位机制的有效性，基本达到了预期的效果。真车测试正在进行中。



## 几个 branch 的内容

### feature/dbw

这是 Singpilot 交付时的 branch. 后续的 branch 都是在这个 branch 的基础上修改的。

### feature/dbw_simple_attack_demo

#### autoware 的修改

这个是正式运行时的 branch，目前可以检测 gps, lidar, actuator 攻击，actuator 攻击时可以辨认速度或转角被攻击，但只能发警告，没有对抗攻击的措施。

相比于  feature/dbw，有 3 处修改： 

1. 修改了 ndt_matching.cpp 文件 ，不再发布  /map  to  /base_link 的 TF。这一 TF 改由 EKF 发布

2. 修改了 pure_pursuit_core.cpp 文件，额外发布 /ctrl_cmd topic，类型为 autoware_msgs/ControlCommand ，结构为：

   ```bash
   float64 linear_velocity
   float64 steering_angle
   ```

   用作 EKF 的控制输入 

3. 在启动 autoware runtime manager 时，

   computing -- autoware_connector -- ndt_pose 这一项删掉，不需要在这里输出 current velocity, 后边由 EKF 发布。

以上是在 Autoware 中的修改，修改完源代码，编译之前，删除以下两个可执行文件

- devel / lib / ndt_localizer / ndt_matching 
- devel / lib / waypoint_follower / pure_pursuit  

然后，只编译 以下两个 ros package

- ndt_localizer 
- waypoint_follower

#### acronis 的修改

在 acronis 文件夹中，也切换到对应的 feature/dbw_simple_attack_demo branch.

在 acronis 根目录下，有两个文件：

- 1_start_kalman_filter.sh  ： 启动 resilient_ekf launch 文件

- 2_start_CUSUM_detector.sh  ： 启动 CUSUM detector

最后的 ros graph 如下：

![acronis_graph](pic/acronis_graph.png)



### feature/dbw_simple_attack_demo_test

这个 branch 是为了方便测试，在  feature/dbw_simple_attack_demo 基础上加入了攻击信号，以便测试检测效果。

![acronis_graph1](pic/acronis_graph1.png)

#### autoware 的修改

对 autoware 的修改只有一个

- 在 waypoint_follower package 中的 twist_filter launch 文件中，将输出 topic  twist_cmd remap 成 twist_cmd_raw

  ```xml
  <remap from="twist_cmd" to="twist_cmd_raw"/>
  ```

这个修改不需要重新编译，比较简单。

#### acronis 的修改

对ekfilter 中的 launch 文件进行修改

- 启动 gnss_attacker : 接收 gnss_pose，发布 fake_pose
- 启动 actuator_attacker : 接收 twist_cmd_raw，发布 twist_cmd
- remap  ekfilter 的如下 topic
  - subscribe  gnss_pose  --> fake_gnss
  - subscribe  twist_cmd --> twist_cmd_raw

另外，添加两个 sh 文件

- 3_start_gnss_attack.sh  : 启动 gnss 攻击，将 gnss 固定在当前位置
- 4_start_actuator_attack.sh ： 启动 actuator 攻击，每个控制命令的 steering angle - 2.0，即右转向。




## 编译 Autoware

首先依次安装

- Ubuntu 16.04

- ROS kinetic 
- Gazebo8
- Gazebo_ros_pkg

另外，安装以下依赖

```bash
sudo apt-get install ros-kinetic-nmea-msgs ros-kinetic-nmea-navsat-driver ros-kinetic-sound-play ros-kinetic-jsk-visualization ros-kinetic-grid-map ros-kinetic-gps-common
sudo apt-get install ros-kinetic-controller-manager ros-kinetic-ros-control ros-kinetic-ros-controllers ros-kinetic-gazebo8-ros-control ros-kinetic-joystick-drivers
sudo apt-get install libnlopt-dev freeglut3-dev qtbase5-dev libqt5opengl5-dev libssh2-1-dev libarmadillo-dev libpcap-dev gksu libgl1-mesa-dev libglew-dev python-wxgtk3.0
```

### 安装 ROS (kinetic)
([参考网址](http://wiki.ros.org/kinetic/Installation/Ubuntu))

1. Configure your Ubuntu repositories to allow "restricted," "universe," and "multiverse."
2. Setup your computer to accept software from packages.ros.org.

 `sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'`
3. Set up your keys

 `sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116`

4. Install

 `sudo apt-get update`

 `sudo apt-get install ros-kinetic-desktop-full`

5. Initialize rosdep: rosdep enables you to easily install system dependencies for source you want to compile

 `sudo rosdep init`

 `rosdep update`

6. Environment setup

 `echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc`

 `source ~/.bashrc`

7. Dependencies for building packages

 `sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential`

注意：上述安装默认 python 2，如果是系统默认 Python 3 的话，可以选择更改系统的默认版本。

### 编译 Autoware

到目前为止，仿真都是在 Jakub 的 develop branch 完成的。

Song zhiwei 的 branch 是  feature/ntu，与 develop 各有不同，但总的来说，有很多新东西。 

从 bitbucket 上边 下载 autoware-ntu-fork 版本的 Autoware，然后编译。

在编译过程中如果提示找不到 opencv-3.2.0-dev ，最简单的方法是在对应的 CMakeLists.txt 中修改那句话 REQUIRE opencv-3.2.0-dev，不再指定这个版本.

### 可能遇到的 bug

**如果实在解决不了 bug，可以重装系统，严格按照步骤重装软件 **

- 从bitbucket上git clone 上述repo有时会报错

```
acronis@acronis-G501VW:~$ git clone git@bitbucket.org:acronis16/autoware-ntu-fork.git
Cloning into 'autoware-ntu-fork'...
remote: Counting objects: 38381, done.
remote: Compressing objects: 100% (14087/14087), done.
packet_write_wait: Connection to 104.192.143.1 port 22: Broken pipe   
fatal: The remote end hung up unexpectedly
fatal: early EOF
fatal: index-pack failed
```

 此时做如下设置即可：

 `git config --global http.postBuffer 1048576000`

- 如果提示找不到 rosjava 相关的文件，可以装 rosjava，参考网址 
  http://wiki.ros.org/rosjava/Tutorials/kinetic/Source%20Installation

- 如果在编译某个package时报错，修改之后可以只编译那个 package，看能否运行，这样调试比较省时间。

- 如果在编译时提示某个package找不到，有两种解决方案：

  1. 从 github中下载对应的repo到src中，一起编译
  2. ros中，一般命名格式为 `ros-<version>-package`，例如要在kinetic ROS 系统中安装math_function package，则用命令：

  `sudo apt install ros-kinetic-math-function`

- 如果提示某个依赖找不到，也可以尝试直接把依赖去掉，例如提示`opencv-3.2.0-dev`找不到，则在CMakeList中删掉对应的依赖

- 如果提示 `std::accumulate` 找不到，则是因为没有添加相应的头文件，`#include <numveric>`

- 只安装 ros-desktop 版本，不要安装 full 版本。这样就不需要删除 gazebo7
- 装完 ros 之后，单独安装 gazebo 8，在安装后续 package 时，如果提示找不到 gazebo7 等字样，也要替换成 gazebo8 对应的 package

## 用 Docker 方式安装、运行 Autoware

内容来自 https://ai4sig.org/2018/07/docker-for-autoware/

亲测可行

1. 安装 Docker . 

2. 安装 nvidia-docker 1，还没有在 nvidia-docker 2 上测试.

   安装流程 https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(version-1.0)

3. 安装好 Docker 之后，下载 autoware 的 image

   ```bash
   docker pull autoware/autoware:1.7.0-kinetic
   ```

4. 下载 demo 用到的数据，实际上就是一个 rosbag，一个 data 里面包含了 pcd map 等。那些 launch 文件都包含了 autoware image 中，启动之后就可以查看了。

   ```bash
   $ mkdir ~/.autoware
   $ cd ~/.autoware
   $ wget http://db3.ertl.jp/autoware/sample_data/sample_moriyama_data.tar.gz
   $ wget http://db3.ertl.jp/autoware/sample_data/sample_moriyama_150324.tar.gz
   $ tar xf sample_moriyama_data.tar.gz
   $ tar xf sample_moriyama_150324.tar.gz
   $ rm sample_moriyama_data.tar.gz
   $ rm sample_moriyama_150324.tar.gz
   ```

5. 创建 Autoware 文件夹存放 docker 启动文件

   ```bash
   $ mkdir -p ~/Autoware/docker/generic
   ```

6. 在上书文件夹中存放如下 run.sh 文件

   ```
   #!/bin/sh
   
   XSOCK=/tmp/.X11-unix
   XAUTH=/home/$USER/.Xauthority
   SHARED_DIR=/home/autoware/.autoware
   HOST_DIR=/home/$USER/.autoware
   
   nvidia-docker run \
       -it --rm \
       --volume=$XSOCK:$XSOCK:rw \
       --volume=$XAUTH:$XAUTH:rw \
       --volume=$HOST_DIR:$SHARED_DIR:rw \
       --env="XAUTHORITY=${XAUTH}" \
       --env="DISPLAY=${DISPLAY}" \
       -u autoware \
       --privileged -v /dev/bus/usb:/dev/bus/usb \
       --net=host \
       autoware/autoware:1.7.0-kinetic
   ```

7. 运行上述 run.sh 文件，报错的话，可能需要 sudo

   ```bash
   $ sh run.sh
   ```

   此时就进入了 Autoware 的环境，其中也设置了 共享文件夹 .autoware，可以与 host 共享文件。

   后边的工作就是普通 autoware 流程一样了，运行 rosbag ，加载 launch , 打开 rviz 等。

   

   注意：在按照 Autoware 官网方式安装时报错了，当时是 nvidia-docker 2 。在运行 Autoware 的 rviz 时，可能会有关于 libGL 的错误，很可能是用的 nvidia-docker 中没有包含 OpenGL。

   为了避免这个问题，才用了上述网页中的安装方式

## 关于acronis repo编译

1. 下载repo: `git clone git@bitbucket.org:acronis16/acronis.git`

2. 在根目录下有三个可执行文件，`make_with_compile_cmds`, `mape_build.sh` 和 `setup_vcan.sh`，第三个文件主要用来设置 CAN 接口的，不用管。

   -`mape_build.sh` 文件中的内容：

   ```
   #!/bin/bash
   # ./make_with_compile_cmds -DCATKIN_WHITELIST_PACKAGES="tablet_socket_msgs;autoware_msgs"
   ./make_with_compile_cmds -DCATKIN_WHITELIST_PACKAGES="mape_msg;acronis_common"
   ./make_with_compile_cmds -DCATKIN_WHITELIST_PACKAGES="mape_misc;mape_monitor;mape_analyzer;can_simulator"
   ```

   这个文件是调用 `make_with_compile_cmds` 文件，执行带参数的 catkin_make 编译。

   命令： `catkin_make -DCATKIN_WHITELIST_PACKAGES="package1;package2"` 是编译指定的包 package1, package2。

   如果 `-DCATKIN_WHITELIST_PACKAGES=""` 则编译所有package

   这样可以设定编译顺序，例如 `acronis_common`会被其他package用到，要先编译它。

   -`make_with_compile_cmds` 文件中的内容：
   ```
    #!/bin/bash
    catkin_make -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ${1}
   ```
   **这两个文件主要工作就是指定了编译的package，为什么分开写呢，应该可以合并成一个的吧！**

   这三个文件主要是 can_bus 方面要用，我们不需要管。

3. 用普通的 catkin_make 编译即可。一般来说，要先编译 acronis_common 和 prius_msgs

4. 如果报错了，提示 registration 找不到，可能是由于 acronis 中 autoware 下面的 acronis_ndt_localizer 调用的文件没找到。

   - 如果我们在实验中用 Autoware 自身的 ndt，那么可以不编译 acronis 中的 autoware package，用

   `catkin_make -DCATKIN_BLACKLIST_PACKAGES="acronis_ndt_localizer"` 不编译此 package.

   - 如果必须要用，可以把 autoware branch 切换到 master，删掉 devel 和 build 文件夹，重新编译一遍 autoware.然后删掉 acronis 中的 devel 和 build 文件夹，重新编译。应该就可以了。也就是说，在 autoware 的 master 中是有这个文件的。

## 安装 julia package

由于node resillient ekf是用 julia 语言编写的，所以要先安装 julia. 参考：https://julialang.org/downloads/platform.html

还要安装本项目中用到的 Julia package：
1. 运行 julia: (可以通过运行 ekfilter.jl 程序查看缺少哪些package，下面直接列出了需要的package，及在julia中的安装方式)
2. `Pkg.add("RobotOS")`
3. `Pkg.add("ODE")`

可能存在的问题：
1. 尽管在 python 中安装了 rospy，但是 Julia 还是提示找不到 rospy，可能使用的不是同一个 python 版本。解决办法是将 julia 用的 python 版本重新设置一下，对应到正确的 python 版本。在 Julia 中输入：
```julia
ENV["PYTHON"] = "... path of the python program you want ..."
Pkg.build("PyCall")
```

## 仿真车辆信息
- max_steering_angle: 0.6458               
- max_acceleration: 4.0422                  
- max_brake_acceleration: 8.0808            
- max_speed: 37.998                        
- wheel_base: 2.86

启动系统时调用
```xml
<rosparam command="load" file="$(find acronis_common)/config/vehicle.yaml" /> 
```

## topic 及相关数据

### rosbag 样本 1 
filtered 数据产生背景： 
- ndt 来时更新 ndt ekf，其中预测之后的角度由 imu 代替。
- gnss 更新同 ndt。
- imu 来时直接更新角度，没有预测、融合的步骤
- combine 更新同 ndt

bag 中信息
```
path:        four_odom_msg.bag
version:     2.0
duration:    18.2s
start:       Jan 01 1970 08:12:42.39 (2562.39)
end:         Jan 01 1970 08:13:00.58 (2580.58)
size:        1004.8 KB
messages:    1289
compression: none [2/2 chunks]
types:       nav_msgs/Odometry [cd5e73d190d741a2f92e81eda573aca7]
topics:      /odom/fake/gnss    18 msgs    : nav_msgs/Odometry
             /odom/fake/imu    536 msgs    : nav_msgs/Odometry
             /odom/fake/ndt     91 msgs    : nav_msgs/Odometry
             /odom/filtered    644 msgs    : nav_msgs/Odometry

```
- gnss 频率 1 Hz
- ndt 频率 5 Hz
- imu 频率 30 Hz

#### nav_msgs/Odometry 数据结构
```
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
string child_frame_id
geometry_msgs/PoseWithCovariance pose
  geometry_msgs/Pose pose
    geometry_msgs/Point position
      float64 x
      float64 y
      float64 z
    geometry_msgs/Quaternion orientation
      float64 x
      float64 y
      float64 z
      float64 w
  float64[36] covariance
geometry_msgs/TwistWithCovariance twist
  geometry_msgs/Twist twist
    geometry_msgs/Vector3 linear
      float64 x
      float64 y
      float64 z
    geometry_msgs/Vector3 angular
      float64 x
      float64 y
      float64 z
  float64[36] covarianc
```

#### 分析 covariance


```python
import rospy
import rosbag
from nav_msgs.msg import Odometry
```


```python
bag = rosbag.Bag("four_odom_msg.bag")
```


```python
ndt = []
gnss = []
imu = []
filtered = []
```


```python
for topic, msg, t in bag.read_messages():
    if topic == '/odom/fake/ndt':
        ndt.append(msg)
    elif topic == '/odom/fake/gnss':
        gnss.append(msg)
    elif topic == '/odom/fake/imu':
        imu.append(msg)
    elif topic == '/odom/filtered':
        filtered.append(msg)
```


```python
ndt[45]
```




    header: 
      seq: 223
      stamp: 
        secs: 2571
        nsecs: 237000000
      frame_id: "/map"
    child_frame_id: "base_link"
    pose: 
      pose: 
        position: 
          x: 13.7646923065
          y: 21.4660491943
          z: 0.0735330581665
        orientation: 
          x: 0.00335164762546
          y: -0.00442257809863
          z: 0.708011481634
          w: 0.706179119725
      covariance: [1.0, 6.93475836088364e-310, 6.95297061405e-310, 6.9529706140601e-310, 1.17060594e-316, 0.0, 9.24e-322, 1.0, 0.0, 0.0, 9.4e-322, 6.93475952855166e-310, 6.952970614068e-310, 6.93475949302637e-310, 1.0, 6.93475952850937e-310, 6.95297061407e-310, 6.93475941915046e-310, 4e-323, 6.93475949302637e-310, 2.5e-323, 1.0, 5e-324, 6.93475952850937e-310, 0.0, 6.9347590164813e-310, 1e-322, 0.0, 1.0, 6.9529706140609e-310, 2.121995791e-314, 6.9529706140601e-310, 4.38894202e-315, 2.132713e-317, 2.1219957905e-314, 1.0]
    twist: 
      twist: 
        linear: 
          x: 0.0
          y: 0.0
          z: 0.0
        angular: 
          x: 0.0
          y: 0.0
          z: 0.0
      covariance: [-1.0, 6.93475836088364e-310, 6.95297061405e-310, 6.9529706140601e-310, 1.17060594e-316, 0.0, 9.24e-322, -1.0, 0.0, 0.0, 9.4e-322, 6.93475952855166e-310, 6.952970614068e-310, 6.93475949302637e-310, -1.0, 6.93475952850937e-310, 6.95297061407e-310, 6.93475941915046e-310, 4e-323, 6.93475949302637e-310, 2.5e-323, -1.0, 5e-324, 6.93475952850937e-310, 0.0, 6.9347590164813e-310, 1e-322, 0.0, -1.0, 6.9529706140609e-310, 2.121995791e-314, 6.9529706140601e-310, 4.38894202e-315, 2.132713e-317, 2.1219957905e-314, -1.0]



**ndt[45] covariance**
```
[1.0,          6.93e-310,    6.95e-310,    6.95e-310,    1.17e-316,    0.0, 
 9.24e-322,    1.0,          0.0,          0.0,          9.4e-322,     6.93e-310, 
 6.95e-310,    6.93e-310,    1.0,          6.93e-310,    6.95e-310,    6.93e-310, 
 4e-323,       6.93e-310,    2.5e-323,     1.0,          5e-324,       6.93e-310, 
 0.0,          6.93e-310,    1e-322,       0.0,          1.0,          6.95e-310, 
 2.12e-314,    6.95e-310,    4.38e-315,    2.13e-317,    2.12e-314,    1.0]
```


```python
gnss[9]
```




    header: 
      seq: 46
      stamp: 
        secs: 2572
        nsecs:         0
      frame_id: "map"
    child_frame_id: "base_link"
    pose: 
      pose: 
        position: 
          x: 13.8994674802
          y: 24.3613905552
          z: 0.0132512470938
        orientation: 
          x: 0.0
          y: 0.0
          z: 0.0
          w: 1.0
      covariance: [1.0, 6.9347187663205e-310, 6.9532407763846e-310, 6.9532407763947e-310, 1.3776297e-316, 0.0, 9.24e-322, 1.0, 0.0, 0.0, 9.4e-322, 6.9347199339885e-310, 6.9532407764026e-310, 6.9347198984632e-310, 1.0, 6.9347199339462e-310, 6.95324077640458e-310, 6.9347198245873e-310, 4e-323, 6.9347198984632e-310, 2.5e-323, 1.0, 5e-324, 6.9347199339462e-310, 0.0, 6.93471942191813e-310, 1.04e-322, 0.0, 1.0, 6.9532407763955e-310, 4.2081641236e-314, 6.9532407763947e-310, 4.38894202e-315, 2.132713e-317, 2.1219957905e-314, 1.0]
    twist: 
      twist: 
        linear: 
          x: 0.0
          y: 0.0
          z: 0.0
        angular: 
          x: 0.0
          y: 0.0
          z: 0.0
      covariance: [-1.0, 6.9347187663205e-310, 6.9532407763846e-310, 6.9532407763947e-310, 1.3776297e-316, 0.0, 9.24e-322, -1.0, 0.0, 0.0, 9.4e-322, 6.9347199339885e-310, 6.9532407764026e-310, 6.9347198984632e-310, -1.0, 6.9347199339462e-310, 6.95324077640458e-310, 6.9347198245873e-310, 4e-323, 6.9347198984632e-310, 2.5e-323, -1.0, 5e-324, 6.9347199339462e-310, 0.0, 6.93471942191813e-310, 1.04e-322, 0.0, -1.0, 6.9532407763955e-310, 4.2081641236e-314, 6.9532407763947e-310, 4.38894202e-315, 2.132713e-317, 2.1219957905e-314, -1.0]



**gnss[9] covariance**
```
[1.0,           6.93e-310,      6.95e-310,      6.95e-310,       1.37e-316,      0.0, 
 9.24e-322,     1.0,            0.0,            0.0,             9.4e-322,       6.93e-310, 
 6.95e-310,     6.93e-310,      1.0,            6.93e-310,       6.95e-310,      6.93e-310, 
 4e-323,        6.93e-310,      2.5e-323,       1.0,             5e-324,         6.93e-310, 
 0.0,           6.93e-310,      1.04e-322,      0.0,             1.0,            6.95e-310, 
 4.2e-314,      6.95e-310,      4.38e-315,      2.13e-317,       2.12e-314,      1.0]
```


```python
imu[250]
```




    header: 
      seq: 1301
      stamp: 
        secs: 2570
        nsecs: 890000000
      frame_id: "base_link"
    child_frame_id: "base_link"
    pose: 
      pose: 
        position: 
          x: 0.0
          y: 0.0
          z: 0.0
        orientation: 
          x: 0.000450775692344
          y: -0.000446700750595
          z: 0.709267821111
          w: 0.704938830819
      covariance: [-1.0, 6.93054181524847e-310, 5.593e-321, 6.93054029719645e-310, 6.93054190919234e-310, 6.95253205377854e-310, 6.95253205377834e-310, -1.0, 6.9525320537888e-310, 2.1345187e-317, 2.084309e-317, 6.95253205377854e-310, 1.119817494e-315, 1.7497147e-317, -1.0, 6.9525320537892e-310, 6.93054029719645e-310, 6.93054029453443e-310, 6.95253205377834e-310, 6.9525320537884e-310, 1.0246977e-316, -1.0, 9.24e-322, 6.95253205377893e-310, 4.38894202e-315, 0.0, 2.1219957905e-314, 6.93054192471074e-310, -1.0, 6.9305418891961e-310, 0.0, 6.93054192466845e-310, 6.9525320537983e-310, 6.93054181530954e-310, 5.4e-323, -1.0]
    twist: 
      twist: 
        linear: 
          x: 0.0
          y: 0.0
          z: 0.0
        angular: 
          x: -0.000121685930041
          y: 0.00230024643166
          z: 0.0384170896223
      covariance: [-1.0, 6.93054181524847e-310, 5.593e-321, 6.93054029719645e-310, 6.93054190919234e-310, 6.95253205377854e-310, 6.95253205377834e-310, -1.0, 6.9525320537888e-310, 2.1345187e-317, 2.084309e-317, 6.95253205377854e-310, 1.119817494e-315, 1.7497147e-317, -1.0, 6.9525320537892e-310, 6.93054029719645e-310, 6.93054029453443e-310, 6.95253205377834e-310, 6.9525320537884e-310, 1.0246977e-316, -1.0, 9.24e-322, 6.95253205377893e-310, 4.38894202e-315, 0.0, 2.1219957905e-314, 6.93054192471074e-310, -1.0, 6.9305418891961e-310, 0.0, 6.93054192466845e-310, 6.9525320537983e-310, 6.93054181530954e-310, 5.4e-323, -1.0]



**imu[250] covariance**
```
[-1.0,         6.93e-310,        5.59e-321,       6.93e-310,         6.93e-310,      6.95e-310, 
 6.95e-310,    -1.0,             6.95e-310,       2.13e-317,         2.08e-317,      6.95e-310, 
 1.11e-315,    1.74e-317,        -1.0,            6.95e-310,         6.93e-310,      6.93e-310, 
 6.95e-310,    6.95e-310,        1.02e-316,       -1.0,              9.24e-322,      6.95e-310, 
 4.38e-315,    0.0,              2.12e-314,       6.93e-310,         -1.0,           6.93e-310, 
 0.0,          6.93e-310,        6.95e-310,       6.93e-310,         5.4e-323,       -1.0]
```

**imu 的负的 covariance 注定了不能参与预测和融合，只能直接更新。**


```python
filtered[300]
```




    header: 
      seq: 1517
      stamp: 
        secs: 2570
        nsecs: 637000000
      frame_id: "map"
    child_frame_id: "base_link"
    pose: 
      pose: 
        position: 
          x: 13.7701794003
          y: 19.0954455259
          z: 0.0404546078072
        orientation: 
          x: 0.000394216417711
          y: -0.000380542888457
          z: 0.708530174409
          w: 0.705680304198
      covariance: [0.43812530642303754, -0.007504178107817448, -1.506040370710083e-05, -1.531331131239851e-05, 6.0780547131368964e-06, 0.0009307766229370187, -0.007504178107817448, 0.5207999596766358, 0.0001130414255235921, 4.0946944820871345e-05, -1.904091395935011e-05, 0.007818595409948559, -1.5060403707100869e-05, 0.00011304142552359212, 0.4305925602465223, 0.001297240265142638, 0.0080442227794455, 1.5359009506212766e-05, -1.531331131239852e-05, 4.094694482087134e-05, 0.0012972402651426382, 0.006251686544889404, -0.001183894397906277, 1.481155834769183e-05, 6.078054713136928e-06, -1.90409139593501e-05, 0.008044222779445496, -0.0011838943979062767, -0.004261553290521786, -6.008411451997989e-06, 0.0009307766229370169, 0.007818595409948555, 1.535900950621274e-05, 1.4811558347691828e-05, -6.0084114519979624e-06, 0.0024411288058794386]
    twist: 
      twist: 
        linear: 
          x: 0.0
          y: 0.0
          z: 0.0
        angular: 
          x: 0.0
          y: 0.0
          z: 0.0
      covariance: [-1.0, 6.93475836088364e-310, 6.95297061405e-310, 6.9529706140601e-310, 1.17060594e-316, 0.0, 9.24e-322, -1.0, 0.0, 0.0, 9.4e-322, 6.93475952855166e-310, 6.952970614068e-310, 6.93475949302637e-310, -1.0, 6.93475952850937e-310, 6.95297061407e-310, 6.93475941915046e-310, 4e-323, 6.93475949302637e-310, 2.5e-323, -1.0, 5e-324, 6.93475952850937e-310, 0.0, 6.9347590164813e-310, 1e-322, 0.0, -1.0, 6.9529706140609e-310, 2.121995791e-314, 6.9529706140601e-310, 4.38894202e-315, 2.132713e-317, 2.1219957905e-314, -1.0]



**filtered[300] covariance**
```
[0.43,         -0.0075,       -1.50e-05,        -1.53e-05,         6.07e-06,        0.00093,
 -0.0075       0.52,          0.000113,         4.09e-05,          -1.9e-05,        0.00781, 
 -1.50e-05,    0.000113,      0.430,            0.001297,          0.008044,        1.53e-05, 
 -1.53e-05,    4.09e-05,      0.001297,         0.006251,          -0.00118,        1.48e-05,
 6.078e-06,    -1.9e-05,      0.0080,           -0.0011,           -0.00426,        -6.00e-06,  
 0.00093,      0.00781,       1.53e-05,         1.48e-05,          -6.00e-06,       0.0024411]
```

#### 分析 msgs 产生时间与到达时间之间的延迟
 /odom/fake/ndt, /odom/fake/gnss, /odom/fake/imu


```python
import rospy
import rosbag
from nav_msgs.msg import Odometry
```


```python
bag = rosbag.Bag('four_odom_msg.bag')
ndt = []
gnss = []
imu = []
for topic, msg, t in bag.read_messages():
    if topic == '/odom/fake/ndt':
        ndt.append([t.to_sec(), msg.header.stamp.to_sec()])
    elif topic == '/odom/fake/gnss':
        gnss.append([t.to_sec(), msg.header.stamp.to_sec()])
    elif topic == '/odom/fake/imu':
        imu.append([t.to_sec(), msg.header.stamp.to_sec()])
```


```python
[i[0] - i[1]  for i in ndt]  # ndt 从产生到接受到延迟约为 0.25s
```




    [0.2480000000000473,
     0.2699999999999818,
     0.24699999999984357,
     0.2480000000000473,
     0.27200000000038926,
     0.25900000000001455,
     0.26099999999996726,
     0.27599999999983993,
     0.2919999999999163,
     0.30700000000024374,
     0.2849999999998545,
     0.26900000000023283,
     ...]




```python
[i[0] - i[1]  for i in gnss]  # gnss 延迟可忽略
```




    [0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0010000000002037268,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0]




```python
[i[0] - i[1]  for i in imu]  # imu 延迟可忽略
```




    0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.007000000000061846,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0,
     0.0019999999999527063,
     ...]





## 拆解 autoware 系统

### autoware rosgraph

Quick start demo 中的 [rosgraph](pic/rosgraph_until_motion1.png)

### run Autoware

[rosgraph_initial](pic/rosgraph_initial1.png)

用 ./run 方式启动 Autoware，实际上 run 文件里面除了一些 terminal 类型识别之类的命令，关键的只有两个
1. roscore
2. rosrun runtime_manager runtime_manager_dialog.py

通过上述两个命令启动 autoware 效果与 ./run 是完全一样的，而且还没有了输入 password 的限制。

所以这里的关键就是 runtime_manager_dialog.py 文件。

如果只启动 autoware，不在里面设置各种 launch 文件，则实际上只启动了一个 runtime_manager node，加载很多 yaml 文件，然后 Pub configuration ，以备 launch 启动的其他节点使用。

![autoware2](pic/autoware2.png)

其中的 .yaml 文件在 runtime_manager_dialog.py 同一目录下。

![autoware1](pic/autoware1.png)



autoware 提供了几个 launch 文件，在 quick_start tab 加载，可以很方便的批量启动特定功能群的节点。我们也可以用后边的几个 tab，只启动需要的 node.

### my_map.launch

[rosgraph_until_map](pic/rosgraph_until_map1.png)

启动了如下节点

- /points_map_loader: 主要 Pub /points_map
- /vector_map_loader: 主要 pub /vector_map
- /world_to_map: 都是与 tf 相关的
- /map_to_mobility

目前来看，其中最关键的是 /points_map_loader，这也是 Jakub 的系统中唯一用到的 node。

如下两种启动方式是等价的：

1. 通过 autoware -- Map tab 启动 /points_map_loader 加载 .pcd 文件

   ![autoware_map1](pic/autoware_map1.png)

2.  通过 roslaunch 启动 /points_map_loader 并加载 .pcd 文件

   ```xml
     <node pkg="map_file" type="points_map_loader" name="points_map_loader" args="noupdate /home/acronis/acronis/data/acrocity-map-filtered.pcd"/>
   ```

   当然也可以通过 rosrun pkg node 的命令行方式启动。

实际上，通过 autoware 信息窗口就可以看出他们的等价。当通过 autoware -- map 加载 pcd 文件时，窗口信息如下：

![autoware_map2](pic/autoware_map2.png)

可以看到其中的提示，本质上就是启动了 map_file pkg 中的 points_map_loader node，并给定参数。

### run gazebo

在 simulation tab 左下角有个 gazebo 按钮，实际上对应了与 runtime_manager_dialog.py 同一目录下的 gazebo.sh 批处理文件，内容如下：

```bash
#!/bin/sh

roslaunch catvehicle catvehicle_skidpan.launch &
gzclient &
roslaunch point_cloud_converter point_cloud_converter.launch &
roslaunch laser_scan_converter laser_scan_converter.launch &
roslaunch twist_cmd_converter twist_cmd_converter.launch &

while :; do sleep 10; done

# EOF
```

实际上就是通过 roslaunch 启动了 gazebo 仿真环境。

为了调用 Jakub 的仿真环境，将 gazebo.sh 中的内容改成如下形式

```bash
#!/bin/sh

roslaunch acronis_world launch_gazebo.launch
```

其中的 Launch 文件就是 Jakub launch 中启动 gazebo 的部分，如下

```xml
<?xml version="1.0"?>
<launch>
<include file="$(find gazebo_ros)/launch/empty_world.launch">
   <arg name="verbose" value="true"/>
   <arg name="world_name" value="$(find acronis_world)/worlds/acrocity_traffic.world"/>
</include>
<rosparam command="load" file="$(find acronis_common)/config/vehicle.yaml" />
</launch>
```

### my_sensing.launch

[rosgraph_until_sensing](pic/rosgraph_until_sensing1.png)

主要是启动摄像头和 velodyne 相关的节点，但是提示找不到摄像头对应的节点

```
 can't locate node [grasshopper3_camera] in package [autoware_pointgrey_drivers]
```

因此我将 camera 相关的 include tag comment 掉，只启动了 velodyne ，主要 Pub /points_raw

![my_sensing1](pic/my_sensing1.png)



- 仿真中替换为 point_cloud_converter 节点

这个从 velodyne 到 /points_raw 的流程对于硬件可能适用，但是用 gazebo 仿真时(包括 gazebo 自带的 catvehicle 仿真环境) 车身直接给出了 /vehicle/points_raw ，不再显示 velodyne 细节。但是这个 /vehicle/points_raw 的 type 是 sensor_msgs/PointCloud ，结构如下：

```
# 每个时刻，每个channel 都有一堆点
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
geometry_msgs/Point32[] points
  float32 x
  float32 y
  float32 z
sensor_msgs/ChannelFloat32[] channels
  string name
  float32[] values
```

但是后续环节需要的 /point_raw 的 type 应为 sensor_msgs/PointCloud2，结构如下：

```xml
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
uint32 height
uint32 width
sensor_msgs/PointField[] fields
  uint8 INT8=1
  uint8 UINT8=2
  uint8 INT16=3
  uint8 UINT16=4
  uint8 INT32=5
  uint8 UINT32=6
  uint8 FLOAT32=7
  uint8 FLOAT64=8
  string name
  uint32 offset
  uint8 datatype
  uint32 count
bool is_bigendian
uint32 point_step
uint32 row_step
uint8[] data
bool is_dense
```

因此在 gazebo 仿真中，需要从车辆发出的 PointCloud 转换到 PointCloud2 类型。这里就额外定义了一个节点 /point_cloud_converter，用 launch 文件启动

```xml
<launch>
	<node pkg="point_cloud_converter" name="point_cloud_converter" type="point_cloud_converter_node" >
		<remap from="points_in" to="/catvehicle/points_raw"/>  # 接收
		<remap from="points2_out" to="/points_raw" />          # 发出
	</node>
</launch>
```

不管是内置的 catvehicle 还是 Jakub 的仿真中，都需要这个环节得到最终的 /points_raw。

实际操作中，可以把 /point_cloud_converter 节点放入 launch_gazebo.launch 文件中，使其随 gazebo 一起启动，形式如下：

- gazebo.sh 文件

```bash
#!/bin/sh

roslaunch acronis_world launch_gazebo.launch
```

- launch_gazebo.launch 文件

```xml
<?xml version="1.0"?>
<launch>
    <include file="$(find gazebo_ros)/launch/empty_world.launch">
        <arg name="verbose" value="true"/>
        <arg name="world_name" value="$(find acronis_world)/worlds/acrocity_traffic.world"/>
    </include>
    
    <rosparam command="load" file="$(find acronis_common)/config/vehicle.yaml" />

    <node pkg="point_cloud_converter" name="point_cloud_converter" type="point_cloud_converter_node" >
        <remap from="points_in" to="/prius/center_laser/points"/>
        <remap from="points2_out" to="/points_raw" />
    </node>    

</launch>
```

### my_localization.launch

 [rosgraph_until_localization](pic/rosgraph_until_localization1.png)

从 quick_start 上边下载的文件有个错误

```xml
  <include file="$(find lidar_localizer)/launch/ndt_matching.launch">
    <arg name="get_height" value="$(arg get_height)" />
  </include>
```

上边的 lidar_localizer 已经改名成 ndt_localizer 了。

这个 launch 的核心是启动了 ndt_matching node.

前边的 /points_raw 经过 /voxel_grid_filter 得到 /filtered_points，送到 ndt_matching 中；

另外，/nmea2tfpose 节点发出 /gnss_pose 信息也送到 ndt_matching。

然后 ndt_matching 发出 /localizer_pose, /ndt_pose 等位置信息。



- gazebo 中

已经有了 /points_raw topic，接下来启动 sensing  -- voxel_grid_filter 节点对其 filtering，得到 /filtered_points



### my_detection.launch

提示错误

```
ResourceNotFound: vision_dpm_ttic_detect
```

没有任何新节点启动

### my_mission_planning.launch

[rosgraph_until_mission](pic/rosgraph_until_mission1.png)

提示错误

```
ERROR: invalid message type: autoware_msgs/ConfigWaypointLoader.
```

在当前系统中，确实没有这个 msg。

主要是 waypoints 相关的节点

- /waypoints_loader: pub topic /lane_waypoints_array
- 经过节点 /lane_rule，得到不同信号灯下的 waypoints_array，然后在节点 /lane_stop 中结合 /light_color 确定 /traffic _waypoints_array，然后根据 /current_pose 在 /lane_select 中计算得到 /base_waypoints 和 /closest_waypoints

### my_motion_planning.launch

[rosgraph_until_motion](pic/rosgraph_until_motion1.png)

主要是避碰和最终的控制

- 上边得到 /base_waypoints 和 /closest_waypoints 送入节点 /obstacle_avoid，pub /safety_waypoints
- 送到 /velocity_set 节点，添加速度信息，最后就的到了 /final_waypoints

- 在 pure_pursuit 中结合当前位置、速度和 final_waypoints，pub /twsit_raw 和 /ctrl_cmd
-  /twist_raw 由节点 /twist_filter 过滤一下，得到 /twist_cmd，然后进入更底层的控制



至此得到了完整的 rosgraph. 注意，这里是 autoware 在 quick_start demo 情况下的 graph， 与我们的 gazebo 闭环仿真不同，与实车仿真也不同。区别之一是： gazebo 仿真中 /lane_waypoints_array 直接送到了 pure_pursuit 节点，而 autoware 默认的是要经过判断 traffic light 等若干步的分析才能得到 /final_waypoints 送到 pure_pursuit.

topic /lane_waypoints_array 的 type 为 autoware_msgs/LaneArray，其格式为

```
lane[] lanes   # 是 lane 数值，其中每个元素都是 lane 类型的，其格式如下
```

topic /final_waypoints 的 type 为 autoware_msgs/lane，其格式为

```
Header header
int32 increment
int32 lane_id
waypoint[] waypoints
```

autoware_msgs/waypoint type 的格式如下

```
# global id
int32 gid 
# local id
int32 lid
geometry_msgs/PoseStamped pose
geometry_msgs/TwistStamped twist
dtlane dtlane
int32 change_flag
WaypointState wpstate
```

## 拆解 Jakub 构造的闭环仿真系统的

### 整个系统的 ROS graph 

[rosgraph](pic/rosgraph1.png)

![rosgraph1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/rosgraph1.png)

### 最关键的 launch 文件

```xml
<?xml version="1.0"?>
<launch>

  <!-- run AUTOWARE first-->

  <!-- run EKF for Localization -->    
  <include file="$(find resilient_kf)/launch/resilient_kf.launch" />
  
  
  <!-- 加载点云地图 -->
  <node pkg="map_file" type="points_map_loader" name="points_map_loader" args="noupdate /home/acronis/acronis/data/acrocity-map-filtered.pcd"/>
 points map loader: 调用了 autoware 的 map_file package 加载 points map. 输入 pcd 文件的路径，然后 Publish topic /points_map (type: sensor_msgs::PointCloud2)，在 rviz 中可以呈现点云地图。
    
如果要用 rosrun 单独运行这个 Node，可以用如下命令：    
rosrun map_file points_map_loader noupdate /home/acronis/acronis/data/acrocity-map-filtered.pcd
# 后边这两个 arg 就作为points_map_loader.cpp 中 main 函数的 argv 送进去了
  
  
  <!-- 启动 ndt_matching node -->
  <include file="$(find acronis_ndt_localizer)/launch/ndt_matching.launch">
    <arg name="publish_map_base_tf" default="false" />
  </include>
  对比 acronis ndt 与 autoware 原版 ndt launch 文件，没有本质的区别， acornis ndt 中额外的设置默认是不启动的
  
  <!-- 这几个参数本来在 autoware 启动时已经设定，应该是 lidar 相对于 base_link 的位置，方便 ndt 准确定位。这里是修改了其中x,y,z的值 -->
  <param name="/tf_x" value="2" />
  <param name="/tf_y" value="1.5" />
  <param name="/tf_z" value="1.8" />  

  <!-- Control -->
  <!-- waypoint loader -->
  <include file="$(find waypoint_maker)/launch/waypoint_loader.launch">
    <arg name="multi_lane_csv" default="/home/acronis/acronis/data/half_circle.csv" />
  </include>
  这个 launch 文件启动了两个 node: waypoint_loader 和 waypoint_marker_publisher.
  - waypoint_loader 调用 csv 文件，publish /lane_waypoints_array，这个 topic 同时被 /pure_pursuit 和 /waypoint_marker_publisher 接收。在 /pure_pursuit 中作为目标点来跟踪.
  - waypoint_marker_publisher 将接收到的 /lane_waypoints_array 转化成 /global_waypoint_mark，由 rviz 接收并显示。

  <!-- tracking waypoints -->
  <include file="$(find acronis_waypoint_follower)/launch/pure_pursuit.launch"/>
  这个 pure_pursuit launch 文件 与 autoware 中的 pure_pursuit launch 文件完全相同。都是启动 pure_pursuit node
    
  <!-- filtering twist command -->
  <node pkg="acronis_waypoint_follower" type="twist_filter" name="twist_filter" output="log"/>
  与 autoware 中的文件没有任何区别
    
 
  <!-- publish vehicle ground truth >
  <node pkg="acronis_world" name="vehicle_status_interpreter" type="vehicle_status_interpreter" >
    <remap from="~input_topic" to="/prius/status"/>
    <remap from="~output_acceleration" to="/coms/accel" />
    <remap from="~output_steering" to="/coms/steering" />
  </node-->
  本来这个 node 是提供车辆实际的控制信号的，后来改成估计控制信号了。这个 node 就废了。  
    

  <!-- GAZEBO -->
  <arg name="model" default="$(find prius_description)/urdf/prius.urdf"/>
  <param name="robot_description" textfile="$(arg model)"/>
  <!--<node name="spawn_urdf" pkg="gazebo_ros" type="spawn_model" args="-param robot_description -urdf -x 0 -y 3 -z 0.5 -model prius"/>-->
  设定 robot_description。实际上这一部分调用 urdf 只是为了在 rviz 中显示 robotmodel，被没有被 gazebo 调用，prius model 已经包含在了 world 中

  <!-- 启动 world，并设定车辆参数 -->
  <include file="$(find gazebo_ros)/launch/empty_world.launch">
   <arg name="verbose" value="true"/>
   <arg name="world_name" value="$(find acronis_world)/worlds/acrocity_traffic.world"/>
  </include>
  <rosparam command="load" file="$(find acronis_common)/config/vehicle.yaml" />

  <!-- gazebo 直接发出来的 /prius/center_laser/points topic 是 sensor_msgs/PointCloud 类型，转化之后的 /points_raw 是 sensor_msgs/PointCloud2 类型 -->
  <node pkg="point_cloud_converter" name="point_cloud_converter" type="point_cloud_converter_node" >
    <remap from="points_in" to="/prius/center_laser/points"/>
    <remap from="points2_out" to="/points_raw" />
  </node>

 <!-- Downsample -->
 <!-- 实时采集的 points 太多了，需要 downsample。这里直接调用 autoware 的文件，实际上是启动了 node /voxel_grid_filter，接收上边转换之后的 /points_raw，发送出 /filtered_points -->
  <include file="$(find points_downsampler)/launch/points_downsample.launch" > </include>

  <node pkg="robot_state_publisher" type="robot_state_publisher" name="robot_state_publisher" >
  </node>

  <!-- 速度命令转化成油门、刹车等底层命令 -->
  <node pkg="acronis_world" type="acceleration_ecu.py" name="acceleration_ecu">
    <remap from="input_topic" to="/twist_cmd" />
    <remap from="output_topic" to="/prius_raw" />
  </node>

    
  <!-- run rviz -->    
  <arg name="rvizconfig" default="$(find acronis_launcher)/rviz/localization_test.rviz" />
  <node name="rviz" pkg="rviz" type="rviz" args="-d $(arg rvizconfig)" required="false" />
  args: -d 后跟 display config 文件，即 .rviz 文件
</launch>
```

### 几条主线

1. pcd 地图文件已经存放好，用 map_file pkg 中的 points_map_loader 直接加载，产生 /points_map topic （type: sensor_msgs::PointCloud2），用来在 rviz 中显示 pcd 地图。

2. 启动 gazebo 仿真系统，并加载 .yaml 参数文件。gazebo 中整个仿真系统 world + model 都是在 acrocity_traffic.world 中定义的，这一部分没有用到 urdf，后者在 rviz 中是需要的。

   因此，如果要修改仿真系统，只需要改 world 文件即可，例如调节 sensor 频率。在 prius.urdf 和  acronis_traffic.world 中都有关于 gps 频率的设定，其中在 gazebo 中真正起作用的是 acronis_traffic.world.

   gazebo 发出 /prius/center_laser/points，经过 /point_cloud_converter 转化成 /points_raw

3. 启动 voxel_grid_filter 处理 /points_raw topic 中的信息

   ```xml
   <launch>
     <arg name="sync" default="false" />
     <arg name="node_name" default="voxel_grid_filter" />
     <arg name="points_topic" default="points_raw" />
     <arg name="output_log" default="false" />
   
     <node pkg="points_downsampler" name="$(arg node_name)" type="$(arg node_name)">
       <param name="points_topic" value="$(arg points_topic)" />
       <remap from="/points_raw" to="/sync_drivers/points_raw" if="$(arg sync)" />
       <param name="output_log" value="$(arg output_log)" />
     </node>
   </launch>
   ```

## 用 gazebo + autoware 重现实车运行的步骤

### 连接 gazebo 与 autoware

- A click on this "Gazebo" button in Autoware Simulation tab in fact runs an executive file named "gazebo.sh" in ros package "runtime_manager"/scripts folder. 

- The original "gazebo.sh" file launches the default gazebo simulation. We should modify the "gazebo.sh" file to run our own simulation.

- The content of "gazebo.sh" should be

  ```bash
  #!/bin/sh
  
  roslaunch acronis_world launch_gazebo.launch &
  
  while :; do sleep 10; done
  ```

  which launchs our own simulation world. 

- The content in "launch_gazebo.launch"  should be 

  ```xml
  <?xml version="1.0"?>
  <launch>
      <include file="$(find gazebo_ros)/launch/empty_world.launch">
          <!--arg name="verbose" value="true"/-->
          <arg name="world_name" value="$(find acronis_world)/worlds/acrocity_traffic.world"/>
      </include>
  
      <rosparam command="load" file="$(find acronis_common)/config/vehicle.yaml" />
  
      <node pkg="point_cloud_converter" name="point_cloud_converter" type="point_cloud_converter_node" >
          <remap from="points_in" to="/prius/center_laser/points"/>
          <remap from="points2_out" to="/points_raw" />
      </node>    
  
      <node pkg="acronis_world" type="acceleration_ecu.py" name="acceleration_ecu">
          <remap from="input_topic" to="/twist_cmd" />
          <remap from="output_topic" to="/prius" />
      </node>
  </launch>
  ```

  If all these preparing work is done correctly, a click on "gazebo" should launch our own simulation world. 

  If gazebo couldn't open by autoware and says that acronis_world is not a launch file or something like that, you can fix it by re-compiling autoware and acronis folder. 

### mapping

这一部分是手动驱动汽车跑一圈，通过 lidar 的数据建立 3D 地图。

#### 手动驱动 prius 的控制插件 libPriusHybridPlugin.so

acrocity_traffic.world 文件中包含了 libPriusHybridPlugin.so 插件，可以通过 wsad 控制车的运动。这个插件生成在 acronis/devel/lib 中，源文件在 acronis_world/plugins 文件夹中。在源文件中， `void PriusHybridPlugin::KeyControlTypeB(const int _key)` 函数描述了按键的操控行为。

如果发现汽车运动太慢等现象，可以修改其中的参数，使汽车更快的加速或转弯。

#### autoware 操作步骤

- Simulation: 打开 gazebo

- ROSBAG: record topic /points_raw

- manually driving for one round (可能需要速度慢一些)

- Simulation: find the rosbag, play, pause

- setup -- tf (1.05   0   1.8   0   0   0)

- Computing: ndt_mapping

  这里参数设置很重要，很可能决定了 mapping 是否成功。可以用下列参数

  - Resolution: 0.5
  - Step size: 0.2
  - Transformation Epsilon: 0.01
  - Maximum iterations: 30
  - Leaf size: 0.5
  - Minimum scan range: 1
  - Minimum add scan shift: 0.5  (这个如果太小，得到的 pcd 过于密集)

  上述参数不一定是最优的，但是可以成功建图的。

- RVIZ: open config  /autoware/ros/src/.config/rviz/ndt_mapping.rviz

  这一部分很费资源，如果感觉机子比较卡，可以关掉 rviz

- Simulation: continue to play until done

- Computing: ndt_mapping -- find a place to save the map file

### waypoint generating

基本按照 autoware 原来的操作步骤

- Simulation: play the rosbag, pause
- Setup -- tf
- Map -- load pcd map
- Map -- tf
- Sensing -- voxel_grid_filter
- Computing -- 







- **Summary: Modified files**:  gazebo files, pointscloud map, waypoint file, ...

  - gazebo.sh: replace the file inside ros package "runtime_manager"/scripts folder.  use "roscd runtime_manager" to go into the ros package "runtime_manager" 
  - launch_gazebo.launch: put inside the ros package "acronis_world"/launch folder.
  - acrocity_traffic.world: replace the file inside  "acronis_world"/world folder
  - autoware-181106test1.pcd: put in anywhere you can easily find
  - tf.launch: this file maybe already exist in autoware/data/tf folder. You can put it anywhere.
  - saved_waypoints13.csv: waypoint file, put in anywhere you can easily find
  - default.rviz: A configuration file for RVIZ such that it can show all necessary information clearly.

- **Simulation $ \rightarrow$ Gazebo **

  - 

- **Setup $\rightarrow$ TF**

  This sets the transformation from base_link (the vehicle body) frame to localizer (the velodyne) frame.

  I am using  (1.05   0   1.8   0   0   0). Slight difference is acceptable.

- **Map $\rightarrow$ Ref**:  load the PCD map **(in files folder)**

- **Map $\rightarrow$ TF**

  This sets the transformation from "world" frame to "map" frame. In our situation, these two frames are the same. So the transformation should be (0 0 0 0 0 0). The loaded file is autoware-ntu-fork/data/tf/tf.launch **(in files folder)**.

- **Sensing $\rightarrow$ Point Downsampler $\rightarrow$ voxel_grid_filter **

- **Computing $\rightarrow$ ndt_localizer $\rightarrow$ ndt_matching **

  First set up the initial pose in app. This is the initial pose of vehicle in the map frame. NDT algorithm needs this initial pose to start its matching algorithm. The value is approximately (-18   12  0.5  0  0  -1.2)

- **Computing $\rightarrow$ autoware_connect $\rightarrow$ vel_pos_connect**

  Make sure that in app the simulation mode is not checked.

- **Computing $\rightarrow$ waypoint_maker $\rightarrow$ waypoint_loader**

  Load the waypoint CSV file **(in files folder)** in app

- **Computing $\rightarrow$ lane_planner $\rightarrow$ lane_rule**

- **Computing $\rightarrow$ lane_planner $\rightarrow$ lane_select**

- **Computing $\rightarrow$ lattice_planner $\rightarrow$ lattice_velocity_set**

- **Computing $\rightarrow$ lattice_planner $\rightarrow$ path_select**

- **Computing $\rightarrow$ waypoint_follower $\rightarrow$ pure_pursuit**

- **Computing $\rightarrow$ waypoint_follower $\rightarrow$ twist_filter**

- **Rviz $\rightarrow$ file $\rightarrow$ open config **

  To have a better visualization, use the configuration file "default.rviz"  **(in files folder)**



Now you can see in both Rviz and gazebo that the car is self-driving	





## 实车设备

### 清单

![singpilot_invoice1](pic/singpilot_invoice2.jpg)

怀疑上边 computer 的型号应该是 GB-BNi7HG6-1060。

### 车体外观

![hardware1](pic/hardware1.png)



### 车载设备

![hardware2](pic/hardware2.png)

### 车载电脑配置

| Dimension                 | 2.6L (220mm * 110mm * 110mm)                                 |
| ------------------------- | ------------------------------------------------------------ |
| Motherboard Size          | 100 * 150 mm                                                 |
| CPU                       | i7 - 7700 HQ  2.8GHz / 3.8 GHz                               |
| Chipset                   | Inter HM175                                                  |
| Memory                    | 2 * DDR4 slots 2133 MHZ, Max. 64GB                           |
| GPU                       | GeForce GTX 1060                                             |
| Graphic memory            | Graphic DDR5 6GB                                             |
| Wifi Card                 | Intel® Dual Band Wireless-AC Intel 8265                      |
| Audio                     | Realtek ALC255                                               |
| Video                     | GeForce® GTX 1060 Supports NVIDIA Discrete Technology (featuring Quad displays: mini DP x2, HDMI x2 Output) |
| HDMI Resolution (Max.)    | 4096x2160 @ 60Hz                                             |
| Mini DP Resolution (Max.) | 3840x2160 @ 60 Hz                                            |
| Expansion Slots           | 1 x M.2 slot (2280_storage) PCIe /SATA (Optane memory support)<br/>1 x M.2 slot (2280_storage) PCIe (CPU directly)
1 x PCIe M.2 NGFF 2230 A-E key slot occupied by the WiFi+BT card |
| Rear I/O                  | 2 x HDMI(2.0)<br/>2 x Mini DisplayPort(1.3)
3 x USB 3.0 
1 x RJ45
1 x DC-In
1 x Kensington lock slot
1 x Head phone Jack
1 x Microphone Jack
2 x USB 3.1 (1 x USB type C) |
| Storage                   | Supports 2.5” HDD/SSD, 7.0/9.5 mm thick (6 Gbps SATA3)       |
| Power Supply              | Input: AC 100-240V ,  Output: DC 19.5V 9.23A                 |
| Environment               | System Environment Operating Temperature: 0°C to +35°C<br/>System Storage Temperature: -20°C to +60°C |
| OS                        | Win10 64bit                                                  |








## 实车操作注意事项

1. 后备箱中电压显示 25V 以下就应该充电
2. charging 的时候要 switch off car，否则会一直响。switch off 的情况下，依然会有充电指示灯，7 格为充满
3. 在后备箱中的 switch 应该都是开启的，除了右边那个没有标记的，还没有对应的设备，所以暂时不开启
4. base_link 是在前轮轴的中心位置
5. 要开启自动驾驶模式，首先是打开 auto key，此时要保证 brake 在抬起的位置 (home position)，完成 brake 矫正。然后打开 car key. 
6. 在自动驾驶过程中，通过 auto key 可以完成自动、半自动的切换



## 实车中 gnss 相关

### rosgraph

![gnss_pose1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/gnss_pose1.jpg)

流程：

1. GPS 收到信号，将 nmea_msgs/Sentence type 的数据发布到 nmea_sentence topic 上。

   其中 nmea_msgs/Sentence type 数据结构如下：

   ```
   std_msgs/Header header
     uint32 seq
     time stamp
     string frame_id
   string sentence
   ```

   每个 msg 中 Sentence 形式还不一样，例如

   ![nmea_structure1](/mnt/A028967828964CE2/Users/Qipeng/learning/pic/nmea_structure1.png)

   

2. 然后由 node /nmea2tfpose 接受 sentence topic，转化成 gnss_pose ，里面包含了 position 和 orientation.

所以，总结来看，从 gps 硬件到  gnss_pose 信息，中间只需要一个 nmea2tfpose 转换一下就可以了。

