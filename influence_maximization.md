
## Kendall Tau

### 基本思想

刻画两个排序之间的相似程度。
假设有排序 $X=(x_1, x_2, \cdots, x_n)$ 和 $Y=(y_1, y_2, \cdots, y_n)$

因此有对应的二元组 $(x_1, y_1), (x_2, y_2), \cdots, (x_n, y_n)$

若二元组 $(x_i, y_i)$ 与 $(x_j, y_j)$ 排序是相符的，即若 $x_i > x_j$， 则也有 $y_i > y_j $， 则称为 concordant，若排序相反，则称为 discordant。

假设 concordant pair的数目为 $n_+$， discordant pair 的数目为 $n_-$, kendall tau 的值为 $\tau = (n_+ - n_-)/(n_+ + n_-)$.

越接近1,说明两个排序越相符

### 例子

|Candidate | interviewer 1 | interviewer2| concordant | discordant|
|--|--|--|--|--|
|A|1|1|11|0|
|B|2|2|10|0|
|C|3|4|8|1|
|D|4|3|8|0|
|E|5|6|6|1|
|F|6|5|6|0|
|G|7|8|4|1|
|H|8|7|4|0|
|I|9|10|2|1|
|J|10|9|2|0|
|K|11|12|0|1|
|L|12|11|0|0|
| |  |totals|61|5| 

计算时可以保持第1行不变，看第1行和后边各行的排序是否相符，共11个可能的 pair 都是相符的

然后看第2行与后边各行的排序，也都是相符的，共 10 个 pair

第3行与后边各行的排序中，只有第3,4行排序不相符，其余都相符，因此共 8 个 concordant pair 和 1 个 discordant pair

...

最终， $\tau = (61-5)/(61+5) = 0.85$
